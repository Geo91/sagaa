<?php

require_once("../../conexion/conn.php"); 
$conexion = mysqli_connect($host, $username, $password, $dbname);

$query = "SELECT * FROM unidad_academica ORDER BY NombreUnidadAcademica ASC";
$result1 = mysqli_query($conexion, $query);

$query2 = "SELECT * FROM organizacion ORDER BY NombreOrganizacion ASC";
$result2 = mysqli_query($conexion, $query2);

$query3 = "SELECT * FROM ubicacion_archivofisico ORDER BY DescripcionUbicacionFisica ASC";
$result3 = mysqli_query($conexion, $query3);

$query4 = "SELECT * FROM prioridad ORDER BY DescripcionPrioridad ASC";
$result4 = mysqli_query($conexion, $query4);

$query5 = "SELECT * FROM estado_seguimiento ORDER BY DescripcionEstadoSeguimiento ASC";
$result5 = mysqli_query($conexion, $query5);

$query6 = "SELECT * FROM categorias_folios ORDER BY NombreCategoria ASC";
$result6 = mysqli_query($conexion, $query6);

$query7 = "SELECT usuario.id_Usuario, CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) AS 'Encargado'
            FROM usuario
            INNER JOIN empleado ON usuario.No_Empleado=empleado.No_Empleado
            INNER JOIN persona ON persona.N_identidad=empleado.N_identidad
            WHERE usuario.Estado = 1 ORDER BY Encargado ASC"; // AND Id_rol <= 50 AND Id_rol >= 10
$result7 = mysqli_query($conexion, $query7);

?>