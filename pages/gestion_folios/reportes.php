<?php


 $maindir = "../../";

require_once($maindir."/fpdf/fpdf.php");
require($maindir."conexion/config.inc.php");

//$idFolio = $_GET['id1'];

if(isset($_GET['contenido']))
{
  $contenido = $_GET['contenido'];
}
else
{
  $contenido = 'gestion_de_folios';
  $navbar_loc = 'contenido';
}

require_once($maindir."funciones/check_session.php");

require_once($maindir."funciones/timeout.php");

require_once($maindir.'pages/navbar.php');

require_once($maindir."conexion/config.inc.php");

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
    

</head>
  <body>
    <div class="container-fluid">
    <div class="row">
        <?php
            require_once("../gestion_folios/navbar.php");
        ?>
        <div class="col-sm-10">
            <section class="content">
    <div class="container">
      <h2>Reportes Folios</h2>
      <div class="panel panel-primary">
        <div class="panel-heading"><strong>Reportes por Usuario</strong></div>

        <div class="panel-body">

          <p><strong>Fecha de Inicio: </strong><input type="date" id="fecha_i" name="datepicker_i" required  val="-1"></p>

          <p><strong>Fecha de Fin: &nbsp;&nbsp;&nbsp;&nbsp;</strong><input type="date" id="fecha_f" name="datepicker_f" required val="-1" ></p>

           <input  id="btgenerar" class="btn btn-primary" type="submit"  value="Generar Reporte" /></td>

        </div>

          </p>

        </div>

      </div>
      <div id="contenedor" class="panel-body">

      </div>
      </section>
    </div>



</div>


</div>



<script>
$( document ).ready(function() {

	$("#btgenerar").on('click',function(){

          //id1 = $(this).data('id');

			data = {

									fecha_i: $("#fecha_i").val(),
									fecha_f: $("#fecha_f").val()
								};

            $.ajax({
                async:true,
                type: "POST",
                dataType: "html",
				        data:data,
                contentType: "application/x-www-form-urlencoded",
                url:"pages/gestion_folios/reporte_filtrado.php",
                success:llegadaGuardar,
                timeout:4000,
                error:problemas
            });
            return false;

        });


});
function llegadaGuardar(datos)
{

  $("#contenedor").load('pages/gestion_folios/reporte_filtrado.php', data);

}

</script>
<script type="text/javascript" src="js/gestion_folios/navbar_lateral.js" ></script>

</body>
</html>


