<?php

$maindir = "../../";

if(isset($_GET['contenido']))
    {
      $contenido = $_GET['contenido'];
    }
  else
    {
      $contenido = 'permisos';
    }

  require_once($maindir."funciones/check_session.php");

  require_once($maindir."funciones/timeout.php");
  //require_once($maindir.'pages/navbar.php');

   if(!isset( $_SESSION['user_id'] ))
  {
    header('Location: '.$maindir.'login/logout.php?code=100');
    exit();
  }

$idusuario= $_SESSION['user_id'];
//echo $idusuario;
$fechai =  $_POST['fecha_i'];
$fechaf =  $_POST['fecha_f'];

  require_once("../../conexion/conn.php");
  $conexion = mysqli_connect($host, $username, $password, $dbname);

  $resultado = "SELECT folios.NroFolio , folios.PersonaReferente, unidad_academica.NombreUnidadAcademica AS ENTIDAD,
              categorias_folios.NombreCategoria, DATE_FORMAT(folios.FechaEntrada,'%d-%m-%Y') as FechaEntrada,
              folios.TipoFolio, folios.id_Usuario
              FROM folios
              INNER JOIN unidad_academica ON folios.UnidadAcademica = unidad_academica.Id_UnidadAcademica
              INNER JOIN categorias_folios ON folios.categoria = categorias_folios.Id_Categoria
              WHERE folios.id_Usuario= $idusuario AND FechaEntrada BETWEEN DATE_FORMAT('$fechai','%Y-%m-%d') AND DATE_FORMAT('$fechaf','%Y-%m-%d')
              UNION
              SELECT folios.NroFolio, folios.PersonaReferente, organizacion.NombreOrganizacion AS ENTIDAD, categorias_folios.NombreCategoria,
              DATE_FORMAT(folios.FechaEntrada,'%d-%m-%Y') as FechaEntrada, folios.TipoFolio, folios.id_Usuario FROM folios
              INNER JOIN organizacion ON folios.Organizacion = organizacion.Id_Organizacion
              INNER JOIN categorias_folios ON folios.categoria = categorias_folios.Id_Categoria
              WHERE folios.id_Usuario= $idusuario AND FechaEntrada BETWEEN DATE_FORMAT('$fechai','%Y-%m-%d') AND DATE_FORMAT('$fechaf','%Y-%m-%d')
              ORDER BY NroFolio ASC";


  $result1 = mysql_query($resultado);

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta charset="iso-8859-1">
</head>


<?php if ($fechai!="" & $fechaf!=""){ ?>
  <body>
    <div class="container">
      <div class="panel panel-primary">
        <div class="panel-heading">Reportes</div>
        <div class="panel-body">
          <?php  echo '<button class="btn btn-danger pull-right" id="exp" data-mode="verPDF" data-id= "'.$idusuario.'"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Exportar a PDF</button>'; ?>
        </br>
        </br>

          <table id="tabla_folios" class="table table-bordered table-striped" style="text-align: center;">
            <thead>
                <tr>
                    <th style="text-align: center;color:white; background-color:#386D95;">Nro Folio</th>
                    <th style="text-align: center;color:white; background-color:#386D95;">Persona Referente</th>
                    <th style="text-align: center;color:white; background-color:#386D95;">Entidad</th>
                    <th style="text-align: center;color:white; background-color:#386D95;">Categor&iacute;a</th>
                    <th style="text-align: center;color:white; background-color:#386D95;">Tipo Folio</th>
                    <th style="text-align: center;color:white; background-color:#386D95;">Fecha Entrada</th>
                </tr>
            </thead>
            <tbody>
                   <tr>
                     <?php
                       while ($row = mysql_fetch_array($result1)) {

                     ?>
                     <td><?php echo $row['NroFolio']; ?></td>
                     <td><?php echo utf8_encode($row['PersonaReferente']); ?></td>
                     <td><?php echo utf8_encode($row['ENTIDAD']); ?></td>
                     <td><?php echo utf8_encode($row['NombreCategoria']); ?></td>
                     <?php
                       $TipoFolio = $row['TipoFolio'];
                         if ($TipoFolio == 0) {
                             echo "<td> Entrada </td>";
                           }else{
                             echo "<td> Salida </td>";
                           } ?>

                     <td><?php echo $row['FechaEntrada']; ?></td>


</tr>
 <?php } ?>
            </tbody>
          </table>

        </div>

        <?php
        }
        else {//condicion en caso de que el usuario no haya introducido un rango de fecha, se mostrar� una alerta!
          echo '<div class="container">';
          echo '<div class="alert alert-danger">';
          echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
          echo '<strong>Error! </strong>';
          echo "Ingrese un Rango de Fechas";
          echo '</div>';
          echo '</div>';

        }
        ?>
          </p>

        </div>

      </div>

  </body>

</html>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#tabla_folios').dataTable(); } );
    </script>
<script>
    $( document ).ready(function() {


        $("#exp").on('click',function(){
          mode = $(this).data('mode');
          id1 = $(this).data('id');
          fecha_i= $("#fecha_i").val();
    		  fecha_f= $("#fecha_f").val();
          if(mode == "verPDF"){
            window.open('pages/gestion_folios/Reportes_pdf.php?&fecha_i='+fecha_i+'&fecha_f='+fecha_f);
          }
        });
    });
</script>
