<?php

$maindir = "../../";

require_once($maindir."funciones/check_session.php");

require_once($maindir."funciones/timeout.php");

 if(!isset( $_SESSION['user_id'] ))
{
  header('Location: '.$maindir.'login/logout.php?code=100');
  exit();
}
$idusuario= $_SESSION['user_id'];

$fechai =  $_GET['fecha_i'];
$fechaf =  $_GET['fecha_f'];

require($maindir."conexion/config.inc.php");

$resultado = "SELECT folios.NroFolio , folios.PersonaReferente, unidad_academica.NombreUnidadAcademica AS ENTIDAD,
            categorias_folios.NombreCategoria, DATE_FORMAT(folios.FechaEntrada,'%d-%m-%Y') as FechaEntrada,
            folios.TipoFolio, folios.id_Usuario
            FROM folios
            INNER JOIN unidad_academica ON folios.UnidadAcademica = unidad_academica.Id_UnidadAcademica
            INNER JOIN categorias_folios ON folios.categoria = categorias_folios.Id_Categoria
            WHERE folios.id_Usuario= $idusuario AND FechaEntrada BETWEEN DATE_FORMAT('$fechai','%Y-%m-%d') AND DATE_FORMAT('$fechaf','%Y-%m-%d')
            UNION
            SELECT folios.NroFolio, folios.PersonaReferente, organizacion.NombreOrganizacion AS ENTIDAD, categorias_folios.NombreCategoria,
            DATE_FORMAT(folios.FechaEntrada,'%d-%m-%Y') as FechaEntrada, folios.TipoFolio, folios.id_Usuario FROM folios
            INNER JOIN organizacion ON folios.Organizacion = organizacion.Id_Organizacion
            INNER JOIN categorias_folios ON folios.categoria = categorias_folios.Id_Categoria
            WHERE folios.id_Usuario= $idusuario AND FechaEntrada BETWEEN DATE_FORMAT('$fechai','%Y-%m-%d') AND DATE_FORMAT('$fechaf','%Y-%m-%d')
            ORDER BY NroFolio ASC";
            $query = $db->prepare($resultado);
            //$query ->bindParam(":Id_Seguimiento",$Id_Seguimiento);
            $query->execute();
            $rows = $query->fetchAll();

require_once($maindir."/fpdf/fpdf.php");

$pdf = new FPDF('L','mm','A4');

$pdf->AddPage();
$pdf->SetFont('Arial', '', 18);
//$pdf->Image($maindir.'assets/img/lucen-aspicio.png', 50,30,200,200, 'PNG');
$pdf->Image($maindir.'assets/img/logo_unah.png' , 40,5,20,35, 'PNG');
$pdf->Image($maindir.'assets/img/logo-cienciasjuridicas.png' , 230,8, 35 , 35,'PNG');
$pdf->Cell(18, 10, '', 0);
$pdf->SetFont('Arial', 'B', 18);
$pdf->Cell(5, 10, '', 0);
$pdf->Cell(230, 25, ("Universidad Nacional Autónoma de Honduras"), 0, 0, "C");
$pdf->Ln(25);
$pdf->SetFont('Arial', 'U', 14);
$pdf->Cell(30, 8, ' ', 0,0,"C");
$pdf->Cell(230, 8, utf8_decode("Reporte de Folios"), 0,0,"C");

$pdf->SetFont('Arial', '', 12);
$pdf->Ln(10);
$pdf->Cell(190, 8, 'Fecha: '.date('Y-m-d'), 0);
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 10);
$pdf->SetFillColor(56,109,149);//Fondo verde de celda
$pdf->SetTextColor(56,109,149);
$pdf->SetFillColor(255,255,204);
$pdf->SetFillColor('150','125','255'); 
$pdf->Cell(40, 9, utf8_decode("No.Folio"), 1,0,"C");
$pdf->Cell(90, 9, utf8_decode("Persona Referente"), 1,0,"C");
$pdf->Cell(70, 9, ("Unidad académica u Organización"), 1,0,"C");
$pdf->Cell(35, 9, ("Nombre Categoria"), 1,0,"C");
$pdf->Cell(20, 9, utf8_decode("Tipo Oficio"), 1,0,"C");
$pdf->Cell(25, 9, utf8_decode("Fecha Entrada"), 1,0,"C");
$pdf->Ln(8);
$pdf->SetFont('Arial', '', 8);
$pdf->SetTextColor(0,0,0);

$result1 = mysql_query($resultado);
//$rows = mysql_fetch_array($result1);
 while ($row = mysql_fetch_array($result1)) {
   $NroFolio = $row['NroFolio'];
   $PersonaReferente = $row['PersonaReferente'];
   $ENTIDAD = $row['ENTIDAD'];
   $NombreCategoria = $row['NombreCategoria'];
   $TipoFolio = $row['TipoFolio'];
   $FechaEntrada = $row['FechaEntrada'];

   $pdf->SetFillColor(2,157,116);
   $pdf->CellFitScale(40, 9,($NroFolio), "L,R,B",0,"C");
   $pdf->SetFillColor(2,157,116);
   $pdf->CellFitScale(90, 9,($PersonaReferente), "L,R,B",0,"C");
   $pdf->CellFitScale(70, 9, ($ENTIDAD), "L,R,B",0,"C");
   $pdf->CellFitScale(35, 9,($NombreCategoria), "L,R,B",0,"C");
   if($TipoFolio == 0){
     $TipoFolio='Entrada';
     $pdf->CellFitScale(20, 9, ($TipoFolio), "L,R,B",0,"C");
   } else {
     $TipoFolio = 'Salida';
     $pdf->CellFitScale(20, 9, ($TipoFolio), "L,R,B",0,"C");
   }

   $pdf->CellFitScale(25, 9, utf8_decode($row['FechaEntrada']), "L,R,B",0,"C");
   $pdf->Ln(9);

}



$pdf->Output('Folios.pdf','I');


 ?>
