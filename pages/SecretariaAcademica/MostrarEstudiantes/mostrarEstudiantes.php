<?php
include ('../../../conexion/config.inc.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <script type="text/javascript" src="pages/SecretariaAcademica/MostrarEstudiantes/scripts.js"></script>
    
    <script type="text/javascript">

        $(document).ready(function () {
            $('#estudiantes').dataTable({
                "order": [[0, "asc"]],
                "fnDrawCallback": function (oSettings) {


                }
                ,
                "language":
                {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "No se han encontrado registros",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
                    "search": "Buscar",
                    "paginate":
                            {
                                "previous": "Anterior",
                                "next" : "Siguiente"
                            }
                }
            }); // example es el id de la tabla
        });

    </script>
    <!-- Script necesario para que la tabla se ajuste a el tamanio de la pag-->
    <script type="text/javascript">
        // For demo to fit into DataTables site builder...
        $('#estudiantes')
                .removeClass('display')
                .addClass('table table-striped table-bordered');
    </script>
  </head>
  <body>
      <!-- .panel-heading -->
      <div class="panel-body">
        <div id = "notificaciones"></div>
        <div class="panel-group" id="accordion">
          <h2>Mostrar Estudiantes</h2>
          <div name="alerta" id="alerta"></div>
          <div class="row">
            <form role="form" id="form" name="form" class="form-horizontal">
            <div>
              <div class="col-sm-3">
                  <input type="button" name="nuevo" id="nuevo" value="Registrar Nuevo Estudiante" class="ActualizarB btn btn-primary" onclick="request(this.id)" readonly/>
              </div>
            </div>
              <!-- TABLA DE ESTUDIANTES CREADAS-->
              <label class="col-sm-12 control-label"></label>

              <div class="col-md-12">
                <div class="panel panel-primary">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <label>
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Estudiantes </label>
                    </h4>
                  </div>
                  <div class="panel-body">
                    <div id="paginador"></div>
                        <?php include ("../../../pages/SecretariaAcademica/MostrarEstudiantes/cargarEstudiantes.php"); ?>
                    <div class="col-sm-3">
                      <input type="button" name="GenerarDocumentos" id="GenerarDocumentos" value="Exportar PDF" class="ActualizarB btn btn-primary"/>
                    </div>
                  </div> 
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </body>
  </html>

  