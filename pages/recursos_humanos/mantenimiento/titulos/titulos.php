<?php
$mkdir = "../../../../";
include($mkdir."conexion/config.inc.php");

if(isset($_GET['contenido'])){
    $contenido = $_GET['contenido'];
}else{
    $contenido = 'recursos_humanos';
}

require_once($mkdir."funciones/check_session.php");

require_once($mkdir."funciones/timeout.php");
  
if(!isset( $_SESSION['user_id'] )){
    header('Location: '.$mkdir.'login/logout.php?code=100');
    exit();
}

?>

<script type="text/javascript">

$(document).ready(function() {

    //inicializarTabla(); //Se inicializa la tabla que despliega los datos de la base de datos
    cargarTitulos(); //Hacemos el llamado para que se cargue la tabla siempre que se cargue la pag.
    obtenerTipoEstudio();

    function soloLetras(text){
        var letters = /^[ a-zA-ZáéíóúÁÉÍÓÚÑñ ]+$/; 
		if(text.match(letters)){
            return true;
		}else{
			return false;
		}
    }

    function validacion(){
	    var nombre = $("#descTitulo").val();

		if(soloLetras(nombre) == false){
		    $("#tituloNombre").addClass("has-warning");
			$("#tituloNombre").find("label").text("Descripción del Título: Solo es permitido el ingreso de letras");
			$("#descTitulo").focus();
			return false;
		}else{
		    $("#tituloNombre").removeClass("has-warning");
			$("#tituloNombre").find("label").text("Descripción del Título");
            return true;
		}
		return true;
	}

    /* Este evento se levanta cada vez que le damos click al boton eliminar */
    $(document).on("click",".eliminar_titulo",function () {
             
        var respuesta = confirm("¿Esta seguro de que desea eliminar el registro seleccionado?");
        if (respuesta)
        {
            var id = $(this).data('id');

            var data = 
            {
                codigo: id
            };
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                data: data,
                url: "pages/recursos_humanos/mantenimiento/titulos/eliminarTitulo.php",
                timeout: 4000,
                contentType: "application/x-www-form-urlencoded",
                success: function(data){
                    $("#notificaciones1").html(data);
                    $("#notificaciones1").fadeOut(4500);
                    cargarTitulos();
                }
            });
        }                 
    });


 /* Este evento se levanta cuando se le da click a el boton de editar */
   $(document).on("click",".editar",function (e){
        e.preventDefault();
        $("#Mdiv_editarTitulo").html("");
        var datos = {
            id:  $(this).data('id') 
        };
        $.ajax({
            async: true,
            type: "POST",
            url: "pages/recursos_humanos/mantenimiento/titulos/actualizarTitulo.php",
            data: datos,
            dataType: "html",
            success: function(data){
                var response = JSON.parse(data);
                var options = '';
                /* En esta seccion llenamos los datos que se van a editar el modal*/ 
                for (var index = 0;index < response.length; index++) 
                {
                   options +=   '<div class="form-group">'+
                                    '<label style="display : none">Codigo del Titulo</label>'+
                                    '<input style="display : none" id="codTitulo" disabled = "true" data-id="' + response[index].codTitulo + '" placeholder="' + response[index].codTitulo + '" class="form-control">'+
                                '</div>'+
                                '<div class="form-group">'+
                                    '<label>Descripción del Título</label>'+
                                    '<input id="descTitulo1" placeholder="'+response[index].descTitulo+'" class="form-control" required>'+
                                '</div>';
                }

                /* Insertamos dentro del div del modal los datos obtenidos en la base de datos */
                $("#Mdiv_editarTitulo").empty();
                $("#Mdiv_editarTitulo").append(options);
                obtenerTipoEstudioModal(); //Llamamos esta función para agregar un combobox a la modal
            },
            timeout: 4000
        });
    });

    /* Evento se levanta cuando queremos insertar un nuevo titulo */
    $("#formInsertar").submit(function (e){
        e.preventDefault();
        if(validacion()){
	        var datos = {
	            descTitulo: $("#descTitulo").val(),
                codTipoEstudio: $("#tipoEstudio").val()
	        };
	        $.ajax({
	            async: true,
	            type: "POST",
	            url: "pages/recursos_humanos/mantenimiento/titulos/insertarTitulo.php",
	            data: datos,
	            dataType: "html",
	            success: function(data){   
	                $("#notificaciones").html(data);
	                $("#notificaciones").fadeOut(4500);
	                cargarTitulos();
	            },
	            timeout: 4000
	        });
    	}
    	return false;

    });

    /* Esta funcion es llamada al principio, es la encargada de actualizar la tabla que muestra los titulos que estan en el sistema en ese momento, se recarga cada vez que se elimina o se actualiza un registro*/
    function cargarTitulos(){
        var datos = {
            accion: 1
         };

        $.ajax({
            async: true,
            type: "POST",
            url: "pages/recursos_humanos/mantenimiento/titulos/cargarTitulos.php",
            data: datos,
            dataType: "html",
            success: function(data){
                var response = JSON.parse(data);
                var options = '';
                /* Seccion para llenar la tabla */
                for (var index = 0;index < response.length; index++) 
                {
                   options += '<tr>' +
                                    '<td style="display : none">' + response[index].codTitulo + '</td>' +
                                    '<td>' + response[index].descTitulo + '</td>' +
                                    '<td>' + response[index].grado + '</td>' +
                                    '<td><center>'+
                                        '<button data-id = "'+ response[index].codTitulo +'" href= "#" class = "editar btn_editar btn btn-info"  data-toggle="modal" data-target = ""><i class="glyphicon glyphicon-edit"></i></button>'+
                                    '</td></center>' +
                                    '<td><center>'+
	                                        '<button data-id = "'+ response[index].codTitulo +'" href= "#" class = "eliminar_titulo btn_editar btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>' +
                                    '</td></center>' +             
                              '</tr>';
                }

            
                $("#cTablaTitulos").html(options);

                /* Script que permite a la tabla hacer busquedas dentro de ella
                    y ordenarla deacuerdo a lo que se presenta en ella. */

                    $('#TableTitulos').dataTable();
            },
            timeout: 4000
        }); 
    }

    function obtenerTipoEstudio()
    {                     
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            url: "pages/recursos_humanos/mantenimiento/titulos/obtenerTipoEstudio.php",
            success: function(response){
                                           
                var arr = JSON.parse(response);
                
                var options = '';
                var val='NULL';
                var def='Seleccione una opción';
                options += '<option value="' + val + '">' +
                                def + '</option>';
                        
                for (var index = 0; index < arr.length; index++) 
                {
                    var codTipoEstudio = arr[index].codTipoEstudio;
                    var descripcion = arr[index].descripcion;
                    
                    options += '<option value="' + codTipoEstudio + '">' +
                                descripcion + '</option>';
                }
                
                $("#tipoEstudio").html(options);
            },
            timeout: 4000,
        });
    }

    //Esta funcion recupera los tipos de estudio almacenados en la base de datos y los muestra en un combobox que se carga en la modal
    function obtenerTipoEstudioModal()
    {                    
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            url: "pages/recursos_humanos/mantenimiento/titulos/obtenerTipoEstudio.php",
            success: function(response){

                var arr = JSON.parse(response);
                var options = '';
                options += '<div class="row form-group">'+
                                '<label class = "col-sm-4 control-label">Grado del Título:</label>'+
                                '<div class = "col-sm-8">'+
                                    '<select class = "form-control" id="tipoEstudioM" name="tipoEstudioM" required>'+
                                        '<option value = "NULL">Seleccione una opción</option>';
                        
                for (var index = 0; index < arr.length; index++) 
                {
                    var codTipoEstudioM = arr[index].codTipoEstudio;
                    var descripcionM = arr[index].descripcion;
                    
                    options += '<option value="' + codTipoEstudioM + '">' +
                                descripcionM + '</option>';
                }
                    options += '</select>'+
                                '</div>'+
                            '</div>';
                
                $("#Mdiv_editarTitulo").append(options); //carga el combobox a la pantalla modal
                $('#compose-modal-modificar').modal('show'); //muestra la pantalla modal
            },
            timeout: 4000,
        });
    }

    /* Seccion que se manda a llamar cuando dentro de la modal aceptamos e indicamos que lo que queremos 
    modificar ya se hizo */

    $("#form_Modificar").submit(function(e) {
            e.preventDefault();
            $("#compose-modal-modificar").modal('hide');
            datosEditados = {
                codTitulo: $("#codTitulo").data('id'),
                descTitulo1: $("#descTitulo1").val(),
                codtipoEstudioM: $("#tipoEstudioM").val()
            };
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                data: datosEditados,
                contentType: "application/x-www-form-urlencoded",
                url: "pages/recursos_humanos/mantenimiento/titulos/tituloEditado.php",
                success: function(data){ 
                    $("#notificaciones1").html(data);
                    $("#notificaciones1").fadeOut(4500);
                    cargarTitulos();
                },
                timeout: 4000
            });
        });
});

</script>



<!-- En este div se muestran las alertas y todos los mensajes que el servidor envía en respuesta a la inserción de nuevos registros en la base de datos -->
<div id = "notificaciones"></div>

<!-- Seccion para que el usuario indique el nuevo país que quiere ingresar -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <label><span class="glyphicon glyphicon-globe" aria-hidden="true"></span> Nuevo Título </label>
            </div>
            <div class="panel-body">
                <div>
                       <div id= "noti1" class="alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden</center></div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <form id = "formInsertar" role="form" action="#" method="POST">
                            <div id="tituloNombre" class="form-group">
                                <label>Nombre del Título</label>
                                <input placeholder = "Se necesita una descripción" type="text"  class="form-control" name="descTitulo" id="descTitulo" required >
                            </div>
                            <div class="row form-group">
                                <label class = "col-sm-4 control-label">Grado del Título:</label>
                                <div class = "col-sm-8">
                                    <select class = "form-control" id="tipoEstudio" name="tipoEstudio" required>
                                        <option value = "NULL">Seleccione una opción</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" name="submit"  id="submit" class="submit btn btn-info glyphicon glyphicon-floppy-disk" > Guardar </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- En este div se muestran todos los mensajes que el servidor envía en respuesta a la modificación y eliminacion dentro de la base de datos-->
<div id = "notificaciones1"></div>

<!-- TABLA PARA DESPLEGAR LOS PAISES QUE SE ENCUENTRAN EN LA BASE DE DATOS-->
<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Países </label>
        </div>
        <div class="panel-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id= "TableTitulos" border="1" class='table table-bordered table-hover table-striped'>
                                    <thead>
                                        <tr>
                                            <th style = 'display:none'>Codigo</th>
                                            <th style="text-align:center;background-color:#386D95;color:white;">Nombre Título</th> 
                                            <th style="text-align:center;background-color:#386D95;color:white;">Grado</th> 
                                            <th style="text-align:center;background-color:#386D95;color:white;">Actualización</th>  
                                            <th style="text-align:center;background-color:#386D95;color:white;">Eliminar</th>                  
                                        </tr>
                                    </thead>
                                    <tbody id = "cTablaTitulos">
                                        <!-- Contenido de la tabla generado atravez de la consulta a 
                                            la base de datos -->
                                    </tbody>
                                </table>       
                            </div>
                    </div>                
                </div>
            </div>       
        </div>
    </div>
</div>

<!-- Modal para editar los registros de la tabla -->

<div  class="modal fade" id="compose-modal-modificar" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="form_Modificar" name="form_Modificar" action="#">
                <div class="modal-header" style = "background-color:#0FA6C3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style = "color:white" id="myModalLabel"><i class="glyphicon glyphicon-edit" style = "color:white"></i> Modificar Títulos</h4>
                </div>
                <div class = "modal-body">
                    <div id= "Mdiv_editarTitulo">

                    </div> 
                </div>
                <div class="modal-footer">
                    <button  id="guardaredicion" class="btn btn-primary glyphicon glyphicon-floppy-disk" > Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>