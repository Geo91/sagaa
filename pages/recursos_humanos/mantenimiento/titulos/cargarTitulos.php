<?php
$mkdir = "../../../../";
include($mkdir."conexion/config.inc.php");

/* Hacemos una consulta a la base de datos para obtener los datos de la tabla */
$query = 'SELECT titulo.id_titulo, titulo.titulo, tipo_estudio.Tipo_estudio from titulo INNER JOIN tipo_estudio on titulo.id_tipo_estudio=tipo_estudio.ID_Tipo_estudio';
$result = mysql_query($query);
$json = array();
$contadorIteracion = 0;


    while ($fila = mysql_fetch_array($result)) { 

        $json[$contadorIteracion] = array
        (
            "codTitulo" => utf8_encode($fila["id_titulo"]),
            "descTitulo" => utf8_encode($fila["titulo"]),
            "grado" => utf8_encode($fila["Tipo_estudio"])
        );

        $contadorIteracion++;
    }

    //Retornamos el jason con todos los elementos tomados de la base de datos.
    echo json_encode($json);
?>