<?php
$mkdir = "../../../../";
include($mkdir."conexion/config.inc.php");

if(isset($_GET['contenido'])){
    $contenido = $_GET['contenido'];
}else{
    $contenido = 'recursos_humanos';
}

require_once($mkdir."funciones/check_session.php");

require_once($mkdir."funciones/timeout.php");
  
if(!isset( $_SESSION['user_id'] )){
    header('Location: '.$mkdir.'login/logout.php?code=100');
    exit();
}

?>

<script type="text/javascript">

$(document).ready(function() {

    cargarCargos(); //Hacemos el llamado para que se cargue la tabla siempre que se cargue la pag.

    function soloLetras(text){
        var letters = /^[ a-zA-ZáéíóúÁÉÍÓÚÑñ]+$/; 
		if(text.match(letters)){
            return true;
		}else{
			return false;
		}
    }

    function validacion(){
	    var nombre = $("#descCargo").val();

		if(soloLetras(nombre) == false){
		    $("#cargoNombre").addClass("has-warning");
			$("#cargoNombre").find("label").text("Nombre del Cargo: Solo es permitido el ingreso de letras");
			$("#descCargo").focus();
			return false;
		}else{
		    $("#cargoNombre").removeClass("has-warning");
			$("#cargoNombre").find("label").text("Nombre del Cargo");
		}
		return true;
	}

    /* Este evento se levanta cada vez que le damos click al bottton eliminar */
    $(document).on("click",".eliminar_cargo",function () {
             
        var respuesta = confirm("¿Esta seguro de que desea eliminar el registro seleccionado?");
        if (respuesta)
        {
            var id = $(this).data('id');

            var data = 
            {
                codigo: id
            };
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                data: data,
                url: "pages/recursos_humanos/mantenimiento/cargos/eliminarCargo.php",
                timeout: 4000,
                contentType: "application/x-www-form-urlencoded",
                success: function(data){
                    $("#notificaciones1").html(data);
                    $("#notificaciones1").fadeOut(4500);
                    cargarCargos();
                }
            });
        }            
    });


 /* Este evento se levanta cuando se le da click a el boton de editar */
   $(document).on("click",".editar",function (e){
        e.preventDefault();
        $("#Mdiv_editarCargo").html("");
        var datos = {
            id:  $(this).data('id') 
        };
        $.ajax({
            async: true,
            type: "POST",
            url: "pages/recursos_humanos/mantenimiento/cargos/actualizarCargo.php",
            data: datos,
            dataType: "html",
            success: function(data){
                var response = JSON.parse(data);
                var options = '';
                /* En esta seccion llenamos los datos que se van a editar el modal*/ 
                for (var index = 0;index < response.length; index++) 
                {
                   options +=   '<div class="form-group">'+
                                    '<label style="display : none">Codigo del Grupo</label>'+
                                    '<input style="display : none" id="codCargo" disabled = "true" data-id="' + response[index].codCargo + '" placeholder="' + response[index].codCargo + '" class="form-control">'+
                                '</div>'+
                                '<div class="form-group">'+
                                    '<label>Nombre del Cargo</label>'+
                                    '<input id="descCargo1" placeholder="'+response[index].descCargo+'" class="form-control" required>'+
                                '</div>';
                }

                /* Insertamos dentro del div del modal los datos obtenidos en la base de datos */
                $("#Mdiv_editarCargo").empty();
                $("#Mdiv_editarCargo").append(options);

                /* Cuando ya tenemos listo el modal con los datos que queriamos abrimos el modal */
                $('#compose-modal-modificar').modal('show');
            },
            timeout: 4000
        });

    });

    /* Evento se levanta cuando queremos insertar un nuevo motivo */
    $("#formInsertar").submit(function (e){
        e.preventDefault();
        if(validacion()){
	        var datos = {
	            descCargo: $("#descCargo").val()
	        }
	        $.ajax({
	            async: true,
	            type: "POST",
	            url: "pages/recursos_humanos/mantenimiento/cargos/insertarCargo.php",
	            data: datos,
	            dataType: "html",
	            success: function(data){
	                $("#notificaciones").html(data);
	                $("#notificaciones").fadeOut(4500);
	                cargarCargos();
	            },
	            timeout: 4000
	        });
    	}
    	return false;

    });

    /* Esta funcion es llamada al principio, es la encargada de actualizar la tabla que muestra los tipo de estudio  que estan en el sistema en ese momento, se recarga cada vez que se elimina o se actualiza */
    function cargarCargos(){
        var datos = {
            accion: 1
         };

        $.ajax({
            async: true,
            type: "POST",
            url: "pages/recursos_humanos/mantenimiento/cargos/cargarCargos.php",
            data: datos,
            dataType: "html",
            success: function(data){
                var response = JSON.parse(data);
                var options = '';
                /* Seccion para llenar la tabla */
                for (var index = 0;index < response.length; index++) 
                {
                   options += '<tr>' +
                                    '<td style="display : none">' + response[index].codCargo + '</td>' +
                                    '<td>' + response[index].descCargo + '</td>' +
                                    '<td><center>'+
                                        '<button data-id = "'+ response[index].codCargo +'" href= "#" class = "editar btn_editar btn btn-info"  data-toggle="modal" data-target = ""><i class="glyphicon glyphicon-edit"></i></button>'+
                                    '</td></center>' +
                                    '<td><center>'+
	                                        '<button data-id = "'+ response[index].codCargo +'" href= "#" class = "eliminar_cargo btn_editar btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>' +
                                    '</td></center>' +             
                              '</tr>';
                }

            
                $("#cTablaCargos").html(options);

                /* Script que permite a la tabla hacer busquedas dentro de ella
                    y ordenarla deacuerdo a lo que se presenta en ella. */

                    $('#TableCargos').dataTable();
            },
            timeout: 4000
        }); 
    }



    /* Seccion que se manda a llamar cuando dentro de la modal aceptamos e indicamos que lo que queremos 
    modificar ya se hizo */

    $("#form_Modificar").submit(function(e) {
            e.preventDefault();
            $("#compose-modal-modificar").modal('hide');
            datosEditados = {
                codCargo: $("#codCargo").data('id'),
                descCargo1: $("#descCargo1").val(),
            };
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                data: datosEditados,
                contentType: "application/x-www-form-urlencoded",
                url: "pages/recursos_humanos/mantenimiento/cargos/cargoEditado.php",
                success: function(data){ 
                    $("#notificaciones1").html(data);
                    $("#notificaciones1").fadeOut(4500);
                    cargarCargos();
                },
                timeout: 4000
            });
        });
});

</script>



<!-- En este div se muestran las alertas y todos los mensajes que el servidor envía en respuesta a la inserción de nuevos registros en la base de datos -->
<div id = "notificaciones"></div>

<!-- Seccion para que el usuario indique el nuevo grupo o comité que quiere ingresar -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <label><span class="glyphicon glyphicon-globe" aria-hidden="true"></span> Nuevo Cargo</label>
            </div>
            <div class="panel-body">
                <div>
                       <div id= "noti1" class="alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden</center></div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <form id = "formInsertar" role="form" action="#" method="POST">
                            <div id="cargoNombre" class="form-group">
                                <label>Nombre del Cargo</label>
                                <input placeholder = "Se necesita un nombre" type="text"  class="form-control" name="descCargo" id="descCargo" required >
                            </div>
                            <button type="submit" name="submit"  id="submit" class="submit btn btn-info glyphicon glyphicon-floppy-disk"> Guardar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- En este div se muestran todos los mensajes que el servidor envía en respuesta a la modificación y eliminacion dentro de la base de datos-->
<div id = "notificaciones1"></div>

<!-- TABLA PARA DESPLEGAR LOS GRUPOS QUE SE ENCUENTRAN EN LA BASE DE DATOS-->
<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Cargos </label>
        </div>
        <div class="panel-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id= "TableCargos" border="1" class='table table-bordered table-hover table-striped'>
                                    <thead>
                                        <tr>
                                            <th style = 'display:none'>Codigo</th>
                                            <th style="text-align:center;background-color:#386D95;color:white;">Descripción</th> 
                                            <th style="text-align:center;background-color:#386D95;color:white;">Actualización</th>  
                                            <th style="text-align:center;background-color:#386D95;color:white;">Eliminar</th>                  
                                        </tr>
                                    </thead>
                                    <tbody id = "cTablaCargos">
                                        <!-- Contenido de la tabla generado atravez de la consulta a 
                                            la base de datos -->
                                    </tbody>
                                </table>       
                            </div>
                    </div>                
                </div>
            </div>       
        </div>
    </div>
</div>

<!-- Modal para editar los registros de la tabla -->

<div  class="modal fade" id="compose-modal-modificar" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="form_Modificar" name="form_Modificar" action="#">
                <div class="modal-header" style = "background-color:#0FA6C3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style = "color:white" id="myModalLabel"><i class="glyphicon glyphicon-edit" style = "color:white"></i> Modificar Cargos</h4>
                </div>
                <div class = "modal-body">
                    <div id= "Mdiv_editarCargo">
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button  id="guardaredicion" class="btn btn-primary" > Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>