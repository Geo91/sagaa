<?php
$mkdir = "../../../../";
include($mkdir."conexion/config.inc.php");

/* Hacemos una consulta a la base de datos para obtener los datos de la tabla */
$query = 'SELECT universidad.Id_universidad, universidad.nombre_universidad, pais.Nombre_pais from universidad INNER JOIN pais on universidad.Id_pais=pais.Id_pais';
$result = mysql_query($query);
$json = array();
$contadorIteracion = 0;


    while ($fila = mysql_fetch_array($result)) { 

        $json[$contadorIteracion] = array
        (
            "codUniversidad" => utf8_encode($fila["Id_universidad"]),
            "descUniversidad" => utf8_encode($fila["nombre_universidad"]),
            "nomPais" => utf8_encode($fila["Nombre_pais"])
        );

        $contadorIteracion++;
    }

    //Retornamos el jason con todos los elementos tomados de la base de datos.
    echo json_encode($json);
?>