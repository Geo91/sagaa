<?php

 $maindir = "../../";

  if(isset($_GET['contenido']))
    {
      $contenido = $_GET['contenido'];
    }
  else
    {
      $contenido = 'recursos_humanos';
    }

  require_once($maindir."funciones/check_session.php");

  require_once($maindir."funciones/timeout.php");
  
   if(!isset( $_SESSION['user_id'] ))
  {
    header('Location: '.$maindir.'login/logout.php?code=100');
    exit();
  }

if(isset($_POST["tipoProcedimiento"])){
    $tipoProcedimiento = $_POST["tipoProcedimiento"];
    if($tipoProcedimiento == "insertar"){
    require_once("../../Datos/insertarEmpleadoXgrupo.php");
    }
     if($tipoProcedimiento == "Eliminar"){
    require_once("../../Datos/eliminarEmpleadoXGrupo.php");
    }
}

include '../../conexion/config.inc.php';
$consulta = "SELECT grupo_o_comite.ID_Grupo_o_comite, grupo_o_comite.Nombre_Grupo_o_comite, COUNT(No_Empleado) AS 'Cantidad' FROM grupo_o_comite_has_empleado right join grupo_o_comite on grupo_o_comite.ID_Grupo_o_comite=grupo_o_comite_has_empleado.ID_Grupo_o_comite GROUP BY grupo_o_comite.Nombre_Grupo_o_comite, grupo_o_comite.ID_Grupo_o_comite ORDER BY grupo_o_comite.ID_Grupo_o_comite;";
$rec = mysql_query($consulta);

//echo ($nGrupo);

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">   
    
<script>
        
   var x;
    x = $(document);
    x.ready(inicio);
           
    var y;
    function inicio()
    {       
        var x;
        x = $(".agregarbg");
        x.click(variable);
                
        var x;
        x = $(".verbg");
        x.click(variable);
        
        var x;
        x = $(".verbg");
        x.click(ver);  
    }
            
    function variable()
    {
        y= $(this).parents("tr").find("td").eq(0).html(); 
    }
                   
    function ver()
    {
        data2 = {idGrupo:y};

        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            //  url:"pages/recursos_humanos/modi_universidad.php",  
            beforeSend: inicioVer,
            success: verGrupo,
            timeout: 4000,
            error: problemasVer
        });
        return false;
    }
            

    $(document).ready(function(){
        $("form").submit(function(e) {
            e.preventDefault();
          
            if(validator()){
                codE=$("#codEmple").val();
                $("#compose-modal").modal('hide');
                data={
                    cod_empleado:codE,
                    idG:y,
                    tipoProcedimiento:"insertar"
                };

                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                   
                    beforeSend: inicioEnvioXgrupo,
                    success: insertarEmpleadoGrupo,
                    timeout: 4000,
                    error: problemasXgrupo
                });
            }
                return false;
            });
        
        $("#buscarE").click(function(){
            {
                if(validator()){
                    id=$("#codEmple").val();
                    data={
                    idempleado:id,
                    };
                
                    $.ajax({
                        async: true,
                        type: "POST",
                        dataType: "html",
                        contentType: "application/x-www-form-urlencoded",
                        url:"pages/recursos_humanos/gestion_Grupos_comite.php",
                        beforeSend: inicioEnvio,
                        success: buscarEmpleado,
                        timeout: 4000,
                        error: problemas
                    });
                }
                return false;
            }
        }); 
   });
        
        
    function soloLetrasYNumeros(text)
    {
	    var letters = /^[0-9]+$/; 
		if(text.match(letters)){
		    return true;
		}else{
		    return false;
		}
	}

    function validator(){
	    var nombre = $("#codEmple").val();

		//valida si se han itroduzido otros digitos aparte de numeros y letras
		if(soloLetrasYNumeros(nombre) == false){
		    $("#buscar").addClass("has-warning");
			$("#buscar").find("label").text("Codigo de empleado: Solo son permitidos numeros");
			$("#codEmple").focus();
			return false;
		}else{
		    $("#buscar").removeClass("has-warning");
			$("#buscar").find("label").text("Codigo empleado");
		}

		return true;
	}

    function inicioEnvio()
    {
        var x = $("#Rbusqueda");
        x.html('Buscando...');
    }

    function buscarEmpleado()
    {
      $("#Rbusqueda").load('Datos/BuscarEmpleado.php',data);
    }
            
    function problemas()
    {
        $("#Rbusqueda").text('Problemas en el servidor.');
    }
             
    function insertarEmpleadoGrupo()
    {
        //$("#contenedor2").load('Datos/insertarEmpleadoXgrupo.php',data);
        $('body').removeClass('modal-open');
        $("#contenedor").load('pages/recursos_humanos/gestion_Grupos_comite.php',data);
    }
            
    function inicioEnvioXgrupo()
    {
        var x = $("#contenedor");
        x.html('Buscando...');
    }

    function problemasXgrupo()
    {
        $("#contenedor").text('Problemas en el servidor.');
    }
                  
    function inicioVer()
    {
        var x = $("#contenedor2");
        x.html('Buscando...');
    }
            
    function verGrupo()
    {
       $("#contenedor2").load('Datos/cargarEmpleadoXGrupo.php',data2);
    }
            
          
    function problemasVer()
    {
        $("#contenedor2").text('Problemas en el servidor.');
    }            

</script>
        

   
   
<!--<script type="text/javascript" charset="utf-8">
          
$(document).ready(function() {
  
    $('#tabla_Grupos').dataTable({
            dom: 'Blfrtip',
        buttons: [

            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                },
                download: 'open'
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: ':visible'
                },
                download: 'open'
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'
        ]
         }); // example es el id de la tabla
    });

</script>-->
       
<script type="text/javascript" charset="utf-8">

    $(document).ready(function()
    {
        $('#tabla_Grupos').dataTable(); // example es el id de la tabla
    });

</script>

</head>

<body>

    <div class="row">

        <h3 class="page-header panel-primary"><center><span class="fa fa-list-alt" aria-hidden="true"></span><strong>  Gestión de Grupos y Comites  </strong></center></h3>

        <div class="col-lg-12">
            
                <?php
                 
                    if(isset($codMensaje) and isset($mensaje)){
                        if($codMensaje == 1){
                            echo '<div class="alert alert-success">';
                            echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
                            echo '<strong>Exito! </strong>';
                            echo $mensaje;
                            echo '</div>';
                        }else{
                            echo '<div class="alert alert-danger">';
                            echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
                            echo '<strong>Error! </strong>';
                            echo $mensaje;
                            echo '</div>';
                        }
                    }

                ?>
            
        </div>
        <!-- /.col-lg-12 -->
    </div>
        <!-- /.row -->   
    <div class="box">
        <div class="box-header">

        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
            <?php
                echo <<<HTML
                    <table id="tabla_Grupos" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="display:none"><strong>Id Grupo</strong></th>
                                <th style="text-align:center;background-color:#386D95;color:white;"><strong>Nombre del Grupo</strong></th>
                                <th style="text-align:center;background-color:#386D95;color:white;"><strong>Cantidad Integrantes</strong></th>
                                <th style="text-align:center;background-color:#386D95;color:white;"><strong>Agregar Miembro</strong></th>
                                <th style="text-align:center;background-color:#386D95;color:white;"><strong>Ver Miembros Actuales</strong></th>
                                       
                            </tr>
                        </thead>
                        <tbody>
HTML;

                while ($row = mysql_fetch_array($rec))  {
                    $idG = $row['ID_Grupo_o_comite']   ;
                    $nombreG = utf8_encode($row['Nombre_Grupo_o_comite']);
                    $cantidad = $row['Cantidad'];

                echo "<tr data-id='".$idG."'>";
                echo <<<HTML
                    <td name="id" style="display:none">$idG</td>

HTML;
                echo <<<HTML
                    <td name="nombreGrupo" >$nombreG</td>

HTML;
                //echo <<<HTML <td><a href='javascript:ajax_("'$url'");'>$NroFolio</a></td>HTML;
                echo <<<HTML
                    <td>$cantidad</td>

HTML;
                echo <<<HTML
                    <td><center> 
                        <button name="agregar"  class="agregarbg btn btn-primary glyphicon glyphicon-plus" data-toggle="modal" data-target="#compose-modal" > </button>
                    </center></td> 
                    
                    <td><center>
                    <button class="verbg btn btn-success glyphicon glyphicon-folder-open"  title="ver"></button>
                    </center></td>         
                                    
HTML;
                echo "</tr>";

            }
                echo <<<HTML
                            </tbody>
                        </table>
HTML;
         
            ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

       
    <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form role="form" id="form" name="form" action="#">
                    <div class="modal-header" style = "background-color:#0FA6C3">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" style = "color:white"><i class="glyphicon glyphicon-floppy-disk" style = "color:white"></i> Agregar un nuevo integrante al grupo </h4>
                    </div>
                    <div class="modal-body">
                        <div id="buscar" class="form-group">
                            
                            <div class="input-group">
                                <span class="input-group-addon">Buscar por número de empleado</span>

                                    <div  class="input-group">              
                                        <input id="codEmple" type="text"   class="form-control" placeholder="Buscar empleado..."  required>
                                        <span class="input-group-btn">
                                          <button id="buscarE" class="btn btn-primary glyphicon glyphicon-search" type="button"></button>
                                        </span>
                                    </div>
                            </div>   
                        </div>
                        <div id="Rbusqueda" class="form-group">

                        </div>
                    </div>
                    <div class="modal-footer clearfix">
                        <button name="submit" id="submit" class="insertarbg btn btn-primary pull-left"><i class="glyphicon glyphicon-pencil"></i> Insertar</button>
                    </div>        
                </form>
        
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    
    <div id="contenedor2" class="panel-body">
     
    </div>

</body>

</html>



