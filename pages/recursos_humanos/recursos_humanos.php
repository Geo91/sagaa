<?php
   //include '../../Datos/conexion.php';
include '../../conexion/config.inc.php';
$maindir = "../../";

if(isset($_GET['contenido']))
{
  $contenido = $_GET['contenido'];
}
else
{
  $contenido = 'recursos_humanos';
}

require_once($maindir."funciones/check_session.php");

require_once($maindir."funciones/timeout.php");

require_once($maindir."pages/navbar.php");


$rol = $_SESSION['user_rol']; 

?>

<html lang="es">
<head>
    
    
    
    <!--<meta charset="utf-8">-->
    
</head>

<body>
	
    <div class="container-fluid">
       	<div class="row">
           	<div class="col-sm-2">

            	<ul class="list-unstyled">
                 	<?php
                 		$url = 'pages/recursos_humanos/recursos_humanos.php?contenido=recursos_humanos';
                 		echo <<<HTML
                 			<h5><li>
			                	<a id="inicio" href="javascript:ajax_('$url');"><i class="fa fa-home"></i> Inicio de Recursos Humanos</a>
			            	</li></h5><br>
HTML;
					?>

                 	<li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#userMenu">
                  		<h5><i class="fa fa-male fa-fw"></i> Gestión de Perfiles <i class="glyphicon glyphicon-chevron-down"></i></h5></a>
		            	<ul class="list-unstyled collapse in" id="userMenu">

		            	<?php
		            		if($rol >= 30){
		            			echo <<<HTML
					            	<li>
					                	<a id="personas" href="#"><i class="fa fa-user"></i> Perfiles</a>
					            	</li>
HTML;
		            		}
		            	?>                

			            <li>
			                <a id="Busqueda"  href="#"><i class="glyphicon glyphicon-search"></i> Búsqueda</a>
			            </li>

		        		</ul>
    				</li>
    
	    			<li class="nav-header"  <?php if($rol <= 29) echo 'style="display: none;"';?>  > <a href="#" data-toggle="collapse" data-target="#userMenu2">
	      				<h5><i class="fa fa-user fa-fw"></i> Gestión de Empleados <i class="glyphicon glyphicon-chevron-down"></i></h5></a>
	  					<ul class="list-unstyled collapse in" id="userMenu2">
	    					<li>
	    						<a id="empleado" href="#"><i class="fa fa-suitcase"></i> Empleados </a>
	    					</li>

	    					<?php
			            		if($rol == 100 || $rol == 50){
			            			echo <<<HTML
	    								<li>
	    									<a id="gestionGC" href="#"><i class="fa fa-users "></i> Gestión de Grupos</a>
	    								</li>
HTML;
								}
							?>

						</ul>
					</li>

                    <li class="nav-header" <?php if($rol < 30){ echo 'style="display: none;"'; } ?> > <a href="#" data-toggle="collapse" data-target="#userMenu">
                        <h5><i class="fa fa-file-archive-o fa-fw"></i> Reportes <i class="glyphicon glyphicon-chevron-down"></i></h5></a>
                        <ul class="list-unstyled collapse in" id="userMenu">
                            <li>
                                <a id="reporteED" href="#"> Empleados por Departamento</a>
                            </li>                        
                        </ul>
                    </li>

					<li class="nav-header" <?php if($rol < 61) echo 'style="display: none;"';?> > <a href="#" data-toggle="collapse" data-target="#menu4">
						<h5><i class="fa fa-cogs fa-fw"></i> Mantenimiento <i class="glyphicon glyphicon-chevron-right"></i></h5></a>

						<ul class="list-unstyled collapse" id="menu4">
						    <li><a id="idiomass" href="#">Idiomas</a></li>
						    <li><a id="clases" href="#">Clases</a></li>
						    <li><a id="cargos" href="#">Cargos</a></li>
						    <li><a id="universidad" href="#">Universidades</a></li>
						    <li><a  id="tipos" href="#">Tipo Estudio</a></li>
						    <li><a id="departamentos" href="#">Departamentos</a></li>
					    	<li><a id="comite" href="#">Grupo o Comité</a></li>
						    <li><a id="pais" href="#">Paises</a></li>
						    <li><a id="titulo" href="#">Título</a></li>
						</ul>
					</li>

				</ul>
			<hr>
			</div>



			<div class="col-sm-9">
	 			<div class="row mt">
	  				<div class="col-md-12">
	      				<div id="contenedor" class="content-panel">

	      					<h3 class="page-header panel-primary"><center><strong>  Recursos Humanos  </strong></center></h3>

	        				<div id="wrapper">
	         
						        <?php

									$query = mysql_query("select count(*)as activos from empleado where estado_empleado=1");
									$row = mysql_fetch_array($query);
									$TotalEmpleadoActivo = $row["activos"];

									$query = mysql_query("select count(*) as inactivos from empleado where estado_empleado!=1");
									$row = mysql_fetch_array($query);
									$TotalEmpleadoInactivo = $row["inactivos"];

									$query = mysql_query("select count(*) as Aspirante from persona where N_identidad not in (Select N_identidad from empleado) and N_identidad not in (Select sa_estudiantes.dni from sa_estudiantes)");
									$row = mysql_fetch_array($query);
									$Asparitante = $row["Aspirante"];
						        
						        ?>
	         
								<input id="Total" type="hidden" value="<?php echo $Total; ?>" >
								<input id="TotalEmpleadoActivo" type="hidden" value="<?php echo $TotalEmpleadoActivo; ?>" >
								<input id="TotalEmpleadoInactivo" type="hidden" value="<?php echo $TotalEmpleadoInactivo; ?>" >
								<input id="Asparitante" type="hidden" value="<?php echo $Asparitante; ?>" >
	         
								<!-- /.row -->
								<div class="row">

						            <div class="col-lg-12">
						                <div class="panel panel-primary">
						                    <div class="panel-heading">
						                        <strong>Empleados por Departamento</strong>
						                    </div>
						                    <div class="panel-body">
						                        <div id="graficaPerfiles"></div>
						                    </div>
						                </div>
						            </div>				            
						        </div>
                                <div class = "row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <strong>Conteo de Empleados - Ex Empleados - Aspirantes</strong>
                                            </div>
                                            <div class="panel-body">
                                                <div id="morris-donut-chart2"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
	    					</div>
						</div>
					</div>
				</div>
			</div>    
		</div>
	</div>

<script>

            /* 
             * To change this license header, choose License Headers in Project Properties.
             * To change this template file, choose Tools | Templates
             * and open the template in the editor.
             */

             var x;
             x = $(document);
             x.ready(menuInicioEmpleado);
             function menuInicioEmpleado()
             {
                var x;
                x = $("#clases");
                x.click(clases);
                
                var x;
                x = $("#cargos");
                x.click(cargos);
                
                var x;
                x = $("#pais");
                x.click(paises);
                
                var x;
                x=$("#departamentos");
                x.click(departamentos);
                
                var x;
                x = $("#idiomas");
                x.click(idiomas);
                
                var x;
                x = $("#tipos");
                x.click(tipo);

                var x;
                x = $("#comite");
                x.click(comite);
                
                var x;
                x = $("#idiomass");
                x.click(idioma);
                
                var x;
                x = $("#empleado");
                x.click(empleados);
                
                var x;
                x = $("#universidad");
                x.click(universidades);
                
                var x;
                x = $("#personas");
                x.click(personas);
                
                var x;
                x = $("#gestionGC");
                x.click(gestiondeGrupos);
                
                
                var x;
                x = $("#titulo");
                x.click(Titulo);
                
                var x;
                x = $("#Busqueda");
                x.click(busqueda);

                var x;
                x = $("#reporteED");
                x.click(reporteEmpDepto);
                
            }

            function idiomas()
            {
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadaidioma,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            }

            function idioma()
            {
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadaIdiomas,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            }

            function comite()
            {
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadacomite,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            }

            function clases()
            {
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadaClases,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            }
            
            function cargos()
            {
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadaCargos,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            }
            function departamentos()
            {
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadaDepartamentos,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            }
            
            function tipo()
            {
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadaTipos,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            }
            
            
            function empleados()
            {
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadaempleado,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            }
            
            function paises(){
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadapais,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            }
            
            function persona(){
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadapersona,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            }
            
            function universidades(){
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadauniversidad,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            }
            
            function personas(){
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    url:"pages/recursos_humanos/cv/menuPersona.php",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadaPersonas,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            }
            
            
            function gestiondeGrupos(){
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadagestiondeGrupos,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            }
            
            function Titulo(){
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadaTitulo,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            }
            
            function busqueda(){
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadabusqueda,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            }

            function reporteEmpDepto(){
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadaReporteED,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            } 


            function inicioEnvio(){
                var x = $("#contenedor");
                x.html('Cargando...');
            }

            function llegadaClases(){
                $("#contenedor").load('pages/recursos_humanos/mantenimiento/clases/clases.php');
                //$("#contenedor").load('pages/recursos_humanos/Clases.php');
                //$("#contenedor").load('../cargarPOAs.php');
            }

            function llegadaIdiomas(){
                //$("#contenedor").load('pages/recursos_humanos/Idiomas.php');
                $("#contenedor").load('pages/recursos_humanos/mantenimiento/idiomas/idiomas.php');
                //$("#contenedor").load('../cargarPOAs.php');
            }

            function llegadacomite(){
                $("#contenedor").load('pages/recursos_humanos/mantenimiento/grupos/grupos.php');
                //$("#contenedor").load('pages/recursos_humanos/Comite_Grupo.php');
                //$("#contenedor").load('../cargarPOAs.php');
            }

            function llegadaCargos(){
                $("#contenedor").load('pages/recursos_humanos/mantenimiento/cargos/cargos.php');
                //$("#contenedor").load('pages/recursos_humanos/Cargos.php');
                //$("#contenedor").load('../cargarPOAs.php');
            }

            function llegadaDepartamentos(){
                //$("#contenedor").load('pages/recursos_humanos/Departamentos.php');
                $("#contenedor").load('pages/recursos_humanos/mantenimiento/departamentos/departamentos.php');
                //$("#contenedor").load('../cargarPOAs.php');
            }
            
            function llegadaTipos(){
                //$("#contenedor").load('pages/recursos_humanos/Tipo_Estudio.php');
                $("#contenedor").load('pages/recursos_humanos/mantenimiento/tipoEstudio/tiposEstudio.php');
                //$("#contenedor").load('../cargarPOAs.php');
            }
            
            function llegadapais(){
                //$("#contenedor").load('pages/recursos_humanos/Pais.php');
                $("#contenedor").load('pages/recursos_humanos/mantenimiento/paises/paises.php');
                //$("#contenedor").load('../cargarPOAs.php');
            }

            function llegadaempleado(){
                $("#contenedor").load('pages/recursos_humanos/Empleados.php');
                //$("#contenedor").load('../cargarPOAs.php');
            }

            function llegadauniversidad(){
                $("#contenedor").load('pages/recursos_humanos/mantenimiento/universidades/universidades.php');
                //$("#contenedor").load('../cargarPOAs.php');
            }

            function llegadaPersonas()
            {
                $("#contenedor").load('pages/recursos_humanos/cv/menuPersona.php');
                
            }

            function llegadagestiondeGrupos(){
                $("#contenedor").load('pages/recursos_humanos/gestion_Grupos_comite.php');
            }
            
            function llegadaTitulo(){
                $("#contenedor").load('pages/recursos_humanos/mantenimiento/titulos/titulos.php');
                //$("#contenedor").load('pages/recursos_humanos/Titulo.php');
            }
            
            function llegadabusqueda(){
                $("#contenedor").load('pages/recursos_humanos/cv/MenuBusqueda.php');
            }

            function llegadaReporteED(){
                $("#contenedor").load('pages/recursos_humanos/reporte/EmpleadosDepartamento.php');
            }

            function problemas(){
                $("#contenedor").text('Problemas en el servidor.');
            }
</script>


        
<script>

    vari= $("#realizadasDelMes").val();
    //Total=$("#Total").val();
    TotalEmpleadoActivo=$("#TotalEmpleadoActivo").val();
    TotalEmpleadoInactivo=$("#TotalEmpleadoInactivo").val();
    Asparitante=$("#Asparitante").val();
    <?php 
        $query = ("SELECT COUNT(persona.N_identidad) as conteo, departamento_laboral.nombre_departamento from persona, empleado, departamento_laboral where persona.N_identidad=empleado.N_identidad and empleado.Id_departamento=departamento_laboral.Id_departamento_laboral group by departamento_laboral.nombre_departamento");
        $result = mysql_query($query);
        $numItems = mysql_num_rows($result);
        
    ?>

    Highcharts.chart('graficaPerfiles',{
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: true,
                type: 'column'
            },
            title: {
                text: 'Empleados por Departamento'
            },
            tooltip: {
                pointFormat: 'No. de Empleados: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            xAxis: {
                categories: ['Departamentos'],
            },
            yAxis: {
                allowDecimals:false
            },
            series: [
                <?php while($fila = mysql_fetch_array($result)){ ?>
                    {
                        name: "<?php echo utf8_encode($fila['nombre_departamento']); ?>",
                        data: [{name: "<?php echo utf8_encode($fila['nombre_departamento']); ?>", y: <?php echo $fila['conteo']; ?> }]
                    },
                <?php 
                    }
                ?>
            ]
        });
            
    $(function() {
               
       	Morris.Donut({
            element: 'morris-donut-chart2',
       		// data:'estadisticasActDelMes.php',
        data:[{
                label: "Empleados",
                value: TotalEmpleadoActivo,
                color:"#1D3969"
            }, {
                label: "Ex-Empleados",
                value: TotalEmpleadoInactivo,
                color: "#9C0F06"
            }, {
                label: "Aspirantes",
                value: Asparitante,
                color:"orange"
            }],
    		resize: true
		});

        
               
   	});

</script>
          
</body>
   
</html>