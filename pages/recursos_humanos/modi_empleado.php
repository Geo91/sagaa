<?php

    include '../../conexion/config.inc.php';

    $existe = FALSE;
    $maindir = "../../";

    if(isset($_GET['contenido'])){
        $contenido = $_GET['contenido'];
    }else{
        $contenido = 'recursos_humanos';
    }

    require_once($maindir."funciones/check_session.php");

    require_once($maindir."funciones/timeout.php");

    if(isset($_POST["tipoProcedimiento"])){
        $tipoProcedimiento = $_POST["tipoProcedimiento"];
        if($tipoProcedimiento == "insertarCargo"){
            require_once("../../Datos/insertarCargoXEmpleado.php");
        }
        if($tipoProcedimiento == "AltaCargo"){
            require_once("../../Datos/actualizarCargoXEmpleado.php");
        }
        if($tipoProcedimiento == "Actualizar"){
            require_once("../../Datos/actualizarEmpleado.php");
        }
    }

    if(isset($_POST['codigo']) and $existe==FALSE ){
 
        $codigoE = $_POST['codigo'];

        $resultado=mysql_query("SELECT * FROM empleado inner join persona on empleado.N_identidad=persona.N_identidad inner join departamento_laboral on departamento_laboral.Id_departamento_laboral=empleado.Id_departamento Where estado_empleado='1' and No_Empleado='".$codigoE."'");

        $resultado2=mysql_query("SELECT * FROM empleado_has_cargo inner join cargo on cargo.ID_cargo=empleado_has_cargo.ID_cargo where No_Empleado='".$codigoE."'");

        if($row=mysql_fetch_array($resultado)){
            $nombreE=utf8_encode($row['Primer_nombre']);
            $apellidoE=utf8_encode($row['Primer_apellido']);
            $depE=utf8_encode($row['nombre_departamento']);
            $depID=$row['Id_departamento_laboral'];
            $obsE=utf8_encode($row['Observacion']);
            $NumEmp = $row['jefe_inmediato'];
            $noE=$row['N_identidad'];
            $fechaE=$row['Fecha_ingreso'];
            $dependencia = $row['id_dependencia'];
            $category = $row['id_categoria'];    
        }
    }else if(isset($_POST['codigo2']) and $existe==TRUE){   
        $codigoE = $_POST['codigo2'];
     
        $resultado=mysql_query("SELECT * FROM empleado inner join persona on empleado.N_identidad=persona.N_identidad inner join departamento_laboral on departamento_laboral.Id_departamento_laboral=empleado.Id_departamento Where estado_empleado='1' and No_Empleado='".$codigoE."'");

        $resultado2=mysql_query("SELECT * FROM empleado_has_cargo inner join cargo on cargo.ID_cargo=empleado_has_cargo.ID_cargo where No_Empleado='".$codigoE."'");

        if($row=mysql_fetch_array($resultado)){
            $nombreE=utf8_encode($row['Primer_nombre']);
            $apellidoE=utf8_encode($row['Primer_apellido']);
            $depE=utf8_encode($row['nombre_departamento']);
            $depID=$row['Id_departamento_laboral'];
            $obsE=utf8_encode($row['Observacion']);
            $NumEmp = $row['jefe_inmediato'];
            $noE=$row['N_identidad'];
            $fechaE=$row['Fecha_ingreso'];
            $dependencia = $row['id_dependencia'];
            $category = $row['id_categoria'];   
        }
    }

?>

<!DOCTYPE html>
<html lang="utf-8">

<head>

    <meta charset="utf-8">

        <script>

            /* 
             * To change this license header, choose License Headers in Project Properties.
             * To change this template file, choose Tools | Templates
             * and open the template in the editor.
             */
         
    var x;
    x = $(document);
    x.ready(inicio);
            
    function inicio(){             
        var x;
        x = $(".insertarbc");
        x.click(nuevoCargo);
        
        var x;
        x = $(".altab");
        x.click(darAlta);   
    }

    function validarControles(){     
        var x = document.getElementById('jefe').checked;                   
        if(x){  
            return '1';
        }else{
            
            return '0';
        }
    }
  
    function nuevoCargo(){
        $("#compose-modal").modal('hide');
        data2={
            codigo:$('#cod').val(),
            cargoE:$('#cargo').val(),
            fechaIngreso:$('#fechaI').val(),
            tipoProcedimiento:"insertarCargo"
          
        };
        
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: ActualizarNuevoCargo,
            timeout: 4000,
            error: problemas
        });
        return false;
    }
            
    function darAlta(){
        id1 = $(this).parents("tr").find("td").eq(0).html();
        var codigoE2 = "<?php echo $codigoE; ?>" ;
         
        var respuesta = confirm("Â¿Esta seguro de que desea DAR DE ALTA al cargo actual de este empleado?");
        if (respuesta) {
            data2={
                codigo:codigoE2,
                cargoE:id1,
                tipoProcedimiento:"AltaCargo"
            };
            
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                beforeSend: inicioEnvio,
                success:Actualizarcargo,
                timeout: 4000,
                error: problemas
            });
            return false;
        }
    }

    $(document).ready(function() {

        $("form").submit(function(e) {
            e.preventDefault();

            if(validator()){
                var id = "<?php echo $noE; ?>" ;
                var codT = "<?php echo $codigoE; ?>" ;
                if(validarControles()==='1')
                    data={
                        codigo:$('#cod').val(),
                        codigo2:codT,
                        nidentidadE:id,
                        departE:$('#dep').val(),
                        fechaE:$('#fecha').val(),
                        depend:$('#dependencia').val(),
                        cat:$('#cat').val(),
                        inmediato:$('#jInmediato').val(),
                        obsE:$('#obs').val(),
                        jefe:'1',
                        tipoProcedimiento:"Actualizar"
                    };
                else{
                    data={
                        codigo:$('#cod').val(),
                        codigo2:codT,
                        nidentidadE:id,
                        departE:$('#dep').val(),
                        fechaE:$('#fecha').val(),
                        depend:$('#dependencia').val(),
                        cat:$('#cat').val(),
                        inmediato:$('#jInmediato').val(),
                        obsE:$('#obs').val(),
                        jefe:'0',
                        tipoProcedimiento:"Actualizar"
                    };
                }

                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: ActualizarEmple,
                    timeout: 4000,
                    error: problemas
                });
            }
          return false;
        });

    });
        
    function soloLetrasYNumeros(text){
        var letters = /^[0-9]+$/; 
        if(text.match(letters)){
            return true;
        }else{
            return false;
        }
    }

    function validator(){ //valida si se han itroduzido otros digitos aparte de numeros y letras
        var nombre = $("#cod").val();
        
        if(soloLetrasYNumeros(nombre) == false){
            $("#CodE").addClass("has-warning");
            $("#CodE").find("label").text("Codigo de empleado: Solo son permitidos numeros");
            $("#cod").focus();
            return false;
        }else{
            $("#CodE").removeClass("has-warning");
            $("#CodE").find("label").text("Codigo empleado");
        }
        return true;
    }

    function inicioEnvio(){
        var x = $("#contenedor");
        x.html('Cargando...');
    }
            
    function ActualizarNuevoCargo(){
        $('body').removeClass('modal-open');
        $("#contenedor").load('pages/recursos_humanos/modi_empleado.php',data2);
    }

    function ActualizarEmple(){
        $("#contenedor").load('pages/recursos_humanos/modi_empleado.php',data);
    }
            
    function Actualizarcargo(){
        $("#contenedor").load('pages/recursos_humanos/modi_empleado.php',data2);
    }
          
    function problemas(){
        $("#contenedor").text('Problemas en el servidor.');
    }
            
    $("[name='my-checkbox']").bootstrapSwitch();
        $('[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {
        if (state == true) {
            estado = 1;
        }else{
            estado = 0;
        }
        return estado;
    });

    function marcarCheckbox(){
        for (i=0;i<document.form.elements.length;i++) 
            if(document.form.elements[i].type == "checkbox")    
                document.form.elements[i].checked=1
    }

</script>

</head>

<body>
        
    <div id="container" class="panel-primary">
      <h3 class="page-header panel-primary"><center><span class="fa fa-file-text-o fa-1.5x" aria-hidden="true"><strong>  Información Laboral  </strong></span></center></h3>
    </div>
    
    <?php
        if(isset($codMensaje) and isset($mensaje)){
            if($codMensaje == 1){
                  echo '<div class="alert alert-success">';
                  echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
                  echo '<strong>Exito! </strong>';
                  echo $mensaje;
                  echo '</div>';
            }else{
                  echo '<div class="alert alert-danger">';
                  echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
                  echo '<strong>Error! </strong>';
                  echo $mensaje;
                  echo '</div>';
            }
        }
    ?>
    
    <div class="row">  
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <strong>Información Personal</strong>                    
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <strong> Número de identidad :</strong>  <?php echo $noE; ?> 
                            </br>
                            <strong> Nombre :</strong> <?php echo $nombreE." ".$apellidoE ; ?> 
                            </br>
                        </div>
                        <div class="col-xs-12">
                         
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <label><strong> Cargos </strong></label> <a class="btn btn-warning" data-toggle="modal" data-target="#compose-modal"><i class="glyphicon glyphicon-plus"></i></a>
                    </h4>
                </div>
                <br>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" id="tabla_prioridad">
                            <thead>
                                <tr>
                                    <th style="text-align:center;background-color:#386D95;color:white;"><strong>ID cargo</strong></th>
                                    <th style="text-align:center;background-color:#386D95;color:white;"><strong>Cargo</strong></th>
                                    <th style="text-align:center;background-color:#386D95;color:white;"><strong>Fecha de ingreso</strong></th>
                                    <th style="text-align:center;background-color:#386D95;color:white;"><strong>Fecha de salida</strong></th>
                                    <th style="text-align:center;background-color:#386D95;color:white;"><strong>Dar de alta</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                  
                    <?php
                        while ($row3=mysql_fetch_array($resultado2)) {
                            $id = $row3['ID_cargo'];
                            $fechaS=$row3['Fecha_salida_cargo'];
                    ?>
                                <tr>
                                    <td id="idcargo"><?php echo $row3['ID_cargo'] ?></td>
                                    <td id="cargoP"><?php echo utf8_encode($row3['Cargo']) ?></td>
                                    <td id="fechaic"><center> <?php echo $row3['Fecha_ingreso_cargo']?> </center></td>
                        <?php
                            if ($fechaS== NULL || $fechaS=="0000-00-00") {
                               
                                echo <<<HTML

                                    <td><center> Actualmente </center></td>  
                                    <td><center><button type="submit" class="altab btn btn-warning glyphicon glyphicon-send"  title="Dar de alta"> </button></center></td>                         
HTML;
                        }else{
                          
                            echo <<<HTML
                          
                                    <td> $fechaS </td>
                                    <td><center><button type="submit" class="altab btn btn-warning glyphicon glyphicon-send"  title="Dar de alta" disabled="TRUE"> </button></center></td> 
HTML;
                                     
                    }
                    ?>
                                </tr>
                    <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <strong>Llene los campos a continuación solicitados</strong>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12"> 

                            <form role="form" id="form" name="form" action="#">
                                
                                <div id="CodE" class="form-group">
                                    <div class="form-input">
                                        <label  class="col-sm-5 control-label" >Número de Empleado :</label>
                                          
                                        <div class="col-sm-5">
                                            <input class="form-control" name="cod_empleado" id="cod" value="<?php echo$codigoE ;?>"  disabled="true"> 
                                        </div>
                                    </div>
                                    <br>
                                </div>
                           
                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Fecha de ingreso como empleado :</label>
                                      
                                    <div class="col-sm-5">
                                        <input type="date"  class="form-control" name="Fecha" id="fecha" value="<?Php echo $fechaE ?>" required/> <!-- agregue el atrributo name que mediante este vas a poder acceder al valor de la etiqueta -->
                                    </div>
                                    <br>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label"><h5><strong>Departamento laboral :</strong></h5></label>
                                    <div class="col-sm-5">
                                        <select name="depar" class="form-control" id="dep">

                                        <?php
                                            $consulta_mysql = "SELECT * FROM `departamento_laboral`";
                                            $resultado3 = mysql_query($consulta_mysql);

                                            while ($row = mysql_fetch_array($resultado3)) {

                                                $id = $row['Id_departamento_laboral'];

                                                if ($id == $depID) {
                                                    echo "<option selected value = '" . $row['Id_departamento_laboral'] . "'>";
                                                } else {
                                                    echo "<option value = '" . $row['Id_departamento_laboral'] . "'>";
                                                }

                                                echo utf8_encode($row["nombre_departamento"]);

                                                echo "</option>";
                                            }
                                        ?>
                                        </select>
                                    </div>
                                    <br>
                                </div>

                                <br>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label"><h5><strong>Dependencia :</strong></h5></label>
                                    <div class="col-sm-5">
                                        <select name="depend" class="form-control" id="dependencia">
                                        <?php
                                            $consulta_mysql = "SELECT * FROM `dependencia`";
                                            $resultado4 = mysql_query($consulta_mysql);

                                            while ($fila = mysql_fetch_array($resultado4)) {

                                                $codigo = $fila['dependencia_id'];

                                                if ($codigo == $dependencia) {
                                                    echo "<option selected value = '" . $codigo . "'>";
                                                } else {
                                                    echo "<option value = '" . $codigo . "'>";
                                                }

                                                echo utf8_encode($fila["descripcion"]);

                                                echo "</option>";
                                            }
                                        ?>                                       
                                        </select>
                                    </div>
                                    <br>
                                </div>

                                <br>

                                <div class="form-group">
                                    <label class="col-lg-5 control-label"><h5><strong>Categoría :</strong></h5></label>
                                    <div class="col-lg-5">
                                        <select name="cat" class="form-control" id="cat">
                                        <?php
                                            $consulta_mysql = "SELECT * FROM `categoria`";
                                            $resultado5 = mysql_query($consulta_mysql);

                                            while ($fila = mysql_fetch_array($resultado5)) {

                                                $codigo = $fila['categoria_id'];

                                                if ($codigo == $category) {
                                                    echo "<option selected value = '" . $codigo . "'>";
                                                } else {
                                                    echo "<option value = '" . $codigo . "'>";
                                                }

                                                echo utf8_encode($fila["descripcion_cat"]);

                                                echo "</option>";
                                            }
                                        ?>                                       
                                        </select>
                                    </div>
                                    <br>
                                </div>

                                <br>

                                <div class="form-group">
                                    <label class="col-lg-5 control-label"><h5><strong>Jefe Inmediato</strong></h5></label>
                                    <div class="col-lg-5">
                                        <select name="jefeInmediato" class="form-control" id="jInmediato">

                                        <?php
                                            $consulta_mysql = "SELECT empleado.No_Empleado, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) as 'Jefe' from persona inner join empleado on persona.N_identidad=empleado.N_identidad where empleado.es_jefe = '1' and empleado.estado_empleado='1'";
                                            $result = mysql_query($consulta_mysql);

                                            while ($fila2 = mysql_fetch_array($result)) {

                                                $No = $fila2['No_Empleado'];

                                                if ($No == $NumEmp) {
                                                    echo "<option selected value = '" . $fila2['No_Empleado'] . "'>";
                                                } else {
                                                    echo "<option value = '" . $fila2['No_Empleado'] . "'>";
                                                }

                                                echo utf8_encode($fila2["Jefe"]);

                                                echo "</option>";
                                            }
                                        ?>

                                        </select>
                                    </div>
                                </div>

                                <br>
                                <hr>                                
                                   
                                <div class="col-lg-12">
                                    <label ><strong>Observación :</strong></label> 
                                    <div class="form-group">
                                        <textarea class="form-control" name="comentarios" rows="3"  id="obs" ><?php echo utf8_encode($obsE);  ?></textarea>
                                    </div>
                                    <div class="panel-body">
                                        <div class="panel-body">
                                            <label>ES JEFE</label>
                                            <input class = 'bootstrapSwitch bootstrap-switch-wrapper bootstrap-switch-id-switch-state bootstrap-switch-animate bootstrap-switch-on' data-id = '0' data-on-color = 'primary' data-off-color  = 'success' data-off-text = 'NO' data-on-text = 'SI' type='checkbox' name='my-checkbox' id='jefe'>  
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-offset-4 col-lg-12">                   
                                    <button id="ActualizarE" class="btn btn-primary">Guardar cambios</button>
                                    <button type="reset" class="btn btn-default">Resetear</button>
                                </div>
                               
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    
    <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form role="form" id="form" name="form" action="#">
                    <div class="modal-header" style = "background-color:#0FA6C3">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" style = "color:white"><i class="glyphicon glyphicon-floppy-disk"></i> Agregar un nuevo cargo a empleado </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Cargo</label>
                            <div class="col-lg-6" >
                                <select name='cargo' class="form-control" id="cargo">

                                    <?php
                                          $consulta_mysql = "SELECT * FROM `cargo`";
                                          $rec = mysql_query($consulta_mysql);

                                          while ($row = mysql_fetch_array($rec)) {
                                              echo "<option value = '" . $row['ID_cargo'] . "'>";

                                              echo utf8_encode($row["Cargo"]);

                                              echo "</option>";
                                          }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group">
                            <label class="col-lg-4 control-label"> Fecha de Ingreso </label>
                            <div class="col-sm-7"><input id="fechaI" type="date" name="fecha" autocomplete="off" class="input-xlarge"  required><br></div>
                        </div>
                        <br>

                        <div id="Rbusqueda" class="form-group">
                    <?php
                    ?>
                        </div>
                    </div>
                    <div class="modal-footer clearfix">
                        <button name="submit" id="submit" class="insertarbc btn btn-primary pull-left"> Guardar Información</button>
                    </div>    
                </form>
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
   </div>
   
</body>


<script>
    $('#tabla_prioridad').dataTable({
            dom: 'Blfrtip',
        buttons: [

            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                },
                download: 'open'
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: ':visible'
                },
                download: 'open'
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'
        ]
         });
</script>
