<?php
session_start();
include "../../../../Datos/conexion.php";
if (isset($_POST['id'])) {
    $titulo = utf8_encode($_POST['titulo']);
    $tipo = utf8_encode($_POST['tipo']);
    $universidad = utf8_encode($_POST['universidad']);
    $identidad=$_POST['identi'];
    echo <<<HTML
<label>Típo</label>
<select id="modTipo" name="tipo" class="form-control">
HTML;
    $pa = mysql_query("SELECT * FROM tipo_estudio");
    while ($row = mysql_fetch_array($pa)) {
        echo '<option value="' . $row['ID_Tipo_estudio'] .'" ';
        if($row['Tipo_estudio'] == $tipo){echo 'selected';}
        echo '>' . utf8_encode($row['Tipo_estudio']) . '</option>';
    }
echo <<<HTML
    </select>
</div></br>
<div class="form-group">
<label>Título</label>
<select id="modTitulo" name="titulo" class="form-control">
    <option value="NULL"> Seleccione una opción </option>
HTML;
        /*$pa=mysql_query("SELECT titulo FROM titulo");
        while($row=mysql_fetch_array($pa)){
            echo '<option value="' . $row['titulo'].'" ';
            if($row['titulo'] == $_POST['titulo']){echo 'selected';}
            echo '>' . $row['titulo'] . '</option>';
        }*/
echo <<<HTML
    </select>
</div>
<div class="form-group">
    <label>Universidad</label>
    <select id="modUniversidad" name="universidad" class="form-control">
HTML;
        $pa = mysql_query("SELECT * FROM universidad");
        while ($row = mysql_fetch_array($pa)) {
            echo '<option value="' . $row['Id_universidad'].'" ';
            if(utf8_encode($row['nombre_universidad']) == $universidad){echo 'selected';}
            echo '>' . utf8_encode($row['nombre_universidad']) . '</option>';
        }
echo <<<HTML
    </select></br>
HTML;
    echo '<button class="btn btn-primary" id="btActualizar">Guardar Información</button>';
    $_SESSION['id'] = $_POST['id'];
}
?>

<script>

    var x;
    x = $(document);
    x.ready(inicio);

    function inicio() {
        var x;
        x = $("#btActualizar");
        x.click(actFA);
    }


    function actFA() {
        var identi = "<?php echo $identidad; ?>" ;
        data = {
            modTipo: $('#modTipo').val(),
            modTitulo: $('#modTitulo').val(),
            modUniversidad: $('#modUniversidad').val(),
            identi:identi,
            tipoProcedimiento:"actualizarFA"
        };

        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: llegadaSelecPersona,
            timeout: 4000,
            error: problemas
        });
        return false;
    }

    function inicioEnvio() {
        var x = $("#cuerpoActFA");
        x.html('Cargando...');
    }

    function llegadaSelecPersona() {
         $('body').removeClass('modal-open');
        $("#contenedor").load('pages/recursos_humanos/cv/EditarCV.php',data);
    }

    function problemas() {
        $("#cuerpoActFA").text('Problemas en el servidor.');
    }


    $(document).on("change","#modTipo",function () {
        id = $("#modTipo").val();            
        cargarTitulos(id);
        return false;
    });

    function cargarTitulos(tipoEstudioID){
        var datos = 
            {
                id:tipoEstudioID
            };

        $.ajax({
            async: true,
            type: "POST",
            data: datos,
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            url: "pages/recursos_humanos/mantenimiento/titulos/obtenerTitulos.php",
            //beforeSend: inicioVer,
            success: function(response){
                                           
                var arr = JSON.parse(response);
                
                var options = '';
                var val='NULL';
                var def='Seleccione una opción';
                options += '<option value="' + val + '">' +
                                def+ '</option>';
                        
                for (var index = 0; index < arr.length; index++) 
                {
                    var idTitulo = arr[index].codTitulo;
                    var descTitulo = arr[index].nombreTitulo;
                    
                    options += '<option value="' + idTitulo + '">' +
                                descTitulo + '</option>';
                }
                
                $("#modTitulo").html(options);
            },
            timeout: 4000,
        });
    }

</script>
