<?php

$maindir = "../../../";
require_once('../../../Datos/conexion.php');
require_once($maindir."fpdf/fpdf.php");
require($maindir."conexion/config.inc.php");

$IDdepto = $_GET['id'];

//Datos generales e info de contacto
$consulta = "SELECT N_identidad, CONCAT(Primer_nombre, ' ', Segundo_nombre, ' ', Primer_apellido, ' ', Segundo_apellido) as NombreC, Fecha_nacimiento, Sexo, Direccion, Correo_electronico, Estado_Civil, Nacionalidad from persona where persona.N_identidad not in (SELECT sa_estudiantes.dni from sa_estudiantes) and persona.N_identidad in (SELECT empleado.N_identidad from empleado Where estado_empleado = '1' AND empleado.Id_departamento in (SELECT departamento_laboral.Id_departamento_laboral from departamento_laboral where departamento_laboral.Id_departamento_laboral = '".$IDdepto."'));";

$query = $db->prepare($consulta);
$query->execute();
$rows = $query->fetchAll();

class PDF extends FPDF{
    // Cabecera de página
    function Header(){

		$this->Image('../../../assets/img/lucen-aspicio.png', 50,30,200,200, 'PNG');
		//$this->Image('../../../assets/img/logo_unah.png' , 10,5,20,35, 'PNG');
		//$this->Image('../../../assets/img/logo-cienciasjuridicas.png' , 170,8, 35 , 35,'PNG');
		$this->Image('../../../assets/img/cabecera(2).png', 0,0,210,40, 'PNG');
		$this->Cell(18, 10, '', 0);
		$this->SetFont('Arial', '', 18);
		$this->Cell(5, 10, '', 0);
		//$this->Cell(140, 10,utf8_decode('Universidad Nacional Autónoma de Honduras'), 0,0,"C");
		$this->Ln(30);
		$this->SetFont('Arial', 'U', 14);
		$this->Cell(30, 8, '', 0,0,"C");
		$this->Cell(130, 8, ' Perfil de la Persona ', 0,0,"C");
		//$this->Rect(6, 45, 200, 240 ,'D');
		$this->SetFont('Arial', '', 12);
		$this->Ln(10);
	}
         
	function Footer(){
		$this->SetY(-15);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,utf8_decode('Página ').$this->PageNo().' de {nb}   - - - -   Generado el ' . date('d-m-y') . ' fecha del sistema',0,0,'C');
	}
}

$pdf = new PDF();
$pdf->SetFont('Arial', '', 12);

$pdf->AliasNbPages();


$Identidad = array();
$nombreCompleto = array();
$fechaNac = array();
$Genero = array();
$email = array();
$EstadoCiv = array();
$Nacional = array();

function array_ref(&$arreglo,$cadena){
    $contador = 0;
	$arreglo[$contador] = $cadena;
}

foreach ($rows as $row) {
	array_ref($Identidad,$row["N_identidad"]);
	array_ref($nombreCompleto,$row["NombreC"]);
	array_ref($fechaNac,$row["Fecha_nacimiento"]);
	array_ref($Genero,$row["Sexo"]);
	array_ref($Direcc,$row["Direccion"]);
	array_ref($email,$row["Correo_electronico"]);
	array_ref($EstadoCiv,$row["Estado_Civil"]);
	array_ref($Nacional,$row["Nacionalidad"]);
	$numeroMayor[1] = count($Identidad);
	$numeroMayor[2] = count($nombreCompleto);
	$numeroMayor[3] = count($fechaNac);
	$numeroMayor[4] = count($Genero);
	$numeroMayor[5] = count($Direcc);
	$numeroMayor[6] = count($email);
	$numeroMayor[7] = count($EstadoCiv);
	$numeroMayor[8] = count($Nacional);
	$n = max($numeroMayor);

	$contador = 0;
	while ($contador < $n) {
		$pdf->AddPage();

		//Experiencia Académica
		$resultado2 = mysql_query("SELECT experiencia_academica.ID_Experiencia_academica, Institucion, Tiempo,Clase FROM experiencia_academica inner join clases_has_experiencia_academica on clases_has_experiencia_academica.ID_Experiencia_academica=experiencia_academica.ID_Experiencia_academica inner join clases on clases.ID_Clases=clases_has_experiencia_academica.ID_Clases WHERE N_identidad='".$Identidad[$contador]."'");

		//Formación académica
		$resultado3 = mysql_query("SELECT estudios_academico.ID_Estudios_academico, titulo.titulo, tipo_estudio.Tipo_estudio, universidad.nombre_universidad FROM estudios_academico, titulo, tipo_estudio, universidad where titulo.id_titulo=estudios_academico.titulo_id AND tipo_estudio.ID_Tipo_estudio=estudios_academico.ID_Tipo_estudio AND universidad.Id_universidad=estudios_academico.Id_universidad AND estudios_academico.N_identidad = '".$Identidad[$contador]."'");

		//Experiencia laboral
		$resultado4 = mysql_query("SELECT experiencia_laboral.ID_Experiencia_laboral, Nombre_empresa, Tiempo, cargo FROM experiencia_laboral inner join experiencia_laboral_has_cargo on experiencia_laboral_has_cargo.ID_Experiencia_laboral=experiencia_laboral.ID_Experiencia_laboral inner join cargo on cargo.ID_cargo=experiencia_laboral_has_cargo.ID_cargo WHERE experiencia_laboral.N_identidad='".$Identidad[$contador]."'");

		//Telefonos
		$resultado5 = mysql_query("SELECT Tipo, Numero FROM telefono WHERE N_identidad= '".$Identidad[$contador]."'");

		if($Genero[$contador] == "M"){
			$gender = "Masculino";
		}else{
			$gender = "Femenino";
		}
		$pdf->Cell(190, 8, 'DATOS GENERALES', 0,0,"C");
		$pdf->Ln(8);

		$pdf->Cell(125, 10, utf8_decode('Número de Identidad: ').$Identidad[$contador], 0);
		$pdf->Ln(5);
		$pdf->Cell(120, 10, utf8_decode('Nombre Completo: '.$nombreCompleto[$contador]), 0);
		$pdf->Ln(5);
		$pdf->Cell(120, 10, utf8_decode('Género: ').$gender, 0);
		$pdf->Ln(5);
		$pdf->Cell(120, 10,utf8_decode('Nacionalidad: '.$Nacional[$contador]), 0);
		$pdf->Ln(5);
		$pdf->Cell(120, 10, 'Fecha de nacimiento: '.$fechaNac[$contador], 0);
		$pdf->Ln(5);
		$pdf->Cell(120, 10, 'Estado Civil: '.$EstadoCiv[$contador], 0);
		$pdf->Ln(15);
		$pdf->Cell(190, 9,utf8_decode('INFORMACIÓN DE CONTACTO'),0,0,'C');
		$pdf->Ln(5);
		$pdf->Cell(125, 8, utf8_decode('Dirección: '.$Direcc[$contador]), 0);
		$pdf->Ln(5);
		$pdf->Cell(125, 8, utf8_decode('Correo Electrónico: ').$email[$contador], 0);
		$pdf->Ln(5);
		$pdf->Cell(125, 8, utf8_decode('Números telefónicos: '), 0);
		$pdf->Ln(5);
		$pdf->Cell(30, 8, 'Tipo', 0);
		$pdf->Cell(40, 8, utf8_decode('Número'), 0);
		$pdf->Ln(5);

		while ($filas_tel = mysql_fetch_array($resultado5)){ //while para mostrar los números telefónicos
		    $pdf->Cell(30, 8, $filas_tel["Tipo"], 0);
		    $pdf->Cell(40, 8, $filas_tel["Numero"], 0);
		    $pdf->Ln(5);
		} //fin del while para mostrar núermos telefónicos

		$pdf->Ln(5);
		$pdf->Cell(190, 9,utf8_decode('FORMACIÓN ACADÉMICA'), 0,0,'C');
		$pdf->Ln(2);

		while ($filas_fa = mysql_fetch_array($resultado3)){ //while para mostrar la formación académica
            $id3 = $filas_fa['ID_Estudios_academico'];
            $titulo = $filas_fa['titulo'];
            $tipoEs = utf8_encode($filas_fa['Tipo_estudio']);
            $univ = $filas_fa['nombre_universidad'];
  
			$pdf->Ln(5);
			$pdf->Cell(125, 8, utf8_decode('Nombre del título: ').$titulo, 0);
			$pdf->Ln(5);
			$pdf->Cell(125, 8, 'Tipo de Estudio: '.$tipoEs, 0);
			$pdf->Ln(5);
			$pdf->Cell(125, 8, 'Universidad: '.$univ, 0);
			$pdf->Ln(5);
		} //fin del while para mostrar la formación académica

		$pdf->Ln(5);
		$pdf->Cell(190, 9, 'EXPERIENCIA LABORAL', 0,0,'C');
		$pdf->Ln(2);
		while ($filas_el = mysql_fetch_array($resultado4)){ //while para mostrar la experiencia laboral
			$id4 = $filas_el['ID_Experiencia_laboral'];
			$nomEmp = $filas_el['Nombre_empresa'];
			$tiempo = $filas_el['Tiempo'];
			$cargo  = utf8_encode($filas_el['cargo']);

			$pdf->Ln(5);
			$pdf->Cell(125, 8, utf8_decode('Nombre de la Empresa: '.$nomEmp), 0);
			$pdf->Ln(5);
			$pdf->Cell(125, 8, 'Tiempo (En meses): '.$tiempo, 0);
			$pdf->Ln(5);
			$pdf->Cell(125, 8, utf8_decode('Cargo ejercido: '.$cargo), 0);
			$pdf->Ln(5);
		}   //fin del while para mostrar la experiencia laboral

		$pdf->Ln(5);
		$pdf->Cell(190, 9, utf8_decode('EXPERIENCIA ACADÉMICA'), 0,0,"C"); 
		$pdf->Ln(2);
		while ($filas_ea = mysql_fetch_array($resultado2)) { //while para mostrar la experiencia académica
			$id2 = $filas_ea['ID_Experiencia_academica'];
			$inst = utf8_decode($filas_ea['Institucion']);
			$tiempo = $filas_ea['Tiempo'];
			$clase = utf8_encode($filas_ea['Clase']);

			$pdf->Ln(5);
			$pdf->Cell(125, 8, 'Nombre de la Empresa: '.$inst, 0);
			$pdf->Ln(5);
			$pdf->Cell(125, 8, 'Tiempo (En meses): '.$tiempo, 0);
			$pdf->Ln(5);
			$pdf->Cell(125, 8, 'Clases impartida: '.$clase, 0);
			$pdf->Ln(5);
		} //fin del while para mostrar la experiencia académica
		$contador=$contador+1;
	} //fin del while
} //fin del foreach 

$resultado6 = mysql_query("SELECT * from departamento_laboral WHERE Id_departamento_laboral = '".$IDdepto."';");
while($filas_dl = mysql_fetch_array($resultado6)){
	$codDepto = $filas_dl['Id_departamento_laboral'];
	$nomDepto = utf8_encode($filas_dl['nombre_departamento']);
}

$pdf->Output('Reporte Perfiles Departamento -'.$nomDepto.'.pdf','D');


?>