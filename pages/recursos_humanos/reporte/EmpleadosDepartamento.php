<?php

require_once('../../../Datos/conexion.php');

$maindir = "../../../";
session_start();
$rol = $_SESSION['user_rol'];

if(isset($_GET['contenido'])){
    $contenido = $_GET['contenido'];
}else{
    $contenido = 'recursos_humanos';
    $navbar_loc = 'contenido';
}

require_once($maindir."funciones/check_session.php");

require_once($maindir."funciones/timeout.php");
  
if(!isset( $_SESSION['user_id'] )){
    header('Location: '.$maindir.'login/logout.php?code=100');
    exit();
}
 
$rol = $_SESSION['user_rol'];

?>


<head>    

    <script>
 
        $(document).ready(function() { //inicio de preparación del documento

            $("#formFiltro").submit(function(e) { //función para realizar el filtrado de la tabla basado en el criterio establecido en el combobox
                e.preventDefault();

                data2={
                  depto: $('#filtro').val()
                };

                $.ajax({
                  async: true,
                  type: "POST",
                  dataType: "html",
                  contentType: "application/x-www-form-urlencoded",
                  beforeSend: filtrando,
                  success: filtradoRealizado,
                  timeout: 10000,
                  error: problemasbusqueda
                });
                return false;
            });

            $("#formPDFEmp").submit(function(e) { //llamado a la función que levanta el documento reporte de Empleados, basado en el criterio establecido en el combobox
                e.preventDefault();

                var condicion = $('#filtro').val();

                if (condicion > 0){
                    exportarPDFEmp(condicion);
                }else{
                    alert("Debe establecer un filtro para poder generar el reporte")
                }
                
            });

            $("#formPDFP").submit(function(e) { //llamado a la función que levanta el documento reporte de Perfiles, basado en el criterio establecido en el combobox
                e.preventDefault();

                var condicion = $('#filtro').val();

                if (condicion > 0){
                    exportarPDFP(condicion);
                }else{
                    alert("Debe establecer un filtro para poder generar el reporte")
                }

            });

        }); //fin de preparación del documento
            
        function filtrando(){
            var x = $("#contenedor3");
            x.html('Cargando...');
        }

        function filtradoRealizado(){
            $("#contenedor3").load('pages/recursos_humanos/reporte/reporteFiltrado.php',data2);
        }
            
        function problemasbusqueda(){
            $("#contenedor").text('Problemas en el servidor.');
        }

        function exportarPDFEmp(codigoDepto){
            window.open("pages/recursos_humanos/reporte/crearPDFEmpleados.php?id="+codigoDepto);
            //$("#contenedor3").load('pages/recursos_humanos/reporte/reporteFiltrado.php',data2);
        }

        function exportarPDFP(codigoDepto){
            window.open("pages/recursos_humanos/reporte/crearPDFPerfiles.php?id="+codigoDepto);
        } 
        
    </script>

</head>




<div id="container" class="panel-primary">
    <h3 class="page-header panel-primary"><center><span class="fa fa-user fa-1.5x" aria-hidden="true"></span><strong>  Reporte de Empleados por Departamento  </strong></center></h3>
    
<?php

    if(isset($codMensaje) and isset($mensaje)){
        if($codMensaje == 1){
            echo '<div class="alert alert-success">';
            echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
            echo '<strong>Exito! </strong>';
            echo $mensaje;
            echo '</div>';
        }else{
            echo '<div class="alert alert-danger">';
            echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
            echo '<strong>Error! </strong>';
            echo $mensaje;
            echo '</div>';
        }
    } 

?>
    
</div>
      

 
<div id="contenedor2" class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span><strong> Empleados </strong></div>
        <div class = "col-sm-2">
            <form id = "formPDFEmp" role = "form" action ="#" method = "POST">
                <br>
                    <center><button type = "submit" class="btn btn-success pull-left">Exportar PDF Empleados</button></center>
                <br>
            </form>
        </div>

        <div class = "col-sm-2">
            <form id = "formPDFP" role = "form" action ="#" method = "POST">
                <br>
                    <center><button type = "submit" class="btn btn-danger pull-left">Exportar PDF Perfiles</button></center>
                <br>
            </form>
        </div>

        <div class = "col-sm-3"></div>

        <div class = "col-sm-5">
            <form id = "formFiltro" role = "form" action ="#" method = "POST">
                <br>
                <div class = "col-sm-12 pull-right">
                    <select id = "filtro" class = "form-control" name = "filtro">
                        <option value = "0" selected> ------------------------- TODOS ------------------------- </option>
                        <?php 
                            $consulta = "SELECT * from departamento_laboral";
                            $resultado = mysql_query($consulta);

                            while ($row = mysql_fetch_array($resultado)) {

                                $id = $row['Id_departamento_laboral'];

                                if ($id == $depID) {
                                    echo "<option value = '" . $row['Id_departamento_laboral'] . "'>";
                                } else {
                                    echo "<option value = '" . $row['Id_departamento_laboral'] . "'>";
                                }

                                echo utf8_encode($row["nombre_departamento"]);

                                echo "</option>";
                            }
                            
                        ?>
                    </select>
                </div>

                <div class = "col-sm-4 pull-right">
                    <button class="btn btn-warning pull-right">Filtrar</button>
                </div>
            </form>
        </div>

        <br>
        <br>        
        <br>
        <br>
        <br>

        <div id = "contenedor3" class = "panel-body">
          <?php
                require_once("../../../Datos/prepararReporte.php");
          ?>
        </div>
</div>
