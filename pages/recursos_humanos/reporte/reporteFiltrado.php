<?php

require_once('../../../Datos/conexion.php');

session_start();
$rol = $_SESSION['user_rol'];
$resultadoFiltro;

    if(isset($_POST['depto'])){
        
        $vfiltro =  $_POST['depto'];
        
        if($vfiltro == 0){
            $consulta = "SELECT * FROM persona WHERE N_identidad not in (SELECT sa_estudiantes.dni from sa_estudiantes) and N_identidad in (SELECT N_identidad from empleado Where estado_empleado='1')";

            $resultadoFiltro = mysql_query($consulta);
        }else{
            $consulta = "SELECT * from persona where persona.N_identidad not in (SELECT sa_estudiantes.dni from sa_estudiantes) and persona.N_identidad in (SELECT empleado.N_identidad from empleado Where estado_empleado='1' AND empleado.Id_departamento = '".$vfiltro."');";

            $resultadoFiltro = mysql_query($consulta);
        }
    }

?>


<script>
    $(document).ready(function(){
        fn_dar_eliminar();               
    });

    var x;
    x = $(document);
    x.ready(inicio);

    function inicio(){ 
        var x;
        x = $(".editarb");
        x.click(editarE);
        
        var x;
        x = $(".editaripb");
        x.click(editarinfoPE);
        
        var x;
        x = $(".verb");
        x.click(VerPerfil);
    };
            
    function fn_dar_eliminar(){
        $(".elimina").click(function(){
            id1 = $(this).parents("tr").find("td").eq(0).html();
            eliminarE();
        });
    };
   
    function eliminarE(){
        var respuesta = confirm("¿Esta seguro de que desea eliminar el registro seleccionado?");
        if (respuesta) {
            data1 = {codigoE: id1};

            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                url: "Datos/eliminarUniversidad.php",
                beforeSend: inicioEnvio,
                success: EliminarEmpleado,
                timeout: 4000,
                error: problemas
            });
            return false;
        }
    };

    function editarE(){
        var pid = $(this).parents("tr").find("td").eq(0).html();
        data = {identi: pid};

        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            //  url:"pages/recursos_humanos/modi_universidad.php",  
            beforeSend: inicioEnvio,
            success: EditarEmpleado,
            timeout: 4000,
            error: problemas
        });
        return false;
    };
    
    function editarinfoPE(){
        var pid = $(this).parents("tr").find("td").eq(0).html();
        data = {identi: pid};

        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            //  url:"pages/recursos_humanos/modi_universidad.php",  
            beforeSend: inicioeditarinfo,
            success: Editarinfo,
            timeout: 4000,
            error: problemas
        });
        return false;
    };

    function VerPerfil(){
        var pid = $(this).parents("tr").find("td").eq(0).html();
        data2 = {identi: pid};

        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            //  url:"pages/recursos_humanos/modi_universidad.php",  
            beforeSend: Ver,
            success: verPerfilP,
            timeout: 4000,
            error: problemasVerPerfil
        });
        return false;
    };
    
    function Ver(){
        var x = $("#contenedor");
        x.html('Cargando...');
    }
    
    function verPerfilP(){
        $("#contenedor").load('pages/recursos_humanos/cv/reportes/personaObtener.php',data2);
    }
    
    function problemasVerPerfil(){
        $("#contenedor").text('Problemas en el servidor.');
    }
    
    function inicioeditarinfo(){
        var x = $("#contenedor");
        x.html('Cargando...');
    }
    
    function Editarinfo(){
        $("#contenedor").load('pages/recursos_humanos/cv/actualizar/personaActualizar.php', data);
    }

    function inicioEnvio(){
        var x = $("#contenedor2");
        x.html('Cargando...');
    }

    function EditarEmpleado(){
        $("#contenedor").load('pages/recursos_humanos/cv/EditarCV.php', data);
    }

    function EliminarEmpleado(){
        $("#contenedor").load('Datos/eliminarEmpleado.php', data1);
    }

    function problemas(){
        $("#contenedor").text('Problemas en el servidor.');
    }

</script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#tabla_empleados').dataTable(); // example es el id de la tabla
    });
</script>
    
<script type="text/javascript">
  // For demo to fit into DataTables site builder...
  $('#tabla_empleados')
    .removeClass('display')
    .addClass('table table-striped table-bordered');
</script>

<div class="box">
    <div class="box-header">
</div><!-- /.box-header -->
           
<div class="box-body table-responsive">
    <?php
        echo <<<HTML
            <table id="tabla_empleados" class="table table-bordered table-striped">
                <thead>
                    <tr>                                            

                    <th style="text-align:center;background-color:#386D95;color:white;">Nro Identidad</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Apellido</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Ver perfil</th>
                    </tr>
                </thead>
                <tbody>
HTML;

        while ($rowFiltro = mysql_fetch_array($resultadoFiltro)){
            $Noidentidad = $rowFiltro['N_identidad'];
            $nombre = utf8_encode($rowFiltro['Primer_nombre']);
            $apellido = utf8_encode($rowFiltro['Primer_apellido']);
                echo "<tr data-id='".$Noidentidad."'>";
                echo <<<HTML
                <td>$Noidentidad</td>

HTML;

                echo <<<HTML
                <td>$nombre</td>

HTML;

                echo <<<HTML
                <td>$apellido</td>

HTML;

                echo <<<HTML

                <td>
                <center>
                    <button class="verb btn btn-success glyphicon glyphicon-folder-open"  title="Ver_perfil"></button>
                </center>
                </td>                   
                        
HTML;

                echo "</tr>";

            }

                   echo <<<HTML
            </tbody>
        </table>

HTML;
             
               ?>
           </div><!-- /.box-body -->
       </div><!-- /.box -->
                           
                            <!-- /.table-responsive -->
    </body>