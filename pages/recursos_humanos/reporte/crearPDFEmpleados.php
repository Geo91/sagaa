<?php

$maindir = "../../../";
require_once('../../../Datos/conexion.php');
require_once($maindir."fpdf/fpdf.php");
require($maindir."conexion/config.inc.php");

$IDdepto = $_GET['id'];

//Datos generales e info de contacto
$consulta = "SELECT persona.N_identidad, CONCAT(Primer_nombre, ' ', Segundo_nombre, ' ', Primer_apellido, ' ', Segundo_apellido) as NombreC, No_Empleado, empleado.Fecha_ingreso, departamento_laboral.nombre_departamento, empleado.Observacion from persona INNER JOIN (empleado INNER JOIN departamento_laboral on empleado.Id_departamento=departamento_laboral.Id_departamento_laboral) on persona.N_identidad=empleado.N_identidad Where estado_empleado = '1' AND empleado.Id_departamento in (SELECT departamento_laboral.Id_departamento_laboral from departamento_laboral where departamento_laboral.Id_departamento_laboral = '".$IDdepto."');";

$query = $db->prepare($consulta);
$query->execute();
$rows = $query->fetchAll();

class PDF extends FPDF{
    // Cabecera de página
    function Header(){

		$this->Image('../../../assets/img/lucen-aspicio.png', 50,30,200,200, 'PNG');
		//$this->Image('../../../assets/img/logo_unah.png' , 10,5,20,35, 'PNG');
		//$this->Image('../../../assets/img/logo-cienciasjuridicas.png' , 170,8, 35 , 35,'PNG');
		$this->Image('../../../assets/img/cabecera(2).png', 0,0,210,40, 'PNG');
		$this->Cell(18, 10, '', 0);
		$this->SetFont('Arial', '', 18);
		$this->Cell(5, 10, '', 0);
		//$this->Cell(140, 10,utf8_decode('Universidad Nacional Autónoma de Honduras'), 0,0,"C");
		$this->Ln(30);
		$this->SetFont('Arial', 'U', 14);
		$this->Cell(30, 8, '', 0,0,"C");
		$this->Cell(130, 8, ' Perfil Laboral del Empleado ', 0,0,"C");
		//$this->Rect(6, 45, 200, 240 ,'D');
		$this->SetFont('Arial', '', 12);
		$this->Ln(10);
	}
         
	function Footer(){
		$this->SetY(-15);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,utf8_decode('Página ').$this->PageNo().' de {nb}   - - - -   Generado el ' . date('d-m-y') . ' fecha del sistema',0,0,'C');
	}
}

$pdf = new PDF();
$pdf->SetFont('Arial', '', 12);

$pdf->AliasNbPages();

$Identidad = array();
$nombreCompleto = array();
$fechaIn = array();
$nEmpleado = array();
$depto = array();


function array_ref(&$arreglo,$cadena){
    $contador = 0;
	$arreglo[$contador] = $cadena;
}

foreach ($rows as $row) {
	array_ref($Identidad,$row["N_identidad"]);
	array_ref($nombreCompleto,$row["NombreC"]);
	array_ref($fechaIn,$row["Fecha_ingreso"]);
	array_ref($nEmpleado,$row["No_Empleado"]);
	array_ref($depto,$row["nombre_departamento"]);
	array_ref($obs, $row["Observacion"]);
	$numeroMayor[1] = count($Identidad);
	$numeroMayor[2] = count($nombreCompleto);
	$numeroMayor[3] = count($fechaIn);
	$numeroMayor[4] = count($nEmpleado);
	$numeroMayor[5] = count($depto);
	$numeroMayor[6] = count($obs);
	$n = max($numeroMayor);

	$contador = 0;
	while ($contador < $n) {
		$pdf->AddPage();

		$resultado2 = mysql_query("SELECT * FROM empleado_has_cargo inner join cargo on cargo.ID_cargo=empleado_has_cargo.ID_cargo where No_Empleado = '".$nEmpleado[$contador]."'");

	    $pdf->Cell(120, 9, utf8_decode('                                                                 INFORMACIÓN PERSONAL'), 0);
	    $pdf->Ln(10);
	    $pdf->Cell(125, 8, utf8_decode('Número de Identidad: ').$Identidad[$contador], 0);
	    $pdf->Cell(120, 10, 'Empleado: '.$nEmpleado[$contador], 0);
	    $pdf->Ln(5);
	    $pdf->Cell(130, 8, 'Nombre: '.$nombreCompleto[$contador], 0);
	    $pdf->Ln(5);
	    $pdf->Cell(130, 8, 'Fecha de ingreso como empleado: '.$fechaIn[$contador], 0);
	    $pdf->Ln(5);
	    $pdf->Cell(130, 8, 'Departamento: '.$depto[$contador], 0);
	    $pdf->Ln(10);
	    $pdf->Cell(120, 9,utf8_decode( '                                                                 INFORMACIÓN LABORAL'), 0);
	    $pdf->Ln(8);

	    while ($filasCargo = mysql_fetch_array($resultado2)) {
	        $fechaS = $filasCargo['Fecha_salida_cargo'];
	        if ($fechaS == NULL || $fechaS == "0000-00-00") {
	             $fecha ="Actualmente";
	        }else{
	            $fecha = $fechaS;
	        }

	        $pdf->Cell(125, 8, 'Cargo: '.$filasCargo['Cargo'], 0);
	        $pdf->Ln(5);
	        $pdf->Cell(125, 8, 'Fecha de Ingreso: '.$filasCargo['Fecha_ingreso_cargo'], 0);
	        $pdf->Ln(5);
	        $pdf->Cell(125, 8, 'Fecha de Salida: '.$fecha, 0);
	        $pdf->Ln(10);
	    }
	     
	    $pdf->Cell(120, 9, '                                                                    OBSERVACIONES', 0);
	    $pdf->Ln(8);
	    $pdf->Cell(125, 8, utf8_decode('Observación: ').$obs[$contador], 0);
	    $pdf->Ln(5);

	    $contador = $contador + 1;
	}
}

$resultado6 = mysql_query("SELECT * from departamento_laboral WHERE Id_departamento_laboral = '".$IDdepto."';");
while($filas_dl = mysql_fetch_array($resultado6)){
	$codDepto = $filas_dl['Id_departamento_laboral'];
	$nomDepto = utf8_encode($filas_dl['nombre_departamento']);
}

$pdf->Output('Reporte Empleados Departamento - '.$nomDepto.'.pdf','D');

?>