<?php 
 //header("Content-Type: text/html;charset=utf-8");
include '../../conexion/config.inc.php';
$maindir = "../../";

try{
    $query = "SELECT COUNT(persona.N_identidad) as conteo, departamento_laboral.nombre_departamento from persona, empleado, departamento_laboral where persona.N_identidad=empleado.N_identidad and empleado.Id_departamento=departamento_laboral.Id_departamento_laboral group by departamento_laboral.nombre_departamento";
    $result = mysql_query($query);
}
  catch(PDOExecption $e){
    $mensaje="Error en la obtencion de datos";
    $codMensaje =0;
}

?>


<script>
$(function () {
    $(document).ready(function () {

        // Build the chart
        $('#graficaEmpDepto').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: "Empleados por Departamento",
                colorByPoint: true,
                data: [
                <?php
                    $numItems = mysql_num_rows($result);
                    for($i = 0 ;$fila = mysql_fetch_array($result); $i++){
                        if ($i == 1){
                            $nombre = utf8_encode($fila["nombre_departamento"]);
                            $y = $fila["conteo"];
                            echo "{name: '".$nombre."', y: ".$y.",
                               sliced: true,
                               selected: true }";
                        }else{
                            $nombre = utf8_encode($fila["nombre_departamento"]);
                            $y = $fila["conteo"];
                            echo "{name: '".$nombre."', y: ".$y."}";
                        }
                        if($i != $numItems) {
                            echo ",";
                        }
                    }
                ?>]
            }]
        });
    });
});

</script>
<div id ="graficaEmpDepto"></div>