<?php

include '../../conexion/config.inc.php';

$maindir = "../../";

if(isset($_GET['contenido'])){
    $contenido = $_GET['contenido'];
}else{
    $contenido = 'recursos_humanos';
}

require_once($maindir."funciones/check_session.php");

require_once($maindir."funciones/timeout.php");

if(!isset( $_SESSION['user_id'] )){
    header('Location: '.$maindir.'login/logout.php?code=100');
    exit();
}

if(isset($_POST["tipoProcedimiento"])){
    $tipoProcedimiento = $_POST["tipoProcedimiento"];

    if($tipoProcedimiento == "insertar"){
        require_once("../../Datos/insertarEmpleado.php");
    }

    if($tipoProcedimiento == "EliminarEmple"){   
        require_once("../../Datos/eliminarEmpleado.php");
    }

    if($tipoProcedimiento == "Eliminar"){
        require_once("../../Datos/eliminarEmpleadoXGrupo.php");
    }
}

$consulta_mysql = "SELECT * FROM `persona`";
$rec = mysql_query($consulta_mysql);

?>

<!DOCTYPE html>
<html lang="utf-8">

<head>

    <meta charset="utf-8">
    
    <script type="text/javascript" src="pages/recursos_humanos/jquery-ui.js"></Script>
    <script>

            /* 
             * To change this license header, choose License Headers in Project Properties.
             * To change this template file, choose Tools | Templates
             * and open the template in the editor.
             */

        var x;
        x = $(document);
        x.ready(buscarPersona);
        
        function buscarPersona(){   
            var x;
            x = $("#buscarP");
            x.click(buscar);
        }


        function buscar(){
            data={
                idpersona:$('#Buscar_persona').val()
            };
            
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                beforeSend: inicioEnvio,
                success: llegadabuscar,
                timeout: 6000,
                error: problemas
            });
            return false;
        }
        
        function inicioEnvio(){
            var x = $("#Rbusqueda");
            x.html('Cargando...');
        }

        function llegadabuscar(){
            $("#Rbusqueda").load('Datos/BuscarPersona.php',data);
        }
        

        function problemas(){
            $("#Rbusqueda").text('Problemas en el servidor.');
        }

    </script>

</head>

<body class="panel-primary">

    <div id="container" class="panel-primary">
        <h3 class="page-header panel-primary"><center><span class="fa fa-users fa-1.5x" aria-hidden="true"></span><strong>  Gestión de Empleados  </strong></center></h3>
        <!--<div class="panel-heading"><span class="glyphicon glyphicon-user" aria-hidden="true"></span><strong> Gestión de Empleados </strong></div>-->
        
        <div class="row ex2" <?php if($_SESSION['user_rol'] <=49 ) echo 'style="display: none;"';?>>
            <a>
                <div class="col-lg-3 col-md-4 col-sm-5">
                    <div class="panel panel-primary">
                        <a id="agregar" href="#">
                            <div class="panel-footer" data-toggle="modal" data-target="#compose-modal" >
                                <span class="pull-left"><strong> Ingresar Nuevo Empleado </strong></span>
                                <span class="pull-right"><i class="fa fa-hand-o-right custom fa-2x"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </a>
        </div>
   
    <?php
     
        if(isset($codMensaje) and isset($mensaje)){
            if($codMensaje == 1){
              echo '<div class="alert alert-success">';
              echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
              echo '<strong>Exito! </strong>';
              echo $mensaje;
              echo '</div>';
            }else{
              echo '<div class="alert alert-danger">';
              echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
              echo '<strong>Error! </strong>';
              echo $mensaje;
              echo '</div>';
            }
        } 

    ?>

    <div id="contenedor3" class="panel-body">
        <?php
            require_once("../../Datos/cargarEmpleados.php");
        ?>
    </div>
  
    <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form role="form" id="form" name="form" action="#">
                    <div class="modal-header" style ="background-color:#0FA6C3">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" style = "color:white;"><i class="fa fa-plus-circle" aria-hidden="true" style ="color:white;"></i> Agregar un Nuevo Empleado </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">Buscar por Número de Identidad</span>

                                <div class="input-group">
                                    <input id="Buscar_persona" type="text"   class="form-control" placeholder="Buscar persona..."    required>
                                    <span class="input-group-btn">
                                    <button id="buscarP" class="btn btn-primary glyphicon glyphicon-search" type="button"></button>
                                    </span>
                                </div>
                            </div>   
                        </div>

                        <div id="Rbusqueda" class="form-group">

                        <?php

                        ?>

                        </div>
                    </div>
           <!--  <div class="modal-footer clearfix">
            <button name="submit" id="submit" class="insertarbg btn btn-primary pull-left"><i class="glyphicon glyphicon-pencil"></i> Insertar</button>
          </div>
           -->
                
                    
                </form>
    
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
   
</body>

</html>


