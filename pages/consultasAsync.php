

<?php
//Este archivo carga la tabla de los Objetivos Estratégicos institucionales
$maindir = "../";
  require_once($maindir . "conexion/config.inc.php");
$q = $_REQUEST["q"];
$dimensionID;
if ($q !== "") {
    //Conexion a la base de Datos ccjj
    /*$conectar = new mysqli('localhost','ddvderecho','DDVD3recho','ccjj');

    $enlace = mysql_connect('localhost', 'ddvderecho', 'DDVD3recho');
    mysql_select_db('ccjj', $enlace);*/

    $qJson = json_decode($q);
    $dimensionID = $qJson->idDimension;
    //echo $dimensionID;

    $functionName = $qJson->functionName;
    if($functionName!=""){
      switch ($functionName) {
        case 'filtrarDimension':
        //consulta para obetener Objetivos estratégicos institucionales
            $sql4 = "SELECT * FROM pe_objetivos_est_inst where dimension_id =".$dimensionID;
            $result4 = $db->prepare($sql4);
            $result4->execute();
          ?>



          <div id="tabla1contenedor">

          <table id="tabla_ObjetivosEs" class="table table-bordered table-striped" style="text-align:center;">
            <thead>
                <tr>
                  <th style="text-align: center;color:white; background-color:#386D95;">Nro.</th>
                  <th style="text-align: center;color:white; background-color:#386D95;">Objetivos Estratégicos Institucionales</th>
                  <th style="text-align: center;color:white; background-color:#386D95;">Descripcion Objetivos Institucionales</th>
                  <th style="text-align: center;color:white; background-color:#386D95;">Editar/Eliminar</th>
                </tr>
            </thead>
            <tbody>
              <?php
                  while ($row_obj_est_inst = $result4->fetch()) {
                    $ido = $row_obj_est_inst['id_objetivo_est_inst'];
                    $nobj = $row_obj_est_inst['nombre_obj_est_inst'];
                    $dobj = $row_obj_est_inst['descripcion_obj_inst'];
                    $idd = $row_obj_est_inst['dimension_id'];
                   ?>
                <tr data-ido = '<?php echo $ido; ?>'
                    data-nobj = '<?php echo $nobj; ?>'
                    data-dobj = '<?php echo $dobj ; ?>'
                    data-idd = '<?php echo $idd; ?>'>

                  <th style="text-align: center;"><?php echo $ido; ?></th>
                  <th><?php echo $nobj; ?></th>
                  <th><?php echo $dobj; ?></th>
                  <th style="text-align: center;">
                        <button type="button" class="EditarObjIns btn btn-info" data-toggle="modal" data-target="#modalEditarObjInst"><span class="glyphicon glyphicon-edit"></span></button>
                        <button type="button" class="btn btn-danger" ><span class="glyphicon glyphicon-trash"></span></button>
                  </th>
                </tr>
                <?php } ?>

            </tbody>
          </table>
          </div>

          <!--fin panel-->

          <?php
          break;
        default:
        ?>
        <div id="tabla1contenedor">

        <table id="tabla_ObjetivosEs" class="table table-bordered table-striped" style="text-align:center;">
          <thead>
              <tr>
                <th style="text-align: center;color:white; background-color:#386D95;">Nro.</th>
                <th style="text-align: center;color:white; background-color:#386D95;">Objetivos Estratégicos Institucionales</th>
                <th style="text-align: center;color:white; background-color:#386D95;"></th>
              </tr>
          </thead>
          <tbody>
              <tr>
                <th></th>
                <th>No hay datos disponibles</th>
                <th></th>
              </tr>
          </tbody>
        </table>
      </div>
<?php
      }
    }
  }
//include('../pages/cargarSelectObjetivosIns.php');

 ?>
 <!-- Modal para agregar nuevos objetivos estrategicos institucionales-->
 <div class="modal fade" id="modalAddObjetivoIns" role="dialog">
   <div class="modal-dialog">
     <div class="modal-content">
       <form role="form" id="formNuevoObjIns">
         <div class="modal-header" style="background-color:#33B3D1; color:white;">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title" style="text-align:center;"><strong>Nuevo Objetivo Estratégico Institucional</strong></h4>
         </div>
         <div class="modal-body">
           <div class="form-group">
             <input id="idd" type="hidden" value="<?php echo $idd; ?>"></input>
             <p><strong>Nombre Objetivo Institucional:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <input id="nombre_obj_est_inst" type="text" style="width:550px" required>
             </p>
             <p><strong>Descripción Objetivo Institucional:</strong>&nbsp;
               <textarea  id="descripcion_obj_inst" type="text" rows="5" cols="75">
               </textarea>
             </p>
           </div>
         </div>
         <div class="modal-footer">
           <div align:"right">
             <button type="button"  class="btn btn-danger" data-dismiss="modal">Cancelar</button>
             <button type="submit" class="btn btn-primary" id="guarda_obj">Guardar</button>

           </div>
         </div>
       </form>
     </div>
   </div>
 </div>


 <!-- Modal Editar Objetivo Estrategico-->
 <div class="modal fade" id="modalEditarObjInst" role="dialog">
 <div class="modal-dialog">
   <div class="modal-content">
     <form role="form" id="formEditarObjInst">
     <div class="modal-header" style="background-color:#33B3D1; color:white;">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h4 class="modal-title" style="text-align:center;"><strong>Editar Objetivo Estratégico Institucional</strong></h4>
     </div>
     <div class="modal-body">
       <div class="form-group">
         <input id="idd" type="hidden" value="<?php echo $idd;?>"> <!--id de la dimension-->
       </div>
       <div class="form-group">
       <p><strong>Nombre Objetivo Institucional</strong>
         <input id="editarNombreObjInst" type="text" style="width:550px" required>
       </p>
     </div>
     <div class="form-group">
       <p><strong>Descripción Objetivo Institucional</strong>
         <textarea  id="editarDescripcionObjInst" type="text" rows="5" cols="75" required>
         </textarea>
       </p>
     </div>
     <div class="modal-footer">
       <div align:"right">
         <button type="button"  class="btn btn-danger" data-dismiss="modal">Cancelar</button>
         <button type="submit" class="btn btn-primary" id="guarda">Guardar</button>
       </div>
     </div>
   </form>
   </div>
 </div>
 </div>
 </div>

 <script type="text/javascript" src="js\plan_estrategico\agregar_obj_institucional.js"></script>
 <script type="text/javascript" src="js\plan_estrategico\editar_obj_estrategico.js"></script>
