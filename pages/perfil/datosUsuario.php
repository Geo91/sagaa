<?php
	require_once("global.php");

	$consulta = mysql_query("SELECT * 
							FROM usuario,roles 
							WHERE usuario.Id_Rol = roles.Id_Rol 
								and usuario.id_Usuario=$userId");

	if($row = mysql_fetch_array($consulta)){
        $empleadoId = $row['No_Empleado'];
        $nombreUsuario = $row['nombre'];
        $rol = $row['Descripcion'];
        $fechaCreacion = $row['Fecha_Creacion'];
	}else{
		echo "No se encuentra información";
	}
?>
<div class="form-horizontal">
		<div class="panel-body">
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <label><span class="glyphicon glyphicon-user" aria-hidden="true"></span>Datos de usuario </label>
                                            </h4>
                                        </div>
                                            <div class="panel-body">
                                                <div class="col-lg-8">
                                                		<div class="form-group">
                                                            <label class="col-sm-5 control-label">Número de empleado: </label>
                                                            <div class="col-sm-7 control-label"><?php echo $empleadoId; ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-5 control-label">nombre de usuario: </label>
                                                            <div class="col-sm-7 control-label"><?php echo $nombreUsuario ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-5 control-label">Rol de usuario: </label>
                                                            <div class="col-sm-7 control-label"><?php echo$rol ?></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-5 control-label">Fecha de creación: </label>
                                                            <div class="col-sm-7 control-label"><?php echo$fechaCreacion ?></div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
