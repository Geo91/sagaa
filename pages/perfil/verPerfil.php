<?php

  $maindir = "../../";

  if(isset($_GET['contenido']))
  {
    $contenido = $_GET['contenido'];
  }
  else
  {
    $contenido = 'home';
  }
  
  $estaEnPrincipal = True;
  
  require_once($maindir."funciones/check_session.php");
  
  require_once($maindir."funciones/timeout.php");
  
  require_once($maindir.'pages/navbar.php');
  
  require_once($maindir."conexion/config.inc.php");

  $userId = $_SESSION['user_id'];
  $consulta = mysql_query("SELECT * 
                    from persona 
                    WHERE persona.N_identidad IN 
                     (select empleado.N_identidad 
                      from empleado,usuario 
                      WHERE usuario.No_Empleado = empleado.No_Empleado 
                            and usuario.id_Usuario=$userId)");
    if ($row = mysql_fetch_array($consulta)) {
        $_SESSION['userIdent'] = $row['N_identidad'];
    }
?>
<div id="wrapper">
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        <li >
                            <a id="DatosGenerales" href="#"><i class="glyphicon glyphicon-user"></i> Datos Generales</a>
                        </li>
                        <li>
                            <a  id="ExpAcademica" href="#"><i class="glyphicon glyphicon-list-alt"></i> Experiencia Académica</a>
                            
                        </li>
                        <li>
                            <a  id="FormAcademica" href="#"><i class="glyphicon glyphicon-pencil"></i> Formación Académica</a>
                            
                        </li>      
                    	<li>
                            <a  id="ExpLaboral" href="#"><i class="glyphicon glyphicon-briefcase"></i> Experiencia Laboral</a>
                            
                        </li> 
                                         
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
         
    </div>
    <div id="page-wrapper">                
        <div id="contenedor">
        </div>
    </div>

<script type="text/javascript" >
    var x;
    x=$(document);
    x.ready(opciones);
    
    function opciones(){

        $("#contenedor").load('pages/perfil/datosUsuario.php');

        var x;
        x=$("#DatosGenerales");
        x.click(ldDatosGenerales);

        var x;
        x=$("#ExpAcademica");
        x.click(ldExpAcademica);

        var x;
        x=$("#FormAcademica");
        x.click(ldFormAcademica);

        var x;
        x=$("#ExpLaboral");
        x.click(ldExpLaboral);
    }

    function ldDatosGenerales(){
        $("#contenedor").load('pages/perfil/datosGenerales.php');
    }
    function ldExpAcademica(){
        $("#contenedor").load('pages/perfil/ExpAcademica.php');
    }
    function ldFormAcademica(){
        $("#contenedor").load('pages/perfil/FormAcademica.php');
    }
    function ldExpLaboral(){
        $("#contenedor").load('pages/perfil/ExpLaboral.php');
    }

</script>