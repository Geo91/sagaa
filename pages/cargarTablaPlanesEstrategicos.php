
<?php
    $maindir = "../";
    require_once($maindir . "conexion/config.inc.php");

    /*$conectar = new mysqli('localhost','ddvderecho','DDVD3recho','ccjj');

    $enlace = mysql_connect('localhost', 'ddvderecho', 'DDVD3recho');
    mysql_select_db('ccjj', $enlace);

    $sql = "select * from pe_planes_estrategicos ORDER BY pe_planes_estrategicos.anio_inicio ASC";
    $result = mysql_query($sql);*/
    $enlace= new PDO('mysql:host=localhost;dbname=ccjj', 'ddvderecho', 'DDVD3rech');

   $sql = "select * from pe_planes_estrategicos ORDER BY pe_planes_estrategicos.anio_inicio ASC";
   $result = $db->prepare($sql);
   $result->execute();

 ?>


<table id="tabla_PlanesEstrategicos" class="table table-bordered table-striped" style="text-align:center;" >
  <thead>
      <tr>
          <th style="text-align: center;color:white; background-color:#386D95;display:none;">Id</th>
          <th style="text-align: center;color:white; background-color:#386D95;">Período</th>
          <th style="text-align: center;color:white; background-color:#386D95;">Fecha Ingreso</th>
          <th style="text-align: center;color:white; background-color:#386D95;">Fecha Modificación</th>
          <th style="text-align: center;color:white; background-color:#386D95;">Fecha Fin</th>
          <th style="text-align: center;color:white; background-color:#386D95;">Editar</th>
          <th style="text-align: center;color:white; background-color:#386D95;">Ir</th>

      </tr>
  </thead>
  <tbody>
    <?php
         while ($row =$result->fetch()) {
           $id_plan_estrategico = $row['id_plan_estrategico'];
           $anio_inicio = $row['anio_inicio'];
           $anio_fin = $row['anio_fin'];
           $fecha_ingreso = $row['fecha_ingreso'];
           $fecha_modificacion = $row['fecha_modificacion'];
           $fecha_terminacion = $row['fecha_terminacion'];
           ?>
           <tr data-id= '<?php echo  $id_plan_estrategico; ?>'
               data-ini = <?php echo $anio_inicio;?>
               data-fin = <?php echo $anio_fin;?>
               data-ingr = <?php echo $fecha_ingreso;?>
               data-mod = <?php echo $fecha_modificacion;?>
               data-ter = <?php echo $fecha_terminacion;?>>
             <td style="display:none;"><?php echo $id_plan_estrategico; ?></td>
             <td><?php echo $anio_inicio.'-'.$anio_fin; ?></td>
             <td><?php echo $fecha_ingreso; ?></td>
             <td><?php echo $fecha_modificacion; ?></td>
             <td><?php echo $fecha_terminacion; ?></td>

             <td><a class="Editar btn btn-info" data-toggle="modal" data-target="#modalEditar">
               <span class="glyphicon glyphicon-edit"></span></a>
             </td>
             <!--<td><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></td>-->
             <td> <button class="Ir<?php echo $auto_increment;?> btn btn-success" type="button" name="button">
               <span class="glyphicon glyphicon-arrow-right"></span></button>
             </td>
            </tr>

        <?php } ?>

  </tbody>
</table>

<link  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!--script para paginar tabla-->
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#tabla_PlanesEstrategicos').dataTable();
  } );
</script>
