
<?php

$maindir = "../";

require_once($maindir."funciones/check_session.php");

require_once($maindir."funciones/timeout.php");

require_once("ChekAutoIncrement.php");

require_once($maindir . "conexion/config.inc.php");

if(isset($_GET['contenido']))
{
	$contenido = $_GET['contenido'];
}
else
{
	$contenido = 'poa';
	$navbar_loc = 'contenido';
}

/*$conectar = new mysqli('localhost','ddvderecho','DDVD3recho','ccjj');*/

/*$enlace = mysql_connect('localhost', 'ddvderecho', 'DDVD3recho');
mysql_select_db('ccjj', $enlace);*/

$enlace= new PDO('mysql:host=localhost;dbname=ccjj', 'ddvderecho', 'DDVD3rech');

$sql = "select * from pe_planes_estrategicos";
$result = $db->prepare($sql);
$result->execute();


?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">

	<!--highcharts-->

	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>


	<!--  <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>-->
	<link  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="js/bootstrap-datepicker.js"></script>

	<script type="text/javascript" src="js\plan_estrategico\agregar_plan_estrategico.js"></script>
	<script type="text/javascript" src="js\plan_estrategico\editar_plan_estrategico.js"></script>


	<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">-->
</head>
<body>

	<div id="contenedor">
		<div id="respuesta">

		</div>

		<h1 class="page-header"><i class="fa fa-list-alt" aria-hidden="true"></i> Plan Estratégico</h1>

		<div class="row">

			<div class="col-lg-12">
				<div class="panel-body">
					<div class="tab-pane">
						<div class="row">
							<!-- Bar -->
							<div class="col-lg-6">
								<header class="panel-heading" style="text-align:center">
									<h3>Cumplimiento de Inidicadores del Plan Estratégico</h3>
								</header>


								<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
							</div>

							<div class="col-lg-6">
								<section class="panel-heading" style="text-align:center">
									<h3>Planes Estratégicos
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">
											<span class="glyphicon glyphicon-plus-sign"></span> Nueva Planificación</button>
											<!--<button type="button" class="btn btn-info" data-toggle="modal" data-target="#nuevoPlan"><i class="fa fa-plus-circle" aria-hidden="true"></i>  Nuevo Plan Estratégico</button>-->
											<!--<button class="btn btn-info"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nuevo Plan Estratégico</button>-->
										</h3>

										<div class="panel panel-primary">

											<div class="panel-heading">
												Mis Planes Estratégicos
											</div>

											<div class="panel-body" style="overflow:auto;">

											</br>
										</br>
										<div id="tabla_planes">
											<?php
											require_once("../pages/cargarTablaPlanesEstrategicos.php");
											?>

										</div>


									</div>

								</section>
							</div>

						</div>
					</div>
				</div>

			</div>

		</div>
	</div>
	<!-- Modal -->

	<!-- Modal Nueva programación de Ingreso del Plan Estratégico-->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<form role="form" id="formNuevoPlan" name="formNuevoPlan">
					<div class="modal-header" style="background-color:#33B3D1; color:white;">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" style="text-align:center;"><strong>Nueva Planificación</strong></h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<p><strong>Año de Inicio de Período:</strong>&nbsp;&nbsp;
								<input id="inicio" type="text" maxlength="4" size="4" onkeypress="return valida(event)" style="width:150px" required>
							</p>
							<p><strong>Año de Inicio de Fin:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input  id="fin" type="text"  maxlength="4" size="4" onkeypress="return valida(event)" style="width:150px" required>
							</p>
							<p><strong>Fecha de Ingreso:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input id="fecha_ingreso" type="date"required >
							</p>
							<p><strong>Fecha de Modificicaión:</strong>&nbsp;&nbsp;&nbsp;&nbsp;
								<input id="fecha_modificacion" type="date" required>
							</p>
							<p><strong>Fecha de Finalización:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input id="fecha_terminacion" type="date" required>
							</p>
						</div>
					</div>
					<div class="modal-footer">
						<div align:"right">
							<button type="button"  class="btn btn-danger" data-dismiss="modal">Cancelar</button>
							<button type="submit" class="btn btn-primary" id="guarda">Guardar</button>

						</div>
					</div>
				</form>
			</div>
		</div>

	</div>
</div>
<!-- Modal Editar Plan -->
<div class="modal fade" id="modalEditar" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form role="form" id="formEditarPlan" name="form">
				<div class="modal-header" style="background-color:#33B3D1; color:white;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" style="text-align:center;"><strong>Editar Planificación</strong></h4>
				</div>
				<div class="modal-body" >
					<div class="form-group">
						<input type="hidden" name="id" value="<?php echo $ide;?>">
					</div>
					<div class="form-group">
						<p><strong>Año de Inicio de Período:</strong>&nbsp;&nbsp;
							<input id="editarInicio" type="text" maxlength="4" size="4" onkeypress="return valida(event)" style="width:150px" >
						</p>
					</div>
					<div class="form-group">
						<p><strong>Año de Inicio de Fin:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input  id="editarFin" type="text"  maxlength="4" size="4" onkeypress="return valida(event)" style="width:150px" value="<?php echo $anio_inicio; ?>">
						</p>
					</div>
					<p><strong>Fecha de Ingreso:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input id="dpIngreso" type="date" name="fechaIngreso">
					</p>
					<div class="form-group">
						<p><strong>Fecha de Modificación:</strong>&nbsp;&nbsp; &nbsp;&nbsp;
							<input id="dpModificacion" type="date" name="fechaModificacion">
						</p>
					</div>
					<div class="form-group">
						<p><strong>Fecha de Terminación:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input id="dpTerminacion" type="date" name="fechaFin" >
						</p>
					</div>
				</div>
				<div class="modal-footer">
					<div align:"right">
						<button type="button"  class="btn btn-danger" data-dismiss="modal">Cancelar</button>
						<button type="submit" class="btn btn-primary" id="guarda">Guardar</button>
					</div>
				</div>
			</form>
		</div>

	</div>
</div>

</div>

</body>
</html>


<!--script que valida que el input solo reciba numeros-->
<script>
	function valida(e){
		tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
    	return true;
    }

    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
   }
  </script>

  <!--fin script-->

  <script type="text/javascript">

  	Highcharts.chart('container', {
  		chart: {
  			type: 'column'
  		},
  		title: {
  			text: ''
  		},

  		xAxis: {
  			categories: [
  			'Período 2010-2014',
  			'Período 2014-2018',
  			'Período 2018-2022',
  			'Período 2022-2026'
  			],
  			crosshair: true
  		},
  		yAxis: {
  			min: 0,
  			title: {
  				text: 'Porcentaje Alcanzado (0%-100%)'
  			}
  		},
  		tooltip: {
  			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
  			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
  			'<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
  			footerFormat: '</table>',
  			shared: true,
  			useHTML: true
  		},
  		plotOptions: {
  			column: {
  				pointPadding: 0.15,
  				borderWidth: 0
  			}
  		},
  		series: [{
  			name: '1er Año',
  			data: [49.9, 71.5, 100, 92.2],
  			color: '#9c0f06'

  		}, {
  			name: '2do Año',
  			data: [83.6, 78.8, 98.5, 93.4],
  			color: '#0fa6c3'

  		}, {
  			name: '3er Año',
  			data: [48.9, 38.8, 39.3, 41.4],
  			color:'#fbb900'

  		}, {
  			name: '4to Año',
  			data: [42.4, 33.2, 34.5, 39.7],
  			color: '#95c11e'

  		}]
  	});

  </script>



  <!-- Script necesario para que la tabla se ajuste a el tamanio de la pag-->
  <script type="text/javascript">
    // For demo to fit into DataTables site builder...
    $('#tabla_PlanesEstrategicos')
    .removeClass('display')
    .addClass('table table-striped table-bordered');
   </script>


   <!--Script para ir a cada registro seleccionado-->
   <script>

   	$(document).on("click",".Ir<?php echo $auto_increment;?>",function () {
   		id = $(this).parents("tr").find("td").eq(0).html();
   		data1 = {inicio: $("#inicio").val(),
   		fin: $("#fin").val(),
   		ide:id,

   	}
   	$.ajax({
   		async: true,
   		type: "POST",
   		dataType: "html",
   		contentType: "application/x-www-form-urlencoded",
        //url: "crearPlanEstrategico.php",
        beforeSend: inicioVer,
        success: llegadaVer,
        timeout: 4000,
        error: problemas
       });
   	return false;

   });

   	function llegadaVer()
   	{
   		$("#contenedor").load('pages/crearPlanEstrategico.php', data1);
   	}

   	function inicioVer()
   	{
   		var x = $("#contenedor");
  //x.html('Cargando...');
 }
 function problemas() {
 	var x = $("#contenedor");
 }

</script>
