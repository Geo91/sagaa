<?php  
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    if(isset($_POST['codigoR'])){
        $idR = $_POST['codigoR'];
        $crltv = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
            
        try{
            $sql = "CALL SP_RSV_IMPRIMIR(?,?,@mensajeError)";
            $query1 = $db->prepare($sql);
            $query1->bindParam(1,$idR,PDO::PARAM_INT);
            $query1->bindParam(2,$crltv,PDO::PARAM_STR);

            $query1->execute();
            $output = $db->query("select @mensajeError")->fetch(PDO::FETCH_ASSOC);
            $mensaje = $output['@mensajeError'];

            if ($mensaje == NULL){
                echo mensajes('El formato de responsabilidad ha sido exportado éxitosamente', 'verde');
                echo "<script type='text/javascript'>
                        window.open('pages/reservaciones/edicion/formato_pdf.php?id='+$idR);
                    </script>";
            }else{
                echo mensajes('Error! '.$mensaje, 'rojo');
            }
        }catch(PDOexception $e){
            echo mensajes('Error! '. $e, 'rojo');
        }
    }else{
        echo mensajes("Hubo un error al intentar exportar el formato de responsabilidad. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
    }
    
?>

<script type="text/javascript">
    $("#tablaDatos").load('pages/reservaciones/edicion/rsv_datosSolicitudes.php');    
</script>