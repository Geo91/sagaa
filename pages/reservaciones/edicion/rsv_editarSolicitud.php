<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    
    if(isset($_POST['idS'])){
    	$pcCodigo = $_POST['idR'];
    	$nE = $_POST['nEmp'];
	    $idsalon = $_POST['idS'];
	    $fechaI = $_POST['fInicio'];
	    $fechaF = $_POST['fFin'];
	    $horaI = $_POST['hInicio'];
	    $horaF = $_POST['hFin'];
	    $cel = $_POST['ncel'];
	    $prop = $_POST['pro'];
	    $clase = $_POST['clase'];
	    $evento = $_POST['evento'];
	    $resp = $_POST['rsp'];
	    $unid = $_POST['uni'];
	    $ext = $_POST['externa'];

    }elseif(isset($_POST['id'])){
    	$pcCodigo = $_POST['id'];

	    $sql2 = "SELECT rsv_reservaciones.salon_id, rsv_reservaciones.n_empleado, rsv_reservaciones.fecha_inicio, rsv_reservaciones.fecha_fin, rsv_reservaciones.horaI, rsv_reservaciones.horaF, rsv_reservaciones.numcelular, rsv_reservaciones.proposito, rsv_reservaciones.clase_id, rsv_reservaciones.evento, rsv_reservaciones.clase_ext, rsv_reservaciones.responsable, rsv_reservaciones.unidad FROM rsv_reservaciones INNER JOIN rsv_salones ON rsv_reservaciones.salon_id=rsv_salones.id_salon WHERE rsv_reservaciones.id_reservacion = '$pcCodigo'";

		$resultado2 = $db->prepare($sql2);
	    $resultado2->execute();
	    while($row = $resultado2->fetch()){
	    	$nE = $row['n_empleado'];
	    	$idsalon = $row['salon_id'];
	    	$fechaI = $row['fecha_inicio'];
	    	$fechaF = $row['fecha_fin'];
	    	$horaI = $row['horaI'];
	    	$horaF = $row['horaF'];
	    	$cel = $row['numcelular'];
	    	$prop = $row['proposito'];
	    	$clase = $row['clase_id'];
	    	$evento = $row['evento'];
	    	$resp = $row['responsable'];
	    	$unid = $row['unidad'];
	    	$ext = $row['clase_ext'];
	    }
	}

	$sql3 = "SELECT rsv_salones.id_salon, rsv_salones.nombre_salon FROM rsv_salones";
    
?>

<div class="modal-header" style = "background-color:#0FA6C3">
  	<button type="button" class="close" data-dismiss="modal">&times;</button>
  	<h4 class="modal-title" style = "color:white">Editar Solicitud de Uso de Salón </h4>
</div>

<div class="modal-body">
        
    <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden<br>En caso de usar Mozilla Firefox el formato de fecha debe ser 'año-mes-día' y el formato de hora debe ser de 24 hrs. Ejem: '14:00:00'</center></div>

    <div class="row">
        <div class="col-lg-12">

	    <!--************************formulario*********************-->           
	        <form class="form-horizontal" role="form" id="formEditarSolicitudSalon" name="formEditarSolicitudSalon">

				<div class="row form-group">
				    <label class ="col-sm-2 control-label" hidden="true">Código Reservación</label>
				    <div class = "col-sm-2">
				        <input type = "text" id = "ecodigoR" value="<?php echo $pcCodigo; ?>" hidden="true" disabled="true">
				    </div>
				</div>

				<div class="row form-group">
	                <label class="col-sm-2 control-label"> Salón </label>  
	                <div class="col-sm-6">
	                    <select class="form-control" id="ecodigoSalon" name="ecodigoSalon">
                            <?php
                                $resultado3 = $db->prepare($sql3);
                                $resultado3->execute();
                                while ($fila3 = $resultado3->fetch()) {
                            ?>
                                    <option value="<?php echo $fila3['id_salon']; ?>" <?php if($fila3['id_salon'] == $idsalon){ echo 'selected'; } ?>> <?php echo $fila3["nombre_salon"]; ?></option>
                            <?php
                                }
                            ?>

                        </select>
	                </div>                      
	            </div>

	            <div class="row form-group"> 
	                <label class="col-sm-2 control-label">Fecha Inicio</label>
	                <div class="col-sm-4">
	                    <input type="date" id="efechaInicio" name="datepicker" value="<?php echo $fechaI ?>" required><span class=""></span>
	                </div>               

	                <label class="col-sm-2 control-label">Fecha Fin</label>
	                <div class="col-sm-2">
	                    <input type="date" id="efechaFin" name="datepicker" value="<?php echo $fechaF ?>" required><span class=""></span>
	                </div>              
	            </div>

	            <div class="row form-group">                    
	                <label class="control-label col-sm-2">Hora Inicio</label>
	                <div class="col-sm-4">
	                    <input type="time" id="ehoraIn" name="horaIn" value="<?php echo $horaI ?>" required><span class=""></span>
	                </div>                 

	                <label class="control-label col-sm-2">Hora Fin</label>
	                <div class="col-sm-2">
	                    <input type="time" id="ehoraFin" name="horaFin" value="<?php echo $horaF ?>" required><span class=""></span>
	                </div>                 
	            </div>

	            <div class="modal-footer">
	                <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
	                <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" > Siguiente <span class="glyphicon glyphicon-circle-arrow-right"></span></button>                                  
	            </div>  

	        </form>
	    <!--************************formulario*********************-->
	    </div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){ 

	    $("#formEditarSolicitudSalon").submit(function(e) {
	        e.preventDefault();
	        var cellphone = '<?php echo $cel; ?>';
	        var nuE = '<?php echo $nE; ?>';
	        var clase = '<?php echo $clase; ?>';
	        var propo = '<?php echo $prop; ?>';
	        var event = '<?php echo $evento; ?>';
	        var respons = '<?php echo $resp; ?>';
	        var unit = '<?php echo $unid; ?>';
	        var exter = '<?php echo $ext; ?>';
	        data={
	            codR:$('#ecodigoR').val(),
	            codS:$('#ecodigoSalon').val(),
	            fIn:$('#efechaInicio').val(),
	            fFi:$('#efechaFin').val(),
	            hIn:$('#ehoraIn').val(),
	            hFi:$('#ehoraFin').val(),
	            cel: cellphone,
	            nE: nuE,
	            clss: clase,
	            prpst: propo,
	            evnt: event,
	            rsp: respons,
	            uni: unit,
	            extr: exter
	        }
	        $.ajax({
	            async: true,
	            type: "POST",
	            dataType: "html",
	            contentType: "application/x-www-form-urlencoded",
	            success: pasoDos,
	            timeout: 6000,
	            error: problemas
	        });
	        return false;
	    });

	    function pasoDos(){
	    	$("#contenidoModal").html('');
    		$("#contenidoModal").load('pages/reservaciones/edicion/rsv_editarSolicitudP.php',data);
	    }

	});

</script>