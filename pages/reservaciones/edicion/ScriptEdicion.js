$(document).ready(function(){

    // Funciones de Edición
    $("#eproposito").click(function(event) {
        event.preventDefault();
        var valor = $('#eproposito').val();
        if (valor==1) {
            $('#eevento').val('');
            $('#elbevento').show('fast');
            $('#edevento').show('fast');
            $('#elbclase').hide('fast');
            $('#edclase').hide('fast');
            $('#eclase').val('0');
            $('#elbexterna').hide('fast');
            $('#edexterna').hide('fast');
            $('#eexterna').val('NULL');
            document.getElementById("eevento").disabled = false;
            document.getElementById("eclase").disabled = true;
            document.getElementById("eexterna").disabled = true;
        }else if(valor==2){
            $('#elbclase').hide('fast');
            $('#edclase').hide('fast');
            $('#eclase').val('0');
            $('#elbevento').hide('fast');
            $('#edevento').hide('fast');
            $('#eevento').val('NULL');
            $('#elbexterna').show('fast');
            $('#edexterna').show('fast');
            $('#eexterna').val('');
            document.getElementById("eclase").disabled = true;
            document.getElementById("eevento").disabled = true;
            document.getElementById("eexterna").disabled = false;
        }else{
            $('#elbclase').show('fast');
            $('#edclase').show('fast');
            $('#eclase').val('0');
            $('#elbevento').hide('fast');
            $('#edevento').hide('fast');
            $('#eevento').val('NULL');
            $('#elbexterna').hide('fast');
            $('#edexterna').hide('fast');
            $('#eexterna').val('NULL');
            document.getElementById("eclase").disabled = false;
            document.getElementById("eevento").disabled = true;
            document.getElementById("eexterna").disabled = true;
        }
    });

    $("#formEditarSolicitudSalonP").submit(function(e) {
        e.preventDefault();
        if(validarFecha() === true){
            if(validarHorario() === true){
                data={
                    idR:$('#ecodigoR').val(),
                    codigo:$('#ecodigoS').val(),
                    fInicio:$('#efechaInicio').val(),
                    fFin:$('#efechaFin').val(),
                    hInicio:$('#ehoraIn').val(),
                    hFin:$('#ehoraFin').val(),
                    respon:$('#eresponsable').val(),
                    nEmp:$('#enumEmp').val(),
                    unit:$('#eunidad').val(),
                    numCel:$('#ecelular').val(),
                    proposito:$('#eproposito').val(),
                    clase:$('#eclase').val(),
                    evento:$('#eevento').val(),
                    externa:$('#eexterna').val()
                }
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: solicitudEditada,
                    timeout: 10000,
                    error: problemas
                });
                return false;
            }else{
                alert("El horario no ha sido ingresado correctamente, intentelo de nuevo");
            }
        }else{
            alert("Las fechas no se han ingresado correctamente, intentelo de nuevo");
        }
        
    });

    //Finalizan Funciones del formulario de edición
    
});

function validarFecha(){
    var fI = $('#efechaInicio').val();
    var fF = $('#efechaFin').val();
    if(fF >= fI){
        return true;
    }else{
        return false;
    }
}

function validarHorario(){
    var hI = $('#ehoraIn').val();
    var hF = $('#ehoraFin').val();
    if(hF > hI){
        return true;
    }else{
        return false;
    }
}

function inicioEnvio(){
    $("#notificacion").empty();
    $("#notificacion").text('Cargando...');
}

function problemas(){
    $("#notificacion").text('Problemas en el servidor.');
}

function editarSolicitudSalon(){
    $("#contenidoModal").load('pages/reservaciones/edicion/rsv_editarSolicitud.php',data);
    $("#modalEdicion").modal('show');
}

function solicitudEditada(){
    $("#notificacion").load('pages/reservaciones/edicion/rsv_solicitudEditada.php',data);
    $("#modalEdicion").modal('hide');
}

function cancelarSolicitudSalon(){
    $("#notificacion").load('pages/reservaciones/edicion/rsv_solicitudCancelada.php',data);
    $("#modalEdicion").modal('hide');
}

function exportar(){
    $("#notificacion").empty();
    $("#notificacion").load('pages/reservaciones/edicion/exportarFormato.php', data);           
};