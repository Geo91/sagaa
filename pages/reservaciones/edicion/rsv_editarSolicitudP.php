<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    $vCodR = $_POST['codR'];
    $vCodS = $_POST['codS'];
    $vFI = $_POST['fIn'];
    $vFF = $_POST['fFi'];
    $vHI = $_POST['hIn'];
    $vHF = $_POST['hFi'];
    $vCel = $_POST['cel'];
    $vNum = $_POST['nE'];
    $vClase = $_POST['clss'];
    $vProp = $_POST['prpst'];
    $vEvent = $_POST['evnt'];
    $vResp = $_POST['rsp'];
    $vUni = $_POST['uni'];
    $vExtr = $_POST['extr'];

    $sql = "SELECT ca_asignaturas.id_asignatura, ca_asignaturas.nombre FROM ca_asignaturas";

?>

<div class="modal-header" style = "background-color:#0FA6C3">
  	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title" style = "color:white">Editar Solicitud de Uso de Salón </h4>
</div>

<div class="modal-body">

    <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden<br>En caso de usar Mozilla Firefox el formato de fecha debe ser 'año-mes-día' y el formato de hora debe ser de 24 hrs. Ejem: '14:00:00'</center>
    </div>
    <!--************************formulario*********************-->           
    <form class="form-horizontal" role="form" id="formEditarSolicitudSalonP" name="formEditarSolicitudSalonP">

		<div class="row form-group">
		    <div class = "col-sm-2">
		        <input type = "text" id = "ecodigoR" value="<?php echo $vCodR; ?>" hidden="true" disabled="true">
		        <input type = "text" id = "ecodigoS" value="<?php echo $vCodS; ?>" hidden="true" disabled="true">
		        <input type="date" id="efechaInicio" name="datepicker" value="<?php echo $vFI ?>" hidden="true" required>
		        <input type="date" id="efechaFin" name="datepicker" value="<?php echo $vFF ?>" hidden="true" required>
		        <input type="time" id="ehoraIn" name="horaIn" value="<?php echo $vHI ?>" hidden="true" required>
		        <input type="time" id="ehoraFin" name="horaIn" value="<?php echo $vHF ?>" hidden="true" required>
		    </div>
		</div>

        <div class="row form-group">
            <label class="col-sm-2 control-label"> Responsable </label>  
            <div class="col-sm-4">
                <input type="text" class="form-control" id="eresponsable" name="responsable" value = "<?php echo $vResp; ?>" title = "Ingrese el nombre de la persona responsable de la actividad" required>
            </div>
            
            <label class ="col-sm-2 control-label"> #Empleado </label>
		    <div class = "col-sm-1">
		        <input type = "text" id = "enumEmp" value = "<?php echo $vNum; ?>" title = "Ingrese el número de empleado de la persona responsable de la actividad" required>
		    </div>       
        </div>

       	<div class="row form-group">
            <label class="col-sm-2 control-label"> Celular </label>  
            <div class="col-sm-4">
                <input type="text" class="form-control" id="ecelular" name="celular" value = "<?php echo $vCel; ?>" title = "Ingrese un número de celular sin guiones">
            </div>                
        </div>

        <div class="row form-group">
            <label class=" col-sm-2 control-label">Unidad</label>
            <div class="col-sm-6">     
                <input type="text" class="form-control" id="eunidad" name="unidad" value = "<?php echo $vUni; ?>" title = "Ingrese el nombre de la unidad responsable de la actividad" required>         
            </div>                     
        </div>

        <div class="row form-group">
            <label class="col-sm-2 control-label"> Propósito </label>  
            <div class="col-sm-1">
                <select id="eproposito" name="proposito" size="3" required>
                	<?php
                    	if($vProp == 0){
                    		echo "<option value='0' selected> Clase </option>
                    			<option value='1'> Evento </option>
                    			<option value='2'> Clase Externa </option>";
                    	}elseif($vProp == 1){
							echo "<option value='0'> Clase </option>
                    			<option value='1' selected> Evento </option>
                    			<option value='2'> Clase Externa </option>";
                    	}else{
							echo "<option value='0'> Clase </option>
                    			<option value='1'> Evento </option>
                    			<option value='2' selected> Clase Externa </option>";
                    	}
                    ?>
                </select>
            </div>
        </div>                    

        <div class="row form-group">
            <label class=" col-sm-2 control-label" id="elbclase" <?php if($vProp == 1 or $vProp == 2){ echo "hidden = 'true'"; } ?> >Clase</label>
            <div class="col-sm-6" id = "edclase" <?php if($vProp == 1 or $vProp == 2){ echo "hidden = 'true'"; } ?> >     
                <select class="form-control" id="eclase" name="clase">
                    <option value="0">Seleccione una opción</option>
                    <?php
                        $resultado = $db->prepare($sql);
                        $resultado->execute();
                        while ($fila = $resultado->fetch()) {
                    ?>
                            <option value="<?php echo $fila['id_asignatura']; ?>" <?php if($fila['id_asignatura'] == $vClase){ echo 'selected'; } ?>> <?php echo $fila["nombre"]; ?></option>
                    <?php
                        }
                    ?>
                </select>                         
            </div>                     

            <label class="col-sm-2 control-label" id="elbevento" <?php if($vProp == 0 or $vProp == 2){ echo "hidden = 'true'"; } ?> > Evento</label>
            <div class="col-sm-6" id="edevento" <?php if($vProp == 0 or $vProp == 2){ echo "hidden = 'true'";} ?> >                        
                <input type="text" class="form-control" id="eevento" name="evento" value = "<?php echo $vEvent; ?>">
            </div>

            <label class="col-sm-2 control-label" id="elbexterna" <?php if($vProp == 0 or $vProp == 1){ echo "hidden = 'true'"; } ?> >Clase</label>
            <div class="col-sm-6" id="edexterna" <?php if($vProp == 0 or $vProp == 1){ echo "hidden = 'true'"; } ?> >              
                <input type="text" class="form-control" value = "<?php echo $vExtr; ?>" id="eexterna" name="externa" >
            </div>                      
        </div>

        <div class="modal-footer">
            <button id="RegresarFormulario" class="btn btn-info"><span class="glyphicon glyphicon-circle-arrow-left"></span> Regresar </button>
            <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" > Enviar <span class="glyphicon glyphicon-circle-arrow-right"></span></button>    
        </div>    

    </form>
    <!--************************formulario*********************-->
</div>

<script type="text/javascript">
	$(document).ready(function(){ 

	    $("#RegresarFormulario").click(function(e) {
	        e.preventDefault();
	        data={
	            idR:$('#ecodigoR').val(),
	            idS:$('#ecodigoS').val(),
	            fInicio:$('#efechaInicio').val(),
	            fFin:$('#efechaFin').val(),
	            hInicio:$('#ehoraIn').val(),
	            hFin:$('#ehoraFin').val(),
	            rsp:$('#eresponsable').val(),
	            nEmp:$('#enumEmp').val(),
	            ncel:$('#ecelular').val(),
	            uni:$('#eunidad').val(),
	            pro:$('#eproposito').val(),
	            clase:$('#eclase').val(),
	            evento:$('#edevento').val(),
	            externa:$('#eexterna').val()
	        }
	        $.ajax({
	            async: true,
	            type: "POST",
	            dataType: "html",
	            contentType: "application/x-www-form-urlencoded",
	            success: pasoUno,
	            timeout: 6000,
	            error: problemas
	        });
	        return false;
	    });

	    function pasoUno(){
	    	$("#contenidoModal").html('');
    		$("#contenidoModal").load('pages/reservaciones/edicion/rsv_editarSolicitud.php',data);
	    }

	});

</script>

<script type="text/javascript" src="pages/reservaciones/edicion/ScriptEdicion.js"></script>