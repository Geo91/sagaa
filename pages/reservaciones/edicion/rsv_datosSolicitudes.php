<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");
    require_once("../ChekAutoIncrement.php");
    $idUsuario = $_SESSION['user_id'];
?>


    <div class="row">
        <div class="col-lg-12">
            <!--<section class="content">-->
                <div class="table-responsive">
                    <table id= "TablaMSolicitudes" border="0" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th style = "display:none;text-align:center;background-color:#386D95;color:white;">Codigo</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Salón</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Ubicación</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Fecha Inicio</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Fecha Fin</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Horario</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Propósito</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Observación</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Estado</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Operaciones</th>
                                <!--<th style="text-align:center;background-color:#386D95;color:white;">Cancelar</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Exportar</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Contenido de la tabla generado atravez de la consulta a 
                                la base de datos -->
                                <?php
                                    try{
                                        $sql = "CALL SP_RSV_DATOS_MSOLICITUDES(?)";
                                        $query = $db->prepare($sql);
                                        $query->bindParam(1,$idUsuario,PDO::PARAM_INT);

                                        $query->execute();
                                        while ($fila = $query->fetch()) {
                                ?>
                                        <tr data-id='<?php echo $fila["id_reservacion"]; ?>'>
                                            <td style="display:none"><?php echo $fila["id_reservacion"]; ?></td>
                                            <td><?php echo $fila["nombre_salon"]; ?></td>
                                            <td><?php echo $fila["ubicacion"]; ?></td>
                                            <td><?php echo $fila["fecha_inicio"]; ?></td>
                                            <td><?php echo $fila["fecha_fin"]; ?></td>
                                            <td><?php echo $fila["horaI"]." - ".$fila["horaF"]; ?></td>
                                            <td><?php echo $fila["proposito"]; ?></td>
                                            <td><?php echo $fila["observacion"]; ?></td>
                                            <td><?php echo $fila["estadoR"]; ?></td>
                                            <td>
                                                <center>
                                                <div class="btn-button-group btn-group-sm">
                                                    <button type="button" class="editarSolicitud<?php echo $auto_increment; ?> btn btn-info"  title="Editar la solicitud." <?php if ($fila["estadoR"] == 'Aprobado' or $fila["estadoR"] == 'Denegado' or $fila["estadoR"] == 'Cancelado'){ echo "disabled"; }?> ><i class="fa fa-pencil-square-o"></i></button>
                                                
                                                    <button type="button" class="cancelarSolicitud<?php echo $auto_increment; ?> btn btn-danger" title="Cancelar la solicitud de uso del salón." <?php if ($fila["estadoR"] == 'Aprobado' or $fila["estadoR"] == 'Cancelado' or $fila["estadoR"] == 'Denegado'){ echo "disabled"; }?> ><i class="fa fa-trash"></i></button>
                                                
                                                    <button type="button" class="exportarPDF<?php echo $auto_increment; ?> btn" title="Exportar la solicitud de permiso aprobado." style="background-color:white;border-color:red;" <?php if ($fila["estadoR"] == 'Espera' or $fila["estadoR"] == 'Denegado' or $fila["estadoR"] == 'Cancelado'){ echo "disabled"; }?> ><i class="fa fa-file-pdf-o"></i></button>
                                                </div>
                                                </center>
                                            </td>         
                                        </tr>
                                <?php 
                                        } //cierre del ciclo while para llenar la tabla de datos
                                    }catch(PDOException $e){
                                        echo "Error: ".$e;
                                    }
                                ?>
                        </tbody>
                    </table>       
                </div>
            <!--</section>-->
        </div>                
    </div>


<script type="text/javascript">   
//opciones para buscador en la tabla
    $('#TablaMSolicitudes')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#TablaMSolicitudes').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {

        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });

    $(document).on("click", ".editarSolicitud<?php echo $auto_increment; ?>", function () {
        var estado = $(this).parents("tr").find("td").eq(8).html();
        if(estado === 'Aprobado' || estado === 'Denegado'){
            alert("La solicitud ya ha sido revisada, no puede editarse");
        }else{
            
            data = {
                id: $(this).parents("tr").data("id")
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                success: editarSolicitudSalon,
                timeout: 15000,
                error: problemas
            }); 
            return false; 
        }
    });

    $(document).on("click", ".cancelarSolicitud<?php echo $auto_increment; ?>", function () {
        var estado = $(this).parents("tr").find("td").eq(8).html();
        var respuesta = confirm("¿Está seguro que desea cancelar esta solicitud?");
        if (respuesta){
            if(estado === 'Denegado'){
                alert("La solicitud ya ha sido denegada");
            }else{
                
                data = {
                    idR: $(this).parents("tr").data("id")
                }
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    success: cancelarSolicitudSalon,
                    timeout: 15000,
                    error: problemas
                }); 
                return false; 
            }
        }
    });

    $(document).on("click", ".exportarPDF<?php echo $auto_increment; ?>", function () {
        var estado = $(this).parents("tr").find("td").eq(8).html();
        var respuesta = confirm("A continuación se exportará el formato de responsabilidad de uso de la sala ¿Desea continuar?");

        if (respuesta){
            if(estado === 'Aprobado'){
                data={
                    codigoR:$(this).parents("tr").data("id")
                }
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    success: exportar,
                    timeout: 10000,
                    error: problemas
                }); 
                return false;
            }else{
                alert("La solicitud no ha sido aprobada, no puede exportarse");
            }
        }    
    });



</script>

<script type="text/javascript" src="pages/reservaciones/edicion/ScriptEdicion.js"></script>