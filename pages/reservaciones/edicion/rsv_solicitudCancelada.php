<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");


    if(isset($_POST['idR'])){

		$pcCodigoR = $_POST['idR'];

        try{
			$consulta=$db->prepare("CALL SP_RSV_CANCELAR_SOLICITUD(?,@pcMensajeError)");
			$consulta->bindParam(1, $pcCodigoR, PDO::PARAM_INT);

			$resultado=$consulta->execute();
			$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
			$mensaje = $output['@pcMensajeError'];

			if ($mensaje == NULL){
			    echo mensajes('Su solicitud ha sido cancelada exitosamente', 'verde');
			}else{
			    echo mensajes('Error!'.$mensaje, 'rojo');
			}
		}catch(PDOexception $e){
			echo mensajes('Error!'. $e, 'rojo');
		}
	}else{
		echo mensajes("Hubo un error al intentar cancelar la solicitud. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
	}

?>

<script type="text/javascript">
	$("#tablaDatos").load('pages/reservaciones/edicion/rsv_datosSolicitudes.php');    
</script>