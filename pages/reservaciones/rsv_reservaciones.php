<?php
    $maindir = "../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");
?>

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong>Salones Reservados</strong></center></h3>
    </div>
</div> 

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <!--<section class="content">-->
                <div class="table-responsive">
                    <table id= "TablaInformacion" border="0" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th style = 'display:none'>Codigo</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th> 
                                <th style="text-align:center;background-color:#386D95;color:white;">Salón</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Fecha Inicio</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Fecha Fin</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Horario</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Propósito</th>         
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Contenido de la tabla generado atravez de la consulta a 
                                la base de datos -->
                                <?php
                                    try{
                                        $sql = "CALL SP_DATOS_SOLICITUDES_SALONES()";
                                        $query = $db->prepare($sql);
                                        $query->execute();
                                        while ($fila = $query->fetch()) {
                                ?>
                                        <tr data-id='<?php echo $fila["id_reservacion"]; ?>'>
                                            <td style = "display:none"><?php echo $fila["id_reservacion"]; ?></td>
                                            <td><?php echo $fila["Empleado"]; ?></td>
                                            <td><?php echo $fila["nombre_salon"]; ?></td>
                                            <td><?php echo $fila["fecha_inicio"]; ?></td>
                                            <td><?php echo $fila["fecha_fin"]; ?></td>
                                            <td><?php echo $fila["horaI"]." - ".$fila["horaF"]; ?></td>
                                            <td><?php echo $fila["proposito"]; ?></td>     
                                        </tr>
                                <?php 
                                        } //cierre del ciclo while para llenar la tabla de datos
                                    }catch(PDOException $e){
                                        echo "Error: ".$e;
                                    }
                                ?>
                        </tbody>
                    </table>       
                </div>
            <!--</section>-->
        </div>                
    </div>
</div>

<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#TablaInformacion')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#TablaInformacion').dataTable({
        "order": [[2, "asc"],[3,"desc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });

</script>
