**************************************************************************
DROP PROCEDURE SP_RSV_EDITAR_SOLICITUD //
CREATE DEFINER=`ddvderecho`@`localhost` PROCEDURE `SP_RSV_EDITAR_SOLICITUD`(IN `pcCodigoR` INT, IN `pcSalon` INT, IN `pcFechaI` DATE, IN `pcFechaF` DATE, IN `pcHoraIn` TIME, IN `pcHoraFin` TIME, IN `pcProposito` INT, IN `pcClase` VARCHAR(7), IN `pcEvento` VARCHAR(200), IN `pcExterna` VARCHAR(200), IN `pcResp` VARCHAR(100), IN `pcUnidad` VARCHAR(150), IN `pcCelular` INT, IN `pcEmp` INT, OUT `pcMensajeError` VARCHAR(1000))
SP: BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);          
    DECLARE vEmp INT;
    DECLARE vTraslape INT;
    DECLARE vSolicitudes INT;
    DECLARE vPrevios INT;
    DECLARE vAdmin INT;
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
        SET pcMensajeError := vcTempMensajeError;
    
    END;
    
    SET vEmp := (SELECT COUNT(empleado.No_Empleado) FROM empleado WHERE empleado.No_Empleado = pcEmp);
    
    SET vTraslape := (SELECT COUNT(id_reservacion) FROM rsv_reservaciones WHERE id_reservacion != pcCodigoR AND salon_id = pcSalon AND (estado_reservacion = 5) AND ((rsv_reservaciones.fecha_inicio BETWEEN pcFechaI AND pcFechaF) OR (rsv_reservaciones.fecha_fin BETWEEN pcFechaI AND pcFechaF)) AND ((rsv_reservaciones.horaI BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00')) OR (rsv_reservaciones.horaF BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00'))))));
                           
    SET vSolicitudes := (SELECT COUNT(id_reservacion) FROM rsv_reservaciones WHERE n_empleado = pcEmp AND id_reservacion != pcCodigoR AND estado_reservacion = 5 AND ((rsv_reservaciones.fecha_inicio BETWEEN pcFechaI AND pcFechaF) OR (rsv_reservaciones.fecha_fin BETWEEN pcFechaI AND pcFechaF)) AND ((rsv_reservaciones.horaI BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00')) OR (rsv_reservaciones.horaF BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00'))))));
    
    SET vPrevios := (SELECT DATEDIFF(pcFechaI,CURRENT_DATE()));
    
    SET vAdmin := (SELECT COUNT(rsv_administrador.n_empleado) FROM rsv_administrador WHERE rsv_administrador.n_empleado = pcEmp AND rsv_administrador.id_salon = pcSalon);
        
    IF (vEmp = 1) THEN
        IF vPrevios > 2 THEN
            IF vSolicitudes = 0 THEN
                IF vTraslape = 1 THEN
                    BEGIN
                        SET pcMensajeError := CONCAT('El salón solicitado ya ha sido reservado. Cambie la fecha y hora e intentelo de nuevo.');
                        LEAVE SP;
                    END;
                END IF;
            ELSE
                BEGIN
                    SET pcMensajeError := ('Usted tiene reservado otro espacio en la fecha y horario especificados. La solicitud no puede realizarse');
                    LEAVE SP;
                END;
            END IF;
        ELSE
            BEGIN
                SET pcMensajeError := ('Las solicitudes de uso de salones deben realizarse con un mínimo de 3 días de anticipación. Cambie la fecha de inicio e intentelo de nuevo');
                LEAVE SP;
            END;
        END IF;
    ELSE
        BEGIN
            SET pcMensajeError := ('El número de empleado ingresado no corresponde con ningún empleado registrado. Ingrese un número de empleado válido');
            LEAVE SP;
        END;
    END IF;
    
    START TRANSACTION;
    IF (vAdmin = 1) THEN
        UPDATE rsv_reservaciones SET n_empleado=pcEmp, salon_id=pcSalon, fecha_inicio=pcFechaI, fecha_fin=pcFechaF, horaI=pcHoraIn, horaF=pcHoraFin, proposito=pcProposito, clase_id=pcClase, evento=pcEvento, clase_ext=pcExterna, responsable=pcResp, unidad=pcUnidad, numcelular=pcCelular, estado_reservacion='5' WHERE id_reservacion = pcCodigoR;
    ELSE
        UPDATE rsv_reservaciones SET n_empleado=pcEmp, salon_id=pcSalon, fecha_inicio=pcFechaI, fecha_fin=pcFechaF, horaI=pcHoraIn, horaF=pcHoraFin, proposito=pcProposito, clase_id=pcClase, evento=pcEvento, clase_ext=pcExterna, responsable=pcResp, unidad=pcUnidad, numcelular=pcCelular, estado_reservacion='1' WHERE id_reservacion = pcCodigoR;
    END IF;

    COMMIT;
    
END