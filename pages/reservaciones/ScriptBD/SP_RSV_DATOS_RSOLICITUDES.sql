*****************************************************************************************
-- PROCEDIMIENTO ALMACENADO PARA OBTENER LOS DATOS DE LAS SOLICITUDES QUE SE REVISARAN
DROP PROCEDURE SP_RSV_DATOS_RSOLICITUDES //
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_DATOS_RSOLICITUDES`(IN `pcUsuario` INT)
BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);
    DECLARE vEmp INT;
    DECLARE vSalon INT;

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
    
    END;
    
    SET vEmp := (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = pcUsuario);
    
    SET vcTempMensajeError := 'Error al obtener la información de las solicitudes';
    
    SELECT rsv_reservaciones.id_reservacion, CONCAT(persona.Primer_nombre,' ', persona.Primer_apellido) as 'Empleado', rsv_salones.nombre_salon, rsv_reservaciones.fecha_inicio, rsv_reservaciones.fecha_fin, rsv_reservaciones.horaI, rsv_reservaciones.horaF, rsv_reservaciones.responsable, rsv_reservaciones.unidad, (CASE rsv_reservaciones.proposito WHEN '0' THEN 'Clase' WHEN '1' THEN 'Evento' END) as 'proposito', ca_asignaturas.nombre, rsv_reservaciones.evento FROM rsv_reservaciones LEFT JOIN empleado on rsv_reservaciones.n_empleado=empleado.No_Empleado LEFT JOIN departamento_laboral on departamento_laboral.Id_departamento_laboral=rsv_reservaciones.unidad INNER JOIN persona on empleado.N_identidad=persona.N_identidad INNER JOIN rsv_salones ON rsv_reservaciones.salon_id=rsv_salones.id_salon LEFT JOIN ca_asignaturas on rsv_reservaciones.clase_id=ca_asignaturas.id_asignatura WHERE rsv_reservaciones.estado_reservacion = '1' AND rsv_reservaciones.salon_id IN (SELECT rsv_administrador.id_salon FROM rsv_administrador WHERE rsv_administrador.n_empleado = vEmp);

END
-- realizado 23/01/2018