*****************************************************************************************
-- PROCEDIMIENTO PARA OBTENER UN LISTADO DE EMPLEADOS QUE HAN SIDO ASIGNADOS COMO ADMINISTRADORES DE UN SALON.
DROP PROCEDURE SP_RSV_DATOS_ADMINISTRADORES //
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_DATOS_ADMINISTRADORES`(IN `pcUsuario` INT)
BEGIN

    DECLARE vsalon INT;
    
    SET vsalon := (SELECT rsv_administrador.id_salon FROM rsv_administrador WHERE rsv_administrador.n_empleado IN (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = pcUsuario));
    
    SELECT No_Empleado, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) AS 'nombre' FROM persona INNER JOIN empleado ON persona.N_identidad=empleado.N_identidad WHERE empleado.No_Empleado IN (SELECT rsv_administrador.n_empleado FROM rsv_administrador WHERE rsv_administrador.id_salon = vsalon);

END
-- realizado 09/10/2017