*****************************************************************************************
-- PROCEDIMIENTO PARA QUITAR PRIVILEGIOS DE ADMINISTRADOR A UN EMPLEADO
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_REMOVER_PRIVILEGIOS`(IN `pcEmpleado` INT, OUT `pcMensajeError` VARCHAR(1000))
SP: BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);   
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
        SET pcMensajeError := vcTempMensajeError;
    
    END;
      
    SET vcTempMensajeError := ('Error al otorgar privilegios de administrador a este empleado');
    
    
    START TRANSACTION;
    
        DELETE FROM rsv_administrador WHERE rsv_administrador.n_empleado = pcEmpleado;

    COMMIT;
    
END
-- realizado 09/10/2017