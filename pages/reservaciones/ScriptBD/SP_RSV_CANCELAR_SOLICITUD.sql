********************************************************************
-- PROCEDIMIENTO ALMACENADO PARA CANCELAR LAS SOLICITUDES DE RESERVACION DE USO DE SALON
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_CANCELAR_SOLICITUD`(IN `pcCodigoR` INT, OUT `pcMensajeError` VARCHAR(1000))
SP: BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);          
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
        SET pcMensajeError := vcTempMensajeError;
    
    END;
      
    SET vcTempMensajeError := ('Error al cancelar la solicitud');
    
    START TRANSACTION;
    
        UPDATE rsv_reservaciones SET estado_reservacion='6', observacion = 'Usted canceló esta solicitud.' WHERE id_reservacion = pcCodigoR;

    COMMIT;
    
END
-- realizado 09/10/2017