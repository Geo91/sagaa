*****************************************************************************************
-- PROCEDIMIENTO PARA REGISTRAR NUEVOS SALONES
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_REGISTRAR_SALON`(IN `pcNombre` VARCHAR(100), IN `pcUbicacion` VARCHAR(100), IN `pcCapacidad` INT, IN `pcEmpleado` INT, OUT `pcMensajeError` VARCHAR(500))
SP: BEGIN

    DECLARE vTempMensajeError VARCHAR(500) DEFAULT ''; 
    DECLARE vContador INT DEFAULT 0; 
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
    
        SET vTempMensajeError := CONCAT('Error: ', vTempMensajeError);
        SET pcMensajeError := vTempMensajeError;
    
    END;    
    
    SELECT
        COUNT(rsv_salones.nombre_salon)
    INTO
        vContador
    FROM
        rsv_salones
    WHERE
        nombre_salon = pcNombre;
        
    IF vContador > 0 then
        SET pcMensajeError := 'Ya hay un salón registrado con ese nombre, intente otro';
        LEAVE SP;
    END IF;
    
    START TRANSACTION;
        INSERT INTO rsv_salones (nombre_salon, capacidad, ubicacion) VALUES (pcNombre, pcCapacidad, pcUbicacion);
        INSERT INTO rsv_administrador (id_salon, n_empleado) VALUES ((SELECT MAX(rsv_salones.id_salon) FROM rsv_salones ORDER BY rsv_salones.id_salon DESC LIMIT 1), pcEmpleado);
        
    COMMIT;

END
-- realizado 09/10/2017
