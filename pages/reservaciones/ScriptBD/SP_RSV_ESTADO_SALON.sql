*****************************************************************************************
-- PROCEDIMIENTO PARA HABILITAR O DESHABILITAR EL USO DE LOS SALONES

CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_ESTADO_SALON`(IN `pcCodigo` INT, OUT `pcMensajeError` VARCHAR(500))
SP: BEGIN

    DECLARE vEstado INT DEFAULT 0;
    
    SET vEstado := (SELECT rsv_salones.habilitado FROM rsv_salones WHERE rsv_salones.id_salon = pcCodigo);
    
    IF vEstado = 1 THEN
        UPDATE rsv_salones SET rsv_salones.habilitado = '0' WHERE rsv_salones.id_salon = pcCodigo;
        UPDATE rsv_reservaciones SET rsv_reservaciones.estado_reservacion = '4', rsv_reservaciones.observacion = 'Este salón ha sido deshabilitado. Se ha denegado su solicitud' WHERE rsv_reservaciones.salon_id = pcCodigo AND rsv_reservaciones.estado_reservacion != 5;
    ELSE
        UPDATE rsv_salones SET rsv_salones.habilitado = '1' WHERE rsv_salones.id_salon = pcCodigo;
    END IF;

END
-- realizado 09/10/2017