************************************************************************************
DROP PROCEDURE SP_RSV_DATOS_MSOLICITUDES //

CREATE DEFINER=`ddvderecho`@`localhost` PROCEDURE `SP_RSV_DATOS_MSOLICITUDES`(IN `pcUsuario` INT)
BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
    
    END;
    
    SET vcTempMensajeError := 'Error al obtener la información de las solicitudes';
    
    SELECT rsv_reservaciones.id_reservacion, rsv_salones.nombre_salon, rsv_salones.ubicacion, rsv_reservaciones.fecha_inicio, rsv_reservaciones.fecha_fin, rsv_reservaciones.horaI, rsv_reservaciones.horaF, (CASE rsv_reservaciones.proposito WHEN '0' THEN 'Clase' WHEN '1' THEN 'Evento' END) as 'proposito', rsv_reservaciones.observacion, (CASE rsv_reservaciones.estado_reservacion WHEN '1' THEN 'Espera' WHEN '4' THEN 'Denegado' WHEN '5' THEN 'Aprobado' WHEN '6' THEN 'Cancelado' END) as 'estadoR' FROM rsv_reservaciones INNER JOIN rsv_salones ON rsv_reservaciones.salon_id=rsv_salones.id_salon WHERE rsv_reservaciones.id_usuario = pcUsuario;

END