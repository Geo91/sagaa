***************************************************************************************
DROP PROCEDURE SP_RSV_APROBAR_SOLICITUD //
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_APROBAR_SOLICITUD`(IN `pcCodigo` INT, IN `pcObservacion` VARCHAR(200), IN `pcHoraI` TIME, IN `pcHoraF` TIME, IN `pcFechaI` DATE, IN `pcFechaF` DATE, OUT `pcMensajeError` VARCHAR(1000))
SP: BEGIN
    
    DECLARE temp_reservacion INT;
    DECLARE fin INTEGER DEFAULT 0;
    
    DECLARE ciclo_reservaciones CURSOR FOR
        SELECT rsv_reservaciones.id_reservacion FROM rsv_reservaciones WHERE rsv_reservaciones.salon_id = (SELECT rsv_reservaciones.salon_id FROM rsv_reservaciones WHERE rsv_reservaciones.id_reservacion = pcCodigo) AND ((rsv_reservaciones.fecha_inicio BETWEEN pcFechaI AND pcFechaF) OR (rsv_reservaciones.fecha_fin BETWEEN pcFechaI AND pcFechaF)) AND (rsv_reservaciones.horaI = pcHoraI OR (rsv_reservaciones.horaI BETWEEN (ADDTIME(pcHoraI,'00:01:00')) AND (SUBTIME(pcHoraF,'00:01:00')) OR (rsv_reservaciones.horaF BETWEEN (ADDTIME(pcHoraI,'00:01:00')) AND (SUBTIME(pcHoraF,'00:01:00'))) OR rsv_reservaciones.horaF = pcHoraF));
        
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET fin=1;
    
    OPEN ciclo_reservaciones;
            get_reservaciones: LOOP
                FETCH ciclo_reservaciones INTO temp_reservacion;
                IF fin = 1 THEN
                    LEAVE get_reservaciones;
                END IF;
                
                UPDATE rsv_reservaciones SET rsv_reservaciones.estado_reservacion = '4', rsv_reservaciones.observacion = 'Se dio prioridad a otra solicitud' WHERE rsv_reservaciones.id_reservacion = temp_reservacion;
                
            END LOOP get_reservaciones;
        CLOSE ciclo_reservaciones;
    
    START TRANSACTION;
    
        UPDATE rsv_reservaciones SET rsv_reservaciones.estado_reservacion = '5', rsv_reservaciones.observacion = pcObservacion WHERE rsv_reservaciones.id_reservacion = pcCodigo;

    COMMIT;
    
END
-- realizado 23/01/2018