*****************************************************************************************
-- PROCEDIMIENTO ALMACENADO PARA EDITAR LA INFORMACIÓN DE LOS SALONES REGISTRADOS
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_EDITAR_SALON`(IN `pcCodigo` INT, IN `pcNombre` VARCHAR(100), IN `pcUbicacion` VARCHAR(100), IN `pcCapacidad` INT, OUT `pcMensajeError` VARCHAR(500))
SP: BEGIN

    DECLARE vTempMensajeError VARCHAR(500) DEFAULT ''; 
    DECLARE vContador INT DEFAULT 0; 
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
    
        SET vTempMensajeError := CONCAT('Error: ', vTempMensajeError);
        SET pcMensajeError := vTempMensajeError;
    
    END;    
    
    SELECT COUNT(rsv_salones.nombre_salon) INTO vContador FROM rsv_salones WHERE nombre_salon = pcNombre AND rsv_salones.id_salon != pcCodigo;
    
    IF vContador > 0 then
        SET pcMensajeError := 'Ya hay un salón registrado con ese nombre, intente otro';
        LEAVE SP;
    END IF;
    
    START TRANSACTION;
        UPDATE rsv_salones SET nombre_salon=pcNombre, capacidad=pcCapacidad, ubicacion=pcUbicacion WHERE rsv_salones.id_salon = pcCodigo;
        
    COMMIT;

END
-- realizado 09/10/2017