*****************************************************************************************
-- PROCEDIMIENTO PARA IMPRIMIR EL FORMATO DE RESPONSABILIDAD DE USO DE LA SALA
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_IMPRIMIR`(IN `pcCodigo` INT, IN `pcCorrelativo` VARCHAR(10), OUT `mensaje` VARCHAR(500))
SP: BEGIN 

DECLARE vnTempMensaje VARCHAR(500);
DECLARE contador INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; 

SET vnTempMensaje := CONCAT('ERROR:', mensaje);
SET mensaje := vnTempMensaje;
END; 

SET contador := (SELECT COUNT(rsv_reservaciones.id_reservacion) from rsv_reservaciones where rsv_reservaciones.estado_reservacion = 5 AND rsv_reservaciones.id_reservacion = pcCodigo);

IF (contador = 0) THEN 
    BEGIN
        SET mensaje := CONCAT('No se pudo exportar la solicitud');
        LEAVE SP;
    END;
END IF;

START TRANSACTION; 
    UPDATE rsv_reservaciones SET  rsv_reservaciones.correlativo = pcCorrelativo where rsv_reservaciones.id_reservacion = pcCodigo; 
COMMIT; 

END
-- realizado 09/10/2017
