********************************
--CREACION TABLA SALONES
CREATE TABLE `ccjj`.`rsv_salones` ( `id_salon` INT NOT NULL AUTO_INCREMENT , `nombre_salon` VARCHAR(100) NOT NULL , `capacidad` INT NOT NULL , `ubicacion` VARCHAR(100) NOT NULL , PRIMARY KEY (`id_salon`)) ENGINE = MyISAM;

ALTER TABLE `rsv_salones` ADD `habilitado` TINYINT(1) NOT NULL DEFAULT '1' AFTER `ubicacion`;
--realizado 09/10/2017
********************************
-- CREACION TABLA ACONDICIONAMIENTO SALONES
CREATE TABLE `ccjj`.`rsv_salon_acondicionamientos` ( `salon_id` INT NOT NULL , `acondicionamiento_id` INT NOT NULL ) ENGINE = MyISAM;


ALTER TABLE rsv_salon_acondicionamientos ADD FOREIGN KEY(salon_id) REFERENCES rsv_salones.id_salon ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE rsv_salon_acondicionamientos ADD FOREIGN KEY(acondicionamiento_id) REFERENCES ca_tipos_acondicionamientos.id_acondicionamiento ON DELETE CASCADE ON UPDATE CASCADE;
-- realizado 09/10/2017
**********************************
-- CREACION TABLA RESERVACIONES
CREATE TABLE `ccjj`.`rsv_reservaciones` ( `id_reservacion` INT NOT NULL AUTO_INCREMENT , `n_empleado` INT NOT NULL , `salon_id` INT NOT NULL , `fecha_inicio` DATE NOT NULL , `fecha_fin` DATE NOT NULL , `horaI` TIME NOT NULL , `horaF` TIME NOT NULL , `proposito` TINYINT NOT NULL , `clase_id` INT NULL , `evento` VARCHAR(200) NULL ,  PRIMARY KEY (`id_reservacion`)) ENGINE = MyISAM;

ALTER TABLE `rsv_reservaciones` CHANGE `clase_id` `clase_id` VARCHAR(7) NULL DEFAULT NULL;

ALTER TABLE `rsv_reservaciones` ADD `estado_reservacion` INT(1) NOT NULL AFTER `evento`;

ALTER TABLE rsv_reservaciones ADD COLUMN observacion VARCHAR(200);

ALTER TABLE `rsv_reservaciones` ADD `numcelular` INT(8) NOT NULL AFTER `evento`;

ALTER TABLE rsv_reservaciones ADD `correlativo` VARCHAR(10) NULL AFTER observacion;

ALTER TABLE rsv_reservaciones ADD FOREIGN KEY(estado_reservacion) REFERENCES estado_permiso.id_estado ON UPDATE CASCADE;
ALTER TABLE rsv_reservaciones ADD FOREIGN KEY(salon_id) REFERENCES rsv_salones.id_salon ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE rsv_reservaciones ADD FOREIGN KEY(n_empleado) REFERENCES empleado.No_Empleado ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE rsv_reservaciones ADD FOREIGN KEY(clase_id) REFERENCES ca_asignaturas.id_asignatura ON DELETE CASCADE ON UPDATE CASCADE;
-- realizado 09/10/2017
*******************************
-CREACION DE TABLA PARA ASIGNAR ADMINISTRADORES DE SALONES
CREATE TABLE `ccjj`.`rsv_administrador` ( `n_empleado` INT NOT NULL , `id_salon` INT NOT NULL , UNIQUE `emp` (`n_empleado`)) ENGINE = MyISAM;

INSERT INTO `rsv_administrador` (`n_empleado`, `id_salon`) VALUES ('13195', '1');
-- realizado 09/10/2017
**************************************************************************
ALTER TABLE `rsv_reservaciones` ADD `clase_ext` VARCHAR(150) NULL AFTER `evento`;
ALTER TABLE `rsv_reservaciones` ADD `responsable` VARCHAR(100) NOT NULL AFTER `evento`;
ALTER TABLE `rsv_reservaciones` ADD `numEmpleado` INT NOT NULL AFTER `responsable`;
ALTER TABLE `rsv_reservaciones` ADD `unidad` VARCHAR(150) NOT NULL AFTER `numcelular`;
ALTER TABLE `rsv_reservaciones` ADD `id_usuario` INT NOT NULL AFTER `correlativo` //
ALTER TABLE rsv_reservaciones ADD FOREIGN KEY(id_usuario) REFERENCES usuario.id_Usuario ON DELETE CASCADE ON UPDATE CASCADE;
-- realizado 11/10/2017