***************************************************************************************
-- PROCEDIMIENTO ALMACENADO PARA DENEGAR LAS SOLICITUDES 
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_DENEGAR_SOLICITUD`(IN `pcCodigo` INT, IN `pcObservacion` VARCHAR(200), OUT `pcMensajeError` VARCHAR(1000))
SP: BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);          

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
        SET pcMensajeError := vcTempMensajeError;
    
    END;
    
    START TRANSACTION;
    
        UPDATE rsv_reservaciones SET rsv_reservaciones.estado_reservacion = '4', rsv_reservaciones.observacion = pcObservacion WHERE rsv_reservaciones.id_reservacion = pcCodigo;

    COMMIT;
    
END
-- realizado 09/10/2017