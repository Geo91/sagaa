*****************************************************************************************
-- PROCEDIMIENTO PARA DAR PRIVILEGIOS DE ADMINISTRADOR A UN EMPLEADO
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_DAR_PRIVILEGIOS`(IN `pcEmpleado` INT, IN `pcUsuario` INT, OUT `pcMensajeError` VARCHAR(1000))
SP: BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);   
    DECLARE vSalon INT;
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
        SET pcMensajeError := vcTempMensajeError;
    
    END;
      
    SET vcTempMensajeError := ('Error al otorgar privilegios de administrador a este empleado');
    
    SET vSalon := (SELECT rsv_administrador.id_salon FROM rsv_administrador WHERE rsv_administrador.n_empleado IN (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = pcUsuario));
    
    START TRANSACTION;
    
        INSERT INTO rsv_administrador(n_empleado, id_salon) VALUES (pcEmpleado, vSalon);

    COMMIT;
    
END
-- realizado 09/10/2017