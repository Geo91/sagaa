*****************************
--PROCEDIMIENTO ALMACENADO PARA VISUALIZAR LOS DATOS DE LAS SOLICITUDES DE LOS SALONES
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_DATOS_SOLICITUDES_SALONES`()
BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
    
    END;
    
    SET vcTempMensajeError := 'Error al obtener la información de las solicitudes';
    
    SELECT rsv_reservaciones.id_reservacion, CONCAT(persona.Primer_nombre,' ',persona.Primer_apellido) as 'Empleado', rsv_salones.nombre_salon, rsv_reservaciones.fecha_inicio, rsv_reservaciones.fecha_fin, rsv_reservaciones.horaI, rsv_reservaciones.horaF, (CASE rsv_reservaciones.proposito WHEN '0' THEN 'Clase' WHEN '1' THEN 'Evento' END) as 'proposito' FROM persona INNER JOIN (empleado INNER JOIN (rsv_reservaciones INNER JOIN rsv_salones ON rsv_reservaciones.salon_id=rsv_salones.id_salon) ON empleado.No_Empleado=rsv_reservaciones.n_empleado) on persona.N_identidad=empleado.N_identidad WHERE rsv_reservaciones.estado_reservacion = '5';

END
-- realizado 09/10/2017