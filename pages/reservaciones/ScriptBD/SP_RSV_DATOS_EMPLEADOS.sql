*****************************************************************************************
--PROCEDIMIENTO PARA OBTENER LOS DATOS DE LOS EMPLEADOS EN EL FORMULARIO DE ASIGNACION DE ADMINISTRADORES
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_DATOS_EMPLEADOS`(IN `pcUsuario` INT, IN `pcOperacion` INT)
BEGIN

    DECLARE vdependencia INT;
    
    SET vdependencia := (SELECT empleado.id_dependencia FROM empleado WHERE empleado.No_Empleado IN (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = pcUsuario));
    
    IF (pcOperacion = 1) THEN
        SELECT No_Empleado, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) AS 'nombre' FROM persona INNER JOIN empleado ON persona.N_identidad=empleado.N_identidad WHERE empleado.id_categoria IN (SELECT categoria.categoria_id FROM categoria WHERE categoria.descripcion_cat = 'Administrativo') AND empleado.id_dependencia = vdependencia AND empleado.No_Empleado NOT IN (SELECT rsv_administrador.n_empleado FROM rsv_administrador);
    ELSE
        SELECT No_Empleado, CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) AS 'nombre' FROM persona INNER JOIN empleado ON persona.N_identidad=empleado.N_identidad WHERE empleado.id_categoria IN (SELECT categoria.categoria_id FROM categoria WHERE categoria.descripcion_cat = 'Administrativo');
    END IF;

END

-- realizado 09/10/2017