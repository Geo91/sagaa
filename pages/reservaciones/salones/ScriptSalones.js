//cargar la tabla desde un principio
    $("#SalonesTabla").load('pages/reservaciones/salones/rsv_datosSalones.php');

$(document).ready(function(){

    $("#formSalones").submit(function(e) {
        e.preventDefault();
        data={
            nSalon:$('#NombreSalon').val(),
            ubicacion:$('#ubicacion').val(),
            cap:$('#capacidad').val(),
            recursos:$('#recursos').val(),
            admin:$('#empleado').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: guardarRegistro,
            timeout: 6000,
            error: problemas
        });
        return false;
    });

    $("#formEditarSalon").submit(function(e) {
        e.preventDefault();
        data={
            mCodigo:$('#modalCodigo').val(),
            mnSalon:$('#modalSalon').val(),
            mUbicacion:$('#modalUbicacion').val(),
            mCap:$('#modalCapacidad').val(),
            mRecursos:$('#modalRecursos').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: editarRegistro,
            timeout: 6000,
            error: problemas
        });
        return false;
    });
    
});

function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function editarSalon(){
    $("#contenidoM").load('pages/reservaciones/salones/rsv_editarSalon.php',data);
    $("#divRespuesta").empty();
    $("#modalEditar").modal('show');
}

function guardarRegistro(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/reservaciones/salones/rsv_guardarSalon.php',data);
    $("#formSalones").trigger("reset");
}

function editarRegistro(){
    $("#divRespuestaEditar").empty();
    $("#divRespuestaEditar").load('pages/reservaciones/salones/rsv_salonEditado.php',data);
    $("#modalEditar").modal('hide');
    $("#divRespuesta").empty();
}

function verRecursos(){
    $("#contenidoM").load('pages/reservaciones/solicitud/rsv_verRecursos.php',data);
    $("#divRespuesta").empty();
    $("#modalEditar").modal('show');
}

function estadoSalon(){
    $("#divRespuestaEditar").empty();
    $("#divRespuestaEditar").load('pages/reservaciones/salones/rsv_estadoSalon.php',data);
    $("#divRespuesta").empty();
}