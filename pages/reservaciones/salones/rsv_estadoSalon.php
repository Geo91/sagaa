<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    if(isset($_POST['id'])) {

		$pcSalon = $_POST['id'];

        try{
			$consulta=$db->prepare("CALL SP_RSV_ESTADO_SALON(?,@pcMensajeError)");
			$consulta->bindParam(1, $pcSalon, PDO::PARAM_INT);

			$resultado=$consulta->execute();
			$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
			$mensaje = $output['@pcMensajeError'];

			if ($mensaje == NULL){
			    echo mensajes('El estado del salón ha sido cambiado exitósamente', 'verde');
			}else{
			    echo mensajes('Error!'.$mensaje, 'rojo');
			}
		}catch(PDOexception $e){
			echo mensajes('Error!'. $e, 'rojo');
		}
	}else{
		echo mensajes("Hubo un error al intentar modificar el estado del registro. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
	}

?>

<script type="text/javascript">
    $("#SalonesTabla").load('pages/reservaciones/salones/rsv_datosSalones.php'); 
</script>