<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    if(isset($_POST['mCodigo']) && isset($_POST['mnSalon']) && isset($_POST['mUbicacion']) && isset($_POST['mCap']) && isset($_POST['mRecursos'])) {

		$pcSalon = $_POST['mCodigo'];
		$pcNombre = $_POST['mnSalon'];
		$pcUbicacion = $_POST['mUbicacion'];
		$pcCap = $_POST['mCap'];
		$pcRecursos = $_POST['mRecursos'];

        try{
			$consulta=$db->prepare("CALL SP_RSV_EDITAR_SALON(?,?,?,?,@pcMensajeError)");
			$consulta->bindParam(1, $pcSalon, PDO::PARAM_INT);
			$consulta->bindParam(2, $pcNombre, PDO::PARAM_INT);
			$consulta->bindParam(3, $pcUbicacion, PDO::PARAM_INT);
			$consulta->bindParam(4, $pcCap, PDO::PARAM_INT);

			$resultado=$consulta->execute();
			$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
			$mensaje = $output['@pcMensajeError'];

			$sql = "DELETE FROM rsv_salon_acondicionamientos WHERE salon_id=$pcSalon;";
            $ejecutar =$db->prepare($sql);
            $ejecutar->execute();

			if ($pcRecursos!=NULL){
                foreach ($pcRecursos as $recurso) {
                    $query = "INSERT INTO `rsv_salon_acondicionamientos` (`salon_id`, `acondicionamiento_id`) VALUES ('$pcSalon', '$recurso');";
                    $result =$db->prepare($query);
                    $result->execute();
                }
            }

			if ($mensaje == NULL){
			    echo mensajes('La información del salón ha sido editada exitosamente', 'verde');
			}else{
			    echo mensajes('Error!'.$mensaje, 'rojo');
			}
		}catch(PDOexception $e){
			echo mensajes('Error!'. $e, 'rojo');
		}
	}else{
		echo mensajes("Hubo un error al intentar editar el registro. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
	}

?>

<script type="text/javascript">
    $("#SalonesTabla").load('pages/reservaciones/salones/rsv_datosSalones.php'); 
</script>