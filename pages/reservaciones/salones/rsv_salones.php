<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."conexion/config.inc.php");

    $idUsuario = $_SESSION['user_id'];
    $operacion = 2;

    $sql = "SELECT ca_tipos_acondicionamientos.id_acondicionamiento, ca_tipos_acondicionamientos.nombre FROM ca_tipos_acondicionamientos";
?>

<div class="row">
    <div class="col-lg-10">
        <h3 class="page-header panel-primary"><center><strong>Gestión de Salones</strong></center></h3>
    </div>
</div> 

<div  id="divRespuesta"></div>

<div class="row">
    <div class = "col-lg-10">
        <div class="panel panel-primary">
            <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Agregar nuevo salón</div>

            <div class="panel-body">
                <div >
                    <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden</center></div>
                </div>
                <div class="row">
                    <div class="col-lg-10">
                    <!--************************formulario*********************-->
                        <form class="form-horizontal" role="form" id="formSalones" name="formSalones">

                            <div class="row form-group" >                    
                                <label class="col-sm-2 control-label"> Nombre del Salón</label>                       
                                <div class="col-sm-6">                            
                                    <input type="text" class="form-control" id="NombreSalon" name="NombreSalon" required>      
                                </div>                  
                            </div>

                            <div class="row form-group" >                    
                                <label class="col-sm-2 control-label"> Ubicación</label>                       
                                <div class="col-sm-6">                            
                                    <input type="text" class="form-control" id="ubicacion" name="ubicacion" required>      
                                </div>                  
                            </div>

                            <div class="row form-group">                            
                                <label class="col-sm-2 control-label">Capacidad</label>
                                <div class="col-sm-1">
                                    <input type="number" id="capacidad" name="capacidad" max="100" min="1" required><span class="">
                                </div>
                            </div>

                            <div class="row form-group">
                                <label class="col-sm-2 control-label"> Recursos </label>  
                                <div class="col-sm-2">
                                    <select id="recursos" name="recursos" size="4" multiple required>
                                        <?php
                                            $result =$db->prepare($sql);
                                            $result->execute();
                                            while ($fila = $result->fetch()) {
                                        ?>
                                                <option value="<?php echo $fila['id_acondicionamiento']; ?>"><?php echo $fila["nombre"]; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>                      
                            </div>

                            <div class="row form-group">
                                <label class=" col-sm-2 control-label">Administrador</label>
                                <div class="col-sm-6">     
                                    <select class = "form-control" id="empleado" name="empleado" value="0" required>
                                        <option value = "0"> Seleccione una opción </option>
                                        <?php
                                            try{
                                                $sql = "CALL SP_RSV_DATOS_EMPLEADOS(?,?)";
                                                $query = $db->prepare($sql);
                                                $query->bindParam(1,$idUsuario,PDO::PARAM_INT);
                                                $query->bindParam(2,$operacion,PDO::PARAM_INT);

                                                $query->execute();
                                                while ($fila = $query->fetch()) {
                                        ?>
                                                    <option value="<?php echo $fila['No_Empleado']; ?>"> <?php echo $fila["nombre"]; ?></option>
                                        <?php 
                                                } //cierre del ciclo while para llenar la tabla de datos
                                            }catch(PDOException $e){
                                                echo "Error: ".$e;
                                            }
                                        ?>
                                    </select>
                                </div>                     
                            </div>

                            <div class="row">
                                <label class="control-label col-sm-2"></label>
                                <div class="col-sm-7">
                                    <p aling ="right">
                                        <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" ><span class=" glyphicon glyphicon-floppy-disk"></span> Agregar </button> 
                                    </p>          
                                </div>                            
                            </div>    

                        </form>
                    <!--************************formulario*********************-->
                    </div>
                </div>
            </div>                                    
        </div>
    </div>
</div>


<!--tabla de asignaturas-->
<div class="row">
    <div class="col-lg-10">
        <h3 class="page-header panel-primary"><center>Historial de Salones Registrados</center></h3>
    </div>
</div>

<div id="divRespuestaEditar" class="col-lg-10">
</div>

<div id="SalonesTabla" class="col-lg-10">
</div>

<!--******************modal de edicion*****************-->
<div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div id = "contenidoM">
            </div>
        </div>
    </div>
</div>

<!--******************modal de edicion*****************-->
<script type="text/javascript" src="pages/reservaciones/salones/ScriptSalones.js"></script>