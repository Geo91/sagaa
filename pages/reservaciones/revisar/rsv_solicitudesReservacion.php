<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");
    require_once("../ChekAutoIncrement.php");
    $idUsuario = $_SESSION['user_id'];
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <!--<section class="content">-->
                <div class="table-responsive">
                    <table id= "TablaRSolicitudes" border="0" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th style = "text-align:center;background-color:#386D95;color:white;">No.</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Empleado</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Unidad</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Detalle</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Operaciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Contenido de la tabla generado atravez de la consulta a 
                                la base de datos -->
                                <?php
                                    try{
                                        $sql = "CALL SP_RSV_DATOS_RSOLICITUDES(?)";
                                        $query = $db->prepare($sql);
                                        $query->bindParam(1,$idUsuario,PDO::PARAM_INT);

                                        $query->execute();
                                        while ($fila = $query->fetch()) {
                                ?>
                                        <tr data-id='<?php echo $fila["id_reservacion"]; ?>' data-emp='<?php echo $fila["Empleado"]; ?>' data-salon='<?php echo $fila["nombre_salon"]; ?>' data-res='<?php echo $fila["responsable"]; ?>' data-fechai='<?php echo date($fila["fecha_inicio"]); ?>' data-fechaf='<?php echo $fila["fecha_fin"]; ?>' data-hinicio='<?php echo $fila["horaI"]; ?>' data-hfin='<?php echo $fila["horaF"]; ?>' data-prop='<?php echo $fila["proposito"]; ?>' data-clase='<?php echo $fila["nombre"]; ?>' data-evento='<?php echo $fila["evento"]; ?>'>
                                            <td><?php echo $fila["id_reservacion"]; ?></td>
                                            <td><?php echo $fila["Empleado"]; ?></td>
                                            <td><?php echo $fila["unidad"]; ?></td>
                                            <td>
                                                <center>
                                                <div>
                                                    <button type="button" class="detalleSolicitud<?php echo $auto_increment; ?> btn btn-warning"  title="Ver detalle de la Solicitud"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                                </div>
                                                </center>
                                            </td>
                                            <td>
                                                <center>
                                                <div>
                                                    <button type="button" class="aprobarSolicitud<?php echo $auto_increment; ?> btn btn-success"  title="Aprobar la Solicitud"><i class="glyphicon glyphicon-thumbs-up"></i></button>
                                                    <button type="button" class="denegarSolicitud<?php echo $auto_increment; ?> btn btn-danger" title="Denegar la Solicitud."><i class="  glyphicon glyphicon-thumbs-down"></i></button>
                                                </div>
                                                </center>
                                            </td>             
                                        </tr>
                                <?php 
                                        } //cierre del ciclo while para llenar la tabla de datos
                                    }catch(PDOException $e){
                                        echo "Error: ".$e;
                                    }
                                ?>
                        </tbody>
                    </table>       
                </div>
            <!--</section>-->
        </div>                
    </div>
</div>

<script type="text/javascript">   
//opciones para buscador en la tabla
    $('#TablaRSolicitudes')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#TablaRSolicitudes').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {

        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });

    $(document).on("click", ".detalleSolicitud<?php echo $auto_increment; ?>", function () {  
        data = {
            idR: $(this).parents("tr").data("id"),
            empl: $(this).parents("tr").data("emp"),
            salon: $(this).parents("tr").data("salon"),
            resp: $(this).parents("tr").data("res"),
            fInicio: $(this).parents("tr").data("fechai"),
            fFin: $(this).parents("tr").data("fechaf"),
            hInicio: $(this).parents("tr").data("hinicio"),
            hFin: $(this).parents("tr").data("hfin"),
            prop: $(this).parents("tr").data("prop"),
            clase: $(this).parents("tr").data("clase"),
            event: $(this).parents("tr").data("evento")
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: detalleSolic,
            timeout: 15000,
            error: problemas
        }); 
        return false; 
    });

    $(document).on("click", ".aprobarSolicitud<?php echo $auto_increment; ?>", function () {  
        data = {
            idR: $(this).parents("tr").data("id")
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: aprobarSR,
            timeout: 15000,
            error: problemas
        }); 
        return false; 
    });

    $(document).on("click", ".denegarSolicitud<?php echo $auto_increment; ?>", function () {    
        data = {
            idR: $(this).parents("tr").data("id")
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: denegarSR,
            timeout: 15000,
            error: problemas
        }); 
        return false; 
    });

</script>