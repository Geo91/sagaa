<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    $vCod = $_POST['idR'];
    $vEmpleado = $_POST['empl'];
    $vNomSalon = $_POST['salon'];
    $vResponsable = $_POST['resp'];
    $vFInicio = $_POST['fInicio'];
    $vFFin = $_POST['fFin'];
    $vHInicio = $_POST['hInicio'];
    $vHFin = $_POST['hFin'];
    $vProp = $_POST['prop'];
    $vClase = $_POST['clase'];
    $vEvento = $_POST['event'];

?>

<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3 class="modal-title" style="color:white">Información detallada de la solicitud</h3>
</div>

<div class="modal-body">
    <form class="form-horizontal" role="form" id="formVer" name="formVer">

	    <div class="form-group">
	        <label style="display : none">Codigo de la Solicitud</label>
	        <input style="display : none" id="codPermiso" disabled = "true" value="<?php echo $vCod; ?>" class="form-control">
	    </div>

	    <div class="form-group">
	        <label class="col-sm-8 control-label" style="text-align:left">Empleado: <?php echo $vEmpleado; ?></label>

	        <label class="col-sm-10 control-label" style="text-align:left">Salón: <?php echo $vNomSalon; ?></label>

	        <label class="col-sm-10 control-label" style="text-align:left">Responsable: <?php echo $vResponsable; ?></label>

	        <label class="col-sm-8 control-label" style="text-align:left">Fecha Inicial: <?php echo $vFInicio; ?></label>

	        <label class="col-sm-8 control-label" style="text-align:left">Fecha Final: <?php echo $vFFin; ?></label>

	        <label class="col-sm-8 control-label" style="text-align:left">Horario: <?php echo $vHInicio." - ".$vHFin; ?></label>

	        <label class="col-sm-8 control-label" style="text-align:left">Proposito: <?php if($vProp == 'Clase'){
                                                echo "Clase - ".$vClase; }else{ echo "Evento - ".$vEvento; }?> </label>
	    </div>

	    <div class="modal-footer">
			<button type="button"  class="btn btn-default" data-dismiss="modal">Volver</button>
		</div>
	</form>
</div>