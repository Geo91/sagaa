<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");
    session_start();
    $idUsuario = $_SESSION['user_id'];

    if(isset($_POST['nemp'])){

		$pcEmpleado = $_POST['nemp'];

        try{
			$consulta=$db->prepare("CALL SP_RSV_DAR_PRIVILEGIOS(?,?,@pcMensajeError)");
			$consulta->bindParam(1, $pcEmpleado, PDO::PARAM_INT);
			$consulta->bindParam(2, $idUsuario, PDO::PARAM_INT);

			$resultado=$consulta->execute();
			$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
			$mensaje = $output['@pcMensajeError'];

			if ($mensaje == NULL){
			    echo mensajes('Los privilegios de administrador fueron asignados exitosamente', 'verde');
			}else{
			    echo mensajes('Error! '.$mensaje, 'rojo');
			}
		}catch(PDOexception $e){
			echo mensajes('Error!'. $e, 'rojo');
		}
	}else{
		echo mensajes("Hubo un error al intentar otorgar privilegios de administrador a este empleado. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
	}

?>

<script type="text/javascript">
	$("#tablaAdministracion").load('pages/reservaciones/administrar/rsv_datosAdministradores.php');
	$("#empleado").reset();
</script>