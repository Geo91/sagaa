<?php

    $maindir = "../../../";
    include($maindir."conexion/config.inc.php");
    require_once("../ChekAutoIncrement.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");
    $idUsuario = $_SESSION['user_id'];
    $operacion = 1;

?>

<!-- Seccion usada para mostrar la tabla de las solicitudes de permiso que estan pendientes de revisión en el sistema -->
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong>Administradores de Espacios</strong></center></h3>
    </div>
</div>

<div id = "notificaciones"></div>


<div class="panel panel-primary">
    <div class="panel-heading">
        <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Asignación de nuevos administradores </label>
    </div>
    <div class="panel-body">
        <div>
            <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor seleccione al empleado que desea agregar como administrador</center></div>
        </div>

        <div class="row">
            <div class="col-lg-10">
            <!--************************formulario*********************-->
                <form class="form-horizontal" role="form" id="formAsignar" name="formAsignar">

                    <div class = "row form-group">
                        <label id = "lbEmpleado" class=" col-sm-2 control-label"> Empleado </label>     
                        <div class="col-sm-4">                   
                            <select class = "form-control" id="empleado" name="empleado" value="0" required>
                                <option value = "0"> Seleccione una opción </option>
                                <?php
                                    try{
                                        $sql = "CALL SP_RSV_DATOS_EMPLEADOS(?,?)";
                                        $query = $db->prepare($sql);
                                        $query->bindParam(1,$idUsuario,PDO::PARAM_INT);
                                        $query->bindParam(2,$operacion,PDO::PARAM_INT);

                                        $query->execute();
                                        while ($fila = $query->fetch()) {
                                ?>
                                            <option value="<?php echo $fila['No_Empleado']; ?>"> <?php echo $fila["nombre"]; ?></option>
                                <?php 
                                        } //cierre del ciclo while para llenar la tabla de datos
                                    }catch(PDOException $e){
                                        echo "Error: ".$e;
                                    }
                                ?>
                            </select>
                        </div>                  
                    </div>

                    <div class="row">
                        <label class="control-label col-sm-2"></label>
                        <div class="col-sm-4">
                            <p aling ="right">
                                <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" ><span class="glyphicon glyphicon-floppy-disk"> Agregar </span></button> 
                            </p>          
                        </div>                            
                    </div>    

                </form>
            <!--************************formulario*********************-->
            </div>
        </div>
    </div>
</div>


<!--************************TABLA DE DATOS************************-->
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center> Administradores Registrados </center></h3>
    </div>
</div>

<div id="divRespuesta">
</div>

<div id="tablaAdministracion">
</div>

<script type="text/javascript" src="pages/reservaciones/administrar/ScriptAdministrar.js"></script>