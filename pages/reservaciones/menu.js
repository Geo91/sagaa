/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

	var y;
	y=$(document);
	y.ready(inicio);

	function inicio(){
		var x;
		x=$("#solicitar");
		x.click(cargarFormularioSolicitud);

		var x1;
		x1=$("#hsolicitudes");
		x1.click(cargarDatosSolicitudes);
		
		var x2;
		x2=$("#revisar");
		x2.click(cargarFormularioRevision);
	  
		var x3;
		x3=$("#asignar");
		x3.click(cargarFormularioAsignacion);
		
		var x4;
		x4=$("#salones");
		x4.click(cargarFormularioSalones);
		
		var x5;
		x5=$("#acondicionamiento");
		x5.click(cargarFormularioAcondicionamientos);		

		var x6;
		x6=$("#editar");
		x6.click(cargarFormularioEditar);

	}

	function inicioEnvio(){
		var x=$("#contenedor");
		x.html('Cargando...');
	}
	
	function cargarFormularioSolicitud(){
		$("#contenedor").empty();
		$("#contenedor").load('pages/reservaciones/solicitud/rsv_solicitud.php');
	}

	function cargarDatosSolicitudes(){
		$("#contenedor").empty();
		$("#contenedor").load('pages/reservaciones/edicion/rsv_mSolicitudes.php');
	}

	function cargarFormularioRevision(){
		$("#contenedor").empty();
		$("#contenedor").load('pages/reservaciones/revisar/rsv_revisarSolicitudes.php');
	}

	function cargarFormularioAsignacion(){
		$("#contenedor").empty();
		$("#contenedor").load('pages/reservaciones/administrar/rsv_administracion.php');
	}

	function cargarFormularioSalones(){
		$("#contenedor").empty();
		$("#contenedor").load('pages/reservaciones/salones/rsv_salones.php');
	}

	function cargarFormularioAcondicionamientos(){
		$("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/TiposAcondicionamiento/TiposAcondicionamiento.php");
	}

	function cargarFormularioEditar(){
		$("#contenedor").empty();
        $("#contenedor").load("pages/reservaciones/rsv_editarSolicitudcontenerconten.php");
	}

