<?php
	$maindir = "../../";
	require_once($maindir . "conexion/config.inc.php");
	$idUsuario = $_SESSION['user_id'];
	$idRol = $_SESSION['user_rol'];

	$sql = "SELECT * FROM rsv_administrador WHERE n_empleado IN (SELECT usuario.No_Empleado FROM usuario where id_Usuario = '$idUsuario')";
	$query = $db->prepare($sql);
	$query -> execute();
	$count = $query->rowCount();
?>

<div class="col-sm-2">
	   <ul class="list-unstyled">
	   
	   <?php
     		$url = 'pages/reservaciones/rsv_principal.php';
     		echo <<<HTML
     			<h5><li>
                	<a id="inicio" href="javascript:ajax_('$url');"><i class="fa fa-home"></i> Inicio</a>
            	</li></h5>
            	<br>
HTML;
		?>

		<h5><li>
			<a id = "solicitar" href="#"><i class="glyphicon glyphicon-paste"></i> Solicitar Salón</a>
		</li></h5>
		
		<h5><li>
			<a id = "hsolicitudes" href="#"><i class="fa fa-folder-open"></i> Mis Solicitudes</a>
		</li></h5>

		<?php
			if($count > 0){
				echo <<<HTML
					<h5><li>
						<a id = "revisar" href="#"><i class="glyphicon glyphicon-check"></i> Revisar Solicitudes</a>
					</li></h5>
HTML;
			}
		?>
	    <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#userMenu"> 

			<?php

				if($rol == 60 or $rol == 100){
					echo "<li><h5><i class='glyphicon glyphicon-cog'></i> Mantenimiento 
					          <i class='glyphicon glyphicon-chevron-down'></i></h5>";

					echo "<ul class='list-unstyled collapse' id='userMenu'>";
						if($count > 0){
							echo "<li><a id='asignar' href='#'>Asignar Administrador</a></li>";
						}
						
						echo "<li><a id='salones' href='#'>Salones</a></li>
					        	<li><a id='acondicionamiento' href='#'>Acondicionamientos</a></li>";
						
							   
					echo "</ul></li>";
				}	
				
			?>
		</li>
                <!-- /.sidebar-collapse -->
 </div> 
			
 <script type="text/javascript" src="pages/reservaciones/menu.js" ></script> 
