<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    include($maindir."conexion/config.inc.php");

?>


<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong>Reservación de Salones</strong></center></h3>
    </div>
</div> 

<div  id="divRespuesta"></div>

<div class="col-lg-12">       
    <div class="panel panel-primary">
        <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span><strong> Salones Disponibles </strong></div>

        <div class="panel-body" align = "center">
            <div id = "tablaSalones">
                <?php require_once("rsv_datos_salones.php"); ?>
        	</div>
        </div>
    </div>
</div>

<!-- Modal para visualizar los recursos disponibles en el salón -->
<div class="modal fade" id="modalRecursos" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div id="contenidoModal">
            </div>
        </div>
    </div>
</div>

<!-- Modal del formulario de solicitud de salones -->
<div class="modal fade" id="modalSolicitud" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div id="contenidoModal2">
            </div>
        </div>
    </div>
</div>

<!-- Modal del aviso previo a solicitar reservación de salones -->
<div class="modal fade" id="modalInformativo" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div id="contenidoModal3">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="pages/reservaciones/solicitud/ScriptReservaciones.js"></script>