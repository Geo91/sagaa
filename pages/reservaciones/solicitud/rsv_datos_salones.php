<?php
	$maindir = "../../../";
	require_once("../ChekAutoIncrement.php");
	require_once($maindir."conexion/config.inc.php");

	$idUsuario = $_SESSION["user_id"];
	$rol = $_SESSION["user_rol"];

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <!--<section class="content">-->
                <div class="table-responsive">
                    <table id= "TablaDatosSalones" border="0" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th style = 'display:none'>Codigo</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th> 
                                <th style="text-align:center;background-color:#386D95;color:white;">Capacidad</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Ubicación</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Recursos</th> 
                                <th style="text-align:center;background-color:#386D95;color:white;">Seleccionar</th>          
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Contenido de la tabla generado atravez de la consulta a 
                                la base de datos -->
                                <?php
                                    try{
                                        $sql = "SELECT * from rsv_salones WHERE habilitado = '1'";
                                        $query = $db->prepare($sql);
                                        $query->execute();
                                        while ($fila = $query->fetch()) {
                                ?>
                                        <tr data-id='<?php echo $fila["id_salon"]; ?>' data-salon='<?php echo $fila["nombre_salon"]; ?>'>
                                            <td style = "display:none"><?php echo $fila["id_salon"]; ?></td>
                                            <td><?php echo $fila["nombre_salon"]; ?></td>
                                            <td><?php echo $fila["capacidad"]; ?></td>
                                            <td><?php echo $fila["ubicacion"]; ?></td>
                                            <td>
                                                <center>
                                                    <button type="button" class="recursos<?php echo $auto_increment; ?> btn btn-warning glyphicon glyphicon-list"  title="Ver información de los recursos disponibles en el salón."></button>
                                                </center>
                                            </td>
                                            <td>
                                                <center>
                                                    <button type="button" class="solicitarSalon<?php echo $auto_increment; ?> btn btn-success glyphicon glyphicon-share"  title="Seleccionar este salón para solicitud."></button>
                                                </center>
                                            </td>         
                                        </tr>
                                <?php 
                                        } //cierre del ciclo while para llenar la tabla de datos
                                    }catch(PDOException $e){
                                        echo "Error: ".$e;
                                    }
                                ?>
                        </tbody>
                    </table>       
                </div>
            <!--</section>-->
        </div>                
    </div>
</div>

<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#TablaDatosSalones')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#TablaDatosSalones').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });

    $(document).on("click", ".recursos<?php echo $auto_increment; ?>", function () {
        data={
            id:$(this).parents("tr").data("id")
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: verRecursos,
            timeout: 6000,
            error: problemas
        }); 
        return false;
    });

    $(document).on("click", ".solicitarSalon<?php echo $auto_increment; ?>", function () {
        data={
            id:$(this).parents("tr").data("id"),
            nombre:$(this).parents("tr").data("salon")
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: solicitarSalon,
            timeout: 6000,
            error: problemas
        }); 
        return false;
        
    });

</script>