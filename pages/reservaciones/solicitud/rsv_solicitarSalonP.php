<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    $vCodS = $_POST['codigo'];
    $vnmS = $_POST['nom'];
    $vFI = $_POST['fInicio'];
    $vFF = $_POST['fFin'];
    $vHI = $_POST['hInicio'];
    $vHF = $_POST['hFin'];

    $sql = "SELECT ca_asignaturas.id_asignatura, ca_asignaturas.nombre FROM ca_asignaturas";

?>

<div class="modal-header" style = "background-color:#0FA6C3">
  	<button type="button" class="close" data-dismiss="modal">&times;</button>
  	<h4 class="modal-title" style = "color:white">Solicitud de Uso de Salón: <br> <?php echo $vnmS; ?> </h4>
</div>

<div class="modal-body">

	    <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden<br>En caso de usar Mozilla Firefox el formato de fecha debe ser 'año-mes-día' y el formato de hora debe ser de 24 hrs. Ejem: '14:00:00'</center>
	    </div>
	    <!--************************formulario*********************-->           
	        <form class="form-horizontal" role="form" id="formSolicitudSalonP" name="formSolicitudSalonP">

				<div class="row form-group">
				    <div class = "col-sm-2">
				        <input type = "text" id = "codigoSalon" value="<?php echo $vCodS; ?>" hidden="true" disabled="true">
				        <input type = "text" id = "nmSalon" value="<?php echo $vnmS; ?>" hidden="true" disabled="true">
				        <input type="date" id="fechaInicio" name="datepicker" value="<?php echo $vFI ?>" hidden="true" required>
				        <input type="date" id="fechaFin" name="datepicker" value="<?php echo $vFF ?>" hidden="true" required>
				        <input type="time" id="horaIn" name="horaIn" value="<?php echo $vHI ?>" hidden="true" required>
				        <input type="time" id="horaFin" name="horaIn" value="<?php echo $vHF ?>" hidden="true" required>
				    </div>
				</div>

	            <div class="row form-group">
	                <label class="col-sm-2 control-label"> Responsable </label>  
	                <div class="col-sm-4">
                        <input type="text" class="form-control" id="responsable" name="responsable" title = "Ingrese el nombre de la persona responsable de la actividad" required>
                    </div>

                    <label class ="col-sm-2 control-label"> #Empleado </label>
				    <div class = "col-sm-1">
				        <input type = "text" id = "numEmp" title = "Ingrese el número de empleado de la persona responsable de la actividad" required>
				    </div>          
	            </div>

	            <div class="row form-group">
	                <label class="col-sm-2 control-label"> Celular </label>  
	                <div class="col-sm-4">
                        <input type="text" class="form-control" id="celular" name="celular" title = "Ingrese un número de celular sin guiones" placeholder="########">
                    </div>                
	            </div>

	            <div class="row form-group">
	                <label class=" col-sm-2 control-label">Unidad</label>
                    <div class="col-sm-6">     
                        <input type="text" class="form-control" id="unidad" name="unidad" title = "Ingrese el nombre de la unidad responsable de la actividad" required>         
                    </div>
	            </div>

	            <div class="row form-group">
	                <label class="col-sm-2 control-label"> Propósito </label>  
	                <div class="col-sm-3">
	                    <select id="proposito" name="proposito" size="3" required>
	                        <option value="0"> Clase </option>
	                        <option value="1"> Evento </option>
	                        <option value="2"> Clase Externa </option>
	                    </select>
	                </div>                      
	            </div>

	            <div class="row form-group">
	                <label class=" col-sm-2 control-label" id="lbclase" hidden = "true">Clase</label>
                    <div class="col-sm-6" id = "dclase" hidden = "true">     
                        <select class="form-control" id="clase" name="clase">
                            <option value="0">Seleccione una opción</option>
                            <?php
                                $resultado = $db->prepare($sql);
                                $resultado->execute();
                                while ($fila = $resultado->fetch()) {
                            ?> 
                                    <option value="<?php echo $fila['id_asignatura']; ?>"> <?php echo $fila["nombre"]; ?></option>
                            <?php
                                }
                            ?>
                        </select>                         
                    </div>                     

	                <label class="col-sm-2 control-label" id="lbevento" hidden = "true">Evento</label>
                    <div class="col-sm-6" id="devento" hidden = "true">              
                        <input type="text" class="form-control" id="evento" name="evento" >
                    </div>                      

	                <label class="col-sm-2 control-label" id="lbexterna" hidden = "true">Clase</label>
                    <div class="col-sm-6" id="dexterna" hidden = "true">              
                        <input type="text" class="form-control" id="externa" name="externa" >
                    </div>                      
	            </div>

	            <div class="modal-footer">
	                <button id="RegresarFormulario" class="btn btn-info"><span class="glyphicon glyphicon-circle-arrow-left"></span> Regresar </button>
	                <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" > Enviar <span class="glyphicon glyphicon-circle-arrow-right"></span></button>    
	            </div>    

	        </form>
	    <!--************************formulario*********************-->
</div>

<script type="text/javascript">
	$(document).ready(function(){ 

	    $("#RegresarFormulario").click(function(e) {
	        e.preventDefault();
	        data={
	            id:$('#codigoSalon').val(),
	            nombre:$('#nmSalon').val(),
	            fInicio:$('#fechaInicio').val(),
	            fFin:$('#fechaFin').val(),
	            hInicio:$('#horaIn').val(),
	            hFin:$('#horaFin').val()
	        }
	        $.ajax({
	            async: true,
	            type: "POST",
	            dataType: "html",
	            contentType: "application/x-www-form-urlencoded",
	            success: pasoUno,
	            timeout: 6000,
	            error: problemas
	        });
	        return false;
	    });

	    function pasoUno(){
	    	$("#contenidoModal2").html('');
    		$("#contenidoModal2").load('pages/reservaciones/solicitud/rsv_solicitarSalon.php',data);
	    }

	});

</script>

<script type="text/javascript" src="pages/reservaciones/solicitud/ScriptReservaciones.js"></script>