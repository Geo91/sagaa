<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    $pcCodigo = $_POST['id'];
    $pcSalon = $_POST['nombre'];

    if(isset($_POST['fInicio']) && isset($_POST['fFin']) && isset($_POST['hInicio']) && isset($_POST['hFin'])){
    	$cdg = 1;
    	$fI = $_POST['fInicio'];
    	$fF = $_POST['fFin'];
    	$hI = $_POST['hInicio'];
    	$hF = $_POST['hFin'];
    }else{
    	$cdg = 0;
    }

    $sql = "SELECT ca_asignaturas.id_asignatura, ca_asignaturas.nombre FROM ca_asignaturas";
    $sql2 = "SELECT departamento_laboral.Id_departamento_laboral, departamento_laboral.nombre_departamento FROM departamento_laboral";

?>

<div class="modal-header" style = "background-color:#0FA6C3">
  	<button type="button" class="close" data-dismiss="modal">&times;</button>
  	<h4 class="modal-title" style = "color:white">Solicitud de Uso de Salón: <br> <?php echo $pcSalon; ?> </h4>
</div>

<div class="modal-body">

	    <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden<br>En caso de usar Mozilla Firefox el formato de fecha debe ser 'año-mes-día' y el formato de hora debe ser de 24 hrs. Ejem: '14:00:00'</center>
	    </div>
	    <!--************************formulario*********************-->           
	        <form class="form-horizontal" role="form" id="formSolicitudSalon" name="formSolicitudSalon">

				<div class="row form-group">
				    <label class ="col-sm-2 control-label" hidden="true">Código</label>
				    <div class = "col-sm-2">
				        <input type = "text" id = "codigoSalon" value="<?php echo $pcCodigo; ?>" hidden="true" disabled="true">
				        <input type = "text" id = "nmSalon" value="<?php echo $pcSalon; ?>" hidden="true" disabled="true">
				    </div>
				</div>

	            <div class="row form-group"> 
	                <label class="col-sm-2 control-label">Fecha Inicio</label>
	                <div class="col-sm-4">
	                    <input type="date" id="fechaInicio" name="datepicker" <?php if($cdg == 1){ echo "value='".$fI."'"; } ?> required><span class=""></span>
	                </div>               

	                <label class="col-sm-2 control-label">Fecha Fin</label>
	                <div class="col-sm-2">
	                    <input type="date" id="fechaFin" name="datepicker" <?php if($cdg == 1){ echo "value='".$fF."'"; } ?> required><span class=""></span>
	                </div>
	            </div>

	            <div class="row form-group">                    
	                <label class="control-label col-sm-2">Hora Inicio</label>
	                <div class="col-sm-4">
	                    <input type="time" id="horaIn" name="horaIn" <?php if($cdg == 1){ echo "value='".$hI."'"; } ?> required><span class=""></span>
	                </div>

	                <label class="control-label col-sm-2">Hora Fin</label>
	                <div class="col-sm-2">
	                    <input type="time" id="horaFin" name="horaFin" <?php if($cdg == 1){ echo "value='".$hF."'"; } ?> required><span class=""></span>
	                </div>
	            </div>

	            <div class="modal-footer">
	                <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
	                <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" > Siguiente <span class="glyphicon glyphicon-circle-arrow-right"></span></button>
	            </div>

	        </form>
	    <!--************************formulario*********************-->
</div>

<script type="text/javascript">
	$(document).ready(function(){ 

	    $("#formSolicitudSalon").submit(function(e) {
	        e.preventDefault();
	        data={
	            codigo:$('#codigoSalon').val(),
	            nom:$('#nmSalon').val(),
	            fInicio:$('#fechaInicio').val(),
	            fFin:$('#fechaFin').val(),
	            hInicio:$('#horaIn').val(),
	            hFin:$('#horaFin').val()
	        }
	        $.ajax({
	            async: true,
	            type: "POST",
	            dataType: "html",
	            contentType: "application/x-www-form-urlencoded",
	            success: pasoDos,
	            timeout: 6000,
	            error: problemas
	        });
	        return false;
	    });

	    function pasoDos(){
	    	$("#contenidoModal2").html('');
    		$("#contenidoModal2").load('pages/reservaciones/solicitud/rsv_solicitarSalonP.php',data);
	    }

	});

</script>