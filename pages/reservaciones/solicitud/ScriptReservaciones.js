
$(document).ready(function(){

    // Funciones de inserción de Solicitud
    $("#proposito").click(function(event) {
        event.preventDefault();
        var valor = $('#proposito').val();
        if (valor==1) {
            $('#evento').val('');
            $('#lbevento').show('fast');
            $('#devento').show('fast');
            $('#lbclase').hide('fast');
            $('#dclase').hide('fast');
            $('#clase').val('NULL');
            $('#lbexterna').hide('fast');
            $('#dexterna').hide('fast');
            $('#externa').val('NULL');
            document.getElementById("evento").disabled = false;
            document.getElementById("clase").disabled = true;
            document.getElementById("externa").disabled = true;
        }else if(valor==2){
            $('#lbclase').hide('fast');
            $('#dclase').hide('fast');
            $('#lbevento').hide('fast');
            $('#devento').hide('fast');
            $('#evento').val('NULL');
            $('#lbexterna').show('fast');
            $('#dexterna').show('fast');
            $('#externa').val('');
            document.getElementById("clase").disabled = true;
            document.getElementById("evento").disabled = true;
            document.getElementById("externa").disabled = false;
        }else{
            $('#lbclase').show('fast');
            $('#dclase').show('fast');
            $('#lbevento').hide('fast');
            $('#devento').hide('fast');
            $('#evento').val('NULL');
            $('#lbexterna').hide('fast');
            $('#dexterna').hide('fast');
            $('#externa').val('NULL');
            document.getElementById("clase").disabled = false;
            document.getElementById("evento").disabled = true;
            document.getElementById("externa").disabled = true;
        }
    });

    $("#formSolicitudSalonP").submit(function(e) {
        e.preventDefault();
        data={
            cod:$('#codigoSalon').val(),
            fIn:$('#fechaInicio').val(),
            fFi:$('#fechaFin').val(),
            hIn:$('#horaIn').val(),
            hFi:$('#horaFin').val(),
            respon:$('#responsable').val(),
            nEmp:$('#numEmp').val(),
            unit:$('#unidad').val(),
            cel:$('#celular').val(),
            propst:$('#proposito').val(),
            clase:$('#clase').val(),
            evento:$('#evento').val(),
            ext:$('#externa').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: abrirAviso,
            timeout: 10000,
            error: problemas
        });
        return false;
    });

    $("#formConfirmacion").submit(function(e) {
        e.preventDefault();
        data={
            codigo:$('#codigoC').val(),
            fInicio:$('#fiC').val(),
            fFin:$('#ffC').val(),
            hInicio:$('#hiC').val(),
            hFin:$('#hfC').val(),
            respon:$('#respC').val(),
            nEmp:$('#empC').val(),
            unit:$('#uniC').val(),
            cel:$('#celularC').val(),
            propost:$('#propC').val(),
            cls:$('#claseC').val(),
            evnt:$('#eventoC').val(),
            extr:$('#externaC').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: confirmado,
            timeout: 10000,
            error: problemas
        });
        return false;
    });

    //Fin de Funciones de solicitud de salón
    
});

function verRecursos(){
    $("#contenidoModal").load('pages/reservaciones/solicitud/rsv_verRecursos.php',data);
    $("#modalRecursos").modal('show');
}

function solicitarSalon(){
    $("#contenidoModal2").load('pages/reservaciones/solicitud/rsv_solicitarSalon.php',data);
    $("#modalSolicitud").modal('show');
    $("#divRespuesta").empty();
}

function abrirAviso(){
    $("#divRespuesta").empty();
    $("#contenidoModal3").load('pages/reservaciones/solicitud/rsv_aviso.php',data);
    $("#modalInformativo").modal('show');
}

function denegarSolicitud(){
    $("#contenidoModal").load('pages/permisos/revision/denegarSolicitud.php',data);
    $("#modalInformativo").modal('show');
}

function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function confirmado(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/reservaciones/solicitud/rsv_solicitudConfirmada.php',data);
    $("#modalInformativo").modal('hide');
    $("#modalSolicitud").modal('hide');
}

