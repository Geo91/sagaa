<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    if(isset($_POST['cod']) && isset($_POST['fIn']) && isset($_POST['fFi']) && isset($_POST['hIn']) && isset($_POST['hFi']) && isset($_POST['respon']) && isset($_POST['nEmp']) && isset($_POST['unit']) && isset($_POST['cel']) && isset($_POST['propst']) && isset($_POST['clase']) && isset($_POST['evento']) && isset($_POST['ext'])) {

        $vCodigo = $_POST['cod'];
        $vFI = $_POST['fIn'];
        $vFF = $_POST['fFi'];
        $vHI = $_POST['hIn'];
        $vHF = $_POST['hFi'];
        $vRp = $_POST['respon'];
        $vEmp = $_POST['nEmp'];
        $vUn = $_POST['unit'];
        $vCel = $_POST['cel'];
        $vP = $_POST['propst'];
        $vCl = $_POST['clase'];
        $vEv = $_POST['evento'];
        $vExt = $_POST['ext'];

    }

?>

<div class="modal-header" style = "background-color:#FFBF00">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3 class="modal-title" style="color:white"><center>¡AVISO!</center></h3>
</div>

<div class="modal-body">
	<div id= "noti1" class="col-lg-14 alert alert-warning" role="alert"><center>El envío de la información ingresada en este formulario no representa garantía en lo que respecta al uso del salón solicitado.<br><br> Toda solicitud está sujeta a revisión por parte del encargado del espacio solicitado, quién podrá autorizar o denegar el uso del mismo.<br><br> Para monitorear el estado de su solicitud acceda a la sección "Mis Solicitudes".</center>
    </div>

    <form class="form-horizontal" role="form" id="formConfirmacion" name="formConfirmacion">

        <div class="row form-group" hidden="true">
            <label class ="col-sm-2 control-label" >Código</label>
            <div class = "col-sm-2">
                <input type = "text" id = "codigoC" value="<?php echo $vCodigo; ?>"  disabled="true">
                <input type="date" id="fiC" name="datepicker" value = "<?php echo $vFI; ?>">
                <input type="date" id="ffC" name="datepicker" value = "<?php echo $vFF; ?>">
                <input type="time" id="hiC" name="horaIn" value = "<?php echo $vHI; ?>">
                <input type="time" id="hfC" name="horaFin" value = "<?php echo $vHF; ?>">
                <input type="text" id="respC" name="responsable" value = "<?php echo $vRp; ?>">
                <input type="text" id="empC" name="numEmpleado" value = "<?php echo $vEmp; ?>">
                <input type="text" id="celularC" name="celular" value = "<?php echo $vCel; ?>">
                <input type="text" id="uniC" name="unidad" value = "<?php echo $vUn;  ?>">
                <input type="text" id="propC" name="proposito" value = "<?php echo $vP; ?>">
                <input type="text" id="claseC" name="clase" value = "<?php echo $vCl;  ?>">     
                <input type="text" id="eventoC" name="evento" value = "<?php echo $vEv; ?>">
                <input type="text" id="externaC" name="externa" value = "<?php echo $vExt; ?>">
            </div>
        </div>


        <div class="modal-footer">
            <button type="button"  class="btn btn-default" data-dismiss="modal">Volver</button>
            <button type="submit" class="btn btn-primary col-sm-offset-10" > Aceptar </button>
        </div>    

    </form>
</div>



<script type="text/javascript" src="pages/reservaciones/solicitud/ScriptReservaciones.js"></script>