<?php


  $maindir = "../";

  if(isset($_GET['contenido']))
    {
      $contenido = $_GET['contenido'];
    }
  else
    {
      $contenido = 'poa';
    }

  require_once($maindir."funciones/check_session.php");

  require_once($maindir."funciones/timeout.php");

  require_once($maindir."pages/navbar.php");

 require_once($maindir . "conexion/config.inc.php");


    include '../Datos/conexion.php';
    $consultaAR = "SELECT * FROM `actividades` WHERE `fecha_fin` <= now()";
    $query1 = $db->prepare($consultaAR);
    $query1->execute();
    $consultaAA = "SELECT * FROM `actividades` WHERE `fecha_fin` >= now() AND `fecha_fin` <= ADDDATE(NOW(), INTERVAL 7 DAY )";
    $query2 = $db->prepare($consultaAA);
    $query2->execute();
    $consultaAV = "SELECT * FROM `actividades` WHERE `fecha_fin` >= ADDDATE(NOW(), INTERVAL 5 DAY )";
    $query3 = $db->prepare($consultaAV);
    $query3->execute();
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/style_info-box.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
    <script type="text/javascript" src="js/menu.js" ></script>
    <!--icons-->
    <link href="css/elegant-icons-style.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
  </head>
  <body>

<div id="wrapper">

    <div class="navbar-default sidebar" role="navigation" >
        <div class="sidebar-nav navbar-collapse" >
            <ul class="nav" id="side-menu">
              <li>
                  <a id="planEstrategico" href="#" style="color:#386D95;"><i class="fa fa-table fa-fw"></i>Plan Estratégico</a>
              </li>
                <li>
                    <a id="crear" href="#" style="color:#386D95;"><i class="fa fa-table fa-fw"></i>POAs</a>
                </li>
                <li>
                    <a id="actividades" href="#" style="color:#386D95;"><i class="fa fa-dashboard fa-fw"></i>Mis Actividades</a>
                </li>
                <li>
                    <a  id="reportes" href="#" style="color:#386D95;"><i class="fa fa-file-pdf-o fa-fw"></i> Reportes</a>

                </li>
                <li>
                    <a  id="estadisticas" href="#" style="color:#386D95;"><i class="fa fa-bar-chart-o fa-fw"></i> Estadisticas</a>

                </li>
                <li data-popover="true" rel="popover" data-placement="right"><a href="#" data-target=".premium-menu" class="nav-header collapsed" data-toggle="collapse" style="color:#386D95;"><i class="fa fa-gears fa-fw"></i>Mantenimiento<i class="fa fa-collapse"></i></a></li>
                <li><ul class="premium-menu nav nav-list collapse">
                    <li ><a id="areas" href="premium-profile.html" style="color:#386D95;"><span class="fa fa-caret-right"></span> Áreas</a></li>
                    <li ><a id="tipoDeAreas" href="premium-blog.html" style="color:#386D95;"><span class="fa fa-caret-right"></span> Tipos De Áreas</a></li>
                </ul>
            </li>


        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>

</div>
<div id="page-wrapper">


    <div id="contenedor">

        <h1 class="page-header"><i class="fa fa-desktop" aria-hidden="true"></i> Administración de POA</h1>
        <div class="row">
    <div class="col-lg-3 col-md-3 col-sm-10 col-xs-10">
      <div class="info-box blue-bg">
        <i class="fa fa-briefcase" aria-hidden="true"></i>
        <div class="count">L6,674</div>
        <div class="title">Presupuesto</div>
      </div><!--/.info-box-->
    </div><!--/.col-->

    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
      <div class="info-box orange-bg">
        <i class="glyphicon glyphicon-piggy-bank"></i>
        <div class="count">L7,358</div>
        <div class="title">Disponible para Planificación</div>
      </div><!--/.info-box-->
    </div><!--/.col-->

    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
      <div class="info-box greenLight-bg">
        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
        <div class="count">L4,362</div>
        <div class="title">Utilizado</div>
      </div><!--/.info-box-->
    </div><!--/.col-->

    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
      <div class="info-box red-bg">
        <i class="fa fa-credit-card" aria-hidden="true"></i>
        <div class="count">L1,426</div>
        <div class="title">Disponible</div>
      </div><!--/.info-box-->
    </div><!--/.col-->
  </div><!--/.row-->
  <div class="row">

    <div class="col-lg-9 col-md-10">
      <div class="panel panel-default">
        <div class="panel-heading" style="color:white; background-color:#386D95;">
          <h5><strong>Porcentaje de Cumplimiento del Plan Por Dimensión Estratégica</strong></h5>

        </div>
        <div class="panel-body">
          <table id="tabla_CumpliDimension" class="table bootstrap-datatable countries">
            <thead>
              <tr>

                <th style="color:#386D95;">No.</th>
                <th style="color:#386D95;">Dimensión Estratégica</th>
                <th style="color:#386D95;">Porcentaje Alcanzado(%)</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1.</td>
                <td>Desarrollo e Innovación Curricular</td>
                <td>
                  <div class="progress progress-striped active">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="73" aria-valuemin="0" aria-valuemax="100" style="width: 73%">
                    </div>
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="27" aria-valuemin="0" aria-valuemax="100" style="width: 27%">
                      </div>
                  </div>
                  <span class="sr-only">73%</span>
                </td>
                <td>73%</td>
              </tr>
              <tr>

                <td>2.</td>
                <td>Investigación Científica</td>

                <td>
                  <div class="progress progress-striped active">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="57" aria-valuemin="0" aria-valuemax="100" style="width: 57%">
                    </div>
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="43" aria-valuemin="0" aria-valuemax="100" style="width: 43%">
                    </div>
                  </div>
                  <span class="sr-only">57%</span>
                </td>
                <td>57%</td>
              </tr>
              <tr>

                <td>3.</td>
                <td>Vinculación Universidad Sociedad</td>

                <td>
                  <div class="progress progress-striped active">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="93" aria-valuemin="0" aria-valuemax="100" style="width: 93%">
                    </div>
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="7" aria-valuemin="0" aria-valuemax="100" style="width: 7%">
                      </div>
                  </div>
                  <span class="sr-only">93%</span>
                </td>
                <td>93%</td>
              </tr>
              <tr>

                <td>4.</td>
                <td>Docencia y Profesorado Universitario</td>
                <td>
                  <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                      </div>
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                      </div>
                  </div>
                  <span class="sr-only">20%</span>
                </td>
                <td>20%</td>
              </tr>
              <tr>

                <td>5.</td>
                <td>Estudiantes</td>
                <td>
                  <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                      </div>
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                      </div>
                  </div>
                  <span class="sr-only">20%</span>
                </td>
                <td>20%</td>
              </tr>
              <tr>

                <td>6.</td>
                <td>Graduados</td>
                <td>
                  <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                      </div>
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                      </div>
                  </div>
                  <span class="sr-only">20%</span>
                </td>
                <td>20%</td>
              </tr>
              <tr>

                <td>7.</td>
                <td>Gestión del Conocimiento</td>
                <td>
                  <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-primaryr" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                      </div>
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                      </div>
                  </div>
                  <span class="sr-only">20%</span>
                </td>
                <td>20%</td>
              </tr>
              <tr>

                <td>8.</td>
                <td>Lo Esencial de la Reforma Universitaria</td>
                <td>
                  <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                      </div>
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                      </div>
                  </div>
                  <span class="sr-only">20%</span>
                </td>
                <td>20%</td>
              </tr>
              <tr>

                <td>9.</td>
                <td>Cultura e Innovación Educativa</td>
                <td>
                  <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                      </div>
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                      </div>
                  </div>
                  <span class="sr-only">20%</span>
                </td>
                <td>20%</td>
              </tr>
              <tr>

                <td>10.</td>
                <td>Aseguramiento de la Calidad y Mejoramiento Ambiental</td>
                <td>
                  <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                      </div>
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                      </div>
                  </div>
                  <span class="sr-only">20%</span>
                </td>
                <td>20%</td>
              </tr>
              <tr>

                <td>11.</td>
                <td>Posgrados</td>
                <td>
                  <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                      </div>
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                      </div>
                  </div>
                  <span class="sr-only">20%</span>
                </td>
                <td>20%</td>
              </tr>

                                <tr>
                                    <td></td>
                                    <td><strong>Total Alcanzado</strong></td>
                                    <td>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="34.81" aria-valuemin="0" aria-valuemax="34.81" style="width: 34.81%">
                                            </div>
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="95.18" aria-valuemin="0" aria-valuemax="65.18" style="width: 65.18%">
                                            </div>
                                        </div>
                                        <span class="sr-only">34.81%</span>
                                    </td>
                                    <td>34.81%</td>
                                </tr>
            </tbody>
          </table>
        </div>

      </div>

    </div><!--/col-->
  </div>


    </div>

</div>

  </body>
</html>
