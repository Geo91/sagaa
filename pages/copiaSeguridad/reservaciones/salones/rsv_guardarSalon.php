<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    if(isset($_POST['nSalon']) && isset($_POST['ubicacion']) && isset($_POST['cap']) && isset($_POST['recursos']) && isset($_POST['admin'])){

		$pcSalon = $_POST['nSalon'];
		$pcUbicacion = $_POST['ubicacion'];
		$pcCap = $_POST['cap'];
		$pcRecursos = $_POST['recursos'];
		$pcAdmin = $_POST['admin'];

        try{
			$consulta=$db->prepare("CALL SP_RSV_REGISTRAR_SALON(?,?,?,?,@pcMensajeError)");
			$consulta->bindParam(1, $pcSalon, PDO::PARAM_INT);
			$consulta->bindParam(2, $pcUbicacion, PDO::PARAM_INT);
			$consulta->bindParam(3, $pcCap, PDO::PARAM_INT);
			$consulta->bindParam(4, $pcAdmin, PDO::PARAM_INT);

			$resultado=$consulta->execute();
			$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
			$mensaje = $output['@pcMensajeError'];

			if ($pcRecursos!=NULL){
                foreach ($pcRecursos as $recurso) {
                    $query = "INSERT INTO `rsv_salon_acondicionamientos` (`salon_id`, `acondicionamiento_id`) VALUES ((SELECT MAX(rsv_salones.id_salon) FROM rsv_salones ORDER BY rsv_salones.id_salon DESC LIMIT 1), '$recurso');";
                    $result =$db->prepare($query);
                    $result->execute();
                }
            }

			if ($mensaje == NULL){
			    echo mensajes('El nuevo salón ha sido registrado exitosamente', 'verde');
			}else{
			    echo mensajes('Error!'.$mensaje, 'rojo');
			}
		}catch(PDOexception $e){
			echo mensajes('Error!'. $e, 'rojo');
		}
	}else{
		echo mensajes("Hubo un error al intentar ingresar el registro. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
	}

?>

<script type="text/javascript">
    $("#SalonesTabla").load('pages/reservaciones/salones/rsv_datosSalones.php'); 
</script>