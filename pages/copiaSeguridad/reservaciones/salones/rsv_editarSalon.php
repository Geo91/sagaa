<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");

    $Codigo = $_POST['id'];
    $nombre = $_POST['nombre'];
    $capacidad = $_POST['capacity'];
    $ubicacion = $_POST['ubicado'];


    $query = "SELECT ca_tipos_acondicionamientos.id_acondicionamiento, ca_tipos_acondicionamientos.nombre, COUNT(salon.salon_id) as recurso FROM ca_tipos_acondicionamientos LEFT JOIN (SELECT * FROM rsv_salon_acondicionamientos WHERE rsv_salon_acondicionamientos.salon_id = $Codigo) as salon on ca_tipos_acondicionamientos.id_acondicionamiento=salon.acondicionamiento_id GROUP BY ca_tipos_acondicionamientos.id_acondicionamiento";

?>

<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title" style = "color:white">Editar Salón</h4>
</div>
<div class="modal-body">
    <!--************************formulario*********************-->
        <form class="form-horizontal" role="form" id="formEditarSalon" name="formEditarSalon">

            <div class="row form-group" style = "display:none">                    
                <label class=" col-sm-2 control-label"> Código</label>         
                <div class="col-sm-4">                            
                    <input type="text" class="form-control" id="modalCodigo" name="modalCodigo" value = "<?php echo $Codigo; ?>" required disabled="true">              
                </div>                  
            </div>

            <div class="row form-group" >                    
                <label class=" col-sm-3 control-label"> Nombre del Salón</label>
                <div class="col-sm-6">                            
                    <input type="text" class="form-control" id="modalSalon" name="modalSalon" value = "<?php echo $nombre; ?>" required>      
                </div>                  
            </div>

            <div class="row form-group" >                    
                <label class=" col-sm-3 control-label"> Ubicación</label>
                <div class="col-sm-8">                            
                    <input type="text" class="form-control" id="modalUbicacion" name="modalUbicacion" value = "<?php echo $ubicacion; ?>" required>      
                </div>                  
            </div>

            <div class="row form-group">                            
                <label class="col-sm-3 control-label">Capacidad</label>
                <div class="col-sm-2">
                    <input type="number" id="modalCapacidad" name="modalCapacidad" max="100" min="1" value = "<?php echo $capacidad; ?>" required><span class="">
                </div>
            </div>

            <div class="row form-group">
                <label class="col-sm-3 control-label"> Recursos </label>  
                <div class="col-sm-1">
                    <select id="modalRecursos" name="modalRecursos" size="3" multiple required>
                        <?php
                            $resultado = $db->prepare($query);
                            $resultado->execute();
                            while ($row = $resultado->fetch()) {
                        ?>
                                <option value="<?php echo $row['id_acondicionamiento']; ?>" <?php if($row['recurso'] > 0){ echo 'selected'; } ?>> <?php echo $row["nombre"]; ?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>                      
            </div>

            <div class="modal-footer">
                <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="guardarCambios" class="btn btn-primary"> Guardar</button>
            </div>
        </form>
    <!--************************formulario*********************-->
</div>

<script type="text/javascript" src="pages/reservaciones/salones/ScriptSalones.js"></script>