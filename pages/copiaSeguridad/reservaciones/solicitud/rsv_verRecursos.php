<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    $pcPermiso = $_POST['id'];
    $sql = "SELECT * FROM ca_tipos_acondicionamientos WHERE ca_tipos_acondicionamientos.id_acondicionamiento IN (SELECT rsv_salon_acondicionamientos.acondicionamiento_id FROM rsv_salon_acondicionamientos WHERE rsv_salon_acondicionamientos.salon_id = '".$pcPermiso."');"

?>

<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title" style="color:white">Recursos Disponibles en el Salón</h4>
</div>

<div class="modal-body">
	<div class="container-fluid">
	    <div class="row">
			<div class="col-lg-12">
            <!--<section class="content">-->
                <div class="table-responsive">
                    <table id= "TablaDatosSalones" border="0" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th style = 'display:none'>Codigo</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th>           
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Contenido de la tabla generado atravez de la consulta a 
                                la base de datos -->
                                <?php
                                    try{
                                        $query = $db->prepare($sql);
                                        $query->execute();
                                        while ($fila = $query->fetch()) {
                                ?>
                                        <tr data-id='<?php echo $fila["id_acondicionamiento"]; ?>'>
                                            <td style = "display:none"><?php echo $fila["id_acondicionamiento"]; ?></td>
                                            <td><?php echo $fila["nombre"]; ?></td>         
                                        </tr>
                                <?php 
                                        } //cierre del ciclo while para llenar la tabla de datos
                                    }catch(PDOException $e){
                                        echo "Error: ".$e;
                                    }
                                ?>
                        </tbody>
                    </table>       
                </div>
            <!--</section>-->
        	</div>

</div>

<div class="modal-footer">
    <button type="button"  class="btn btn-default" data-dismiss="modal">Volver</button>
</div>
