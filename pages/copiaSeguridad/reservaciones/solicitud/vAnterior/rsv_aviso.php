<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    if(isset($_POST['codigo']) && isset($_POST['fInicio']) && isset($_POST['fFin']) && isset($_POST['hInicio']) && isset($_POST['hFin']) && isset($_POST['cel']) && isset($_POST['proposito']) && isset($_POST['clase']) && isset($_POST['evento'])) {

        $vCodigo = $_POST['codigo'];
        $vFI = $_POST['fInicio'];
        $vFF = $_POST['fFin'];
        $vHI = $_POST['hInicio'];
        $vHF = $_POST['hFin'];
        $vCel = $_POST['cel'];
        $vP = $_POST['proposito'];
        $vClase = $_POST['clase'];
        $vEvento = $_POST['evento'];
    }

?>

<div class="modal-header" style = "background-color:#FFBF00">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3 class="modal-title" style="color:white"><center>¡AVISO!</center></h3>
</div>

<div class="modal-body">
	<div id= "noti1" class="col-lg-14 alert alert-warning" role="alert"><center>El envío de la información ingresada en este formulario no representa garantía en lo que respecta al uso del salón solicitado.<br><br> Toda solicitud está sujeta a revisión por parte del encargado del espacio solicitado, quién podrá autorizar o denegar el uso del mismo.<br><br> Para monitorear el estado de su solicitud acceda a la sección "Mis Solicitudes".</center>
    </div>

    <form class="form-horizontal" role="form" id="formConfirmacion" name="formConfirmacion">

        <div class="row form-group" hidden="true">
            <label class ="col-sm-2 control-label" >Código</label>
            <div class = "col-sm-2">
                <input type = "text" id = "codigoC" value="<?php echo $vCodigo; ?>"  disabled="true">
            </div>
        </div>

        <div class="row form-group" hidden="true"> 
            <label class="col-sm-2 control-label">Fecha Inicio</label>
            <div class="col-sm-4">
                <input type="date" id="fiC" name="datepicker" value = "<?php echo $vFI; ?>" required><span class=""></span>
            </div>               

            <label class="col-sm-2 control-label">Fecha Fin</label>
            <div class="col-sm-2">
                <input type="date" id="ffC" name="datepicker" value = "<?php echo $vFF; ?>" required><span class=""></span>
            </div>              
        </div>

        <div class="row form-group" hidden="true">                    
            <label class="control-label col-sm-2">Hora Inicio</label>
            <div class="col-sm-4">
                <input type="time" id="hiC" name="horaIn" value = "<?php echo $vHI; ?>" required><span class=""></span>
            </div>                 

            <label class="control-label col-sm-2">Hora Fin</label>
            <div class="col-sm-2">
                <input type="time" id="hfC" name="horaFin" value = "<?php echo $vHF; ?>" required><span class=""></span>
            </div>                 
        </div>

        <div class="row form-group" hidden="true">
            <label class="col-sm-2 control-label"> Celular </label>  
            <div class="col-sm-4">
                <input type="text" class="form-control" id="celularC" name="celular" value = "<?php echo $vCel; ?>">
            </div>                
        </div>

        <div class="row form-group" hidden="true">
            <label class="col-sm-2 control-label"> Propósito </label>  
            <div class="col-sm-2">
                <input type="text" class="form-control" id="propC" name="proposito" value = "<?php echo $vP; ?>">
            </div>                      
        </div>

        <div class="row form-group" hidden="true">
            <label class=" col-sm-2 control-label" id="lbclase">Clase</label>
                <div class="col-sm-6" id = "dclase">
                    <input type="text" class="form-control" id="claseC" name="clase" value = "<?php echo $vClase;  ?>">                        
                </div>                     
        </div>

        <div class="row form-group" hidden="true">
            <label class="col-sm-2 control-label" id="lbevento">Evento</label>
                <div class="col-sm-6" id="devento">                            
                    <input type="text" class="form-control" id="eventoC" name="evento" value = "<?php echo $vEvento; ?>">
                </div>                      
        </div>

        <div class="modal-footer">
            <button type="button"  class="btn btn-default" data-dismiss="modal">Volver</button>
            <button type="submit" class="btn btn-primary col-sm-offset-10" > Aceptar <span class="glyphicon glyphicon-circle-arrow-right"></span></button>
        </div>    

    </form>
</div>



<script type="text/javascript" src="pages/reservaciones/solicitud/ScriptReservaciones.js"></script>