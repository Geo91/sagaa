
$(document).ready(function(){

    // Funciones de inserción de Solicitud
    $("#proposito").click(function(event) {
        event.preventDefault();
        var valor = $('#proposito').val();
        if (valor==1) {
            $('#evento').val('');
            $('#lbevento').show('fast');
            $('#devento').show('fast');
            $('#lbclase').hide('fast');
            $('#dclase').hide('fast');
            $('#clase').val('NULL');
            document.getElementById("evento").disabled = false;
            document.getElementById("clase").disabled = true;
        }else{
            $('#lbclase').show('fast');
            $('#dclase').show('fast');
            $('#lbevento').hide('fast');
            $('#devento').hide('fast');
            $('#evento').val('NULL');
            document.getElementById("clase").disabled = false;
            document.getElementById("evento").disabled = true;
        }
    });

    $("#formSolicitudSalon").submit(function(e) {
        e.preventDefault();
        data={
            codigo:$('#codigoSalon').val(),
            fInicio:$('#fechaInicio').val(),
            fFin:$('#fechaFin').val(),
            hInicio:$('#horaIn').val(),
            hFin:$('#horaFin').val(),
            cel:$('#celular').val(),
            proposito:$('#proposito').val(),
            clase:$('#clase').val(),
            evento:$('#evento').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: abrirAviso,
            timeout: 6000,
            error: problemas
        });
        return false;
    });

    $("#formConfirmacion").submit(function(e) {
        e.preventDefault();
        data={
            codigo:$('#codigoC').val(),
            fInicio:$('#fiC').val(),
            fFin:$('#ffC').val(),
            hInicio:$('#hiC').val(),
            hFin:$('#hfC').val(),
            cel:$('#celularC').val(),
            proposito:$('#propC').val(),
            clase:$('#claseC').val(),
            evento:$('#eventoC').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: confirmado,
            timeout: 6000,
            error: problemas
        });
        return false;
    });

    //Fin de Funciones de solicitud de salón
    
});

function verRecursos(){
    $("#contenidoModal").load('pages/reservaciones/solicitud/rsv_verRecursos.php',data);
    $("#modalRecursos").modal('show');
}

function solicitarSalon(){
    $("#contenidoModal2").load('pages/reservaciones/solicitud/rsv_solicitarSalon.php',data);
    $("#modalSolicitud").modal('show');
    $("#divRespuesta").empty();
}

function abrirAviso(){
    $("#divRespuesta").empty();
    $("#contenidoModal3").load('pages/reservaciones/solicitud/rsv_aviso.php',data);
    $("#modalInformativo").modal('show');
}

function denegarSolicitud(){
    $("#contenidoModal").load('pages/permisos/revision/denegarSolicitud.php',data);
    $("#modalInformativo").modal('show');
}

function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function confirmado(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/reservaciones/solicitud/rsv_solicitudConfirmada.php',data);
    $("#modalInformativo").modal('hide');
    $("#modalSolicitud").modal('hide');
}

