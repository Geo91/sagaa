<?php  
    $maindir = "../../../";
    require_once($maindir."conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    if(isset($_POST['codigo']) && isset($_POST['fInicio']) && isset($_POST['fFin']) && isset($_POST['hInicio']) && isset($_POST['hFin']) && isset($_POST['proposito']) && isset($_POST['cel'])) {
        session_start();
        $idUsuario = $_SESSION['user_id'];
        $id = $_POST['codigo'];
        $fI = $_POST['fInicio'];
        $fF = $_POST['fFin'];
        $hIn = $_POST['hInicio'];
        $hFn = $_POST['hFin'];
        $prop = $_POST['proposito'];
        $clase = $_POST['clase'];
        $evento = $_POST['evento'];
        $celular = $_POST['cel'];

        try{
            $sql = "CALL SP_RSV_SOLICITAR_SALON(?,?,?,?,?,?,?,?,?,?,@mensajeError)";
            $query1 = $db->prepare($sql);
            $query1->bindParam(1,$id,PDO::PARAM_INT);
            $query1->bindParam(2,$fI,PDO::PARAM_STR);
            $query1->bindParam(3,$fF,PDO::PARAM_STR);
            $query1->bindParam(4,$hIn,PDO::PARAM_INT);
            $query1->bindParam(5,$hFn,PDO::PARAM_INT);
            $query1->bindParam(6,$prop,PDO::PARAM_INT);
            $query1->bindParam(7,$clase,PDO::PARAM_STR);
            $query1->bindParam(8,$evento,PDO::PARAM_STR);
            $query1->bindParam(9,$celular,PDO::PARAM_INT);
            $query1->bindParam(10,$idUsuario,PDO::PARAM_INT);

            $query1->execute();
            $output = $db->query("select @mensajeError")->fetch(PDO::FETCH_ASSOC);
            $mensaje = $output['@mensajeError'];

            if(is_null($mensaje)){
                echo mensajes("Su solicitud ha sido enviada para su evaluación", "verde");
            }else{
                echo mensajes("Error! ". $mensaje, "rojo");
            }

        }catch(PDOExecption $e){
            echo mensajes("Error: ".$e, "rojo");
        }
    }else{
        echo mensajes("Hubo un error al ingresar su solicitud. Intentelo de nuevo o contacte al administrador del sistema", "rojo");
    } 
    
?>

<script type="text/javascript">
    $("#tablaSalones").load('pages/reservaciones/solicitud/rsv_datos_salones.php');
</script>