<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    $pcCodigo = $_POST['id'];
    $pcSalon = $_POST['nombre'];

    $sql = "SELECT ca_asignaturas.id_asignatura, ca_asignaturas.nombre FROM ca_asignaturas";
    $sql2 = "SELECT departamento_laboral.Id_departamento_laboral, departamento_laboral.nombre_departamento FROM departamento_laboral";

?>

<div class="modal-header" style = "background-color:#0FA6C3">
  	<button type="button" class="close" data-dismiss="modal">&times;</button>
  	<h4 class="modal-title" style = "color:white">Solicitud de Uso de Salón: <br> <?php echo $pcSalon; ?> </h4>
</div>

<div class="modal-body">

	    <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden<br>En caso de usar Mozilla Firefox el formato de fecha debe ser 'año-mes-día' y el formato de hora debe ser de 24 hrs. Ejem: '14:00:00'</center>
	    </div>
	    <!--************************formulario*********************-->           
	        <form class="form-horizontal" role="form" id="formSolicitudSalon" name="formSolicitudSalon">

				<div class="row form-group">
				    <label class ="col-sm-2 control-label" hidden="true">Código</label>
				    <div class = "col-sm-2">
				        <input type = "text" id = "codigoSalon" value="<?php echo $pcCodigo; ?>" hidden="true" disabled="true">
				    </div>
				</div>

	            <div class="row form-group"> 
	                <label class="col-sm-2 control-label">Fecha Inicio</label>
	                <div class="col-sm-4">
	                    <input type="date" id="fechaInicio" name="datepicker" required><span class=""></span>
	                </div>               

	                <label class="col-sm-2 control-label">Fecha Fin</label>
	                <div class="col-sm-2">
	                    <input type="date" id="fechaFin" name="datepicker" required><span class=""></span>
	                </div>              
	            </div>

	            <div class="row form-group">                    
	                <label class="control-label col-sm-2">Hora Inicio</label>
	                <div class="col-sm-4">
	                    <input type="time" id="horaIn" name="horaIn" required><span class=""></span>
	                </div>                 

	                <label class="control-label col-sm-2">Hora Fin</label>
	                <div class="col-sm-2">
	                    <input type="time" id="horaFin" name="horaFin" required><span class=""></span>
	                </div>                 
	            </div>

	            <div class="row form-group">
	                <label class="col-sm-2 control-label"> Responsable </label>  
	                <div class="col-sm-4">
                        <input type="text" class="form-control" id="responsable" name="responsable" title = "Ingrese el nombre de la persona responsable de la actividad" required>
                    </div>           
	            </div>

	            <div class="row form-group">
	                <label class="col-sm-2 control-label"> Celular </label>  
	                <div class="col-sm-4">
                        <input type="text" class="form-control" id="celular" name="celular" title = "Ingrese un número de celular sin guiones" placeholder="########">
                    </div>                
	            </div>

	            <div class="row form-group">
	                <label class=" col-sm-2 control-label">Unidad</label>
	                    <div class="col-sm-6">     
	                        <select class="form-control" id="unidad" name="unidad">
	                            <option value="0">Seleccione una opción</option>
	                            <?php
                                    $result = $db->prepare($sql2);
                                    $result->execute();
                                    while ($row = $result->fetch()) {
                                ?> 
                                        <option value="<?php echo $row['Id_departamento_laboral']; ?>"> <?php echo $row["nombre_departamento"]; ?></option>
                                <?php
                                    }
                                ?>

	                        </select>                         
	                    </div>                     
	            </div>

	            <div class="row form-group">
	                <label class="col-sm-2 control-label"> Propósito </label>  
	                <div class="col-sm-2">
	                    <select id="proposito" name="proposito" size="2" required>
	                        <option value="0"> Clase </option>
	                        <option value="1"> Evento </option>
	                    </select>
	                </div>                      
	            </div>

	            <div class="row form-group">
	                <label class=" col-sm-2 control-label" id="lbclase" hidden = "true">Clase</label>
	                    <div class="col-sm-6" id = "dclase" hidden = "true">     
	                        <select class="form-control" id="clase" name="clase">
	                            <option value="0">Seleccione una opción</option>
	                            <?php
                                    $resultado = $db->prepare($sql);
                                    $resultado->execute();
                                    while ($fila = $resultado->fetch()) {
                                ?> 
                                        <option value="<?php echo $fila['id_asignatura']; ?>"> <?php echo $fila["nombre"]; ?></option>
                                <?php
                                    }
                                ?>

	                        </select>                         
	                    </div>                     
	            </div>

	            <div class="row form-group">
	                <label class="col-sm-2 control-label" id="lbevento" hidden = "true">Evento</label>
	                    <div class="col-sm-6" id="devento" hidden = "true">              
	                        <input type="text" class="form-control" id="evento" name="evento" >
	                    </div>                      
	            </div>

	            <div class="modal-footer">
	                <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
	                <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" > Enviar <span class="glyphicon glyphicon-circle-arrow-right"></span></button>                                   
	            </div>    

	        </form>
	    <!--************************formulario*********************-->
</div>

<script type="text/javascript" src="pages/reservaciones/solicitud/ScriptReservaciones.js"></script>