$(document).ready(function(){

    // Funciones de Edición
    $("#eproposito").click(function(event) {
        event.preventDefault();
        var valor = $('#eproposito').val();
        if (valor==1) {
            $('#eevento').val('');
            $('#elbevento').show('fast');
            $('#edevento').show('fast');
            $('#elbclase').hide('fast');
            $('#edclase').hide('fast');
            $('#eclase').val('NULL');
            document.getElementById("eevento").disabled = false;
            document.getElementById("eclase").disabled = true;
        }else{
            $('#elbclase').show('fast');
            $('#edclase').show('fast');
            $('#eclase').val(0);
            $('#elbevento').hide('fast');
            $('#edevento').hide('fast');
            $('#eevento').val('NULL');
            document.getElementById("eclase").disabled = false;
            document.getElementById("eevento").disabled = true;
        }
    });

    $("#formEditarSolicitudSalon").submit(function(e) {
        e.preventDefault();
        if(validarFecha() === true){
            if(validarHorario() === true){
                data={
                    idR:$('#ecodigoR').val(),
                    codigo:$('#ecodigoSalon').val(),
                    fInicio:$('#efechaInicio').val(),
                    fFin:$('#efechaFin').val(),
                    hInicio:$('#ehoraIn').val(),
                    hFin:$('#ehoraFin').val(),
                    respon:$('#eresponsable').val(),
                    unit:$('#eunidad').val(),
                    numCel:$('#ecelular').val(),
                    proposito:$('#eproposito').val(),
                    clase:$('#eclase').val(),
                    evento:$('#eevento').val()
                }
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: solicitudEditada,
                    timeout: 6000,
                    error: problemas
                });
                return false;
            }else{
                alert("El horario no ha sido ingresado correctamente, intentelo de nuevo");
            }
        }else{
            alert("Las fechas no se han ingresado correctam}, intentelo de nuevo");
        }
        
    });

    //Finalizan Funciones del formulario de edición
    
});

function validarFecha(){
    var fI = $('#efechaInicio').val();
    var fF = $('#efechaFin').val();
    if(fF >= fI){
        return true;
    }else{
        return false;
    }
}

function validarHorario(){
    var hI = $('#ehoraIn').val();
    var hF = $('#ehoraFin').val();
    if(hF > hI){
        return true;
    }else{
        return false;
    }
}

function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function editarSolicitudSalon(){
    $("#contenidoModal").load('pages/reservaciones/edicion/rsv_editarSolicitud.php',data);
    $("#modalEdicion").modal('show');
}

function solicitudEditada(){
    $("#notificacion").load('pages/reservaciones/edicion/rsv_solicitudEditada.php',data);
    $("#modalEdicion").modal('hide');
}

function cancelarSolicitudSalon(){
    $("#notificacion").load('pages/reservaciones/edicion/rsv_solicitudCancelada.php',data);
    $("#modalEdicion").modal('hide');
}

function exportar(){
    $("#notificacion").empty();
    $("#notificacion").load('pages/reservaciones/edicion/exportarFormato.php', data);           
};