<?php

    $maindir = "../../../";

    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");
    require_once($maindir."fpdf/fpdf.php");


    $idR = $_GET['id'];

    $consulta = "SELECT CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ', persona.Segundo_apellido) as 'Empleado', empleado.No_Empleado, CONCAT(rsv_reservaciones.fecha_inicio,' AL ',rsv_reservaciones.fecha_fin) as 'fechas', CONCAT(rsv_reservaciones.horaI,' - ',rsv_reservaciones.horaF) as 'horario', rsv_reservaciones.numcelular, rsv_salones.nombre_salon, rsv_salones.ubicacion FROM persona INNER JOIN (empleado INNER JOIN (rsv_reservaciones INNER JOIN rsv_salones ON rsv_reservaciones.salon_id=rsv_salones.id_salon) ON empleado.No_Empleado=rsv_reservaciones.n_empleado) ON persona.N_identidad=empleado.N_identidad WHERE rsv_reservaciones.id_reservacion = :cR;";

    $query = $db->prepare($consulta);
    $query ->bindParam(":cR",$idR);
    $query->execute();
    $result = $query->fetch();

    class PDF extends FPDF{
         // Cabecera de página
             
        function Footer(){
            $idR = $_GET['id'];
            $consulta2 = "SELECT rsv_reservaciones.correlativo from rsv_reservaciones where rsv_reservaciones.estado_reservacion = 5 AND rsv_reservaciones.id_reservacion='".$idR."'";
            $resultado2 = mysql_query($consulta2);
            $result2 = mysql_fetch_array($resultado2);
            $cltv = $result2['correlativo'];

            $this->SetY(-20);
            $this->SetFont('Arial','I',8);
            $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().' de {nb}   - - - -   Impreso desde SAGAA el ' . date('d-m-y') . ' fecha del sistema   - - - -   '.$cltv,0,0,'C');
        }  
    }


    $pdf = new PDF();
    $pdf->AliasNbPages();

    $pdf->AddPage();
    $pdf->SetFont('Times', '', 18);
    $pdf->Image($maindir.'assets/img/enc_doc.png', 5,5,200,25, 'PNG');
    $pdf->Cell(22, 10, '', 0);
    $pdf->Ln(25);
    $pdf->SetFont('Arial', '', 18);
    $pdf->Cell(5, 10, '', 0);
    $pdf->Cell(180, 10, utf8_decode('UNIVERSIDAD NACIONAL AUTÓNOMA DE HONDURAS'), 0,0,"C");
    $pdf->Ln(10);
    $pdf->SetFont('Arial', '', 12);
    $pdf->Cell(25, 8, '', 0,0,"C");
    $pdf->Cell(135, 8, utf8_decode('FACULTAD DE CIENCIAS JURÍDICAS'), 0,0,"C");
    $pdf->SetFont('Arial', '', 10);
    $pdf->Ln(10);
    $pdf->SetFont('Arial', '', 12);
    $pdf->Cell(25, 8, '', 0,0,"C");
    $pdf->Cell(135, 8, utf8_decode($result['nombre_salon']), 0,0,"C");
    $pdf->SetFont('Arial', '', 10);
    $pdf->Ln(15);
    $pdf->SetFont('Arial', 'U', 12);
    $pdf->Cell(25, 8, '', 0,0,"C");
    $pdf->Cell(135, 8, utf8_decode('CONTROL DE USO'), 0,0,"C");
    $pdf->SetFont('Arial', '', 10);
    $pdf->Ln(10);
    $pdf->Cell(14);
    $pdf->MultiCell(160, 8, 'YO, '.utf8_decode($result['Empleado']).utf8_decode(' CON NÚMERO DE EMPLEADO ').$result['No_Empleado'].utf8_decode(', HABIENDO HECHO LA RESPECTIVA SOLICITUD DE RESERVACIÓN DE USO DE ').utf8_decode($result['nombre_salon']).'; UBICADO EN '.utf8_decode($result['ubicacion']).'; EN LAS FECHAS DEL '.$result['fechas'].'; Y EN EL HORARIO '.$result['horario'].utf8_decode('  ME COMPROMETO A HACER BUEN USO DEL INMUEBLE Y ME HAGO RESPONSABLE DEL EXTRAVÍO O DAÑO DEL MOBILIARIO QUE SE ENCUENTRA EN ESTE.') , 0, 'J', 0);

    $pdf->Ln(15);
    $pdf->Cell(14);
    $pdf->Cell(60, 20, 'OBSERVACIONES');
    $pdf->Ln(12);
    $pdf->Cell(14);
    $pdf->MultiCell(160, 7, '________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________', 0, 'J', 0);

    $pdf->Ln(15);
    $pdf->Cell(14);
    $pdf->Cell(42, 20, 'FECHA: '.date('d-m-y'), 0,0,"");
    $pdf->Ln(7);
    $pdf->Cell(14);
    $pdf->Cell(42, 20, 'CELULAR: '.$result['numcelular'], 0,0,"");
    $pdf->Ln(50);
    $pdf->Cell(5);
    $pdf->Cell(75, 8, '_______________________________', 0,0,"C");
    $pdf->Cell(135, 8, '_______________________________', 0,0,"C");
    $pdf->Ln(5);
    $pdf->Cell(80, 8, 'SOLICITANTE', 0,0,"C");
    $pdf->Cell(130, 8, 'ADMINISTRADOR', 0,0,"C");
    $pdf->Ln(15);
    $pdf->Cell(5);

    $pdf->Output('FormatoResponsabilidad.pdf','D');

?>