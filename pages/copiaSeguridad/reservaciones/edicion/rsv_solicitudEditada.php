<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    session_start();
    $idUsuario = $_SESSION['user_id'];

    if(isset($_POST['idR']) && isset($_POST['codigo']) && isset($_POST['fInicio']) && isset($_POST['fFin']) && isset($_POST['hInicio']) && isset($_POST['hFin']) && isset($_POST['numCel']) && isset($_POST['respon']) && isset($_POST['unit']) && isset($_POST['proposito'])){

		$pcCodigoR = $_POST['idR'];
		$pcSalon = $_POST['codigo'];
		$pcfInicio = $_POST['fInicio'];
		$pcfFin = $_POST['fFin'];
		$pchInicio = $_POST['hInicio'];
		$pchFin = $_POST['hFin'];
		$pcResp = $_POST['respon'];
		$pcUni = $_POST['unit'];
		$pcProp = $_POST['proposito'];
		$pcClase = $_POST['clase'];
		$pcEvento = $_POST['evento'];
		$pcCelular = $_POST['numCel'];

        try{
			$consulta=$db->prepare("CALL SP_RSV_EDITAR_SOLICITUD(?,?,?,?,?,?,?,?,?,?,?,?,?,@pcMensajeError)");
			$consulta->bindParam(1, $pcCodigoR, PDO::PARAM_INT);
			$consulta->bindParam(2, $pcSalon, PDO::PARAM_INT);
			$consulta->bindParam(3, $pcfInicio, PDO::PARAM_INT);
			$consulta->bindParam(4, $pcfFin, PDO::PARAM_INT);
			$consulta->bindParam(5, $pchInicio, PDO::PARAM_INT);
			$consulta->bindParam(6, $pchFin, PDO::PARAM_INT);
			$consulta->bindParam(7, $pcProp, PDO::PARAM_INT);
			$consulta->bindParam(8, $pcClase, PDO::PARAM_STR);
			$consulta->bindParam(9, $pcEvento, PDO::PARAM_STR);
			$consulta->bindParam(10, $pcResp, PDO::PARAM_STR);
            $consulta->bindParam(11, $pcUni, PDO::PARAM_INT);
			$consulta->bindParam(12, $pcCelular, PDO::PARAM_INT);
			$consulta->bindParam(13, $idUsuario, PDO::PARAM_INT);


			$resultado=$consulta->execute();
			$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
			$mensaje = $output['@pcMensajeError'];

			if ($mensaje == NULL){
			    echo mensajes('Su solicitud ha sido editada exitosamente', 'verde');
			}else{
			    echo mensajes('Error! '.$mensaje, 'rojo');
			}
		}catch(PDOexception $e){
			echo mensajes('Error! '. $e, 'rojo');
		}
	}else{
		echo mensajes("Hubo un error al intentar editar la solicitud. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
	}

?>

<script type="text/javascript">
	$("#tablaDatos").load('pages/reservaciones/edicion/rsv_datosSolicitudes.php');    
</script>