<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    $pcCodigo = $_POST['id'];

    $sql = "SELECT ca_asignaturas.id_asignatura, ca_asignaturas.nombre FROM ca_asignaturas";
    $sql2 = "SELECT rsv_reservaciones.salon_id, rsv_reservaciones.fecha_inicio, rsv_reservaciones.fecha_fin, rsv_reservaciones.horaI, rsv_reservaciones.horaF, rsv_reservaciones.numcelular, rsv_reservaciones.proposito, rsv_reservaciones.clase_id, rsv_reservaciones.evento FROM rsv_reservaciones INNER JOIN rsv_salones ON rsv_reservaciones.salon_id=rsv_salones.id_salon WHERE rsv_reservaciones.id_reservacion = '$pcCodigo'";
    $sql3 = "SELECT rsv_salones.id_salon, rsv_salones.nombre_salon FROM rsv_salones";
	$resultado2 = $db->prepare($sql2);
    $resultado2->execute();
    while($row = $resultado2->fetch()){
    	$idsalon = $row['salon_id'];
    	$fechaI = $row['fecha_inicio'];
    	$fechaF = $row['fecha_fin'];
    	$horaI = $row['horaI'];
    	$horaF = $row['horaF'];
    	$cel = $row['numcelular'];
    	$prop = $row['proposito'];
    	$clase = $row['clase_id'];
    	$evento = $row['evento'];
    }
?>

<div class="modal-header" style = "background-color:#0FA6C3">
  	<button type="button" class="close" data-dismiss="modal">&times;</button>
  	<h4 class="modal-title" style = "color:white">Editar Solicitud de Uso de Salón </h4>
</div>

<div class="modal-body">
        
    <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden<br>En caso de usar Mozilla Firefox el formato de fecha debe ser 'año-mes-día' y el formato de hora debe ser de 24 hrs. Ejem: '14:00:00'</center></div>

    <div class="row">
        <div class="col-lg-12">

	    <!--************************formulario*********************-->           
	        <form class="form-horizontal" role="form" id="formEditarSolicitudSalon" name="formEditarSolicitudSalon">

				<div class="row form-group">
				    <label class ="col-sm-2 control-label" hidden="true">Código Reservación</label>
				    <div class = "col-sm-2">
				        <input type = "text" id = "ecodigoR" value="<?php echo $pcCodigo; ?>" hidden="true" disabled="true">
				    </div>
				</div>

				<div class="row form-group">
	                <label class="col-sm-2 control-label"> Salón </label>  
	                <div class="col-sm-6">
	                    <select class="form-control" id="ecodigoSalon" name="ecodigoSalon">
                            <?php
                                $resultado3 = $db->prepare($sql3);
                                $resultado3->execute();
                                while ($fila3 = $resultado3->fetch()) {
                            ?>
                                    <option value="<?php echo $fila3['id_salon']; ?>" <?php if($fila3['id_salon'] == $idsalon){ echo 'selected'; } ?>> <?php echo $fila3["nombre_salon"]; ?></option>
                            <?php
                                }
                            ?>

                        </select>
	                </div>                      
	            </div>

	            <div class="row form-group"> 
	                <label class="col-sm-2 control-label">Fecha Inicio</label>
	                <div class="col-sm-4">
	                    <input type="date" id="efechaInicio" name="datepicker" value="<?php echo $fechaI ?>" required><span class=""></span>
	                </div>               

	                <label class="col-sm-2 control-label">Fecha Fin</label>
	                <div class="col-sm-2">
	                    <input type="date" id="efechaFin" name="datepicker" value="<?php echo $fechaF ?>" required><span class=""></span>
	                </div>              
	            </div>

	            <div class="row form-group">                    
	                <label class="control-label col-sm-2">Hora Inicio</label>
	                <div class="col-sm-4">
	                    <input type="time" id="ehoraIn" name="horaIn" value="<?php echo $horaI ?>" required><span class=""></span>
	                </div>                 

	                <label class="control-label col-sm-2">Hora Fin</label>
	                <div class="col-sm-2">
	                    <input type="time" id="ehoraFin" name="horaFin" value="<?php echo $horaF ?>" required><span class=""></span>
	                </div>                 
	            </div>

	           	<div class="row form-group">
	                <label class="col-sm-2 control-label"> Celular </label>  
	                <div class="col-sm-4">
                        <input type="text" class="form-control" id="ecelular" name="celular" value = "<?php echo $cel; ?>" title = "Ingrese un número de celular sin guiones">
                    </div>                
	            </div>

	            <div class="row form-group">
	                <label class="col-sm-2 control-label"> Propósito </label>  
	                <div class="col-sm-1">
	                    <select id="eproposito" name="proposito" size="2" required>
	                    	<?php
	                        	if($prop == 0){
	                        		echo "<option value='0' selected> Clase </option>
	                        			<option value='1'> Evento </option>";
	                        	}else{
									echo "<option value='0'> Clase </option>
	                        			<option value='1' selected> Evento </option>";
	                        	}
	                        ?>
	                    </select>
	                </div>                      
	            </div>

	            <div class="row form-group">
	                <label class=" col-sm-2 control-label" id="elbclase" <?php if($prop == 1){ echo "hidden = 'true'"; } ?> >Clase</label>
                    <div class="col-sm-6" id = "edclase" <?php if($prop == 1){ echo "hidden = 'true'"; } ?> >     
                        <select class="form-control" id="eclase" name="clase">
                            <option value="0">Seleccione una opción</option>
                            <?php
                                $resultado = $db->prepare($sql);
                                $resultado->execute();
                                while ($fila = $resultado->fetch()) {
                            ?>
                                    <option value="<?php echo $fila['id_asignatura']; ?>" <?php if($fila['id_asignatura'] == $clase){ echo 'selected'; } ?>> <?php echo $fila["nombre"]; ?></option>
                            <?php
                                }
                            ?>

                        </select>                         
                    </div>                     
	            </div>

	            <div class="row form-group">
	                <label class="col-sm-2 control-label" id="elbevento" <?php if($prop == 0){ echo "hidden = 'true'"; } ?> > Evento</label>
	                    <div class="col-sm-4" id="edevento" <?php if($prop == 0){ echo "hidden = 'true'";} ?> >                        
	                        <input type="text" class="form-control" id="eevento" name="evento" value = "<?php echo $evento; ?>">
	                    </div>                      
	            </div>

	            <div class="modal-footer">
	                <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
	                <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" > Guardar <span class="glyphicon glyphicon-circle-arrow-right"></span></button>                                  
	            </div>  

	        </form>
	    <!--************************formulario*********************-->
	    </div>
	</div>
</div>

<script type="text/javascript" src="pages/reservaciones/edicion/ScriptEdicion.js"></script>