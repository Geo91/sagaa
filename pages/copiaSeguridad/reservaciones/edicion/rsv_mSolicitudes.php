
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong>Mis Solicitudes</strong></center></h3>
    </div>
</div>

<div id = "notificacion"></div>

<div id = "tablaDatos">
    <?php require_once("rsv_datosSolicitudes.php") ?>
</div>

<!-- Modal para visualizar los recursos disponibles en el salón -->
<div class="modal fade" id="modalEdicion" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div id="contenidoModal">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="pages/reservaciones/edicion/ScriptEdicion.js"></script>