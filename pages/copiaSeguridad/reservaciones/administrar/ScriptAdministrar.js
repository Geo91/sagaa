//cargar la tabla desde un principio
    $("#tablaAdministracion").load('pages/reservaciones/administrar/rsv_datosAdministradores.php');

$(document).ready(function(){

    $("#formAsignar").submit(function(e) {
        e.preventDefault();
        data={
            nemp:$('#empleado').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: otorgarPrivilegios,
            timeout: 6000,
            error: problemas
        });
        return false;
    });
    
});

function inicioEnvio(){
    $("#notificaciones").empty();
    $("#notificaciones").text('Cargando...');
}

function problemas(){
    $("#notificaciones").text('Problemas en el servidor.');
}

function removerPrivilegios(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/reservaciones/administrar/rsv_privilegiosRemovidos.php',data);
}

function otorgarPrivilegios(){
    $("#notificaciones").empty();
    $("#notificaciones").load('pages/reservaciones/administrar/rsv_privilegiosOtorgados.php',data);
}

