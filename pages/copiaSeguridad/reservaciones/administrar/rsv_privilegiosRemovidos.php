<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");


    if(isset($_POST['nEmp'])) {

		$pcEmpleado = $_POST['nEmp'];

        try{
			$consulta=$db->prepare("CALL SP_RSV_REMOVER_PRIVILEGIOS(?,@pcMensajeError)");
			$consulta->bindParam(1, $pcEmpleado, PDO::PARAM_INT);

			$resultado=$consulta->execute();
			$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
			$mensaje = $output['@pcMensajeError'];

			if ($mensaje == NULL){
			    echo mensajes('Los privilegios de administrador fueron removidos exitosamente', 'verde');
			}else{
			    echo mensajes('Error!'.$mensaje, 'rojo');
			}
		}catch(PDOexception $e){
			echo mensajes('Error!'. $e, 'rojo');
		}
	}else{
		echo mensajes("Hubo un error al intentar remover los privilegios de administrador a este empleado. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
	}

?>

<script type="text/javascript">
	$("#tablaAdministracion").load('pages/reservaciones/administrar/rsv_datosAdministradores.php');
</script>