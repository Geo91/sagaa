<?php
    $maindir = "../../../";
    include($maindir."conexion/config.inc.php");
    require_once("../ChekAutoIncrement.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");
    $idUsuario = $_SESSION['user_id'];
?>

<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaAdministradores" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style ="display:none">Código</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Remover</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    try{
                        $sql = "CALL SP_RSV_DATOS_ADMINISTRADORES(?)";
                        $query = $db->prepare($sql);
                        $query->bindParam(1,$idUsuario,PDO::PARAM_INT);

                        $query->execute();
                        while ($fila = $query->fetch()) {
                ?>
                        <tr data-id='<?php echo $fila["No_Empleado"]; ?>'> 
                            <td style = "display:none"><?php echo $fila["No_Empleado"]; ?></td>
                            <td><?php echo $fila["nombre"]; ?></td>
                            <td>
                                <center>
                                    <button type="button" class="removerPrivilegios<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-remove"  title="Remover privilegios de administrador.">
                                </center>
                            </td>           
                        </tr>
                <?php 
                        } //cierre del ciclo while para llenar la tabla de datos
                    }catch(PDOException $e){
                        echo "Error: ".$e;
                    }
                ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div>

<script type="text/javascript">   
//opciones para buscador en la tabla 
    $('#tablaAdministradores')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaAdministradores').dataTable({
        "order": [[0,"asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });

    $(document).on("click", ".removerPrivilegios<?php echo $auto_increment; ?>", function () {  
        var respuesta = confirm("¿Está seguro que desea remover privilegios de administrador a este empleado?");
        if (respuesta){
            data = {
                nEmp: $(this).parents("tr").data("id")
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                success: removerPrivilegios,
                timeout: 8000,
                error: problemas
            }); 
            return false; 
        }
    });

</script>