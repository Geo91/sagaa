
$(document).ready(function(){

    $("#formAprobarSR").submit(function(e) {
        e.preventDefault();
        data={
            codigo:$('#idReservacion').val(),
            observacion:$('#obs').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: reservacionAprobada,
            timeout: 6000,
            error: problemas
        });
        return false;
    });

    $("#formDenegarSR").submit(function(e) {
        e.preventDefault();
        data={
            codigo:$('#idReservacion').val(),
            observacion:$('#obs').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: reservacionDenegada,
            timeout: 6000,
            error: problemas
        });
        return false;
    });
    
});

function aprobarSR(){
    $("#contenidoModal").load('pages/reservaciones/revisar/rsv_aprobarReservacion.php',data);
    $("#modalRevisar").modal('show');
}

function denegarSR(){
    $("#contenidoModal").load('pages/reservaciones/revisar/rsv_denegarReservacion.php',data);
    $("#modalRevisar").modal('show');
}

function inicioEnvio(){
    $("#notificaciones").empty();
    $("#notificaciones").text('Cargando...');
}

function problemas(){
    $("#notificaciones").text('Problemas en el servidor.');
}

function reservacionAprobada(){
    $("#notificaciones").empty();
    $("#notificaciones").load('pages/reservaciones/revisar/rsv_reservacionAprobada.php',data);
    $("#modalRevisar").modal('hide');
}

function reservacionDenegada(){
    $("#notificaciones").empty();
    $("#notificaciones").load('pages/reservaciones/revisar/rsv_reservacionDenegada.php',data);
    $("#modalRevisar").modal('hide');
}
