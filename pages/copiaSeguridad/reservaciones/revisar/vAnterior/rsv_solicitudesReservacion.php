<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");
    require_once("../ChekAutoIncrement.php");
    $idUsuario = $_SESSION['user_id'];
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <!--<section class="content">-->
                <div class="table-responsive">
                    <table id= "TablaRSolicitudes" border="0" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th style = "text-align:center;background-color:#386D95;color:white;">Codigo</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Empleado</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Fecha Inicio</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Fecha Fin</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Horario</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Propósito</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Aprobar</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Denegar</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Contenido de la tabla generado atravez de la consulta a 
                                la base de datos -->
                                <?php
                                    try{
                                        $sql = "CALL SP_RSV_DATOS_RSOLICITUDES(?)";
                                        $query = $db->prepare($sql);
                                        $query->bindParam(1,$idUsuario,PDO::PARAM_INT);

                                        $query->execute();
                                        while ($fila = $query->fetch()) {
                                ?>
                                        <tr data-id='<?php echo $fila["id_reservacion"]; ?>'>
                                            <td><?php echo $fila["id_reservacion"]; ?></td>
                                            <td><?php echo $fila["Empleado"]; ?></td>
                                            <td><?php echo $fila["fecha_inicio"]; ?></td>
                                            <td><?php echo $fila["fecha_fin"]; ?></td>
                                            <td><?php echo $fila["horaI"]." - ".$fila["horaF"]; ?></td>
                                            <td><?php if($fila["proposito"] == 'Clase'){
                                                echo "Clase: ".$fila["nombre"]; }else{ echo "Evento: ".$fila["evento"]; }?></td>
                                            <td>
                                                <center>
                                                    <button type="button" class="aprobarSolicitud<?php echo $auto_increment; ?> btn btn-success"  title="Editar la solicitud."><i class="    glyphicon glyphicon-thumbs-up"></i></button>
                                                </center>
                                            </td>
                                            <td>
                                                <center>
                                                    <button type="button" class="denegarSolicitud<?php echo $auto_increment; ?> btn btn-danger" title="Exportar la solicitud de permiso aprobado."><i class="  glyphicon glyphicon-thumbs-down"></i></button>
                                                </center>
                                            </td>             
                                        </tr>
                                <?php 
                                        } //cierre del ciclo while para llenar la tabla de datos
                                    }catch(PDOException $e){
                                        echo "Error: ".$e;
                                    }
                                ?>
                        </tbody>
                    </table>       
                </div>
            <!--</section>-->
        </div>                
    </div>
</div>

<script type="text/javascript">   
//opciones para buscador en la tabla
    $('#TablaRSolicitudes')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#TablaRSolicitudes').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {

        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });

    $(document).on("click", ".aprobarSolicitud<?php echo $auto_increment; ?>", function () {  
        data = {
            idR: $(this).parents("tr").data("id")
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: aprobarSR,
            timeout: 15000,
            error: problemas
        }); 
        return false; 
    });

    $(document).on("click", ".denegarSolicitud<?php echo $auto_increment; ?>", function () {    
        data = {
            idR: $(this).parents("tr").data("id")
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: denegarSR,
            timeout: 15000,
            error: problemas
        }); 
        return false; 
    });

</script>