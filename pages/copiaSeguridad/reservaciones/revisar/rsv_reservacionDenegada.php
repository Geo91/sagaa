<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");


    if(isset($_POST['codigo']) && isset($_POST['observacion'])) {

		$pcCodigoR = $_POST['codigo'];
		$pcObservacion = $_POST['observacion'];

        try{
			$consulta=$db->prepare("CALL SP_RSV_DENEGAR_SOLICITUD(?,?,@pcMensajeError)");
			$consulta->bindParam(1, $pcCodigoR, PDO::PARAM_INT);
			$consulta->bindParam(2, $pcObservacion, PDO::PARAM_STR);

			$resultado=$consulta->execute();
			$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
			$mensaje = $output['@pcMensajeError'];

			if ($mensaje == NULL){
			    echo mensajes('La solicitud ha sido denegada exitosamente', 'verde');
			}else{
			    echo mensajes('Error!'.$mensaje, 'rojo');
			}
		}catch(PDOexception $e){
			echo mensajes('Error!'. $e, 'rojo');
		}
	}else{
		echo mensajes("Hubo un error al intentar denegar la solicitud. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
	}

?>

<script type="text/javascript">
	$("#TablaSolicitudes").load('pages/reservaciones/revisar/rsv_solicitudesReservacion.php');   
</script>