
<!-- Seccion usada para mostrar la tabla de las solicitudes de permiso que estan pendientes de revisión en el sistema -->
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong>Revisión de Solicitudes</strong></center></h3>
    </div>
</div>

<div id = "notificaciones"></div>

<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Solicitudes de Reservación de Salón Pendientes </label>
        </div>
        <div class="panel-body">
            <div>
                <div id= "noti1" class="alert alert-info" role="alert"><center>Seleccione la solicitud que desee aprobar o denegar</center></div>
            </div>

            <div id = "TablaSolicitudes">
                <?php include("rsv_solicitudesReservacion.php"); ?>
            </div>   
                
        </div>
    </div>
</div>

<!-- Modal para revisar las solicitudes de reservación -->
<div class="modal fade" id="modalRevisar" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div id="contenidoModal">
        </div>
      </div>
    </div>
</div>

<script type="text/javascript" src="pages/reservaciones/revisar/ScriptRevisar.js"></script>