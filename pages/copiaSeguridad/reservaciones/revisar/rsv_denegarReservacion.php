<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir."conexion/config.inc.php");

    $pcReservacion = $_POST['idR'];
?>

<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3 class="modal-title" style="color:white">Denegación de la solicitud de reservación</h3>
</div>

<div class="modal-body">
	<form class="form-horizontal" role="form" id="formDenegarSR" name="formAprobar">
		<div class="form-group col-sm-12" style="display:none">
		    <label>Código Reservación</label>
				<input id="idReservacion" value = "<?php echo $pcReservacion; ?>" class="form-control" required disabled>
		</div>

		<div class="form-group col-sm-12">
		    <label>Observación</label>
		    <input id="obs" placeholder = "Agregar una observación" class="form-control" required>
		</div>
<br>
<br>
<br>
<br>
		<div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		    <button type = "submit" class="btn btn-primary"> Enviar <span class="glyphicon glyphicon-chevron-right"></span></button>
		</div>
	</form>
</div>

<script type="text/javascript" src="pages/reservaciones/revisar/ScriptRevisar.js"></script>