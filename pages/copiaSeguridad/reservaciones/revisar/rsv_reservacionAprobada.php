<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");


    if(isset($_POST['codigo']) && isset($_POST['observacion'])){

		$pcCodigoR = $_POST['codigo'];
		$pcObservacion = $_POST['observacion'];

		$sql = "SELECT rsv_reservaciones.horaI, rsv_reservaciones.horaF, rsv_reservaciones.fecha_inicio, rsv_reservaciones.fecha_fin from rsv_reservaciones WHERE rsv_reservaciones.id_reservacion = $pcCodigoR";
		$result=$db->prepare($sql);
		$result->execute();
		while($fila = $result->fetch()){
			$pcHoraI = $fila["horaI"];
			$pcHoraF = $fila["horaF"];
			$pcFechaI = $fila["fecha_inicio"];
			$pcFechaF = $fila["fecha_fin"];
		}

        try{
			$consulta=$db->prepare("CALL SP_RSV_APROBAR_SOLICITUD(?,?,?,?,?,?,@pcMensajeError)");
			$consulta->bindParam(1, $pcCodigoR, PDO::PARAM_INT);
			$consulta->bindParam(2, $pcObservacion, PDO::PARAM_STR);
			$consulta->bindParam(3, $pcHoraI, PDO::PARAM_INT);
			$consulta->bindParam(4, $pcHoraF, PDO::PARAM_INT);
			$consulta->bindParam(5, $pcFechaI, PDO::PARAM_STR);
			$consulta->bindParam(6, $pcFechaF, PDO::PARAM_STR);

			$resultado=$consulta->execute();
			$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
			$mensaje = $output['@pcMensajeError'];

			if ($mensaje == NULL){
			    echo mensajes('La solicitud ha sido aprobada exitosamente', 'verde');
			}else{
			    echo mensajes('Error!'.$mensaje, 'rojo');
			}
		}catch(PDOexception $e){
			echo mensajes('Error!'. $e, 'rojo');
		}
	}else{
		echo mensajes("Hubo un error al intentar aprobar la solicitud. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
	}

?>

<script type="text/javascript">
	$("#TablaSolicitudes").load('pages/reservaciones/revisar/rsv_solicitudesReservacion.php');   
</script>