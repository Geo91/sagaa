<?php
    $maindir = "../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    include($maindir."conexion/config.inc.php");

?>


<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong>Reservación de Salones</strong></center></h3>
    </div>
</div> 

<div  id="divRespuesta"></div>

<div class="col-lg-12">       
    <div class="panel panel-primary">
        <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span><strong> Solicitud de Uso de Salón </strong></div>

        <div class="panel-body" align = "center">
            <div >
                <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden<br>En caso de usar mozilla firefox el formato de fecha debe ser 'año-mes-día' </center></div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                <!--**************************************Formulario*****************************************-->
                    <form class = "form-horizontal" role="form" id="formSolicitud" name="formSolicitud">
                    
                    	<div class="row form-group"> 
                            <label class="col-sm-2 control-label">Fecha Inicio</label>
                            <div class="col-sm-2">
                                <input type="date" id="fechaInicio" name="datepicker" required>
                            </div>               

                            <label class="col-sm-2 control-label">Fecha Fin</label>
                            <div class="col-sm-2">
                                <input type="date" id="fechaFin" name="datepicker" required>
                            </div>               
                        </div>

                        <div class="row form-group"> 
                            <label class="control-label col-sm-3">Hora Inicio</label>
                            <div class="col-sm-2">
                                <input type="time" id="horaIn" name="horaIn" required>
                            </div>              

                            <label class="control-label col-sm-3">Hora Fin</label>
                            <div class="col-sm-2">
                                <input type="time" id="horaFn" name="horaFn" required>
                            </div>               
                        </div>

	                    <div class="row form-group">                    
	                        <label class=" col-sm-2 control-label"> Propósito </label>                       
	                        <div class="col-sm-2">
	                            <select id="proposito" name="proposito" size="2" required>
	                                <option value="0"> Clase </option>
	                                <option value="1"> Evento </option>
	                            </select>            
	                        </div> 
	                    </div>

	                    <div class = "row form-group">
	                        <label id = "lbEmpleado" class=" col-sm-2 control-label"> Clase </label>     
	                        <div class="col-sm-4">                   
	                            <select class = "form-control" id="empleado" name="empleado" value="0" required title="Seleccione la clase que se impartirá en el salón.">
	                                <option value = "0"> Seleccione una opción </option>
	                                <?php
	                                    $resultado = $db->prepare($query);
	                                    $resultado->execute();
	                                    while ($fila = $resultado->fetch()) {
	                                ?> 
	                                        <option value="<?php echo $fila['No_Empleado']; ?>"> <?php echo $fila["nombre"]; ?></option>
	                                <?php
	                                    }
	                                ?>
	                            </select>
	                        </div>                  
	                    </div>

	                    <div class="row form-group">
                            <label class="col-sm-3 control-label">Especificar</label>
                            <div class="col-sm-4">                            
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Especifique el nombre del evento">
                            </div>
                        </div>
                    
            		</form>
        		</div>
        	</div>
        </div>
    </div>
</div>