<?php
    $maindir = "../../";

    if(isset($_GET['contenido'])){
        $contenido = $_GET['contenido'];
    }else{
        $contenido = 'reservaciones';
    }

    require_once($maindir."funciones/check_session.php");

    require_once($maindir."funciones/timeout.php");

    require_once($maindir."pages/navbar.php");

    if(!isset( $_SESSION['user_id'] )){
      header('Location: '.$maindir.'login/logout.php?code=100');
      exit();
    }
  
?>

<html lang ="en">
<body>
	<?php
		require_once($maindir."pages/reservaciones/rsv_menu.php");
	?>

<div class="container-fluid">
    <!--<div class="row">-->
        <div class="col-sm-10">
            <div id="contenedor" class="content-panel">
    			<div class="col-sm-14">
                    <div id="contenedor2" class="content-panel">
                        <?php 
                            require_once($maindir . 'pages/reservaciones/rsv_reservaciones.php');
                        ?>
                    </div>
                </div> 
            </div>
        </div>
        <div class="panel-body">
        </div>
        <!-- /#page-wrapper -->
    <!--</div>-->
    <!-- /#wrapper -->
</div>

<!--	-->
</body>
</html>

 

