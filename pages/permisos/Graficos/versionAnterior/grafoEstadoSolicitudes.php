<?php

    $maindir = "../../../";

/* Accedemos a la base de datos */
include ($maindir.'conexion/conn.php');
session_start();
$nEmp='';
$q='';
$idusuario = $_SESSION['user_id'];
$q = 'SELECT No_Empleado as Empleado FROM usuario WHERE id_Usuario = ' .$idusuario;
$resultado = mysql_query($q);
for($i = 0 ; ; $i++){
    $fila = mysql_fetch_array($resultado);
    if ($i == 0){
        $nEmp = $fila["Empleado"];
        break;
    }else{
        $mensaje="Error en la obtencion de datos";
        $codMensaje =0;
    }
}  



try{

    $query = "SELECT estado, count(id_Permisos) as 'NUMERO_SOLICITUDES' from permisos where No_Empleado = " .$nEmp. " and fecha_solicitud between CAST(CONCAT(YEAR(NOW()), '-', MONTH(NOW()), '-01') as date) and CAST(LAST_DAY(NOW()) as date) GROUP by estado;";
    $result = mysql_query($query, $conexion) or die("error en la consulta1");
    
    $codMensaje = 1;
  }
  catch(PDOExecption $e){
    $mensaje="Error en la obtencion de datos";
    $codMensaje =0;
  }

?>
    
<script>
$(function () {
    $(document).ready(function () {

        // Build the chart
        $('#graficaEstadoSolicitudes').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Solicitudes por Estado',
                colorByPoint: true,
                data: [
                <?php
                    $numItems = mysql_num_rows($result);
                    for($i = 0 ;$fila = mysql_fetch_array($result); $i++){
                        if ($i == 1){
                            $nombre = $fila["estado"];
                            $y = $fila["NUMERO_SOLICITUDES"];
                            echo "{name: '".$nombre."', y: ".$y.",
                               sliced: true,
                               selected: true }";
                        }else{
                            $nombre = $fila["estado"];
                            $y = $fila["NUMERO_SOLICITUDES"];
                            echo "{name: '".$nombre."', y: ".$y."}";
                        }
                        if($i != $numItems) {
                            echo ",";
                        }
                    }  
                ?>]
            }]
        });
    });
});

</script>
<div id ="graficaEstadoSolicitudes"></div>