 <!-- libreria para generar graficos -->
<link href="css/datepicker.css" rel="stylesheet">
<link href="css/prettify.css" rel="stylesheet">

<script src="js/prettify.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/jsPDF/jspdf.js"></script>
<!-- Procedimento para obtener las solitudes del sistema -->
<script>

/* Cargar la grafica de solicitudes la cual nos muestra las graficas que estan activas en el sistema 
 y tambien las desactivas */
cargarGraficaSolicitudes();

/* Carga la grafica de estudiantes con el numero de Solicitudes realizadas */
cargarGraficaTipos();

/* Carga la tabla con las solicitudes activas en el sistema */

/* Funcion para cargar dentro del div TipoSolicitudes los graficos en la direccion indicada */
function cargarGraficaSolicitudes(){
    $("#EstadoSolicitudes").load("pages/permisos/Graficos/grafoEstadoSolicitudes.php");
}

function cargarGraficaTipos(){
    $("#TipoSolicitudes").load("pages/permisos/Graficos/grafoTipoSolicitudes.php");
}
/* Funcion que llena el cuerpo de la tabla con las solicitudes en el sitema para luego llegar la tabla */

</script>


<!-- En esta seccion se muestran los primeros dos divs, el primero muestra el estado  de solicitudes 
    y la cantidad de ellas.
    El segundo el numero de estudiantes dependiendo el tipo de estudiante -->

<!-- div para mostrar errores en el servidor -->
<div class = "col-md-12" id = "notificaciones"></div>
<div class = "col-md-12">
    <div class = "col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <label><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Grafico Mensual de Solicitudes por Estado </label>
            </div>
            <div class="panel-body" id = "EstadoSolicitudes"></div>
        </div>
    </div>
    <div class = "col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <label><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Grafico Mensual de Solicitudes por Tipo </label>
            </div>
            <div class="panel-body" id= "TipoSolicitudes">
            </div>
        </div>
    </div>
</div>





