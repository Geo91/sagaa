<?php
	$sql = "SELECT COUNT(gi_administradores.id_administrador) as 'conteo' FROM gi_administradores WHERE gi_administradores.n_empleado = (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = :pU)";
	$result = $db->prepare($sql);
	$result->bindParam(":pU",$idusuario);
	$result->execute();
	$row = $result->fetch();
	$nvl = $row['conteo'];
?>

<div class="col-sm-2">
	   <ul class="list-unstyled">
	   
	   <?php
     		$url = 'pages/permisos/permisos_principal.php?contenido=permisos';
     		echo <<<HTML
     			<h5><li>
                	<a id="inicio" href="javascript:ajax_('$url');"><i class="fa fa-home"></i> Inicio de Permisos</a>
            	</li></h5><br>
HTML;
		?>

		<li class="nav-header"> <a href="#" class = "collapsed" data-toggle="collapse" aria-expanded="false" data-target="#userMenu2">
		<h5> <i class="fa fa-edit fa-fw" ></i> Solicitudes <i class="glyphicon glyphicon-chevron-down"></i></h5>
				
			<ul class="list-unstyled collapse" id="userMenu2">
				<li>
					<a id="solicitud" href="#">Solicitud Personal</a>
				</li>
					
				<?php						

					if($rol==30 or $rol==50 or $rol == 100 or $rol == 60 or $rol == 55 or $rol == 45){
						echo "<li><a id ='revision' href='#'>Revisión</a></li>";
					}						
				?>

				<li>
					<a id='nE' href='#'>Mis Solicitudes</a>
				</li>

				<li>
					<a id='vacaciones' href='#'>Vacaciones</a>
				</li>
			</ul>
		</li>

		<li class="nav-header"> <a href="#" class = "collapsed" data-toggle="collapse" aria-expanded="false" data-target="#userMenu3">
		<h5> <i class="glyphicon glyphicon-list-alt" ></i> Informe de Inasistencias <i class="glyphicon glyphicon-chevron-down"></i></h5>
				
			<ul class="list-unstyled collapse" id="userMenu3">
				<?php
					if($rol == 100 or $rol == 40 or $rol == 45){
						echo "<li><a id='informe' href='#''>Cargar Documento</a></li>";
					}						
				?>

				<li>
					<a id='justificar' href='#'>Justificaciones</a>
				</li>

				<?php						

					if($nvl == 1){
						echo "<li><a id ='revisarjust' href='#'>Revisión</a></li>";
					}

					//if($rol == 100 or $rol == 40 or $rol == 45){
						//echo "<li><a id='gOficio' href='#''>Generar Oficio</a></li>";
					//}						
				?>

			</ul>
		</li>

		<!-- /.nav-second-level -->

		<li class='nav-header'> <a href='#' data-toggle='collapse' data-target='#userMenu4'>
			<h5><i class='glyphicon glyphicon-book'></i> Reportes <i class='glyphicon glyphicon-chevron-down'></i></h5>
				<ul class='list-unstyled collapse' id='userMenu4'>
					<li><a id='reportepersonal' href='#'><i class='fa fa-table fa-fw'></i>Mi Reporte Personal</a></li>
			<?php
				if($rol==50 or $rol==30 or $rol == 100 or $rol == 60 or $rol == 55){	
					echo "<li><a id='reportesgraficos' href='#'><i class='fa fa-table fa-fw'></i>Gráficos</a></li>";
					//echo "<li><a id='reportetotal' href='#'><i class='fa fa-table fa-fw'></i>Reporte Mensual</a></li>";
					echo "<li><a id='reportemotivos' href='#'><i class='fa fa-table fa-fw'></i>Reporte por Motivos</a></li>";
					//echo "<li><a id='reportetipos' href='#'><i class='fa fa-table fa-fw'></i>Reporte por tipo de permiso</a></li>";*/
					if($rol == 50 or $rol == 100){
						echo "<li><a id='reportedepto' href='#'><i class='fa fa-table fa-fw'></i>Reporte por Departamento</a></li>";
					}
				}					
	            
	        ?>
	        	</ul>
	    </li>

	    <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#userMenu"> 
			<?php 
				if($rol == 100 or $rol == 60){
					echo "<li><h5><i class='glyphicon glyphicon-wrench'></i> Mantenimiento 
					          <i class='glyphicon glyphicon-chevron-down'></i></h5>";

					echo "<ul class='list-unstyled collapse' id='userMenu'>";
						if($rol == 60){
							echo "<li><a id='asignar' href='#'>Asignar Vacaciones</a></li>";
						}else{
							echo "<li><a id='asignar' href='#'>Asignar Vacaciones</a></li>
							<li><a id='asistencia' href='#'>Asistencia a Usuarios</a></li>
					        <li><a id='motivos' href='#'>Motivos</a></li> 
					        <li><a id='edificios' href='#'>Edificios</a></li>
					        <li><a id='TipoDePermisos' href='#'>Tipo de Permisos</a></li>  
					        </ul></li>";
						}	
				}
			?>
		</li>
                <!-- /.sidebar-collapse -->
 </div> 
			
 <script type="text/javascript" src="js/gestion_permisos/principal.js" ></script> 
