<?php
    $maindir = "../../../";
    include($maindir."conexion/config.inc.php");
    require_once("../ChekAutoIncrement.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");
    $query = "SELECT vacaciones_empleado.empleado_id, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) as 'nombre', vacaciones_empleado.año, vacaciones_empleado.dias, vacaciones_empleado.disponibilidad, vacaciones_empleado.caducidad from persona inner join (empleado inner join vacaciones_empleado on empleado.No_Empleado = vacaciones_empleado.empleado_id) on persona.N_identidad=empleado.N_identidad ORDER BY vacaciones_empleado.año ASC;";
?>

<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaAsignacionesV" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style ="display:none">Código</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Año</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Días</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Disponibilidad</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Caducidad</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Editar</th>                    
                </tr>
            </thead>
            <tbody>
                <?php
                    $result =$db->prepare($query);
                    $result->execute();
                    while ($fila = $result->fetch()) {
                        ?>
                        <tr data-id='<?php echo $fila["empleado_id"]; ?>'> 
                            <td style = "display:none"><?php echo $fila["empleado_id"]; ?></td>
                            <td><?php echo $fila["nombre"]; ?></td>
                            <td><?php echo $fila["año"]; ?></td>
                            <td><?php echo $fila["dias"]; ?></td>
                            <td><center><?php echo $fila["disponibilidad"]; ?></center></td>
                            <td><center><?php echo $fila["caducidad"]; ?></center></td>
                            <td>
                                <center>
                                    <button type="button" class="editarAsignacion btn btn-info glyphicon glyphicon-edit"  title="Editar Vacaciones Asignadas">
                                </center>
                            </td>           
                        </tr>
                <?php } ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div>

<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#tablaAsignacionesV')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaAsignacionesV').dataTable({
        "order": [[2,"asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
    /*$(document).on("click", ".eliminarAsignacion<?php //echo $auto_increment; ?>", function () {
        if (confirm("¿Está seguro de que desea eliminar este registro?")){
            codigoE = $(this).parents("tr").data("id");
            anio = $(this).parents("tr").data("anio");

            data={
                Codigo:codigo,
                anio:anio
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                success: eliminarAsignacion,
                timeout: 6000,
                error: problemas
            }); 
            return false;
        }
    });*/

</script>