<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $idUsuario = $_SESSION['user_id'];
    $idRol = $_SESSION['user_rol'];

    $query = "SELECT permisos.id_Permisos, permisos.No_Empleado, permisos.fecha_solicitud, vacaciones.año_actual, vacaciones.dias_año_actual, vacaciones.año_anterior, vacaciones.dias_año_anterior, vacaciones.total_dias, vacaciones.fecha_inicio, vacaciones.fecha_fin, (CASE permisos.estado WHEN 1 THEN 'Espera' WHEN 2 THEN 'Espera' WHEN 3 THEN 'Preaprobado' WHEN 4 THEN 'Denegado' WHEN 5 THEN 'Aprobado' END) as estadoPermiso, permisos.observacion FROM permisos, empleado, vacaciones WHERE permisos.No_Empleado=empleado.No_Empleado AND permisos.id_Permisos=vacaciones.id_solicitud AND ((permisos.estado = '4' AND permisos.fecha_solicitud BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND DATE_ADD(CURDATE(), INTERVAL 15 DAY)) OR (permisos.estado < 3 AND permisos.estado > 0) OR (permisos.estado = '5' AND permisos.impreso = '0')) AND permisos.No_Empleado IN (SELECT No_Empleado FROM usuario WHERE id_Usuario = '".$idUsuario."')";
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
                <div class="table-responsive">
                    <table id= "TablaGestionesVacaciones" border="0" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th style = 'display:none'>Codigo</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Fecha Gestión</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Días Año Actual</th> 
                                <th style="text-align:center;background-color:#386D95;color:white;">Días Año Anterior</th> 
                                <th style="text-align:center;background-color:#386D95;color:white;">Días Solicitados</th>  
                                <th style="text-align:center;background-color:#386D95;color:white;">Período</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Estado</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Observación</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Editar</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Documento</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Contenido de la tabla generado atravez de la consulta a 
                                la base de datos -->
                                <?php
                                    $result =$db->prepare($query);
                                    $result->execute();
                                    while ($fila = $result->fetch()) {
                                        ?>
                                        <tr data-id='<?php echo $fila["id_Permisos"]; ?>' data-dactual='<?php echo $fila["dias_año_actual"]?>' data-danterior='<?php echo $fila['dias_año_anterior']; ?>' data-fecha='<?php echo $fila["fecha_solicitud"]; ?>' data-emp='<?php echo $fila["No_Empleado"]?>'>
                                            <td style = "display:none"><?php echo $fila["id_Permisos"]; ?></td>
                                            <td><?php echo $fila["fecha_solicitud"]; ?></td>
                                            <td><?php echo $fila["año_actual"]." / ".$fila["dias_año_actual"]; ?></td>
                                            <td><?php echo $fila["año_anterior"]." / ".$fila["dias_año_anterior"]; ?></td>
                                            <td><?php echo $fila["total_dias"]; ?></td>
                                            <td><?php echo $fila["fecha_inicio"]." al ".$fila["fecha_fin"]; ?></td>
                                            <td><?php echo $fila["estadoPermiso"]; ?></td>
                                            <td><?php echo $fila["observacion"]; ?></td>
                                            <td>
                                                <center>
                                                    <button type="button" class="editarGestion btn btn-info"  title="Editar la solicitud de permiso."><i class="fa fa-pencil-square-o"></i></button>
                                                </center>
                                            </td>
                                            <td>
                                                <center>
                                                    <button type="button" class="subirDocumento<?php echo $auto_increment; ?> btn btn-success"  title="Subir Documento Soporte."><span class="glyphicon glyphicon-open"></span></button>
                                                </center>
                                            </td>     
                                        </tr>
                                <?php } ?>
                        </tbody>
                    </table>       
                </div>
        </div>                
    </div>
</div>

<script type="text/javascript">   

    $('#TablaGestionesVacaciones')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#TablaGestionesVacaciones').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {
        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)",
            "search": "Buscar",
            "paginate":
                {
                    "previous": "Anterior",
                    "next" : "Siguiente"
                }
        }
    });

    $(document).on("click", ".subirDocumento<?php echo $auto_increment; ?>", function () {
        var estado = $(this).parents("tr").find("td").eq(6).html();
        if(estado === 'Aprobado' || estado === 'Preaprobado'){
            alert("La solicitud ya ha sido revisada, no puede subir documentos soporte.");
        }else{
            data={
                id:$(this).parents("tr").data("id"),
                emp:$(this).parents("tr").data("emp")
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                success: subirDoc,
                timeout: 6000,
                error: problemas
            }); 
            return false;
        }
    });

</script>
