//$("#tablaGestiones").load('pages/permisos/vacaciones/datosGestionVacaciones.php');

$(document).ready(function(){

    $("#panelVacaciones").click(function(event) {
        event.preventDefault();
        $('#flechaVacaciones').toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
    });  

    $("#formSolicitudVacaciones").submit(function(e) {
        e.preventDefault();
        if (validar() === true){
            data={
                anioActual: $('#Actual').val(),
                actualDias: $('#diasActual').val(),
                anioAnterior: $('#Anterior').val(),
                anteriorDias: $('#diasAnterior').val(),
                fechaI: $('#fechaInicio').val()
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                beforeSend: inicioEnvio,
                success: enviarGestion,
                timeout: 8000,
                error: problemas
            }); 
            return false;
        }else{
            alert("No pueden ingresarse gestiones con cero días solicitados");
        }
    });

    $("#formEditarSolicitudVacaciones").submit(function(e) {
        e.preventDefault();
        if (validar() === true){
            data={
                codigoG: $('#codigoGestion').val(),
                anioActual: $('#modalActual').val(),
                actualDias: $('#modaldiasActual').val(),
                anioAnterior: $('#modalAnterior').val(),
                anteriorDias: $('#modaldiasAnterior').val(),
                fechaI: $('#modalfechaInicio').val()
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                beforeSend: inicioEnvio,
                success: editarGestionVacaciones,
                timeout: 8000,
                error: problemas
            }); 
            return false;
        }else{
            alert("No pueden ingresarse gestiones con cero días solicitados");
        }

    });

    $("#Actual").click(function(event) {
        event.preventDefault();
        var valor = $('#Actual').val();
        if (valor==1) {
            $('#lbActual').show('fast');
            $('#diasActual').show('fast');
            document.getElementById("diasActual").min = 1;
            $('#diasActual').val(1);
            document.getElementById("diasActual").disabled = false;
        }else{
            $('#lbActual').hide('fast');
            $('#diasActual').hide('fast');
            document.getElementById("diasActual").min = 0;
            $('#diasActual').val(0);
            document.getElementById("diasActual").disabled = true;
        }
    });

    $("#Anterior").click(function(event) {
        event.preventDefault();
        var valor = $('#Anterior').val();
        if (valor==1) {
            $('#lbAnterior').show('fast');
            $('#diasAnterior').show('fast');
            document.getElementById("diasAnterior").min = 1;
            $('#diasAnterior').val(1);
            document.getElementById("diasAnterior").disabled = false;
        }else{
            $('#lbAnterior').hide('fast');
            $('#diasAnterior').hide('fast');
            document.getElementById("diasAnterior").min = 0;
            $('#diasAnterior').val(0);
            document.getElementById("diasAnterior").disabled = true;
        }
    });

    $("#modalActual").click(function(event) {
        event.preventDefault();
        var valor = $('#modalActual').val();
        if (valor==1) {
            $('#modallbActual').show('fast');
            $('#modaldiasActual').show('fast');
            document.getElementById("modaldiasActual").min = 1;
            $('#modaldiasActual').val(1);
            document.getElementById("modaldiasActual").disabled = false;
        }else{
            $('#modallbActual').hide('fast');
            $('#modaldiasActual').hide('fast');
            document.getElementById("modaldiasActual").min = 0;
            $('#modaldiasActual').val(0);
            document.getElementById("modaldiasActual").disabled = true;
        }
    });

    $("#modalAnterior").click(function(event) {
        event.preventDefault();
        var valor = $('#modalAnterior').val();
        if (valor==1) {
            $('#modallbAnterior').show('fast');
            $('#modaldiasAnterior').show('fast');
            document.getElementById("modaldiasAnterior").min = 1;
            $('#modaldiasAnterior').val(1);
            document.getElementById("modaldiasAnterior").disabled = false;
        }else{
            $('#modallbAnterior').hide('fast');
            $('#modaldiasAnterior').hide('fast');
            document.getElementById("diasAnterior").min = 0;
            $('#modaldiasAnterior').val(0);
            document.getElementById("diasAnterior").disabled = true;
        }
    });

});

$(document).on("click", ".editarGestion", function () {
    var estado = $(this).parents("tr").find("td").eq(6).html();
    var id = $(this).parents("tr").data("id");
    var dActuales = $(this).parents("tr").data("dactual");
    var dAnteriores = $(this).parents("tr").data("danterior");
    var fecha = $(this).parents("tr").data("fecha");
    
    if(estado === 'Aprobado' || estado === 'Preaprobado'){
        alert("La gestión ya ha sido revisada, no puede editarse");
    }else{

        if(dActuales > 0){
            actual = 1;
        }else{
            actual = 0;
            $('#modallbActual').hide('fast');
            $('#modaldiasActual').hide('fast');
            document.getElementById("modaldiasActual").min = 0;
            $('#modaldiasActual').val(0);
            document.getElementById("modaldiasActual").disabled = true;
        }
        if(dAnteriores > 0){
            anterior = 1;
        }else{
            anterior = 0;
            $('#modallbAnterior').hide('fast');
            $('#modaldiasAnterior').hide('fast');
            document.getElementById("modaldiasAnterior").min = 0;
            $('#modaldiasAnterior').val(0);
            document.getElementById("modaldiasAnterior").disabled = true;
        }
        
        $("#codigoGestion").val(id);
        $("#modalAnterior").val(anterior);
        $("#modaldiasAnterior").val(dAnteriores);
        $("#modalActual").val(actual);
        $("#modaldiasActual").val(dActuales);
        $("#modalfechaInicio").val(fecha);

        $("#modalEditarGestion").modal('show');
    }
});


function inicioEnvio(){
    $("#divRespuesta").text('Cargando...');
}

function validar(){
    var dAct = $("#Actual").val();
    var dAnt = $("#Anterior").val();
    if(dAct === 0 && dAnt === 0){
        return false;
    }else{
        return true;
    }
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function enviarGestion(){
    $("#divRespuesta").load('pages/permisos/vacaciones/insertarRegistroVacaciones.php',data);
    $('#formSolicitudVacaciones').trigger("reset");
}

function editarGestionVacaciones(){
    $("#divRespuestaEditar").load('pages/permisos/vacaciones/editarGestion.php',data);
    $("#modalEditarGestion").modal('hide');
    $("#divRespuesta").text("");
}

function subirDoc(){
    $("#contenidoModalDoc").load('pages/permisos/vacaciones/subirDocVacaciones.php',data);
    $("#modalDoc").modal('show');
}