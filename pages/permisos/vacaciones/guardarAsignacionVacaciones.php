<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    if(isset($_POST['asignacion']) && isset($_POST['empleado']) && isset($_POST['anio']) && isset($_POST['dias'])) {

        $pcAsignacion = $_POST['asignacion'];
        $pcEmpleado = $_POST['empleado'];
        $pcAnio = $_POST['anio'];
        $pcDiasAsignados = $_POST['dias'];

        try{
            $consulta=$db->prepare("CALL SP_ASIGNAR_VACACIONES(?,?,?,?,@pcMensajeError)");
            $consulta->bindParam(1, $pcAsignacion, PDO::PARAM_INT);
            $consulta->bindParam(2, $pcAnio, PDO::PARAM_INT);
            $consulta->bindParam(3, $pcDiasAsignados, PDO::PARAM_INT);
            $consulta->bindParam(4, $pcEmpleado, PDO::PARAM_INT);

            $resultado=$consulta->execute();
            $output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
            $mensaje = $output['@pcMensajeError'];

            if ($mensaje == NULL){
                echo mensajes('La asignación de vacaciones se completó satisfactoriamente', 'verde');
            }else{
                echo mensajes('Error!'.$mensaje, 'rojo');
            }
        }catch(PDOexception $e){
            echo mensajes('Error!'. $e, 'rojo');
        }
    }else{
        echo mensajes("Hubo un error al intentar asignar las vacaciones. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
    }

?>

<script type="text/javascript">
    $("#tablaAsignaciones").load('pages/permisos/vacaciones/DatosAsignaciones.php');
</script>