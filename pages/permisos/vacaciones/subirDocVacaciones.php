<?php 
    $maindir = "../../../";
    include($maindir."conexion/config.inc.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");

    if(isset($_POST['id']) && isset($_POST['emp'])) {

        $vCod = $_POST['id'];
        $vNum = $_POST['emp'];
    }

    $identificador = $vNum."-".$vCod;

?>

<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel" style = "color:white">Subir Documento</h4>
</div>

<div class="modal-body">

    <form class="form-horizontal" id="formDocPermiso" enctype="multipart/form-data" method="POST">
      
        <input style="display:none" type="text" id="identificador" name="identificador" value = "<?php echo $identificador; ?>" disabled="">                                          
    
        <label>Seleccione el documento: </label>
        <br>
        <input type="file" id="userImage" name="userImage" accept=".pdf">
        <br>

        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary" id="foto">Guardar</button>

    </form>
</div>

<script>

$(document).ready(function(){

    $("#formDocPermiso").submit(function(e) {
        e.preventDefault();
        var perm = "<?php echo $vCod?>";
        var id = "<?php echo $identificador; ?>";

        var parametros = new FormData($("#formDocPermiso")[0]);
        parametros.append("cod", perm);
        parametros.append("name", id);
        $.ajax({
            url: "pages/permisos/vacaciones/cargarArchivo.php",
            type: "POST",
            data: parametros,
            contentType: false,
            processData: false,
        })
            .done(function(res){
                $("#divRespuestaEditar").html(res);
                $("#modalDoc").modal("hide");
            });

    });

});

</script>


<!--******************Script de interacción*****************-->
<script type="text/javascript" src="pages/permisos/vacaciones/Script.js"></script>