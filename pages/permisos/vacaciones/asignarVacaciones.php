<?php
    $maindir = "../../../";
    include($maindir."conexion/config.inc.php");
    require_once("../ChekAutoIncrement.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");

    $query = "SELECT No_Empleado, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) as 'nombre' FROM persona inner join empleado on persona.N_identidad=empleado.N_identidad WHERE empleado.No_Empleado IN (SELECT empleado_has_cargo.No_Empleado FROM empleado_has_cargo WHERE empleado_has_cargo.ID_cargo NOT IN ('10','116'));";
?>

<!--*******************************ENCABEZADO********************************-->
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong>Gestión de Vacaciones</strong></center></h3>
    </div>
</div> 

<div  id="divRespuesta"></div>

<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Asignación de Vacaciones </div>

    <div class="panel-body" >
        <div>
            <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden</center></div>
        </div>
        <div class="row">
            <div class="col-lg-12">
            <!--************************formulario*********************-->
                <form class="form-horizontal" role="form" id="formVacaciones" name="formVacaciones">
                    <div class="row form-group">                    
                        <label class=" col-sm-2 control-label"> Asignación </label>                       
                        <div class="col-sm-2">
                            <select id="Asignar" name="Asignar" size="2" required>
                                <option value="0"> Global </option>
                                <option value="1"> Personalizada </option>
                            </select>            
                        </div> 
                    </div>

                    <div class = "row form-group">
                        <label id = "lbEmpleado" class=" col-sm-2 control-label"> Empleado </label>     
                        <div class="col-sm-4">                   
                            <select class = "form-control" id="empleado" name="empleado" value="0" required>
                                <option value = "0"> Seleccione una opción </option>
                                <?php
                                    $resultado = $db->prepare($query);
                                    $resultado->execute();
                                    while ($fila = $resultado->fetch()) {
                                ?> 
                                        <option value="<?php echo $fila['No_Empleado']; ?>"> <?php echo $fila["nombre"]; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>                  
                    </div>

                    <div class = "row form-group">
                        <label class="col-sm-2 control-label">Año</label>
                        <div class="col-sm-2">
                            <input type="number" id="anio" name="año" placeholder = "####" min = "2016" value = "2016" required>
                        </div>
                    </div>

                    <div class = "row form-group">
                        <label class="col-sm-2 control-label">Días Asignados</label>
                        <div class="col-sm-2">
                            <input type="number" id="diasAsignados" name="diasAsignados" min = "0" max="28" value = "0" required>
                        </div>
                    </div>

                    <div class="row">
                        <label class="control-label col-sm-2"></label>
                        <div class="col-sm-5">
                            <p aling ="right">
                                <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" ><span class="glyphicon glyphicon-floppy-disk"> Guardar </span></button> 
                            </p>          
                        </div>                            
                    </div>    

                </form>
            <!--************************formulario*********************-->
            </div>
        </div>
    </div>                                    
</div>

<!--************************TABLA DE DATOS************************-->
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center> Historial de Vacaciones Asignadas </center></h3>
    </div>
</div>

<div id="divRespuestaEditar">
</div>

<div id="tablaAsignaciones">
</div>

<!--******************modal de edicion*****************-->
<div class="modal fade" id="modalEditarAsignacion" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header" style = "background-color:#0FA6C3">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style = "color:white">Editar vacaciones asignadas</h4>
        </div>
        <div class="modal-body">
            <!--************************formulario*********************-->
                <form class="form-horizontal" role="form" id="formEditarVacacionesAsignadas" name="formEditarVacacionesAsignadas">
                    
                    <div class = "row form-group">
                        <label id = "lbEmpleado" class=" col-sm-2 control-label"> Empleado </label>     
                        <div class="col-sm-8">                   
                            <select class = "form-control" id="modalEmpleado" name="modalEmpleado" value="0" required disabled = "true">
                                <option value = "0"> Seleccione una opción </option>
                                <?php
                                    $query2 = "SELECT No_Empleado, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) as 'nombre' FROM persona inner join empleado on persona.N_identidad=empleado.N_identidad WHERE empleado.No_Empleado IN (SELECT empleado_has_cargo.No_Empleado FROM empleado_has_cargo WHERE empleado_has_cargo.ID_cargo NOT IN ('10','116'));";
                                    $resultado = $db->prepare($query2);
                                    $resultado->execute();
                                    while ($fila2 = $resultado->fetch()) {
                                ?> 
                                        <option value="<?php echo $fila2['No_Empleado']; ?>"> <?php echo $fila2["nombre"]; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>                  
                    </div>

                    <div class = "row form-group">
                        <label class="col-sm-2 control-label">Año</label>
                        <div class="col-sm-2">
                            <input type="number" id="modalAnio" name="modalAño" min = "2016" required disabled = "true">
                        </div>
                    </div>

                    <div class = "row form-group">
                        <label class="col-sm-2 control-label">Días Asignados</label>
                        <div class="col-sm-2">
                            <input type="number" id="modaldiasAsignados" name="modaldiasAsignados" min = "0" max="28" value = "0" required>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button  id="guardarCambios" class="btn btn-primary"> Guardar </button>
                    </div>
                </form>
            <!--************************formulario*********************-->
        </div>
      </div>
    </div>
</div>

<!--***************************SCRIPT DEL FORMULARIO*************************-->
<script type = "text/javascript" src = "pages/permisos/vacaciones/ScriptAsignarV.js"></script>