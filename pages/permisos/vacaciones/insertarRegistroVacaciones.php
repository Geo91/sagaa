<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    session_start();
    $idUsuario = $_SESSION['user_id'];

    if(isset($_POST['anioActual']) && isset($_POST['actualDias']) && isset($_POST['anioAnterior']) && isset($_POST['anteriorDias']) && isset($_POST['fechaI'])){

		$pcActual = $_POST['anioActual'];
		$pcDiasActual = $_POST['actualDias'];
		$pcAnterior = $_POST['anioAnterior'];
		$pcDiasAnterior = $_POST['anteriorDias'];
		$pcFechaI = $_POST['fechaI'];
		$pcTotalDias = $pcDiasActual + $pcDiasAnterior;

        try{
			$consulta=$db->prepare("CALL SP_SOLICITAR_VACACIONES(?,?,?,?,?,?,?,@pcMensajeError)");
			$consulta->bindParam(1, $pcActual, PDO::PARAM_INT);
			$consulta->bindParam(2, $pcDiasActual, PDO::PARAM_INT);
			$consulta->bindParam(3, $pcAnterior, PDO::PARAM_INT);
			$consulta->bindParam(4, $pcDiasAnterior, PDO::PARAM_INT);
			$consulta->bindParam(5, $pcTotalDias, PDO::PARAM_INT);
			$consulta->bindParam(6, $pcFechaI, PDO::PARAM_STR);
			$consulta->bindParam(7, $idUsuario, PDO::PARAM_INT);

			$resultado=$consulta->execute();
			$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
			$mensaje = $output['@pcMensajeError'];

			if ($mensaje == NULL){
			    echo mensajes('Su gestión de vacaciones ha sido ingresada exitosamente', 'verde');
			}else{
			    echo mensajes('Error!'.$mensaje, 'rojo');
			}
		}catch(PDOexception $e){
			echo mensajes('Error!'. $e, 'rojo');
		}
	}else{
		echo mensajes("Hubo un error al intentar ingresar la gestión de vacaciones. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
	}

?>

<script type="text/javascript">
	$("#tablaGestiones").load('pages/permisos/vacaciones/datosGestionVacaciones.php');
</script>