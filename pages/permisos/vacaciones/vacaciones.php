<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    include($maindir."conexion/config.inc.php");

    $idUser = $_SESSION["user_id"];
    $consulta = "SELECT vacaciones_empleado.año, vacaciones_empleado.dias, vacaciones_empleado.disponibilidad, vacaciones_empleado.caducidad from vacaciones_empleado where vacaciones_empleado.empleado_id in (SELECT No_Empleado from usuario where usuario.id_Usuario = '$idUser') and vacaciones_empleado.caducidad > NOW() ORDER BY vacaciones_empleado.año DESC LIMIT 1";

    $consulta2 = "SELECT vacaciones_empleado.año, vacaciones_empleado.dias, vacaciones_empleado.disponibilidad, vacaciones_empleado.caducidad from vacaciones_empleado where vacaciones_empleado.empleado_id in (SELECT No_Empleado from usuario where usuario.id_Usuario = 'idUser') and vacaciones_empleado.caducidad > NOW() ORDER BY vacaciones_empleado.año ASC LIMIT 1";
?>

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong>Gestión de Vacaciones</strong></center></h3>
    </div>
</div> 

<div  id="divRespuesta"></div>
      

<div class="row">
    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingTwo">
            <h4 class="panel-title">
                <span id="flechaVacaciones" class="glyphicon glyphicon-chevron-down"></span> Gestión de Vacaciones
            </h4>
        </div>
        
            <div class="panel-body">

                <div id="contenedorVacaciones">
                    <?php require_once("solicitudVacaciones.php"); ?>
                </div>

                <!--***********************INFORMACIÓN*********************-->
                <div id="infoPanel1" class="col-sm-3">
                    <div class="row">
                        <div class="col-md-offset-8"></div>
                    </div>
                    <div class="row">
                        <div class="panel panel-info">

                            <div class="panel-heading"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Vacaciones Asignadas</div>

                            <div class="panel-body" >
                                <div class="row">
                                    <!--************************Datos*********************-->
                                    <div class="col-lg-12">
                                    
                                        <?php
                                            $resultado = $db->prepare($consulta);
                                            $resultado->execute();
                                            while ($fila = $resultado->fetch()) {
                                        ?> 
                                        <br>
                                            <div class="row" >                    
                                                <label class=" col-sm-3 control-label">Año</label>
                                                <div><?php echo $fila["año"]; ?></div>
                                            </div>                     
                                            <div class="row">                          
                                                <label class=" col-sm-3" > Días</label>
                                                <div><?php echo $fila["dias"]; ?></div>
                                            </div>
                                            <div class="row">                          
                                                <label class=" col-sm-3" > Disponibles desde: </label>
                                                <br>
                                                <div><?php echo $fila["disponibilidad"]; ?></div>
                                            </div>
                                            <div class="row">                          
                                                <label class=" col-sm-4" > Caducan: </label>
                                                <div><?php echo $fila["caducidad"]; ?></div>
                                            </div>
                                        <?php
                                            }
                                        ?>
<hr>
                                        <?php
                                            $resultado2 = $db->prepare($consulta2);
                                            $resultado2->execute();
                                            while ($fila2 = $resultado2->fetch()) {
                                        ?> 
                                            <div class="row" >
                                                <label class=" col-sm-3 control-label">Año</label>
                                                <div><?php echo $fila2["año"]; ?></div>
                                            </div>                     
                                            <div class="row">
                                                <label class=" col-sm-3" > Días</label>
                                                <div><?php echo $fila2["dias"]; ?></div>
                                            </div>
                                            <div class="row">
                                                <label class=" col-sm-3" > Disponibles desde: </label>
                                                <br>
                                                <div><?php echo $fila2["disponibilidad"]; ?></div>
                                            </div>
                                            <div class="row">
                                                <label class=" col-sm-4" > Caducan: </label>
                                                <div><?php echo $fila2["caducidad"]; ?></div>
                                            </div>
                                        <br>
                                        <?php
                                            }
                                        ?>

                                    </div>   
                                    <!--************************Datos*********************-->

                                </div>
                            </div>
                        </div>                                    
                    </div>
                </div> 
                <!--*******************FIN DE INFORMACIÓN*********************-->
            </div>
        </div>
    </div>
</div>


<!--tabla de gestiones de vacaciones-->
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center> Historial de Gestiones de Vacaciones </center></h3>
    </div>
</div> 

<div id="divRespuestaEditar">
</div>

<div id="tablaGestiones">
    <?php include("datosGestionVacaciones.php"); ?>
</div>

<!--************MODAL DE EDICIÓN********************-->
<div class="modal fade" id="modalEditarGestion" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header" style = "background-color:#0FA6C3">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style = "color:white">Editar Solicitud de Vacaciones</h4>
        </div>
        <div class="modal-body">
            <div id= "noti1" class="col-lg-12 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden<br>La fecha de finalización de las vacaciones se calculará automáticamente en base al total de días solicitados<br>En caso de usar mozilla firefox el formato de fecha debe ser 'año-mes-día' </center>
            </div>
            <!--************************formulario*********************-->           
                <form class="form-horizontal" role="form" id="formEditarSolicitudVacaciones" name="formEditarSolicitudVacaciones">

                    <div class="row form-group">
                        <label class ="col-sm-2 control-label" hidden="true">Código</label>
                        <div class = "col-sm-2">
                            <input type = "text" id = "codigoGestion" hidden="true" disabled="true">
                        </div>
                    </div>

                    <div class="row form-group"> 
                        <label class=" col-sm-2 control-label"> Año Anterior </label>  
                        <div class="col-sm-2">
                            <select id="modalAnterior" name="Anterior" size="2" required>
                                <option value="0"> NO </option>
                                <option value="1"> SI </option>
                            </select>
                        </div>               

                        <label id = "modallbAnterior" class="col-sm-3 control-label">Días Año Anterior</label>        
                        <div class="col-xs-3">                   
                            <input type="number" id="modaldiasAnterior" name="diasAñoAnterior" min="1" max="30" value="1" required>
                        </div>              
                    </div>

                    <div class="row form-group">                    
                        <label class=" col-sm-2 control-label"> Año Actual </label>                       
                        <div class="col-sm-2">
                            <select id="modalActual" name="Actual" size="2" required>
                                <option value="0"> NO </option>
                                <option value="1"> SI </option>
                            </select>            
                        </div>                  

                        <label id = "modallbActual" class="col-sm-3 control-label">Días Año Actual</label>     
                        <div class="col-xs-1">                   
                            <input type="number" id="modaldiasActual" name="diasAñoActual" min="1" max="30" value="1" required>
                        </div>                  
                    </div>

                    <div class="row form-group">
                        <label class="col-sm-2 control-label">Fecha Inicio</label>
                        <div class="col-sm-2">
                            <input type="date" id="modalfechaInicio" name="datepicker" required><span class="">
                        </div>        
                    </div>

                    <div class="modal-footer">
                        <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" > Enviar <span class="glyphicon glyphicon-circle-arrow-right"></span></button>                                   
                    </div>    

                </form>
            <!--************************formulario*********************-->
        </div>
      </div>
    </div>
</div>

<!-- Modal para revisar las solicitudes de permiso -->
<div class="modal fade" id="modalDoc" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div id="contenidoModalDoc">
        </div>
      </div>
    </div>
</div>

<!--******************JAVASCRIPT*****************-->
<script type="text/javascript" src="pages/permisos/vacaciones/Script.js"></script>