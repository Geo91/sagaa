<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    if(isset($_POST['codigoE']) && isset($_POST['anio']) && isset($_POST['diasAsignados'])) {

        $pcEmpleado = $_POST['codigoE'];
        $pcAnio = $_POST['anio'];
        $pcDiasAsignados = $_POST['diasAsignados'];

        try{
            $consulta=$db->prepare("CALL SP_MODIFICAR_VACACIONES_ASIGNADAS(?,?,?,@pcMensajeError)");
            $consulta->bindParam(1, $pcAnio, PDO::PARAM_INT);
            $consulta->bindParam(2, $pcDiasAsignados, PDO::PARAM_INT);
            $consulta->bindParam(3, $pcEmpleado, PDO::PARAM_INT);

            $resultado=$consulta->execute();
            $output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
            $mensaje = $output['@pcMensajeError'];

            if ($mensaje == NULL){
                echo mensajes('El registro se editó satisfactoriamente', 'verde');
            }else{
                echo mensajes('Error!'.$mensaje, 'rojo');
            }
        }catch(PDOexception $e){
            echo mensajes('Error!'. $e, 'rojo');
        }
    }else{
        echo mensajes("Hubo un error al intentar editar las vacaciones asignadas. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
    }

?>

<script type="text/javascript">
    $("#tablaAsignaciones").load('pages/permisos/vacaciones/DatosAsignaciones.php');
</script>