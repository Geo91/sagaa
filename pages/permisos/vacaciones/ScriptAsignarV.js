//cargar la tabla desde un principio
    $("#tablaAsignaciones").load('pages/permisos/vacaciones/DatosAsignaciones.php');

$(document).ready(function(){

    $("#formVacaciones").submit(function(e) {
        e.preventDefault();
        data={
            asignacion: $('#Asignar').val(),
            empleado: $('#empleado').val(),
            anio: $('#anio').val(),
            dias: $('#diasAsignados').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: enviarInformación,
            timeout: 6000,
            error: problemas
        }); 
        return false;
    });

    $("#formEditarVacacionesAsignadas").submit(function(e) {
        e.preventDefault();
        data={
            codigoE:$('#modalEmpleado').val(),
            anio:$('#modalAnio').val(),
            diasAsignados:$('#modaldiasAsignados').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: editarAsignacion,
            timeout: 6000,
            error: problemas
        }); 
        $("#modalEditarAsignacion").modal('hide');
        return false;
    });

    $("#Asignar").click(function(event) {
        event.preventDefault();
        var valor = $('#Asignar').val();
        if (valor==1) {
            //$('#dActual').show('fast');
            $('#lbEmpleado').show('fast');
            $('#empleado').show('fast');
            $('#empleado').val(0);
            document.getElementById("empleado").disabled = false;
        }else{
            //$('#dActual').hide('fast');
            $('#lbEmpleado').hide('fast');
            $('#empleado').hide('fast');
            $('#empleado').val(0);
            document.getElementById("empleado").disabled = true;
        }
    });

});

$(document).on("click", ".editarAsignacion", function () {
    codigo = $(this).parents("tr").data("id");
    year = $(this).parents("tr").find("td").eq(2).html();
    dias = $(this).parents("tr").find("td").eq(3).html();

    $("#modalEmpleado").val(codigo);
    $("#modalAnio").val(year);
    $("#modaldiasAsignados").val(dias);

    $("#modalEditarAsignacion").modal('show');
});

function inicioEnvio(){
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function enviarInformación(){
    $("#divRespuesta").load('pages/permisos/vacaciones/guardarAsignacionVacaciones.php',data);
    $('#formVacaciones').trigger("reset");
}

function eliminarAsignacion(){
    $("#divRespuestaEditar").load('pages//permisos/vacaciones/eliminarVacacionesAsignadas.php',data);
}

function editarAsignacion(){
    $("#divRespuestaEditar").load('pages/permisos/vacaciones/editarAsignacion.php',data);
    $("#modalEditarAsignacion").modal('hide');
}