********************************
--CREACION TABLA SALONES
CREATE TABLE `ccjj`.`rsv_salones` ( `id_salon` INT NOT NULL AUTO_INCREMENT , `nombre_salon` VARCHAR(100) NOT NULL , `capacidad` INT NOT NULL , `ubicacion` VARCHAR(100) NOT NULL , PRIMARY KEY (`id_salon`)) ENGINE = MyISAM;

ALTER TABLE `rsv_salones` ADD `habilitado` TINYINT(1) NOT NULL DEFAULT '1' AFTER `ubicacion`;
--realizado 09/10/2017
********************************
-- CREACION TABLA ACONDICIONAMIENTO SALONES
CREATE TABLE `ccjj`.`rsv_salon_acondicionamientos` ( `salon_id` INT NOT NULL , `acondicionamiento_id` INT NOT NULL ) ENGINE = MyISAM;


ALTER TABLE rsv_salon_acondicionamientos ADD FOREIGN KEY(salon_id) REFERENCES rsv_salones.id_salon ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE rsv_salon_acondicionamientos ADD FOREIGN KEY(acondicionamiento_id) REFERENCES ca_tipos_acondicionamientos.id_acondicionamiento ON DELETE CASCADE ON UPDATE CASCADE;
-- realizado 09/10/2017
**********************************
-- CREACION TABLA RESERVACIONES
CREATE TABLE `ccjj`.`rsv_reservaciones` ( `id_reservacion` INT NOT NULL AUTO_INCREMENT , `n_empleado` INT NOT NULL , `salon_id` INT NOT NULL , `fecha_inicio` DATE NOT NULL , `fecha_fin` DATE NOT NULL , `horaI` TIME NOT NULL , `horaF` TIME NOT NULL , `proposito` TINYINT NOT NULL , `clase_id` INT NULL , `evento` VARCHAR(200) NULL ,  PRIMARY KEY (`id_reservacion`)) ENGINE = MyISAM;

ALTER TABLE `rsv_reservaciones` CHANGE `clase_id` `clase_id` VARCHAR(7) NULL DEFAULT NULL;

ALTER TABLE `rsv_reservaciones` ADD `estado_reservacion` INT(1) NOT NULL AFTER `evento`;

ALTER TABLE rsv_reservaciones ADD COLUMN observacion VARCHAR(200);

ALTER TABLE `rsv_reservaciones` ADD `numcelular` INT(8) NOT NULL AFTER `evento`;

ALTER TABLE rsv_reservaciones ADD `correlativo` VARCHAR(10) NULL AFTER observacion;

ALTER TABLE rsv_reservaciones ADD FOREIGN KEY(estado_reservacion) REFERENCES estado_permiso.id_estado ON UPDATE CASCADE;
ALTER TABLE rsv_reservaciones ADD FOREIGN KEY(salon_id) REFERENCES rsv_salones.id_salon ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE rsv_reservaciones ADD FOREIGN KEY(n_empleado) REFERENCES empleado.No_Empleado ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE rsv_reservaciones ADD FOREIGN KEY(clase_id) REFERENCES ca_asignaturas.id_asignatura ON DELETE CASCADE ON UPDATE CASCADE;
-- realizado 09/10/2017
*************************************
--INSERCION DE DATOS A LA TABLAS SALONES
INSERT INTO `rsv_salones` (`id_salon`, `nombre_salon`, `capacidad`, `ubicacion`) VALUES (NULL, 'Sala de Juicios Orales José Cecilio del Valle', '60', 'Edificio Sala de Juicios Orales frente a Edificio Alma Mater'), (NULL, 'Salon Multiusos', '50', 'Instituto de Investigación Jurídica Edificio C2 Primer Piso'), (NULL, 'Sala de Juicio Oral', '80', 'Consultorio Jurídico Gratuito');

INSERT INTO `rsv_salon_acondicionamientos` (`salon_id`, `acondicionamiento_id`) VALUES ('1', '3'), ('1', '4'), ('1', '5'), ('2', '1'), ('2', '5'), ('2', '3'), ('3', '1'), ('3', '3'), ('3', '4');

*****************************
--PROCEDIMIENTO ALMACENADO PARA VISUALIZAR LOS DATOS DE LAS SOLICITUDES DE LOS SALONES
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_DATOS_SOLICITUDES_SALONES`()
BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
    
    END;
    
    SET vcTempMensajeError := 'Error al obtener la información de las solicitudes';
    
    SELECT rsv_reservaciones.id_reservacion, CONCAT(persona.Primer_nombre,' ',persona.Primer_apellido) as 'Empleado', rsv_salones.nombre_salon, rsv_reservaciones.fecha_inicio, rsv_reservaciones.fecha_fin, rsv_reservaciones.horaI, rsv_reservaciones.horaF, (CASE rsv_reservaciones.proposito WHEN '0' THEN 'Clase' WHEN '1' THEN 'Evento' END) as 'proposito' FROM persona INNER JOIN (empleado INNER JOIN (rsv_reservaciones INNER JOIN rsv_salones ON rsv_reservaciones.salon_id=rsv_salones.id_salon) ON empleado.No_Empleado=rsv_reservaciones.n_empleado) on persona.N_identidad=empleado.N_identidad WHERE rsv_reservaciones.estado_reservacion = '5';

END
-- realizado 09/10/2017
*******************************
-CREACION DE TABLA PARA ASIGNAR ADMINISTRADORES DE SALONES
CREATE TABLE `ccjj`.`rsv_administrador` ( `n_empleado` INT NOT NULL , `id_salon` INT NOT NULL , UNIQUE `emp` (`n_empleado`)) ENGINE = MyISAM;

INSERT INTO `rsv_administrador` (`n_empleado`, `id_salon`) VALUES ('13195', '1');
-- realizado 09/10/2017
******************************************************************************
--CREACION PROCEDIMIENTO ALMACENADO PARA ALMACENAR LAS SOLICITUDES DE RESERVACIONES DE LOS SALONES
CREATE DEFINER=`ddvderecho`@`localhost` PROCEDURE `SP_RSV_SOLICITAR_SALON`(IN `pcCodigo` INT, IN `pcFechaI` DATE, IN `pcFechaF` DATE, IN `pcHoraIn` TIME, IN `pcHoraFin` TIME, IN `pcProposito` INT, IN `pcClase` VARCHAR(7), IN `pcEvento` VARCHAR(200), IN `pcExterna` VARCHAR(200), IN `pcResp` VARCHAR(100), IN `pcUnidad` VARCHAR(150), IN `pcCelular` INT, IN `pcEmp` INT, OUT `pcMensajeError` VARCHAR(1000))
SP: BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);          
    DECLARE vEmp INT;
    DECLARE vTraslape INT;
    DECLARE vSolicitudes INT;
    DECLARE vPrevios INT;
    DECLARE vAdmin INT;
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
        SET pcMensajeError := vcTempMensajeError;
    
    END;
    
    SET vEmp := (SELECT COUNT(empleado.No_Empleado) FROM empleado WHERE empleado.No_Empleado = pcEmp);
    
    SET vTraslape := (SELECT COUNT(rsv_reservaciones.id_reservacion) FROM rsv_reservaciones WHERE n_empleado = vEmp AND salon_id = pcCodigo AND (estado_reservacion = 5) AND ((rsv_reservaciones.fecha_inicio BETWEEN pcFechaI AND pcFechaF) OR (rsv_reservaciones.fecha_fin BETWEEN pcFechaI AND pcFechaF)) AND ((rsv_reservaciones.horaI BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00')) OR (rsv_reservaciones.horaF BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00'))))));
                           
    SET vSolicitudes := (SELECT COUNT(id_reservacion) FROM rsv_reservaciones WHERE n_empleado = vEmp AND estado_reservacion = 5 AND ((rsv_reservaciones.fecha_inicio BETWEEN pcFechaI AND pcFechaF) OR (rsv_reservaciones.fecha_fin BETWEEN pcFechaI AND pcFechaF)) AND ((rsv_reservaciones.horaI BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00')) OR (rsv_reservaciones.horaF BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00'))))));
    
    SET vPrevios := (SELECT DATEDIFF(pcFechaI,CURRENT_DATE())); 
    
    SET vAdmin := (SELECT COUNT(rsv_administrador.n_empleado) FROM rsv_administrador WHERE rsv_administrador.n_empleado = pcEmp AND rsv_administrador.id_salon = pcCodigo);
    
    IF (vEmp = 1) THEN
        IF vPrevios >= 3 THEN
            IF vSolicitudes = 0 THEN
                IF vTraslape = 1 THEN
                    BEGIN
                        SET pcMensajeError := CONCAT('El salón solicitado ya ha sido reservado. Cambie la fecha y hora e intentelo de nuevo.');
                        LEAVE SP;
                    END;
                END IF;
            ELSE
                BEGIN
                    SET pcMensajeError := ('Usted tiene reservado otro espacio en la fecha y horario especificados. La solicitud no puede realizarse');
                    LEAVE SP;
                END;
            END IF;
        ELSE
            BEGIN
                SET pcMensajeError := ('Las solicitudes de uso de salones deben realizarse con un mínimo de 3 días de anticipación. Cambie la fecha de inicio e intentelo de nuevo');
                LEAVE SP;
            END;
        END IF;
    ELSE
        BEGIN
            SET pcMensajeError := ('El número de empleado ingresado no corresponde con ningún empleado registrado. Ingrese un número de empleado válido');
            LEAVE SP;
        END;
    END IF;
    
    START TRANSACTION;
    
    IF (vAdmin = 1) THEN
        INSERT INTO rsv_reservaciones (n_empleado, salon_id, fecha_inicio, fecha_fin, horaI, horaF, proposito, clase_id, evento, clase_ext, responsable, numcelular, unidad, estado_reservacion) VALUES (pcEmp, pcCodigo, pcFechaI, pcFechaF, pcHoraIn, pcHoraFin, pcProposito, pcClase, pcEvento, pcExterna, pcResp, pcCelular, pcUnidad, '5');
    ELSE
        INSERT INTO rsv_reservaciones (n_empleado, salon_id, fecha_inicio, fecha_fin, horaI, horaF, proposito, clase_id, evento, clase_ext, responsable, numcelular, unidad, estado_reservacion) VALUES (pcEmp, pcCodigo, pcFechaI, pcFechaF, pcHoraIn, pcHoraFin, pcProposito, pcClase, pcEvento, pcExterna, pcResp, pcCelular, pcUnidad, '1');
    END IF;

    COMMIT;
    
END
-- realizado 09/10/2017
********************************************************************************
INSERT INTO `estado_permiso` (`id_estado`, `descripcion`) VALUES (NULL, 'Cancelado');

********************************************************************************
-- PROCEDIMIENTO ALMACENADO PARA OBTENER LOS DATOS DE LAS SOLICITUDES DEL EMPLEADO
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_DATOS_MSOLICITUDES`(IN `pcUsuario` INT)
BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);
    DECLARE vEmp INT;

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
    
    END;
    
    SET vEmp := (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = pcUsuario);
    
    SET vcTempMensajeError := 'Error al obtener la información de las solicitudes';
    
    SELECT rsv_reservaciones.id_reservacion, rsv_salones.nombre_salon, rsv_salones.ubicacion, rsv_reservaciones.fecha_inicio, rsv_reservaciones.fecha_fin, rsv_reservaciones.horaI, rsv_reservaciones.horaF, (CASE rsv_reservaciones.proposito WHEN '0' THEN 'Clase' WHEN '1' THEN 'Evento' END) as 'proposito', rsv_reservaciones.observacion, (CASE rsv_reservaciones.estado_reservacion WHEN '1' THEN 'Espera' WHEN '4' THEN 'Denegado' WHEN '5' THEN 'Aprobado' WHEN '6' THEN 'Cancelado' END) as 'estadoR' FROM rsv_reservaciones INNER JOIN rsv_salones ON rsv_reservaciones.salon_id=rsv_salones.id_salon WHERE rsv_reservaciones.n_empleado = vEmp;

END
-- realizado 09/10/2017
*********************************************************************************
--PROCEDIMIENTO ALMACENADO PARA EDITAR LOS DATOS DE UNA SOLICITUD
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_EDITAR_SOLICITUD`(IN `pcCodigoR` INT, IN `pcSalon` INT, IN `pcFechaI` DATE, IN `pcFechaF` DATE, IN `pcHoraIn` TIME, IN `pcHoraFin` TIME, IN `pcProposito` INT, IN `pcClase` VARCHAR(7), IN `pcEvento` VARCHAR(200), IN `pcCelular` INT, IN `pcUsuario` INT, OUT `pcMensajeError` VARCHAR(1000))
SP: BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);          
    DECLARE vEmp INT;
    DECLARE vTraslape INT;
    DECLARE vSolicitudes INT;
    DECLARE vPrevios INT;
    DECLARE vAdmin INT;
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
        SET pcMensajeError := vcTempMensajeError;
    
    END;
    
    SET vEmp := (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = pcUsuario);
    
    SET vTraslape := (SELECT COUNT(id_reservacion) FROM rsv_reservaciones WHERE id_reservacion != pcCodigoR AND salon_id = pcSalon AND (estado_reservacion = 5) AND ((rsv_reservaciones.fecha_inicio BETWEEN pcFechaI AND pcFechaF) OR (rsv_reservaciones.fecha_fin BETWEEN pcFechaI AND pcFechaF)) AND ((rsv_reservaciones.horaI BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00')) OR (rsv_reservaciones.horaF BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00'))))));
                           
    SET vSolicitudes := (SELECT COUNT(id_reservacion) FROM rsv_reservaciones WHERE n_empleado = vEmp AND id_reservacion != pcCodigoR AND estado_reservacion = 5 AND ((rsv_reservaciones.fecha_inicio BETWEEN pcFechaI AND pcFechaF) OR (rsv_reservaciones.fecha_fin BETWEEN pcFechaI AND pcFechaF)) AND ((rsv_reservaciones.horaI BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00')) OR (rsv_reservaciones.horaF BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00'))))));
    
    SET vPrevios := (SELECT DATEDIFF(pcFechaI,CURRENT_DATE()));
    
    SET vAdmin := (SELECT COUNT(rsv_administrador.n_empleado) FROM rsv_administrador WHERE rsv_administrador.n_empleado = vEmp AND rsv_administrador.id_salon = pcSalon);
        
    SET vcTempMensajeError := ('Error al ingresar la solicitud');
    IF vPrevios >= 3 THEN
        IF vSolicitudes = 0 THEN
            IF vTraslape = 1 THEN
                BEGIN
                    SET pcMensajeError := CONCAT('El salón solicitado ya ha sido reservado. Cambie la fecha y hora e intentelo de nuevo.');
                    LEAVE SP;
                END;
            END IF;
        ELSE
            BEGIN
                SET pcMensajeError := ('Usted tiene reservado otro espacio en la fecha y horario especificados. La solicitud no puede realizarse');
                LEAVE SP;
            END;
        END IF;
    ELSE
        BEGIN
            SET pcMensajeError := ('La nueva fecha ingresada debe ser mínimo 3 días posterior a la fecha de hoy. Cambie la fecha de inicio e intentelo de nuevo');
            LEAVE SP;
        END;
    END IF;
    
    START TRANSACTION;
    IF (vAdmin = 1) THEN
        UPDATE rsv_reservaciones SET n_empleado=vEmp, salon_id=pcSalon, fecha_inicio=pcFechaI, fecha_fin=pcFechaF, horaI=pcHoraIn, horaF=pcHoraFin, proposito=pcProposito, clase_id=pcClase, evento=pcEvento, numcelular=pcCelular, estado_reservacion='5' WHERE id_reservacion = pcCodigoR;
    ELSE
        UPDATE rsv_reservaciones SET n_empleado=vEmp, salon_id=pcSalon, fecha_inicio=pcFechaI, fecha_fin=pcFechaF, horaI=pcHoraIn, horaF=pcHoraFin, proposito=pcProposito, clase_id=pcClase, evento=pcEvento, numcelular=pcCelular, estado_reservacion='1' WHERE id_reservacion = pcCodigoR;
    END IF;

    COMMIT;
    
END
-- realizado 09/10/2017
********************************************************************
-- PROCEDIMIENTO ALMACENADO PARA CANCELAR LAS SOLICITUDES DE RESERVACION DE USO DE SALON
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_CANCELAR_SOLICITUD`(IN `pcCodigoR` INT, OUT `pcMensajeError` VARCHAR(1000))
SP: BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);          
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
        SET pcMensajeError := vcTempMensajeError;
    
    END;
      
    SET vcTempMensajeError := ('Error al cancelar la solicitud');
    
    START TRANSACTION;
    
        UPDATE rsv_reservaciones SET estado_reservacion='6', observacion = 'Usted canceló esta solicitud.' WHERE id_reservacion = pcCodigoR;

    COMMIT;
    
END
-- realizado 09/10/2017
*****************************************************************************************
-- PROCEDIMIENTO ALMACENADO PARA OBTENER LOS DATOS DE LAS SOLICITUDES QUE SE REVISARAN
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_DATOS_RSOLICITUDES`(IN `pcUsuario` INT)
BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);
    DECLARE vEmp INT;
    DECLARE vSalon INT;

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
    
    END;
    
    SET vEmp := (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = pcUsuario);
    
    SET vSalon := (SELECT rsv_administrador.id_salon FROM rsv_administrador WHERE rsv_administrador.n_empleado = vEmp);
    
    SET vcTempMensajeError := 'Error al obtener la información de las solicitudes';
    
    SELECT rsv_reservaciones.id_reservacion, CONCAT(persona.Primer_nombre,' ', persona.Primer_apellido) as 'Empleado', rsv_salones.nombre_salon, rsv_reservaciones.fecha_inicio, rsv_reservaciones.fecha_fin, rsv_reservaciones.horaI, rsv_reservaciones.horaF, (CASE rsv_reservaciones.proposito WHEN '0' THEN 'Clase' WHEN '1' THEN 'Evento' END) as 'proposito', ca_asignaturas.nombre, rsv_reservaciones.evento FROM rsv_reservaciones LEFT JOIN empleado on rsv_reservaciones.n_empleado=empleado.No_Empleado INNER JOIN persona on empleado.N_identidad=persona.N_identidad INNER JOIN rsv_salones ON rsv_reservaciones.salon_id=rsv_salones.id_salon LEFT JOIN ca_asignaturas on rsv_reservaciones.clase_id=ca_asignaturas.id_asignatura WHERE rsv_reservaciones.estado_reservacion = '1' AND rsv_reservaciones.salon_id = vSalon;

END
-- realizado 09/10/2017
***************************************************************************************
-- PROCEDIMIENTO ALMACENADO PARA DENEGAR LAS SOLICITUDES 
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_DENEGAR_SOLICITUD`(IN `pcCodigo` INT, IN `pcObservacion` VARCHAR(200), OUT `pcMensajeError` VARCHAR(1000))
SP: BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);          

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
        SET pcMensajeError := vcTempMensajeError;
    
    END;
    
    START TRANSACTION;
    
        UPDATE rsv_reservaciones SET rsv_reservaciones.estado_reservacion = '4', rsv_reservaciones.observacion = pcObservacion WHERE rsv_reservaciones.id_reservacion = pcCodigo;

    COMMIT;
    
END
-- realizado 09/10/2017
***************************************************************************************
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_APROBAR_SOLICITUD`(IN `pcCodigo` INT, IN `pcObservacion` VARCHAR(200), IN `pcHoraI` TIME, IN `pcHoraF` TIME, IN `pcFechaI` DATE, IN `pcFechaF` DATE, OUT `pcMensajeError` VARCHAR(1000))
SP: BEGIN
    
    DECLARE temp_reservacion INT;
    DECLARE fin INTEGER DEFAULT 0;
    
    DECLARE ciclo_reservaciones CURSOR FOR
        SELECT rsv_reservaciones.id_reservacion FROM rsv_reservaciones WHERE ((rsv_reservaciones.fecha_inicio BETWEEN pcFechaI AND pcFechaF) OR (rsv_reservaciones.fecha_fin BETWEEN pcFechaI AND pcFechaF)) AND (rsv_reservaciones.horaI = pcHoraI OR (rsv_reservaciones.horaI BETWEEN (ADDTIME(pcHoraI,'00:01:00')) AND (SUBTIME(pcHoraF,'00:01:00')) OR (rsv_reservaciones.horaF BETWEEN (ADDTIME(pcHoraI,'00:01:00')) AND (SUBTIME(pcHoraF,'00:01:00'))) OR rsv_reservaciones.horaF = pcHoraF));
        
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET fin=1;
    
    OPEN ciclo_reservaciones;
            get_reservaciones: LOOP
                FETCH ciclo_reservaciones INTO temp_reservacion;
                IF fin = 1 THEN
                    LEAVE get_reservaciones;
                END IF;
                
                UPDATE rsv_reservaciones SET rsv_reservaciones.estado_reservacion = '4', rsv_reservaciones.observacion = 'Se dio prioridad a otra solicitud' WHERE rsv_reservaciones.id_reservacion = temp_reservacion;
                
            END LOOP get_reservaciones;
        CLOSE ciclo_reservaciones;
    
    START TRANSACTION;
    
        UPDATE rsv_reservaciones SET rsv_reservaciones.estado_reservacion = '5', rsv_reservaciones.observacion = pcObservacion WHERE rsv_reservaciones.id_reservacion = pcCodigo;

    COMMIT;
    
END
-- realizado 09/10/2017
*****************************************************************************************
--PROCEDIMIENTO PARA OBTENER LOS DATOS DE LOS EMPLEADOS EN EL FORMULARIO DE ASIGNACION DE ADMINISTRADORES
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_DATOS_EMPLEADOS`(IN `pcUsuario` INT, IN `pcOperacion` INT)
BEGIN

    DECLARE vdependencia INT;
    
    SET vdependencia := (SELECT empleado.id_dependencia FROM empleado WHERE empleado.No_Empleado IN (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = pcUsuario));
    
    IF (pcOperacion = 1) THEN
        SELECT No_Empleado, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) AS 'nombre' FROM persona INNER JOIN empleado ON persona.N_identidad=empleado.N_identidad WHERE empleado.id_categoria IN (SELECT categoria.categoria_id FROM categoria WHERE categoria.descripcion_cat = 'Administrativo') AND empleado.id_dependencia = vdependencia AND empleado.No_Empleado NOT IN (SELECT rsv_administrador.n_empleado FROM rsv_administrador);
    ELSE
        SELECT No_Empleado, CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) AS 'nombre' FROM persona INNER JOIN empleado ON persona.N_identidad=empleado.N_identidad WHERE empleado.id_categoria IN (SELECT categoria.categoria_id FROM categoria WHERE categoria.descripcion_cat = 'Administrativo');
    END IF;

END

-- realizado 09/10/2017
*****************************************************************************************
-- PROCEDIMIENTO PARA OBTENER UN LISTADO DE EMPLEADOS QUE HAN SIDO ASIGNADOS COMO ADMINISTRADORES DE UN SALON.
DROP PROCEDURE SP_RSV_DATOS_ADMINISTRADORES //
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_DATOS_ADMINISTRADORES`(IN `pcUsuario` INT)
BEGIN

    DECLARE vsalon INT;
    
    SET vsalon := (SELECT rsv_administrador.id_salon FROM rsv_administrador WHERE rsv_administrador.n_empleado IN (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = pcUsuario));
    
    SELECT No_Empleado, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) AS 'nombre' FROM persona INNER JOIN empleado ON persona.N_identidad=empleado.N_identidad WHERE empleado.No_Empleado IN (SELECT rsv_administrador.n_empleado FROM rsv_administrador WHERE rsv_administrador.id_salon = vsalon);

END
-- realizado 09/10/2017
*****************************************************************************************
-- PROCEDIMIENTO PARA DAR PRIVILEGIOS DE ADMINISTRADOR A UN EMPLEADO
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_DAR_PRIVILEGIOS`(IN `pcEmpleado` INT, IN `pcUsuario` INT, OUT `pcMensajeError` VARCHAR(1000))
SP: BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);   
    DECLARE vSalon INT;
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
        SET pcMensajeError := vcTempMensajeError;
    
    END;
      
    SET vcTempMensajeError := ('Error al otorgar privilegios de administrador a este empleado');
    
    SET vSalon := (SELECT rsv_administrador.id_salon FROM rsv_administrador WHERE rsv_administrador.n_empleado IN (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = pcUsuario));
    
    START TRANSACTION;
    
        INSERT INTO rsv_administrador(n_empleado, id_salon) VALUES (pcEmpleado, vSalon);

    COMMIT;
    
END
-- realizado 09/10/2017
*****************************************************************************************
-- PROCEDIMIENTO PARA QUITAR PRIVILEGIOS DE ADMINISTRADOR A UN EMPLEADO
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_REMOVER_PRIVILEGIOS`(IN `pcEmpleado` INT, OUT `pcMensajeError` VARCHAR(1000))
SP: BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);   
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
        SET pcMensajeError := vcTempMensajeError;
    
    END;
      
    SET vcTempMensajeError := ('Error al otorgar privilegios de administrador a este empleado');
    
    
    START TRANSACTION;
    
        DELETE FROM rsv_administrador WHERE rsv_administrador.n_empleado = pcEmpleado;

    COMMIT;
    
END
-- realizado 09/10/2017
*****************************************************************************************
-- PROCEDIMIENTO PARA REGISTRAR NUEVOS SALONES
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_REGISTRAR_SALON`(IN `pcNombre` VARCHAR(100), IN `pcUbicacion` VARCHAR(100), IN `pcCapacidad` INT, IN `pcEmpleado` INT, OUT `pcMensajeError` VARCHAR(500))
SP: BEGIN

    DECLARE vTempMensajeError VARCHAR(500) DEFAULT ''; 
    DECLARE vContador INT DEFAULT 0; 
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
    
        SET vTempMensajeError := CONCAT('Error: ', vTempMensajeError);
        SET pcMensajeError := vTempMensajeError;
    
    END;    
    
    SELECT
        COUNT(rsv_salones.nombre_salon)
    INTO
        vContador
    FROM
        rsv_salones
    WHERE
        nombre_salon = pcNombre;
        
    IF vContador > 0 then
        SET pcMensajeError := 'Ya hay un salón registrado con ese nombre, intente otro';
        LEAVE SP;
    END IF;
    
    START TRANSACTION;
        INSERT INTO rsv_salones (nombre_salon, capacidad, ubicacion) VALUES (pcNombre, pcCapacidad, pcUbicacion);
        INSERT INTO rsv_administrador (id_salon, n_empleado) VALUES ((SELECT MAX(rsv_salones.id_salon) FROM rsv_salones ORDER BY rsv_salones.id_salon DESC LIMIT 1), pcEmpleado);
        
    COMMIT;

END
-- realizado 09/10/2017
*****************************************************************************************
-- PROCEDIMIENTO ALMACENADO PARA EDITAR LA INFORMACIÓN DE LOS SALONES REGISTRADOS
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_EDITAR_SALON`(IN `pcCodigo` INT, IN `pcNombre` VARCHAR(100), IN `pcUbicacion` VARCHAR(100), IN `pcCapacidad` INT, OUT `pcMensajeError` VARCHAR(500))
SP: BEGIN

    DECLARE vTempMensajeError VARCHAR(500) DEFAULT ''; 
    DECLARE vContador INT DEFAULT 0; 
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
    
        SET vTempMensajeError := CONCAT('Error: ', vTempMensajeError);
        SET pcMensajeError := vTempMensajeError;
    
    END;    
    
    SELECT COUNT(rsv_salones.nombre_salon) INTO vContador FROM rsv_salones WHERE nombre_salon = pcNombre AND rsv_salones.id_salon != pcCodigo;
    
    IF vContador > 0 then
        SET pcMensajeError := 'Ya hay un salón registrado con ese nombre, intente otro';
        LEAVE SP;
    END IF;
    
    START TRANSACTION;
        UPDATE rsv_salones SET nombre_salon=pcNombre, capacidad=pcCapacidad, ubicacion=pcUbicacion WHERE rsv_salones.id_salon = pcCodigo;
        
    COMMIT;

END
-- realizado 09/10/2017
*****************************************************************************************
-- PROCEDIMIENTO PARA HABILITAR O DESHABILITAR EL USO DE LOS SALONES

CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_ESTADO_SALON`(IN `pcCodigo` INT, OUT `pcMensajeError` VARCHAR(500))
SP: BEGIN

    DECLARE vEstado INT DEFAULT 0;
    
    SET vEstado := (SELECT rsv_salones.habilitado FROM rsv_salones WHERE rsv_salones.id_salon = pcCodigo);
    
    IF vEstado = 1 THEN
        UPDATE rsv_salones SET rsv_salones.habilitado = '0' WHERE rsv_salones.id_salon = pcCodigo;
        UPDATE rsv_reservaciones SET rsv_reservaciones.estado_reservacion = '4', rsv_reservaciones.observacion = 'Este salón ha sido deshabilitado. Se ha denegado su solicitud' WHERE rsv_reservaciones.salon_id = pcCodigo AND rsv_reservaciones.estado_reservacion != 5;
    ELSE
        UPDATE rsv_salones SET rsv_salones.habilitado = '1' WHERE rsv_salones.id_salon = pcCodigo;
    END IF;

END
-- realizado 09/10/2017
*****************************************************************************************
-- PROCEDIMIENTO PARA IMPRIMIR EL FORMATO DE RESPONSABILIDAD DE USO DE LA SALA
CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_RSV_IMPRIMIR`(IN `pcCodigo` INT, IN `pcCorrelativo` VARCHAR(10), OUT `mensaje` VARCHAR(500))
SP: BEGIN 

DECLARE vnTempMensaje VARCHAR(500);
DECLARE contador INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; 

SET vnTempMensaje := CONCAT('ERROR:', mensaje);
SET mensaje := vnTempMensaje;
END; 

SET contador := (SELECT COUNT(rsv_reservaciones.id_reservacion) from rsv_reservaciones where rsv_reservaciones.estado_reservacion = 5 AND rsv_reservaciones.id_reservacion = pcCodigo);

IF (contador = 0) THEN 
    BEGIN
        SET mensaje := CONCAT('No se pudo exportar la solicitud');
        LEAVE SP;
    END;
END IF;

START TRANSACTION; 
    UPDATE rsv_reservaciones SET  rsv_reservaciones.correlativo = pcCorrelativo where rsv_reservaciones.id_reservacion = pcCodigo; 
COMMIT; 

END
-- realizado 09/10/2017
**************************************************************************
ALTER TABLE `rsv_reservaciones` ADD `clase_ext` VARCHAR(150) NULL AFTER `evento`;
ALTER TABLE `rsv_reservaciones` ADD `responsable` VARCHAR(100) NOT NULL AFTER `evento`;
ALTER TABLE `rsv_reservaciones` ADD `numEmpleado` INT NOT NULL AFTER `responsable`;
ALTER TABLE `rsv_reservaciones` ADD `unidad` VARCHAR(150) NOT NULL AFTER `numcelular`;
ALTER TABLE `rsv_reservaciones` ADD `id_usuario` INT NOT NULL AFTER `correlativo` //
ALTER TABLE rsv_reservaciones ADD FOREIGN KEY(id_usuario) REFERENCES usuario.id_Usuario ON DELETE CASCADE ON UPDATE CASCADE;
-- realizado 11/10/2017
**************************************************************************
DROP PROCEDURE SP_RSV_SOLICITAR_SALON //
CREATE DEFINER=`ddvderecho`@`localhost` PROCEDURE `SP_RSV_SOLICITAR_SALON`(IN `pcCodigo` INT, IN `pcFechaI` DATE, IN `pcFechaF` DATE, IN `pcHoraIn` TIME, IN `pcHoraFin` TIME, IN `pcProposito` INT, IN `pcClase` VARCHAR(7), IN `pcEvento` VARCHAR(200), IN `pcExterna` VARCHAR(200), IN `pcResp` VARCHAR(100), IN `pcUnidad` VARCHAR(150), IN `pcCelular` INT, IN `pcEmp` INT, IN `pcUser` INT, OUT `pcMensajeError` VARCHAR(1000))
SP: BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);          
    DECLARE vEmp INT;
    DECLARE vTraslape INT;
    DECLARE vSolicitudes INT;
    DECLARE vPrevios INT;
    DECLARE vAdmin INT;
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
        SET pcMensajeError := vcTempMensajeError;
    
    END;
    
    SET vEmp := (SELECT COUNT(empleado.No_Empleado) FROM empleado WHERE empleado.No_Empleado = pcEmp);
    
    SET vTraslape := (SELECT COUNT(rsv_reservaciones.id_reservacion) FROM rsv_reservaciones WHERE n_empleado = vEmp AND salon_id = pcCodigo AND (estado_reservacion = 5) AND ((rsv_reservaciones.fecha_inicio BETWEEN pcFechaI AND pcFechaF) OR (rsv_reservaciones.fecha_fin BETWEEN pcFechaI AND pcFechaF)) AND ((rsv_reservaciones.horaI BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00')) OR (rsv_reservaciones.horaF BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00'))))));
                           
    SET vSolicitudes := (SELECT COUNT(id_reservacion) FROM rsv_reservaciones WHERE n_empleado = vEmp AND estado_reservacion = 5 AND ((rsv_reservaciones.fecha_inicio BETWEEN pcFechaI AND pcFechaF) OR (rsv_reservaciones.fecha_fin BETWEEN pcFechaI AND pcFechaF)) AND ((rsv_reservaciones.horaI BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00')) OR (rsv_reservaciones.horaF BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00'))))));
    
    SET vPrevios := (SELECT DATEDIFF(pcFechaI,CURRENT_DATE())); 
    
    SET vAdmin := (SELECT COUNT(rsv_administrador.n_empleado) FROM rsv_administrador WHERE rsv_administrador.n_empleado = pcEmp AND rsv_administrador.id_salon = pcCodigo);
    
    IF (vEmp = 1) THEN
        IF vPrevios > 2 THEN
            IF vSolicitudes = 0 THEN
                IF vTraslape = 1 THEN
                    BEGIN
                        SET pcMensajeError := CONCAT('El salón solicitado ya ha sido reservado. Cambie la fecha y hora e intentelo de nuevo.');
                        LEAVE SP;
                    END;
                END IF;
            ELSE
                BEGIN
                    SET pcMensajeError := ('Usted tiene reservado otro espacio en la fecha y horario especificados. La solicitud no puede realizarse');
                    LEAVE SP;
                END;
            END IF;
        ELSE
            BEGIN
                SET pcMensajeError := ('Las solicitudes de uso de salones deben realizarse con un mínimo de 3 días de anticipación. Cambie la fecha de inicio e intentelo de nuevo');
                LEAVE SP;
            END;
        END IF;
    ELSE
        BEGIN
            SET pcMensajeError := ('El número de empleado ingresado no corresponde con ningún empleado registrado. Ingrese un número de empleado válido');
            LEAVE SP;
        END;
    END IF;
    
    START TRANSACTION;
    
    IF (vAdmin = 1) THEN
        INSERT INTO rsv_reservaciones (n_empleado, salon_id, fecha_inicio, fecha_fin, horaI, horaF, proposito, clase_id, evento, clase_ext, responsable, numcelular, unidad, estado_reservacion, id_usuario) VALUES (pcEmp, pcCodigo, pcFechaI, pcFechaF, pcHoraIn, pcHoraFin, pcProposito, pcClase, pcEvento, pcExterna, pcResp, pcCelular, pcUnidad, '5', pcUser);
    ELSE
        INSERT INTO rsv_reservaciones (n_empleado, salon_id, fecha_inicio, fecha_fin, horaI, horaF, proposito, clase_id, evento, clase_ext, responsable, numcelular, unidad, estado_reservacion, id_usuario) VALUES (pcEmp, pcCodigo, pcFechaI, pcFechaF, pcHoraIn, pcHoraFin, pcProposito, pcClase, pcEvento, pcExterna, pcResp, pcCelular, pcUnidad, '1', pcUser);
    END IF;

    COMMIT;
    
END

**************************************************************************
DROP PROCEDURE SP_RSV_EDITAR_SOLICITUD //
CREATE DEFINER=`ddvderecho`@`localhost` PROCEDURE `SP_RSV_EDITAR_SOLICITUD`(IN `pcCodigoR` INT, IN `pcSalon` INT, IN `pcFechaI` DATE, IN `pcFechaF` DATE, IN `pcHoraIn` TIME, IN `pcHoraFin` TIME, IN `pcProposito` INT, IN `pcClase` VARCHAR(7), IN `pcEvento` VARCHAR(200), IN `pcExterna` VARCHAR(200), IN `pcResp` VARCHAR(100), IN `pcUnidad` VARCHAR(150), IN `pcCelular` INT, IN `pcEmp` INT, OUT `pcMensajeError` VARCHAR(1000))
SP: BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);          
    DECLARE vEmp INT;
    DECLARE vTraslape INT;
    DECLARE vSolicitudes INT;
    DECLARE vPrevios INT;
    DECLARE vAdmin INT;
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
        SET pcMensajeError := vcTempMensajeError;
    
    END;
    
    SET vEmp := (SELECT COUNT(empleado.No_Empleado) FROM empleado WHERE empleado.No_Empleado = pcEmp);
    
    SET vTraslape := (SELECT COUNT(id_reservacion) FROM rsv_reservaciones WHERE id_reservacion != pcCodigoR AND salon_id = pcSalon AND (estado_reservacion = 5) AND ((rsv_reservaciones.fecha_inicio BETWEEN pcFechaI AND pcFechaF) OR (rsv_reservaciones.fecha_fin BETWEEN pcFechaI AND pcFechaF)) AND ((rsv_reservaciones.horaI BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00')) OR (rsv_reservaciones.horaF BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00'))))));
                           
    SET vSolicitudes := (SELECT COUNT(id_reservacion) FROM rsv_reservaciones WHERE n_empleado = pcEmp AND id_reservacion != pcCodigoR AND estado_reservacion = 5 AND ((rsv_reservaciones.fecha_inicio BETWEEN pcFechaI AND pcFechaF) OR (rsv_reservaciones.fecha_fin BETWEEN pcFechaI AND pcFechaF)) AND ((rsv_reservaciones.horaI BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00')) OR (rsv_reservaciones.horaF BETWEEN (ADDTIME(pcHoraIn,'00:01:00')) AND (SUBTIME(pcHoraFin,'00:01:00'))))));
    
    SET vPrevios := (SELECT DATEDIFF(pcFechaI,CURRENT_DATE()));
    
    SET vAdmin := (SELECT COUNT(rsv_administrador.n_empleado) FROM rsv_administrador WHERE rsv_administrador.n_empleado = pcEmp AND rsv_administrador.id_salon = pcSalon);
        
    IF (vEmp = 1) THEN
        IF vPrevios > 2 THEN
            IF vSolicitudes = 0 THEN
                IF vTraslape = 1 THEN
                    BEGIN
                        SET pcMensajeError := CONCAT('El salón solicitado ya ha sido reservado. Cambie la fecha y hora e intentelo de nuevo.');
                        LEAVE SP;
                    END;
                END IF;
            ELSE
                BEGIN
                    SET pcMensajeError := ('Usted tiene reservado otro espacio en la fecha y horario especificados. La solicitud no puede realizarse');
                    LEAVE SP;
                END;
            END IF;
        ELSE
            BEGIN
                SET pcMensajeError := ('Las solicitudes de uso de salones deben realizarse con un mínimo de 3 días de anticipación. Cambie la fecha de inicio e intentelo de nuevo');
                LEAVE SP;
            END;
        END IF;
    ELSE
        BEGIN
            SET pcMensajeError := ('El número de empleado ingresado no corresponde con ningún empleado registrado. Ingrese un número de empleado válido');
            LEAVE SP;
        END;
    END IF;
    
    START TRANSACTION;
    IF (vAdmin = 1) THEN
        UPDATE rsv_reservaciones SET n_empleado=pcEmp, salon_id=pcSalon, fecha_inicio=pcFechaI, fecha_fin=pcFechaF, horaI=pcHoraIn, horaF=pcHoraFin, proposito=pcProposito, clase_id=pcClase, evento=pcEvento, clase_ext=pcExterna, responsable=pcResp, unidad=pcUnidad, numcelular=pcCelular, estado_reservacion='5' WHERE id_reservacion = pcCodigoR;
    ELSE
        UPDATE rsv_reservaciones SET n_empleado=pcEmp, salon_id=pcSalon, fecha_inicio=pcFechaI, fecha_fin=pcFechaF, horaI=pcHoraIn, horaF=pcHoraFin, proposito=pcProposito, clase_id=pcClase, evento=pcEvento, clase_ext=pcExterna, responsable=pcResp, unidad=pcUnidad, numcelular=pcCelular, estado_reservacion='1' WHERE id_reservacion = pcCodigoR;
    END IF;

    COMMIT;
    
END

************************************************************************************
DROP PROCEDURE SP_RSV_DATOS_MSOLICITUDES //

CREATE DEFINER=`ddvderecho`@`localhost` PROCEDURE `SP_RSV_DATOS_MSOLICITUDES`(IN `pcUsuario` INT)
BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
    
    END;
    
    SET vcTempMensajeError := 'Error al obtener la información de las solicitudes';
    
    SELECT rsv_reservaciones.id_reservacion, rsv_salones.nombre_salon, rsv_salones.ubicacion, rsv_reservaciones.fecha_inicio, rsv_reservaciones.fecha_fin, rsv_reservaciones.horaI, rsv_reservaciones.horaF, (CASE rsv_reservaciones.proposito WHEN '0' THEN 'Clase' WHEN '1' THEN 'Evento' END) as 'proposito', rsv_reservaciones.observacion, (CASE rsv_reservaciones.estado_reservacion WHEN '1' THEN 'Espera' WHEN '4' THEN 'Denegado' WHEN '5' THEN 'Aprobado' WHEN '6' THEN 'Cancelado' END) as 'estadoR' FROM rsv_reservaciones INNER JOIN rsv_salones ON rsv_reservaciones.salon_id=rsv_salones.id_salon WHERE rsv_reservaciones.id_usuario = pcUsuario;

END