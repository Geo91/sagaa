
<div class="col-md-9 col-sm-9">
    <div class="panel panel-info">
        <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Solicitud de Vacaciones</div>

        <div class="panel-body" >
            <div id= "noti1" class="col-lg-12 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden<br>La fecha de finalización de las vacaciones se calculará automáticamente en base al total de días solicitados<br>En caso de usar mozilla firefox el formato de fecha debe ser 'año-mes-día' </center></div>
            <div class="row">
                <div class="col-lg-12">
                <!--************************formulario*********************-->
                    <form class="form-horizontal" role="form" id="formSolicitudVacaciones" name="formSolicitudVacaciones">

                        <div class="row form-group"> 
                            <label class=" col-sm-2 control-label"> Año Anterior </label>  
                            <div class="col-sm-2">
                                <select id="Anterior" name="Anterior" size="2" required>
                                    <option value="0"> NO </option>
                                    <option value="1"> SI </option>
                                </select>
                            </div>               

                            <label id = "lbAnterior" class="col-sm-3 control-label">Días Año Anterior</label>        
                            <div class="col-xs-3">                   
                                <input type="number" id="diasAnterior" name="diasAñoAnterior" min="1" max="30" value="1" required>
                            </div>              
                        </div>

                        <div class="row form-group">                    
                            <label class=" col-sm-2 control-label"> Año Actual </label>                       
                            <div class="col-sm-2">
                                <select id="Actual" name="Actual" size="2" required>
                                    <option value="0"> NO </option>
                                    <option value="1"> SI </option>
                                </select>            
                            </div>                  

                            <label id = "lbActual" class="col-sm-3 control-label">Días Año Actual</label>     
                            <div class="col-xs-1">                   
                                <input type="number" id="diasActual" name="diasAñoActual" min="1" max="30" value="1" required>
                            </div>                  
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 control-label">Fecha Inicio</label>
                            <div class="col-sm-2">
                                <input type="date" id="fechaInicio" name="datepicker" required><span class="">
                            </div>        
                        </div>

                        <div class="row">
                            <label class="control-label col-sm-2"></label>
                            <div class="col-sm-8">
                                <p aling ="right">
                                    <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" > Enviar <span class="glyphicon glyphicon-circle-arrow-right"></span></button> 
                                </p>          
                            </div>                            
                        </div>    

                    </form>
                <!--************************formulario*********************-->
                </div>
            </div>
        </div>
    </div>
</div>