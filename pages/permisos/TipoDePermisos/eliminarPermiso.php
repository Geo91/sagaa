<?php  
$maindir = "../../../";
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if(isset($_POST['Codigo'])){

        $id = $_POST['Codigo'];
        
        try{
            $sql = "CALL SP_ELIMINAR_PERMISO(?,@mensajeError)";
            $query1 = $db->prepare($sql);
            $query1->bindParam(1,$id,PDO::PARAM_STR);
            $query1->execute();

            $output = $db->query("select @mensajeError")->fetch(PDO::FETCH_ASSOC);
            $mensaje = $output['@mensajeError'];

            if(is_null($mensaje)){
                echo mensajes("Se ha eliminando el registro seleccionado","verde");
            }
            else{
                echo mensajes("Error! ". $mensaje, "rojo");
            }
            
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }else{
        echo mensajes("Ha ocurrido un error del servidor...","rojo");
    }
    
?>

<script type="text/javascript">
    $("#TiposTabla").load('pages/permisos/TipoDePermisos/DatosTipos.php');
</script>