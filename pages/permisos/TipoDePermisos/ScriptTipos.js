//cargar la tabla desde un principio
    $("#TiposTabla").load('pages/permisos/TipoDePermisos/DatosTipos.php');

$(document).ready(function(){   

//para cuando se da submit al formulario formAsignatura
    $("#formTipoPermiso").submit(function(e) {
        e.preventDefault();
        data={
            Nombre:$('#NombreTipoPermiso').val(),
            CodigoM:$('#selectMotivo').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: agregarTipoPermiso,
            timeout: 6000,
            error: problemas
        }); 
        return false;
    });


//para cuando se da submit al formulario formAsignatura
    $("#formEditarTipoPermiso").submit(function(e) {
        e.preventDefault();
        data={
            Codigo:$('#modalCodigo').val(),
            Nombre:$('#modalNombre').val(),
            CodigoM:$('#modalMotivo').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: editarTipoPermiso,
            timeout: 6000,
            error: problemas
        }); 
        $("#modalEditar").modal('hide');
        return false;
    });

});

//transfiere la información del registro seleccionado en la tabla, a la modal para editarla.
$(document).on("click", ".editarTipoPermiso", function () {
    codigo = $(this).parents("tr").find("td").eq(0).html();
    Nombre = $(this).parents("tr").find("td").eq(1).html();
    codigoM = $(this).parents("tr").find("td").eq(2).attr('id').valueOf();

    $("#modalCodigo").val(codigo);
    $("#modalNombre").val(Nombre);
    $("#modalMotivo").val(codigoM);

    $("#modalEditar").modal('show');
});


function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function agregarTipoPermiso(){
    $("#divRespuesta").load('pages/permisos/TipoDePermisos/insertarPermiso.php',data);
    $('#formTipoPermiso').trigger("reset");
}

function editarTipoPermiso(){
    $("#divRespuestaEditar").load('pages/permisos/TipoDePermisos/editarTipoPermiso.php',data);
    $("#TiposTabla").load('pages/permisos/TipoDePermisos/DatosTipos.php');
}

function eliminarTipoPermiso(){
    $("#divRespuestaEditar").load('pages/permisos/TipoDePermisos/eliminarPermiso.php',data);
}