<?php  
    $maindir = "../../../";
    require_once($maindir."Datos/funciones.php");
    include($maindir."conexion/config.inc.php");

    if(isset($_POST['Nombre']) && isset($_POST['CodigoM'])){
        $pcnombre = $_POST['Nombre'];
        $codmotivo = $_POST['CodigoM'];
        try{
            $sql = "CALL SP_REGISTRAR_TIPO_PERMISO(?,?,@mensajeError)";
            $query1 = $db->prepare($sql);
            $query1->bindParam(1,$pcnombre,PDO::PARAM_STR);
            $query1->bindParam(2,$codmotivo,PDO::PARAM_STR);
            $query1->execute();

            $output = $db->query("select @mensajeError")->fetch(PDO::FETCH_ASSOC);
            $mensaje = $output['@mensajeError'];
            
            if(is_null($mensaje)){
                echo mensajes("El registro se ha guardado exitosamente","verde");
            }else{
                echo mensajes("El registro no pudo ingresarse", "rojo");
            }

        }catch(PDOExecption $e){
            echo mensaje("Error!  ".$e->getMessage(), "rojo");
        }
    }else{
        echo mensajes("Hubo un error al intentar guardar el registro. Intentelo de nuevo o contacte al administrador del sistema", "rojo");
    }    
    
?>

<script type="text/javascript">
    $("#TiposTabla").load('pages/permisos/TipoDePermisos/DatosTipos.php');
    $('#formEdificios').trigger("reset");
</script>