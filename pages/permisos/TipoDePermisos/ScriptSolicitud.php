//Valida que se haya elegido una fecha y que el campo no esté vacío
    function validaFecha(){
        fecha=$("#fecha").val();
        if(fecha.length>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    //Valida que ningún combobox sea null
    function validaCombobox(){
        if($("#motivo").val()==='NULL')
        {
            alert("Debes seleccionar un motivo");
            return false;
        }
        else
        {            
            if($("#edificio").val()==='NULL')
            {
                alert("Debes seleccionar el edificio donde se registra");
                return false;
            }
            else
            {
                if($("#tipoPermiso").val()==='NULL')
                {
                    alert("Debes seleccionar un tipo de permiso");
                    return false;
                }
                else
                {
                    if($("#jefe").val()==='NULL')
                    {
                        alert("Debes seleccionar a su jefe inmediato");
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
        }
    }

    $(document).on("change","#motivo",function () {
        id = $("#motivo").val();            
        obtenerTipos(id);
        return false;
    });

    $("#formSolicitud").submit(function (e) {
        e.preventDefault();
        if(validaCombobox()===true) //Se validan que se haya seleccionado una de las opciones de los combobox
        {
            if(validaFecha()===false) //Se valida que se haya ingresado una fecha
            {
                alert('Debes de ingresar una fecha');
                }
                else
                {
                    //Se recopila la información que se enviara a la base de datos
                    data2 = {empleado: $("#empleado").val(),
                    nombre: $("#nombre").val(),
                    motivo: $("#motivo").val(),
                    edificio: $("#edificio").val(),
                    tipoPerm: $("#tipoPermiso").val(),
                    jefe: $("#jefe").val(),
                    cantidad: $("#cantidad").val(),
                    fecha: $("#fecha").val(),
                    horaIn: $("#horaIn").val(),
                    horaFin: $("#horaFi").val() 
                    };

                    $.ajax({
                        async: true,
                        type: "POST",
                        dataType: "html",
                        contentType: "application/x-www-form-urlencoded",
                        url: "pages/permisos/solicitudpersonal/registrarSolicitudPermiso.php",
                        beforeSend: inicioEnvio,
                        success: llegadaGuardar,
                        timeout: 4000,
                    });
                } 
        }                                                           
        return false;
    });

    function llegadaGuardar()
    {
        $("#contenedor2").load('pages/permisos/solicitudpersonal/registrarSolicitudPermiso.php', data2);
        $("#formSolicitud").trigger("reset");
    }
