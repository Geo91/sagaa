<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query1 = "SELECT tipodepermiso.id_tipo_permiso, tipodepermiso.tipo_permiso, motivos.Motivo_ID, motivos.descripcion_motivo from tipodepermiso, motivos where tipodepermiso.id_motivo = motivos.Motivo_ID order by motivos.descripcion_motivo DESC;";
?>

<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaTipoPermisos" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style ="display:none">Código</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Tipo de Permiso</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Motivo</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Editar</th>  
                    <th style="text-align:center;background-color:#386D95;color:white;">Eliminar</th>                  
                </tr>
            </thead>
            <tbody>
                <?php
                    $result =$db->prepare($query1);
                    $result->execute();
                    while ($fila = $result->fetch()) {
                        ?>
                        <tr data-id='<?php echo $fila["id_tipo_permiso"]; ?>'>
                            <td style = "display:none"><?php echo $fila["id_tipo_permiso"]; ?></td>
                            <td><?php echo $fila["tipo_permiso"]; ?></td>
                            <td id='<?php echo $fila["Motivo_ID"]; ?>'=><?php echo $fila["descripcion_motivo"]; ?></td>
                            <td>
                                <center>
                                    <button type="button" class="editarTipoPermiso btn btn-info glyphicon glyphicon-edit"  title="Editar Tipo de Permiso">
                                </center>
                            </td>       
                            <td>
                                <center>
                                    <button type="button" class="eliminarTipoPermiso<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-remove"  title="Eliminar Tipo de Permiso">
                                </center>
                            </td>     
                        </tr>
                <?php } ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div>

<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#tablaTipoPermisos')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaTipoPermisos').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
    $(document).on("click", ".eliminarTipoPermiso<?php echo $auto_increment; ?>", function () {
        if (confirm("¿Está seguro de que desea eliminar este registro?")){
            codigo = $(this).parents("tr").data("id");
            data={
                Codigo:codigo
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                success: eliminarTipoPermiso,
                timeout: 6000,
                error: problemas
            }); 
            return false;
        }
    });

</script>