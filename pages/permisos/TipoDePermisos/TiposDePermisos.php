<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");

?>

<div class="row">
    <div class="col-lg-10">
        <h3 class="page-header panel-primary"><center><strong>Gestión de Tipos de Permiso </strong></center></h3>
    </div>
</div> 

<div  id="divRespuesta"></div>

<div class="row">
    <div class = "col-lg-10">
        <div class="panel panel-primary">
            <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Agregar nuevo tipo de permiso</div>

            <div class="panel-body">
                <div >
                    <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden</center></div>
                </div>
                <div class="row">
                    <div class="col-lg-10">
                    <!--************************formulario*********************-->
                        <form class="form-horizontal" role="form" id="formTipoPermiso" name="formTipoPermiso">

                            <div class="row form-group" >                    
                                <label class=" col-sm-2 control-label"> Descripción </label>                       
                                <div class="col-sm-6">                            
                                    <input placeholder = "Descripción del tipo de permiso" type="text" class="form-control" id="NombreTipoPermiso" name="NombreTipoPermiso" required>      
                                </div>                  
                            </div>

                            <div class="row form-group">
                                <label class=" col-sm-2 control-label" > Motivo</label>
                                <div class="col-sm-6">                            
                                    <select class="form-control" id="selectMotivo" name="selectMotivo" required>
                                        <option value="-1">Seleccione una opción</option>

                                        <?php
                                            $query = "SELECT * FROM `motivos`";
                                            $result = $db->prepare($query);
                                            $result->execute();
                                            while ($fila = $result->fetch()) {
                                        ?> 
                                                <option value="<?php echo $fila['Motivo_ID']; ?>"> <?php echo $fila["descripcion_motivo"]; ?></option>
                                        <?php
                                            }
                                        ?>

                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <label class="control-label col-sm-1"></label>
                                <div class="col-sm-7">
                                    <p aling ="right">
                                        <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" ><span class=" glyphicon glyphicon-floppy-disk"></span> Agregar</button> 
                                    </p>          
                                </div>                            
                            </div>   
                
                        </form>
                    <!--************************formulario*********************-->
                    </div>
                </div>
            </div>                                    
        </div>
    </div>
</div>


<!--tabla de asignaturas-->
<div class="row">
    <div class="col-lg-10">
        <h3 class="page-header panel-primary"><center>Historial de Tipos de Permiso</center></h3>
    </div>
</div>

<div id="divRespuestaEditar" class="col-lg-10">
</div>

<div id="TiposTabla" class="col-lg-10">
</div>

<!--******************modal de edicion*****************-->
<div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header" style = "background-color:#0FA6C3">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style = "color:white">Editar tipo de permiso</h4>
        </div>
        <div class="modal-body">
            <!--************************formulario*********************-->
                <form class="form-horizontal" role="form" id="formEditarTipoPermiso" name="formEditarTipoPermiso">

                    <div class="row form-group" style = "display:none">                    
                        <label class=" col-sm-2 control-label"> Código</label>                       
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control" id="modalCodigo" name="modalCodigo" required disabled="true">              
                        </div>                  
                    </div>
                    
                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label"> Descripción </label>
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control" id="modalNombre" name="modalNombre" required autocomplete="off">      
                        </div>                  
                    </div>

                    <div class="row form-group">
                        <label class=" col-sm-2 control-label" >Motivo</label>
                        <div class="col-sm-8">                            
                            <select class="form-control" id="modalMotivo" name="modalMotivo" required>
                                <option value="">Seleccione una opción</option>
                                <?php
                                    $query2 = "SELECT * FROM `motivos`";
                                    $resultado =$db->prepare($query2);
                                    $resultado->execute();
                                    while ($filas = $resultado->fetch()) {
                                ?> 
                                        <option value="<?php echo $filas['Motivo_ID']; ?>";> <?php echo $filas["descripcion_motivo"]; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button  id="guardarCambios" class="btn btn-primary"> Guardar</button>
                    </div>
                </form>
            <!--************************formulario*********************-->
        </div>
      </div>
    </div>
</div>

<!--******************modal de edicion*****************-->
<script type="text/javascript" src="pages/permisos/TipoDePermisos/ScriptTipos.js"></script>