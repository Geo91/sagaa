<?php  
    $maindir = "../../../";
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if(isset($_POST['Codigo']) && isset($_POST['Nombre']) && isset($_POST['CodigoM'])){
        $id = $_POST['Codigo'];
        $nombre = $_POST['Nombre'];
        $codM = $_POST['CodigoM'];

        try{
            $sql = "CALL SP_ACTUALIZAR_PERMISO(?,?,?,@mensajeError)";
            $query1 = $db->prepare($sql);
            $query1->bindParam(1,$nombre,PDO::PARAM_STR);
            $query1->bindParam(2,$id,PDO::PARAM_STR);
            $query1->bindParam(3,$codM,PDO::PARAM_STR);
            $query1->execute();

            $output = $db->query("select @mensajeError")->fetch(PDO::FETCH_ASSOC);
            $mensaje = $output['@mensajeError'];

            if(is_null($mensaje)){
            echo  mensajes("El registro seleccionado se ha modificiado exitosamente", "verde");
            }else{
                echo mensajes("Error!  ". $mensaje, "rojo");
            }

        }catch(PDOExecption $e){
            echo mensajes("Error: ".$e, "rojo");
        }
    }else{
        echo mensajes("Hubo un error al intentar modificar el registro. Intentelo de nuevo o contacte al administrador del sistema", "rojo");
    }   

?>

<script type = "text/javascript">
    $("#TiposTabla").load('pages/permisos/TipoDePermisos/DatosTipos.php');
</script>