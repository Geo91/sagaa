 <!-- libreria para generar graficos -->

<link href="css/datepicker.css" rel="stylesheet">
<link href="css/prettify.css" rel="stylesheet">

<script src="js/prettify.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/jsPDF/jspdf.js"></script>
<!-- Procedimento para obtener las solitudes del sistema -->
<script>

/* Cargar la grafica de solicitudes la cual nos muestra las graficas que estan activas en el sistema 
 y tambien las desactivas */
cargarGraficoMotivos();

/* Carga la grafica de estudiantes con el numero de Solicitudes realizadas */
cargarGraficoTipos();

cargarGraficoDepartamentos();

/* Carga la tabla con las solicitudes activas en el sistema */

/* Funcion para cargar dentro del div TipoSolicitudes los graficos en la direccion indicada */
function cargarGraficoMotivos(){
    $("#graficoMotivoSolicitudes").load("pages/permisos/reporteria/graficos/reporteGraficoMotivos.php");
}

function cargarGraficoTipos(){
    $("#graficoTipoSolicitudes").load("pages/permisos/reporteria/graficos/reporteGraficoTipos.php");
}

function cargarGraficoAprobadas(){
    $("#graficoAprobadasDepto").load("pages/permisos/reporteria/graficos/reporteGraficoDepartamentos.php");
}

function cargarGraficoDenegadas(){
    $("#graficoDenegadasDepto").load("pages/permisos/reporteria/graficos/reporteDenegadasDepartamento.php");
}


function cargarGraficoDepartamentos(){
    var datos = {
        accion: 1
     };

    $.ajax({
        async: true,
        type: "POST",
        url: "pages/permisos/reporteria/graficos/verificarUsuario.php",
        data: datos,
        dataType: "html",
        success: function(data){
            var response = JSON.parse(data);
            var index = 0;
            if(response.length > 0){
                $("#oculto").show();
                cargarGraficoAprobadas();
                cargarGraficoDenegadas();
            }
        },
        timeout: 4000
    }); 
}


</script>  

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10">
            <div id="contenedor" class="content-panel">
                <div class="col-lg-12">
                    <h1 class="page-header">Gráfico Mensual de Solicitudes por Motivo y Tipo</h1>
                </div>
            </div>
        <div class="panel-body">
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
</div>

<!-- En esta seccion se muestran los primeros dos divs, el primero muestra el estado  de solicitudes 
    y la cantidad de ellas.
    El segundo el numero de estudiantes dependiendo el tipo de estudiante -->

<!-- div para mostrar errores en el servidor -->
<div class = "col-md-12" id = "notificaciones"></div>
<div class = "col-md-12">
    <div class = "col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <label><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> Solicitudes por Motivo </label>
            </div>
            <div class="panel-body" id = "graficoMotivoSolicitudes"></div>
        </div>
    </div>
    <div class = "col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <label><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> Solicitudes por Tipo </label>
            </div>
            <div class="panel-body" id= "graficoTipoSolicitudes">
            </div>
        </div>
    </div>
</div>
<div class = "col-md-12" id = "oculto" style="display:none">
    <div class = "col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <label><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> Solicitudes Aprobadas por Departamento </label>
            </div>
            <div class="panel-body" id = "graficoAprobadasDepto"></div>
        </div>
    </div>
    <div class = "col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <label><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> Solicitudes Denegadas por Departamento </label>
            </div>
            <div class="panel-body" id = "graficoDenegadasDepto"></div>
        </div>
    </div>
</div>