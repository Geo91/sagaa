<?php 
    header("Content-Type: text/html;charset=utf-8");
    $mkdir = "../../../../";
    include($mkdir."conexion/config.inc.php");

    try{
        $query = "SELECT departamento_laboral.nombre_departamento as DEPARTAMENTO, COUNT(permisos.id_Permisos) as NUMERO_SOLICITUDES from permisos inner join departamento_laboral on (permisos.id_departamento=departamento_laboral.Id_departamento_laboral) where permisos.Estado IN ('Denegado') and fecha_solicitud between CAST(CONCAT(YEAR(NOW()), '-', MONTH(NOW()), '-01') as date) and CAST(LAST_DAY(NOW()) as date) group by DEPARTAMENTO";
        $result = mysql_query($query);
    }
      catch(PDOExecption $e){
        $mensaje="Error en la obtencion de datos";
        $codMensaje =0;
    }
?>

<script>
$(function () {
    $(document).ready(function () {

        // Build the chart
        $('#reporteGraficoDenegadasDepto').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: "Denegadas",
                colorByPoint: true,
                data: [
                <?php
                    $numItems = mysql_num_rows($result);
                    for($i = 0 ;$fila = mysql_fetch_array($result); $i++){
                        if ($i == 1){
                            $nombre = $fila["DEPARTAMENTO"];
                            $y = $fila["NUMERO_SOLICITUDES"];
                            echo "{name: '".$nombre."', y: ".$y.",
                               sliced: true,
                               selected: true }";
                        }else{
                            $nombre = $fila["DEPARTAMENTO"];
                            $y = $fila["NUMERO_SOLICITUDES"];
                            echo "{name: '".$nombre."', y: ".$y."}";
                        }
                        if($i != $numItems) {
                            echo ",";
                        }
                    }  
                ?>]
            }]
        });
    });
});

</script>
<div id ="reporteGraficoDenegadasDepto"></div>