<script type="text/javascript">
   /* Script que permite cargar todos los elementos de la tabla para mostrar los datos que estan
    Almacenados en la base de datos */
$(document).ready(function() {

	obtenerDepartamentos(); //Hacemos el llamado a la funciÃ³n que llenarÃ¡ el combobox para seleccionar el filtro de departamento.

	//FunciÃ³n que vÃ¡lida que los filtros de fecha hayan sido establecidos.
   function validaFecha()
    {
        fechaIn = $("#fechaIn").val();
        fechaFin = $("#fechaFin").val();
        if(fechaIn.length>0)
        {
        	if(fechaFin.length > 0){
        		return true;
        	}else{
        		return false;
        	}
        }else{
            return false;
        }
    }
	
	//FunciÃ³n que obtiene los departamentos que existen en la base de datos y los muestra en el combobox para que el usuario pueda usarlos como filtro para el reporte
    function obtenerDepartamentos() 
    {            
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                url: "pages/permisos/reporteria/reporteDepartamento/cargarDepartamento.php",
                success: function(response){
                                               
                    var arr = JSON.parse(response);
                    
                    var options = '';
                    var val='0';
                    var def='---------------TODOS---------------';
                    options += '<option value="' + val + '">' +
                                    def+ '</option>';
                            
                    for (var index = 0; index < arr.length; index++) 
                    {
                        var codDepartamento = arr[index].codDepartamento;
                        var nombreDepartamento = arr[index].nombre;
                        
                        options += '<option value="' + codDepartamento + '">' +
                                    nombreDepartamento + '</option>';
                    }
                    
                    $("#depto").html(options);                                                        
                    
                },
                timeout: 4000,  
            });
    }

    //PreparaciÃ³n del formulario principal del reporte por departamentos

    $("#formReporte").submit(function (e) {
        e.preventDefault();

            if(validaFecha()===false) //Se valida que se hayaningresados las fechas con que se filtrarÃ¡ el reporte
            {
                alert('El filtro por fechas estÃ¡ incompleto');
            }else{
            	generarReporte();
            }                                                   
    }); //Termina la funciÃ³n de guardado de solicitudes

    function generarReporte(){
    	//Se recopila la informaciÃ³n que se filtrara el reporte
        var datos2 = {fechaI: $("#fechaIn").val(),
        fechaF: $("#fechaFin").val(),
        departamento: $("#depto").val()
        };

        $.ajax({
            async: true,
            type: "POST",
            contentType: "application/x-www-form-urlencoded",
            url: "pages/permisos/reporteria/reporteDepartamento/generarReporteD.php",
            data: datos2,
            dataType: "html",
            success: function(data){
                var response = JSON.parse(data);
                var options = '';
                /* Seccion para llenar la tabla */
                for (var index = 0;index < response.length; index++) 
                {
                   options += '<tr>' +
                                    '<td style="display : none">' + response[index].idPermiso + '</td>' +
                                    '<td>' + response[index].nombreEmp + '</td>' +
                                    '<td>' + response[index].depto + '</td>' +
                                    '<td>' + response[index].tipo + '</td>' +
                                    '<td>' + response[index].motivo + '</td>' +
                                    '<td>' + response[index].fecha + '</td>' +
                                    '<td>' + response[index].dias + '</td>' +
                                    '<td>' + response[index].horaIn + '</td>' +
                                    '<td>' + response[index].horaFin + '</td>' +
                                    '<td>' + response[index].estado + '</td>' +
                                    '<td>' + response[index].observ + '</td>' +
                              '</tr>';
                }

                $('#reporte').show();

                $("#cTablaReporte").html(options);

                $('#TableReporte').dataTable(); /* Script que permite a la tabla hacer busquedas dentro de ella y ordenarla deacuerdo a lo que se presenta en ella. */
                //$("#reporte").show();
            },
            timeout: 4000,
        });
    }
});

</script>

<br>
<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Reporte de Solicitudes por Departamento </div>
    <div class="panel-body" align = "center">
        <div class="row">
            <div class="col-lg-10">
                <form class="form-horizontal" role="form" id="formReporte" name="formReporte">         
                     
                     <div class="row form-group">                            
                        <label class="col-sm-2 control-label">Fecha Inicio</label>
                        <div class="col-sm-2">
                            <input type="date" id="fechaIn" name="datepicker"><span class=""></span>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-sm-2 control-label">Fecha Final</label>
                        <div class="col-sm-2">
                            <input type="date" id="fechaFin" name="datepicker"><span class=""></span>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class=" col-sm-2 control-label">Departamento</label>
                        <div class="col-sm-4">                            
                            <select class="form-control" id="depto" name="depto">
                                <option value="0">----------Todos----------</option>
                            </select>
                        </div>
                    </div>      
                    
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <p align ="left">
                                <button id="generar" type = "submit" class="submit btn btn-primary btn-primary col-sm-offset-1" ><span class=" glyphicon glyphicon-transfer"></span>  Generar Reporte </button> 
                            </p>          
                        </div>                            
                    </div>                                                            
                </form>
            </div>
        </div>
    </div>                                    
</div>                        

<div id = "notificaciones1"></div>

<!-- Seccion usada para mostrar la tabla de las solicitudes de permiso que estan pendientes de revisiÃ³n en el sistema -->
<div class="col-lg-14" id="reporte"" style="display:none">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Historial de Solicitudes </label>
        </div>
        <div class="panel-body">

                   <div id= "noti1" class="alert alert-info" role="alert"><center>A continuación se presentan las solicitudes que los empleados de la facultad de ciencias jurídicas han realizado este mes</center></div>

        
        <div class="panel-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-16">
                    <button class="btn btn-success pull-right" id="btnmuestra" onclick="toexcel('TableReporte','Solicitudes') ;">Exportar Excel <i class="fa fa-file-excel-o" aria-hidden="true"></i></button><br><br><br>
                        <section class="content">
                            <div class="table-responsive">
                                <table id= "TableReporte" border="0" class='table table-bordered table-hover' style="text-align:center;"
>
                                    <thead>
                                        <tr>
                                            <th style = 'display:none'style="text-align:center;color:white;background-color:#386D95;width: 250px;">Codigo</th>
                                            <th style="text-align:center;color:white;background-color:#386D95;width: 280px;">Empleado</th>
                                            <th style="text-align:center;color:white;background-color:#386D95;width: 100px;">Departamento</th>
                                            <th style="text-align:center;color:white;background-color:#386D95;width: 60px;">Tipo</th>
                                            <th style="text-align:center;color:white;background-color:#386D95;width: 70px;">Motivo</th>
                                            <th style="text-align:center;color:white;background-color:#386D95;width: 100px;">Fecha</th>
                                            <th style="text-align:center;color:white;background-color:#386D95;width: 50px;">Dias</th>
                                            <th style="text-align:center;color:white;background-color:#386D95;width: 90px;">Hora Inicio</th>
                                            <th style="text-align:center;color:white;background-color:#386D95;width: 80px;">Hora Fin</th>
                                            <th style="text-align:center;color:white;background-color:#386D95;width: 80px;">Estado</th>
                                            <th style="text-align:center;color:white;background-color:#386D95;">Observación</th>
                                        </tr>
                                    </thead>
                                    <tbody id = "cTablaReporte">
                                        <!-- Contenido de la tabla generado atravez de la consulta a 
                                            la base de datos -->
                                    </tbody>
                                </table>       
                            </div>
                        </section>
                    </div>                
                </div>
            </div>       
        </div>
        </div>
    </div>
</div>
<script src="js/toExcel.js"></script>

<script>
   function toexcel(tabla,worksheet){
   //$('#'+tabla).dataTable().fnDestroy();
   tableToExcel(TableReporte,worksheet);
   //$('#'+tabla).dataTable();
  }
</script>