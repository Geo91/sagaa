<?php
    $maindir = "../../../../";

    //require_once ("../../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    $idUsuario = $_SESSION['user_id'];
    $qd = "SELECT empleado.id_dependencia FROM empleado WHERE empleado.No_Empleado = (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = :piu)";
    $rqd = $db->prepare($qd);
    $rqd->bindParam(":piu",$idUsuario);
    $rqd->execute();

    while ($fila = $rqd->fetch()){
        $dp = $fila['id_dependencia'];
    }

    if($dp == 3){
        $qm = "SELECT motivos.Motivo_ID, motivos.descripcion_motivo FROM motivos WHERE motivos.especial = '1'";
    }else{
        $qm = "SELECT motivos.Motivo_ID, motivos.descripcion_motivo FROM motivos WHERE motivos.especial = '0'";
    }

?>

<div id="divRespuesta"></div>

<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span><strong> Reporte de Permisos por Motivos </strong></div>
    <div class="panel-body" >
        <div class="row">
            <form class="form-horizontal col-sm-12" role="form" id="frmotivos" name="reporteMotivos">

                <div class="row form-group" >                    
                    <label class=" col-sm-2 control-label" >Fecha Inicio</label>
                    <div class="col-sm-2">     
                        <input type="date" class="form-control" id="fechaI" name="fechaInicio" required>      
                    </div>   
                </div>

                <div class="row form-group">                              
                    <label class=" col-sm-2 control-label" >Fecha Fin</label>
                    <div class="col-sm-2">     
                        <input type="date" class="form-control" id="fechaF" name="fechaFinal" required>      
                    </div>                  
                </div>
                <br>

                <div class="row form-group">
                    <label class=" col-sm-2 control-label" >Motivo: </label>
                    <div class="col-sm-4">                            
                        <select class="form-control" id="motivo" name="Motivos" required>
                            <option value="-1">---------------Todos---------------</option>
                            <?php
                                $rqm = $db->prepare($qm);
                                $rqm->execute(); 
                                while ($fqm = $rqm->fetch()) {
                            ?> 
                                    <option value="<?php echo $fqm['Motivo_ID']; ?>">
                                    <?php echo $fqm["descripcion_motivo"]; ?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <label class="control-label col-sm-3"></label>
                    <div class="col-sm-6">
                        <p aling ="right">
                            <button id="generarrp" class="btn btn-primary btn-primary col-sm-offset-10"> Generar Reporte <span class=" glyphicon glyphicon-arrow-right"></span></button> 
                        </p>          
                    </div>                            
                </div>

            </form>
        </div>
    </div>
</div>

<div id="divReporte"></div>                      

<script type="text/javascript" src="pages/permisos/reportes/motivos/scriptrmotivos.js"></script>