<script type="text/javascript">
   /* Script que permite cargar todos los elementos de la tabla para mostrar los datos que estan
    Almacenados en la base de datos */

//Preparación del formulario principal del reporte por departamentos
$(document).ready(function() {

	obtenerMotivos(); //Hacemos el llamado a la función que llenará el combobox para seleccionar el filtro de departamento.

	//Función que válida que los filtros de fecha hayan sido establecidos.
   function validaFecha()
    {
        fechaIn = $("#fechaIn").val();
        fechaFin = $("#fechaFin").val();
        if(fechaIn.length>0)
        {
        	if(fechaFin.length > 0){
        		return true;
        	}else{
        		return false;
        	}
        }else{
            return false;
        }
    } //fin de la función validaFecha
	
	//Función que obtiene los motivos que existen en la base de datos y los muestra en el combobox para que el usuario pueda usarlos como filtro para el reporte
    function obtenerMotivos()
    {                      
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            url: "pages/permisos/motivo/cargarMotivos.php",
            success: function(response){                       
                var arr = JSON.parse(response);
                var options = '';
                var val='0';
                var def='---------------TODOS--------------';
                options += '<option value="' + val + '">' +
                                def+ '</option>';
                        
                for (var index = 0; index < arr.length; index++) 
                {
                    var codMotivo= arr[index].codMotivo;
                    var descMotivo = arr[index].descMotivo;
                    
                    options += '<option value="' + codMotivo + '">' +
                                descMotivo + '</option>';
                }
                $("#motivo").html(options);                                                        
            },
            timeout: 4000,
        });
    } //Fin de la función obtenerMotivos.

    //Función llamada por el botón "Generar Reporte"
    $("#formReporte").submit(function (e) {
        e.preventDefault();
            if(validaFecha()===false) //Se valida que se hayaningresados las fechas con que se filtrará el reporte
            {
                alert('El filtro por fechas está incompleto');
            }else{
            	generarReporte();
            }                                                   
    }); //Termina la función de guardado de solicitudes

    //Función que genera el reporte y muestra la tabla de datos en pantalla
    function generarReporte(){
    	//Se recopila la información que se filtrara el reporte
        var datos2 = {fechaI: $("#fechaIn").val(),
        fechaF: $("#fechaFin").val(),
        motivo: $("#motivo").val()
        };

        $.ajax({
            async: true,
            type: "POST",
            contentType: "application/x-www-form-urlencoded",
            url: "pages/permisos/reporteria/reporteMotivos/generarReporteM.php",
            data: datos2,
            dataType: "html",
            success: function(data){
                var response = JSON.parse(data);
                var options = '';
                /* Seccion para llenar la tabla */
                for (var index = 0;index < response.length; index++) 
                {
                   options += '<tr>' +
                                    '<td style="display : none">' + response[index].idPermiso + '</td>' +
                                    '<td>' + response[index].nombreEmp + '</td>' +
                                    '<td>' + response[index].depto + '</td>' +
                                    '<td>' + response[index].tipo + '</td>' +
                                    '<td>' + response[index].motivo + '</td>' +
                                    '<td>' + response[index].fecha + '</td>' +
                                    '<td>' + response[index].dias + '</td>' +
                                    '<td>' + response[index].horaIn + '</td>' +
                                    '<td>' + response[index].horaFin + '</td>' +
                                    '<td>' + response[index].estado + '</td>' +
                                    '<td>' + response[index].observ + '</td>' +
                              '</tr>';
                }

                $('#reporte').show(); //Revela la tabla que muestra la información obtenida

                $("#cTablaReporte").html(options); //Llena la tabla con la información obtenida

                $('#TableReporte').dataTable(); /* Script que permite a la tabla hacer busquedas dentro de ella y ordenarla deacuerdo a lo que se presenta en ella. */
            },
            timeout: 4000,
        });
    } //fin de la función generarReporte
});

</script>

<!--Código html que muestra el formulario de campos para filtrar el reporte-->
<br>
<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Reporte de Solicitudes por Motivo </div>
    <div class="panel-body" align = "center">
        <div class="row">
            <div class="col-lg-10">
                <form class="form-horizontal" role="form" id="formReporte" name="formReporte">         
                     
                     <div class="row form-group">                            
                        <label class="col-sm-2 control-label">Fecha Inicio</label>
                        <div class="col-sm-2">
                            <input type="date" id="fechaIn" name="datepicker"><span class=""></span>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-sm-2 control-label">Fecha Final</label>
                        <div class="col-sm-2">
                            <input type="date" id="fechaFin" name="datepicker"><span class=""></span>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class=" col-sm-2 control-label">Motivo</label>
                        <div class="col-sm-4">                            
                            <select class="form-control" id="motivo" name="motivo">
                                <option value="0">----------Todos----------</option>
                            </select>
                        </div>
                    </div>      
                    
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <p align ="left">
                                <button id="generar" type = "submit" class="submit btn btn-primary btn-primary col-sm-offset-1" ><span class=" glyphicon glyphicon-transfer"></span>  Generar Reporte </button> 
                            </p>          
                        </div>                            
                    </div>                                                            
                </form>
            </div>
        </div>
    </div>                                    
</div>                        

<div id = "notificaciones1"></div>

<!-- Seccion usada para mostrar la tabla de las solicitudes de permiso que estan pendientes de revisión en el sistema -->
<div class="col-lg-12" id="reporte"" style="display:none">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Historial de Solicitudes </label>
        </div>
        <div class="panel-body">
            <div>
                   <div id= "noti1" class="alert alert-info" role="alert"><center>A continuación se presentan las solicitudes que los empleados de la facultad de ciencias jurídicas han realizado este mes</center></div>
            </div>
        
        <div class="panel-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <section class="content">
                            <div class="table-responsive">
                                <table id= "TableReporte" border="0" class='table table-bordered table-hover'>
                                    <thead>
                                        <tr>
                                            <th style = 'display:none'>Codigo</th>
                                            <th>Empleado</th>
                                            <th>Departamento</th>
                                            <th>Tipo</th>
                                            <th>Motivo</th>
                                            <th>Fecha</th>
                                            <th>Dias</th>
                                            <th>Hora Inicio</th>
                                            <th>Hora Fin</th>
                                            <th>Estado</th>
                                            <th>Observación</th>
                                        </tr>
                                    </thead>
                                    <tbody id = "cTablaReporte">
                                        <!-- Contenido de la tabla generado atravez de la consulta a 
                                            la base de datos -->
                                    </tbody>
                                </table>       
                            </div>
                        </section>
                    </div>                
                </div>
            </div>       
        </div>
        </div>
    </div>
</div>