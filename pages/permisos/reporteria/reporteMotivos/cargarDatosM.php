<?php
    $maindir = "../../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO

if (isset($_POST['fi']) && isset($_POST['ff']) && isset($_POST['idm'])) {

    $i = $_POST['fi'];
    $f = $_POST['ff'];
    $m = $_POST['idm'];
    session_start();
    $idUsuario = $_SESSION['user_id'];

    if($m == -1){
        $conrpm = "SELECT permisos.id_Permisos, motivos.descripcion_motivo, tipodepermiso.tipo_permiso, permisos.fecha_solicitud, permisos.dias_permiso, TIME_FORMAT(permisos.hora_inicio, '%H %i') as 'hora_inicio', TIME_FORMAT(permisos.hora_finalizacion, '%H %i') as 'hora_finalizacion', permisos.justificacion, (CASE permisos.estado WHEN 1 THEN 'No revisado' WHEN 2 THEN 'No revisado' WHEN 3 THEN 'Preaprobado' WHEN 4 THEN 'Denegado' WHEN 5 THEN 'Aprobado' END) as estadoPermiso, permisos.observacion FROM permisos, tipodepermiso, motivos, empleado, departamento_laboral WHERE permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND tipodepermiso.id_tipo_permiso=permisos.id_tipo_permiso AND permisos.id_motivo=motivos.Motivo_ID AND permisos.fecha_solicitud BETWEEN :pfi AND :pff";

        $rconrpm = $db->prepare($conrpm);
        $rconrpm->bindParam(":pfi",$i);
        $rconrpm->bindParam(":pff",$f);

    }else{
        $conrpm = "SELECT permisos.id_Permisos, motivos.descripcion_motivo, tipodepermiso.tipo_permiso, permisos.fecha_solicitud, permisos.dias_permiso, TIME_FORMAT(permisos.hora_inicio, '%H %i') as 'hora_inicio', TIME_FORMAT(permisos.hora_finalizacion, '%H %i') as 'hora_finalizacion', permisos.justificacion, (CASE permisos.estado WHEN 1 THEN 'No revisado' WHEN 2 THEN 'No revisado' WHEN 3 THEN 'Preaprobado' WHEN 4 THEN 'Denegado' WHEN 5 THEN 'Aprobado' END) as estadoPermiso, permisos.observacion FROM permisos, tipodepermiso, motivos, empleado, departamento_laboral WHERE permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND tipodepermiso.id_tipo_permiso=permisos.id_tipo_permiso AND permisos.id_motivo=motivos.Motivo_ID AND permisos.id_motivo = :pm AND permisos.fecha_solicitud BETWEEN :pfi AND :pff";

        $rconrpm = $db->prepare($conrpm);
        $rconrpm->bindParam(":pfi",$i);
        $rconrpm->bindParam(":pff",$f);
        $rconrpm->bindParam(":pm",$m);

    }
    
    $rconrpm->execute();

?>

<div class="row">
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaReporteMotivos" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style="text-align:center;background-color:#386D95;color:white;<?php if($m > 0){ echo 'display:none'; } ?>">Motivo</th>                                          
                    <th style="text-align:center;background-color:#386D95;color:white;">Tipo</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Fecha</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Días</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Hora Inicio</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Hora Fin</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Justificación</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Estado</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Observación</th>
                </tr>
            </thead>
            <tbody>
            <?php
                while($fila = $rconrpm->fetch()){ 
            ?>
                <tr>
                    <td <?php if($m > 0){ echo 'style="display:none"'; } ?>><?php echo $fila['descripcion_motivo']; ?></td>
                    <td><?php echo $fila['tipo_permiso']; ?></td>
                    <td><?php echo $fila['fecha_solicitud']; ?></td>
                    <td><?php echo $fila['dias_permiso']; ?></td>
                    <td><?php echo $fila['hora_inicio']; ?></td>
                    <td><?php echo $fila['hora_finalizacion']; ?></td>
                    <td><?php echo $fila['justificacion']; ?></td>
                    <td><?php echo $fila['estadoPermiso']; ?></td>
                    <td><?php echo $fila['observacion']; ?></td>
                </tr>
            <?php    
                }
            ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div>
</div>

<script type="text/javascript">

    $('#tablaReporteMotivos')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaReporteMotivos').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
</script>

<?php }else{
    echo mensajes("Problemas en la página, intentelo de nuevo o contacte al administrador del sistema.","rojo");
} ?>