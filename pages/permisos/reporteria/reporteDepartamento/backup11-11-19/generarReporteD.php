<?php
    $maindir = "../../../../";
    include($maindir."conexion/config.inc.php");

    $fechaIni = $_POST['fechaI'];
    $fechaFin = $_POST['fechaF'];
    $idDepto = $_POST['departamento'];

    if($idDepto == 0){
    	$query = "SELECT permisos.id_Permisos, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) as 'Empleado', departamento_laboral.nombre_departamento, tipodepermiso.tipo_permiso, motivos.descripcion_motivo, permisos.fecha, permisos.dias_permiso, permisos.hora_inicio, permisos.hora_finalizacion, (CASE permisos.estado WHEN 1 THEN 'Espera' WHEN 2 THEN 'Espera' WHEN 3 THEN 'Preaprobado' WHEN 4 THEN 'Denegado' WHEN 5 THEN 'Aprobado' END) as estadoPermiso, permisos.observacion FROM permisos, persona, tipodepermiso, motivos, empleado, departamento_laboral WHERE empleado.N_identidad=persona.N_Identidad AND permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND tipodepermiso.id_tipo_permiso=permisos.id_tipo_permiso AND permisos.id_motivo=motivos.Motivo_ID AND permisos.fecha_solicitud BETWEEN '".$fechaIni."' AND '".$fechaFin."'";
    }else{
    	$query = "SELECT permisos.id_Permisos, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) as 'Empleado', departamento_laboral.nombre_departamento, tipodepermiso.tipo_permiso, motivos.descripcion_motivo, permisos.fecha, permisos.dias_permiso, permisos.hora_inicio, permisos.hora_finalizacion, (CASE permisos.estado WHEN 1 THEN 'Espera' WHEN 2 THEN 'Espera' WHEN 3 THEN 'Preaprobado' WHEN 4 THEN 'Denegado' WHEN 5 THEN 'Aprobado' END) as estadoPermiso, permisos.observacion FROM permisos, persona, tipodepermiso, motivos, empleado, departamento_laboral WHERE empleado.N_identidad=persona.N_Identidad AND permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND tipodepermiso.id_tipo_permiso=permisos.id_tipo_permiso AND permisos.id_motivo=motivos.Motivo_ID AND departamento_laboral.Id_departamento_laboral = '".$idDepto."' AND permisos.fecha_solicitud BETWEEN '".$fechaIni."' AND '".$fechaFin."'";
    }

    $result = mysql_query($query);
    $json = array();
    $iteracion = 0;

    while ($linea =  mysql_fetch_array($result)){
        $json[$iteracion] = array
            (
                "idPermiso" => utf8_encode($linea["id_Permisos"]),
                "nombreEmp" => utf8_encode($linea["Empleado"]),
                "depto" => utf8_encode($linea["nombre_departamento"]),
                "tipo" => utf8_encode($linea["tipo_permiso"]),
                "motivo" => utf8_encode($linea["descripcion_motivo"]),
                "fecha" => utf8_encode($linea["fecha"]),
                "dias" => utf8_encode($linea["dias_permiso"]),
                "horaIn" => utf8_encode($linea["hora_inicio"]),
                "horaFin" => utf8_encode($linea["hora_finalizacion"]),
                "estado" => utf8_encode($linea["estadoPermiso"]),
                "observ" => utf8_encode($linea["observacion"])
            ); 
        $iteracion++;
    }
	echo json_encode($json);
?>