$(document).ready(function(){   

//para cuando se da submit al formulario formProgramacion
    $("#frdeptos").submit(function(e) {
        e.preventDefault();
        var finicio = $('#fechaI').val();
        var ffin = $('#fechaF').val();
        var dept = $('#depto').val();
        var dpd = $('#dp').val();

        if(ffin > finicio){
            data={
                fi:finicio,
                ff:ffin,
                idd:dept,
                dep:dpd
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                beforeSend: inicioEnvio,
                success: llenarTabla,
                timeout: 9000,
                error: problemas
            }); 
            return false;
        }else{
            alert("La fecha de fin debe ser mayor a la fecha de inicio.");
        }
        
    });
});

function inicioEnvio(){
    $("#divReporte").text('Cargando...');
}

function problemas(){
    $("#divReporte").text('Problemas en el servidor.');
}

function llenarTabla(){
    $("#divReporte").load('pages/permisos/reportes/depto/cargarDatosD.php',data);
}