<?php
    $maindir = "../../../../";

    //require_once ("../../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    $idUsuario = $_SESSION['user_id'];
    $qd = "SELECT empleado.id_dependencia FROM empleado WHERE empleado.No_Empleado = (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = :piu)";
    $rqd = $db->prepare($qd);
    $rqd->bindParam(":piu",$idUsuario);
    $rqd->execute();

    while ($fila = $rqd->fetch()){
        $dp = $fila['id_dependencia'];
    }

    if($dp == 1){
        $qdepto = "SELECT * FROM departamento_laboral where departamento_laboral.Id_departamento_laboral IN ('7','8','9','10','11','13','14','15','16','18','21','22','23','24')";
    }elseif($dp == 3){
        $qdepto = "SELECT * FROM departamento_laboral where departamento_laboral.Id_departamento_laboral IN ('21','22','23','24')";
    }else{
        $qdepto = "SELECT * FROM departamento_laboral where departamento_laboral.Id_departamento_laboral IN ('15')";
    }

?>

<div id="divRespuesta"></div>

<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span><strong> Reporte de Permisos por Departamentos </strong></div>
    <div class="panel-body" >
        <div class="row">
            <form class="form-horizontal col-sm-12" role="form" id="frdeptos" name="reporteDepartamentos">

                <div class="row form-group" style="display:none">         
                    <label class="col-sm-3 control-label">Dependencia</label>
                    <div class="col-sm-2">
                            <input type="text" class="form-control" id="dp" name="Dependencia" value = "<?php echo $dp; ?>" required disabled>                         
                    </div>                  
                </div>

                <div class="row form-group" >                    
                    <label class=" col-sm-2 control-label" >Fecha Inicio</label>
                    <div class="col-sm-2">     
                        <input type="date" class="form-control" id="fechaI" name="fechaInicio" required>      
                    </div>   
                </div>

                <div class="row form-group">                              
                    <label class=" col-sm-2 control-label" >Fecha Fin</label>
                    <div class="col-sm-2">     
                        <input type="date" class="form-control" id="fechaF" name="fechaFinal" required>      
                    </div>                  
                </div>
                <br>

                <div class="row form-group">
                    <label class=" col-sm-2 control-label" >Departamento: </label>
                    <div class="col-sm-4">                            
                        <select class="form-control" id="depto" name="Departamentos" required>
                            <option value="-1">---------------Todos---------------</option>
                            <?php
                                $rqdepto = $db->prepare($qdepto);
                                $rqdepto->execute(); 
                                while ($fqdepto = $rqdepto->fetch()) {
                            ?> 
                                    <option value="<?php echo $fqdepto['Id_departamento_laboral']; ?>">
                                    <?php echo $fqdepto["nombre_departamento"]; ?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <label class="control-label col-sm-3"></label>
                    <div class="col-sm-6">
                        <p aling ="right">
                            <button id="generarrp" class="btn btn-primary btn-primary col-sm-offset-10"> Generar Reporte <span class=" glyphicon glyphicon-arrow-right"></span></button> 
                        </p>          
                    </div>                            
                </div>

            </form>
        </div>
    </div>
</div>

<div id="divReporte"></div>                      

<script type="text/javascript" src="pages/permisos/reportes/depto/scriptrdeptos.js"></script>