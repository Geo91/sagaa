<script type="text/javascript">
   /* Script que permite cargar todos los elementos de la tabla para mostrar los datos que estan
    Almacenados en la base de datos */
$(document).ready(function() {

	obtenerDepartamentos(); //Hacemos el llamado a la función que llenará el combobox para seleccionar el filtro de departamento.

	//Función que válida que los filtros de fecha hayan sido establecidos.
   function validaFecha()
    {
        fechaIn = $("#fechaIn").val();
        fechaFin = $("#fechaFin").val();
        if(fechaIn.length>0)
        {
        	if(fechaFin.length > 0){
        		return true;
        	}else{
        		return false;
        	}
        }else{
            return false;
        }
    }
	
	//Función que obtiene los departamentos que existen en la base de datos y los muestra en el combobox para que el usuario pueda usarlos como filtro para el reporte
    function obtenerDepartamentos() 
    {            
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                url: "pages/permisos/reporteria/reporteDepartamento/cargarDepartamento.php",
                success: function(response){
                                               
                    var arr = JSON.parse(response);
                    
                    var options = '';
                    var val='0';
                    var def='---------------TODOS---------------';
                    options += '<option value="' + val + '">' +
                                    def+ '</option>';
                            
                    for (var index = 0; index < arr.length; index++) 
                    {
                        var codDepartamento = arr[index].codDepartamento;
                        var nombreDepartamento = arr[index].nombre;
                        
                        options += '<option value="' + codDepartamento + '">' +
                                    nombreDepartamento + '</option>';
                    }
                    
                    $("#depto").html(options);                                                        
                    
                },
                timeout: 4000,  
            });
    }

    //Preparación del formulario principal del reporte por departamentos

    $("#formReporte").submit(function (e) {
        e.preventDefault();

            if(validaFecha()===false) //Se valida que se hayaningresados las fechas con que se filtrará el reporte
            {
                alert('El filtro por fechas está incompleto');
            }else{
            	generarReporte();
            }                                                   
    }); //Termina la función de guardado de solicitudes

    function generarReporte(){
    	//Se recopila la información que se filtrara el reporte
        var datos2 = {fechaI: $("#fechaIn").val(),
        fechaF: $("#fechaFin").val(),
        departamento: $("#depto").val()
        };

        $.ajax({
            async: true,
            type: "POST",
            contentType: "application/x-www-form-urlencoded",
            url: "pages/permisos/reporteria/reporteDepartamento/generarReporteD.php",
            data: datos2,
            dataType: "html",
            success: function(data){
                var response = JSON.parse(data);
                var options = '';
                /* Seccion para llenar la tabla */
                for (var index = 0;index < response.length; index++) 
                {
                   options += '<tr>' +
                                    '<td style="display : none">' + response[index].idPermiso + '</td>' +
                                    '<td>' + response[index].nombreEmp + '</td>' +
                                    '<td>' + response[index].depto + '</td>' +
                                    '<td>' + response[index].tipo + '</td>' +
                                    '<td>' + response[index].motivo + '</td>' +
                                    '<td>' + response[index].fecha + '</td>' +
                                    '<td>' + response[index].dias + '</td>' +
                                    '<td>' + response[index].horaIn + '</td>' +
                                    '<td>' + response[index].horaFin + '</td>' +
                                    '<td>' + response[index].estado + '</td>' +
                                    '<td>' + response[index].observ + '</td>' +
                              '</tr>';
                }

                $('#reporte').show();

                $("#cTablaReporte").html(options);

                $('#TableReporte').dataTable(); /* Script que permite a la tabla hacer busquedas dentro de ella y ordenarla deacuerdo a lo que se presenta en ella. */
                //$("#reporte").show();
            },
            timeout: 4000,
        });
    }
});

</script>

<br>
<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Reporte de Solicitudes por Departamento </div>
    <div class="panel-body" align = "center">
        <div class="row">
            <div class="col-lg-10">
                <form class="form-horizontal" role="form" id="formReporte" name="formReporte">         
                     
                     <div class="row form-group">                            
                        <label class="col-sm-2 control-label">Fecha Inicio</label>
                        <div class="col-sm-2">
                            <input type="date" id="fechaIn" name="datepicker"><span class=""></span>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-sm-2 control-label">Fecha Final</label>
                        <div class="col-sm-2">
                            <input type="date" id="fechaFin" name="datepicker"><span class=""></span>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class=" col-sm-2 control-label">Departamento</label>
                        <div class="col-sm-4">                            
                            <select class="form-control" id="depto" name="depto">
                                <option value="0">----------Todos----------</option>
                            </select>
                        </div>
                    </div>      
                    
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <p align ="left">
                                <button id="generar" type = "submit" class="submit btn btn-primary btn-primary col-sm-offset-1" ><span class=" glyphicon glyphicon-transfer"></span>  Generar Reporte </button> 
                            </p>          
                        </div>                            
                    </div>                                                            
                </form>
            </div>
        </div>
    </div>                                    
</div>                        

<div id = "notificaciones1"></div>

<!-- Seccion usada para mostrar la tabla de las solicitudes de permiso que estan pendientes de revisión en el sistema -->
<div class="col-lg-14" id="reporte"" style="display:none">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Historial de Solicitudes </label>
        </div>
        <div class="panel-body">

                   <div id= "noti1" class="alert alert-info" role="alert"><center>A continuación se presentan las solicitudes que los empleados de la facultad de ciencias jurídicas han realizado este mes</center></div>

        
        <div class="panel-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-16">
                        <section class="content">
                            <div class="table-responsive">
                                <table id= "TableReporte" border="0" class='table table-bordered table-hover' style="text-align:center;background-color:#386D95;"
>
                                    <thead>
                                        <tr>
                                            <th style = 'display:none'>Codigo</th>
                                            <th style="text-align:center;color:white;">Empleado</th>
                                            <th style="text-align:center;color:white;">Departamento</th>
                                            <th style="text-align:center;color:white;">Tipo</th>
                                            <th style="text-align:center;color:white;">Motivo</th>
                                            <th style="text-align:center;color:white;">Fecha</th>
                                            <th style="text-align:center;color:white;">Dias</th>
                                            <th style="text-align:center;color:white;">Hora Inicio</th>
                                            <th style="text-align:center;color:white;">Hora Fin</th>
                                            <th style="text-align:center;color:white;">Estado</th>
                                            <th style="text-align:center;color:white;">Observación</th>
                                        </tr>
                                    </thead>
                                    <tbody id = "cTablaReporte">
                                        <!-- Contenido de la tabla generado atravez de la consulta a 
                                            la base de datos -->
                                    </tbody>
                                </table>       
                            </div>
                        </section>
                    </div>                
                </div>
            </div>       
        </div>
        </div>
    </div>
</div>