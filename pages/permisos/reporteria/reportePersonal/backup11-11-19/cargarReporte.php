<?php
    $maindir = "../../../../";
    include($maindir."conexion/config.inc.php");

    session_start();
    $idUsuario = $_SESSION['user_id'];
        $query = "SELECT permisos.id_Permisos, tipodepermiso.tipo_permiso, motivos.descripcion_motivo, permisos.fecha_solicitud, permisos.dias_permiso, permisos.hora_inicio, permisos.hora_finalizacion, (CASE permisos.estado WHEN 1 THEN 'Espera' WHEN 2 THEN 'Espera' WHEN 3 THEN 'Preaprobado' WHEN 4 THEN 'Denegado' WHEN 5 THEN 'Aprobado' END) as estadoPermiso, permisos.observacion FROM permisos, tipodepermiso, motivos, empleado, departamento_laboral WHERE permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND tipodepermiso.id_tipo_permiso=permisos.id_tipo_permiso AND permisos.id_motivo=motivos.Motivo_ID AND permisos.fecha_solicitud BETWEEN CONCAT(YEAR(NOW()),'-',MONTH(NOW()),'-01') AND LAST_DAY(NOW()) AND permisos.No_Empleado IN (SELECT No_Empleado FROM usuario WHERE id_Usuario = '".$idUsuario."')";

    $result = mysql_query($query);
    $json = array();
    $iteracion = 0;

    while ($linea =  mysql_fetch_array($result)){
        $json[$iteracion] = array
            (
                "idPermiso" => utf8_encode($linea["id_Permisos"]),
                "tipo" => utf8_encode($linea["tipo_permiso"]),
                "motivo" => utf8_encode($linea["descripcion_motivo"]),
                "fecha" => utf8_encode($linea["fecha_solicitud"]),
                "dias" => utf8_encode($linea["dias_permiso"]),
                "horaIn" => utf8_encode($linea["hora_inicio"]),
                "horaFin" => utf8_encode($linea["hora_finalizacion"]),
                "estado" => utf8_encode($linea["estadoPermiso"]),
                "observ" => utf8_encode($linea["observacion"])
            ); 
        $iteracion++;
    }
	echo json_encode($json);
?>