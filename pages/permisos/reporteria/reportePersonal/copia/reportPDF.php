<?php

$maindir = "../../../../";

require_once($maindir."fpdf/fpdf.php");
//require($maindir."conexion/config.inc.php");
require($maindir."Datos/conexion.php");

session_start();
$idUsuario = $_SESSION['user_id'];
    $query = "SELECT permisos.id_Permisos, tipodepermiso.tipo_permiso, motivos.descripcion_motivo, permisos.fecha_solicitud, permisos.dias_permiso, permisos.hora_inicio, permisos.hora_finalizacion, permisos.estado, permisos.observacion FROM permisos, tipodepermiso, motivos, empleado, departamento_laboral WHERE permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND tipodepermiso.id_tipo_permiso=permisos.id_tipo_permiso AND permisos.id_motivo=motivos.Motivo_ID AND permisos.fecha_solicitud BETWEEN CONCAT(YEAR(NOW()),'-',MONTH(NOW()),'-01') AND LAST_DAY(NOW()) AND permisos.No_Empleado IN (SELECT No_Empleado FROM usuario WHERE id_Usuario = '".$idUsuario."')";

class PDF extends FPDF{
     // Cabecera de página
         
    function Footer(){

    	$this->SetY(15);
        $this->SetFont('Arial','I',8);
        $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().' de {nb}   - - - -   Impreso el ' . date('d-m-y') . ' fecha del sistema ',0,0,'C');

        $this->SetY(-15);
        $this->SetFont('Arial','I',8);
        $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().' de {nb}   - - - -   Impreso el ' . date('d-m-y') . ' fecha del sistema ',0,0,'C');
    }
         
}

$pdf = new PDF();
$pdf->AliasNbPages();

$pdf->Output('ReportePersonal.pdf','I');

?>