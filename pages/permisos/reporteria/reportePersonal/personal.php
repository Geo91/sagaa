<?php
    $maindir = "../../../../";

    //require_once ("../../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

?>

<div id="divRespuesta"></div>

<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span><strong> Reporte de Permisos Personales </strong></div>
    <div class="panel-body" >
        <div class="row">
            <form class="form-horizontal col-sm-12" role="form" id="frpersonal" name="reportePersonal">

                <div class="row form-group" >                    
                    <label class=" col-sm-2 control-label" >Fecha Inicio</label>
                    <div class="col-sm-2">     
                        <input type="date" class="form-control" id="fechaI" name="fechaInicio" required>      
                    </div>   
                </div>

                <div class="row form-group">                              
                    <label class=" col-sm-2 control-label" >Fecha Fin</label>
                    <div class="col-sm-2">     
                        <input type="date" class="form-control" id="fechaF" name="fechaFinal" required>      
                    </div>                  
                </div>
                <br>

                <div class="row">
                    <label class="control-label col-sm-3"></label>
                    <div class="col-sm-6">
                        <p aling ="right">
                            <button id="generarrp" class="btn btn-primary btn-primary col-sm-offset-10"> Generar Reporte <span class=" glyphicon glyphicon-arrow-right"></span></button> 
                        </p>          
                    </div>                            
                </div>

            </form>
        </div>
    </div>
</div>

<div id="divReporte"></div>                      

<script type="text/javascript" src="pages/permisos/reportes/personal/scriptPersonal.js"></script>