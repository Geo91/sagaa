<?php

$maindir = "../../../../";

require_once($maindir."fpdf/fpdf.php");
//require($maindir."conexion/config.inc.php");
require($maindir."conexion/config.inc.php");

session_start();
$idUsuario = $_SESSION['user_id'];
$rol = $_SESSION['user_rol'];

if($rol==50 or $rol==100){
    $sql = "SELECT CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) as 'Empleado', departamento_laboral.nombre_departamento, tipodepermiso.tipo_permiso, motivos.descripcion_motivo, permisos.fecha, permisos.hora_inicio, permisos.hora_finalizacion, permisos.estado, permisos.observacion FROM permisos, persona, tipodepermiso, motivos, empleado, departamento_laboral WHERE empleado.N_identidad=persona.N_Identidad AND permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND tipodepermiso.id_tipo_permiso=permisos.id_tipo_permiso AND permisos.id_motivo=motivos.Motivo_ID AND permisos.fecha_solicitud BETWEEN CONCAT(YEAR(NOW()),'-',MONTH(NOW()),'-01') AND LAST_DAY(NOW())";
}else{
    $sql = "SELECT CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) as 'Empleado', departamento_laboral.nombre_departamento, tipodepermiso.tipo_permiso, motivos.descripcion_motivo, permisos.fecha, permisos.hora_inicio, permisos.hora_finalizacion, permisos.estado, permisos.observacion FROM permisos, persona, tipodepermiso, motivos, empleado, departamento_laboral WHERE empleado.N_identidad=persona.N_Identidad AND permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND tipodepermiso.id_tipo_permiso=permisos.id_tipo_permiso AND permisos.id_motivo=motivos.Motivo_ID AND permisos.fecha_solicitud BETWEEN CONCAT(YEAR(NOW()),'-',MONTH(NOW()),'-01') AND LAST_DAY(NOW()) AND permisos.jefe_inmediato IN (SELECT No_Empleado FROM usuario WHERE id_Usuario = '".$idUsuario."')";
}    

	$query = $db->prepare($sql);
	$query->execute();
	$rows = $query->fetchAll();

class PDF extends FPDF{
     // Cabecera de página
         
    function Footer(){

    	$maindir = "../../../../";

    	$this->SetY(15);
    	$this->SetFont('Arial', '', 18);
    	$this->Image($maindir.'assets/img/cabecera(2).png', 0,0,280,30, 'PNG');

        $this->SetFont('Arial','I',8);
        $this->Image($maindir.'assets/img/pieDoc.png', 0,130,280,85, 'PNG');

        $this->SetY(-30);
        $this->SetFont('Arial','I',8);
        $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().' de {nb}   - - - -   Impreso el ' . date('d-m-y') . ' fecha del sistema   - - - -'. utf8_decode(' Impresión de Prueba'),0,0,'C');
    }
         
}

$pdf = new PDF('L','mm','LETTER');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(18);
$pdf->Cell(38, 10, '', 0);
$pdf->SetFont('Arial', 'U', 18);
$pdf->Cell(40, 10, '', 0);
$pdf->Cell(70, 10, utf8_decode('Reporte Personal - Permisos del Mes'), 0);
$pdf->Ln(5);
$pdf->SetFont('Arial', '', 12);
$pNombre = $result['Primer_nombre'];
$sNombre = $result['Segundo_nombre'];
$pApellido = $result['Primer_apellido'];
$sApellido = $result['Segundo_Apellido'];
$nombreCompleto = "{$pNombre} {$sNombre} {$pApellido} {$sApellido}";
$pdf->Cell(5);
$pdf->Cell(80, 20, 'No. Empleado:  '.$result['No_Empleado'], 0,0,"");
$pdf->Cell(15);
$pdf->Cell(80, 20, 'Departamento:  '.$result['nombre_departamento'], 0,0,"");
$pdf->Ln(6);
$pdf->Cell(5);
$pdf->Cell(80, 20, 'Nombre:  '.$nombreCompleto, 0,0,"");
$pdf->Cell(15);
$pdf->Cell(80, 20, 'Cargo:  '.$result['Cargo'], 0,0,"");
$pdf->Ln(15);
$pdf->SetFont('Arial', 'B', 8);

$pdf->SetTextColor(255,255,255);  // Establece el color del texto (en este caso es blanco)
$pdf->SetFillColor(2, 144, 188); // establece el color del fondo de la celda (en este caso es AZUL
$pdf->Cell(5);
$pdf->Cell(30, 8, utf8_decode("Motivo"), 1,0,"C", True);
$pdf->Cell(50, 8, utf8_decode("Tipo de Permiso"), 1,0,"C", True);
$pdf->Cell(25, 8, utf8_decode("Fecha"), 1,0,"C", True);
$pdf->Cell(15, 8, utf8_decode("Días"), 1,0,"C", True);
$pdf->Cell(25, 8, utf8_decode("Hora Inicio"), 1,0,"C", True);
$pdf->Cell(25, 8, utf8_decode("Hora Fin"), 1,0,"C", True);
$pdf->Cell(30, 8, utf8_decode("Estado"), 1,0,"C", True);
$pdf->Cell(50, 8, utf8_decode("Observaciones"), 1,0,"C", True);
$pdf->Ln(8);
$pdf->SetTextColor(0,0,0);

$Motivo=array();
$TipoPermiso=array();
$Fecha =array();
$Dias=array();
$HoraI=array();
$HoraF=array();
$Estado=array();
$Obs=array();

function array_ref(&$arreglo,$cadena,$limite) {
    $limite=$limite-10;
    $contador=0;
	if (strlen($cadena)<$limite){
		$arreglo[$contador]=$cadena;
	}else{

    	while ( strlen($cadena)>$limite) {
	        $cadena2=substr($cadena,0,$limite);
	        $arreglo[$contador]=$cadena2;
	        $contador=$contador+1;
	        $cadena3=substr($cadena,$limite-1,strlen($cadena));
	        $cadena=$cadena3;
	        if (strlen($cadena)< $limite) {
	            
	            $arreglo[$contador]=$cadena;
	            $cadena="";
	        }
   		}  
  	}
}

foreach ($rows as $row) {
	array_ref($Motivo,$row["descripcion_motivo"],30);
	array_ref($TipoPermiso,$row["tipo_permiso"],50);
	array_ref($Fecha,$row["fecha_solicitud"],25);
	array_ref($Dias,$row["dias_permiso"],15);
	array_ref($HoraI,$row["hora_inicio"],25);
	array_ref($HoraF,$row["hora_finalizacion"],25);
	array_ref($Estado,$row["estado"],30);
	array_ref($Obs,$row["observacion"],50);
	$numeroMayor[1]=count($Motivo);
	$numeroMayor[2]=count($TipoPermiso);
	$numeroMayor[3]=count($Fecha);
	$numeroMayor[4]=count($Dias);
	$numeroMayor[5]=count($HoraI);
	$numeroMayor[6]=count($HoraF);
	$numeroMayor[7]=count($Estado);
	$numeroMayor[8]=count($Obs);
	$n= max($numeroMayor);

	$contador=0;
	while ( $contador<$n) {

	  	if ($contador==$n-1){

	     	if($contador<count($Motivo)){
	     		$pdf->Cell(5);
	            $pdf->CellFitSpace(30, 8, utf8_decode($Motivo[$contador]), "L,R,B",0,"C");
	       	}else{
	            $pdf->Cell(30, 8, utf8_decode(" "), "L,R,B",0,"C");
	        }

	  		if($contador<count($TipoPermiso)){
	            $pdf->CellFitScale(50, 8, utf8_decode($TipoPermiso[$contador]), "L,R,B",0,"C");
	        }else{
	            $pdf->Cell(50, 8, utf8_decode(""), "L,R,B",0,"C");
	       	}

	  		if($contador<count($Fecha)){
	        	$pdf->CellFitSpace(25, 8, utf8_decode($Fecha[$contador]), "L,R,B",0,"C");
	       	}else{
	   			$pdf->Cell(25, 8, utf8_decode(""), "L,R,B",0,"C");
	 		}

	 		if($contador<count($Dias)){
	   			$pdf->CellFitSpace(15, 8, utf8_decode($Dias[$contador]), "L,R,B",0,"C");
	 		}else{
	   			$pdf->Cell(15, 8, utf8_decode(""), "L,R,B",0,"C");
	   		}

			if($contador<count($HoraI)){
				$pdf->CellFitSpace(25, 8, utf8_decode($HoraI[$contador]), "L,R,B",0,"C");
			}else{
				$pdf->Cell(25, 8, utf8_decode(""), "L,R,B",0,"C");
			}

			if($contador<count($HoraF)){
				$pdf->CellFitSpace(25, 8, utf8_decode($HoraF[$contador]), "L,R,B",0,"C");
			}else{
				$pdf->Cell(25, 8, utf8_decode(""), "L,R,B",0,"C");
			}

			if($contador<count($Estado)){
				$pdf->CellFitSpace(30, 8, utf8_decode($Estado[$contador]), "L,R,B",0,"C");
			}else{
				$pdf->Cell(30, 8, utf8_decode(""), "L,R,B",0,"C");
			}

			if($contador<count($Obs)){
				$pdf->CellFitSpace(50, 8, utf8_decode($Obs[$contador]), "L,R,B",0,"C");
			}else{
				$pdf->Cell(50, 8, utf8_decode(""), "L,R,B",0,"C");
			}
	    
	  	}else{

			if($contador<count($Motivo)){
				$pdf->Cell(5);
	            $pdf->CellFitSpace(30, 8, utf8_decode($Motivo[$contador]), "L,R,B",0,"C");
	       	}else{
	            $pdf->Cell(30, 8, utf8_decode(" "), "L,R,B",0,"C");
	        }

	  		if($contador<count($TipoPermisov)){
	            $pdf->CellFitScale(50, 8, utf8_decode($TipoPermiso[$contador]), "L,R,B",0,"C");
	        }else{
	            $pdf->Cell(50, 8, utf8_decode(""), "L,R,B",0,"C");
	       	}

	  		if($contador<count($Fecha)){
	        	$pdf->CellFitSpace(25, 8, utf8_decode($Fecha[$contador]), "L,R,B",0,"C");
	       	}else{
	   			$pdf->Cell(25, 8, utf8_decode(""), "L,R,B",0,"C");
	 		}

	 		if($contador<count($Dias)){
	   			$pdf->CellFitSpace(15, 8, utf8_decode($Dias[$contador]), "L,R,B",0,"C");
	 		}else{
	   			$pdf->Cell(15, 8, utf8_decode(""), "L,R,B",0,"C");
	   		}

			if($contador<count($HoraI)){
				$pdf->CellFitSpace(25, 8, utf8_decode($TipodeFolio[$contador]), "L,R,B",0,"C");
			}else{
				$pdf->Cell(25, 8, utf8_decode(""), "L,R,B",0,"C");
			}

			if($contador<count($HoraF)){
				$pdf->CellFitSpace(25, 8, utf8_decode($TipodeFolio[$contador]), "L,R,B",0,"C");
			}else{
				$pdf->Cell(25, 8, utf8_decode(""), "L,R,B",0,"C");
			}

			if($contador<count($Estado)){
				$pdf->CellFitSpace(30, 8, utf8_decode($TipodeFolio[$contador]), "L,R,B",0,"C");
			}else{
				$pdf->Cell(30, 8, utf8_decode(""), "L,R,B",0,"C");
			}

			if($contador<count($Obs)){
				$pdf->CellFitSpace(50, 8, utf8_decode($TipodeFolio[$contador]), "L,R,B",0,"C");
			}else{
				$pdf->Cell(50, 8, utf8_decode(""), "L,R,B",0,"C");
			}
		}

		$pdf->Ln();
		$contador=$contador+1;

	}

}

$pdf->Output('ReportePersonal.pdf','I');

?>