<?php
session_start();
    $rol = $_SESSION['user_rol'];
?>

<script type="text/javascript">
   /* Script que permite cargar todos los elementos de la tabla para mostrar los datos que estan
    Almacenados en la base de datos */
$(document).ready(function() {

    //inicializarTabla(); //Se inicializa la tabla que despliega los datos de la base de datos
    cargarSolicitudes(); //Hacemos el llamado para que se cargue la tabla siempre que se cargue la pag.
    
    /* Esta funcion es llamada al principio, es la encargada de actualizar la tabla que muestra las solicitudes pendientes por revisar de cada uno de los jefes de departamento, que estan en el sistema en ese momento, se recarga cada vez que se deniega o se aprueba un permiso */
    function cargarSolicitudes(){
        var datos = {
            accion: 1
        };

        $.ajax({
            async: true,
            type: "POST",
            url: "pages/permisos/reporteria/reporteMensual/cargarReporte.php",
            data: datos,
            dataType: "html",
            success: function(data){
                var response = JSON.parse(data);
                var options = '';
                /* Seccion para llenar la tabla */
                for (var index = 0;index < response.length; index++) 
                {
                   options += '<tr>' +
                                    '<td style="display : none">' + response[index].idPermiso + '</td>' +
                                    '<td>' + response[index].nombreEmp + '</td>' +
                                    <?php
                                        if($rol == 100){
                                            echo "'<td>' + response[index].depto + '</td>' +";
                                        }else{
                                            echo "'<td style='display:none'>' + response[index].depto + '</td>' +";
                                        }
                                    ?>
                                    '<td>' + response[index].tipo + '</td>' +
                                    '<td>' + response[index].motivo + '</td>' +
                                    '<td>' + response[index].fecha + '</td>' +
                                    '<td>' + response[index].horaIn + '</td>' +
                                    '<td>' + response[index].horaFin + '</td>' +
                                    '<td>' + response[index].estado + '</td>' +
                                    '<td>' + response[index].observ + '</td>' +
                              '</tr>';
                }

            
                $("#cTablaReporte").html(options);

                /* Script que permite a la tabla hacer busquedas dentro de ella
                    y ordenarla deacuerdo a lo que se presenta en ella. */

                    $('#TableReporte').dataTable();   
            },
            timeout: 4000
        }); 
    } //finaliza la función de carga de solicitudes
    
});

</script>
<!DOCTYPE html>

<html lang="en">
<body>

<div id = "notificaciones1"></div>

<!-- Seccion usada para mostrar la tabla de las solicitudes de permiso que estan pendientes de revisión en el sistema -->
<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Historial de Solicitudes </label>
        </div>
        <div class="panel-body">
            <div>
                   <div id= "noti1" class="alert alert-info" role="alert"><center>A continuación se presentan las solicitudes que los empleados de la facultad de ciencias jurídicas han realizado este mes</center></div>
            </div>
        
        <div class="panel-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <section class="content">
			<button class="btn btn-success pull-right" id="btnmuestra" onclick="toexcel('TableReporte','Solicitudes') ;">Exportar Excel <i class="fa fa-file-excel-o" aria-hidden="true"></i></button><br><br><br>
                            <div class="box-body table-responsive">
                                <table id= "TableReporte" border="0" class='table table-bordered table-hover' >
                                    <thead style="text-align:center;background-color:#386D95;">
                                        <tr>
                                            <th style = 'display:none';style="text-align:center;color:white;background-color:#386D95;">Codigo</th>
                                            <th style="text-align:center;color:white;background-color:#386D95;width: 250px;">Empleado</th>
                                            <?php 
                                                if($rol == 100){
                                                    echo "<th style='text-align:center;color:white;background-color:#386D95;width: 250px;'>Departamento</th>";
                                                }
                                            ?>
                                            <th style="text-align:center;color:white;background-color:#386D95;width: 100px;">Tipo</th>  
                                            <th style="text-align:center;color:white;background-color:#386D95;width: 70px;">Motivo</th>
                                            <th style="text-align:center;color:white;background-color:#386D95;width: 80px;">Fecha</th>
                                            <th style="text-align:center;color:white;background-color:#386D95;width: 80px;">Hora Inicio</th>
                                            <th style="text-align:center;color:white;background-color:#386D95;width: 60px;">Hora Fin</th>
                                            <th style="text-align:center;color:white;background-color:#386D95;width: 80px;">Estado</th>
                                            <th style="text-align:center;color:white;background-color:#386D95;">Observación</th>  
                                        </tr>
                                    </thead>
                                    <tbody id = "cTablaReporte">
                                        <!-- Contenido de la tabla generado atravez de la consulta a 
                                        la base de datos -->
                                       
                                </tbody>                                
				</table>       
                            </div>
                        </section>
                    </div>                
                </div>
            </div>       
        </div>
        </div>
    </div>
</div>

<script src="js/toExcel.js"></script>

<script>
   function toexcel(tabla,worksheet){
   //$('#'+tabla).dataTable().fnDestroy();
   tableToExcel(TableReporte,worksheet);
   //$('#'+tabla).dataTable();
  }
</script>
</body>
</html>