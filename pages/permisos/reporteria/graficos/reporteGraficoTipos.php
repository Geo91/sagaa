<?php 
 header("Content-Type: text/html;charset=utf-8");
$mkdir = "../../../../";
include($mkdir."conexion/config.inc.php");

session_start();
$nEmp='';
$q='';
$idusuario = $_SESSION['user_id'];
$rol_usr = $_SESSION['user_rol'];

$q = 'SELECT No_Empleado as Empleado FROM usuario WHERE id_Usuario = ' .$idusuario;
$resultado = mysql_query($q);
for($i = 0 ; ; $i++){
    $fila = mysql_fetch_array($resultado);
    if ($i == 0){
        $nEmp = $fila["Empleado"];
        break;
    }else{
        $mensaje="Error en la obtencion de datos";
        $codMensaje =0;
    }
}
  
if($rol_usr == 50){
	try{
	    $query = "SELECT tipodepermiso.tipo_permiso as TIPO, COUNT(permisos.id_Permisos) as NUMERO_SOLICITUDES 
	        from permisos inner join tipodepermiso on (permisos.id_tipo_permiso=tipodepermiso.id_tipo_permiso) 
	        where permisos.estado IN ('5','4') and fecha_solicitud between CAST(CONCAT(YEAR(NOW()), '-', MONTH(NOW()), '-01') as date) and CAST(LAST_DAY(NOW()) as date) group by TIPO";
	    $result = mysql_query($query);
	}catch(PDOExecption $e){
	    $mensaje="Error en la obtencion de datos";
	    $codMensaje =0;
	}	
}else{
	try{
	    $query = "SELECT tipodepermiso.tipo_permiso as TIPO, COUNT(permisos.id_Permisos) as NUMERO_SOLICITUDES 
	        from permisos inner join tipodepermiso on (permisos.id_tipo_permiso=tipodepermiso.id_tipo_permiso) 
	        where jefe_inmediato = " .$nEmp. " and permisos.estado IN ('5','4') and fecha_solicitud between CAST(CONCAT(YEAR(NOW()), '-', MONTH(NOW()), '-01') as date) and CAST(LAST_DAY(NOW()) as date) group by TIPO";
	    $result = mysql_query($query);
	}catch(PDOExecption $e){
	    $mensaje="Error en la obtencion de datos";
	    $codMensaje =0;
	}
}
?>


<script>
$(function () {
    $(document).ready(function () {

        // Build the chart
        $('#reportegraficoTipoSolicitudes').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: "Solicitudes por Tipo",
                colorByPoint: true,
                data: [
                <?php
                    $numItems = mysql_num_rows($result);
                    for($i = 0 ;$fila = mysql_fetch_array($result); $i++){
                        if ($i == 1){
                            $nombre = $fila["TIPO"];
                            $y = $fila["NUMERO_SOLICITUDES"];
                            echo "{name: '".$nombre."', y: ".$y.",
                               sliced: true,
                               selected: true }";
                        }else{
                            $nombre = $fila["TIPO"];
                            $y = $fila["NUMERO_SOLICITUDES"];
                            echo "{name: '".$nombre."', y: ".$y."}";
                        }
                        if($i != $numItems) {
                            echo ",";
                        }
                    }  
                ?>]
            }]
        });
    });
});

</script>
<div id ="reportegraficoTipoSolicitudes"></div>