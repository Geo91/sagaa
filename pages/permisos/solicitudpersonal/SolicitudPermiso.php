

<div  id="divRespuesta"></div>

<div class="col-lg-10">       
    <div class="panel panel-primary">
        <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span><strong> Solicitud de Permiso </strong></div>

        <div class="panel-body" align = "center">
            <div >
                <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden<br>En caso de usar mozilla firefox el formato de fecha debe ser 'año-mes-día' </center></div>
            </div>

            <div id = "formularioSolicitud">
                <?php require_once("formularioSolicitud.php"); ?>
            </div>
            
        </div>                                    
    </div> 
</div>

<!-- Modal para visualizar los recursos disponibles en el salón -->
<div class="modal fade" id="modalSolicitud" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div id="contenidoModal">
            </div>
        </div>
    </div>
</div>

<!--******************Script de interacción*****************-->
                       
