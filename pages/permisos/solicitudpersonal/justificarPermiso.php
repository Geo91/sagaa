<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    $vemp = $_POST['empleado'];
    $vnom = $_POST['nombre'];
    $vmot = $_POST['motivo'];
    $vedi = $_POST['edificio'];
    $vtip = $_POST['tipoPerm'];
    $vjef = $_POST['jefe'];
    $vcan = $_POST['cantidad'];
    $vfec = $_POST['fecha'];
    $vHI = $_POST['horaIn'];
    $vHF = $_POST['horaFin'];


?>

<div class="modal-header" style = "background-color:#0FA6C3">
  	<button type="button" class="close" data-dismiss="modal">&times;</button>
  	<h4 class="modal-title" style = "color:white">Justificación de Solicitud de Permiso <br> </h4>
</div>

<div class="modal-body">

    <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden</center>
    </div>
    <!--************************formulario*********************-->           
    <form class="form-horizontal" role="form" id="formSolicitudP" name="formSolicitudP">

		<div class="row form-group" hidden = "true">
		    <div class = "col-sm-2">
		        <input type = "text" id = "emp" value="<?php echo $vemp; ?>" hidden="true" disabled="true">
		        <input type = "text" id = "nomb" value="<?php echo $vnom; ?>" hidden="true" disabled="true">
		        <input type = "text" id = "mot" value="<?php echo $vmot; ?>" hidden="true" disabled="true">
		        <input type = "text" id = "edif" value="<?php echo $vedi; ?>" hidden="true" disabled="true">
		        <input type = "text" id = "tipo" value="<?php echo $vtip; ?>" hidden="true" disabled="true">
		        <input type = "text" id = "jefe" value="<?php echo $vjef; ?>" hidden="true" disabled="true">
		        <input type = "text" id = "cant" value="<?php echo $vcan; ?>" hidden="true" disabled="true">
		        <input type="date" id="fechaP" name="datepicker" value="<?php echo $vfec ?>" hidden="true" required>
		        <input type="time" id="horaI" name="horaIn" value="<?php echo $vHI ?>" hidden="true" required>
		        <input type="time" id="horaF" name="horaF" value="<?php echo $vHF ?>" hidden="true" required>
		    </div>
		</div>

        <div class="form-group col-sm-10">
            <label class="control-label"> Justificación </label>
            <input type="text" class="form-control" id="info" name="info" title = "Ingrese la justificación de su solicitud." required>

        </div>

        <div class="form-group col-sm-12">
            <label class="control-label"> Ejemplos: </label>
            <label class="control-label"> "Asistir a cita médica debido a que me encuentro con problemas de salud" </label>
            <label class="control-label"> "Asistir a una reunión en el colegio de mi hijo." </label>
            <label class="control-label"> "ir a realizar unos tramites en el INPREUNAH" </label>

        </div>

        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <hr>

        <div class="modal-footer">
            <button id="RegresarFormulario" class="btn btn-info"><span class="glyphicon glyphicon-circle-arrow-left"></span> Regresar </button>
            <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" > Enviar <span class="glyphicon glyphicon-circle-arrow-right"></span></button>    
        </div>    

    </form>
	    <!--************************formulario*********************-->
</div>

<!--******************Script de interacción*****************-->
<script type="text/javascript" src="pages/permisos/solicitudpersonal/ScriptSolicitud.js"></script>