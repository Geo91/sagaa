<?php
$mkdir = "../../../";
include($mkdir."conexion/config.inc.php");
session_start();
$pcNumEmpleado = $_POST['empleado'];
$pcCodMotivo = $_POST['motivo'];
$pcCodEdificio = $_POST['edificio'];
$pcCodTipoPermiso = $_POST['tipoPerm'];
$pcCodJefe = $_POST['jefe'];
$pcDias = $_POST['cantidad'];
$pcFecha = $_POST['fecha'];
$pcHoraIn=$_POST['horaIn'];
$pcHoraFin=$_POST['horaFin'];
$pcUsuario = $_SESSION['user_id'];

$consulta=$db->prepare("CALL SP_REGISTRAR_PERMISOS(?,?,?,?,?,?,?,?,?,?,@pcMensajeError)");
$consulta->bindParam(1, $pcNumEmpleado, PDO::PARAM_INT);
$consulta->bindParam(2, $pcCodMotivo, PDO::PARAM_INT);
$consulta->bindParam(3, $pcCodEdificio, PDO::PARAM_INT);
$consulta->bindParam(4, $pcCodTipoPermiso, PDO::PARAM_INT);
$consulta->bindParam(5, $pcCodJefe, PDO::PARAM_INT);
$consulta->bindParam(6, $pcDias, PDO::PARAM_INT);
$consulta->bindParam(7, $pcFecha, PDO::PARAM_STR);
$consulta->bindParam(8, $pcHoraIn, PDO::PARAM_STR);
$consulta->bindParam(9, $pcHoraFin, PDO::PARAM_STR);
$consulta->bindParam(10, $pcUsuario, PDO::PARAM_INT);

$resultado=$consulta->execute();

$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);

$mensaje = $output['@pcMensajeError'];

if ($mensaje == NULL)
{
    echo '<div id="resultado" class="alert alert-success">
    ' . utf8_encode('Su solicitud ha sido enviada a su jefe inmediato para su revisi�n'). '</div>';
}
else
{
    echo '<div id="resultado" class="alert alert-danger">
    ' . $mensaje . '</div>';
}

//include 'nuevaSolicitud.php';

?>

<script type="text/javascript">
$(document).ready(function() {
    setTimeout(function() {
        $(" ").fadeOut(1500);
    },3000);
	
});
</script>

