<?php 
$maindir = "../../../";
include($maindir."conexion/config.inc.php");
?>

<script >   
    
    //Valida que se haya elegido una fecha y que el campo no esté vacío
    function validaFecha()
    {
        fecha=$("#fecha").val();
        if(fecha.length>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    //Valida que ningún combobox sea null
    function validaCombobox()
    {
        if($("#motivo").val()==='NULL')
        {
            alert("Debes seleccionar un motivo");
            return false;
        }
        else
        {            
            if($("#edificio").val()==='NULL')
            {
                alert("Debes seleccionar el edificio donde se registra");
                return false;
            }
            else
            {
                if($("#tipoPermiso").val()==='NULL')
                {
                    alert("Debes seleccionar un tipo de permiso");
                    return false;
                }
                else
                {
                    if($("#jefe").val()==='NULL')
                    {
                        alert("Debes seleccionar a su jefe inmediato");
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
        }
    }
    
    function obtenerMotivos()
    {                  
        id =$("#identidad").val();
            
            var datos = 
                {
                    id:id
                }; //Array 
            
            $.ajax({
                async: true,
                type: "POST",
                data:datos,
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                url: "pages/permisos/motivo/cargarMotivos.php",
                //beforeSend: inicioVer,
                success: function(response){
                                               
                    var arr = JSON.parse(response);
                    
                    var options = '';
                    var val='NULL';
                    var def='Seleccione una opción';
                    options += '<option value="' + val + '">' +
                                    def+ '</option>';
                            
                    for (var index = 0; index < arr.length; index++) 
                    {
                        var codMotivo= arr[index].codMotivo;
                        var descMotivo = arr[index].descMotivo;
                        
                        options += '<option value="' + codMotivo + '">' +
                                    descMotivo + '</option>';
                    }
                    
                    $("#motivo").html(options);
                },
                timeout: 4000,
        });
    }

    function obtenerEdificios()
    {                  
        id =$("#identidad").val();
            
            var datos = 
                {
                    id:id
                }; //Array 
            
            $.ajax({
                async: true,
                type: "POST",
                data:datos,
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                url: "pages/permisos/edificio/cargaEdificios.php",
                //beforeSend: inicioVer,
                success: function(response){
                                               
                    var arr = JSON.parse(response);
                    
                    var options = '';
                    var val='NULL';
                    var def='Seleccione una opción';
                    options += '<option value="' + val + '">' +
                                    def+ '</option>';
                            
                    for (var index = 0; index < arr.length; index++) 
                    {
                        var codEdificio = arr[index].codEdificio;
                        var nombreEdificio = arr[index].nombreEdificio;
                        
                        options += '<option value="' + codEdificio + '">' +
                                    nombreEdificio + '</option>';
                    }
                    $("#edificio").html(options);                                                        
                },
                timeout: 4000,
            });
    }

    $(document).on("change","#motivo",function () {
        id = $("#motivo").val();            
        obtenerTipos(id);
        return false;
    });

    function obtenerTipos(motivoID)
    {                  
        var datos = 
            {
                id:motivoID
            }; //Array 
            
        $.ajax({
            async: true,
            type: "POST",
            data:datos,
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            url: "pages/permisos/solicitudpersonal/obtenerTipoPermiso.php",
            success: function(response){
                                           
                var arr = JSON.parse(response);
                
                var options = '';
                var val='NULL';
                var def='Seleccione una opción';
                options += '<option value="' + val + '">' +
                                def+ '</option>';
                        
                for (var index = 0; index < arr.length; index++) 
                {
                    var idPermiso = arr[index].idpermiso;
                    var tipoPermiso = arr[index].tipopermiso;
                    
                    options += '<option value="' + idPermiso + '">' +
                                tipoPermiso + '</option>';
                }
                
                $("#tipoPermiso").html(options);                                                        
                
            },
            timeout: 4000,    
        });
    }

    function obtenerJefes()
    {                  
        id =$("#identidad").val();
            
            var datos = 
                { 
                    id:id
                }; //Array 
            
            $.ajax({
                async: true,
                type: "POST",
                data:datos,
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                url: "pages/permisos/solicitudpersonal/ObtenerJefes.php",
                success: function(response){
                                               
                    var arr = JSON.parse(response);
                    
                    var options = '';
                    var val='NULL';
                    var def='Seleccione una opción';
                    options += '<option value="' + val + '">' +
                                    def+ '</option>';
                            
                    for (var index = 0; index < arr.length; index++) 
                    {
                        var no_empleado = arr[index].no_empleado;
                        var nombreJefe = arr[index].nombreJefe;
                        
                        options += '<option value="' + no_empleado + '">' +
                                    nombreJefe + '</option>';
                    }
                    
                    $("#jefe").html(options);                                                        
                    
                },
                timeout: 4000,  
            });
    }

    function obtenerDatosEmpleado() {    
    //Esta función recupera la información del empleado que se encuentra logueado de manera que esté no pueda ingresar otra información que no sea la propia.
        $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        url: "pages/permisos/solicitudpersonal/ObtenerDatosEmpleado.php",
        
            success: function(response){
                
                var arr = JSON.parse(response);
                if(arr.length > 0)
                {                                                                
                    $("#empleado").val(arr[0].num_empleado);
                    $("#nombre").val(arr[0].nombreEmpleado);
                }
                else
                {                             
                    alert("Hubo un problema, por favor intentelo de nuevo más tarde");
                }
                
            },
            timeout: 4000,
        });
        return false;
    };

    //Esta función transfiera la información del formulario al archivo donde el procedimiento almacenado se encarga de procesarla hacia la base de datos.
    $(document).ready(function () {
        obtenerDatosEmpleado();
        obtenerMotivos();
        obtenerEdificios();
        obtenerJefes();

                $("#formSolicitud").submit(function (e) {
                    e.preventDefault();
                    if(validaCombobox()===true) //Se validan que se haya seleccionado una de las opciones de los combobox
                    {
                        if(validaFecha()===false) //Se valida que se haya ingresado una fecha
                        {
                            alert('Debes de ingresar una fecha');
                            }
                            else
                            {
                                //Se recopila la información que se enviara a la base de datos
                                data2 = {empleado: $("#empleado").val(),
                                nombre: $("#nombre").val(),
                                motivo: $("#motivo").val(),
                                edificio: $("#edificio").val(),
                                tipoPerm: $("#tipoPermiso").val(),
                                jefe: $("#jefe").val(),
                                cantidad: $("#cantidad").val(),
                                fecha: $("#fecha").val(),
                                horaIn: $("#horaIn").val(),
                                horaFin: $("#horaFi").val() 
                                };

                                $.ajax({
                                    async: true,
                                    type: "POST",
                                    dataType: "html",
                                    contentType: "application/x-www-form-urlencoded",
                                    url: "pages/permisos/solicitudpersonal/registrarSolicitudPermiso.php",
                                    beforeSend: inicioEnvio,
                                    success: llegadaGuardar,
                                    timeout: 4000,
                                });
                            } 
                    }                                                           
                    return false;
                });         
    }); //Termina la función de guardado de solicitudes

    function llegadaGuardar()
    {
        $("#contenedor2").load('pages/permisos/solicitudpersonal/registrarSolicitudPermiso.php', data2);
        limpiarControles();
        obtenerDatosEmpleado();
        obtenerMotivos();
        obtenerEdificios();
        obtenerTipos();
        obtenerJefes();
    }

    function inicioEnvio()
    {
        var x = $("#contenedor2");
        x.html('Cargando...');
    }


    function limpiarControles()
    {           
        $('#empleado').val(' ');
        $('#nombre').val(' ');
        $('#cantidad').val(' ');
        $('#fecha').val(' ');
        $('#horaIn').val(' ');
        $('#horaFi').val(' ');
    }      
             
</script>
<div  id="contenedor2"></div>  
<div class="col-lg-10">       
<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span><strong> Solicitud de Permiso </strong></div>
    <br>
    <br>
    <div class="panel-body" align = "center">
        <div class="row">
            <div class="col-lg-12">
                <form class="form-horizontal" role="form" id="formSolicitud" name="formSolicitud">

                    <div class="row form-group" >                    
                        <label class="col-sm-3 control-label"id="2">N° Empleado</label>                       
                        <div class="col-sm-2">                            
                            <input type="text" class="form-control" id="empleado" name="empleado" required disabled="">                           
                        </div>                                                                                                                         
                    </div>
            
                    <div class="row form-group">
                        <label class="col-sm-3 control-label">Nombre</label>
                        <div class="col-sm-4">                            
                            <input type="text" class="form-control" id="nombre" name="nombre" required disabled="">                                
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class = "col-sm-3 control-label">Solicito permiso por motivo de:</label>
                        <div class = "col-sm-4">
                            <select class = "form-control" id="motivo" name="motivo" required>
                                <option value = "NULL">Seleccione una opción</option>
                            </select>
                        </div>
                    </div>

                    <div class="row form-group">                                                                                                                     
                        <label class=" col-sm-3 control-label">Edificio donde se registra</label>
                        <div class="col-sm-4">                            
                            <select class="form-control" id="edificio" name="edificio" required>
                                <option value="NULL">Seleccione una opción</option>
                            </select>
                        </div>                        
                    </div>
            
                    <div class="row form-group">
                        <label class=" col-sm-3 control-label">Tipo de Permiso</label>
                        <div class="col-sm-4">     
                            <select class="form-control" id="tipoPermiso" name="tipoPermiso" required>
                                <option value="NULL">Seleccione una opción</option>
                            </select>                         
                        </div>
                    </div>            
                     
                    <div class="row form-group">
                        <label class=" col-sm-3 control-label">Jefe Inmediato</label>
                        <div class="col-sm-4">                            
                            <select class="form-control" id="jefe" name="jefe">
                                <option value="NULL">Seleccione una opción</option>
                            </select>
                        </div>
                    </div>
                                  
                    <div class="row form-group">                            
                        <label class="col-sm-3 control-label">Fecha</label>
                        <div class="col-sm-2">
                            <input type="date" id="fecha" name="datepicker" required><span class="">
                        </div>
                    </div>

                    <div class="row form-group">                            
                        <label class="control-label col-sm-3">Días</label>
                        <div class="col-sm-1">
                            <input type="number" id="cantidad" name="cantidad" max="30" min="0" required><span class="">
                        </div>
                    </div>

                    <div class="row form-group">                            
                        <label class="control-label col-sm-3">Hora Inicio</label>
                        <div class="col-sm-1">
                            <input type="time" id="horaIn" name="horaIn" required><span class="">
                        </div>
                    </div>

                    <div class="row form-group">                            
                        <label class="control-label col-sm-3">Hora Fin</label>
                        <div class="col-sm-1">
                            <input type="time" id="horaFi" name="horaFi" required><span class="">
                        </div>
                    </div>
                    
                    <div class="row">
                        <label class="control-label col-sm-3"></label>
                        <div class="col-sm-6">
                            <p aling ="right">
                                <button class="btn btn-primary btn-primary col-sm-offset-10"><span class=" glyphicon glyphicon-floppy-disk"></span> Guardar </button> 
                            </p>          
                        </div>                            
                    </div>                                                            
                </form>
            </div>
        </div>
    </div>                                    
</div> 
</div>                       
