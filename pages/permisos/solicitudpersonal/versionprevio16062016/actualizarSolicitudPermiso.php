<?php
$mkdir = "../../../";
include($mkdir."conexion/config.inc.php");

$pcCodEmpleado = $_POST['empleado'];
$pcCodPermiso = $_POST['permiso'];
$pcCodMotivo = $_POST['motivo'];
$pcCodEdificio = $_POST['edificio'];
$pcCodTipoPermiso = $_POST['tipoPerm'];
$pcCodJefe = $_POST['jefe'];
$pcDias = $_POST['cantidad'];
$pcFecha = $_POST['fecha'];
$pcHoraIn=$_POST['horaIn'];
$pcHoraFin=$_POST['horaFin'];

$consulta=$db->prepare("CALL SP_MODIFICAR_PERMISO(?,?,?,?,?,?,?,?,?,?,@pcMensajeError)");
$consulta->bindParam(1, $pcCodEmpleado, PDO::PARAM_INT);
$consulta->bindParam(2, $pcCodPermiso, PDO::PARAM_INT);
$consulta->bindParam(3, $pcCodMotivo, PDO::PARAM_INT);
$consulta->bindParam(4, $pcCodEdificio, PDO::PARAM_INT);
$consulta->bindParam(5, $pcCodTipoPermiso, PDO::PARAM_INT);
$consulta->bindParam(6, $pcCodJefe, PDO::PARAM_INT);
$consulta->bindParam(7, $pcDias, PDO::PARAM_INT);
$consulta->bindParam(8, $pcFecha, PDO::PARAM_STR);
$consulta->bindParam(9, $pcHoraIn, PDO::PARAM_STR);
$consulta->bindParam(10, $pcHoraFin, PDO::PARAM_STR);

$resultado=$consulta->execute();

$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);

$mensaje = $output['@pcMensajeError'];

if ($mensaje == NULL)
{
    echo '<div id="resultado" class="alert alert-success">
    ' . utf8_encode('Su solicitud ha sido actualizada'). '</div>';
}
else
{
    echo '<div id="resultado" class="alert alert-danger">
    ' . $mensaje . '</div>';
}

?>

<script type="text/javascript">
$(document).ready(function() {
    setTimeout(function() {
        $(" ").fadeOut(1500);
    },3000);
	
});
</script>

