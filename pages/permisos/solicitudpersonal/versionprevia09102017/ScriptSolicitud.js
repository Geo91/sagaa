//Valida que se haya elegido una fecha y que el campo no esté vacío
function obtenerTipos(motivoID){                  
    var datos = 
        {
            id:motivoID
        }; //Array 
        
    $.ajax({
        async: true,
        type: "POST",
        data:datos,
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        url: "pages/permisos/solicitudpersonal/obtenerTipoPermiso.php",
        success: function(response){
                                       
            var arr = JSON.parse(response);
            
            var options = '';
            var val='NULL';
            var def='Seleccione una opción';
            options += '<option value="' + val + '">' +
                            def+ '</option>';
                    
            for (var index = 0; index < arr.length; index++) 
            {
                var idPermiso = arr[index].idpermiso;
                var tipoPermiso = arr[index].tipopermiso;
                
                options += '<option value="' + idPermiso + '">' +
                            tipoPermiso + '</option>';
            }
            
            $("#tipoPermiso").html(options);                                                        
            
        },
        timeout: 6000,    
    });
}

function validaFecha(){
    fecha=$("#fecha").val();
    if(fecha.length>0)
    {
        return true;
    }
    else
    {
        return false;
    }
}


//Valida que ningún combobox sea null
function validaCombobox(){
    if($("#motivo").val()==='NULL')
    {
        alert("Debes seleccionar un motivo");
        return false;
    }
    else
    {            
        if($("#edificio").val()==='NULL')
        {
            alert("Debes seleccionar el edificio donde se registra");
            return false;
        }
        else
        {
            if($("#tipoPermiso").val()==='NULL')
            {
                alert("Debes seleccionar un tipo de permiso");
                return false;
            }
            else
            {
                if($("#jefe").val()==='NULL')
                {
                    alert("Debes seleccionar a su jefe inmediato");
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
    }
}

$(document).on("change","#motivo",function () {
    id = $("#motivo").val();            
    obtenerTipos(id);
    return false;
});

$("#formSolicitud").submit(function (e) {
    e.preventDefault();
    if(validaCombobox()===true) //Se validan que se haya seleccionado una de las opciones de los combobox
    {
        if(validaFecha()===false) //Se valida que se haya ingresado una fecha
        {
            alert('Debe ingresarse una fecha');
            }
            else
            {
                //Se recopila la información que se enviara a la base de datos
                data2 = {empleado: $("#empleado").val(),
                nombre: $("#nombre").val(),
                motivo: $("#motivo").val(),
                edificio: $("#edificio").val(),
                tipoPerm: $("#tipoPermiso").val(),
                jefe: $("#jefe").val(),
                cantidad: $("#cantidad").val(),
                fecha: $("#fecha").val(),
                horaIn: $("#horaIn").val(),
                horaFin: $("#horaFi").val() 
                };

                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: llegadaGuardar,
                    timeout: 6000,
                    error: problemas
                });
            }
    }                                                           
    return false;
});

function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function llegadaGuardar(){
    $("#divRespuesta").load('pages/permisos/solicitudpersonal/registrarSolicitudPermiso.php', data2);
    $("#formSolicitud").trigger("reset");
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor. Favor contactar al administrador del sistema.');
}


