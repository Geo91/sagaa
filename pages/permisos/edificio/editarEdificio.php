<?php
	$maindir = "../../../";
	require_once($maindir."Datos/funciones.php");
	require_once($maindir."conexion/config.inc.php");

	if(isset($_POST['Codigo']) && isset($_POST['Nombre'])){
		$Codigo = $_POST['Codigo'];
		$Nombre = $_POST['Nombre'];

		try{
			$sql = "CALL SP_ACTUALIZAR_EDIFICIOS(?,?,@mensajeError)";
			$query1 = $db->prepare($sql);
			$query1->bindParam(1, $Nombre, PDO::PARAM_STR);
			$query1->bindParam(2, $Codigo, PDO::PARAM_STR);
			$query1->execute();

			$output = $db->query("select @mensajeError")->fetch(PDO::FETCH_ASSOC);
			$mensaje = $output['@mensajeError'];
			if(is_null($mensaje)){
				echo mensajes("Se ha modificiado el registro seleccionado", "verde");
			}else{
				echo mensajes("Error!  ". $mensaje, "rojo");
			}
		}catch(PDOException $e){
			echo mensajes("Ha ocurrido un error del servidor...", "rojo");
		}
	}else{
		echo mensajes("Hubo un error al intentar modificar el registro. Intentelo de nuevo o contacte al administrador del sistema", "rojo");
	}
?>

<script type = "text/javascript">
	$("#EdificiosTabla").load('pages/permisos/edificio/datosEdificios.php');
</script>