//cargar la tabla desde un principio
    $("#EdificiosTabla").load('pages/permisos/edificio/datosEdificios.php');

$(document).ready(function(){   

//para cuando se da submit al formulario formAsignatura
    $("#formEdificios").submit(function(e) {
        e.preventDefault();
        data={
            Nombre:$('#NombreEdificio').val()                
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: agregarEdificio,
            timeout: 6000,
            error: problemas
        }); 
        return false;
    });


//para cuando se da submit al formulario formAsignatura
    $("#formEditarEdificio").submit(function(e) {
        e.preventDefault();
        data={
            Codigo:$('#modalCodigo').val(),
            Nombre:$('#modalNombre').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: editarEdificio,
            timeout: 6000,
            error: problemas
        }); 
        $("#modalEditar").modal('hide');
        return false;
    });

});

//para editar una asignatura
$(document).on("click", ".editarEdificio", function () {
    codigo = $(this).parents("tr").find("td").eq(0).html();
    Nombre = $(this).parents("tr").find("td").eq(1).html();

    $("#modalCodigo").val(codigo);
    $("#modalNombre").val(Nombre);

    $("#modalEditar").modal('show');
});


function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function agregarEdificio(){
    $("#divRespuesta").load('pages/permisos/edificio/insertarEdificios.php',data);
    $('#formEdificios').trigger("reset");
}

function editarEdificio(){
    $("#divRespuestaEditar").load('pages/permisos/edificio/editarEdificio.php',data);
    $("#EdificiosTabla").load('pages/permisos/edificio/datosEdificios.php');
}

function eliminarEdificio(){
    $("#divRespuestaEditar").load('pages/permisos/edificio/eliminarEdificios.php',data);
}