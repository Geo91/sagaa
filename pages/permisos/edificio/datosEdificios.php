<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query1 = "SELECT * FROM `edificios`";
?>

<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaEdificios" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style ="display:none">Codigo</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th> 
                    <th style="text-align:center;background-color:#386D95;color:white;">Editar</th>  
                    <th style="text-align:center;background-color:#386D95;color:white;">Eliminar</th>                  
                </tr>
            </thead>
            <tbody>
                <?php
                    $result =$db->prepare($query1);
                    $result->execute();
                    while ($fila = $result->fetch()) {
                        ?>
                        <tr data-id='<?php echo $fila["Edificio_ID"]; ?>'>
                            <td style = "display:none"><?php echo $fila["Edificio_ID"]; ?></td>
                            <td><?php echo $fila["descripcion"]; ?></td>
                            <td>
                                <center>
                                    <button type="button" class="editarEdificio btn btn-info glyphicon glyphicon-edit"  title="Editar Edificio">
                                </center>
                            </td>       
                            <td>
                                <center>
                                    <button type="button" class="eliminarEdificio<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-remove"  title="Eliminar Edificio">
                                </center>
                            </td>     
                        </tr>
                <?php } ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div>

<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#tablaEdificios')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaEdificios').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
    $(document).on("click", ".eliminarEdificio<?php echo $auto_increment; ?>", function () {
        if (confirm("¿Está seguro de que desea eliminar este registro?")){
            codigo = $(this).parents("tr").data("id");
            data={
                Codigo:codigo
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                success: eliminarEdificio,
                timeout: 6000,
                error: problemas
            }); 
            return false;
        }
    });

</script>