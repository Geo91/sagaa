<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    include($maindir."conexion/config.inc.php");

?>


<div class="row">
    <div class="col-lg-10">
        <h3 class="page-header panel-primary"><center><strong>Gestión de Informes de Inasistencias</strong></center></h3>
    </div>
</div> 

<div class ="col-lg-10" id="divRespuesta"></div>

<div class="col-lg-10">       
    <div id = "formulario">
        <?php require_once("formInforme.php"); ?>
	</div>
</div>

<div class="col-lg-10">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Informes de Inasistencia Año Actual</label>
        </div>
        <div class="panel-body">

            <div id = "TablaInforme">
                <?php include("datosInformes.php"); ?>
            </div>   
                
        </div>
    </div>
</div>

<!-- Modal para revisar las solicitudes de reservación -->
<div class="modal fade" id="modalDocumento" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div id="contenidoModal">
            </div>
        </div>
    </div>
</div>

<!-- Modal para revisar las solicitudes de reservación -->
<div class="modal fade" id="modalVisualizar" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="contenidoModalV">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="pages/permisos/justificaciones/ScriptInforme.js"></script>