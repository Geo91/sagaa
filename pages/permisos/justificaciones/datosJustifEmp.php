<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");
    
    if(isset($_POST['idI'])){
    	$codI = $_POST['idI'];
    	$cEmp = $_POST['nemp'];
	    $cNombre = $_POST['name'];

	function dia($fecha){
        switch (date('l', strtotime(($fecha)))) {
            case 'Monday':
                $dia = 'Lunes';
                break;
            case 'Tuesday':
                $dia = 'Martes';
                break;
            case 'Wednesday':
                $dia = 'Miércoles';
                break;
            case 'Thursday':
                $dia = 'Jueves';
                break;
            case 'Friday':
                $dia = 'Viernes';
                break;
            case 'Saturday':
                $dia = 'Sábado';
                break;
            case 'Sunday':
                $dia = 'Domingo';
                break;
        }
        return $dia;
    }

?>

<div class="modal-header" style = "background-color:#0FA6C3">
  	<button type="button" class="close" data-dismiss="modal">&times;</button>
  	<h4 class="modal-title" style = "color:white">Justificaciones: <?php echo $cNombre; ?> </h4>
</div>

<div class="modal-body">

	<div id = "divRespuestaM"></div>

    <div class="row">
        <div class="col-lg-12">

	    <!--************************formulario*********************-->           
	        <form class="form-horizontal" role="form" id="formRevisarJustificacion" name="formRevisarJustificacion">

	        	<div class="row form-group" style="display:none">
	                <label class="col-sm-2 control-label"> Empleado </label>
	                <div class="col-sm-2">
                        <input type="text" class="form-control" id="empleado" name="empleado" value = "<?php echo $cEmp; ?>">
                        <input type="text" class="form-control" id="mname" name="mname" value = "<?php echo $cNombre; ?>">
                        <input type="text" class="form-control" id="vinforme" name="informe" value = "<?php echo $codI; ?>">
                    </div>
                </div>

				<div class="box">
				    <div class="box-body table-responsive">
				        <table id= "TablaDatosJE" border="0" class='table table-bordered table-striped'>
	                        <thead>
	                            <tr>
	                                <th style = "text-align:center;background-color:#386D95;color:white;display:none">ID</th>
	                                <th style = "text-align:center;background-color:#386D95;color:white;" width="10">Día</th>
	                                <th style="text-align:center;background-color:#386D95;color:white;">Fecha</th>
	                                <th style="text-align:center;background-color:#386D95;color:white;">Min. E</th>
	                                <th style = "text-align:center;background-color:#386D95;color:white;">Min. S</th>
	                                <th style="text-align:center;background-color:#386D95;color:white;">Fuera H. Ent</th>
	                                <th style="text-align:center;background-color:#386D95;color:white;">Fuera H. Sal</th>
	                                <th style = "text-align:center;background-color:#386D95;color:white;">Justificación</th>
	                                <th style="text-align:center;background-color:#386D95;color:white;">OP</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            <!-- Contenido de la tabla generado atravez de la consulta a 
	                                la base de datos -->
	                                <?php
	                                    try{
	                                        $proc = "CALL SP_GI_DATOS_JUSTIFICACIONES_EMP(?,?)";
	                                        $queryproc = $db->prepare($proc);
	                                        $queryproc->bindParam(1,$cEmp,PDO::PARAM_STR);
	                                        $queryproc->bindParam(2,$codI,PDO::PARAM_INT);

	                                        $queryproc->execute();
	                                        while ($row = $queryproc->fetch()) {
	                                ?>
	                                        <tr data-id='<?php echo $row["id_justificacion"]; ?>' data-fecha='<?php echo $row["fecha"]; ?>' data-mine='<?php echo $row["min_ent"];?>' data-mins='<?php echo $row["min_sal"];?>' data-fhe='<?php echo $row["fuera_hent"];?>' data-fhs='<?php echo $row["fuera_hsal"];?>' data-justificacion='<?php echo $row["justificacion"];?>'>
	                                            <td style="display:none"><?php echo $row["id_justificacion"]; ?></td>
	                                            <td><?php echo dia($row["fecha"]); ?></td>
	                                            <td><?php echo $row["fecha"]; ?></td>
	                                            <td><?php echo $row["min_ent"]; ?></td>
	                                            <td><?php echo $row["min_sal"]; ?></td>
	                                            <td><?php echo $row["fuera_hent"]; ?></td>
	                                            <td><?php echo $row["fuera_hsal"]; ?></td>
	                                            <td><?php echo $row["justificacion"]; ?></td>
	                                            <td>
	                                                <center>
	                                                <div class = "btn-group-vertical btn-group-md">
	                                                    <button type="button" class="aprobarJustificacion<?php echo $auto_increment; ?> btn btn-success" title="Aprobar la justificación."><span class="glyphicon glyphicon-thumbs-up"></span></button>
	                                                    <button type="button" class="denegarJustificacion<?php echo $auto_increment; ?> btn btn-danger" title="Denegar la justificación."><span class="glyphicon glyphicon-thumbs-down"></span></button>
	                                                </div>
	                                                </center>
	                                            </td>    
	                                        </tr>
	                                <?php
	                                        } //cierre del ciclo while para llenar la tabla de datos
	                                    }catch(PDOException $e){
	                                        echo "Error: ".$e;
	                                    }
	                                ?>
	                        </tbody>
	                    </table>
				    </div><!-- /.box-body -->
				</div>

	            <div class="modal-footer">
	                <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
	                <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" > Aprobar Todo <span class="glyphicon glyphicon-check"></span></button>                                  
	            </div>  

	        </form>
	    <!--************************formulario*********************-->
	    </div>
	</div>
</div>

<?php
	}else{
	echo mensajes("Se ha producido un error al obtener la información, contacte al administrador del sistema", "rojo");	
}
?>

<script type="text/javascript">   
//opciones para buscador en la tabla
    $('#TablaDatosJE')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#TablaDatosJE').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {

        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });


    $(document).on("click", ".aprobarJustificacion<?php echo $auto_increment; ?>", function () {

        data = {
            idJ: $(this).parents("tr").data("id"),
            op: 1,
            vNE: $("#empleado").val(),
        	vInf: $("#vinforme").val(),
        	vName: $("#mname").val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: justificacionRevisada,
            timeout: 12000,
            error: problemas
        }); 
        return false; 
    });

    $(document).on("click", ".denegarJustificacion<?php echo $auto_increment; ?>", function () {

        data = {
            idJ: $(this).parents("tr").data("id"),
            op: 0,
            vNE: $("#empleado").val(),
    		vInf: $("#vinforme").val(),
    		vName: $("#mname").val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: justificacionRevisada,
            timeout: 12000,
            error: problemas
        }); 
        return false; 
    
    });

</script>


<script type="text/javascript" src="pages/permisos/justificaciones/ScriptRevisionJ.js"></script>