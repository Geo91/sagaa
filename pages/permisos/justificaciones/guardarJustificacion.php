<?php
	$maindir = "../../../";
	require_once($maindir."conexion/config.inc.php");
	require_once($maindir."Datos/funciones.php");

	session_start();
	$idUsuario = $_SESSION['user_id'];
	$pF = $_POST['vF'];
	$pME = $_POST['vME'];
	$pFHE = $_POST['vFHE'];
	$pMS = $_POST['vMS'];
	$pFHS = $_POST['vFHS'];
	$pJ = $_POST['vJ'];
	$pD = $_POST['vD'];

	try{
		$consulta=$db->prepare("CALL SP_GI_GUARDAR_JUSTIFICACIONES(?,?,?,?,?,?,?,?,@pcMensajeError)");
		$consulta->bindParam(1, $pF, PDO::PARAM_INT);
		$consulta->bindParam(2, $pME, PDO::PARAM_INT);
		$consulta->bindParam(3, $pFHE, PDO::PARAM_INT);
		$consulta->bindParam(4, $pMS, PDO::PARAM_INT);
		$consulta->bindParam(5, $pFHS, PDO::PARAM_INT);
		$consulta->bindParam(6, $pJ, PDO::PARAM_STR);
		$consulta->bindParam(7, $pD, PDO::PARAM_STR);
		$consulta->bindParam(8, $idUsuario, PDO::PARAM_INT);

		$resultado=$consulta->execute();
		$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
		$mensaje = $output['@pcMensajeError'];

		if ($mensaje == NULL){
		    echo mensajes('La justificación fue ingresada exitosamente','verde');
		}else{
			echo mensajes("Error! ". $mensaje, "rojo");
		}
	}catch(PDOExecption $e){
        echo mensajes("Error: ".$e, "rojo");
    }

    $consulta->closeCursor();

?>

<script type="text/javascript">
    $("#TablaJustificaciones").load('pages/permisos/justificaciones/datosJustificaciones.php');
    $('#formJustificaciones').trigger("reset");
</script>