<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir."conexion/config.inc.php");

    $vInf = $_POST['idI'];

?>

<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title" style="color:white">Exportar Oficio de Justificaciones y No Justificaciones</h4>
</div>

<div class="modal-body">

	<div>
	    <div id= "noti1" class="alert alert-info" role="alert"><center>Por favor ingrese el número de oficio con el que se generará el oficio de Justificaciones y No Justificaciones.</center></div>
	</div>

	<form class="form-horizontal" role="form" id="formOficio" name="formOficio">
		<div class="form-group col-sm-12" style="display:none">
		    <label>Código Informe</label>
				<input id="idInforme" value = "<?php echo $vInf; ?>" class="form-control" required disabled>
		</div>

		<div class="form-group col-sm-12">
		    <label>Número de Oficio</label>
		    <input id="numO" placeholder = "Oficio FCJ-X-XXX/XXXX" class="form-control">
		</div>

<br>
<br>

		<div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal"> Volver</button>
		    <button type = "submit"  class="btn btn-primary"> Continuar</button>
		</div>
	</form>
</div>

<script type="text/javascript" src="pages/permisos/justificaciones/ScriptInforme.js"></script>