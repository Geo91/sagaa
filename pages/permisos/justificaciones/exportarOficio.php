<?php  
    $maindir = "../../../";
    require_once($maindir."conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    if(isset($_POST['cdI']) && isset($_POST['nO'])) {
        session_start();
        $idUsuario = $_SESSION['user_id'];
        $id = $_POST['cdI'];
        $nFolio = $_POST['nO'];

        $correlativo = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
            
        try{
            $sql = "CALL SP_GI_FORMATO_JUSTIFICACIONES(?,?,?,@mensajeError)";
            $query1 = $db->prepare($sql);
            $query1->bindParam(1,$id,PDO::PARAM_INT);
            $query1->bindParam(2,$idUsuario,PDO::PARAM_INT);
            $query1->bindParam(3,$correlativo,PDO::PARAM_STR);

            $query1->execute();
            $output = $db->query("select @mensajeError")->fetch(PDO::FETCH_ASSOC);
            $mensaje = $output['@mensajeError'];

            if ($mensaje == NULL){
                echo mensajes('El oficio de justificaciones y no justificaciones ha sido exportado exitósamente.', 'verde');
                echo "<script type='text/javascript'>
                        var id = '$id';
                        var nF = '$nFolio';
                        window.open('pages/permisos/justificaciones/formatojustificaciones_pdf.php?id='+id+'&nF='+nF);
                    </script>";
            }else{
                echo mensajes('Error!'.$mensaje, 'rojo');
            }
        }catch(PDOexception $e){
            echo mensajes('Error!'. $e, 'rojo');
        }
    }else{
        echo mensajes("Hubo un error al intentar exportar el oficio. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
    }

?>

<script type="text/javascript">
    $("#TablaInforme").load('pages/permisos/justificaciones/datosInformes.php');
</script>