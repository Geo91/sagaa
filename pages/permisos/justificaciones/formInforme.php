<?php
    $maindir = "../../../";

    require_once("../ChekAutoIncrement.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir . "conexion/config.inc.php");
?>

<div  id="divRespuesta"></div>

<div class="row">
    <div class = "col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Agregar nuevo informe de inasistencia</div>

            <div class="panel-body">
                <div >
                    <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden</center></div>
                </div>
                <div class="row">
                    <div class="col-lg-10">
                    <!--************************formulario*********************-->
                        <form class="form-horizontal" role="form" id="formInformes" name="formInformes">

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><h5><strong> Dependencia </strong></h5></label>
                                <div class="col-sm-5">
                                    <select name="depend" class="form-control" id="dependencia">
                                    <?php
                                        $consulta = "SELECT * FROM `dependencia`";
                                        $result = $db->prepare($consulta);
                                        $result->execute();
                                        while ($fila = $result->fetch()) {

                                            $codigo = $fila['dependencia_id'];

                                            echo "<option value = '" . $codigo . "'>";

                                            echo ($fila["descripcion"]);

                                            echo "</option>";
                                        }
                                    ?>                                       
                                    </select>
                                </div>
                                <br>
                            </div>

                            <div class="form-group">
                                <label class=" col-sm-3 control-label" >Año</label>
                                <div class="col-sm-2">     
                                    <input type="number" class="form-control" id="Anio" name="Año" min="2018" max="2100" required
                                    value="2018">      
                                </div> 
                            </div>

                            <div class="row form-group" >                    
                                <label class=" col-sm-3 control-label"> Mes</label>                       
                                <div class="col-sm-5">
                                    <select class="form-control" id="mes_informe" name="mes_informe" title="mes correspondiente al informe de inasistencias que se está creando" required>
                                        <option value = 'Enero'>Enero</option>
                                        <option value = 'Febrero'>Febrero</option>
                                        <option value = 'Marzo'>Marzo</option>
                                        <option value = 'Abrik'>Abrik</option>
                                        <option value = 'Mayo'>Mayo</option>
                                        <option value = 'Junio'>Junio</option>
                                        <option value = 'Julio'>Julio</option>
                                        <option value = 'Agosto'>Agosto</option>
                                        <option value = 'Septiembre'>Septiembre</option>
                                        <option value = 'Octubre'>Octubre</option>
                                        <option value = 'Noviembre'>Noviembre</option>
                                        <option value = 'Diciembre'>Diciembre</option>
                                    </select> 
                                </div>                  
                            </div>

                            <div class="row">
                                <label class="control-label col-sm-2"></label>
                                <div class="col-sm-7">
                                    <p aling ="right">
                                        <button type="submit" class="btn btn-primary btn-info col-sm-offset-10" > Continuar <span class="glyphicon glyphicon-chevron-right"></span></button> 
                                    </p>          
                                </div>                            
                            </div>    

                        </form>
                    <!--************************formulario*********************-->
                    </div>
                </div>
            </div>                                    
        </div>
    </div>
</div>