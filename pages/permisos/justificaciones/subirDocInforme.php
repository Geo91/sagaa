<?php 
    $maindir = "../../../";
    include($maindir."conexion/config.inc.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");

    if(isset($_POST['vMesInf']) && isset($_POST['vAnio']) && isset($_POST['vDepend'])) {

        $cMes = $_POST['vMesInf'];
        $cAnio = $_POST['vAnio'];
        $cDep = $_POST['vDepend'];
    }

    $consulta = "SELECT dependencia.descripcion FROM `dependencia` where dependencia.dependencia_id = '".$cDep."'";
    $result = $db->prepare($consulta);
    $result->execute();
    while ($fila = $result->fetch()) {

        $desc = $fila["descripcion"];

    }

    $consulta2 = "SELECT gi_informe_inasistencias.id_informe FROM `gi_informe_inasistencias` where gi_informe_inasistencias.mes = :cM AND gi_informe_inasistencias.anio = :cA AND gi_informe_inasistencias.id_dependencia = :cD;";
    $resultado = $db->prepare($consulta2);
    $resultado->bindParam(":cM", $cMes);
    $resultado->bindParam(":cA", $cAnio);
    $resultado->bindParam(":cD", $cDep);
    $resultado->execute();

    while ($fila2 = $resultado->fetch()) {

        $idI = $fila2["id_informe"];

    }     

    $identificador = $cDep."-".$cMes."-".$cAnio;

?>

<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel" style = "color:white">Subir Documento</h4>
</div>

<div class="modal-body">

    <form class="form-horizontal" id="formDocInforme" enctype="multipart/form-data" method="POST">
      
        <input style="display:none" type="text" id="identificador" name="identificador" value = "<?php echo $identificador; ?>" disabled="">                                          
    
        <label>Seleccione el documento: </label>
        <br>
        <input type="file" id="userImage" name="userImage" accept=".pdf">
        <br>

        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary" id="foto">Guardar</button>

    </form>
</div>

<script>

$(document).ready(function(){

    $("#formDocInforme").submit(function(e) {
        e.preventDefault();
        var inf = "<?php echo $idI; ?>";
        var id = "<?php echo $identificador; ?>";

        var parametros = new FormData($("#formDocInforme")[0]);
        parametros.append("cod", inf);
        parametros.append("name", id);
        $.ajax({
            url: "pages/permisos/justificaciones/subirArchivoInforme.php",
            type: "POST",
            data: parametros,
            contentType: false,
            processData: false,
        })
            .done(function(res){
                $("#divRespuesta").html(res);
                $("#modalDocumento").modal("hide");
            });

    });

});

</script>


<!--******************Script de interacción*****************-->
<!--<script type="text/javascript" src="pages/permisos/justificaciones/ScriptInforme.js"></script>-->