<?php
    $maindir = "../../../";
	require_once($maindir."conexion/config.inc.php");
	require_once($maindir . "Datos/funciones.php");
    
	if(is_array($_FILES)) {
		if(is_uploaded_file($_FILES['userImage']['tmp_name'])) {
			if(!empty($_POST['cod'])){
				$codI = $_POST['cod'];
				$ndoc = $_POST['name'];
				$sourcePath = $_FILES['userImage']['tmp_name'];
				$archivo = $_FILES['userImage']['name'];
				$trozos = explode(".", $archivo); 
				$extension = end($trozos);
				$nombreA = $ndoc.'.'.$extension;
				$targetPath = 'documentos/'.$nombreA;
			
				if(move_uploaded_file($sourcePath,$targetPath)) {
					$consulta=$db->prepare("CALL SP_GI_DOCUMENTO_INFORME_INASISTENCIAS(?,?,@mensajeError)");
					$consulta -> bindParam(1, $codI, PDO::PARAM_INT);
					$consulta -> bindParam(2, $nombreA, PDO::PARAM_STR);
					$resultado = $consulta -> execute();

					$output = $db -> query("select @mensajeError")->fetch(PDO::FETCH_ASSOC);
					$respuesta = $output['@mensajeError'];
		        	if($respuesta==NULL){
					    echo mensajes(utf8_encode("El archivo fue subido exitosamente"), "verde");
					}else{
					    echo mensajes("Hubo un problema al cargar el archivo, intentelo de nuevo o contacte al administrador del sistema.". $mensaje, "rojo");
					}
		       		
				}
			}
		}
	}

?>