<?php
	$maindir = "../../../";
	require_once($maindir."conexion/config.inc.php");
	require_once($maindir."Datos/funciones.php");

	session_start();
	//$idUsuario = $_SESSION['user_id'];
	$pF = $_POST['vmF'];
	$pME = $_POST['vmME'];
	$pFHE = $_POST['vmFHE'];
	$pMS = $_POST['vmMS'];
	$pFHS = $_POST['vmFHS'];
	$pJ = $_POST['vmJ'];
	$pC = $_POST['vmC'];

	try{
		$consulta=$db->prepare("CALL SP_GI_EDITAR_JUSTIFICACIONES(?,?,?,?,?,?,?,@pcMensajeError)");
		$consulta->bindParam(1, $pF, PDO::PARAM_INT);
		$consulta->bindParam(2, $pME, PDO::PARAM_INT);
		$consulta->bindParam(3, $pFHE, PDO::PARAM_INT);
		$consulta->bindParam(4, $pMS, PDO::PARAM_INT);
		$consulta->bindParam(5, $pFHS, PDO::PARAM_INT);
		$consulta->bindParam(6, $pJ, PDO::PARAM_STR);
		$consulta->bindParam(7, $pC, PDO::PARAM_STR);

		$resultado=$consulta->execute();
		$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
		$mensaje = $output['@pcMensajeError'];

		if ($mensaje == NULL){
		    echo mensajes('La justificación fue editada exitosamente','verde');
		}else{
			echo mensajes("Error! ". $mensaje, "rojo");
		}
	}catch(PDOExecption $e){
        echo mensajes("Error: ".$e, "rojo");
    }

    $consulta->closeCursor();

?>

<script type="text/javascript">
    $("#TablaJustificaciones").load('pages/permisos/justificaciones/datosJustificaciones.php');
    $('#formJustificaciones').trigger("reset");
</script>