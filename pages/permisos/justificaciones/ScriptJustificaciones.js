
$("#formJustificaciones").submit(function (e) {
    e.preventDefault();

    //Se recopila la información que se enviara a la base de datos
    data = {
    	vF: $("#fecha").val(),
        vME: $("#minentrada").val(),
    	vFHE: $("#fhent").val(),
        vMS: $("#minsalida").val(),
        vFHS: $("#fhsal").val(),
        vJ: $("#justificacion").val(),
        vD: $("#docInf").val()
    };

    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        beforeSend: inicioEnvio,
        success: guardarJ,
        timeout: 6000,
        error: problemas
    });

    return false;
});

$("#formEditarJustificacion").submit(function (e) {
    e.preventDefault();

    //Se recopila la información que se enviara a la base de datos
    data = {
        vmF: $("#mfecha").val(),
        vmME: $("#mminentrada").val(),
        vmFHE: $("#mfhent").val(),
        vmMS: $("#mminsalida").val(),
        vmFHS: $("#mfhsal").val(),
        vmJ: $("#mjustificacion").val(),
        vmC: $("#codigoJ").val()
    };

    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        beforeSend: inicioEnvio,
        success: editarJ,
        timeout: 6000,
        error: problemas
    });

    return false;
});

function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor. Favor contactar al administrador del sistema.');
}

function guardarJ(){
	$("#divRespuesta").empty();
    $("#divRespuesta").load('pages/permisos/justificaciones/guardarJustificacion.php', data);
}

function deshabilitarI(){
	$("#divRespuesta").empty();
    $("#divRespuesta").load('pages/permisos/justificaciones/inhabilitarInforme.php', data);           
}

function abrirInforme(){
    $("#contenidoModal").load("pages/permisos/revision/verDoc.php",data);
    $("#modalJustificaciones").modal("show");
}

function formeditarJ(){
    $("#contenidoModal").load('pages/permisos/justificaciones/formEditarJustificacion.php', data);
    $("#modalJustificaciones").modal("show");
}

function editarJ(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/permisos/justificaciones/editarJustificacion.php', data);
    $("#modalJustificaciones").modal("hide");
}

function eliminarJ(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/permisos/justificaciones/eliminarJustificacion.php', data);
}