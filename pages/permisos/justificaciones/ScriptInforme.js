
$("#formInformes").submit(function (e) {
    e.preventDefault();

    //Se recopila la información que se enviara a la base de datos
    data = {
    	vDepend: $("#dependencia").val(),
        vAnio: $("#Anio").val(),
    	vMesInf: $("#mes_informe").val()
    };

    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        beforeSend: inicioEnvio,
        success: guardarInforme,
        timeout: 8000,
        error: problemas
    });

    return false;
});

$("#formOficio").submit(function(e) {
    e.preventDefault();
    data={
        cdI:$('#idInforme').val(),
        nO:$('#numO').val()
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        beforeSend: inicioEnvio,
        success: exportarOficio,
        timeout: 8000,
        error: problemas
    }); 
    return false;
});

function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor. Favor contactar al administrador del sistema.');
}

function guardarInforme(){
	$("#divRespuesta").empty();
    $("#divRespuesta").load('pages/permisos/justificaciones/guardarInforme.php', data);
    $("#contenidoModal").load('pages/permisos/justificaciones/subirDocInforme.php',data);
    $("#modalDocumento").modal('show');
    $("#TablaInforme").load('pages/permisos/justificaciones/datosInformes.php'); 
}

function deshabilitarI(){
	$("#divRespuesta").empty();
    $("#divRespuesta").load('pages/permisos/justificaciones/inhabilitarInforme.php', data);           
};

function abrirInforme(){
    $("#contenidoModalV").load("pages/permisos/revision/verDoc.php",data);
    $("#modalVisualizar").modal("show");
}

function generarO(){
    $("#contenidoModal").load('pages/permisos/justificaciones/generarOficio.php', data);
    $("#modalDocumento").modal("show");
}

function exportarOficio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/permisos/justificaciones/exportarOficio.php', data);           
    $("#modalDocumento").modal("hide");
};