CREATE TABLE `gi_informe_inasistencias` (
  `id_informe` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `mes` VARCHAR(12) NOT NULL,
  `anio` YEAR(4) NOT NULL,
  `documento_informe` VARCHAR(50) NULL,
  `id_dependencia` INT(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `gi_informe_inasistencias`
  ADD KEY `id_dependencia` (`id_dependencia`);

ALTER TABLE `gi_informe_inasistencias`
  ADD CONSTRAINT `gi_informe_inasistencias_ibfk_1` FOREIGN KEY (`id_dependencia`) REFERENCES `dependencia` (`dependencia_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `gi_informe_inasistencias` ADD `fecha` DATE NOT NULL AFTER `id_dependencia`;
ALTER TABLE `gi_informe_inasistencias` ADD `habilitado` TINYINT(1) NOT NULL DEFAULT '1' AFTER `fecha`;
ALTER TABLE `gi_informe_inasistencias` ADD `impreso` TINYINT(1) NOT NULL DEFAULT '0' AFTER `habilitado`;
ALTER TABLE `gi_informe_inasistencias` ADD `correlativo` VARCHAR(10) NULL AFTER `habilitado`;
-- REALIZADO
************************************************************************************************

CREATE TABLE `gi_justificacion_inasistencias` (
  `id_justificacion` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `fecha` date NOT NULL,
  `min_ent` int(3) NOT NULL,
  `min_sal` int(3) NOT NULL,
  `fuera_hent` time NOT NULL,
  `fuera_hsal` time NOT NULL,
  `justificacion` VARCHAR(500) NULL,
  `estado` int(1) NOT NULL,
  `n_empleado` VARCHAR(20) NOT NULL
  `informe_id` INT(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `gi_justificacion_inasistencias`
  ADD KEY `n_empleado` (`n_empleado`),
  ADD KEY `informe_id` (`informe_id`);

ALTER TABLE `gi_justificacion_inasistencias`
  ADD CONSTRAINT `gi_informe_inasistencias_ibfk_2` FOREIGN KEY (`informe_id`) REFERENCES `gi_informe_inasistencias` (`id_informe`) ON DELETE CASCADE ON UPDATE CASCADE;

-- REALIZADO
**************************************************************************************************

CREATE TABLE `gi_administradores` (
  `id_administrador` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nivel` INT(1) NOT NULL,
  `n_empleado` YEAR(4) NOT NULL,
  `id_dependencia` INT(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `gi_administradores`
  ADD KEY `id_dependencia` (`id_dependencia`);

ALTER TABLE `gi_administradores`
  ADD CONSTRAINT `gi_administradores_ibfk_1` FOREIGN KEY (`id_dependencia`) REFERENCES `dependencia` (`dependencia_id`) ON DELETE CASCADE ON UPDATE CASCADE;
-- REALIZADO
************************************************************************************************

CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_GI_DOCUMENTO_INFORME_INASISTENCIAS`(IN `pcCodigo` INT, IN `pcDoc` VARCHAR(50), OUT `mensajeError` INT)
SP: BEGIN 

DECLARE vnTempMensaje VARCHAR(500);
DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; 

SET vnTempMensaje := CONCAT('ERROR:', mensaje);
SET mensajeError := vnTempMensaje;
END; 

START TRANSACTION; 
    UPDATE gi_informe_inasistencias SET gi_informe_inasistencias.documento_informe = pcDoc WHERE gi_informe_inasistencias.id_informe = pcCodigo; 
COMMIT; 

END
-- REALIZADO
****************************************************************

CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_GI_GUARDAR_INFORMES`(IN `pMes` VARCHAR(12), IN `pAnio` YEAR(4), IN `pDependencia` INT(11), OUT `pcMensajeError` VARCHAR(500))
SP: BEGIN

    DECLARE vTempMensajeError VARCHAR(500) DEFAULT '';
    DECLARE vContador INT DEFAULT 0; 
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
    
        SET vTempMensajeError := CONCAT('Error: ', vTempMensajeError);
        SET pcMensajeError := vTempMensajeError;
    
    END;    
    
    SELECT
        COUNT(gi_informe_inasistencias.id_informe)
    INTO
        vContador
    FROM
        gi_informe_inasistencias
    WHERE
        gi_informe_inasistencias.mes = pMes AND gi_informe_inasistencias.anio = pAnio AND gi_informe_inasistencias.id_dependencia = pDependencia;
        
    IF vContador > 0 then
        SET pcMensajeError := 'Ya hay un informe registrado con el mes ingresado, intente nuevamente';
        LEAVE SP;
    END IF;
    
    START TRANSACTION;
        INSERT INTO gi_informe_inasistencias (mes, anio, id_dependencia, fecha) VALUES (pMes, pAnio, pDependencia, NOW());
                
    COMMIT;

END
-- REALIZADO
****************************************************************

CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_GI_DATOS_INFORMES`(IN `pcUsuario` INT)
BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);
    DECLARE vEmp INT;
    DECLARE vDepend INT;

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
    
    END;
    
    SET vEmp := (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = pcUsuario);
    
    SET vDepend := (SELECT empleado.id_dependencia FROM empleado WHERE empleado.No_Empleado = vEmp);
    
    SET vcTempMensajeError := 'Error al obtener la información de las solicitudes';
    
    SELECT gi_informe_inasistencias.id_informe, gi_informe_inasistencias.mes, gi_informe_inasistencias.anio, gi_informe_inasistencias.documento_informe FROM gi_informe_inasistencias WHERE YEAR(gi_informe_inasistencias.fecha) = YEAR(NOW()) AND gi_informe_inasistencias.id_dependencia = vDepend; 

END
-- REALIZADO
****************************************************************

CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_GI_VALIDAR_INFORME`(IN `pcUsuario` INT)
BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);
    DECLARE vEmp INT;
    DECLARE vDepend INT;

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
    
    END;
    
    SET vEmp := (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = pcUsuario);
    
    SET vDepend := (SELECT empleado.id_dependencia FROM empleado WHERE empleado.No_Empleado = vEmp);
    
    SET vcTempMensajeError := 'Error al obtener la información';
    
    SELECT gi_informe_inasistencias.documento_informe, gi_informe_inasistencias.habilitado FROM gi_informe_inasistencias WHERE (MONTH(gi_informe_inasistencias.fecha) = MONTH(NOW())) AND gi_informe_inasistencias.habilitado = '1' AND gi_informe_inasistencias.id_dependencia = vDepend ORDER BY gi_informe_inasistencias.id_informe DESC; 
    
END
-- REALIZADO 
****************************************************************

CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_GI_GUARDAR_JUSTIFICACIONES`(IN `pFecha` VARCHAR(12), IN `pMinent` INT, IN `pFuerahe` TIME, IN `pMinsal` INT, IN `pFuerahs` TIME, IN `pJustificacion` VARCHAR(500), IN `pInforme` VARCHAR(50), IN `pUsuario` INT, OUT `pcMensajeError` VARCHAR(500))
SP: BEGIN

    DECLARE vTempMensajeError VARCHAR(500) DEFAULT '';
    DECLARE vContador INT DEFAULT 0;
    DECLARE vEmp INT;
    DECLARE vInforme INT;
    DECLARE vAdmin INT;
    DECLARE vNivel INT;
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
    
        SET vTempMensajeError := CONCAT('Error: ', vTempMensajeError);
        SET pcMensajeError := vTempMensajeError;
    
    END;
    
    SET vEmp := (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = pUsuario);
    
    SET vInforme := (SELECT gi_informe_inasistencias.id_informe FROM gi_informe_inasistencias WHERE gi_informe_inasistencias.documento_informe = pInforme);
    
    SET vAdmin := (SELECT COUNT(gi_administradores.id_administrador) FROM gi_administradores WHERE gi_administradores.n_empleado = vEmp);
    
    SELECT
        COUNT(gi_justificacion_inasistencias.id_justificacion)
    INTO
        vContador
    FROM
        gi_justificacion_inasistencias
    WHERE
        gi_justificacion_inasistencias.fecha = pFecha AND gi_justificacion_inasistencias.n_empleado = vEmp;
        
    IF vContador > 0 then
        SET pcMensajeError := 'La fecha ingresada ya ha sido justificada';
        LEAVE SP;
    END IF;
    
    START TRANSACTION;
    IF (vAdmin = 1) THEN
      SET vNivel := (SELECT gi_administradores.nivel FROM gi_administradores WHERE gi_administradores.n_empleado = vEmp);
        IF (vNivel = 1) THEN
          INSERT INTO gi_justificacion_inasistencias (fecha, min_ent, fuera_hent, min_sal, fuera_hsal, justificacion, estado, n_empleado, informe_id) VALUES (pFecha, pMinent, pFuerahe, pMinsal, pFuerahs, pJustificacion, '2', vEmp, vInforme);
        ELSE
          INSERT INTO gi_justificacion_inasistencias (fecha, min_ent, fuera_hent, min_sal, fuera_hsal, justificacion, estado, n_empleado, informe_id) VALUES (pFecha, pMinent, pFuerahe, pMinsal, pFuerahs, pJustificacion, '1', vEmp, vInforme);
        END IF;
    ELSE        
        INSERT INTO gi_justificacion_inasistencias (fecha, min_ent, fuera_hent, min_sal, fuera_hsal, justificacion, estado, n_empleado, informe_id) VALUES (pFecha, pMinent, pFuerahe, pMinsal, pFuerahs, pJustificacion, '0', vEmp, vInforme);
    END IF;

    COMMIT;

END
-- REALIZADO
*********************************************************************

CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_GI_DATOS_JUSTIFICACIONES`(IN `pcUsuario` INT, OUT `pcMensajeError` VARCHAR(500))
BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);
    DECLARE vEmp INT;
    DECLARE vDepend INT;

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
    
    END;
    
    SET vEmp := (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = pcUsuario);
    
    SET vDepend := (SELECT empleado.id_dependencia FROM empleado WHERE empleado.No_Empleado = vEmp);
    
    SET vcTempMensajeError := 'Error al obtener la información de las solicitudes';
    
    SELECT gi_justificacion_inasistencias.id_justificacion, DAY(gi_justificacion_inasistencias.fecha) as 'dia', gi_justificacion_inasistencias.fecha, gi_justificacion_inasistencias.min_ent, gi_justificacion_inasistencias.fuera_hent, gi_justificacion_inasistencias.min_sal, gi_justificacion_inasistencias.fuera_hsal, gi_justificacion_inasistencias.justificacion, (CASE gi_justificacion_inasistencias.estado WHEN '0' THEN 'Espera' WHEN '1' THEN 'VB' WHEN '2' THEN 'Justificado' WHEN '3' THEN 'No Justificado' END) as 'estado' FROM gi_justificacion_inasistencias WHERE (MONTH(gi_justificacion_inasistencias.fecha) = (MONTH(NOW())-1)) AND gi_justificacion_inasistencias.n_empleado = vEmp; 

END
-- REALIZADO
****************************************************************

CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_GI_EDITAR_JUSTIFICACIONES`(IN `pFecha` VARCHAR(12), IN `pMinent` INT, IN `pFuerahe` TIME, IN `pMinsal` INT, IN `pFuerahs` TIME, IN `pJustificacion` VARCHAR(500), IN `pCodigo` INT, OUT `pcMensajeError` VARCHAR(500))
SP: BEGIN

    DECLARE vTempMensajeError VARCHAR(500) DEFAULT '';
    DECLARE vContador INT DEFAULT 0;
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
    
        SET vTempMensajeError := CONCAT('Error: ', vTempMensajeError);
        SET pcMensajeError := vTempMensajeError;
    
    END;
        
    SELECT
        COUNT(gi_justificacion_inasistencias.id_justificacion)
    INTO
        vContador
    FROM
        gi_justificacion_inasistencias
    WHERE
        gi_justificacion_inasistencias.fecha = pFecha AND gi_justificacion_inasistencias.id_justificacion != pCodigo;
        
    IF vContador > 0 then
        SET pcMensajeError := 'La fecha ingresada ya ha sido justificada';
        LEAVE SP;
    END IF;
    
    START TRANSACTION;

      UPDATE gi_justificacion_inasistencias SET fecha=pFecha, min_ent=pMinent, fuera_hent=pFuerahe, min_sal=pMinsal, fuera_hsal=pFuerahs, justificacion=pJustificacion WHERE gi_justificacion_inasistencias.id_justificacion = pCodigo;
        
                
    COMMIT;

END
-- REALIZADO
****************************************************************

CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_GI_ELIMINAR_JUSTIFICACIONES`(IN `pCodigo` INT, OUT `mensaje` VARCHAR(250))
SP: BEGIN

   DECLARE EXIT HANDLER FOR SQLEXCEPTION
   BEGIN
     SET mensaje = "No se pudo realizar la operacion, por favor intente de nuevo dentro de un momento";
     ROLLBACK;
   END;
   
  DELETE FROM gi_justificacion_inasistencias WHERE gi_justificacion_inasistencias.id_justificacion = pCodigo;
END
-- REALIZADO
****************************************************************

CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_GI_JUSTIFICACIONES_EMPLEADOS`(IN `pcUsuario` INT)
BEGIN

    DECLARE vEmp INT;
    DECLARE vDepend INT;
    DECLARE vNivel INT;

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
    
    END;
    
    SET vEmp := (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = pcUsuario);
    
    SET vDepend := (SELECT empleado.id_dependencia FROM empleado WHERE empleado.No_Empleado = vEmp);
    
    SET vNivel := (SELECT gi_administradores.nivel FROM gi_administradores WHERE gi_administradores.n_empleado = vEmp);
    
    IF (vNivel = 0) THEN
      SELECT empleado.No_Empleado, CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'Empleado', cargo.Cargo, gi_informe_inasistencias.id_informe, gi_informe_inasistencias.mes FROM persona, empleado, empleado_has_cargo, cargo, dependencia, gi_informe_inasistencias WHERE empleado_has_cargo.ID_cargo=cargo.ID_cargo AND empleado.No_Empleado=empleado_has_cargo.No_Empleado AND dependencia.dependencia_id=gi_informe_inasistencias.id_dependencia AND empleado.id_dependencia=dependencia.dependencia_id AND empleado.N_identidad=persona.N_identidad AND dependencia.dependencia_id = vDepend AND (MONTH(gi_informe_inasistencias.fecha)=MONTH(NOW())) AND empleado.No_Empleado IN (SELECT gi_justificacion_inasistencias.n_empleado FROM gi_justificacion_inasistencias WHERE gi_justificacion_inasistencias.estado = 0);
    ELSEIF (vNivel = 1) THEN 
      SELECT empleado.No_Empleado, CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'Empleado', cargo.Cargo, gi_informe_inasistencias.id_informe, gi_informe_inasistencias.mes FROM persona, empleado, empleado_has_cargo, cargo, dependencia, gi_informe_inasistencias WHERE empleado_has_cargo.ID_cargo=cargo.ID_cargo AND empleado.No_Empleado=empleado_has_cargo.No_Empleado AND dependencia.dependencia_id=gi_informe_inasistencias.id_dependencia AND empleado.id_dependencia=dependencia.dependencia_id AND empleado.N_identidad=persona.N_identidad AND dependencia.dependencia_id = vDepend AND (MONTH(gi_informe_inasistencias.fecha)=MONTH(NOW())) AND empleado.No_Empleado IN (SELECT gi_justificacion_inasistencias.n_empleado FROM gi_justificacion_inasistencias WHERE gi_justificacion_inasistencias.estado = 1);
    END IF;
    
END
-- REALIZADO
*********************************************************************

CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_GI_DATOS_JUSTIFICACIONES_EMP`(IN `pEmp` VARCHAR(20), IN `pInf` INT)
BEGIN

    DECLARE vcTempMensajeError VARCHAR(1000);

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
    
        ROLLBACK;
        SET vcTempMensajeError := CONCAT('Error: ', vcTempMensajeError);
    
    END;
       
    SET vcTempMensajeError := 'Error al obtener la información de las solicitudes';
    
    SELECT gi_justificacion_inasistencias.id_justificacion, gi_justificacion_inasistencias.fecha, gi_justificacion_inasistencias.min_ent, gi_justificacion_inasistencias.fuera_hent, gi_justificacion_inasistencias.min_sal, gi_justificacion_inasistencias.fuera_hsal, gi_justificacion_inasistencias.justificacion FROM gi_justificacion_inasistencias WHERE (MONTH(gi_justificacion_inasistencias.fecha) = (MONTH(NOW())-1)) AND gi_justificacion_inasistencias.n_empleado = pEmp AND gi_justificacion_inasistencias.informe_id = pInf; 

END
-- REALIZADO
*********************************************************************

CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_GI_REVISAR_JUSTIFICACIONES`(IN `pCodigo` VARCHAR(20), IN `pOperador` INT, OUT `mensajeError` VARCHAR(200))
SP: BEGIN 

    DECLARE vnTempMensaje VARCHAR(500);
    DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; 

    SET vnTempMensaje := CONCAT('ERROR:', mensaje);
    SET mensajeError := vnTempMensaje;
    END; 

    START TRANSACTION;
        IF (pOperador = 1) THEN
            UPDATE gi_justificacion_inasistencias SET gi_justificacion_inasistencias.estado = '2' WHERE gi_justificacion_inasistencias.id_justificacion = pCodigo;
        ELSEIF (pOperador = 0) THEN
            UPDATE gi_justificacion_inasistencias SET gi_justificacion_inasistencias.estado = '3' WHERE gi_justificacion_inasistencias.id_justificacion = pCodigo;
        END IF;

    COMMIT; 

END
-- REALIZADO
*********************************************************************

CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_GI_APROBAR_JUSTIFICACIONES_T`(IN `pEmpleado` VARCHAR(20), IN `pInforme` INT, IN `pUsuario` INT, OUT `pcMensajeError` VARCHAR(1000))
SP: BEGIN
    
    DECLARE temp_justificacion INT;
    DECLARE fin INTEGER DEFAULT 0;
    DECLARE nivel INTEGER DEFAULT 0;
    DECLARE emp INT;
    
    DECLARE ciclo_justificaciones CURSOR FOR
        SELECT gi_justificacion_inasistencias.id_justificacion FROM gi_justificacion_inasistencias WHERE gi_justificacion_inasistencias.n_empleado = pEmpleado AND gi_justificacion_inasistencias.informe_id = pInforme AND gi_justificacion_inasistencias.estado IN ('0','1');
        
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET fin=1;
    
    SET emp := (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = pUsuario);
    
    SET nivel := (SELECT gi_administradores.nivel FROM gi_administradores WHERE gi_administradores.n_empleado = emp);

    START TRANSACTION;
        OPEN ciclo_justificaciones;
            get_justificaciones: LOOP
                FETCH ciclo_justificaciones INTO temp_justificacion;
                IF fin = 1 THEN
                    LEAVE get_justificaciones;
                END IF;
                
                IF (nivel = 0) THEN
                  UPDATE gi_justificacion_inasistencias SET gi_justificacion_inasistencias.estado = '1' WHERE gi_justificacion_inasistencias.id_justificacion = temp_justificacion;
                ELSEIF (nivel = 1) THEN
                  UPDATE gi_justificacion_inasistencias SET gi_justificacion_inasistencias.estado = '2' WHERE gi_justificacion_inasistencias.id_justificacion = temp_justificacion;
                END IF;
                
            END LOOP get_justificaciones;
        CLOSE ciclo_justificaciones;

    COMMIT;
    
END
-- REALIZADO
**************************************************************************

CREATE DEFINER=`aeayestas`@`localhost` PROCEDURE `SP_GI_FORMATO_JUSTIFICACIONES`(IN `pCodigo` INT, IN `pUsuario` INT, IN `pCorrelativo` VARCHAR(10), OUT `mensaje` VARCHAR(500))
SP: BEGIN 
DECLARE vnTempMensaje VARCHAR(500);
DECLARE contador INT;

DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN ROLLBACK; 

SET vnTempMensaje := CONCAT('ERROR:', mensaje);
SET mensaje := vnTempMensaje;
END; 

SET contador := (SELECT COUNT(gi_informe_inasistencias.id_informe) FROM gi_informe_inasistencias WHERE gi_informe_inasistencias.habilitado = '0' AND gi_informe_inasistencias.id_informe = pCodigo);

IF (contador = 0) THEN 
    BEGIN
        SET mensaje := CONCAT('No se pudo exportar el documento');
        LEAVE SP;
    END;
END IF;

START TRANSACTION; 
    
    UPDATE gi_informe_inasistencias SET gi_informe_inasistencias.impreso = '1', gi_informe_inasistencias.correlativo = pCorrelativo WHERE gi_informe_inasistencias.id_informe = pCodigo; 
    
COMMIT; 

END
-- REALIZADO
**************************************************************************