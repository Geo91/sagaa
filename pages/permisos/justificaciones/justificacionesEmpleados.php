<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");
    require_once("../ChekAutoIncrement.php");
    $idUsuario = $_SESSION['user_id'];

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <!--<section class="content">-->
                <div class="table-responsive">
                    <table id= "TablaEmpleadosJustificaciones" border="0" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th style = "text-align:center;background-color:#386D95;color:white;display:none">ID</th>
                                <th style = "text-align:center;background-color:#386D95;color:white;">No Empleado</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Empleado</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Puesto</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Mes</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Ver</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Contenido de la tabla generado atravez de la consulta a 
                                la base de datos -->
                                <?php
                                    try{
                                        $proc = "CALL SP_GI_JUSTIFICACIONES_EMPLEADOS(?)";
                                        $query = $db->prepare($proc);
                                        $query->bindParam(1,$idUsuario,PDO::PARAM_INT);

                                        $query->execute();

                                        while ($row = $query->fetch()) {
                                ?>
                                        <tr data-id='<?php echo $row["id_informe"]; ?>' data-emp='<?php echo $row["No_Empleado"]; ?>' data-nombre='<?php echo $row["Empleado"]; ?>'>
                                            <td style="display:none"><?php echo $row["id_informe"]; ?></td>
                                            <td><?php echo $row["No_Empleado"]; ?></td>
                                            <td><?php echo $row["Empleado"]; ?></td>
                                            <td><?php echo $row["Cargo"]; ?></td>
                                            <td><?php echo $row["mes"]; ?></td>
                                            <td>
                                                <center>
                                                <div>
                                                    <button type="button" class="verJustificaciones<?php echo $auto_increment; ?> btn btn-warning" title="Editar la justificación." ><span class="glyphicon glyphicon-list-alt"></span></button>

                                                </div>
                                                </center>
                                            </td>    
                                        </tr>
                                <?php
                                        } //cierre del ciclo while para llenar la tabla de datos
                                    }catch(PDOException $e){
                                        echo "Error: ".$e;
                                    }
                                ?>
                        </tbody>
                    </table>       
                </div>
            <!--</section>-->
        </div>                
    </div>
</div>

<script type="text/javascript">   
//opciones para buscador en la tabla
    $('#TablaEmpleadosJustificaciones')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#TablaEmpleadosJustificaciones').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {

        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });


    $(document).on("click", ".verJustificaciones<?php echo $auto_increment; ?>", function () {  
        data = {
            idI: $(this).parents("tr").data("id"),
            nemp: $(this).parents("tr").data("emp"),
            name: $(this).parents("tr").data("nombre"),
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: verJ,
            timeout: 12000,
            error: problemas
        }); 
        return false; 
    });

</script>
