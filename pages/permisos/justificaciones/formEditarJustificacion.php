<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");
    
    if(isset($_POST['idJ'])){
    	$codJ = $_POST['idJ'];
    	$cF = $_POST['fecha'];
	    $cME = $_POST['ment'];
	    $cMS = $_POST['msal'];
	    $cFHE = $_POST['fhent'];
	    $cFHS = $_POST['fhsal'];
	    $cJust = $_POST['justif'];

?>

<div class="modal-header" style = "background-color:#0FA6C3">
  	<button type="button" class="close" data-dismiss="modal">&times;</button>
  	<h4 class="modal-title" style = "color:white">Editar Justificación de Inasistencia </h4>
</div>

<div class="modal-body">
        
    <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden</center></div>

    <div class="row">
        <div class="col-lg-12">

	    <!--************************formulario*********************-->           
	        <form class="form-horizontal" role="form" id="formEditarJustificacion" name="formEditarJustificacion">

				<div class="row form-group" style="display:none">
	                <label class="col-sm-2 control-label"> Codigo </label>
	                <div class="col-sm-2">
                        <input type="text" class="form-control" id="codigoJ" name="codigoJ" value = "<?php echo $codJ; ?>">
                    </div>
                </div>

		        <div class="row form-group"> 
		            <label class="col-sm-2 control-label">Fecha</label>
		            <div class="col-sm-4">
		                <input type="date" id="mfecha" name="datepicker" value="<?php echo $cF; ?>" required><span class=""></span>
		            </div>               
		        </div>

		        <div class="row form-group">
	                <label class="col-sm-2 control-label"> Min. Entrada </label>
	                <div class="col-sm-2">
                        <input type="number" class="form-control" id="mminentrada" name="minentrada" min = "0" value = "<?php echo $cME; ?>" required>
                    </div>

                    <label class="control-label col-sm-3">Fuera Horario Entrada</label>
	                <div class="col-sm-3">
	                    <input type="time" id="mfhent" name="fhent" value = "<?php echo $cFHE; ?>" required><span class=""></span>
	                </div>   
	            </div>

                <div class="row form-group">
	                <label class="col-sm-2 control-label"> Min. Salida </label>
	                <div class="col-sm-2">
                        <input type="number" class="form-control" id="mminsalida" name="minsalida" min = "0" value = "<?php echo $cMS; ?>"required>
                    </div>

                    <label class="control-label col-sm-3">Fuera Horario Salida</label>
	                <div class="col-sm-3">
	                    <input type="time" id="mfhsal" name="fhsal" value = "<?php echo $cFHS; ?>" required><span class=""></span>
	                </div>   
	            </div>

				<div class="row form-group">
	                <label class=" col-sm-2 control-label">Justificación</label>
                    <div class="col-sm-7">     
                        <textarea rows="3" class="form-control" id="mjustificacion" name="justificacion" title = "Ingrese una justificación" value = "" required><?php echo $cJust; ?></textarea>       
                    </div>
	            </div>

	            <div class="modal-footer">
	                <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
	                <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" > Siguiente <span class="glyphicon glyphicon-circle-arrow-right"></span></button>                                  
	            </div>  

	        </form>
	    <!--************************formulario*********************-->
	    </div>
	</div>
</div>

<?php
	}else{
	echo mensajes("Hubo un error al intentar editar la justificación seleccionada, contacte al administrador del sistema");	
}
?>

<script type="text/javascript" src="pages/permisos/justificaciones/ScriptJustificaciones.js"></script>