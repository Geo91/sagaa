<?php

    $maindir = "../../../";
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    include($maindir."conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    $idUsuario = $_SESSION['user_id'];

    $sql = "CALL SP_GI_VALIDAR_INFORME(?)";
    $resultado=$db->prepare($sql);
    $resultado->bindParam(1,$idUsuario,PDO::PARAM_INT);
    $resultado->execute();
    $conteo = $resultado->rowCount();

    while($fila = $resultado->fetch()){
    	$doc = $fila['documento_informe'];
    	$estado = $fila['habilitado'];
    }

    $resultado->closeCursor();

?>

<div  id="divRespuestaJustificacion"></div>

<?php if($conteo == 1){ ?>

<div class="row">
    <div class = "col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Agregar nuevo informe de inasistencia</div>

            <div class="panel-body">
                <div >
                    <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden</center></div>
                </div>
                <div class="row">
                    <div class="col-lg-10">
                    <!--************************formulario*********************-->
                        <form class="form-horizontal" role="form" id="formJustificaciones" name="formJustificaciones">

                        	<div class="row form-group" style="display:none">
				                <label class="col-sm-2 control-label"> Documento </label>
				                <div class="col-sm-2">
			                        <input type="text" class="form-control" id="docInf" name="docInf" value = "<?php echo $doc; ?>">
			                    </div>
			                </div>

					        <div class="row form-group"> 
					            <label class="col-sm-2 control-label">Fecha</label>
					            <div class="col-sm-4">
					                <input type="date" id="fecha" name="datepicker" required><span class=""></span>
					            </div>               
					        </div>

					        <div class="row form-group">
				                <label class="col-sm-2 control-label"> Min. Entrada </label>
				                <div class="col-sm-2">
			                        <input type="number" class="form-control" id="minentrada" name="minentrada" min = "0" required>
			                    </div>

			                    <label class="control-label col-sm-3">Fuera Horario Entrada</label>
				                <div class="col-sm-3">
				                    <input type="time" id="fhent" name="fhent" required><span class=""></span>
				                </div>   
				            </div>

                            <div class="row form-group">
				                <label class="col-sm-2 control-label"> Min. Salida </label>
				                <div class="col-sm-2">
			                        <input type="number" class="form-control" id="minsalida" name="minsalida" min = "0" required>
			                    </div>

			                    <label class="control-label col-sm-3">Fuera Horario Salida</label>
				                <div class="col-sm-3">
				                    <input type="time" id="fhsal" name="fhsal" required><span class=""></span>
				                </div>   
				            </div>

							<div class="row form-group">
				                <label class=" col-sm-2 control-label">Justificación</label>
			                    <div class="col-sm-7">     
			                        <textarea rows="3" class="form-control" id="justificacion" name="justificacion" title = "Ingrese una justificación" required></textarea>       
			                    </div>
				            </div>

                            <div class="modal-footer">
                                        <button type="button" class="verDocumento btn btn-primary btn-warning"> Ver Reporte <span class="glyphicon glyphicon-folder-open"></span></button> 
                                        <button type="submit" class="btn btn-primary" > Continuar <span class="glyphicon glyphicon-chevron-right"></span></button>                             
                            </div>    

                        </form>
                    <!--************************formulario*********************-->
                    </div>
                </div>
            </div>                                    
        </div>
    </div>
</div>
<?php 
	}elseif($conteo > 1){
		echo mensajes("Se ha producido un error, contacte al administrador del sistema","rojo");
	}else{
		echo mensajes("No hay reportes pendientes de justificar","amarillo");
	}
?>

<script>

$(document).on("click", ".verDocumento", function () {  

        data = {
            arch: $("#docInf").val(),
            tp: 2
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: abrirInforme,
            timeout: 8000,
            error: problemas
        }); 

        return false;
    });

</script>
