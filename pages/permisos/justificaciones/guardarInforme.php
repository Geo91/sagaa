<?php
	$maindir = "../../../";
	require_once($maindir."conexion/config.inc.php");
	require_once($maindir."Datos/funciones.php");

	session_start();
	$pD = $_POST['vDepend'];
	$pA = $_POST['vAnio'];
	$pMI = $_POST['vMesInf'];

	try{
		$consulta=$db->prepare("CALL SP_GI_GUARDAR_INFORMES(?,?,?,@pcMensajeError)");
		$consulta->bindParam(1, $pMI, PDO::PARAM_STR);
		$consulta->bindParam(2, $pA, PDO::PARAM_INT);
		$consulta->bindParam(3, $pD, PDO::PARAM_INT);

		$resultado=$consulta->execute();
		$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
		$mensaje = $output['@pcMensajeError'];

		if ($mensaje == NULL){
		    echo '';
		}else{
			echo mensajes("Error! ". $mensaje, "rojo");
		}
	}catch(PDOExecption $e){
        echo mensajes("Error: ".$e, "rojo");
    }

?>


