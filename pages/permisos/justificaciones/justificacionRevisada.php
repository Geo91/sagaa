<?php

	$maindir = "../../../";
	require_once($maindir."conexion/config.inc.php");
	require_once($maindir."Datos/funciones.php");

	$pJ = $_POST['idJ'];
	$pO = $_POST['op'];
	$pNE = $_POST['vNE'];
	$pInf = $_POST['vInf'];
	$pName = $_POST['vName'];

	try{
		$consulta=$db->prepare("CALL SP_GI_REVISAR_JUSTIFICACIONES(?,?,@pcMensajeError)");
		$consulta->bindParam(1, $pJ, PDO::PARAM_INT);
		$consulta->bindParam(2, $pO, PDO::PARAM_INT);

		$resultado=$consulta->execute();
		$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
		$mensaje = $output['@pcMensajeError'];

		if ($mensaje == NULL){
		    if($pO == 1){
		    	echo mensajes('La justificación fue aprobada exitosamente','verde');
		   	}else{
		   		echo mensajes('La justificación fue denegada exitosamente','verde');
		   	}
		}else{
			echo mensajes("Error! ". $mensaje, "rojo");
		}
	}catch(PDOExecption $e){
        echo mensajes("Error: ".$e, "rojo");
    }

    $consulta->closeCursor();

?>

<script type="text/javascript">
	var codE = '<?php echo $pNE; ?>';
	var codI = '<?php echo $pInf; ?>';
	var nombre = '<?php echo $pName; ?>';

	data = {
		idI: codI,
		nemp: codE,
		name: nombre
	}
	
    $("#contenidoModal").load("pages/permisos/justificaciones/datosJustifEmp.php",data);
    $("#modalRevisionJ").modal("show");
</script>