<?php

    $maindir = "../../../";

    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");
    require_once($maindir."fpdf/fpdf.php");

    $idI = $_GET['id'];
    $vNO = $_GET['nF'];

    $consulta = "SELECT gi_informe_inasistencias.id_informe, gi_informe_inasistencias.mes, gi_informe_inasistencias.anio, gi_informe_inasistencias.correlativo, dependencia.dependencia_id, dependencia.descripcion FROM gi_informe_inasistencias INNER JOIN dependencia ON gi_informe_inasistencias.id_dependencia=dependencia.dependencia_id WHERE gi_informe_inasistencias.id_informe = :cI";

    $query = $db->prepare($consulta);
    $query ->bindParam(":cI",$idI);
    $query->execute();
    $result = $query->fetch();

    $inf = $result['id_informe'];
    $mes = $result['mes'];
    $anio = $result['anio'];
    $crltv = $result['correlativo'];
    $idepend = $result['dependencia_id'];
    $depen = $result['descripcion'];
    
    function mes($fecha){
        switch (date('m',strtotime($fecha))) {
            case '01':
                $mes = 'Enero';
                break;
            case '02':
                $mes = 'Febrero';
                break;
            case '03':
                $mes = 'Marzo';
                break;
            case '04':
                $mes = 'Abril';
                break;
            case '05':
                $mes = 'Mayo';
                break;
            case '06':
                $mes = 'Junio';
                break;
            case '07':
                $mes = 'Julio';
                break;
            case '08':
                $mes = 'Agosto';
                break;
            case '09':
                $mes = 'Septiembre';
                break;
            case '10':
                $mes = 'Octubre';
                break;
            case '11':
                $mes = 'Noviembre';
                break;
            case '12':
                $mes = 'Diciembre';
                break;
        }
        return $mes;
    }

    function dia($fecha){
        switch (date('l', strtotime(($fecha)))) {
            case 'Monday':
                $dia = 'Lunes';
                break;
            case 'Tuesday':
                $dia = 'Martes';
                break;
            case 'Wednesday':
                $dia = 'Miércoles';
                break;
            case 'Thursday':
                $dia = 'Jueves';
                break;
            case 'Friday':
                $dia = 'Viernes';
                break;
            case 'Saturday':
                $dia = 'Sábado';
                break;
            case 'Sunday':
                $dia = 'Domingo';
                break;
        }
        return $dia;
    }

    function Mayus($texto) {
        $resp = strtr(strtoupper($texto),"àèìòùáéíóúçñäëïöü","ÀÈÌÒÙÁÉÍÓÚÇÑÄËÏÖÜ");
        return $resp;
    }


class PDF extends FPDF{
     // Cabecera de página
         
    /*function Footer(){
        $maindir = "../../../";
        $idPermiso = $_GET['id'];
        $consulta2 = "SELECT permisos.correlativo from permisos where permisos.estado = 5 AND permisos.id_Permisos='".$idPermiso."'";
        $resultado2 = mysql_query($consulta2);
        $result2 = mysql_fetch_array($resultado2);
        $cltv = $result2['correlativo'];

    }*/
         
}


$pdf = new PDF();
$pdf->AliasNbPages();

    $pdf->AddPage('P','Letter',0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Image($maindir.'assets/img/enc_doc.png', 2,1,211,32, 'PNG'); //Encabezado
    $pdf->Image($maindir.'assets/img/pieDoc.png', 5,148,208,130, 'PNG'); //Pie de página
    $pdf->Cell(160);
    $pdf->Cell(0, 15, 'Tel: 2216-5100 ', 0,0,"R");
    $pdf->Ln(4);
    $pdf->Cell(0, 15, 'Edificio A-2 ', 0,0,"R");
    $pdf->Ln(20);
    $pdf->SetFont('Arial', 'I', 12);
    $pdf->Cell(0, 8, utf8_decode($vNO), 0,0,"C");
    $pdf->Ln(6);
    $pdf->SetFont('Arial', 'I', 12);
    $pdf->Cell(0, 8, utf8_decode('Tegucigalpa M.D.C, '.date('d').' de '.mes(date('Y-m-d')).' del '.date('Y')), 0,0,"C");
    $pdf->SetFont('Arial', '', 12);
    
    $pdf->Ln(15);
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode('Máster'), 0,0,"");
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B','12');
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode('Jacinta Ruíz'), 0,0,"");
    $pdf->Ln(6);
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode('Secretaria Ejecutiva de Desarrollo de Personal'), 0,0,"");
    $pdf->Ln(6);
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode('UNAH'), 0,0,"");
    $pdf->Ln(6);
    $pdf->SetFont('Arial', '', '12');
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode('Su oficina'), 0,0,"");

    $pdf->Ln(15);
    $pdf->Cell(12);
    $pdf->Cell(0,10,utf8_decode('Estimada Máster Ruíz:'), 0,0,"");

    $pdf->Ln(13);
    $pdf->Cell(12);

    if($idepend == '1'){
        $aux = 'de la ';
    }else{
        $aux = 'del ';
    }

    $pdf->MultiCell(170, 7,utf8_decode('Con el presente le remito el Cuadro de Justificaciones y No Justificaciones correspondiente al informe de inasistencias del personal administrativo '.$aux.$depen.' del mes de '.$mes.'-'.$anio.'.'), 0,"J",0);
    
    
    $pdf->Ln(6);
    $pdf->Cell(12);
    $pdf->MultiCell(170, 7,utf8_decode('Asimismo, respetuosamente se solicita no aplicar deducciones de salario al personal que se muestra justificado.'), 0,"J",0);

    $pdf->Ln(6);
    $pdf->Cell(12);
    $pdf->MultiCell(170, 7,utf8_decode('Agradeciéndole su valiosa colaboración, me suscribo de usted con las muestras de mi consideración y estima.'), 0,"J",0);

    $pdf->Ln(10);
    $pdf->Cell(12);
    $pdf->Cell(0,10,'Atentamente,',0,0,"");
    $pdf->Ln(25);
    $pdf->SetFont('Arial', 'IB', 12);
    if($idepend == '1'){
        $pdf->Cell(0, 8, utf8_decode('ABG. BESSY MARGOTH NAZAR HERRERA'), 0,0,"C");
        $pdf->Ln(6);
        $pdf->Cell(0, 8, utf8_decode('FACULTAD DE CIENCIAS JURÍDICAS'), 0,0,"C");
        $pdf->Ln(6);
        $pdf->Cell(0, 8, utf8_decode('DECANA'), 0,0,"C");
    }elseif($idepend == '2'){
        $pdf->Cell(0, 8, utf8_decode('ABG. JAVIER DAVID LOPEZ PADILLA'), 0,0,"C");
        $pdf->Ln(6);
        $pdf->Cell(0, 8, utf8_decode('INSTITUTO DE INVESTIGACIÓN JURÍDICAS'), 0,0,"C");
        $pdf->Ln(6);
        $pdf->Cell(0, 8, utf8_decode('DIRECTOR IIJ'), 0,0,"C");
    }else{
        $pdf->Cell(0, 8, utf8_decode('ABG. ERLINDA ESPERANZA FLORES FLORES'), 0,0,"C");
        $pdf->Ln(6);
        $pdf->Cell(0, 8, utf8_decode('CONSULTORIO JURÍDICO GRATUITO'), 0,0,"C");
        $pdf->Ln(6);
        $pdf->Cell(0, 8, utf8_decode('DIRECTORA CJG'), 0,0,"C");
    }

    $pdf->Ln(18);
    $pdf->SetFont('Arial','I','8');
    $pdf->Cell(12);
    $pdf->Cell(0,4,utf8_decode('cc. Departamento de Efectividad.'));
    $pdf->Ln(3);
    $pdf->Cell(12);
    $pdf->Cell(0,4,utf8_decode('cc. Archivo'));
    $pdf->SetY(-32);
    $pdf->SetFont('Arial','I',8);
    $pdf->Cell(0,10,utf8_decode('Página ').$pdf->PageNo().' de {nb}   - - - -   Impreso el ' . date('d-m-y') . ' fecha del sistema   - - - -   '.$crltv,0,0,'C');


    $pdf->AddPage('P','Letter',0);
    $pdf->Ln(6);
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->Cell(8);
    $pdf->MultiCell(180, 6, utf8_decode('REPORTE DE DÍAS NO MARCADOS: JUSTIFICACIONES Y NO JUSTIFICACIONES'), 0,"C",false);
    $pdf->Cell(12);
    $pdf->Ln();
    $pdf->Cell(0, 10, strtoupper(utf8_decode(Mayus($mes))).' '.$anio, 0,0,"C");
    $pdf->Ln(15);
    
    $sqlemp = "SELECT CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'Empleado', empleado.No_Empleado, cargo.Cargo FROM persona, empleado, empleado_has_cargo, cargo WHERE persona.N_identidad=empleado.N_identidad AND empleado.No_Empleado=empleado_has_cargo.No_Empleado AND empleado_has_cargo.ID_cargo=cargo.ID_cargo AND empleado.No_Empleado IN (SELECT gi_justificacion_inasistencias.n_empleado FROM gi_justificacion_inasistencias WHERE gi_justificacion_inasistencias.informe_id IN (SELECT gi_informe_inasistencias.id_informe FROM gi_informe_inasistencias WHERE gi_informe_inasistencias.id_informe = :cI AND gi_informe_inasistencias.id_dependencia = :cD))";

    $queryemp = $db->prepare($sqlemp);
    $queryemp->bindParam(":cI",$inf);
    $queryemp->bindParam(":cD",$idepend);
    $queryemp->execute();

    while ($filaemp = $queryemp->fetch()){
        $name = $filaemp['Empleado'];
        $nemp = $filaemp['No_Empleado'];
        $puesto = $filaemp['Cargo'];

        $pdf->Cell(12);
        $pdf->Cell(70, 10, utf8_decode('NOMBRE: '.$name), 0,0,"C", False);
        $pdf->Ln(6);
        $pdf->Cell(12);
        $pdf->Cell(50, 10, utf8_decode('N. DE EMPLEADO: '.$nemp), 0,0,"C", False);
        $pdf->Ln(6);
        $pdf->Cell(12);
        $pdf->Cell(100, 10, utf8_decode('PUESTO: '.$puesto), 0,0,"C", False);
        $pdf->Ln(15);


        $consulta2 = "SELECT gi_justificacion_inasistencias.fecha, gi_justificacion_inasistencias.min_ent, gi_justificacion_inasistencias.min_sal, gi_justificacion_inasistencias.fuera_hent, gi_justificacion_inasistencias.fuera_hsal, gi_justificacion_inasistencias.estado, gi_justificacion_inasistencias.justificacion FROM gi_justificacion_inasistencias WHERE gi_justificacion_inasistencias.n_empleado = :cE AND gi_justificacion_inasistencias.informe_id = :cI";

        $query2 = $db->prepare($consulta2);
        $query2->bindParam(":cE",$nemp);
        $query2->bindParam(":cI",$inf);
        $query2->execute();

        /*while ($fila2 = $query2->fetch()){
            $nAula = $fila2['nAula'];

            $pdf->SetFont('Arial', 'B', 18);
            $pdf->Cell(0, 10, utf8_decode('AULA -  '.$nAula.'  EDIFICIO - '.$nEdif), 0,0,"C");
            $pdf->SetTextColor(255,255,255);  // Establece el color del texto (en este caso es blanco)
            $pdf->SetFillColor(0, 134, 179); // establece el color del fondo de la celda (en este caso es AZUL*/
            $pdf->SetFont('Arial', 'B', 16);
            //Encabezados de la tabla
            $pdf->Ln(10);
            $pdf->Cell(2);
            $pdf->Cell(25, 10, utf8_decode("DÍA"), "T,B",0,"C", True);
            $pdf->Cell(25, 10, utf8_decode("FECHA"), "T,B",0,"C", True);
            $pdf->Cell(30, 10, utf8_decode("MIN. ENTRADA"), "T,B",0,"C", True);
            $pdf->Cell(30, 10, utf8_decode("MIN. SALIDA"), "T,B",0,"C", True);
            $pdf->Cell(30, 10, utf8_decode("FUERA HORARIO DE ENTRADA"), "T,B",0,"C", True);
            $pdf->Cell(30, 10, utf8_decode("FUERA HORARIO DE SALIDA"), "T,B",0,"C", True);
            $pdf->Cell(20, 10, utf8_decode("ESTADO"), "T,B",0,"C", True);
            $pdf->Cell(30, 10, utf8_decode("OBSERVACIONES"), "T,B",0,"C", True);
            $pdf->Ln(8);
            $pdf->SetFont('Arial', 'B', 14);
            $pdf->SetTextColor(0,0,0);
            $pdf->Ln(1);

            /*$sql = "CALL SP_CA_DISTRIBUCION_AULAS_EDIFICIO(?,?,?,?)";
            $query3 = $db->prepare($sql);
            $query3->bindParam(1,$varPeriodo,PDO::PARAM_INT);
            $query3->bindParam(2,$varAnio,PDO::PARAM_INT);
            $query3->bindParam(3,$nEdif,PDO::PARAM_STR);
            $query3->bindParam(4,$nAula,PDO::PARAM_INT);
            $query3->execute();

            while ($ordenados = $query3->fetch()){
                $altura = 10;
                //$query3->closeCursor();

                $sqldias = "SELECT ca_secciones_dias.id_dia FROM ca_secciones_dias WHERE ca_secciones_dias.id_seccion = :cSE;";
                $resultado = $db->prepare($sqldias);
                $resultado->bindParam(":cSE",$ordenados['id_seccion']);
                $resultado->execute();
                $resultado->closeCursor();
                $dias = $resultado->rowCount();
                if($dias == 2){
                    $cadena = "(";
                    while($filas = $resultado->fetch()){
                        if($filas['id_dia']==1){
                            $cadena = $cadena."Lu";
                        }elseif($filas['id_dia']==2){
                            $cadena = $cadena."Ma";
                        }elseif($filas['id_dia']==3){
                            $cadena=$cadena."Mi)";
                        }elseif($filas['id_dia']==4){
                            $cadena=$cadena."Ju)";
                        }
                    }
                }else{
                    $cadena="";
                }

                $pdf->Cell(2);
                $pdf->CellFitSpace(50, $altura, "De ".hora(date("H",strtotime($ordenados["Hora_inicio"])))." a ".hora(date("H",strtotime($ordenados["Hora_fin"]))), "B",0,"C");
                $pdf->CellFitScale(110, $altura, utf8_decode($ordenados["nombreE"]), "B",0,"L");
                $pdf->CellFitSpace(25, $altura, utf8_decode($ordenados["id_asignatura"]), "B",0,"C");
                $pdf->CellFitScale(125, $altura, utf8_decode($ordenados["nAsignatura"])." ".$cadena, "B",0,"C");
                $pdf->Ln();

                //$query3->openCursor();
                
            }*/

            $pdf->Ln(15);

        //}
    }
    

    $pdf->Output('Oficio de Justificaciones - NOMBREDELMES.pdf','I');


?>