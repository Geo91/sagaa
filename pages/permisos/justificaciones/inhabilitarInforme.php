<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    if(isset($_POST['id'])) {

		$pcSalon = $_POST['id'];

        try{
			$consulta=$db->prepare("CALL SP_GI_INHABILITAR_INFORME(?,@pcMensajeError)");
			$consulta->bindParam(1, $pcSalon, PDO::PARAM_INT);

			$resultado=$consulta->execute();
			$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
			$mensaje = $output['@pcMensajeError'];

			if ($mensaje == NULL){
			    echo mensajes('El informe ha sido inhabilitado exitósamente', 'verde');
			}else{
			    echo mensajes('Error!'.$mensaje, 'rojo');
			}
		}catch(PDOexception $e){
			echo mensajes('Error!'. $e, 'rojo');
		}
	}else{
		echo mensajes("Hubo un error al intentar inhabilitar el informe seleccionado.", "rojo");
	}

?>

<script type="text/javascript">
    $("#TablaInforme").load('pages/permisos/justificaciones/datosInformes.php'); 
</script>