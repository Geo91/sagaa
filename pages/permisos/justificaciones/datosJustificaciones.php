<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");
    require_once("../ChekAutoIncrement.php");
    $idUsuario = $_SESSION['user_id'];

    function dia($fecha){
        switch (date('l', strtotime(($fecha)))) {
            case 'Monday':
                $dia = 'Lunes';
                break;
            case 'Tuesday':
                $dia = 'Martes';
                break;
            case 'Wednesday':
                $dia = 'Miércoles';
                break;
            case 'Thursday':
                $dia = 'Jueves';
                break;
            case 'Friday':
                $dia = 'Viernes';
                break;
            case 'Saturday':
                $dia = 'Sábado';
                break;
            case 'Sunday':
                $dia = 'Domingo';
                break;
        }
        return $dia;
    }
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <!--<section class="content">-->
                <div class="table-responsive">
                    <table id= "TablaDatosJustificaciones" border="0" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th style = "text-align:center;background-color:#386D95;color:white;display:none">ID</th>
                                <th style = "text-align:center;background-color:#386D95;color:white;">Día</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Fecha</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Min. E</th>
                                <th style = "text-align:center;background-color:#386D95;color:white;">Min. S</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Fuera H. Ent</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Fuera H. Sal</th>
                                <th style = "text-align:center;background-color:#386D95;color:white;">Justificación</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Estado</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Editar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Contenido de la tabla generado atravez de la consulta a 
                                la base de datos -->
                                <?php
                                    try{
                                        $proc = "CALL SP_GI_DATOS_JUSTIFICACIONES(?)";
                                        $queryproc = $db->prepare($proc);
                                        $queryproc->bindParam(1,$idUsuario,PDO::PARAM_INT);

                                        $queryproc->execute();
                                        while ($row = $queryproc->fetch()) {
                                ?>
                                        <tr data-id='<?php echo $row["id_justificacion"]; ?>' data-fecha='<?php echo $row["fecha"]; ?>' data-mine='<?php echo $row["min_ent"];?>' data-mins='<?php echo $row["min_sal"];?>' data-fhe='<?php echo $row["fuera_hent"];?>' data-fhs='<?php echo $row["fuera_hsal"];?>' data-justificacion='<?php echo $row["justificacion"];?>'>
                                            <td style="display:none"><?php echo $row["id_justificacion"]; ?></td>
                                            <td><?php echo dia($row["fecha"]); ?></td>
                                            <td><?php echo $row["fecha"]; ?></td>
                                            <td><?php echo $row["min_ent"]; ?></td>
                                            <td><?php echo $row["min_sal"]; ?></td>
                                            <td><?php echo $row["fuera_hent"]; ?></td>
                                            <td><?php echo $row["fuera_hsal"]; ?></td>
                                            <td><?php echo $row["justificacion"]; ?></td>
                                            <td><?php echo $row["estado"]; ?></td>
                                            <td>
                                                <center>
                                                <div class = "btn-group-vertical btn-group-sm">
                                                    <button type="button" class="editarJustificacion<?php echo $auto_increment; ?> btn btn-info" title="Editar la justificación." <?php if($row["estado"] == 'VB' || $row["estado"] == 'Justificado'){ echo "disabled = 'true'"; } ?>><span class="glyphicon glyphicon-edit"></span></button>
                                                    <button type="button" class="eliminarJustificacion<?php echo $auto_increment; ?> btn btn-danger" title="Editar la justificación." <?php if($row["estado"] == 'VB' || $row["estado"] == 'Justificado'){ echo "disabled = 'true'"; } ?>><span class="glyphicon glyphicon-trash"></span></button>
                                                </div>
                                                </center>
                                            </td>    
                                        </tr>
                                <?php
                                        } //cierre del ciclo while para llenar la tabla de datos
                                    }catch(PDOException $e){
                                        echo "Error: ".$e;
                                    }
                                ?>
                        </tbody>
                    </table>       
                </div>
            <!--</section>-->
        </div>                
    </div>
</div>

<script type="text/javascript">   
//opciones para buscador en la tabla
    $('#TablaDatosJustificaciones')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#TablaDatosJustificaciones').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {

        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });


    $(document).on("click", ".editarJustificacion<?php echo $auto_increment; ?>", function () {  
        data = {
            idJ: $(this).parents("tr").data("id"),
            fecha: $(this).parents("tr").data("fecha"),
            ment: $(this).parents("tr").data("mine"),
            msal: $(this).parents("tr").data("mins"),
            fhent: $(this).parents("tr").data("fhe"),
            fhsal: $(this).parents("tr").data("fhs"),
            justif: $(this).parents("tr").data("justificacion")
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: formeditarJ,
            timeout: 12000,
            error: problemas
        }); 
        return false; 
    });

    $(document).on("click", ".eliminarJustificacion<?php echo $auto_increment; ?>", function () {
        var respuesta = confirm("¿Esta seguro que desea eliminar esta justificación? \n\n Una vez eliminado, el registro no podrá recuperarse.");
        
        if (respuesta){ 
            data = {
                idJ: $(this).parents("tr").data("id")
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                success: eliminarJ,
                timeout: 12000,
                error: problemas
            }); 
            return false; 
        }
    });

</script>
