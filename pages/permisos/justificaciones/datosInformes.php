<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");
    require_once("../ChekAutoIncrement.php");
    $idUsuario = $_SESSION['user_id'];

    $sql = "SELECT COUNT(gi_administradores.id_administrador) FROM gi_administradores WHERE gi_administradores.n_empleado IN (SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario = :cU)";
    $query = $db->prepare($sql);
    $query->bindParam(":cU",$idUsuario);
    $query->execute();
    $conteo = $query->rowCount();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <!--<section class="content">-->
                <div class="table-responsive">
                    <table id= "TablaInformes" border="0" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th style = "text-align:center;background-color:#386D95;color:white;display:none">ID</th>
                                <th style = "text-align:center;background-color:#386D95;color:white;">Mes</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Año</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Estado</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Operaciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Contenido de la tabla generado atravez de la consulta a 
                                la base de datos -->
                                <?php
                                    try{
                                        $sql = "CALL SP_GI_DATOS_INFORMES(?)";
                                        $query = $db->prepare($sql);
                                        $query->bindParam(1,$idUsuario,PDO::PARAM_INT);

                                        $query->execute();
                                        while ($fila = $query->fetch()) {
                                ?>
                                        <tr data-id='<?php echo $fila["id_informe"]; ?>' data-doc='<?php echo $fila["documento_informe"]; ?>' >
                                            <td style="display:none"><?php echo $fila["id_informe"]; ?></td>
                                            <td><?php echo $fila["mes"]; ?></td>
                                            <td><?php echo $fila["anio"]; ?></td>
                                            <td><?php echo $fila["estado"]; ?></td>
                                            <td>
                                                <center>
                                                <div>
                                                    <button type="button" class="verDocumento<?php echo $auto_increment; ?> btn btn-warning"  title="Ver el reporte de inasistencias de este mes."><span class="glyphicon glyphicon-eye-open"></span></button>
                                                    <button type="button" class="deshabilitarInforme<?php echo $auto_increment; ?> btn btn-danger" title="Deshabilitar las justificaciones para este reporte." <?php if($conteo == 0 || $fila["estado"] == 'Inhabilitado'){ echo "disabled = 'true'"; } ?>><span class="  glyphicon glyphicon-ban-circle"></span></button>
                                                    <button type="button" class="generarOficio<?php echo $auto_increment; ?> btn btn-danger" title="Generar oficio de justificación de marcaciones." style="border-color:red;background-color:white"><i class="fa fa-file-pdf-o" style="color:red"></i></button>
                                                </div>
                                                </center>
                                            </td>             
                                        </tr>
                                <?php 
                                        } //cierre del ciclo while para llenar la tabla de datos
                                    }catch(PDOException $e){
                                        echo "Error: ".$e;
                                    }
                                ?>
                        </tbody>
                    </table>       
                </div>
            <!--</section>-->
        </div>                
    </div>
</div>

<script type="text/javascript">   
//opciones para buscador en la tabla
    $('#TablaInformes')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#TablaInformes').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {

        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });


    $(document).on("click", ".verDocumento<?php echo $auto_increment; ?>", function () {  

        data = {
            arch: $(this).parents("tr").data("doc"),
            tp: 2
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: abrirInforme,
            timeout: 8000,
            error: problemas
        }); 

        return false;
    });

    $(document).on("click", ".deshabilitarInforme<?php echo $auto_increment; ?>", function () {   
        var respuesta = confirm("¿Esta seguro que desea cambiar el estado del salón seleccionado? \n Una vez inhabilitado, el informe no podrá volver a habilitarse.");
        
        if (respuesta){ 
            data = {
                id: $(this).parents("tr").data("id")
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                success: deshabilitarI,
                timeout: 10000,
                error: problemas
            }); 
            return false; 
        }
    });

    $(document).on("click", ".generarOficio<?php echo $auto_increment; ?>", function () {   
        data = {
            idI: $(this).parents("tr").data("id")
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: generarO,
            timeout: 10000,
            error: problemas
        }); 
        return false;         
    });

</script>

<script type="text/javascript" src="pages/permisos/justificaciones/ScriptInforme.js"></script>