<?php
	$maindir = "../../../";
	require_once($maindir."conexion/config.inc.php");
	require_once($maindir."Datos/funciones.php");

	session_start();
	//$idUsuario = $_SESSION['user_id'];

	$pCod = $_POST['idJ'];

	try{
		$consulta=$db->prepare("CALL SP_GI_ELIMINAR_JUSTIFICACIONES(?,@pcMensajeError)");
		$consulta->bindParam(1, $pCod, PDO::PARAM_INT);

		$resultado=$consulta->execute();
		$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
		$mensaje = $output['@pcMensajeError'];

		if ($mensaje == NULL){
		    echo mensajes('La justificación fue eliminada exitosamente','verde');
		}else{
			echo mensajes("Error! ". $mensaje, "rojo");
		}
	}catch(PDOExecption $e){
        echo mensajes("Error: ".$e, "rojo");
    }

    $consulta->closeCursor();

?>

<script type="text/javascript">
    $("#TablaJustificaciones").load('pages/permisos/justificaciones/datosJustificaciones.php');
    $('#formJustificaciones').trigger("reset");
</script>