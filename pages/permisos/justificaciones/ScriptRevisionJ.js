
$("#formRevisarJustificacion").submit(function (e) {
    e.preventDefault();

    //Se recopila la información que se enviara a la base de datos
    data = {
        vNE: $("#empleado").val(),
        vInf: $("#vinforme").val()
    };

    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        beforeSend: inicioEnvio,
        success: revisarTodo,
        timeout: 10000,
        error: problemas
    });

    return false;
});

function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor. Favor contactar al administrador del sistema.');
}

function justificacionRevisada(){
	$("#divRespuestaM").empty();
    $("#divRespuestaM").load('pages/permisos/justificaciones/justificacionRevisada.php', data);
}

function deshabilitarI(){
	$("#divRespuesta").empty();
    $("#divRespuesta").load('pages/permisos/justificaciones/inhabilitarInforme.php', data);           
}

function verJ(){
    $("#contenidoModal").load("pages/permisos/justificaciones/datosJustifEmp.php",data);
    $("#modalRevisionJ").modal("show");
}

function formeditarJ(){
    $("#contenidoModal").load('pages/permisos/justificaciones/formEditarJustificacion.php', data);
    $("#modalRevisionJ").modal("show");
}

function revisarTodo(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/permisos/justificaciones/revisarJustificacionesT.php', data);
    $("#modalRevisionJ").modal("hide");
}

function eliminarJ(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/permisos/justificaciones/eliminarJustificacion.php', data);
}