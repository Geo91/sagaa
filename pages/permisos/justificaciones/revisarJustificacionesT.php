<?php

	$maindir = "../../../";
	require_once($maindir."conexion/config.inc.php");
	require_once($maindir."Datos/funciones.php");

	session_start();
	$idUsuario = $_SESSION['user_id'];
	$pNE = $_POST['vNE'];
	$pInf = $_POST['vInf'];

	try{
		$consulta=$db->prepare("CALL SP_GI_APROBAR_JUSTIFICACIONES_T(?,?,?,@pcMensajeError)");
		$consulta->bindParam(1, $pNE, PDO::PARAM_STR);
		$consulta->bindParam(2, $pInf, PDO::PARAM_INT);
		$consulta->bindParam(3, $idUsuario, PDO::PARAM_INT);

		$resultado=$consulta->execute();
		$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
		$mensaje = $output['@pcMensajeError'];

		if ($mensaje == NULL){
	   		echo mensajes('Las justificaciones fueron aprobadas exitosamente','verde');
		}else{
			echo mensajes("Error! ". $mensaje, "rojo");
		}
	}catch(PDOExecption $e){
        echo mensajes("Error: ".$e, "rojo");
    }

    $consulta->closeCursor();

?>

<script type="text/javascript">
	$("#TablaJustificaciones").load('pages/permisos/justificaciones/justificacionesEmpleados.php');
</script>