<?php  
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    if(isset($_POST['codPermiso'])){
        $id = $_POST['codPermiso'];
            
        try{
            $sql = "CALL SP_HABILITAR_PERMISO(?,@mensajeError)";
            $query1 = $db->prepare($sql);
            $query1->bindParam(1,$id,PDO::PARAM_INT);

            $query1->execute();
            $output = $db->query("select @mensajeError")->fetch(PDO::FETCH_ASSOC);
            $mensaje = $output['@mensajeError'];

            if ($mensaje == NULL){
                echo mensajes('El permiso ha sido habilitado para exportarse éxitosamente', 'verde');
            }else{
                echo mensajes('Error!'.$mensaje, 'rojo');
            }
        }catch(PDOexception $e){
            echo mensajes('Error!'. $e, 'rojo');
        }
    }else{
        echo mensajes("Hubo un error al intentar exportar la solicitud. Inténtelo de nuevo o contacte al administrador del sistema", "amarillo");
    }
    
?>

<script type="text/javascript">
    $("#TablaSolicitudesExportadas").load('pages/permisos/revision/DatosSolicitudesExportadas.php');
</script>
