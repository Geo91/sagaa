<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir."conexion/config.inc.php");

    $pcPermiso = $_POST['codigo'];

?>


<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3 class="modal-title" style="color:white">Exportar Formato de Solicitud de Vacaciones</h3>
</div>

<div class="modal-body">

	<div>
	    <div id= "noti1" class="alert alert-info" role="alert"><center>Por favor ingrese el número de oficio con que se generará el formato de solicitud de vacaciones.</center></div>
	</div>

	<form class="form-horizontal" role="form" id="formExportar" name="formExportar">
		<div class="form-group col-sm-12" style="display:none">
		    <label>Código Permiso</label>
				<input id="idGestion" value = "<?php echo $pcPermiso; ?>" class="form-control" required disabled>
		</div>

		<div class="form-group col-sm-12">
		    <label>Número de Oficio</label>
		    <input id="numF" placeholder = "Oficio FCJ-D-XXX/XXXX" class="form-control">
		</div>

<br>
<br>

		<div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal"> Volver</button>
		    <button type = "submit" id="exportarFormato" class="btn btn-primary"> Continuar</button>
		</div>
	</form>
</div>

<script type="text/javascript" src="pages/permisos/revision/ScriptRevision.js"></script>