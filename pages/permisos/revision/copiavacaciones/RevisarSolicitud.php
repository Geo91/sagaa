
<div id = "notificaciones1"></div>

<!-- Seccion usada para mostrar la tabla de las solicitudes de permiso que estan pendientes de revisión en el sistema -->
<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Solicitudes de Permiso Pendientes </label>
        </div>
        <div class="panel-body">
            <div>
                <div id= "noti1" class="alert alert-info" role="alert"><center>Por favor seleccione la solicitud que desee revisar</center></div>
            </div>

            <div id = "TablaSolicitudes">
                <?php include("DatosSolicitudes.php"); ?>
            </div>   
                
        </div>
    </div>
</div>

<div class="col-lg-12">
<br>
</div>

<div id = "notificaciones2"></div>

<!-- Seccion usada para mostrar la tabla de las gestiones de vacaciones que estan pendientes de revisión en el sistema -->
<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Gestiones de Vacaciones Pendientes </label>
        </div>
        <div class="panel-body">
            <div>
                <div id= "noti1" class="alert alert-info" role="alert"><center>Por favor seleccione la gestión que desee evaluar</center></div>
            </div>

            <div id = "TablaGestionesV">
                <?php include("datosGestiones.php"); ?>
            </div>   
                
        </div>
    </div>
</div>

<!-- Modal para revisar las solicitudes de permiso -->
<div class="modal fade" id="modalSolicitud" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div id="contenidoModal">
        </div>
      </div>
    </div>
</div>

<!-- Modal para revisar las gestiones de vacaciones -->
<div class="modal fade" id="modalGestion" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div id="contenidoModalGestion">
        </div>
      </div>
    </div>
</div>

<!-- Modal para revisar las solicitudes de permiso -->
<div class="modal fade" id="modalDoc" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div id="contenidoModalDoc">
        </div>
      </div>
    </div>
</div>


<script type="text/javascript" src="pages/permisos/revision/ScriptRevision.js"></script>