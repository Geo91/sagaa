<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    $pcPermiso = $_POST['id'];
    $pcEmpleado = $_POST['empleado'];

    $query = "SELECT permisos.id_Permisos, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) as 'Empleado', departamento_laboral.nombre_departamento, vacaciones.año_actual, vacaciones.dias_año_actual, vacaciones.año_anterior, vacaciones.dias_año_anterior, vacaciones.total_dias, vacaciones.fecha_Inicio, vacaciones.fecha_fin, permisos.documento from permisos, persona, empleado, departamento_laboral,vacaciones WHERE permisos.No_Empleado=empleado.No_Empleado AND empleado.N_identidad=persona.N_identidad AND permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.id_Permisos=vacaciones.id_solicitud AND permisos.id_Permisos = '".$pcPermiso."'";

    $consulta = "SELECT vacaciones_empleado.año, vacaciones_empleado.dias, vacaciones_empleado.disponibilidad, vacaciones_empleado.caducidad from vacaciones_empleado where vacaciones_empleado.empleado_id = '$pcEmpleado' and vacaciones_empleado.caducidad > NOW() ORDER BY vacaciones_empleado.año DESC LIMIT 1";

    $consulta2 = "SELECT vacaciones_empleado.año, vacaciones_empleado.dias, vacaciones_empleado.disponibilidad, vacaciones_empleado.caducidad from vacaciones_empleado where vacaciones_empleado.empleado_id = '$pcEmpleado' and vacaciones_empleado.caducidad > NOW() ORDER BY vacaciones_empleado.año ASC LIMIT 1";

?>

<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3 class="modal-title" style="color:white">Información detallada de la gestión</h3>
</div>

<div class="modal-body">

	<form class="form-horizontal" role="form" id="formVisualizarGestion" name="formVisualizarGestion">

		<div class="panel panel-info"> <!--***********INFORMACIÓN DE LA GESTIÓN****************-->

	        <div class="panel-heading"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Información de la Gestión</div>

	        <div class="panel-body" >
	            <div class="row">
	                <!--************************Datos*********************-->
	                <div class="col-lg-12">

						<?php
					        $result =$db->prepare($query);
					        $result->execute();
					        while ($fila = $result->fetch()) {
					    ?>
							    <div class="form-group" hidden="true">
							        <label style="display : none">Código del Permiso</label>
							        <input style="display : none" id="codPermiso" disabled = "true" data-id="<?php echo $fila['id_Permisos']; ?>" class="form-control">
							    </div>

							    <div class="row">
							        <label class = "col-sm-8 control-label" style="text-align: left">Empleado: <?php echo $fila["Empleado"]; ?></label>
							    </div>

							    <div class="row">
							        <label class = "col-sm-12 control-label" style="text-align: left">Departamento: <?php echo "  ".$fila["nombre_departamento"]; ?></label>
							    </div>

							    <div class="row">
							        <label class = "col-sm-6 control-label" style="text-align: left">Año Actual: <?php echo "  ".$fila["dias_año_actual"]." días / ".$fila["año_actual"]; ?></label>
							    </div>

							    <div class="row">
							        <label class = "col-sm-6 control-label" style="text-align: left">Año Anterior: <?php echo "  ".$fila["dias_año_anterior"]." días / ".$fila["año_anterior"]; ?></label>
							    </div>

							    <div class="row">
							        <label class="col-sm-6 control-label" style="text-align: left">Total días solicitados: <?php echo "  ".$fila["total_dias"]; ?></label>
							    </div>

							    <div class="row">
							        <label class="col-sm-6 control-label" style="text-align: left">Fecha de Inicio: <?php echo "  ".$fila["fecha_Inicio"]; ?></label>
							    </div>

							    <div class="row">
							        <label class="col-sm-6 control-label" style="text-align: left">Fecha de Finalización: <?php echo "  ".$fila["fecha_fin"]; ?></label>
							    </div>
							    <div class="form-group" hidden="true">
							        <label style="display : none">Documento</label>
							        <input style="display : none" id="docu" disabled = "true" value="<?php $dcmt=$fila['documento']; echo $dcmt; ?>" class="form-control">
							    </div>
					    <?php 
					    	}
						?>
						<!--</form>-->
					</div>
				</div>
			</div>
		</div> <!--***********INFORMACIÓN DE LA GESTIÓN****************-->

		<div class="panel panel-info"> <!--***********INFORMACIÓN DE LAS VACACIONES****************-->

		    <div class="panel-heading"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Vacaciones Asignadas</div>

		    <div class="panel-body" >
		        <div class="row">
		            <!--************************Datos*********************-->
		            <div class="col-lg-12">
		            
		                <?php
		                    $resultado = $db->prepare($consulta);
		                    $resultado->execute();
		                    while ($row = $resultado->fetch()) {
		                ?> 
		                    <div class="row" >                    
		                        <label class=" col-sm-4 control-label" style="text-align: left">Año Actual: <?php echo $row["año"]; ?></label>
		                    </div>                     
		                    <div class="row">                          
		                        <label class=" col-sm-4" style="text-align: left"> Días: <?php echo $row["dias"]; ?></label>
		                    </div>
		                    <div class="row">                          
		                        <label class=" col-sm-6" style="text-align: left"> Disponibles desde: <?php echo $row["disponibilidad"]; ?></label>
		                    </div>
		                <?php
		                    }
		                ?>
					<hr>
		                <?php
		                    $resultado2 = $db->prepare($consulta2);
		                    $resultado2->execute();
		                    while ($fila2 = $resultado2->fetch()) {
		                ?> 
		                    <div class="row" >
		                        <label class=" col-sm-4 control-label" style="text-align: left">Año Anterior: <?php echo $fila2["año"]; ?></label>
		                    </div>                     
		                    <div class="row">
		                        <label class=" col-sm-4" style="text-align: left"> Días: <?php echo $fila2["dias"]; ?></label>
		                    </div>
		                    <div class="row">
		                        <label class=" col-sm-6" style="text-align: left"> Disponibles desde: <?php echo $fila2["disponibilidad"]; ?></label>
		                    </div>
		                <?php
		                    }
		                ?>

		            </div>   
		            <!--************************Datos*********************-->
		        </div>
		    </div>
		</div> <!--***********INFORMACIÓN DE LAS VACACIONES****************-->

		<div class="modal-footer">
			<button type="button"  class="btn btn-default" data-dismiss="modal">Volver</button>
			<button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" <?php if($dcmt==NULL){ echo "disabled='true'"; } ?> ><span class="glyphicon glyphicon-folder-open"></span>  Documento </button>
		</div>
	</form>

</div>

<script type="text/javascript">

$(document).ready(function(){

    $("#formVisualizarGestion").submit(function(e) {

        var dcP = $("#docu").val();

        data = {
        	arch: dcP,
        	tp: 1
        }
        $.ajax({
	        async: true,
	        type: "POST",
	        dataType: "html",
	        contentType: "application/x-www-form-urlencoded",
	        success: abrirDoc,
	        timeout: 8000,
	        error: problemas
	    }); 

	    return false;

    });

});

</script>

<script type="text/javascript" src="pages/permisos/revision/ScriptRevision.js"></script>