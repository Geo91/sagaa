<?php
	$maindir = "../../../";
	require_once("../ChekAutoIncrement.php");
	require_once($maindir."conexion/config.inc.php");

	$idUsuario = $_SESSION["user_id"];
	$rol = $_SESSION["user_rol"];

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <!--<section class="content">-->
                <div class="table-responsive">
                    <table id= "TablaGestiones" border="0" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th style = 'display:none'>Codigo</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th> 
                                <th style="text-align:center;background-color:#386D95;color:white;">Departamento</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Seleccionar</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Aprobar</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Denegar</th>
                                <?php
                                	if($rol == 50 or $rol == 55){
                                		echo "<th style='text-align:center;background-color:#386D95;color:white;'>Exportar</th>";
                                	}
                                ?>           
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Contenido de la tabla generado atravez de la consulta a 
                                la base de datos -->
                                <?php
                                    try{
                                        $sql = "CALL SP_DATOS_GESTIONES_VACACIONES(?,?)";
                                        $query = $db->prepare($sql);
                                        $query->bindParam(1,$idUsuario,PDO::PARAM_INT);
                                        $query->bindParam(2,$rol,PDO::PARAM_INT);

                                        $query->execute();
                                        while ($fila = $query->fetch()) {
                                ?>
                                        <tr data-id='<?php echo $fila["id_Permisos"]; ?>' data-emp='<?php echo $fila["No_Empleado"]; ?>' data-estado='<?php echo $fila['estadoPermiso'] ?>'>
                                            <td style = "display:none"><?php echo $fila["id_Permisos"]; ?></td>
                                            <td><?php echo $fila["Empleado"]; ?></td>
                                            <td><?php echo $fila["nombre_departamento"]; ?></td>
                                            <td>
                                                <center>
                                                    <button type="button" class="verGestion<?php echo $auto_increment; ?> btn btn-warning glyphicon glyphicon-share-alt"  title="Ver información detallada de la gestión."></button>
                                                </center>
                                            </td>
                                            <td>
                                                <center>
                                                    <button type="button" class="aprobarGestion<?php echo $auto_increment; ?> btn btn-success glyphicon glyphicon-thumbs-up"  title="Aprobar la gestión de vacaciones."></button>
                                                </center>
                                            </td>     
                                            <td>
                                                <center>
                                                    <button type="button" class="denegarGestion<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-thumbs-down"  title="Denegar la gestión de vacaciones."></button>
                                                </center>
                                            </td>
                                            <?php
                                            	if($rol == 50 or $rol == 55){
                                            		echo "<td>
			                                                <center>
			                                                    <button type='button' class='exportar<?php echo $auto_increment; ?> btn title='Exportar formato de vacaciones.' style='background-color:white;border-color:red;'><i class='fa fa-file-pdf-o'></i></button>
			                                                </center>
			                                            </td>";
                                            	}
                                            ?>     
                                        </tr>
                                <?php 
                                        } //cierre del ciclo while para llenar la tabla de datos
                                    }catch(PDOException $e){
                                        echo "Error: ".$e;
                                    }
                                ?>
                        </tbody>
                    </table>       
                </div>
            <!--</section>-->
        </div>                
    </div>
</div>

<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#TablaGestiones')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#TablaGestiones').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });

    $(document).on("click", ".verGestion<?php echo $auto_increment; ?>", function () {
	    data={
	        id:$(this).parents("tr").data("id"),
	        empleado:$(this).parents("tr").data("emp")
	    }
	    $.ajax({
	        async: true,
	        type: "POST",
	        dataType: "html",
	        contentType: "application/x-www-form-urlencoded",
	        success: abrirGestion,
	        timeout: 6000,
	        error: problemas
	    }); 
	    return false;
	});

    $(document).on("click", ".aprobarGestion<?php echo $auto_increment; ?>", function () {
	    data={
	        id:$(this).parents("tr").data("id"),
	        empleado:$(this).parents("tr").data("emp")
	    }
	    $.ajax({
	        async: true,
	        type: "POST",
	        dataType: "html",
	        contentType: "application/x-www-form-urlencoded",
	        beforeSend: inicioEnvio,
	        success: aprobarGestion,
	        timeout: 6000,
	        error: problemas
	    }); 
	    return false;
	    
	});

	$(document).on("click", ".denegarGestion<?php echo $auto_increment; ?>", function () {
	    data={
	        id:$(this).parents("tr").data("id"),
	        empleado:$(this).parents("tr").data("emp")
	    }
	    $.ajax({
	        async: true,
	        type: "POST",
	        dataType: "html",
	        contentType: "application/x-www-form-urlencoded",
	        beforeSend: inicioEnvio,
	        success: denegarGestion,
	        timeout: 6000,
	        error: problemas
	    }); 
	    return false;
	});

	$(document).on("click", ".exportar<?php echo $auto_increment; ?>", function () {
    var estado = $(this).parents("tr").data("estado");
    var respuesta = confirm("El formato de vacaciones solo puede ser exportado una vez ¿Está seguro que desea continuar?");
    if (respuesta){
	    if(estado === 'Aprobado'){
	    	data={
		        codigo:$(this).parents("tr").data("id")
		    }
		    $.ajax({
		        async: true,
		        type: "POST",
		        dataType: "html",
		        contentType: "application/x-www-form-urlencoded",
		        success: exportarGestion,
		        timeout: 6000,
		        error: problemas
		    }); 
		    return false;
	    }else{
	    	alert("La gestión no ha sido aprobada, no puede exportarse");
	    }
	}    
});

</script>