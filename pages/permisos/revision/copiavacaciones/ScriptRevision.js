
$(document).ready(function(){

    $("#formAprobar").submit(function(e) {
        e.preventDefault();
        data={
            codigo:$('#idPermiso').val(),
            state:$('#estado').val(),
            obs:$('#observacion').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: solicitudAprobada,
            timeout: 6000,
            error: problemas
        }); 
        return false;
    });

    $("#formDenegar").submit(function(e) {
        e.preventDefault();
        data={
            codigo:$('#idPermiso').val(),
            obs:$('#observacion').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: solicitudDenegada,
            timeout: 6000,
            error: problemas
        }); 
        return false;
    });

    $("#formAprobarGestion").submit(function(e) {
        e.preventDefault();
        data={
            codigo:$('#idGestion').val(),
            state:$('#estadoG').val(),
            obs:$('#obsG').val(),
            empleado:$('#emp').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: gestionAprobada,
            timeout: 6000,
            error: problemas
        }); 
        return false;
    });

    $("#formDenegarGestion").submit(function(e) {
        e.preventDefault();
        data={
            codigo:$('#idGestion').val(),
            obs:$('#obsG').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: gestionDenegada,
            timeout: 6000,
            error: problemas
        }); 
        return false;
    });

    $("#formDenegarGestion").submit(function(e) {
        e.preventDefault();
        data={
            codigo:$('#idGestion').val(),
            obs:$('#obsG').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: gestionDenegada,
            timeout: 6000,
            error: problemas
        }); 
        return false;
    });
    
});

function abrirSolicitud(){
    $("#contenidoModal").load('pages/permisos/revision/verSolicitud.php',data);
    $("#modalSolicitud").modal('show');
}

function aprobarSolicitud(){
    $("#contenidoModal").load('pages/permisos/revision/aprobarSolicitud.php',data);
    $("#modalSolicitud").modal('show');
}

function denegarSolicitud(){
    $("#contenidoModal").load('pages/permisos/revision/denegarSolicitud.php',data);
    $("#modalSolicitud").modal('show');
}

function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function solicitudAprobada(){
    $("#notificaciones1").empty();
    $("#notificaciones1").load('pages/permisos/revision/solicitudAprobada.php',data);
    $("#modalSolicitud").modal('hide');
}

function solicitudDenegada(){
    $("#notificaciones1").empty();
    $("#notificaciones1").load('pages/permisos/revision/solicitudDenegada.php',data);
    $("#modalSolicitud").modal('hide');
}

function abrirGestion(){
    $("#contenidoModalGestion").load('pages/permisos/revision/verGestion.php',data);
    $("#modalGestion").modal('show');
}

function aprobarGestion(){
    $("#contenidoModalGestion").load('pages/permisos/revision/aprobarGestion.php',data);
    $("#modalGestion").modal('show');
}

function denegarGestion(){
    $("#contenidoModalGestion").load('pages/permisos/revision/denegarGestion.php',data);
    $("#modalGestion").modal('show');
}

function gestionAprobada(){
    $("#notificaciones2").empty();
    $("#notificaciones2").load('pages/permisos/revision/gestionAprobada.php',data);
    $("#modalGestion").modal('hide');
}

function gestionDenegada(){
    $("#notificaciones2").empty();
    $("#notificaciones2").load('pages/permisos/revision/gestionDenegada.php',data);
    $("#modalGestion").modal('hide');
}

function abrirDoc(){
    $("#contenidoModalDoc").load("pages/permisos/revision/verDoc.php",data);
    $("#modalDoc").modal("show");
}