
$(document).ready(function(){

    $("#formAprobar").submit(function(e) {
        e.preventDefault();
        data={
            codigo:$('#idPermiso').val(),
            state:$('#estado').val(),
            obs:$('#observacion').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: solicitudAprobada,
            timeout: 10000,
            error: problemas
        }); 
        return false;
    });

    $("#formDenegar").submit(function(e) {
        e.preventDefault();
        data={
            codigo:$('#idPermiso').val(),
            obs:$('#observacion').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: solicitudDenegada,
            timeout: 10000,
            error: problemas
        }); 
        return false;
    });

    $("#formAprobarGestion").submit(function(e) {
        e.preventDefault();
        data={
            codigo:$('#idGestion').val(),
            state:$('#estadoG').val(),
            obs:$('#obsG').val(),
            empleado:$('#emp').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: gestionAprobada,
            timeout: 10000,
            error: problemas
        }); 
        return false;
    });

    $("#formDenegarGestion").submit(function(e) {
        e.preventDefault();
        data={
            codigo:$('#idGestion').val(),
            obs:$('#obsG').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: gestionDenegada,
            timeout: 10000,
            error: problemas
        }); 
        return false;
    });

    $("#formExportar").submit(function(e) {
        e.preventDefault();
        data={
            cdg:$('#idGestion').val(),
            nF:$('#numF').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: gestionExportada,
            timeout: 10000,
            error: problemas
        }); 
        return false;
    });
    
});

function abrirSolicitud(){
    $("#contenidoModal").load('pages/permisos/revision/verSolicitud.php',data);
    $("#modalSolicitud").modal('show');
}

function aprobarSolicitud(){
    $("#contenidoModal").load('pages/permisos/revision/aprobarSolicitud.php',data);
    $("#modalSolicitud").modal('show');
}

function denegarSolicitud(){
    $("#contenidoModal").load('pages/permisos/revision/denegarSolicitud.php',data);
    $("#modalSolicitud").modal('show');
}

function inicioEnvio(){
    $("#notificaciones1").empty();
    $("#notificaciones1").text('Cargando...');
}

function problemas(){
    $("#notificaciones1").text('Problemas en el servidor.');
}

function solicitudAprobada(){
    $("#notificaciones1").empty();
    $("#notificaciones1").load('pages/permisos/revision/solicitudAprobada.php',data);
    $("#modalSolicitud").modal('hide');
}

function solicitudDenegada(){
    $("#notificaciones1").empty();
    $("#notificaciones1").load('pages/permisos/revision/solicitudDenegada.php',data);
    $("#modalSolicitud").modal('hide');
}

function abrirGestion(){
    $("#contenidoModalGestion").load('pages/permisos/revision/verGestion.php',data);
    $("#modalGestion").modal('show');
}

function aprobarGestion(){
    $("#contenidoModalGestion").load('pages/permisos/revision/aprobarGestion.php',data);
    $("#modalGestion").modal('show');
}

function denegarGestion(){
    $("#contenidoModalGestion").load('pages/permisos/revision/denegarGestion.php',data);
    $("#modalGestion").modal('show');
}

function gestionAprobada(){
    $("#notificaciones1").empty();
    $("#notificaciones1").load('pages/permisos/revision/gestionAprobada.php',data);
    $("#modalGestion").modal('hide');
}

function gestionDenegada(){
    $("#notificaciones2").empty();
    $("#notificaciones2").load('pages/permisos/revision/gestionDenegada.php',data);
    $("#modalGestion").modal('hide');
}

function gestionExportada(){
    $("#notificaciones1").empty();
    $("#notificaciones2").load('pages/permisos/revision/formatoGestion.php',data);
    $("#modalGestion").modal('hide');
}

function abrirDoc(){
    $("#contenidoModalDoc").load("pages/permisos/revision/verDoc.php",data);
    $("#modalDoc").modal("show");
}

function exportarGestion(){
    $("#contenidoModalGestion").load("pages/permisos/revision/exportarFormato.php",data);
    $("#modalGestion").modal("show");
}