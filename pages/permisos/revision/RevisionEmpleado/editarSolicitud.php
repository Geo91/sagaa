<?php
    $maindir = "../../../../";
    require_once($maindir."conexion/config.inc.php");
    session_start();
    $pcPermiso = $_POST['id'];
    $idUsuario = $_SESSION['user_id'];
    $query = "SELECT permisos.id_Permisos, permisos.No_Empleado, permisos.id_Edificio_registro, permisos.id_tipo_permiso, permisos.id_motivo, permisos.fecha_solicitud, permisos.dias_permiso, permisos.hora_inicio, permisos.hora_finalizacion, permisos.justificacion FROM permisos where permisos.id_Permisos = '$pcPermiso' and (permisos.estado = 1 or permisos.estado = 2)";

    $query3 = "SELECT empleado.No_Empleado, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) as 'Jefe' from persona inner join empleado on persona.N_identidad=empleado.N_identidad where empleado.No_Empleado IN (SELECT empleado.jefe_inmediato from empleado where empleado.No_Empleado IN (SELECT usuario.No_Empleado from usuario where usuario.id_Usuario = '".$idUsuario."'))";

    $resultado = $db->prepare($query);
	$resultado -> execute();
	while ($row = $resultado->fetch()){
		$codigo = $row['id_Permisos'];
		$empleado = $row['No_Empleado'];
		$edificio = $row['id_Edificio_registro'];
		$tipo = $row['id_tipo_permiso'];
		$motivo = $row['id_motivo'];
		$fecha = $row['fecha_solicitud'];
		$dias = $row['dias_permiso'];
		$horaI = $row['hora_inicio'];
		$horaF = $row['hora_finalizacion'];
        $just = $row['justificacion'];
	}

?>


<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3 class="modal-title" style="color:white">Edición de la Solicitud</h3>
</div>
<div class="modal-body">

    <div >
        <div id= "noti1" class="col-lg-14 alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden<br>En caso de usar mozilla firefox el formato de fecha debe ser 'año-mes-día' </center>
        </div>
    </div>
	<form class="form-horizontal" role="form" id="formEditarSolicitud" name="formEditarSolicitud">

		<div class="row form-group" style="display:none">                    
		    <label class="col-sm-3 control-label">Codigo</label>                       
		    <div class="col-sm-2">
		            <input type="text" class="form-control" id="permiso" name="permiso" value = "<?php echo $codigo; ?>" required disabled>                         
		    </div>                  
		</div>

		<div class="row form-group" style="display:none">                    
		    <label class="col-sm-3 control-label">Empleado</label>                       
		    <div class="col-sm-2">
		            <input type="text" class="form-control" id="empleado" name="empleado" value = "<?php echo $empleado; ?>" required disabled>                         
		    </div>                  
		</div>

		<div class="row form-group">
            <label class = "col-sm-3 control-label">Solicito permiso por motivo de:</label>
            <div class = "col-sm-6">
                <select class = "form-control" id="motivo" name="motivo" required>
                    <option value = "NULL">Seleccione una opción</option>

                    <?php
                    	$query1 = "SELECT * FROM motivos;";
                        $result1 = $db->prepare($query1);
                        $result1->execute();
                        while ($fila1 = $result1->fetch()) {
                    ?> 
                            <option value="<?php echo $fila1['Motivo_ID']; ?>" <?php if($fila1['Motivo_ID'] == $motivo){ echo 'selected'; }?>> <?php echo $fila1["descripcion_motivo"]; ?></option>
                    <?php
                        }
                    ?>

                </select>
            </div>
        </div>

        <div class="row form-group">
            <label class=" col-sm-3 control-label">Edificio donde se registra</label>
            <div class="col-sm-6">                            
                <select class="form-control" id="edificio" name="edificio" required>
                    <option value="NULL">Seleccione una opción</option>
                    <?php
                        $query2 = "SELECT * from edificios;";
                        $result2 = $db->prepare($query2);
                        $result2->execute();
                        while ($fila2 = $result2->fetch()) {
                    ?> 
                            <option value="<?php echo $fila2['Edificio_ID'];?>" <?php if($fila2['Edificio_ID'] == $edificio){ echo 'selected'; }?>> <?php echo $fila2["descripcion"]; ?></option>
                    <?php
                        }
                    ?>
                </select>
            </div>                        
        </div>

        <div class="row form-group">
            <label class=" col-sm-3 control-label">Tipo de Permiso</label>
            <div class="col-sm-6">     
                <select class="form-control" id="tipoPermiso" name="tipoPermiso" required>
                    <option value="NULL">Seleccione una opción</option>
                    <?php
                        $query4 = "SELECT * from tipodepermiso where tipodepermiso.id_motivo = $motivo;";
                        $result4 = $db->prepare($query4);
                        $result4->execute();
                        while ($fila4 = $result4->fetch()) {
                    ?> 
                            <option value="<?php echo $fila4['id_tipo_permiso'];?>" <?php if($fila4['id_tipo_permiso'] == $tipo){ echo 'selected'; }?>> <?php echo $fila4["tipo_permiso"]; ?></option>
                    <?php
                        }
                    ?>
                </select>                         
            </div>
        </div>            
         
        <div class="row form-group">
            <label class=" col-sm-3 control-label">Jefe Inmediato</label>
            <div class="col-sm-6">                            
                    <?php
                        $result3 = $db->prepare($query3);
                        $result3->execute();
                        while ($fila3 = $result3->fetch()) {
                    ?> 
                            <input style = "display:none" type="text" class="form-control" id="jefe" name="jefe" value = "<?php echo $fila3['No_Empleado'] ?>" disabled="" hidden = "true">
                            <input type="text" class="form-control" id="njefe" name="njefe" value = "<?php echo $fila3['Jefe']; ?>" required disabled="">
                    <?php
                        }
                    ?>
            </div>
        </div>
                      
        <div class="row form-group">                            
            <label class="col-sm-3 control-label">Fecha</label>
            <div class="col-sm-2">
                <input type="date" id="fecha" name="datepicker" value = "<?php echo $fecha; ?>" required><span class="">
            </div>
        </div>

        <div class="row form-group">                            
            <label class="control-label col-sm-3">Días</label>
            <div class="col-sm-1">
                <input type="number" id="cantidad" name="cantidad" max="30" min="0" value = "<?php echo $dias; ?>" required><span class="">
            </div>
        </div>

        <div class="row form-group">                            
            <label class="control-label col-sm-3">Hora Inicio</label>
            <div class="col-sm-1">
                <input type="time" id="horaIn" name="horaIn" value = "<?php echo $horaI; ?>" required><span class="">
            </div>
        </div>

        <div class="row form-group">                            
            <label class="control-label col-sm-3">Hora Fin</label>
            <div class="col-sm-1">
                <input type="time" id="horaFi" name="horaFi" value = "<?php echo $horaF; ?>" required><span class="">
            </div>
        </div>

        <div class="row form-group">
            <label class="control-label col-sm-3"> Justificación </label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="info" name="info" value = "<?php echo $just; ?>" title = "Ingrese la justificación de su solicitud." required>
            </div>
        </div>
        
		<div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		    <button type = "submit" id="guardarCambios" class="btn btn-primary"> Guardar Cambios</button>
		</div>

        

	</form>
</div>

<script type="text/javascript" src="pages/permisos/revision/RevisionEmpleado/ScriptRevEmpleado.js"></script>