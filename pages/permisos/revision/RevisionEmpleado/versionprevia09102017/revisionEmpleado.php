
<div id = "notificaciones1"></div>

<!-- Seccion usada para mostrar la tabla de las solicitudes de permiso que estan pendientes de revisión en el sistema -->
<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Historial de Solicitudes </label>
        </div>
        <div class="panel-body">
            <div>
                <div id= "noti1" class="alert alert-info" role="alert"><center>A continuación se presentan las solicitudes realizadas este mes</center></div>
            </div>

            <div id = "TablaMisSolicitudes">
                <?php include("DatosSolicitudesEmpleado.php"); ?>
            </div>   
                
        </div>
    </div>
</div>

<!-- Modal para revisar las solicitudes de permiso -->
<div class="modal fade" id="modalEditarSolicitud" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div id="contenidoModal">
        </div>
      </div>
    </div>
</div>


<script type="text/javascript" src="pages/permisos/revision/RevisionEmpleado/ScriptRevEmpleado.js"></script>