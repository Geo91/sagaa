<?php  
    $maindir = "../../../../";
    require_once("../../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");
    $idUsuario = $_SESSION['user_id'];

    if(isset($_POST['codPermiso'])){
        $id = $_POST['codPermiso'];
        $correlativo = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
            
        try{
            $sql = "CALL SP_IMPRIMIR_PERMISO(?,?,?,@mensajeError)";
            $query1 = $db->prepare($sql);
            $query1->bindParam(1,$id,PDO::PARAM_INT);
            $query1->bindParam(2,$idUsuario,PDO::PARAM_INT);
            $query1->bindParam(3,$correlativo,PDO::PARAM_STR);

            $query1->execute();
            $output = $db->query("select @mensajeError")->fetch(PDO::FETCH_ASSOC);
            $mensaje = $output['@mensajeError'];

            if ($mensaje == NULL){
                echo mensajes('El Permiso ha sido exportado éxitosamente', 'verde');
                echo "<script type='text/javascript'>
                        window.open('pages/permisos/revision/crear_pdfpermiso.php?id='+$id);
                    </script>";
            }else{
                echo mensajes('Error!'.$mensaje, 'rojo');
            }
        }catch(PDOexception $e){
            echo mensajes('Error!'. $e, 'rojo');
        }
    }else{
        echo mensajes("Hubo un error al intentar exportar la solicitud. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
    }
    
?>

<script type="text/javascript">
    $("#TablaMisSolicitudes").load('pages/permisos/revision/RevisionEmpleado/DatosSolicitudesEmpleado.php');
</script>
