<?php
    $maindir = "../../../../";
    require_once("../../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    $idUsuario = $_SESSION['user_id'];

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <!--<section class="content">-->
                <div class="table-responsive">
                    <table id= "TablaPermisos" border="0" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th style = 'display:none'>Codigo</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Tipo de Permiso</th> 
                                <th style="text-align:center;background-color:#386D95;color:white;">Motivo</th>  
                                <th style="text-align:center;background-color:#386D95;color:white;">Fecha</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Horario</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Estado</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Observación</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Editar</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Documento</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Exportar</th>  
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Contenido de la tabla generado atravez de la consulta a 
                                la base de datos -->
                                <?php
                                    try{
                                        $sql = "CALL SP_DATOS_PERMISOS_PERSONALES(?)";
                                        $query = $db->prepare($sql);
                                        $query->bindParam(1,$idUsuario,PDO::PARAM_INT);

                                        $query->execute();
                                        while ($fila = $query->fetch()) {
                                ?>
                                        <tr data-id='<?php echo $fila["id_Permisos"]; ?>' data-emp='<?php echo $fila["No_Empleado"]; ?>' data-doc='<?php echo $fila["documento"]; ?>'>
                                            <td style = "display:none"><?php echo $fila["id_Permisos"]; ?></td>
                                            <td><?php echo $fila["tipo_permiso"]; ?></td>
                                            <td><?php echo $fila["descripcion_motivo"]; ?></td>
                                            <td><?php echo $fila["fecha_solicitud"]; ?></td>
                                            <td><?php echo $fila["hora_inicio"]." - ".$fila["hora_finalizacion"]; ?></td>
                                            <td><?php echo $fila["estadoPermiso"]; ?></td>
                                            <td><?php echo $fila["observacion"]; ?></td>
                                            <td>
                                                <center>
                                                    <button type="button" class="editarSolicitud<?php echo $auto_increment; ?> btn btn-info"  <?php if($fila['estadoPermiso']=='Aprobado'){ echo "disabled='true'"; } ?> title="Editar la solicitud de permiso."><i class="fa fa-pencil-square-o"></i></button>
                                                </center>
                                            </td> 
                                            <td>
                                                <center>
                                                <div class="btn-button-group">
                                                    <button type="button" class="subirDocumento<?php echo $auto_increment; ?> btn btn-success"  <?php if($fila['estadoPermiso']=='Aprobado'){ echo "disabled='true'"; } ?> title="Subir Documento Soporte."><i class="fa fa-cloud-upload" aria-hidden="true"></i></button>
                                                    <button type="button" style="background-color:#FE9A2E;color:white;border-color:#FE9A2E" class="verDocumento<?php echo $auto_increment; ?> btn btn-primary" <?php if($fila['documento']==NULL){ echo "disabled='true'"; } ?> title="Ver Documento"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                                </div>
                                                </center>
                                            </td>    
                                            <td>
                                                <center>
                                                    <button type="button" class="exportarSolicitud<?php echo $auto_increment; ?> btn btn-danger" title="Exportar la solicitud de permiso aprobado."><i class="fa fa-file-pdf-o"></i></button>
                                                </center>
                                            </td>     
                                        </tr>
                                <?php 
                                        } //cierre del ciclo while para llenar la tabla de datos
                                    }catch(PDOException $e){
                                        echo "Error: ".$e;
                                    }
                                ?>
                        </tbody>
                    </table>       
                </div>
            <!--</section>-->
        </div>                
    </div>
</div>

<script type="text/javascript">   
//opciones para buscador en la tabla 
    $('#TablaPermisos')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#TablaPermisos').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });

$(document).on("click", ".editarSolicitud<?php echo $auto_increment; ?>", function () {
    var estado = $(this).parents("tr").find("td").eq(5).html();
    if(estado === 'Aprobado' || estado === 'Preaprobado'){
    	alert("La solicitud ya ha sido revisada, no puede editarse");
    }else{
	    data={
	           id:$(this).parents("tr").data("id")
	    }
	    $.ajax({
	        async: true,
	        type: "POST",
	        dataType: "html",
	        contentType: "application/x-www-form-urlencoded",
	        success: editarSolicitud,
	        timeout: 6000,
	        error: problemas
	    }); 
	    return false;
    }
});

$(document).on("click", ".subirDocumento<?php echo $auto_increment; ?>", function () {
    var estado = $(this).parents("tr").find("td").eq(5).html();
    if(estado === 'Aprobado' || estado === 'Preaprobado'){
        alert("La solicitud ya ha sido revisada, no puede subir documentos soporte.");
    }else{
        data={
            id:$(this).parents("tr").data("id"),
            emp:$(this).parents("tr").data("emp")
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: subirDoc,
            timeout: 6000,
            error: problemas
        }); 
        return false;
    }
});

$(document).on("click", ".exportarSolicitud<?php echo $auto_increment; ?>", function () {
    var estado = $(this).parents("tr").find("td").eq(5).html();
    var respuesta = confirm("El permiso solo puede ser exportado una vez ¿Está seguro que desea exportar la solicitud?");
    if (respuesta){
	    if(estado === 'Aprobado'){
	    	data={
		        codPermiso:$(this).parents("tr").data("id")
		    }
		    $.ajax({
		        async: true,
		        type: "POST",
		        dataType: "html",
		        contentType: "application/x-www-form-urlencoded",
		        success: exportar,
		        timeout: 6000,
		        error: problemas
		    }); 
		    return false;
	    }else{
	    	alert("La solicitud no ha sido aprobada, no puede exportarse");
	    }
	}    
});

$(document).on("click", ".verDocumento<?php echo $auto_increment; ?>", function () {

    data = {
        arch: $(this).parents("tr").data("doc"),
        tp: 0
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: abrirDoc,
        timeout: 8000,
        error: problemas
    }); 

    return false;   
});

</script>

<script type="text/javascript" src="pages/permisos/revision/RevisionEmpleado/ScriptRevEmpleado.js"></script>