<?php
    $maindir = "../../../../";
	require_once($maindir."conexion/config.inc.php");
    
	if(is_array($_FILES)) {
		if(is_uploaded_file($_FILES['userImage']['tmp_name'])) {
			if(!empty($_POST['cod'])){
				$codP = $_POST['cod'];
				$ndoc = $_POST['name'];
				$sourcePath = $_FILES['userImage']['tmp_name'];
				$archivo = $_FILES['userImage']['name'];
				$trozos = explode(".", $archivo); 
				$extension = end($trozos);
				$nombreA = $ndoc.'.'.$extension;
				$targetPath = 'documentos/'.$nombreA;
			
				if(move_uploaded_file($sourcePath,$targetPath)) {
					$consulta=$db->prepare("CALL SP_DOCUMENTO_PERMISO(?,?,@mensajeError)");
					$consulta -> bindParam(1, $codP, PDO::PARAM_INT);
					$consulta -> bindParam(2, $nombreA, PDO::PARAM_STR);
					$resultado = $consulta -> execute();

					$output = $db -> query("select @mensajeError")->fetch(PDO::FETCH_ASSOC);
					$respuesta = $output['@mensajeError'];
		        	if($respuesta==NULL){
		                $mensaje = '¡El archivo fue subido exitosamente!';
		               	echo '<div class="alert alert-success">';
					    echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
					    echo '<center>';
					    echo $mensaje;
					    echo '</center></div>';
		       		}else{
		               $mensaje = 'Hubo un problema al cargar el archivo, intentelo de nuevo o contacte al administrador del sistema.';
		               	echo '<div class="alert alert-success">';
					    echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
					    echo '<center><strong>Error!</strong>';
					    echo $mensaje;
					    echo '</center></div>';
		    		}
				}
			}
		}
	}

?>