<script type="text/javascript">
   /* Script que permite cargar todos los elementos de la tabla para mostrar los datos que estan
    Almacenados en la base de datos */
$(document).ready(function() {

    //inicializarTabla(); //Se inicializa la tabla que despliega los datos de la base de datos
    cargarSolicitudes(); //Hacemos el llamado para que se cargue la tabla siempre que se cargue la pag.

    $(document).on("click",".imprimir_Permiso",function (e) {
        e.stopPropagation();
        e.preventDefault();
        var respuesta = confirm("El permiso solo puede ser exportado una vez ¿Está seguro que desea exportar la solicitud?");
        if (respuesta)
        {
            var id = $(this).data('id');

            var datos = 
            {
                codPermiso: id,
                operacion: 0
            };
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                data: datos,
                url: "pages/permisos/revision/RevisionEmpleado/verificacionPermiso.php",
                timeout: 4000,
                contentType: "application/x-www-form-urlencoded",
                success: function(response){
                    var arr = JSON.parse(response);
                    if(arr[0].conteo > 0)
                    {                                                                
                        exportar(id);
                    }
                    else
                    {                             
                        alert("Error al exportar: La solicitud no ha sido aprobada");
                    }
                }
            });
        }              
    }); //finaliza la función de aprobación de solicitudes

    $(document).on("click",".editar_Permiso",function (e) {
        e.stopPropagation();
        e.preventDefault();
            var id = $(this).data('id');
            var datos = 
            {
                codPermiso: id,
                operacion: 1
            };
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                data: datos,
                url: "pages/permisos/revision/RevisionEmpleado/verificacionPermiso.php",
                timeout: 4000,
                contentType: "application/x-www-form-urlencoded",
                success: function(response){
                    var arr = JSON.parse(response);
                    if(arr[0].conteo > 0)
                    {                                                                
                        $("#contenedor").load('pages/permisos/solicitudpersonal/editarSolicitudPermiso.php?cod='+id);
                    }
                    else
                    {                             
                        alert("Error: La solicitud ha sido revisada, no puede editarse");
                    }
                }
            });
                     
    }); //finaliza la función de aprobación de solicitudes

        /* Este evento se levanta cada vez que le damos click al bottón aprobar, este evento envíe el identificador de la solicitud para poder ser aprobada en la base de datos */
    function exportar(codigoP){ 

        var datos = 
        {
            codPermiso: codigoP
        };
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            data: datos,
            url: "pages/permisos/revision/RevisionEmpleado/exportarPermiso.php",
            timeout: 4000,
            contentType: "application/x-www-form-urlencoded",
            success: function(data){
                $("#notificaciones1").html(data);
                $("#notificaciones1").fadeOut(4500);
                window.open("pages/permisos/revision/crear_pdfpermiso.php?id="+codigoP);
                reinicio();
            }
        });
                     
    }; //finaliza la función de aprobación de solicitudes
    
    /* Esta funcion es llamada al principio, es la encargada de actualizar la tabla que muestra las solicitudes pendientes por revisar de cada uno de los jefes de departamento, que estan en el sistema en ese momento, se recarga cada vez que se deniega o se aprueba un permiso */
    function cargarSolicitudes(){
        var datos = {
            accion: 1
        };

        $.ajax({
            async: true,
            type: "POST",
            url: "pages/permisos/revision/RevisionEmpleado/cargarSolicitudesEmpleado.php",
            data: datos,
            dataType: "html",
            success: function(data){
                var response = JSON.parse(data);
                var options = '';
                /* Seccion para llenar la tabla */
                for (var index = 0;index < response.length; index++) 
                {
                   options += '<tr>' +
                                    '<td style="display : none">' + response[index].idPermiso + '</td>' +
                                    '<td>' + response[index].tipo + '</td>' +
                                    '<td>' + response[index].motivo + '</td>' +
                                    '<td>' + response[index].fecha + '</td>' +
                                    '<td>' + response[index].horaIn + '</td>' +
                                    '<td>' + response[index].horaFin + '</td>' +
                                    '<td>' + response[index].estado + '</td>' +
                                    '<td>' + response[index].observ + '</td>' +
                                    '<td><center>' +
                                        '<button data-id = "' + response[index].idPermiso +'" href= "#" class = "editar_Permiso btn btn-warning" data-target = ""><i class="fa fa-pencil-square-o"></i></button>' +
                                    '</td></center>' +
                                    '<td><center>' +
                                        '<button data-id = "' + response[index].idPermiso +'" href= "#" class = "imprimir_Permiso btn btn-danger" data-target = ""><i class="fa fa-file-pdf-o"></i></button>' +
                                    '</td></center>' +
                              '</tr>';
                }

                $("#cTablaPermisos").html("");
                $("#cTablaPermisos").html(options);

                /* Script que permite a la tabla hacer busquedas dentro de ella
                    y ordenarla deacuerdo a lo que se presenta en ella. */

                    $('#TablePermisos').dataTable();
            },
            timeout: 4000
        }); 
    } //finaliza la función de carga de solicitudes
    
});

    function reinicio()
    {
        $("#contenedor").load('pages/permisos/revision/RevisionEmpleado/revisionEmpleado.php');
         //$("#contenedor").load('../permisos/Revision.php');
    }

</script>

<div id = "notificaciones1"></div>

<!-- Seccion usada para mostrar la tabla de las solicitudes de permiso que estan pendientes de revisión en el sistema -->
<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Historial de Solicitudes </label>
        </div>
        <div class="panel-body">
            <div>
                   <div id= "noti1" class="alert alert-info" role="alert"><center>A continuación se presentan las solicitudes realizadas este mes</center></div>
            </div>
        
        <!--<div class="panel-body">-->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <!--<section class="content">-->
                            <div id = "tablero" class="table-responsive">
                                <table id= "TablePermisos" border="0" class='table table-bordered table-hover'>
                                    <thead>
                                        <tr>
                                            <th style = 'display:none'>Codigo</th>
                                            <th style="text-align:center;background-color:#386D95;color:white;">Tipo de Permiso</th> 
                                            <th style="text-align:center;background-color:#386D95;color:white;">Motivo</th>  
                                            <th style="text-align:center;background-color:#386D95;color:white;">Fecha</th>
                                            <th style="text-align:center;background-color:#386D95;color:white;">Hora Inicio</th>
                                            <th style="text-align:center;background-color:#386D95;color:white;">Hora Fin</th>
                                            <th style="text-align:center;background-color:#386D95;color:white;">Estado</th>
                                            <th style="text-align:center;background-color:#386D95;color:white;">Observación</th>
                                            <th style="text-align:center;background-color:#386D95;color:white;">Editar</th>
                                            <th style="text-align:center;background-color:#386D95;color:white;">Exportar PDF</th>  
                                        </tr>
                                    </thead>
                                    <tbody id = "cTablaPermisos">
                                        <!-- Contenido de la tabla generado atravez de la consulta a 
                                            la base de datos -->
                                    </tbody>
                                </table>       
                            </div>
                        <!--</section>-->
                    </div>                
                </div>
            </div>       
        <!--</div>-->
        </div>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
  //$(document).ready(function() {
    //$('#TablePermisos').dataTable(); // example es el id de la tabla
    //} );
</script>


