<?php
    $maindir = "../../../../";
    require_once("../../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    if(isset($_POST['codigo']) && isset($_POST['NoEmpleado']) && isset($_POST['motivo']) && isset($_POST['edificio']) && isset($_POST['tipoPerm']) && isset($_POST['jefe']) && isset($_POST['cantidad']) && isset($_POST['fecha']) && isset($_POST['horaIn']) && isset($_POST['horaFin']) && isset($_POST['justif'])){

		$pcCodPermiso = $_POST['codigo'];
		$pcCodEmpleado = $_POST['NoEmpleado'];
		$pcCodMotivo = $_POST['motivo'];
		$pcCodEdificio = $_POST['edificio'];
		$pcCodTipoPermiso = $_POST['tipoPerm'];
		$pcCodJefe = $_POST['jefe'];
		$pcDias = $_POST['cantidad'];
		$pcFecha = $_POST['fecha'];
		$pcHoraIn=$_POST['horaIn'];
		$pcHoraFin=$_POST['horaFin'];
		$pcJustificacion = $_POST['justif'];

        try{
			$consulta=$db->prepare("CALL SP_MODIFICAR_PERMISO(?,?,?,?,?,?,?,?,?,?,?,@pcMensajeError)");
			$consulta->bindParam(1, $pcCodEmpleado, PDO::PARAM_STR);
			$consulta->bindParam(2, $pcCodPermiso, PDO::PARAM_INT);
			$consulta->bindParam(3, $pcCodMotivo, PDO::PARAM_INT);
			$consulta->bindParam(4, $pcCodEdificio, PDO::PARAM_INT);
			$consulta->bindParam(5, $pcCodTipoPermiso, PDO::PARAM_INT);
			$consulta->bindParam(6, $pcCodJefe, PDO::PARAM_INT);
			$consulta->bindParam(7, $pcDias, PDO::PARAM_INT);
			$consulta->bindParam(8, $pcFecha, PDO::PARAM_STR);
			$consulta->bindParam(9, $pcHoraIn, PDO::PARAM_STR);
			$consulta->bindParam(10, $pcHoraFin, PDO::PARAM_STR);
			$consulta->bindParam(11, $pcJustificacion, PDO::PARAM_STR);

			$resultado=$consulta->execute();
			// $consulta->closeCursor();
			$output = $db->query("select @pcMensajeError")->fetch(PDO::FETCH_ASSOC);
			$mensaje = $output['@pcMensajeError'];

			if ($mensaje == NULL){
			    echo mensajes('La solicitud ha sido editada éxitosamente', 'verde');
			}else{
			    echo mensajes('Error!'.$mensaje, 'azul');
			}
		}catch(PDOexception $e){
			echo mensajes('Error!'. $e, 'rojo');
		}
	}else{
		echo mensajes("Hubo un error al intentar editar la solicitud. Inténtelo de nuevo o contacte al administrador del sistema", "azul");
	}

?>

<script type="text/javascript">
    $("#TablaMisSolicitudes").load('pages/permisos/revision/RevisionEmpleado/DatosSolicitudesEmpleado.php');
</script>