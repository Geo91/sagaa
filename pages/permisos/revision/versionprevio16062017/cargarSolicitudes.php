<?php
$mkdir = "../../../";
include($mkdir."conexion/config.inc.php");

session_start();
$idUsuario = $_SESSION['user_id'];
$idRol = $_SESSION['user_rol'];

	if($idRol == 50)
	{
		$query = "SELECT permisos.id_Permisos, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) AS 'Empleado', departamento_laboral.nombre_departamento, permisos.fecha_solicitud, permisos.dias_permiso, permisos.jefe_inmediato FROM permisos, persona, departamento_laboral, empleado WHERE permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND empleado.N_identidad=persona.N_identidad AND (permisos.fecha_solicitud BETWEEN DATE_SUB(CURDATE(), INTERVAL 60 DAY) AND DATE_ADD(CURDATE(), INTERVAL 15 DAY)) AND (permisos.estado = 3 OR (permisos.estado = 1 AND permisos.jefe_inmediato IN (SELECT No_Empleado FROM usuario WHERE id_Usuario = '".$idUsuario."'))) ORDER BY permisos.fecha_solicitud DESC";
	}else{
		if($idRol == 60){
            $query = "SELECT permisos.id_Permisos, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) AS 'Empleado', departamento_laboral.nombre_departamento, permisos.fecha_solicitud FROM permisos, persona, departamento_laboral, empleado WHERE permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND empleado.N_identidad=persona.N_identidad AND (permisos.estado = 2 OR permisos.jefe_inmediato IN (SELECT No_Empleado FROM usuario WHERE id_Usuario = '".$idUsuario."'))";
        }else{
            $query = "SELECT permisos.id_Permisos, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) AS 'Empleado', departamento_laboral.nombre_departamento, permisos.fecha_solicitud FROM permisos, persona, departamento_laboral, empleado WHERE permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND empleado.N_identidad=persona.N_identidad AND (permisos.dias_permiso < 3 AND permisos.estado = 1 AND permisos.jefe_inmediato IN (SELECT No_Empleado FROM usuario WHERE id_Usuario = '".$idUsuario."'))";
        } 
	}

$result = mysql_query($query);
$json = array();
$iteracion = 0;

while ($linea =  mysql_fetch_array($result)){
    $json[$iteracion] = array
        (
            "idPermiso" => utf8_encode($linea["id_Permisos"]),
            "nombreEmpleado" => utf8_encode($linea["Empleado"]),
            "departamento" => utf8_encode($linea["nombre_departamento"]),
            "fecha" => utf8_encode($linea["fecha_solicitud"])
        ); 

    $iteracion++;
}
	echo json_encode($json);


?>