<?php

$maindir = "../../../";

require_once($maindir."fpdf/fpdf.php");
//require($maindir."conexion/config.inc.php");
require($maindir."Datos/conexion.php");

$idPermiso = $_GET['id'];
//try{
    $consulta = "SELECT permisos.id_Permisos, permisos.No_Empleado, persona.Primer_nombre, persona.Segundo_nombre, persona.Primer_apellido, persona.Segundo_Apellido, permisos.dias_permiso, DATE_FORMAT(permisos.fecha_solicitud,'%d-%m-%Y') as 'fecha', permisos.hora_inicio, permisos.hora_finalizacion, motivos.descripcion_motivo as mtd, departamento_laboral.nombre_departamento, permisos.estado, edificios.descripcion, tipodepermiso.tipo_permiso, permisos.justificacion, permisos.jefe_inmediato, dependencia.descripcion as 'depen' from permisos inner join motivos on permisos.id_motivo=motivos.Motivo_ID inner join empleado on empleado.No_Empleado=permisos.No_Empleado inner join dependencia on empleado.id_dependencia=dependencia.dependencia_id inner join persona on persona.N_identidad=empleado.N_identidad inner join departamento_laboral on departamento_laboral.id_departamento_laboral=permisos.id_departamento inner join edificios on edificios.Edificio_ID=permisos.id_Edificio_Registro inner join tipodepermiso on permisos.id_tipo_permiso=tipodepermiso.id_tipo_permiso where permisos.estado = 5 AND permisos.id_Permisos='".$idPermiso."'";
    $resultado = mysql_query($consulta);
    $result = mysql_fetch_array($resultado);

    $conjefe = "SELECT persona.Primer_nombre, persona.Segundo_nombre, persona.Primer_apellido FROM empleado INNER JOIN persona ON empleado.N_identidad=persona.N_identidad WHERE empleado.No_Empleado = '".$result['jefe_inmediato']."'";
    $resultadoj = mysql_query($conjefe);
    $resultjefe = mysql_fetch_array($resultadoj);

    $consulta2 = "SELECT permisos.correlativo from permisos where permisos.estado = 5 AND permisos.id_Permisos='".$idPermiso."'";
    $resultado2 = mysql_query($consulta2);
    $result2 = mysql_fetch_array($resultado2);
    $cltv = $result2['correlativo'];

function mes($fecha){
    switch (date('m',strtotime($fecha))) {
        case '01':
            $mes = 'Enero';
            break;
        case '02':
            $mes = 'Febrero';
            break;
        case '03':
            $mes = 'Marzo';
            break;
        case '04':
            $mes = 'Abril';
            break;
        case '05':
            $mes = 'Mayo';
            break;
        case '06':
            $mes = 'Junio';
            break;
        case '07':
            $mes = 'Julio';
            break;
        case '08':
            $mes = 'Agosto';
            break;
        case '09':
            $mes = 'Septiembre';
            break;
        case '10':
            $mes = 'Octubre';
            break;
        case '11':
            $mes = 'Noviembre';
            break;
        case '12':
            $mes = 'Diciembre';
            break;
    }
    return $mes;
}

function dia($fecha){
    switch (date('l', strtotime(($fecha)))) {
        case 'Monday':
            $dia = 'Lunes';
            break;
        case 'Tuesday':
            $dia = 'Martes';
            break;
        case 'Wednesday':
            $dia = 'Mi閞coles';
            break;
        case 'Thursday':
            $dia = 'Jueves';
            break;
        case 'Friday':
            $dia = 'Viernes';
            break;
        case 'Saturday':
            $dia = 'S醔ado';
            break;
        case 'Sunday':
            $dia = 'Domingo';
            break;
    }
    return $dia;
}

class PDF extends FPDF{
     // Cabecera de p谩gina
         
    function Footer(){
        $idPermiso = $_GET['id'];
        $consulta2 = "SELECT permisos.correlativo from permisos where permisos.estado = 5 AND permisos.id_Permisos='".$idPermiso."'";
        $resultado2 = mysql_query($consulta2);
        $result2 = mysql_fetch_array($resultado2);
        $cltv = $result2['correlativo'];

        $this->SetY(-30);
        $this->SetFont('Arial','I',8);
        $this->Cell(0,10,utf8_decode('P谩gina ').$this->PageNo().' de {nb}   - - - -   Impreso el ' . date('d-m-y') . ' fecha del sistema   - - - -   '.$cltv,0,0,'C');
    }
         
}


$pdf = new PDF();
$pdf->AliasNbPages();

if($result['dias_permiso'] < 2){
    $pdf->AddPage();
    $pdf->SetFont('Times', '', 18);
    $pdf->Image($maindir.'assets/img/lucen-aspicio.png', 120,30,100,100, 'PNG');
    $pdf->Image($maindir.'assets/img/logo_unah.png' , 15,5,15,22, 'PNG');
    $pdf->Cell(22, 10, '', 0);
    $pdf->SetFont('Arial', '', 18);
    $pdf->Cell(5, 10, '', 0);
    $pdf->Cell(70, 10, utf8_decode('Universidad Nacional Aut贸noma de Honduras'), 0);
    $pdf->Ln(12);
    $pdf->SetFont('Arial', 'U', 14);
    $pdf->Cell(25, 8, '', 0,0,"C");
    $pdf->Cell(130, 8, utf8_decode(' Control de Permisos Personales '), 0,0,"C");
    $pdf->SetFont('Arial', '', 10);
    $pdf->Ln(10);
    $pNombre = $result['Primer_nombre'];
    $sNombre = $result['Segundo_nombre'];
    $pApellido = $result['Primer_apellido'];
    $sApellido = $result['Segundo_Apellido'];
    $nombreCompleto = "{$pNombre} {$sNombre} {$pApellido} {$sApellido}";
    $pdf->Cell(10);
    $pdf->Cell(100, 20, 'Nombre :  '.$nombreCompleto, 0,0,"");
    $pdf->Cell(40, 20, 'No. De Empleado: '.$result['No_Empleado'], 0,0,"");
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(150, 20, utf8_decode('Unidad Acad茅mica:  ').$result['nombre_departamento'], 0);
    $pdf->Cell(10);
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(105, 20, 'Se solicita el permiso por motivo de: ', 0,0,"");
    $pdf->Ln(7);
    if($result['mtd'] == 'Salud'){
        $pdf->Cell(10);
        $pdf->Cell(137, 20, 'Salud ( X )    Estudio (  )      Otros (  )', 0,0,"");
    }elseif($result['mtd'] == 'Estudio'){
        $pdf->Cell(10);
        $pdf->Cell(137, 20, 'Salud (  )    Estudio ( X )      Otros (  )', 0,0,"");
    }else{
        $pdf->Cell(10);
        $pdf->Cell(137, 20, 'Salud (  )    Estudio (  )      Otros ( X )', 0,0,"");
    }
    
    //$pdf->Cell(140, 20, 'Nombre del edificio en que marca: '.$result['descripcion'], 0,0,"");
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(137, 20, 'Edificio donde tiene registrada su asistencia: '.$result['descripcion'], 0,0,"");
    //$pdf->Cell(137, 20, 'Tipo de permiso: '.$result['tipo_permiso'], 0,0,"");
    //$pdf->Cell(0, 20, utf8_decode('Duraci贸n en dias: ').$result['dias_permiso'], 0,0,"");
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(137, 20, utf8_decode('Tiempo de duraci贸n del permiso: '), 0,0,"");
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(42, 20, 'Fecha: '.$result['fecha'], 0,0,"");
    $pdf->Cell(10, 10, '', 0);
    $pdf->Cell(10);
    $pdf->Cell(42, 20, 'Hora Inicio: '.$result['hora_inicio'], 0,0,"");
    $pdf->Cell(10, 10, '', 0);
    $pdf->Cell(10);
    $pdf->Cell(40, 20, utf8_decode('Hora Finalizaci贸n: ') .$result['hora_finalizacion'], 0);
    $pdf->Ln(20);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(5);
    $pdf->Cell(75, 8, '_______________________________', 0,0,"C");
    $pdf->Cell(135, 8, '_______________________________', 0,0,"C");
    $pdf->Ln(7);
    $pdf->Cell(80, 8, 'Firma del Solicitante', 0,0,"C");
    $pdf->Cell(125, 8, 'V.B del Jefe Inmediato  ', 0,0,"C");
    $pdf->Ln(12);
    $pdf->Cell(5);
    $pdf->Cell(180, 8, '____________________________________', 0,0,"C");
    $pdf->Ln(7);
    $pdf->Cell(5);
    $pdf->Cell(180, 8, utf8_decode('Secci贸n de Efectividad y Control'), 0,0,"C");
    $pdf->Ln(4);
    $pdf->Cell(180, 8, 'del Recurso Humano', 0,0,"C");
    $pdf->Ln(8);
    $pdf->Ln(20);
    $pdf->SetY(130);
    $pdf->SetFont('Arial','I',8);
    $pdf->Cell(0,10,utf8_decode('P谩gina ').$pdf->PageNo().' de {nb}   - - - -   Impreso el ' . date('d-m-y') . ' fecha del sistema   - - - -   '.$cltv,0,0,'C');
    $pdf->Ln(15);
    
    $pdf->Line(0,140,350,140);

    $pdf->Image($maindir.'assets/img/lucen-aspicio.png', 120,170,100,100, 'PNG');
    $pdf->Image($maindir.'assets/img/logo_unah.png',15,147,15,22,'PNG');
    $pdf->Cell(22, 10, '', 0);
    $pdf->SetFont('Arial', '', 18);
    $pdf->Cell(5, 10, '', 0);
    $pdf->Cell(70, 10, utf8_decode('Universidad Nacional Aut贸noma de Honduras'), 0);
    $pdf->Ln(12);
    $pdf->SetFont('Arial', 'U', 14);
    $pdf->Cell(25, 8, '', 0,0,"C");
    $pdf->Cell(130, 8, utf8_decode(' Control de Permisos Personales '), 0,0,"C");
    $pdf->SetFont('Arial', '', 10);
    $pdf->Ln(10);
    $pNombre = $result['Primer_nombre'];
    $sNombre = $result['Segundo_nombre'];
    $pApellido = $result['Primer_apellido'];
    $sApellido = $result['Segundo_Apellido'];
    $nombreCompleto = "{$pNombre} {$sNombre} {$pApellido} {$sApellido}";
    $pdf->Cell(10);
    $pdf->Cell(100, 20, 'Nombre :  '.$nombreCompleto, 0,0,"");
    $pdf->Cell(40, 20, 'No. De Empleado: '.$result['No_Empleado'], 0,0,"");
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(150, 20, utf8_decode('Unidad Acad茅mica:  ').$result['nombre_departamento'], 0);
    $pdf->Cell(10);
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(105, 20, 'Se solicita el permiso por motivo de: ', 0,0,"");
    $pdf->Ln(7);
    if($result['mtd'] == 'Salud'){
        $pdf->Cell(10);
        $pdf->Cell(137, 20, 'Salud ( X )    Estudio (  )      Otros (  )', 0,0,"");
    }elseif($result['mtd'] == 'Estudio'){
        $pdf->Cell(10);
        $pdf->Cell(137, 20, 'Salud (  )    Estudio ( X )      Otros (  )', 0,0,"");
    }else{
        $pdf->Cell(10);
        $pdf->Cell(137, 20, 'Salud (  )    Estudio (  )      Otros ( X )', 0,0,"");
    }
    
    //$pdf->Cell(140, 20, 'Nombre del edificio en que marca: '.$result['descripcion'], 0,0,"");
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(137, 20, 'Edificio donde tiene registrada su asistencia: '.$result['descripcion'], 0,0,"");
    //$pdf->Cell(137, 20, 'Tipo de permiso: '.$result['tipo_permiso'], 0,0,"");
    //$pdf->Cell(0, 20, utf8_decode('Duraci贸n en dias: ').$result['dias_permiso'], 0,0,"");
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(137, 20, utf8_decode('Tiempo de duraci贸n del permiso: '), 0,0,"");
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(42, 20, 'Fecha: '.$result['fecha'], 0,0,"");
    $pdf->Cell(10, 10, '', 0);
    $pdf->Cell(10);
    $pdf->Cell(42, 20, 'Hora Inicio: '.$result['hora_inicio'], 0,0,"");
    $pdf->Cell(10, 10, '', 0);
    $pdf->Cell(10);
    $pdf->Cell(40, 20, utf8_decode('Hora Finalizaci贸n: ') .$result['hora_finalizacion'], 0);
    $pdf->Ln(20);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(5);
    $pdf->Cell(75, 8, '_______________________________', 0,0,"C");
    $pdf->Cell(135, 8, '_______________________________', 0,0,"C");
    $pdf->Ln(7);
    $pdf->Cell(80, 8, 'Firma del Solicitante', 0,0,"C");
    $pdf->Cell(125, 8, 'V.B del Jefe Inmediato  ', 0,0,"C");
    $pdf->Ln(12);
    $pdf->Cell(5);
    $pdf->Cell(180, 8, '____________________________________', 0,0,"C");
    $pdf->Ln(7);
    $pdf->Cell(5);
    $pdf->Cell(180, 8, utf8_decode('Secci贸n de Efectividad y Control'), 0,0,"C");
    $pdf->Ln(4);
    $pdf->Cell(180, 8, 'del Recurso Humano', 0,0,"C");
    $pdf->Ln(8);
    $pdf->Ln(20);
    $pdf->SetY(130);
    $pdf->SetFont('Arial','I',8);
    $pdf->Cell(0,10,utf8_decode('P谩gina ').$pdf->PageNo().' de {nb}   - - - -   Impreso el ' . date('d-m-y') . ' fecha del sistema   - - - -   '.$cltv,0,0,'C');
    $pdf->Ln(15);

    $pdf->AddPage();
    $pdf->SetFont('Arial', '', 9);
    $pdf->Image($maindir.'assets/img/enc_doc.png', 2,1,207,32, 'PNG'); //Encabezado
    $pdf->Image($maindir.'assets/img/pieDoc.png', 2,165,208,130, 'PNG'); //Pie de p谩gina
    $pdf->Cell(160);
    $pdf->Cell(0, 15, 'Tel: 2216-5100 ', 0,0,"R");
    $pdf->Ln(4);
    $pdf->Cell(0, 15, 'Edificio A-2 ', 0,0,"R");
    $pdf->Cell(22, 10, '', 0);
    $pdf->Cell(5, 10, '', 0);
    $pdf->Ln(25);
    $pdf->SetFont('Arial', 'U', 14);
    $pdf->Cell(25, 8, '', 0,0,"C");
    $pdf->Cell(130, 8, utf8_decode(' Control de Permisos Personales '), 0,0,"C");
    $pdf->SetFont('Arial', '', 10);
    $pdf->Ln(20);
    $pdf->Cell(12);
    $pdf->SetFont('Arial','B','12');
    $pdf->Cell(0, 10, utf8_decode('M谩ster'), 0,0,"");
    $pdf->Ln(5);
    $pdf->Cell(12);
    $pdf->Cell(0, 10, $resultjefe['Primer_nombre'].' '.$resultjefe['Segundo_nombre'].' '.$resultjefe['Primer_apellido'], 0,0,"");
    $pdf->Ln(6);
    $pdf->Cell(12);
    $pdf->Cell(0, 10, $result['depen'], 0,0,"");
    $pdf->Ln(6);
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode('UNAH'), 0,0,"");
    $pdf->Ln(6);
    $pdf->SetFont('Arial', '', '12');
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode('Su oficina'), 0,0,"");
    $pdf->Ln(20);
    $pdf->Cell(12);
    $pdf->MultiCell(170, 7,'Yo '.$nombreCompleto.' con No. de empleado: '.$result['No_Empleado'].' solicito permiso para ausentarme de mis labores diarias el'.utf8_decode(' d铆a ').dia($result['fecha']).' '.date('d',strtotime($result['fecha'])).' de '.mes($result['fecha']).utf8_decode(' del presente a帽o, en horario de'.$result['hora_inicio'].' a '.$result['hora_finalizacion'].' con el prop贸sito de ').$result['justificacion'].'.', 0,"J",0);

    $pdf->Ln(8);
    $pdf->Cell(12);
    $pdf->MultiCell(170, 7,utf8_decode('Se adjunta la ficha de solicitud de permiso.'), 0,"J",0);

    $pdf->Ln(8);
    $pdf->Cell(12);
    $pdf->MultiCell(170, 7,utf8_decode('Agradeciendo de antemano su valiosa colaboraci贸n, me suscribo respetuosamente.'), 0,"J",0);

    $pdf->Ln(7);
    $pdf->Cell(12);
    $pdf->Cell(0,10,'Atentamente,',0,0,"");
    $pdf->Ln(65);
    $pdf->SetFont('Arial', 'IB', 12);
    $pdf->Cell(0, 8, strtoupper($nombreCompleto), 0,0,"C");
    $pdf->Ln(8);
    $pdf->Cell(0, 8, utf8_decode('Facultad de Ciencias Jur铆dicas'), 0,0,"C");

    $pdf->Output('Permiso.pdf','I');

};

if($result['dias_permiso'] == 2){
    $n = 0;
    while ($n < $result['dias_permiso']){

        $consulta2 = "SELECT DATE_FORMAT(DATE(DATE_ADD(permisos.fecha_solicitud, INTERVAL '".$n."' DAY)), '%d-%m-%Y') as 'intervaloFecha' from permisos where permisos.id_Permisos='".$idPermiso."'";
        $resultadoF = mysql_query($consulta2);
        $resultF = mysql_fetch_array($resultadoF);

        $pdf->AddPage();
        $pdf->SetFont('Times', '', 18);
        $pdf->Image($maindir.'assets/img/lucen-aspicio.png', 120,30,100,100, 'PNG');
        $pdf->Image($maindir.'assets/img/logo_unah.png' , 15,5,15,22, 'PNG');
        $pdf->Cell(22, 10, '', 0);
        $pdf->SetFont('Arial', '', 18);
        $pdf->Cell(5, 10, '', 0);
        $pdf->Cell(70, 10, utf8_decode('Universidad Nacional Aut贸noma de Honduras'), 0);
        $pdf->Ln(12);
        $pdf->SetFont('Arial', 'U', 14);
        $pdf->Cell(25, 8, '', 0,0,"C");
        $pdf->Cell(130, 8, utf8_decode(' Control de Permisos Personales '), 0,0,"C");
        $pdf->SetFont('Arial', '', 10);
        $pdf->Ln(10);
        $pNombre = $result['Primer_nombre'];
        $sNombre = $result['Segundo_nombre'];
        $pApellido = $result['Primer_apellido'];
        $sApellido = $result['Segundo_Apellido'];
        $nombreCompleto = "{$pNombre} {$sNombre} {$pApellido} {$sApellido}";
        $pdf->Cell(10);
        $pdf->Cell(100, 20, 'Nombre :  '.$nombreCompleto, 0,0,"");
        $pdf->Cell(40, 20, 'No. De Empleado: '.$result['No_Empleado'], 0,0,"");
        $pdf->Ln(7);
        $pdf->Cell(10);
        $pdf->Cell(150, 20, utf8_decode('Unidad Acad茅mica:  ').$result['nombre_departamento'], 0);
        $pdf->Cell(10);
        $pdf->Ln(7);
        $pdf->Cell(10);
        $pdf->Cell(105, 20, 'Se solicita el permiso por motivo de: ', 0,0,"");
        $pdf->Ln(7);
        if($result['mtd'] == 'Salud'){
            $pdf->Cell(10);
            $pdf->Cell(137, 20, 'Salud ( X )    Estudio (  )      Otros (  )', 0,0,"");
        }elseif($result['mtd'] == 'Estudio'){
            $pdf->Cell(10);
            $pdf->Cell(137, 20, 'Salud (  )    Estudio ( X )      Otros (  )', 0,0,"");
        }else{
            $pdf->Cell(10);
            $pdf->Cell(137, 20, 'Salud (  )    Estudio (  )      Otros ( X )', 0,0,"");
        }
        
        //$pdf->Cell(140, 20, 'Nombre del edificio en que marca: '.$result['descripcion'], 0,0,"");
        $pdf->Ln(7);
        $pdf->Cell(10);
        $pdf->Cell(137, 20, 'Edificio donde tiene registrada su asistencia: '.$result['descripcion'], 0,0,"");
        //$pdf->Cell(137, 20, 'Tipo de permiso: '.$result['tipo_permiso'], 0,0,"");
        //$pdf->Cell(0, 20, utf8_decode('Duraci贸n en dias: ').$result['dias_permiso'], 0,0,"");
        $pdf->Ln(7);
        $pdf->Cell(10);
        $pdf->Cell(137, 20, utf8_decode('Tiempo de duraci贸n del permiso: '), 0,0,"");
        $pdf->Ln(7);
        $pdf->Cell(10);
        $pdf->Cell(42, 20, 'Fecha: '.$resultF['intervaloFecha'], 0,0,"");
        $pdf->Cell(10, 10, '', 0);
        $pdf->Cell(10);
        $pdf->Cell(42, 20, 'Hora Inicio: '.$result['hora_inicio'], 0,0,"");
        $pdf->Cell(10, 10, '', 0);
        $pdf->Cell(10);
        $pdf->Cell(40, 20, utf8_decode('Hora Finalizaci贸n: ') .$result['hora_finalizacion'], 0);
        $pdf->Ln(20);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(5);
        $pdf->Cell(75, 8, '_______________________________', 0,0,"C");
        $pdf->Cell(135, 8, '_______________________________', 0,0,"C");
        $pdf->Ln(7);
        $pdf->Cell(80, 8, 'Firma del Solicitante', 0,0,"C");
        $pdf->Cell(125, 8, 'V.B del Jefe Inmediato  ', 0,0,"C");
        $pdf->Ln(12);
        $pdf->Cell(5);
        $pdf->Cell(180, 8, '____________________________________', 0,0,"C");
        $pdf->Ln(7);
        $pdf->Cell(5);
        $pdf->Cell(180, 8, utf8_decode('Secci贸n de Efectividad y Control'), 0,0,"C");
        $pdf->Ln(4);
        $pdf->Cell(180, 8, 'del Recurso Humano', 0,0,"C");
        $pdf->Ln(8);
        $pdf->Ln(20);
        
        $pdf->Line(0,140,350,140);

        $pdf->Image($maindir.'assets/img/lucen-aspicio.png', 120,170,100,100, 'PNG');
        $pdf->Image($maindir.'assets/img/logo_unah.png',15,147,15,22,'PNG');
        $pdf->Cell(22, 10, '', 0);
        $pdf->SetFont('Arial', '', 18);
        $pdf->Cell(5, 10, '', 0);
        $pdf->Cell(70, 10, utf8_decode('Universidad Nacional Aut贸noma de Honduras'), 0);
        $pdf->Ln(12);
        $pdf->SetFont('Arial', 'U', 14);
        $pdf->Cell(25, 8, '', 0,0,"C");
        $pdf->Cell(130, 8, utf8_decode(' Control de Permisos Personales '), 0,0,"C");
        $pdf->SetFont('Arial', '', 10);
        $pdf->Ln(10);
        $pNombre = $result['Primer_nombre'];
        $sNombre = $result['Segundo_nombre'];
        $pApellido = $result['Primer_apellido'];
        $sApellido = $result['Segundo_Apellido'];
        $nombreCompleto = "{$pNombre} {$sNombre} {$pApellido} {$sApellido}";
        $pdf->Cell(10);
        $pdf->Cell(100, 20, 'Nombre :  '.$nombreCompleto, 0,0,"");
        $pdf->Cell(40, 20, 'No. De Empleado: '.$result['No_Empleado'], 0,0,"");
        $pdf->Ln(7);
        $pdf->Cell(10);
        $pdf->Cell(150, 20, utf8_decode('Unidad Acad茅mica:  ').$result['nombre_departamento'], 0);
        $pdf->Cell(10);
        $pdf->Ln(7);
        $pdf->Cell(10);
        $pdf->Cell(105, 20, 'Se solicita el permiso por motivo de: ', 0,0,"");
        $pdf->Ln(7);
        if($result['mtd'] == 'Salud'){
            $pdf->Cell(10);
            $pdf->Cell(137, 20, 'Salud ( X )    Estudio (  )      Otros (  )', 0,0,"");
        }elseif($result['mtd'] == 'Estudio'){
            $pdf->Cell(10);
            $pdf->Cell(137, 20, 'Salud (  )    Estudio ( X )      Otros (  )', 0,0,"");
        }else{
            $pdf->Cell(10);
            $pdf->Cell(137, 20, 'Salud (  )    Estudio (  )      Otros ( X )', 0,0,"");
        }
        
        //$pdf->Cell(140, 20, 'Nombre del edificio en que marca: '.$result['descripcion'], 0,0,"");
        $pdf->Ln(7);
        $pdf->Cell(10);
        $pdf->Cell(137, 20, 'Edificio donde tiene registrada su asistencia: '.$result['descripcion'], 0,0,"");
        //$pdf->Cell(137, 20, 'Tipo de permiso: '.$result['tipo_permiso'], 0,0,"");
        //$pdf->Cell(0, 20, utf8_decode('Duraci贸n en dias: ').$result['dias_permiso'], 0,0,"");
        $pdf->Ln(7);
        $pdf->Cell(10);
        $pdf->Cell(137, 20, utf8_decode('Tiempo de duraci贸n del permiso: '), 0,0,"");
        $pdf->Ln(7);
        $pdf->Cell(10);
        $pdf->Cell(42, 20, 'Fecha: '.$resultF['intervaloFecha'], 0,0,"");
        $pdf->Cell(10, 10, '', 0);
        $pdf->Cell(10);
        $pdf->Cell(42, 20, 'Hora Inicio: '.$result['hora_inicio'], 0,0,"");
        $pdf->Cell(10, 10, '', 0);
        $pdf->Cell(10);
        $pdf->Cell(40, 20, utf8_decode('Hora Finalizaci贸n: ') .$result['hora_finalizacion'], 0);
        $pdf->Ln(20);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(5);
        $pdf->Cell(75, 8, '_______________________________', 0,0,"C");
        $pdf->Cell(135, 8, '_______________________________', 0,0,"C");
        $pdf->Ln(7);
        $pdf->Cell(80, 8, 'Firma del Solicitante', 0,0,"C");
        $pdf->Cell(125, 8, 'V.B del Jefe Inmediato  ', 0,0,"C");
        $pdf->Ln(12);
        $pdf->Cell(5);
        $pdf->Cell(180, 8, '____________________________________', 0,0,"C");
        $pdf->Ln(7);
        $pdf->Cell(5);
        $pdf->Cell(180, 8, utf8_decode('Secci贸n de Efectividad y Control'), 0,0,"C");
        $pdf->Ln(4);
        $pdf->Cell(180, 8, 'del Recurso Humano', 0,0,"C");
        $pdf->Ln(8);
        $pdf->Ln(20);

        $n = ($n + 1);
    }

    $pdf->AddPage();
    $pdf->SetFont('Arial', '', 9);
    $pdf->Image($maindir.'assets/img/enc_doc.png', 2,1,207,32, 'PNG'); //Encabezado
    $pdf->Image($maindir.'assets/img/pieDoc.png', 2,165,208,130, 'PNG'); //Pie de p谩gina
    $pdf->Cell(160);
    $pdf->Cell(0, 15, 'Tel: 2216-5100 ', 0,0,"R");
    $pdf->Ln(4);
    $pdf->Cell(0, 15, 'Edificio A-2 ', 0,0,"R");
    $pdf->Cell(22, 10, '', 0);
    $pdf->Cell(5, 10, '', 0);
    $pdf->Ln(25);
    $pdf->SetFont('Arial', 'U', 14);
    $pdf->Cell(25, 8, '', 0,0,"C");
    $pdf->Cell(130, 8, utf8_decode(' Control de Permisos Personales '), 0,0,"C");
    $pdf->SetFont('Arial', '', 10);
    $pdf->Ln(20);
    $pdf->Cell(12);
    $pdf->SetFont('Arial','B','12');
    $pdf->Cell(0, 10, utf8_decode('M谩ster'), 0,0,"");
    $pdf->Ln(5);
    $pdf->Cell(12);
    $pdf->Cell(0, 10, $resultjefe['Primer_nombre'].' '.$resultjefe['Segundo_nombre'].' '.$resultjefe['Primer_apellido'], 0,0,"");
    $pdf->Ln(6);
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode($result['depen']), 0,0,"");
    $pdf->Ln(6);
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode('UNAH'), 0,0,"");
    $pdf->Ln(6);
    $pdf->SetFont('Arial', '', '12');
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode('Su oficina'), 0,0,"");
    $pdf->Ln(20);
    $pdf->Cell(12);
    $pdf->MultiCell(170, 7,'Yo '.$nombreCompleto.' con No. de empleado: '.$result['No_Empleado'].' solicito permiso para ausentarme de mis labores diarias desde el'.utf8_decode(' d铆a ').dia($result['fecha']).' '.date('d',strtotime($result['fecha'])).' al '.dia($resultF['intervaloFecha']).' '.date('d',strtotime($resultF['intervaloFecha'])).' de '.mes($result['fecha']).utf8_decode(' del presente a帽o, con el prop贸sito de ').$result['justificacion'].'.', 0,"J",0);

    $pdf->Ln(8);
    $pdf->Cell(12);
    $pdf->MultiCell(170, 7,utf8_decode('Se adjunta la ficha de solicitud de permiso.'), 0,"J",0);

    $pdf->Ln(8);
    $pdf->Cell(12);
    $pdf->MultiCell(170, 7,utf8_decode('Agradeciendo de antemano su valiosa colaboraci贸n, me suscribo respetuosamente.'), 0,"J",0);

    $pdf->Ln(7);
    $pdf->Cell(12);
    $pdf->Cell(0,10,'Atentamente,',0,0,"");
    $pdf->Ln(65);
    $pdf->SetFont('Arial', 'IB', 12);
    $pdf->Cell(0, 8, strtoupper($nombreCompleto), 0,0,"C");
    $pdf->Ln(8);
    $pdf->Cell(0, 8, utf8_decode('Facultad de Ciencias Jur铆dicas'), 0,0,"C");

    $pdf->Output('Permiso.pdf','I');
    
};

if($result['dias_permiso'] > 2){
    $consulta2 = "SELECT DATE_FORMAT(DATE(DATE_ADD(permisos.fecha_solicitud, INTERVAL '".$result['dias_permiso']."' DAY)), '%d-%m-%Y') as 'intervaloFecha' from permisos where permisos.id_Permisos='".$idPermiso."'";
    $resultadoF = mysql_query($consulta2);
    $resultF = mysql_fetch_array($resultadoF);

    $pdf->AddPage();
    $pdf->SetFont('Times', '', 18);
    $pdf->Image($maindir.'assets/img/lucen-aspicio.png', 120,30,100,100, 'PNG');
    $pdf->Image($maindir.'assets/img/logo_unah.png' , 15,5,15,22, 'PNG');
    $pdf->Cell(22, 10, '', 0);
    $pdf->SetFont('Arial', '', 18);
    $pdf->Cell(5, 10, '', 0);
    $pdf->Cell(70, 10, utf8_decode('Universidad Nacional Aut贸noma de Honduras'), 0);
    $pdf->Ln(12);
    $pdf->SetFont('Arial', 'U', 14);
    $pdf->Cell(25, 8, '', 0,0,"C");
    $pdf->Cell(130, 8, utf8_decode(' Control de Permisos Personales'), 0,0,"C");
    $pdf->SetFont('Arial', '', 10);
    $pdf->Ln(10);
    $pNombre = $result['Primer_nombre'];
    $sNombre = $result['Segundo_nombre'];
    $pApellido = $result['Primer_apellido'];
    $sApellido = $result['Segundo_Apellido'];
    $nombreCompleto = "{$pNombre} {$sNombre} {$pApellido} {$sApellido}";
    $pdf->Cell(10);
    $pdf->Cell(80, 20, 'Yo:  '.$nombreCompleto, 0,0,"");
    $pdf->Cell(40, 20, 'Con No. de Empleado: '.$result['No_Empleado'], 0,0,"");
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(150, 20, utf8_decode('Hago  solicitud  por  escrito,  para  que  se  remita  a  la  Secretar铆a  Ejecutiva  de  Desarrollo  de  Personal '));
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(150, 20, utf8_decode('la  documentaci贸n   adjunta  por   ausentarme  de  mis  labores,  las  cu谩les   desempe帽o  a  diario  en  el '), 0);
    $pdf->Cell(10);
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(150, 20, utf8_decode('Departamento  de:  ').$result['nombre_departamento'], 0);
    $pdf->Cell(10);
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(105, 20, utf8_decode('Se solicita esta gesti贸n por motivo de: ').$result['mtd'], 0,0,"");
    $pdf->Cell(10);
    $pdf->Cell(137, 20, 'Tipo de permiso: '.$result['tipo_permiso'], 0,0,"");
    //$pdf->Cell(140, 20, 'Nombre del edificio en que marca: '.$result['descripcion'], 0,0,"");
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(137, 20, 'Nombre del edificio en que marca: '.$result['descripcion'], 0,0,"");
    //$pdf->Cell(137, 20, 'Tipo de permiso: '.$result['tipo_permiso'], 0,0,"");
    $pdf->Cell(0, 20, utf8_decode('Duraci贸n en dias: ').$result['dias_permiso'], 0,0,"");
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(42, 20, 'En la fecha: '.$result['fecha'], 0,0,"");
    $pdf->Cell(10, 10, '', 0);
    $pdf->Cell(10);
    $pdf->Cell(42, 20, 'Hora Inicio: '.$result['hora_inicio'], 0,0,"");
    $pdf->Cell(10, 10, '', 0);
    $pdf->Cell(10);
    $pdf->Cell(40, 20, utf8_decode('Hora Finalizaci贸n: ') .$result['hora_finalizacion'], 0);
    $pdf->Ln(28);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(5);
    $pdf->Cell(75, 8, '_______________________________', 0,0,"C");
    $pdf->Cell(135, 8, '_______________________________', 0,0,"C");
    $pdf->Ln(7);
    $pdf->Cell(80, 8, 'Firma del Solicitante', 0,0,"C");
    $pdf->Cell(125, 8, 'V.B del Jefe Inmediato  ', 0,0,"C");
    $pdf->Ln(10);
    $pdf->Ln(10);
    $pdf->Ln(20);
    
    $pdf->Line(0,140,350,140);

    $pdf->Image($maindir.'assets/img/lucen-aspicio.png', 120,170,100,100, 'PNG');
    $pdf->Image($maindir.'assets/img/logo_unah.png',15,147,15,22,'PNG');
    $pdf->Cell(22, 10, '', 0);
    $pdf->SetFont('Arial', '', 18);
    $pdf->Cell(5, 10, '', 0);
    $pdf->Cell(70, 10, utf8_decode('Universidad Nacional Aut贸noma de Honduras'), 0);
    $pdf->Ln(12);
    $pdf->SetFont('Arial', 'U', 14);
    $pdf->Cell(25, 8, '', 0,0,"C");
    $pdf->Cell(130, 8, utf8_decode(' Control de Permisos Personales'), 0,0,"C");
    $pdf->SetFont('Arial', '', 10);
    $pdf->Ln(10);
    $pNombre = $result['Primer_nombre'];
    $sNombre = $result['Segundo_nombre'];
    $pApellido = $result['Primer_apellido'];
    $sApellido = $result['Segundo_Apellido'];
    $nombreCompleto = "{$pNombre} {$sNombre} {$pApellido} {$sApellido}";
    $pdf->Cell(10);
    $pdf->Cell(80, 20, 'Yo:  '.$nombreCompleto, 0,0,"");
    $pdf->Cell(40, 20, 'Con No. de Empleado: '.$result['No_Empleado'], 0,0,"");
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(150, 20, utf8_decode('Hago  solicitud  por  escrito,  para  que  se  remita  a  la  Secretar铆a  Ejecutiva  de  Desarrollo  de  Personal '));
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(150, 20, utf8_decode('la  documentaci贸n   adjunta  por   ausentarme  de  mis  labores,  las  cu谩les   desempe帽o  a  diario  en  el '), 0);
    $pdf->Cell(10);
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(150, 20, utf8_decode('Departamento  de:  ').$result['nombre_departamento'], 0);
    $pdf->Cell(10);
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(105, 20, utf8_decode('Se solicita esta gesti贸n por motivo de: ').$result['mtd'], 0,0,"");
    $pdf->Cell(10);
    $pdf->Cell(137, 20, 'Tipo de permiso: '.$result['tipo_permiso'], 0,0,"");
    //$pdf->Cell(140, 20, 'Nombre del edificio en que marca: '.$result['descripcion'], 0,0,"");
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(137, 20, 'Nombre del edificio en que marca: '.$result['descripcion'], 0,0,"");
    //$pdf->Cell(137, 20, 'Tipo de permiso: '.$result['tipo_permiso'], 0,0,"");
    $pdf->Cell(0, 20, utf8_decode('Duraci贸n en dias: ').$result['dias_permiso'], 0,0,"");
    $pdf->Ln(7);
    $pdf->Cell(10);
    $pdf->Cell(42, 20, 'En la fecha: '.$result['fecha'], 0,0,"");
    $pdf->Cell(10, 10, '', 0);
    $pdf->Cell(10);
    $pdf->Cell(42, 20, 'Hora Inicio: '.$result['hora_inicio'], 0,0,"");
    $pdf->Cell(10, 10, '', 0);
    $pdf->Cell(10);
    $pdf->Cell(40, 20, utf8_decode('Hora Finalizaci贸n: ') .$result['hora_finalizacion'], 0);
    $pdf->Ln(28);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(5);
    $pdf->Cell(75, 8, '_______________________________', 0,0,"C");
    $pdf->Cell(135, 8, '_______________________________', 0,0,"C");
    $pdf->Ln(7);
    $pdf->Cell(80, 8, 'Firma del Solicitante', 0,0,"C");
    $pdf->Cell(125, 8, 'V.B del Jefe Inmediato  ', 0,0,"C");
    $pdf->Ln(20);

    $pdf->AddPage();
    $pdf->SetFont('Arial', '', 9);
    $pdf->Image($maindir.'assets/img/enc_doc.png', 2,1,207,32, 'PNG'); //Encabezado
    $pdf->Image($maindir.'assets/img/pieDoc.png', 2,165,208,130, 'PNG'); //Pie de p谩gina
    $pdf->Cell(160);
    $pdf->Cell(0, 15, 'Tel: 2216-5100 ', 0,0,"R");
    $pdf->Ln(4);
    $pdf->Cell(0, 15, 'Edificio A-2 ', 0,0,"R");
    $pdf->Cell(22, 10, '', 0);
    $pdf->Cell(5, 10, '', 0);
    $pdf->Ln(25);
    $pdf->SetFont('Arial', 'U', 14);
    $pdf->Cell(25, 8, '', 0,0,"C");
    $pdf->Cell(130, 8, utf8_decode(' Control de Permisos Personales '), 0,0,"C");
    $pdf->SetFont('Arial', '', 10);
    $pdf->Ln(20);
    $pdf->Cell(12);
    $pdf->SetFont('Arial','B','12');
    $pdf->Cell(0, 10, utf8_decode('M谩ster'), 0,0,"");
    $pdf->Ln(5);
    $pdf->Cell(12);
    $pdf->Cell(0, 10, $resultjefe['Primer_nombre'].' '.$resultjefe['Segundo_nombre'].' '.$resultjefe['Primer_apellido'], 0,0,"");
    $pdf->Ln(6);
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_encode($result['depen']), 0,0,"");
    $pdf->Ln(6);
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode('UNAH'), 0,0,"");
    $pdf->Ln(6);
    $pdf->SetFont('Arial', '', '12');
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode('Su oficina'), 0,0,"");
    $pdf->Ln(20);
    $pdf->Cell(12);
    $pdf->MultiCell(170, 7,'Yo '.$nombreCompleto.' con No. de empleado: '.$result['No_Empleado'].' solicito permiso para ausentarme de mis labores diarias desde el'.utf8_decode(' d铆a ').dia($result['fecha']).' '.date('d',strtotime($result['fecha'])).' al '.dia($resultF['intervaloFecha']).' '.date('d',strtotime($resultF['intervaloFecha'])).' de '.mes($result['fecha']).utf8_decode(' del presente a帽o, con el prop贸sito de ').$result['justificacion'].'.', 0,"J",0);

    $pdf->Ln(8);
    $pdf->Cell(12);
    $pdf->MultiCell(170, 7,utf8_decode('Se adjunta la ficha de solicitud de permiso.'), 0,"J",0);

    $pdf->Ln(8);
    $pdf->Cell(12);
    $pdf->MultiCell(170, 7,utf8_decode('Agradeciendo de antemano su valiosa colaboraci贸n, me suscribo respetuosamente.'), 0,"J",0);

    $pdf->Ln(7);
    $pdf->Cell(12);
    $pdf->Cell(0,10,'Atentamente,',0,0,"");
    $pdf->Ln(65);
    $pdf->SetFont('Arial', 'IB', 12);
    $pdf->Cell(0, 8, strtoupper($nombreCompleto), 0,0,"C");
    $pdf->Ln(8);
    $pdf->Cell(0, 8, utf8_decode('Facultad de Ciencias Jur铆dicas'), 0,0,"C");

    $pdf->Output('Permiso.pdf','I');

};


?>