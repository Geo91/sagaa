<div id = "notificaciones1"></div>

<!-- Seccion usada para mostrar la tabla de las solicitudes de permiso que estan pendientes de revisión en el sistema -->
<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Historial de Solicitudes </label>
        </div>
        <div class="panel-body">
            <div>
                <div id= "noti1" class="alert alert-info" role="alert"><center>A continuación se presentan las solicitudes que se han exportado</center></div>
            </div>

            <div id = "TablaSolicitudesExportadas">
                <?php include("DatosSolicitudesExportadas.php"); ?>
            </div>   
                
        </div>
    </div>
</div>

<script type="text/javascript" src="pages/permisos/revision/ScriptAdmin.js"></script>