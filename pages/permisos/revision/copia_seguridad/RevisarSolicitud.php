<script type="text/javascript">
   /* Script que permite cargar todos los elementos de la tabla para mostrar los datos que estan
    Almacenados en la base de datos */
$(document).ready(function() {

    //inicializarTabla(); //Se inicializa la tabla que despliega los datos de la base de datos
    cargarSolicitudes(); //Hacemos el llamado para que se cargue la tabla siempre que se cargue la pag.

    /* Este evento se levanta cada vez que le damos click al bott贸n aprobar, este evento env铆e el identificador de la solicitud para poder ser aprobada en la base de datos */
    $(document).on("click",".aprobar_Permiso",function (e) {
        e.preventDefault();

        $("#Mdiv_justificarA").html("");
        var datos = { 
            
            id:  $(this).data('id') 
        };
        $.ajax({
            async: true,
            type: "POST",
            url: "pages/permisos/revision/denegarSolicitud.php",
            data: datos,
            dataType: "html",
            success: function(data){
                var response = JSON.parse(data);
                var options = '';
       
                for (var index = 0;index < response.length; index++) 
                {
                   options +=   '<div class="form-group">' +
                                    '<label style="display:none">Codigo del permiso</label>' +
                                    '<input style="display:none "id="codPermiso1" disabled = "true" data-id="' + response[index].codPermiso + '" placeholder="' + response[index].codPermiso + '" class="form-control">' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<label>Observaci髇</label>' +
                                    '<input id="obs1" placeholder = "' + response[index].obs + '" class="form-control">' +
                                '</div>';
                }

                $("#Mdiv_justificarA").empty();
                $("#Mdiv_justificarA").html(options);
                
                $('#compose-modal-aprobar').modal('show');
            },
            timeout: 4000
        });            
    }); //finaliza la funci贸n de aprobaci贸n de solicitudes


/* Este evento se levanta cuando se le da click a el boton "seleccionar", estrae la informaci贸n de la solicitud seleccionada */
    $(document).on("click",".seleccionar_Permiso",function (e){
        e.preventDefault();
        $("#Mdiv_revisarSolicitud").html("");
        var datos = { 
            
            id:  $(this).data('id') 
        };
        $.ajax({
            async: true,
            type: "POST",
            url: "pages/permisos/revision/actualizarSolicitud.php",
            data: datos,
            dataType: "html",
            success: function(data){
                var response = JSON.parse(data);
                var options = '';
                /* En esta seccion llenamos los datos que se van a editar el modal */
                for (var index = 0;index < response.length; index++) 
                {
                   options +=   '<div class="form-group">' +
                                    '<label style="display : none">Codigo del Permiso</label>' +
                                    '<input style="display : none" id="codPermiso" disabled = "true" data-id="' + response[index].codPermiso + '" placeholder="' + response[index].codPermiso + '" class="form-control">' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<label>Empleado: ' + response[index].nombreEmpleado + '</label>' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<label>Departamento: ' + response[index].departamento + '</label>' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<label>Motivo: ' + response[index].motivo + '</label>' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<label>Edificio donde se registra: ' + response[index].edificio + '</label>' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<label>Tipo de Permiso: ' + response[index].tipoPermiso + '</label>' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<label>Duraci贸n en d铆as: ' + response[index].cantidad + '</label>' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<label>Hora Inicio: ' + response[index].horaIn + '</label>' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<label>Hora Finalizaci贸n: ' + response[index].horaFin + '</label>' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<label>Fecha: ' + response[index].fecha + '</label>' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<label>Observaci贸n: ' + response[index].obs + '</label>' +
                                '</div>';
                }

                /* Insertamos dentro del div del modal los datos obtenidos en la base de datos */
                $("#Mdiv_revisarSolicitud").empty();
                $("#Mdiv_revisarSolicitud").append(options);

                /* Cuando ya tenemos listo el modal con los datos que queriamos abrimos el modal */
                $('#compose-modal-revisar').modal('show');
            },
            timeout: 10000
        });
        //cargarSolicitudes();
    }); //finaliza la carga de solicitudes en la modal

   $(document).on("click",".denegar_Permiso",function (e){
        e.preventDefault();

        $("#Mdiv_justificarD").html("");
        var datos = { 
            
            id:  $(this).data('id') 
        };
        $.ajax({
            async: true,
            type: "POST",
            url: "pages/permisos/revision/denegarSolicitud.php",
            data: datos,
            dataType: "html",
            success: function(data){
                var response = JSON.parse(data);
                var options = '';
       
                for (var index = 0;index < response.length; index++) 
                {
                   options +=   '<div class="form-group">' +
                                    '<label style="display : none">Codigo del permiso</label>' +
                                    '<input style="display : none" id="codPermiso2" disabled = "true" data-id="' + response[index].codPermiso + '" placeholder="' + response[index].codPermiso + '" class="form-control">' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<label>Justificacion</label>' +
                                    '<input id="obs1" placeholder = "' + response[index].obs + '" class="form-control" required>' +
                                '</div>';
                }

                $("#Mdiv_justificarD").empty();
                $("#Mdiv_justificarD").html(options);
                
                $('#compose-modal-denegar').modal('show');
            },
            timeout: 4000
        });
    });

    
    /* Esta funcion es llamada al principio, es la encargada de actualizar la tabla que muestra las solicitudes pendientes por revisar de cada uno de los jefes de departamento, que estan en el sistema en ese momento, se recarga cada vez que se deniega o se aprueba un permiso */
	function cargarSolicitudes(){
        var datos = {
            accion: 1
        };

        $.ajax({
            async: true,
            type: "POST",
            url: "pages/permisos/revision/cargarSolicitudes.php",
            data: datos,
            dataType: "html",
            success: function(data){
                var response = JSON.parse(data);
                var options = '';
                /* Seccion para llenar la tabla */
                for (var index = 0;index < response.length; index++) 
                {
                   options += '<tr>' +
                                    '<td style="display : none">' + response[index].idPermiso + '</td>' +
                                    '<td>' + response[index].nombreEmpleado + '</td>' +
                                    '<td>' + response[index].departamento + '</td>' +
                                    '<td>' + response[index].fecha + '</td>' +
                                    '<td><center>' +
                                        '<button data-id = "' + response[index].idPermiso +'" href= "#" class = "seleccionar_Permiso btn btn-info" data-target = ""><i class="glyphicon glyphicon-share-alt"></i></button>' +
                                    '</td></center>' +
                                    '<td><center>' +
                                        '<button data-id = "' + response[index].idPermiso +'" href= "#" class = "aprobar_Permiso btn btn-success" data-target = ""><i class="glyphicon glyphicon-thumbs-up"></i></button>' +
                                    '</td></center>' +
                                    '<td><center>' +
                                        '<button data-id = "' + response[index].idPermiso +'" href= "#" class = "denegar_Permiso btn btn-warning" data-target = ""><i class="glyphicon glyphicon-thumbs-down"></i></button>' +
                                    '</td></center>' +
                              '</tr>';
                }

            
                $("#cTablaPermisos").html(options);

                /* Script que permite a la tabla hacer busquedas dentro de ella
                    y ordenarla deacuerdo a lo que se presenta en ella. */

                    $('#TablePermisos').dataTable();
            },
            timeout: 15000
        }); 
    } //finaliza la funci贸n de carga de solicitudes


    /* Seccion que se manda a llamar cuando dentro de la modal damos click en el bot贸n de "Enviar", estipulando que la justificaci贸n se ha ingresado  */

    $("#form_Denegar").submit(function(e) {
        e.preventDefault();
        $("#compose-modal-denegar").modal('hide');
        datosRevisados = {
            codPermiso: $("#codPermiso2").data('id'),
            obs: $("#obs1").val(),
        };
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            data: datosRevisados,
            contentType: "application/x-www-form-urlencoded",
            url: "pages/permisos/revision/solicitudDenegada.php",
            success: function(data){ 
                $("#notificaciones1").html(data);
                $("#notificaciones1").fadeOut(5000);
                cargarSolicitudes();
            },
            timeout: 4000
        });
    });

    $("#form_Aprobar").submit(function(e) {
        e.preventDefault();
        $("#compose-modal-aprobar").modal('hide');
        datosRevisados = {
            codPermiso: $("#codPermiso1").data('id'),
            obs: $("#obs1").val(),
        };
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            data: datosRevisados,
            contentType: "application/x-www-form-urlencoded",
            url: "pages/permisos/revision/solicitudAprobada.php",
            success: function(data){ 
                $("#notificaciones1").html(data);
                $("#notificaciones1").fadeOut(5000);
                cargarSolicitudes();
            },
            timeout: 4000
        });
    });
});

</script>

<div id = "notificaciones1"></div>

<!-- Seccion usada para mostrar la tabla de las solicitudes de permiso que estan pendientes de revisi贸n en el sistema -->
<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Solicitudes Pendientes </label>
        </div>
        <div class="panel-body">
            <div>
                   <div id= "noti1" class="alert alert-info" role="alert"><center>Por favor seleccione la solicitud que desee revisar</center></div>
            </div>
        
            <!--<div class="panel-body">-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <!--<section class="content">-->
                                <div class="table-responsive">
                                    <table id= "TablePermisos" border="0" class='table table-bordered table-striped'>
                                        <thead>
                                            <tr>
                                                <th style = 'display:none'>Codigo</th>
                                                <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th> 
                                                <th style="text-align:center;background-color:#386D95;color:white;">Departamento</th>  
                                                <th style="text-align:center;background-color:#386D95;color:white;">Fecha</th>
                                                <th style="text-align:center;background-color:#386D95;color:white;">Seleccionar</th>
                                                <th style="text-align:center;background-color:#386D95;color:white;">Aprobar</th>
                                                <th style="text-align:center;background-color:#386D95;color:white;">Denegar</th>           
                                            </tr>
                                        </thead>
                                        <tbody id = "cTablaPermisos">
                                            <!-- Contenido de la tabla generado atravez de la consulta a 
                                                la base de datos -->
                                        </tbody>
                                    </table>       
                                </div>
                            <!--</section>-->
                        </div>                
                    </div>
                </div>       
            <!--</div>-->
        </div>
    </div>
</div>

<!-- Modal para revisar las solicitudes de permiso -->

<div  class="modal fade" id="compose-modal-revisar" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="form_Revisar" name="form_Revisar" action="#">
                <div class="modal-header" style = "background-color:#0FA6C3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel" style = "color:white">Detalle de la Solicitud</h4>
                </div>
                <div class = "modal-body">
                    <div id= "Mdiv_revisarSolicitud">
                        
                    </div>                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Volver</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal para justificar la denegaci贸n de las solicitudes de permiso -->

<div  class="modal fade" id="compose-modal-denegar" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="form_Denegar" name="form_Denegar" action="#">
                <div class="modal-header" style = "background-color:#0FA6C3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel" style = "color:white">Justificaci贸n de Solicitud de Permiso Denegada</h4>
                </div>
                <div class = "modal-body">
                    <div id= "Mdiv_justificarD">
                        
                    </div>                
                </div>
                <div class="modal-footer">
                    <button  id="enviarD" class="btn btn-warning">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal para justificar la aprobaci贸n de una solicitud de permiso -->
<!-- Modal para editar los Periodos -->

<div  class="modal fade" id="compose-modal-aprobar" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="form_Aprobar" name="form_Aprobar" action="#">
                <div class="modal-header" style = "background-color:#0FA6C3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel" style = "color:white">Observaci贸n a la Solicitud de Permiso Aprobada</h4>
                </div>
                <div class = "modal-body">
                    <div id= "Mdiv_justificarA">
                        
                    </div>                
                </div>
                <div class="modal-footer">
                    <button  id="enviarA" class="btn btn-warning">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>