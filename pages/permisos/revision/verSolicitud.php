<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    $pcPermiso = $_POST['id'];
    $query = "SELECT permisos.id_Permisos, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) as 'Empleado', departamento_laboral.nombre_departamento, motivos.descripcion_motivo, edificios.descripcion, tipodepermiso.tipo_permiso, permisos.dias_permiso, permisos.hora_inicio, permisos.hora_finalizacion, permisos.fecha_solicitud, permisos.observacion, permisos.documento, permisos.justificacion from permisos, persona, edificios, motivos, tipodepermiso, empleado, departamento_laboral WHERE permisos.No_Empleado=empleado.No_Empleado AND empleado.N_identidad=persona.N_identidad AND permisos.id_motivo=motivos.Motivo_ID AND permisos.id_tipo_permiso=tipodepermiso.id_tipo_permiso AND permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.id_Edificio_Registro=edificios.Edificio_ID and permisos.id_Permisos = '".$pcPermiso."'";

?>

<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3 class="modal-title" style="color:white">Información detallada de la solicitud</h3>
</div>

<div class="modal-body">
    <form class="form-horizontal" role="form" id="formVer" name="formVer">

	<?php
	    $result =$db->prepare($query);
	    $result->execute();
	    while ($fila = $result->fetch()) {
	?>
		    <div class="form-group">
		        <label style="display : none">Codigo del Permiso</label>
		        <input style="display : none" id="codPermiso" disabled = "true" value="<?php echo $fila['id_Permisos']; ?>" class="form-control">
		    </div>

		    <div class="form-group">
		        <label class="col-sm-8 control-label" style="text-align:left">Empleado: <?php echo $fila['Empleado']; ?></label>

		        <label class="col-sm-10 control-label" style="text-align:left">Departamento: <?php echo $fila['nombre_departamento']; ?></label>

		        <label class="col-sm-8 control-label" style="text-align:left">Motivo: <?php echo $fila['descripcion_motivo']; ?></label>

		        <label class="col-sm-8 control-label" style="text-align:left">Edificio donde se registra: <?php echo $fila['descripcion']; ?></label>

		        <label class="col-sm-8 control-label" style="text-align:left">Tipo de Permiso: <?php echo $fila['tipo_permiso']; ?></label>

		        <label class="col-sm-8 control-label" style="text-align:left">Duración en días: <?php echo $fila['dias_permiso']; ?></label>

		        <label class="col-sm-8 control-label" style="text-align:left">Horario: <?php echo $fila['hora_inicio']." - ".$fila['hora_finalizacion']; ?></label>

		        <label class="col-sm-8 control-label" style="text-align:left">Fecha: <?php echo $fila['fecha_solicitud']; ?></label>

		        <label class="col-sm-10 control-label" style="text-align:left">Justificación: <?php echo $fila['justificacion']; ?></label>

		        <label class="col-sm-10 control-label" style="text-align:left">Observación: <?php echo $fila['observacion']; ?></label>
		    </div>

		    <div class="form-group">
		        <label style="display : none">Documento</label>
		        <input style="display : none" id="doc" disabled = "true" value="<?php echo $fila['documento']; ?>" class="form-control">
		    </div>

		    <div class="modal-footer">
				<button type="button"  class="btn btn-default" data-dismiss="modal">Volver</button>
				<button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" <?php if($fila['documento']==NULL){ echo "disabled='true'"; } ?> ><span class="glyphicon glyphicon-folder-open"></span>  Documento </button>
			</div>
    <?php 
    	}
	?>
	</form>
</div>

<script type="text/javascript">

$(document).ready(function(){

    $("#formVer").submit(function(e) {

        var dcP = $("#doc").val();

        data = {
        	arch: dcP,
        	tp: 0
        }
        $.ajax({
	        async: true,
	        type: "POST",
	        dataType: "html",
	        contentType: "application/x-www-form-urlencoded",
	        success: abrirDoc,
	        timeout: 8000,
	        error: problemas
	    }); 

	    return false;

    });

});

</script>

<script type="text/javascript" src="pages/permisos/revision/ScriptRevision.js"></script>