<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir."conexion/config.inc.php");

    $pcPermiso = $_POST['id'];
    $pcEmpleado = $_POST['empleado'];
    $query = "SELECT permisos.id_Permisos, permisos.estado, permisos.observacion from permisos where permisos.id_Permisos = '".$pcPermiso."'";

?>


<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3 class="modal-title" style="color:white">Aprobación de la Gestión de Vacaciones</h3>
</div>
<div class="modal-body">
	<form class="form-horizontal" role="form" id="formAprobarGestion" name="formAprobarGestion">
		<div class="form-group col-sm-12" style="display:none">
		    <label>Código Permiso</label>
				<input id="idGestion" value = "<?php echo $pcPermiso; ?>" class="form-control" required disabled>
		</div>
		<?php 
			$result=$db->prepare($query);
			$result->execute();
			while($fila = $result->fetch()){
		?>
				<div class="form-group col-sm-12" style="display:none;">
				    <label>Estado</label>
					<input id="estadoG" value = "<?php echo $fila['estado']; ?>" class="form-control" required disabled>
				</div>
				<div class="form-group col-sm-12">
				    <label>Justificación</label>
				    <input id="obsG" placeholder = "<?php if($fila['observacion'] == NULL){ echo 'Agregar justificación u observación'; }else{echo $fila['observacion']; }  ?>" class="form-control">
				</div>
				<div class="form-group col-sm-12" style="display:none;">
				    <label>Empleado</label>
					<input id="emp" value = "<?php echo $pcEmpleado ?>" class="form-control" required disabled>
				</div>
	    <?php
	    	}
	    ?>
<br>
<br>

		<div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal"> Volver</button>
		    <button type = "submit" id="guardarCambiosGestion" class="btn btn-primary"> Enviar</button>
		</div>
	</form>
</div>

<script type="text/javascript" src="pages/permisos/revision/ScriptRevision.js"></script>