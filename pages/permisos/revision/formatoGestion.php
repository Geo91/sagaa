<?php  
    $maindir = "../../../";
    require_once($maindir."conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    if(isset($_POST['cdg']) && isset($_POST['nF'])) {
        session_start();
        $idUsuario = $_SESSION['user_id'];
        $id = $_POST['cdg'];
        $nF = $_POST['nF'];

        $correlativo = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
            
        try{
            $sql = "CALL SP_IMPRIMIR_PERMISO(?,?,?,@mensajeError)";
            $query1 = $db->prepare($sql);
            $query1->bindParam(1,$id,PDO::PARAM_INT);
            $query1->bindParam(2,$idUsuario,PDO::PARAM_INT);
            $query1->bindParam(3,$correlativo,PDO::PARAM_STR);

            $query1->execute();
            $output = $db->query("select @mensajeError")->fetch(PDO::FETCH_ASSOC);
            $mensaje = $output['@mensajeError'];

            if ($mensaje == NULL){
                echo mensajes('El formato de solicitud de vacaciones ha sido exportado éxitosamente', 'verde');
                echo "<script type='text/javascript'>
                        var id = '$id';
                        var nF = '$nF';
                        window.open('pages/permisos/revision/formatopdfvacaciones.php?id='+id+'&nf='+nF);
                    </script>";
            }else{
                echo mensajes('Error!'.$mensaje, 'rojo');
            }
        }catch(PDOexception $e){
            echo mensajes('Error!'. $e, 'rojo');
        }
    }else{
        echo mensajes("Hubo un error al intentar exportar el formato de solicitud. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
    }

?>

<script type="text/javascript">
    $("#TablaGestionesV").load('pages/permisos/revision/datosGestiones.php');
</script>