<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    $pcPermiso = $_POST['id'];
    $query = "SELECT permisos.id_Permisos, permisos.observacion from permisos where permisos.id_Permisos = '".$pcPermiso."'";

?>


<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3 class="modal-title" style="color:white">Denegación de la Gestión de Vacaciones</h3>
</div>
<div class="modal-body">
	<form class="form-horizontal" role="form" id="formDenegarGestion" name="form_DenegarGestion">
		<div class="form-group col-sm-12" style = "display:none">
		    <label>Justificación</label>
				<input id="idPermiso" value = "<?php echo $pcPermiso; ?>" class="form-control" required disabled>
		</div>
		<div class="form-group col-sm-12">
		    <label>Justificación</label>
		    <?php 
		    	$result=$db->prepare($query);
		    	$result->execute();
		    	while($fila = $result->fetch()){
		    ?>
				<input id="observacion" placeholder = "<?php if($fila['observacion'] == NULL){ echo 'Agregar justificación u observación'; }else{echo $fila['observacion']; }  ?>" class="form-control" required>
		    <?php
		    	}
		    ?>
		</div>
<br>
<br>
<br>
<br>
		<div class="modal-footer">
		    <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
		    <button type = "submit" id="guardarCambios" class="btn btn-primary"> Enviar</button>
		</div>
	</form>
</div>

<script type="text/javascript" src="pages/permisos/revision/ScriptRevision.js"></script>