<?php  
    $maindir = "../../../";
    require_once($maindir."conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    if(isset($_POST['codigo']) && isset($_POST['obs']) && isset($_POST['state'])){
        session_start();
        $idUsuario = $_SESSION['user_id'];
        $id = $_POST['codigo'];
        $obs = $_POST['obs'];
        $estadoP = $_POST['state'];

        try{
            $sql = "CALL SP_APROBAR_PERMISO(?,?,?,?,@mensajeError)";
            $query1 = $db->prepare($sql);      
            $query1->bindParam(1,$id,PDO::PARAM_INT);
            $query1->bindParam(2,$obs,PDO::PARAM_STR); 
            $query1->bindParam(3,$idUsuario,PDO::PARAM_INT);
            $query1->bindParam(4,$estadoP,PDO::PARAM_INT); 

            $query1->execute();
            $output = $db->query("select @mensajeError")->fetch(PDO::FETCH_ASSOC);
            $mensaje = $output['@mensajeError'];

            if(is_null($mensaje)){
                echo mensajes("La solicitud seleccionada ha sido aprobada éxitosamente", "verde");
            }else{
                echo mensajes("Error! ". $mensaje, "rojo");
            }

        }catch(PDOExecption $e){
            echo mensajes("Error: ".$e, "rojo");
        }
    }else{
        echo mensajes("Hubo un error al intentar aprobar la solicitud. Intentelo de nuevo o contacte al administrador del sistema", "azul");
    } 
    
?>

<script type="text/javascript">
    $("#TablaSolicitudes").load('pages/permisos/revision/DatosSolicitudes.php');
</script>