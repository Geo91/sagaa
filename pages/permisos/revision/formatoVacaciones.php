<?php  
    $maindir = "../../../";

    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."phpword/PHPWord.php");
    require_once($maindir."Datos/funciones.php");
    session_start();
    $idUsuario = $_SESSION['user_id'];

    $PHPWord = new PHPWord();

    // Every element you want to append to the word document is placed in a section. So you need a section:
    $section = $PHPWord->createSection();

    // After creating a section, you can append elements:
    $section->addText('Hello world!');

    $objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
    $objWriter->save('helloWorld.docx');

?>

