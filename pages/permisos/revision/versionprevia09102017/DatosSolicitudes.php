<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $idUsuario = $_SESSION['user_id'];
    $idRol = $_SESSION['user_rol'];

    if($idRol == 50)
    {
        $query = "SELECT permisos.id_Permisos, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) AS 'Empleado', departamento_laboral.nombre_departamento, permisos.fecha_solicitud, permisos.dias_permiso, permisos.jefe_inmediato FROM permisos, persona, departamento_laboral, empleado WHERE permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND empleado.N_identidad=persona.N_identidad AND (permisos.fecha_solicitud BETWEEN DATE_SUB(CURDATE(), INTERVAL 60 DAY) AND DATE_ADD(CURDATE(), INTERVAL 15 DAY)) AND (permisos.estado = 3 OR (permisos.estado = 1 AND permisos.jefe_inmediato IN (SELECT No_Empleado FROM usuario WHERE id_Usuario = '".$idUsuario."'))) ORDER BY permisos.fecha_solicitud DESC";
    }else{
        if($idRol == 60){
            $query = "SELECT permisos.id_Permisos, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) AS 'Empleado', departamento_laboral.nombre_departamento, permisos.fecha_solicitud FROM permisos, persona, departamento_laboral, empleado WHERE permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND empleado.N_identidad=persona.N_identidad AND (permisos.estado = 2 OR permisos.jefe_inmediato IN (SELECT No_Empleado FROM usuario WHERE id_Usuario = '".$idUsuario."'))";
        }else{
            $query = "SELECT permisos.id_Permisos, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) AS 'Empleado', departamento_laboral.nombre_departamento, permisos.fecha_solicitud FROM permisos, persona, departamento_laboral, empleado WHERE permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND empleado.N_identidad=persona.N_identidad AND (permisos.dias_permiso < 3 AND permisos.estado = 1 AND permisos.jefe_inmediato IN (SELECT No_Empleado FROM usuario WHERE id_Usuario = '".$idUsuario."'))";
        } 
    }
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <!--<section class="content">-->
                <div class="table-responsive">
                    <table id= "TablaPermisos" border="0" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th style = 'display:none'>Codigo</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th> 
                                <th style="text-align:center;background-color:#386D95;color:white;">Departamento</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Fecha</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Seleccionar</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Aprobar</th>
                                <th style="text-align:center;background-color:#386D95;color:white;">Denegar</th>           
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Contenido de la tabla generado atravez de la consulta a 
                                la base de datos -->
                                <?php
                                    $result =$db->prepare($query);
                                    $result->execute();
                                    while ($fila = $result->fetch()) {
                                        ?>
                                        <tr data-id='<?php echo $fila["id_Permisos"]; ?>'>
                                            <td style = "display:none"><?php echo $fila["id_Permisos"]; ?></td>
                                            <td><?php echo $fila["Empleado"]; ?></td>
                                            <td><?php echo $fila["nombre_departamento"]; ?></td>
                                            <td><?php echo $fila["fecha_solicitud"]; ?></td>
                                            <td>
                                                <center>
                                                    <button type="button" class="verSolicitud<?php echo $auto_increment; ?> btn btn-warning glyphicon glyphicon-share-alt"  title="Ver información detallada de la solicitud."></button>
                                                </center>
                                            </td>
                                            <td>
                                                <center>
                                                    <button type="button" class="aprobarSolicitud<?php echo $auto_increment; ?> btn btn-success glyphicon glyphicon-thumbs-up""  title="Aprobar la solicitud de permiso."></button>
                                                </center>
                                            </td>     
                                            <td>
                                                <center>
                                                    <button type="button" class="denegarSolicitud<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-thumbs-down""  title="Denegar la solicitud de permiso."></button>
                                                </center>
                                            </td>     
                                        </tr>
                                <?php } ?>
                        </tbody>
                    </table>       
                </div>
            <!--</section>-->
        </div>                
    </div>
</div>

<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#TablaPermisos')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#TablaPermisos').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });

$(document).on("click", ".verSolicitud<?php echo $auto_increment; ?>", function () {
    data={
        id:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: abrirSolicitud,
        timeout: 6000,
        error: problemas
    }); 
    return false;
});


$(document).on("click", ".aprobarSolicitud<?php echo $auto_increment; ?>", function () {
    data={
        id:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        beforeSend: inicioEnvio,
        success: aprobarSolicitud,
        timeout: 6000,
        error: problemas
    }); 
    return false;
    
});

$(document).on("click", ".denegarSolicitud<?php echo $auto_increment; ?>", function () {
    data={
        id:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        beforeSend: inicioEnvio,
        success: denegarSolicitud,
        timeout: 6000,
        error: problemas
    }); 
    return false;
});

</script>
