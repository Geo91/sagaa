
$(document).ready(function(){  

    $("#formAprobar").submit(function(e) {
        e.preventDefault();
        data={
            codigo:$('#idPermiso').val(),
            state:$('#estado').val(),
            obs:$('#observacion').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: solicitudAprobada,
            timeout: 6000,
            error: problemas
        }); 
        return false;
    });

    $("#formDenegar").submit(function(e) {
        e.preventDefault();
        data={
            codigo:$('#idPermiso').val(),
            obs:$('#observacion').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: solicitudDenegada,
            timeout: 6000,
            error: problemas
        }); 
        return false;
    });
    
});

function abrirSolicitud(){
    $("#contenidoModal").load('pages/permisos/revision/verSolicitud.php',data);
    $("#modalSolicitud").modal('show');
}

function aprobarSolicitud(){
    $("#contenidoModal").load('pages/permisos/revision/aprobarSolicitud.php',data);
    $("#modalSolicitud").modal('show');
}

function denegarSolicitud(){
    $("#contenidoModal").load('pages/permisos/revision/denegarSolicitud.php',data);
    $("#modalSolicitud").modal('show');
}

function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function solicitudAprobada(){
    $("#notificaciones1").empty();
    $("#notificaciones1").load('pages/permisos/revision/solicitudAprobada.php',data);
    $("#modalSolicitud").modal('hide');
}

function solicitudDenegada(){
    $("#notificaciones1").empty();
    $("#notificaciones1").load('pages/permisos/revision/solicitudDenegada.php',data);
    $("#modalSolicitud").modal('hide');
}