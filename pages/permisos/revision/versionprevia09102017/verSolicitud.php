<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    $pcPermiso = $_POST['id'];
    $query = "SELECT permisos.id_Permisos, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) as 'Empleado', departamento_laboral.nombre_departamento, motivos.descripcion_motivo, edificios.descripcion, tipodepermiso.tipo_permiso, permisos.dias_permiso, permisos.hora_inicio, permisos.hora_finalizacion, permisos.fecha_solicitud, permisos.observacion from permisos, persona, edificios, motivos, tipodepermiso, empleado, departamento_laboral WHERE permisos.No_Empleado=empleado.No_Empleado AND empleado.N_identidad=persona.N_identidad AND permisos.id_motivo=motivos.Motivo_ID AND permisos.id_tipo_permiso=tipodepermiso.id_tipo_permiso AND permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.id_Edificio_Registro=edificios.Edificio_ID and permisos.id_Permisos = '".$pcPermiso."'";

?>

<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3 class="modal-title" style="color:white">Información detallada de la solicitud</h3>
</div>
<div class="modal-body">
	<?php
        $result =$db->prepare($query);
        $result->execute();
        while ($fila = $result->fetch()) {
    ?>
		    <div class="form-group">
		        <label style="display : none">Codigo del Permiso</label>
		        <input style="display : none" id="codPermiso" disabled = "true" data-id="<?php echo $fila['id_Permisos']; ?>" class="form-control">
		    </div>

		    <div class="form-group">
		        <label>Empleado: <?php echo $fila['Empleado']; ?></label>
		    </div>

		    <div class="form-group">
		        <label>Departamento: <?php echo $fila['nombre_departamento']; ?></label>
		    </div>

		    <div class="form-group">
		        <label>Motivo: <?php echo $fila['descripcion_motivo']; ?></label>
		    </div>

		    <div class="form-group">
		        <label>Edificio donde se registra: <?php echo $fila['descripcion']; ?></label>
		    </div>

		    <div class="form-group">
		        <label>Tipo de Permiso: <?php echo $fila['tipo_permiso']; ?></label>
		    </div>

		    <div class="form-group">
		        <label>Duración en días: <?php echo $fila['dias_permiso']; ?></label>
		    </div>

		    <div class="form-group">
		        <label>Hora Inicio: <?php echo $fila['hora_inicio']; ?></label>
		    </div>

		    <div class="form-group">
		        <label>Hora Finalización: <?php echo $fila['hora_finalizacion']; ?></label>
		    </div>

		    <div class="form-group">
		        <label>Fecha: <?php echo $fila['fecha_solicitud']; ?></label>
		    </div>

		    <div class="form-group">
		        <label>Observación: <?php echo $fila['observacion']; ?></label>
		    </div>
    <?php 
    	}
	?>
</div>

<div class="modal-footer">
    <button type="button"  class="btn btn-default" data-dismiss="modal">Salir</button>
</div>
