<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $idUsuario = $_SESSION['user_id'];
    $idRol = $_SESSION['user_rol'];

    if($idRol == 50)
    {
        $query = "SELECT permisos.id_Permisos, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) AS 'Empleado', departamento_laboral.nombre_departamento, permisos.fecha_solicitud, permisos.dias_permiso, permisos.jefe_inmediato FROM permisos, persona, departamento_laboral, empleado WHERE permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND empleado.N_identidad=persona.N_identidad AND (permisos.fecha_solicitud BETWEEN DATE_SUB(CURDATE(), INTERVAL 60 DAY) AND DATE_ADD(CURDATE(), INTERVAL 15 DAY)) AND (permisos.estado = 3 OR (permisos.estado = 1 AND permisos.jefe_inmediato IN (SELECT No_Empleado FROM usuario WHERE id_Usuario = '".$idUsuario."'))) ORDER BY permisos.fecha_solicitud DESC";
    }else{
        if($idRol == 60){
            $query = "SELECT permisos.id_Permisos, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) AS 'Empleado', departamento_laboral.nombre_departamento, permisos.fecha_solicitud FROM permisos, persona, departamento_laboral, empleado WHERE permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND empleado.N_identidad=persona.N_identidad AND (permisos.estado = 2 OR permisos.jefe_inmediato IN (SELECT No_Empleado FROM usuario WHERE id_Usuario = '".$idUsuario."'))";
        }else{
            $query = "SELECT permisos.id_Permisos, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) AS 'Empleado', departamento_laboral.nombre_departamento, permisos.fecha_solicitud FROM permisos, persona, departamento_laboral, empleado WHERE permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.No_Empleado=empleado.No_Empleado AND empleado.N_identidad=persona.N_identidad AND (permisos.dias_permiso < 3 AND permisos.estado = 1 AND permisos.jefe_inmediato IN (SELECT No_Empleado FROM usuario WHERE id_Usuario = '".$idUsuario."'))";
        } 
    }
?>

<div id = "notificaciones1"></div>

<!-- Seccion usada para mostrar la tabla de las solicitudes de permiso que estan pendientes de revisión en el sistema -->
<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Solicitudes Pendientes </label>
        </div>
        <div class="panel-body">
            <div>
                <div id= "noti1" class="alert alert-info" role="alert"><center>Por favor seleccione la solicitud que desee revisar</center></div>
            </div>

            <div id = "TablaSolicitudes">
                <?php include("DatosSolicitudes.php"); ?>
            </div>   
                
        </div>
    </div>
</div>

<!-- Modal para revisar las solicitudes de permiso -->
<div class="modal fade" id="modalSolicitud" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div id="contenidoModal">
        </div>
      </div>
    </div>
</div>


<script type="text/javascript" src="pages/permisos/revision/ScriptRevision.js"></script>