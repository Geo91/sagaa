<?php
    $mkdir = "../../../";
    include($mkdir."conexion/config.inc.php");
    $pcPermiso = $_POST['id'];
    $query = "SELECT permisos.id_Permisos, CONCAT(persona.Primer_nombre, ' ', persona.Segundo_nombre, ' ', persona.Primer_apellido, ' ', persona.Segundo_apellido) as 'Empleado', departamento_laboral.nombre_departamento, motivos.descripcion_motivo, edificios.descripcion, tipodepermiso.tipo_permiso, permisos.dias_permiso, permisos.hora_inicio, permisos.hora_finalizacion, permisos.fecha, permisos.observacion from permisos, persona, edificios, motivos, tipodepermiso, empleado, departamento_laboral WHERE permisos.No_Empleado=empleado.No_Empleado AND empleado.N_identidad=persona.N_identidad AND permisos.id_motivo=motivos.Motivo_ID AND permisos.id_tipo_permiso=tipodepermiso.id_tipo_permiso AND permisos.id_departamento=departamento_laboral.Id_departamento_laboral AND permisos.id_Edificio_Registro=edificios.Edificio_ID and permisos.id_Permisos = '".$pcPermiso."'";
    $result = mysql_query($query);
    $json = array();
    $contadorIteracion = 0;
    while ($fila = mysql_fetch_array($result)) { 
        $json[$contadorIteracion] = array
            (
                "CodPermiso" => utf8_encode($fila["id_Permisos"]),
                "nombreEmpleado" => utf8_encode($fila["Empleado"]),
                "departamento" => utf8_encode($fila["nombre_departamento"]),
                "motivo" => utf8_encode($fila["descripcion_motivo"]),
                "edificio" => utf8_encode($fila["descripcion"]),
                "tipoPermiso" => utf8_encode($fila["tipo_permiso"]),
                "cantidad" => utf8_encode($fila["dias_permiso"]),
                "horaIn" => utf8_encode($fila ["hora_inicio"]),
                "horaFin" => utf8_encode($fila["hora_finalizacion"]),
                "fecha" => utf8_encode($fila["fecha"]),
                "obs" => utf8_encode($fila["observacion"])
            );

        $contadorIteracion++;
    }

    //Retornamos el jason con todos los elmentos tomados de la base de datos.
    echo json_encode($json);

?>