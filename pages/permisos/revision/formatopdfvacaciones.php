<?php

    $maindir = "../../../";

    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."funciones/timeout.php");
    require_once($maindir."fpdf/fpdf.php");

    $idV = $_GET['id'];
    $nF = $_GET['nf'];

    $consulta = "SELECT permisos.No_Empleado, CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'Empleado', persona.Sexo, vacaciones.año_actual, vacaciones.dias_año_actual, vacaciones.año_anterior, vacaciones.dias_año_anterior, vacaciones.total_dias, vacaciones.fecha_Inicio, vacaciones.fecha_fin FROM persona INNER JOIN (empleado INNER JOIN (permisos LEFT JOIN vacaciones ON permisos.id_Permisos=vacaciones.id_solicitud) ON empleado.No_Empleado=permisos.No_Empleado) ON persona.N_identidad=empleado.N_identidad WHERE permisos.id_Permisos = :cV";

    $query = $db->prepare($consulta);
    $query ->bindParam(":cV",$idV);
    $query->execute();
    $result = $query->fetch();

    $nomEmp = $result['Empleado'];
    $nEmp = $result['No_Empleado'];
    $gen = $result['Sexo'];
    $actual = $result['año_actual'];
    $anterior = $result['año_anterior'];
    $dActual = $result['dias_año_actual'];
    $dAnterior = $result['dias_año_anterior'];
    $total = $result['total_dias'];
    $fInicio = $result['fecha_Inicio'];
    $fFin = $result['fecha_fin'];

    function mes($fecha){
        switch (date('m',strtotime($fecha))) {
            case '01':
                $mes = 'Enero';
                break;
            case '02':
                $mes = 'Febrero';
                break;
            case '03':
                $mes = 'Marzo';
                break;
            case '04':
                $mes = 'Abril';
                break;
            case '05':
                $mes = 'Mayo';
                break;
            case '06':
                $mes = 'Junio';
                break;
            case '07':
                $mes = 'Julio';
                break;
            case '08':
                $mes = 'Agosto';
                break;
            case '09':
                $mes = 'Septiembre';
                break;
            case '10':
                $mes = 'Octubre';
                break;
            case '11':
                $mes = 'Noviembre';
                break;
            case '12':
                $mes = 'Diciembre';
                break;
        }
        return $mes;
    }

    function dia($fecha){
        switch (date('l', strtotime(($fecha)))) {
            case 'Monday':
                $dia = 'Lunes';
                break;
            case 'Tuesday':
                $dia = 'Martes';
                break;
            case 'Wednesday':
                $dia = 'Miércoles';
                break;
            case 'Thursday':
                $dia = 'Jueves';
                break;
            case 'Friday':
                $dia = 'Viernes';
                break;
            case 'Saturday':
                $dia = 'Sábado';
                break;
            case 'Sunday':
                $dia = 'Domingo';
                break;
        }
        return $dia;
    }

    function unidad($numeroU){
        switch ($numeroU){
        case 9:
            $numu = "Nueve";
            break;
        case 8:
            $numu = "Ocho";
            break;
        case 7:
            $numu = "Siete";
            break;
        case 6:
            $numu = "Seis";
            break;
        case 5:
            $numu = "Cinco";
            break;
        case 4:
            $numu = "Cuatro";
            break;
        case 3:
            $numu = "Tres";
            break;
        case 2:
            $numu = "Dos";
            break;
        case 1:
            $numu = "Uno";
            break;
        case 0:
            $numu = "";
            break;
        }
        return $numu;
    }

    function decena($numdero){

        if ($numdero >= 90 && $numdero <= 99){
            $numd = "Noventa";
            if ($numdero > 90){
                $numd = $numd." y ".(unidad($numdero - 90));
            }        
        }
        else if ($numdero >= 80 && $numdero <= 89){
            $numd = "Ochenta";
            if ($numdero > 80){
                $numd = $numd." y ".(unidad($numdero - 80));
            }
        }
        else if ($numdero >= 70 && $numdero <= 79){
            $numd = "Setenta";
            if ($numdero > 70){
                $numd = $numd." y ".(unidad($numdero - 70));
            }
        }
        else if ($numdero >= 60 && $numdero <= 69){
            $numd = "Sesenta";
            if ($numdero > 60){
                $numd = $numd." y ".(unidad($numdero - 60));
            }
        }
        else if ($numdero >= 50 && $numdero <= 59){
            $numd = "Cincuenta";
            if ($numdero > 50){
                $numd = $numd." y ".(unidad($numdero - 50));
            }
        }
        else if ($numdero >= 40 && $numdero <= 49){
            $numd = "Cuarenta";
            if ($numdero > 40){
                $numd = $numd." y ".(unidad($numdero - 40));
            }
        }
        else if ($numdero >= 30 && $numdero <= 39){
            $numd = "Treinta";
            if ($numdero > 30){
                $numd = $numd." y ".(unidad($numdero - 30));
            }
        }
        else if ($numdero >= 20 && $numdero <= 29){
            if ($numdero == '20'){
                $numd = "Veinte";
            }
            else{
                $numd = "Veinti".(unidad($numdero - 20));
            }
        }
        else if ($numdero >= 10 && $numdero <= 19){
            switch ($numdero){
                case 10:
                    $numd = "Diez";
                    break;
                case 11:
                    $numd = "Once";
                    break;
                case 12:
                    $numd = "Doce";
                    break;
                case 13:
                    $numd = "Trece";
                    break;
                case 14:
                    $numd = "Catorce";
                    break;
                case 15:
                    $numd = "Quince";
                    break;
                case 16:
                    $numd = "Dieciseis";
                    break;
                case 17:
                    $numd = "Diecisiete";
                    break;
                case 18:
                    $numd = "Dieciocho";
                    break;
                case 19:
                    $numd = "Diecinueve";
                    break;
            }
        }
        else{
            $numd = unidad($numdero);
        }
        return $numd;
    } //Fin de la función decena

class PDF extends FPDF{
     // Cabecera de página
         
    function Footer(){
        $maindir = "../../../";
        $idPermiso = $_GET['id'];
        $consulta2 = "SELECT permisos.correlativo from permisos where permisos.estado = 5 AND permisos.id_Permisos='".$idPermiso."'";
        $resultado2 = mysql_query($consulta2);
        $result2 = mysql_fetch_array($resultado2);
        $cltv = $result2['correlativo'];

    }
         
}


$pdf = new PDF();
$pdf->AliasNbPages();


    $pdf->AddPage('P','Letter',0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Image($maindir.'assets/img/enc_doc.png', 2,1,211,32, 'PNG'); //Encabezado
    $pdf->Image($maindir.'assets/img/pieDoc.png', 5,148,208,130, 'PNG'); //Pie de página
    $pdf->Cell(160);
    $pdf->Cell(0, 15, 'Tel: 2216-5100 ', 0,0,"R");
    $pdf->Ln(4);
    $pdf->Cell(0, 15, 'Edificio A-2 ', 0,0,"R");
    $pdf->Ln(20);
    $pdf->SetFont('Arial', 'I', 12);
    $pdf->Cell(0, 8, utf8_decode($nF), 0,0,"C");
    $pdf->Ln(6);
    $pdf->SetFont('Arial', 'I', 12);
    $pdf->Cell(0, 8, utf8_decode('Tegucigalpa M.D.C, '.date('d').' de '.mes(date('Y-m-d')).' del '.date('Y')), 0,0,"C");
    $pdf->SetFont('Arial', '', 12);
    
    $pdf->Ln(15);
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode('Máster'), 0,0,"");
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B','12');
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode('Jacinta Ruíz'), 0,0,"");
    $pdf->Ln(6);
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode('Secretaria Ejecutiva de Desarrollo de Personal'), 0,0,"");
    $pdf->Ln(6);
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode('UNAH'), 0,0,"");
    $pdf->Ln(6);
    $pdf->SetFont('Arial', '', '12');
    $pdf->Cell(12);
    $pdf->Cell(0, 10, utf8_decode('Su oficina'), 0,0,"");

    $pdf->Ln(15);
    $pdf->Cell(12);
    $pdf->Cell(0,10,utf8_decode('Estimada Máster Ruíz:'), 0,0,"");

    $pdf->Ln(13);
    $pdf->Cell(12);

    if($gen == 'F'){
        $texto = 'la Empleada '.$nomEmp;
    }else{
        $texto = 'el Empleado '.$nomEmp;
    }

    if($dActual > 0 && $dAnterior > 0){
        $pdf->MultiCell(170, 7,utf8_decode('Muy atentamente, me permito solicitar la autorización de '.$total.' ('.decena($total).') días de vacaciones, de los cuáles '.$dAnterior.' ('.decena($dAnterior).') corresponden al año '.$anterior.' y '.$dActual.' ('.decena($dActual).') corresponden al año 2017, a '.$texto.', con número de empleado '.$nEmp.', mismas que tomará a partir del '.dia($fInicio).' '.date('d',strtotime($fInicio)).' de '.mes($fInicio).' al '.dia($fFin).' '.date('d',strtotime($fFin)).' de '.mes($fFin).' de '.$actual.'.'), 0,"J",0);
    }else{
        if($dAnterior > 0){
            $pdf->MultiCell(170, 7,utf8_decode('Muy atentamente, me permito solicitar la autorización de '.$total.' ('.decena($total).') días de vacaciones correspondientes al año '.$anterior.', a '.$texto.', con número de empleado '.$nEmp.', mismas que tomará a partir del '.dia($fInicio).' '.date('d',strtotime($fInicio)).' de '.mes($fInicio).' al '.dia($fFin).' '.date('d',strtotime($fFin)).' de '.mes($fFin).' de '.$actual.'.'), 0,"J",0);
        }else{
            $pdf->MultiCell(170, 7,utf8_decode('Muy atentamente, me permito solicitar la autorización de '.$total.' ('.decena($total).') días de vacaciones correspondientes al año '.$dActual.', a '.$texto.', con número de empleado '.$nEmp.', mismas que tomará a partir del '.dia($fInicio).' '.date('d',strtotime($fInicio)).' de '.mes($fInicio).' al '.dia($fFin).' '.date('d',strtotime($fFin)).' de '.mes($fFin).' de '.$actual.'.'), 0,"J",0);
        }
    }
    
    $pdf->Ln(8);
    $pdf->Cell(12);
    $pdf->MultiCell(170, 7,utf8_decode('Se adjunta la nota de cálculo de vacaciones extendida por el Departamento de Estrategia Laboral.'), 0,"J",0);

    $pdf->Ln(8);
    $pdf->Cell(12);
    $pdf->MultiCell(170, 7,utf8_decode('En espera de su respuesta, agradezco su valiosa colaboración y me suscribo con las muestras de mi consideración y estima.'), 0,"J",0);

    $pdf->Ln(7);
    $pdf->Cell(12);
    $pdf->Cell(0,10,'Atentamente,',0,0,"");
    $pdf->Ln(20);
    $pdf->SetFont('Arial', 'IB', 12);
    $pdf->Cell(0, 8, utf8_decode('ABG. BESSY MARGOTH NAZAR HERRERA'), 0,0,"C");
    $pdf->Ln(6);
    $pdf->Cell(0, 8, utf8_decode('FACULTAD DE CIENCIAS JURÍDICAS'), 0,0,"C");
    $pdf->Ln(6);
    $pdf->Cell(0, 8, utf8_decode('DECANA'), 0,0,"C");

    $pdf->Ln(12);
    $pdf->SetFont('Arial','I','8');
    $pdf->Cell(12);
    $pdf->Cell(0,4,utf8_decode('cc. Departamento de Efectividad.'));
    $pdf->Ln(3);
    $pdf->Cell(12);
    $pdf->Cell(0,4,utf8_decode('cc. Administración FCJ.'));
    $pdf->Ln(3);
    $pdf->Cell(12);
    $pdf->Cell(0,4,utf8_decode('cc. Interesado'));
    $pdf->Ln(3);
    $pdf->Cell(12);
    $pdf->Cell(0,4,utf8_decode('cc. Archivo'));
    

    $pdf->Output('Permiso.pdf','D');


?>