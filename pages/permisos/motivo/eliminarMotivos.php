<?php

    $maindir = "../../../";
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Codigo'])){
        
        $Codigo = $_POST['Codigo'];

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        try {
            $sql = "CALL SP_ELIMINAR_MOTIVOS(?,@mensajeError)";
            $query1 = $db->prepare($sql);
            $query1->bindParam(1,$Codigo,PDO::PARAM_STR);
            $query1->execute();

            $output = $db->query("select @mensajeError")->fetch(PDO::FETCH_ASSOC);
            $mensaje = $output['@mensajeError'];

            if(is_null($mensaje)){
                echo mensajes("Se ha eliminando el registro seleccionado","verde");
            }
            else{
                echo mensajes("Error! ". $mensaje, "rojo");
            }
            
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }else{
        echo mensajes("Ha ocurrido un error del servidor...","rojo");
    }

?>

<script type="text/javascript">
    $("#MotivosTabla").load('pages/permisos/motivo/DatosMotivos.php');
    /*$(document).ready(function(){
        setTimeout(function(){
            $('#mensaje').fadeOut(3000);
        },3000);
    });*/
</script>