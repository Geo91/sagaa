<?php  
    $maindir = "../../../";
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Nombre']) ){
    
        $pcnombre = $_POST['Nombre'];

        try{
            $sql = "CALL SP_REGISTRAR_MOTIVO(?,@mensajeError)";
            $query1 = $db->prepare($sql);
            $query1->bindParam(1,$pcnombre,PDO::PARAM_STR);
            $query1->execute();

            $output = $db->query("select @mensajeError")->fetch(PDO::FETCH_ASSOC);
            $mensaje = $output['@mensajeError'];
            
            if(is_null($mensaje)){
                echo mensajes("El registro se ha guardado exitosamente","verde");
            }else{
                echo mensajes("El registro no pudo ingresarse", "rojo");
            }

        }catch(PDOExecption $e){
            echo mensaje("Error!  ".$e->getMessage(), "rojo");
        }
    }else{
        echo mensajes("Hubo un error al intentar guardar el registro. Intentelo de nuevo", "rojo");
    }
    
?>

<script type="text/javascript">
    $("#MotivosTabla").load('pages/permisos/motivo/DatosMotivos.php');
    $('#formMotivos').trigger("reset");
</script>