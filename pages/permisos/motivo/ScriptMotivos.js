//cargar la tabla desde un principio
    $("#MotivosTabla").load('pages/permisos/motivo/DatosMotivos.php');

$(document).ready(function(){   

//para cuando se da submit al formulario formAsignatura
    $("#formMotivos").submit(function(e) {
        e.preventDefault();
        data={
            Nombre:$('#NombreMotivo').val()                
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: agregarMotivo,
            timeout: 6000,
            error: problemas
        }); 
        return false;
    });


//para cuando se da submit al formulario formAsignatura
    $("#formEditarMotivo").submit(function(e) {
        e.preventDefault();
        data={
            Codigo:$('#modalCodigo').val(),
            Nombre:$('#modalNombre').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: editarMotivo,
            timeout: 6000,
            error: problemas
        }); 
        $("#modalEditar").modal('hide');
        return false;
    });

});

//para editar una asignatura
$(document).on("click", ".editarMotivo", function () {
    codigo = $(this).parents("tr").find("td").eq(0).html();
    Nombre = $(this).parents("tr").find("td").eq(1).html();

    $("#modalCodigo").val(codigo);
    $("#modalNombre").val(Nombre);

    $("#modalEditar").modal('show');
});


function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function agregarMotivo(){
    $("#divRespuesta").load('pages/permisos/motivo/insertarMotivo.php',data);
    $('#formMotivos').trigger("reset");
}

function editarMotivo(){
    $("#divRespuestaEditar").load('pages/permisos/motivo/editarMotivos.php',data);
}

function eliminarMotivo(){
    $("#divRespuestaEditar").load('pages/permisos/motivo/eliminarMotivos.php',data);
}