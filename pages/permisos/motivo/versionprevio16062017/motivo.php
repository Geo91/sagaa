
<script type="text/javascript">
   /* Script que permite cargar todos los elementos de la tabla para mostrar los datos que estan
    Almacenados en la base de datos */
$(document).ready(function() {

    //inicializarTabla(); //Se inicializa la tabla que despliega los datos de la base de datos
    cargarMotivos(); //Hacemos el llamado para que se cargue la tabla siempre que se cargue la pag.

    /* Este evento se levanta cada vez que le damos click al bottton eliminar */
    $(document).on("click",".elimina_motivo",function () {
             
        var respuesta = confirm("¿Esta seguro de que desea eliminar el registro seleccionado?");
        if (respuesta)
        {
            var id = $(this).data('id');

            var data = 
            {
                codigo: id
            };
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                data: data,
                url: "pages/permisos/motivo/eliminarMotivos.php",
                timeout: 4000,
                contentType: "application/x-www-form-urlencoded",
                success: function(data){
                    $("#notificaciones1").html(data);
                    $("#notificaciones1").fadeOut(1500);
                    cargarMotivos();
                }
            });
        }                 
    });


 /* Este evento se levanta cuando se le da click a el boton de editar */
   $(document).on("click",".editar",function (e){
        e.preventDefault();
        $("#Mdiv_editarMotivos").html("");
        var datos = { 
            
            id:  $(this).data('id') 
        };
        $.ajax({
            async: true,
            type: "POST",
            url: "pages/permisos/motivo/actualizarMotivos.php",
            data: datos,
            dataType: "html",
            success: function(data){
                var response = JSON.parse(data);
                var options = '';
                /* En esta seccion llenamos los datos que se van a editar el modal*/ 
                for (var index = 0;index < response.length; index++) 
                {
                   options +=   '<div class="form-group">'+
                                    '<label style="display : none">Codigo del Motivo</label>'+
                                    '<input style="display : none" id="codMotivo" disabled = "true" data-id="' + response[index].codMotivo + '" placeholder="' + response[index].codMotivo + '" class="form-control">'+
                                '</div>'+
                                '<div class="form-group">'+
                                    '<label>Descripción del Motivo</label>'+
                                    '<input id="descMotivo1" placeholder="'+response[index].descMotivo+'" class="form-control" required>'+
                                '</div>';
                }

                /* Insertamos dentro del div del modal los datos obtenidos en la base de datos */
                $("#Mdiv_editarMotivos").empty();
                $("#Mdiv_editarMotivos").append(options);

                /* Cuando ya tenemos listo el modal con los datos que queriamos abrimos el modal */
                $('#compose-modal-modificar').modal('show');
            },
            timeout: 4000
        });

    });

    /* Evento se levanta cuando queremos insertar un nuevo motivo */
    $("#formInsertar").submit(function (e){
        e.preventDefault();

        var datos = {
            descMotivo: $("#descMotivo").val()
        }
        $.ajax({
            async: true,
            type: "POST",
            url: "pages/permisos/motivo/insertarMotivo.php",
            data: datos,
            dataType: "html",
            success: function(data){   
                $("#notificaciones").html(data);
                $("#notificaciones").fadeOut(4500);
                cargarMotivos();
            },
            timeout: 4000
        });

    });

    /* Esta funcion es llamada al principio, es la encargada de actualizar la tabla que muestra
        las ciudades que estan en el sistema en ese momento, se recarga cada vez que se elimina o se 
        actualiza */
    function cargarMotivos(){
        var datos = {
            accion: 1
         };

        $.ajax({
            async: true,
            type: "POST",
            url: "pages/permisos/motivo/cargarMotivos.php",
            data: datos,
            dataType: "html",
            success: function(data){
                var response = JSON.parse(data);
                var options = '';
                /* Seccion para llenar la tabla */
                for (var index = 0;index < response.length; index++) 
                {
                   options += '<tr>' +
                                    '<td style="display : none">' + response[index].codMotivo + '</td>' +
                                    '<td>' + response[index].descMotivo + '</td>' +
                                    '<td><center>'+
                                        '<button data-id = "'+ response[index].codMotivo +'" href= "#" class = "editar btn_editar btn btn-info"  data-toggle="modal" data-target = ""><i class="glyphicon glyphicon-edit"></i></button>'+
                                    '</td></center>' +
                                    '<td><center>'+
                                        '<button data-id = "'+ response[index].codMotivo +'" href= "#" class = "elimina_motivo btn_editar btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>'+
                                    '</td></center>' +             
                              '</tr>';
                }

            
                $("#cTablaMotivos").html(options);

                /* Script que permite a la tabla hacer busquedas dentro de ella
                    y ordenarla deacuerdo a lo que se presenta en ella. */

                    $('#TableMotivos').dataTable();
            },
            timeout: 4000
        }); 
    }



    /* Seccion que se manda a llamar cuando dentro de la modal aceptamos e indicamos que lo que queremos 
    modificar ya se hiso */

    $("#form_Modificar").submit(function(e) {
            e.preventDefault();
            $("#compose-modal-modificar").modal('hide');
            datosEditados = {
                codMotivo: $("#codMotivo").data('id'),
                descMotivo1: $("#descMotivo1").val()
            };
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                data: datosEditados,
                contentType: "application/x-www-form-urlencoded",
                url: "pages/permisos/motivo/motivoEditado.php",
                success: function(data){ 
                    $("#notificaciones1").html(data);
                    $("#notificaciones1").fadeOut(4500000);
                    cargarMotivos();
                },
                timeout: 4000
            });
        });
});

</script>


<!-- Div para cargar informacion recibida por el servidor -->
<div id = "notificaciones"></div>

<!-- Seccion para que el usuario indique el nuevo motivo que quiere ingresar -->
<div class="row"> 

    <div class="col-lg-10">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <label><span class="glyphicon glyphicon-globe" aria-hidden="true"></span> Nuevo Motivo </label>
            </div>
            <div class="panel-body">
                <div>
                       <div id= "noti1" class="alert alert-info" role="alert"><center>Por favor ingrese los datos que a continuación se le piden</center></div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <form id = "formInsertar" role="form" action="#" method="POST">
                            <div id="Nplan" class="form-group">
                                <label>Descripción del motivo</label>
                                <input placeholder = "Se necesita una descripción" type="text"  class="form-control" name="descMotivo" id="descMotivo" required >
                            </div>
<!--                            <div id="UV" class="form-group">
                                <label>Unidades Valorativas</label>
                                <input placeholder = "hay que volarselo" type="number"  class="form-control" name="uvPlan" id="uvPlan" required >
                            </div>-->
                            <button type="submit" name="submit"  id="submit" class="submit btn btn-info" >Agregar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- En este div se muestran todos los mensajes con respecto a la creacion y eliminacion dentro de la 
    que el servidor responda -->
<div id = "notificaciones1"></div>

<!-- Seccion usada para mostrar la tabla de los planes de estudio que estan presentes en el sistema -->
<div class="col-lg-10">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <label><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Motivos </label>
        </div>
        <div class="panel-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-10">
                        <section class="content">
                            <div class="table-responsive">
                                <table id= "TableMotivos" border="1" class='table table-bordered table-striped'>
                                    <thead>
                                        <tr>
                                            <th style = 'display:none'>Codigo</th>
                                            <th style="text-align:center;background-color:#386D95;color:white;">Descripción</th> 
                                            <th style="text-align:center;background-color:#386D95;color:white;">Actualización</th>  
                                            <th style="text-align:center;background-color:#386D95;color:white;">Eliminar</th>                  
                                        </tr>
                                    </thead>
                                    <tbody id = "cTablaMotivos">
                                        <!-- Contenido de la tabla generado atravez de la consulta a 
                                            la base de datos -->
                                    </tbody>
                                </table>       
                            </div>
                        </section>
                    </div>                
                </div>
            </div>       
        </div>
    </div>
</div>

<!-- Modal para editar los motivos -->

<div  class="modal fade" id="compose-modal-modificar" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="form_Modificar" name="form_Modificar" action="#">
                <div class="modal-header" style = "background-color:#0FA6C3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel" style = "color:white">Modificar Motivos</h4>
                </div>
                <div class = "modal-body">
                    <div id= "Mdiv_editarMotivos">
                        
                    </div>                
                </div>
                <div class="modal-footer">
                    <button  id="guardaredicion" class="btn btn-primary" >Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>






