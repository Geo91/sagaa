<?php
    $maindir = "../../";

    if(isset($_GET['contenido']))
    {
        $contenido = $_GET['contenido'];
    }
    else
    {
        $contenido = 'permisos';
    }

    require_once($maindir."funciones/check_session.php");

    require_once($maindir."funciones/timeout.php");

    require_once($maindir."pages/navbar.php");

    if(!isset( $_SESSION['user_id'] ))
    {
      header('Location: '.$maindir.'login/logout.php?code=100');
      exit();
    }
?>

<html lang ="en">
<body>

<?php 
    $idusuario = $_SESSION['user_id'];
    $rol = $_SESSION['user_rol'];
  	require($maindir."conexion/config.inc.php");
        
        $q = 'SELECT No_Empleado FROM usuario WHERE Id_Usuario ='.$idusuario;
        $r = $db->prepare($q);
        $r->execute();
        $datos = $r->fetch();
        $nEE = $datos['No_Empleado'];
        $_SESSION['nEE'] = $nEE;
        
        $q = 'SELECT recibirNotificacion FROM empleado_has_cargo WHERE No_Empleado ='.$nEE;
        $r = $db->prepare($q);
        $r->execute();
        $datos = $r->fetch();
        $nE = $datos['recibirNotificacion'];
		require_once($maindir."pages/permisos/menu_permisos.php");
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10">
            <div id="contenedor" class="content-panel">
                <div class="col-lg-12">
                    <h1 class="page-header">Gestión de Inasistencias y Permisos</h1>
                </div>
    						<div class="col-sm-14">
                    <div id="contenedor2" class="content-panel">
                        <?php 
                            require_once($maindir . 'pages/permisos/Graficos/pantallaPrincipal.php');
                        ?>
                    </div>
                </div> 
            </div>
        </div>
        <div class="panel-body">
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
</div>

<!--	-->
</body>
</html>

 