<?php
  $id = $_POST['ide'];
  $maindir = "../";
  require_once($maindir . "conexion/config.inc.php");


  if(isset($_GET['contenido']))
  {
    $contenido = $_GET['contenido'];
  }
  else
  {
    $contenido = 'poa';
     $navbar_loc = 'contenido';
  }

//Conexion a la base de Datos ccjj
  /*$conectar = new mysqli('localhost','ddvderecho','DDVD3recho','ccjj');

  $enlace = mysql_connect('localhost', 'ddvderecho', 'DDVD3recho');
  mysql_select_db('ccjj', $enlace);*/

  

//Consulta para obtener los Planes Estrategicos
    $sql = "SELECT * FROM pe_planes_estrategicos where pe_planes_estrategicos.id_plan_estrategico ="."$id";
    $result = $db->prepare($sql);
    $result->execute();
    while ($row=$result->fetch()) {
      $id_plan_estrategico = $row['id_plan_estrategico'];
      $anio_inicio = $row['anio_inicio'];
      $anio_fin = $row['anio_fin'];
      $fecha_ingreso = $row['fecha_ingreso'];
      $fecha_modificacion = $row['fecha_modificacion'];
      $fecha_terminacion = $row['fecha_terminacion'];
    }

//consulta para obetener Objetivos estratégicos institucionales
    $sql4 = "SELECT * FROM pe_objetivos_est_inst";
    $result4 = $db->prepare($sql4);
    $result4->execute();

 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <!--<script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>-->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


    <script type="text/javascript" src="js\plan_estrategico\editar_dimension.js"></script>
    <script type="text/javascript" src="js\plan_estrategico\eliminar_dimension.js"></script>

    <script type="text/javascript" src="js\plan_estrategico\agregar_obj_institucional.js"></script>
    <script type="text/javascript" src="js\plan_estrategico\editar_obj_estrategico.js"></script>
    <!--Script para obtener el valor de los select (combobox)-->

    <script type="text/javascript">
      $(document).ready(function(){
          $("select.cbx_dimension").change(function(){
              var selectedDimension = $(this).children("option:selected").val();
              var myObj = {"functionName":"filtrarDimension","idDimension":selectedDimension};
              var myJSON = JSON.stringify(myObj);
              
               if($("cbx_dim").value=!0){
                document.getElementById("btnOEI").disabled=false;
              }
              

              var conexion1 = new XMLHttpRequest();
              conexion1.onreadystatechange = procesarEventos;
              conexion1.open("GET", "pages/consultasAsync.php?q=" + myJSON, true);
              conexion1.send();

              function procesarEventos()
              {
                var tabla1contenedor = document.getElementById("tabla1contenedor");

                if(conexion1.readyState == 4)
                {
                  tabla1contenedor.innerHTML = conexion1.responseText;
                  console.log(conexion1.responseText);
                }
                else
                {
                  tabla1contenedor.innerHTML = 'Cargando...';
                }
              }

              var conexion2 = new XMLHttpRequest();
              conexion2.onreadystatechange = procesarEventos2;
              conexion2.open("GET", "pages/cargarSelectObjetivosIns.php?q=" + myJSON, true);
              conexion2.send();

              function procesarEventos2()
              {
                var combo1 = document.getElementById("combo1");

                if(conexion2.readyState == 4)
                {
                  combo1.innerHTML = conexion2.responseText;
                  var cbx_objEst = document.getElementById("cbx_objEst");
                  cbx_objEst.addEventListener('change', function() {   cargarObjEstrategicos(this.value); }, false);

                  console.log(conexion2.responseText);
                }
                else
                {
                  combo1.innerHTML = 'Cargando...';
                }
              }

              var conexion3 = new XMLHttpRequest();
              //prueba
              var conexion4 = new XMLHttpRequest();//prueba

              function cargarObjEstrategicos(objetivoId){
                //alert(objetivoId);
                var myObj1 = {"functionName":"filtrarObjetivo","idObjEstrategico":objetivoId};
                var myJSON1 = JSON.stringify(myObj1);

                conexion3.onreadystatechange = procesarEventos3;
                conexion3.open("GET", "pages/cargarTablaObjUnidad.php?p=" + myJSON1, true);
                conexion3.send();
                //codigo de prueba
                conexion4.onreadystatechange = procesarEventos4;
                conexion4.open("GET", "pages/cargarSelectObjetivosUnidad.php?p=" + myJSON1, true);
                conexion4.send();
              }

              function procesarEventos3()
              {
                var tabla2contenedor = document.getElementById("tabla2contenedor");

                if(conexion3.readyState == 4)
                {
                  tabla2contenedor.innerHTML = conexion3.responseText;
                  console.log(conexion3.responseText);
                }
                else
                {
                  tabla2contenedor.innerHTML = 'Cargando...';
                }
              }
              //llenar combobox Objetivos Unidad en Area Estrategica(prueba)
              function procesarEventos4()
              {
                var combo2 = document.getElementById("combo2");

                if(conexion4.readyState == 4)
                {
                  combo2.innerHTML = conexion4.responseText;
                  var cbx_objUnidad = document.getElementById("cbx_objUnidad");
                  cbx_objUnidad.addEventListener('change', function() {   cargarObjUnidad(this.value); }, false);

                  console.log(conexion4.responseText);
                }
                else
                {
                  combo2.innerHTML = 'Cargando...';
                }
              }

              //llenar tabla Area Estrategica
              var conexion5 = new XMLHttpRequest();//prueba

              //prueba area Estrategica
              var conexion6 = new XMLHttpRequest();

              function cargarObjUnidad(unidadId){

                var myObj3 = {"functionName":"filtrarUnidad","idObjUnidad":unidadId};
                var myJSON3 = JSON.stringify(myObj3);

                conexion5.onreadystatechange = procesarEventos5;
                conexion5.open("GET", "pages/cargarTablaArea.php?p=" + myJSON3, true);
                conexion5.send();

                //codigo de prueba
                conexion6.onreadystatechange = procesarEventos6;
                conexion6.open("GET", "pages/cargarSelectAreaEstrategica.php?p=" + myJSON3, true);
                conexion6.send();
              }

              function procesarEventos5()
              {
                var tabla3contenedor = document.getElementById("tabla3contenedor");

                if(conexion5.readyState == 4)
                {
                  tabla3contenedor.innerHTML = conexion5.responseText;
                  console.log(conexion5.responseText);
                }
                else
                {
                  tabla3contenedor.innerHTML = 'Cargando...';
                }
              }
              //llenar combobox Area Estrategica en Acciones Estrategicas(prueba)
              function procesarEventos6()
              {
                var combo3 = document.getElementById("combo3");

                if(conexion6.readyState == 4)
                {
                  combo3.innerHTML = conexion6.responseText;
                  var cbx_Area = document.getElementById("cbx_Area");
                  cbx_Area.addEventListener('change', function() {   cargarAreaEstrategica(this.value); }, false);

                  console.log(conexion6.responseText);
                }
                else
                {
                  combo3.innerHTML = 'Cargando...';
                }
              }
              //llenar tabla Acciones Estrategicas
              var conexion7 = new XMLHttpRequest();//prueba
              //var para combo responsables
              var conexion8 = new XMLHttpRequest();

              function cargarAreaEstrategica(areaId){

                var myObj4 = {"functionName":"filtrarArea","idArea":areaId};
                var myJSON4 = JSON.stringify(myObj4);

                conexion7.onreadystatechange = procesarEventos7;
                conexion7.open("GET", "pages/cargarTablaAcciones.php?p=" + myJSON4, true);
                conexion7.send();

                conexion8.onreadystatechange = procesarEventos8;
                conexion8.open("GET", "pages/cargarSelectAcciones.php?p=" + myJSON4, true);
                conexion8.send();

              }

              //tabla Acciones Estrategicas
              function procesarEventos7()
              {
                var tabla4contenedor = document.getElementById("tabla4contenedor");

                if(conexion7.readyState == 4)
                {
                  tabla4contenedor.innerHTML = conexion7.responseText;
                  console.log(conexion7.responseText);
                }
                else
                {
                  tabla4contenedor.innerHTML = 'Cargando...';
                }
              }

//cargar combobox responsables
function procesarEventos8()
{
  var combo4 = document.getElementById("combo4");

  if(conexion8.readyState == 4)
  {
    combo4.innerHTML = conexion8.responseText;
    var cbx_Acciones = document.getElementById("cbx_Acciones");
    cbx_Acciones.addEventListener('change', function() {   cargarAccionesEstrategicas(this.value); }, false);

    console.log(conexion6.responseText);
  }
  else
  {
    combo4.innerHTML = 'Cargando...';
  }
}

//cargar tabla responsables

var conexion9 = new XMLHttpRequest();

function cargarAccionesEstrategicas(accionId){

var myObj5 = {"functionName":"filtrarAccion","idAccion":accionId};
var myJSON5 = JSON.stringify(myObj5);
//alert(myJSON5);
conexion9.onreadystatechange = procesarEventos9;
conexion9.open("GET", "pages/cargarTablaResponsables.php?p=" + myJSON5, true);
conexion9.send();

}

function procesarEventos9()
{
  var tabla5contenedor = document.getElementById("tabla5contenedor");

  if(conexion9.readyState == 4)
  {
    tabla5contenedor.innerHTML = conexion9.responseText;
    console.log(conexion9.responseText);
  }
  else
  {
    tabla5contenedor.innerHTML = 'Cargando...';
  }
}

          });
      });
    </script>

  </head>
  <body>

    <div id="contenedor">
      <h2 class="page-header"><i class="fa fa-list-alt" aria-hidden="true"></i>Plan Estratégico</h2>

      <div id="respuesta">

      </div>
      <div class="row">

        <div class="col-lg-12">

            <div class="col-lg-2">
              <div class="">
                <div class="panel-body">
                </div>
              </div>
            </div>
            <div class="col-lg-8">
                  <div class="panel panel-primary" >
                    <h3 style="text-align:center">Periodo</h3>
                      <div class="panel-body" >
                        <p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          Año de Inicio de Período:</strong>&nbsp;&nbsp;
                          <input  id="inicio" class="btn btn-default" style="background-color:white;width: 235px" value="<?php echo $anio_inicio;?>" readonly>
                        </p>
                        <p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          Año de Fin de Período:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <input  id="Fin" class="btn btn-default" style="background-color:white;width: 235px " value="<?php echo $anio_fin;?>" readonly>
                        </p>
                    </div>
                </div>
          </div>
        </div>

<div class="col-lg-12">
  <div class="col-lg-2">
    <div class="">
        <div class="panel-body">
        </div>
    </div>
  </div>
  <div class="col-lg-8">
    <div class="panel-group" id="accordion">

  <div class="panel panel-primary">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><strong>Dimensiones Estratégicas
          <span class="glyphicon glyphicon-chevron-down"></span></strong>
        </a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse in">
      <h3 style="text-align:center">Agregar Dimensión Estratégica

        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalAddDimension">
          <span class="glyphicon glyphicon-plus"></span>
        </button>
      </h3>
      <div class="panel-body">
        <div id="contenedor-dimensiones">
        <table id="tabla_dimensiones" class="table table-bordered table-striped" style="text-align:center;">
          <thead>
              <tr>
                <th style="text-align: center;color:white; background-color:#386D95;">Nro.</th>
                <th style="text-align: center;color:white; background-color:#386D95;">Dimensión Estratégica</th>
                <th style="text-align: center;color:white; background-color:#386D95;">Descripcion Dimension</th>
                <th style="text-align: center;color:white; background-color:#386D95;">Editar/Eliminar</th>
              </tr>
          </thead>
          <tbody>
            <?php
            //Consulta para obtener las Dimensiones Estrategicas
                $sql3 = "SELECT * FROM pe_dimensiones_estrategicas where pe_dimensiones_estrategicas.plan_estrategico_id ="."$id";
                $result3 = $db->prepare($sql3);
                $result3->execute();
                while ($row_dimension = $result3->fetch()) {
                  $idd = $row_dimension['id_dimension_estrategica'];
                  $ndi = $row_dimension['nombre_dimension'];
                  $ddi = $row_dimension['descripcion_dimension'];
                  $pid = $row_dimension['plan_estrategico_id'];


                 ?>
                 <tr data-idd = '<?php echo $idd; ?>'
                     data-ndi = '<?php echo $ndi; ?>'
                     data-ddi = '<?php  echo $ddi;?>'
                     data-ide = '<?php  echo $pid;?>'>

                   <td style="text-align:center;"><?php echo $idd; ?></td>
                   <td><?php echo $ndi; ?></td>
                   <td><?php echo $ddi; ?></td>
                   <td><a class="Edimension btn btn-info" data-toggle="modal" data-target="#modalEditarDimension">
                     <span class="glyphicon glyphicon-edit"></span></a>
                     <a type="button" class="eliminarDimension btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
                   </td>

                 </tr>

               <?php } ?>

          </tbody>
        </table>
      </div>
    </div>
    </div>
  </div>

  </div>
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Objetivos Estratégicos Institucionales
            <span class="glyphicon glyphicon-chevron-down"></span>
        </a>
      </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
      <h3 style="text-align:center">Agregar Objetivos Estratégicos Institucionales
          <button type="button" id="btnOEI" class="btn btn-success" data-toggle="modal" data-target="#modalAddObjetivoIns" disabled="true" ><span class="glyphicon glyphicon-plus"></span></button>


      </h3>
      <div class="panel-body">
        <p><strong>Dimensión Estratégica:</strong>

          <select class="cbx_dimension" id="cbx_dim">
              <option value="0">Seleccione una dimensión:</option>
              <?php

                  $sql = "SELECT * FROM pe_dimensiones_estrategicas where pe_dimensiones_estrategicas.plan_estrategico_id ="."$id";
                  $result = $db->prepare($sql3);
                  $result->execute();

                  while ($row =$result->fetch()) {
                    $idd = $row['id_dimension_estrategica'];
                    $ndi = $row['nombre_dimension'];
                    echo '<option value="'.$idd.'">'.$ndi.'</option>';
                  }

                ?>
          </select>
          
        </p>
        <div id="tabla1contenedor">

        </div>
    </div>
    </div>
  </div>
  <!--panel para el ingreso de los Objetivos estrategicos de la unidad-->
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Objetivos Estratégicos de la Unidad
          <span class="glyphicon glyphicon-chevron-down"></span>
        </a>
      </h4>
    </div>
    <div id="collapse3" class="panel-collapse collapse">
      <h3 style="text-align:center">Agregar Objetivo Estratégico de la Unidad
          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalEditarObjUnidad"><span class="glyphicon glyphicon-plus"></span></button>
      </h3>
      <div class="panel-body">

            <div id="combo1">

            </div>

        <div id="tabla2contenedor">

        </div>
    </div>
    </div>
  </div>
  <!--fin panel-->
  <!--panel de ingreso de áreas estratégicas-->
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Áreas Estratégicas
          <span class="glyphicon glyphicon-chevron-down"></span>
        </a>
      </h4>
    </div>
    <div id="collapse4" class="panel-collapse collapse">
      <h3 style="text-align:center">Agregar Áreas Estratégicas
          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalAddArea"><span class="glyphicon glyphicon-plus"></span></button>
      </h3>
      <div class="panel-body">
        <div id="combo2">

        </div>
        <div id="tabla3contenedor">

        </div>
    </div>
    </div>
  </div>

  <!--panel de ingreso de áreas estratégicas-->
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">Acciones Estratégicas
          <span class="glyphicon glyphicon-chevron-down"></span>
        </a>

      </h4>
    </div>
    <div id="collapse5" class="panel-collapse collapse">
      <h3 style="text-align:center">Agregar Acciones Estratégicas
          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalAddAccion"><span class="glyphicon glyphicon-plus"></span></button>
      </h3>
      <div class="panel-body">
        <div id="combo3">

        </div>
        <div id="tabla4contenedor">

        </div>
    </div>
    </div>
  </div>
  <!--fin panel-->
  <!--panel de ingreso de áreas estratégicas-->
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">Responsables
          <span class="glyphicon glyphicon-chevron-down"></span>
        </a>

      </h4>
    </div>
    <div id="collapse6" class="panel-collapse collapse">
      <h3 style="text-align:center">Agregar Responsable
          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalAddResponsable"><span class="glyphicon glyphicon-plus"></span></button>
      </h3>
      <div class="panel-body">
        <div id="combo4">

        </div>

        <div id="tabla5contenedor">

        </div>
    </div>
    </div>
  </div>
  <!--fin panel-->

</div>
</div>
</div>
</div>

<!--Ventanas modales-->
<!-- Modal para agregar nuevas dimensiones estrategicas-->
<div class="modal fade" id="modalAddDimension" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" id="formNuevaDimension">
        <div class="modal-header" style="background-color:#33B3D1; color:white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align:center;"><strong>Nueva Dimensión Estratégica</strong></h4>
        <script language="JavaScript" type="text/javascript" src="js\plan_estrategico\agregar_dimension.js"></script>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <input id="id" name="id" type="hidden" value="<?php echo $id; ?>"></input>
            <p><strong>Nombre Dimensión:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input id="nombre_di" type="text" style="width:550px" required>
            </p>
            <p><strong>Descripción Dimensión:</strong>&nbsp;
              <textarea  id="descripcion_di" type="text" rows="5" cols="75">
              </textarea>
            </p>

          </div>
        </div>
        <div class="modal-footer">
          <div align:"right">
            <button type="button"  class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            <input type="submit" class="btn btn-primary" id="guarda_dimension" value="Guardar">
          </div>
        </div>
      </form>
    </div>
  </div>
</div>



<!-- Modal Editar Dimension Estrategica-->
<div class="modal fade" id="modalEditarDimension"  role="dialog">
<div class="modal-dialog">
  <div class="modal-content">
    <form role="form" id="formEditarDimension" name="formEditarD">
    <div class="modal-header" style="background-color:#33B3D1; color:white;">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title" style="text-align:center;"><strong>Editar Dimensión Estratégica</strong></h4>
    </div>
    <div class="modal-body" >
      <div class="form-group">
        <input id="ide"  value="<?php echo $id;?>"> <!--id del plan estrategico-->
      </div>
      <div class="form-group">
      <p><strong>Nombre Dimensión</strong>
        <input id="editarNombreDimension" type="text" style="width:550px" required>
      </p>
    </div>
    <div class="form-group">
      <p><strong>Descripción Dimensión</strong>
        <textarea  id="editarDescripcionDimension" type="text" rows="5" cols="75">
        </textarea>
      </p>
    </div>
    <div class="modal-footer">
      <div align:"right">
        <button type="button"  class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary" id="guarda">Guardar</button>
      </div>
    </div>
  </form>
  </div>
</div>
</div>
</div>





  </body>
</html>



<!--script para paginar tabla-->
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#tabla_dimensiones').dataTable();
  } );
</script>


<!-- Script necesario para que la tabla se ajuste a el tamanio de la pag-->
<script type="text/javascript" charset="utf-8">
    // For demo to fit into DataTables site builder...
    $('#tabla_dimensiones')
            .removeClass('display')
            .addClass('table table-striped table-bordered');
</script>



<?php $variablePHP = "<script> document.write(nombre_dimension) </script>"; ?>
