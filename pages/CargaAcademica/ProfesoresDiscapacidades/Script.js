//cargar la tabla inicialmente
    $("#datosTabla").load('pages/CargaAcademica/ProfesoresDiscapacidades/DatosDiscapacidades.php');

//para ver discapacidades
$(document).on("click", ".VerDiscapacidades", function () {
    data={
                No_empleado:$(this).parents("tr").data("id")
        }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: verDiscapacidades,
        timeout: 4000,
        error: problemas
    }); 
    $("#modalEditar").modal('show');
    return false;
});


function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function guardarProfesorDiscapacidad(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/CargaAcademica/ProfesoresDiscapacidades/GuardarDiscapacidad.php',data);
}

function removerDiscapacidades(){
    $("#divRespuestaEditar").empty();
    $("#divRespuestaEditar").load('pages/CargaAcademica/ProfesoresDiscapacidades/EliminarDiscapacidad.php',data);
}

function verDiscapacidades(){
    $("#modalContenido").load('pages/CargaAcademica/ProfesoresDiscapacidades/DatosDiscapacidadesProfesor.php',data);
}