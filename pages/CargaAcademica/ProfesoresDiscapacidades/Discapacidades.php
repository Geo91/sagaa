<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");

if($_SESSION['user_rol']!=30){  
?>

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong>Gestión de Docentes con Discapacidad</strong></center></h3>
    </div>
</div> 

<div  id="divRespuesta"></div>    

<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Agregar discapacidad a un docente</div>

    <div class="panel-body" >
        <div class="row">
            <div class="col-lg-12">
                <div id="divFormulario">
                    <?php require_once("FormProfesorDiscapacidad.php"); ?>
                </div>
            </div>
        </div>
    </div>                                    
</div> 
<?php } ?>
<!--tabla de asignaturas-->
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center>Profesores con discapacidades</center></h3>
    </div>
</div> 
<div id="divRespuestaEditar">
</div>
<div id="datosTabla">
</div>
<!--******************modal de edicion*****************-->
<div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header" style = "background-color:#0FA6C3">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style = "color:white">Discapacidades</h4>
        </div>
        <div class="modal-body">
            <div id="modalContenido"></div>
        </div>
        <div class="modal-footer">
            <button type="button"  class="btn btn-default" data-dismiss="modal"> Salir </button>
        </div>
      </div>
    </div>
</div>
<!--******************modal de edicion*****************-->
<script type="text/javascript" src="pages/CargaAcademica/ProfesoresDiscapacidades/Script.js"></script>