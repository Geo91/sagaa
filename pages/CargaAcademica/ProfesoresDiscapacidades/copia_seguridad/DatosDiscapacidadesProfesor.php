<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    if (isset($_POST['No_empleado'])) {
        $No_empleado = $_POST['No_empleado'];
        $query1 = "SELECT ca_tipos_discapacidades.id_discapacidad,
                            ca_tipos_discapacidades.nombre 
                FROM ca_profesores_discapacidades,ca_tipos_discapacidades
                WHERE ca_profesores_discapacidades.id_discapacidad=ca_tipos_discapacidades.id_discapacidad
                and ca_profesores_discapacidades.No_Empleado='$No_empleado'";
?>
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaAsignaturas" class="table table-bordered table-striped">
            <thead>
                <tr>                                            
                    <th>ID</th>
                    <th>Tipo de discapacidad</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $result =$db->prepare($query1);
                $result->execute();
                while ($fila = $result->fetch()) {
                    ?>
                    <tr data-id='<?php echo $fila["id_discapacidad"]; ?>'>
                        <td><?php echo $fila["id_discapacidad"]; ?></td>
                        <td><?php echo $fila["nombre"]; ?></td>
                    </tr>
            <?php } ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div>
<?php
}else{
    echo mensajes("Problemas con la conexión","azul");;
}
?>