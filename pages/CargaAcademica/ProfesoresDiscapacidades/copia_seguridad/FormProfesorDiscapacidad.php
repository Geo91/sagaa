<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir . "conexion/config.inc.php");

    $query1 = "SELECT * FROM `ca_tipos_discapacidades`";

    $query = "SELECT empleado.No_Empleado, 
                    CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombre
            FROM empleado,persona,cargo,empleado_has_cargo
            WHERE empleado.N_identidad=persona.N_identidad
                and empleado_has_cargo.No_Empleado=empleado.No_Empleado
                and empleado_has_cargo.ID_cargo=cargo.ID_cargo
                and empleado.estado_empleado=1
                and (cargo.Cargo='Docente' 
                    or cargo.Cargo='Decana'
                    or cargo.Cargo='Jefe de Departamento'
                    or cargo.Cargo='Coordinador de Maestría')
                and empleado_has_cargo.Fecha_salida_cargo is null
                and empleado.No_Empleado not in (SELECT ca_profesores_discapacidades.No_Empleado FROM ca_profesores_discapacidades)
            ORDER BY empleado.No_Empleado";
?>

<!--************************formulario*********************-->
    <form class="form-horizontal" role="form" id="formProfesorDiscapacidad" name="formProfesorDiscapacidad">

        <div class="row form-group">
            <label class=" col-sm-2 control-label" >Profesor</label>
            <div class="col-sm-8">                            
                <select class="form-control" id="NoEmpleado" name="NoEmpleado" required>
                    <option value="">Seleccione una opción</option>
                    <?php
                         $result =$db->prepare($query);
                         $result->execute();
                        while ($fila = $result->fetch()) {
                            ?>
                            <option value="<?php echo $fila["No_Empleado"] ?>">
                            <?php 
                                $largo = strlen($fila["No_Empleado"]);
                                if ($largo<=7) {
                                    $espacio = str_repeat ( "&nbsp", 7-$largo);
                                }else{
                                    $espacio=" ";
                                }
                                echo "█ No.".$fila["No_Empleado"].$espacio."█ &nbsp&nbsp".$fila["nombre"]; ?></option>
                                <?php
                            }
                        ?>
                </select>
            </div>
        </div>

        <div class="row form-group">
            <label class=" col-sm-2 control-label" >Tipos de discapacidades</label>
            <div class="col-sm-6">                            
                <select class="form-control" id="TipoDiscapacidad" name="TipoDiscapacidad" size="3" multiple title="seleccione una o más opciones" required>
                    <?php
                         $result =$db->prepare($query1);
                         $result->execute();
                        while ($fila = $result->fetch()) {
                            ?>
                            <option value="<?php echo $fila["id_discapacidad"]; ?>"><?php echo $fila["nombre"]; ?></option>
                            <?php
                        }
                    ?>
                </select>
            </div>
            <br>
            <span class="glyphicon glyphicon-info-sign"></span> Seleccione una o más opciones
        </div>

        <div class="row">
            <label class="control-label col-sm-2"></label>
            <div class="col-sm-8">
                <p aling ="right">
                    <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" ><span class=" glyphicon glyphicon-floppy-disk"></span> Agregar</button> 
                </p>          
            </div>                            
        </div>    

    </form>
<!--************************formulario*********************-->
<script type="text/javascript">
$(document).ready(function(){   

//para cuando se da submit al formulario formAsignatura
    $("#formProfesorDiscapacidad").submit(function(e) {
        e.preventDefault();
        data={
                    No_Empleado:$('#NoEmpleado').val(),
                    tiposDiscapacidades:$('#TipoDiscapacidad').val()
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: guardarProfesorDiscapacidad,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });

});
</script>