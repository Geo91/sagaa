<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query1 = "SELECT DISTINCT(ca_profesores_discapacidades.No_Empleado) as No_empleado,
                persona.N_identidad,
                CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombre 
                FROM ca_profesores_discapacidades,empleado,persona
                WHERE ca_profesores_discapacidades.No_Empleado=empleado.No_Empleado
                    and empleado.N_identidad=persona.N_identidad";
?>
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaAsignaturas" class="table table-bordered table-striped">
            <thead>
                <tr>                                            
                    <th>No Empleado</th>
                    <th>Identidad</th>
                    <th>Nombre</th>
                    <th>Discapacidades</th>
                <?php if($_SESSION['user_rol']!=30){  ?>
                    <th>Remover</th>
                <?php } ?>
                </tr>
            </thead>
            <tbody>
            <?php
                $result =$db->prepare($query1);
                $result->execute();
                while ($fila = $result->fetch()) {
                    ?>
                    <tr data-id='<?php echo $fila["No_empleado"]; ?>'>
                        <td><?php echo $fila["No_empleado"]; ?></td>
                        <td><?php echo $fila["N_identidad"]; ?></td>
                        <td><?php echo $fila["nombre"]; ?></td>
                        <td>
                            <center>
                                <button type="button" class="VerDiscapacidades btn btn-primary glyphicon glyphicon-eye-open"  title="Ver discapacidades"> Ver</button>
                            </center>
                        </td>       
                    <?php if($_SESSION['user_rol']!=30){  ?>
                        <td>
                            <center>
                                <button type="button" class="RemoverDiscapacidades<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-remove"  title="Deshabilitar Asignatura">
                            </center>
                        </td>     
                    <?php } ?>
                    </tr>
            <?php } ?>
            </tbody>
            <tfoot>
                <tr>                     
                    <th>No Empleado</th>
                    <th>Identidad</th>
                    <th>Nombre</th>
                    <th>Discapacidades</th>
                <?php if($_SESSION['user_rol']!=30){  ?>
                    <th>Remover</th>
                <?php } ?>
                </tr>
            </tfoot>
        </table>
    </div><!-- /.box-body -->
</div>
<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#tablaAsignaturas')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaAsignaturas').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
$(document).on("click", ".RemoverDiscapacidades<?php echo $auto_increment; ?>", function () {
    if (confirm("¿Está seguro de que desea eliminar las discapacidades de este empleado?")){
        codigo = $(this).parents("tr").data("id");
        data={
                    No_empleado:codigo
            }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: removerDiscapacidades,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    }
});
</script>