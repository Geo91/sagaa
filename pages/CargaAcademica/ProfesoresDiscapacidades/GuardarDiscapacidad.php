<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['No_Empleado']) && 
        isset($_POST['tiposDiscapacidades']) ){
        try{
            $No_Empleado = $_POST['No_Empleado'];
            $tiposDiscapacidades = $_POST['tiposDiscapacidades'];
            foreach ($tiposDiscapacidades as $tipo) {
                $query = "INSERT INTO `ca_profesores_discapacidades` (`No_Empleado`, `id_discapacidad`) VALUES ('$No_Empleado', '$tipo');";
                $result =$db->prepare($query);
                $result->execute();
            }
            echo mensajes("Datos agregados correctamente al empleado No.$No_Empleado.","verde");
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>
<script type="text/javascript">
    $("#divFormulario").load('pages/CargaAcademica/ProfesoresDiscapacidades/FormProfesorDiscapacidad.php');
    $("#datosTabla").load('pages/CargaAcademica/ProfesoresDiscapacidades/DatosDiscapacidades.php');
</script>