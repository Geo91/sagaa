<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['No_empleado'])){
        
        $No_empleado = $_POST['No_empleado'];

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query = "DELETE FROM ca_profesores_discapacidades WHERE ca_profesores_discapacidades.No_Empleado='$No_empleado'";

        try{
            $result =$db->prepare($query);
            $result->execute();

            echo mensajes("Se han eliminado las discapacidades del empleado No.$No_empleado","verde");
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }else{
        echo mensajes("Problemas con la conexión","azul");
    }
?>
<script type="text/javascript">
    $("#divFormulario").load('pages/CargaAcademica/ProfesoresDiscapacidades/FormProfesorDiscapacidad.php');
    $("#datosTabla").load('pages/CargaAcademica/ProfesoresDiscapacidades/DatosDiscapacidades.php');
</script>