<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    $bandera = false;
    if (isset($_POST['EdificioID']) &&
    isset($_POST['NombreEdificio']) ){
        try{
            $EdificioID = $_POST['EdificioID'];
            $NombreEdificio = $_POST['NombreEdificio'];

            $query = "DELETE FROM ca_edificios
                    WHERE ca_edificios.id_edificio=$EdificioID";

            $result =$db->prepare($query);
            $result->execute();
            $mensaje = "El edificio `$NombreEdificio` ha sido ELIMINADO";
            $color = "verde";
            echo mensajes($mensaje,$color);
            $bandera = true;
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
    if ($bandera) {
?>
<script type="text/javascript">
    $("#divContenido").empty();
    $("#divContenido").load('pages/CargaAcademica/Edificios/FormNuevoEdificio.php',data);
    $("#PaginaEdificios").empty();
    $("#PaginaEdificios").load('pages/CargaAcademica/Edificios/DatosEdificios.php');
</script>
<?php
    }
?>