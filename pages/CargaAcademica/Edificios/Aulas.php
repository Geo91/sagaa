<?php
    $maindir = "../../../";

    require_once("../ChekAutoIncrement.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."conexion/config.inc.php");   

    if (isset($_POST['edificioId']) &&
    isset($_POST['nombre']) &&
    isset($_POST['descripcion']) &&
    isset($_POST['prioridad']) ) {
        $edificioId = $_POST['edificioId'];
        $nombre = $_POST['nombre'];
        $descripcion = $_POST['descripcion'];
        $prioridad = $_POST['prioridad'];
        $Aulas=false;
?>
<div class="well" style="background-color:#E0F2F7">
    <div class="row">
        <div class="btn-group">
            <button type="button" id="btnEditarEdificio" class="btn btn-info"><span class="glyphicon glyphicon-edit"></span> Editar</button>
            <button type="button" id="btnEliminarEdificio" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Eliminar</button>
        </div>
        <div class="col-sm-7">
            <h4>Edificio: <?php echo $nombre; 
                            if ($prioridad==1) {
                                echo " (principal)";
                            }
                        ?></h4>
        </div>
    </div>
    <p><?php echo $descripcion; ?></p>
</div>
<div class="panel panel-primary">
    <div class="panel-heading"> Aulas</div>
    <div class="panel-body">

        <input type="hidden" class="form-control" id="EdificioID" name="EdificioID" value="<?php echo $edificioId; ?>">

        <input type="hidden" class="form-control" id="NombreEdificio" name="NombreEdificio" value="<?php echo $nombre; ?>">

        <input type="hidden" class="form-control" id="DescripcionEdificio" name="DescripcionEdificio" value="<?php echo $descripcion; ?>">

        <input type="hidden" class="form-control" id="prioridad" name="prioridad" value="<?php echo $prioridad; ?>">
        
        <button type="button" id="agregarNuevaAula" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Agregar nueva aula</button><br><br>

        <div id="divTablaAulas">
            <?php include("DatosAulas.php"); ?>
        </div>
    </div>
</div>

<?php 
    }
?>
<script type="text/javascript">
    $("#btnEliminarEdificio").click(function(event) {
        event.preventDefault();
        if($('#AulasEdificios').val()==0){
            if(confirm("¿Esta seguro que desea eliminar el edificio?")){
                data={
                            EdificioID:$('#EdificioID').val(),
                            NombreEdificio:$('#NombreEdificio').val()
                        }
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: eliminarEdificio,
                    timeout: 4000,
                    error: problemas
                }); 
                return false;
            }
        }else{
            alert("No se puede eliminar el edificio mientras este tenga aulas registradas.");
        }
    });
</script>
<script type="text/javascript" src="pages/CargaAcademica/Edificios/Script.js"></script>