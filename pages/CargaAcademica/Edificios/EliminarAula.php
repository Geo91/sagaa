<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    $bandera = false;
    if (isset($_POST['aulaID']) &&
    isset($_POST['nombreAula']) &&
    isset($_POST['edificioId']) &&
    isset($_POST['nombreEdificio']) &&
    isset($_POST['descripcion']) &&
    isset($_POST['prioridad']) ){
        try{
            $aulaID = $_POST['aulaID'];
            $nombreAula = $_POST['nombreAula'];
            $edificioId = $_POST['edificioId'];
            $nombreEdificio = $_POST['nombreEdificio'];
            $descripcion = $_POST['descripcion'];
            $prioridad = $_POST['prioridad'];

            $query = "SELECT * FROM ca_secciones WHERE ca_secciones.id_aula=$aulaID";

            $ExisteAula = false;
            $result =$db->prepare($query);
            $result->execute();
            while ($fila = $result->fetch()) {
                $ExisteAula = true;
                break;
            }
            if ($ExisteAula) {
                $mensaje = "El aula `$nombreAula` no se puede eliminar ya que hay registros de secciones en esta.";
                $color = "azul";
            }else{
                $query = "DELETE FROM ca_aulas WHERE ca_aulas.id_aula=$aulaID";
                $result =$db->prepare($query);
                $result->execute();
                $mensaje = "El aula `$nombreAula` ha sido ELIMINADA";
                $color = "verde";
                ?>
                    <script type="text/javascript">
                        DatosRecargarAulas();
                        function DatosRecargarAulas(){
                            data={
                                edificioId:<?php echo "'$edificioId'"; ?>,
                                nombre:<?php echo "'$nombreEdificio'"; ?>,
                                descripcion:<?php echo "'$descripcion'"; ?>,
                                prioridad:<?php echo "'$prioridad'"; ?>
                            }
                            $.ajax({
                                async: true,
                                type: "POST",
                                dataType: "html",
                                contentType: "application/x-www-form-urlencoded",
                                success: RecargarAulas,
                                timeout: 4000,
                                error: problemas
                            }); 
                            return false;
                        }
                        function RecargarAulas(){
                            $("#divContenido").empty();
                            $("#divContenido").load('pages/CargaAcademica/Edificios/Aulas.php',data);
                        }
                    </script>
                <?php
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>