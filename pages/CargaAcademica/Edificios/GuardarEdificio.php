<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['nombre']) && 
        isset($_POST['Descripcion']) && 
        isset($_POST['Principal']) ){
        try{
            $nombre = $_POST['nombre'];
            $Descripcion = $_POST['Descripcion'];
            $Principal = $_POST['Principal'];

            $query = "SELECT * FROM `ca_edificios` WHERE `nombre`='$nombre'";
            $bandera=true;
            $result =$db->prepare($query);
            $result->execute();
            while ($fila = $result->fetch()) {
                $bandera = false;
                break;
            }

            if ($Principal=="true") {
                $queryE = "UPDATE `ca_edificios` SET `prioridad` = '2' WHERE `ca_edificios`.`prioridad` = '1';";
                $result =$db->prepare($queryE);
                $result->execute();
                $prioridadE=1;
            }else{
                $prioridadE=2;
            }

            if ($bandera){
                $query = "INSERT INTO `ca_edificios` (`id_edificio`, `nombre`, `descripcion`, `prioridad`) VALUES (NULL, '$nombre', '$Descripcion', '$prioridadE');";
                $result =$db->prepare($query);
                $result->execute();
                $mensaje = "El edificio `$nombre` se ha guardado.";
                $color = "verde";
            }else{
                $mensaje = "Ya existe un edificio con el nombre `$nombre`";
                $color = "azul";
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>
<script type="text/javascript">
    $("#PaginaEdificios").empty();
    $("#PaginaEdificios").load('pages/CargaAcademica/Edificios/DatosEdificios.php');
</script>