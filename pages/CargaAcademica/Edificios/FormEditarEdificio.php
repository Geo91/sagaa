<?php
    $maindir = "../../../";
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    if (isset($_POST['EdificioID']) &&
        isset($_POST['NombreEdificio']) &&
        isset($_POST['Descripcion']) &&
        isset($_POST['prioridad']) ) {
        $EdificioID = $_POST['EdificioID'];
        $NombreEdificio = $_POST['NombreEdificio'];
        $Descripcion = $_POST['Descripcion'];
        $prioridad = $_POST['prioridad'];
?>
<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title" style = "color:white">Editar edificio</h4>
</div>
<div class="modal-body">
    <form class="form-horizontal" role="form" id="formEditarEdificio" name="formEditarEdificio">

        <input type="hidden" class="form-control" id="EdificioID" name="EdificioID" value="<?php echo $EdificioID; ?>">

        <div class="row form-group" >                    
            <label class=" col-sm-2 control-label" >Nombre</label>
            <div class="col-sm-8">               
                <input type="text" class="form-control" id="nombre" name="nombre" maxlength="50" <?php echo " value='$NombreEdificio'"; ?>
                required autocomplete="off">
            </div>
        </div>

        <div class="row form-group">
            <div class=" col-sm-1 " ></div>
            <div class="col-sm-9 input-group">
                <span class="input-group-addon"><strong>Descripción</strong></span>
                <textarea style="resize: vertical" id="Descripcion" class="form-control" name="Descripcion" required="required" rows="3" maxlength="70"><?php echo $Descripcion; ?></textarea>
            </div>
        </div>

        <div class="row form-group" >        
            <div class="col-sm-8">  
                <div class="col-sm-8" >       
                    <label >
                        <input id="chkPrincipal" type="checkbox" 
                        <?php if ($prioridad==1){
                                echo " checked ";
                        }?>/> Edificio principal
                    </label>   
                </div>   
            </div>              
        </div>

        <div class="modal-footer">
            <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button  id="guardarCambios" class="btn btn-primary" ><span class=" glyphicon glyphicon-floppy-disk"></span> Guardar</button>
        </div>
    </form>
</div>
<?php  
    }else{
        echo mensajes("Problemas en el servidor","azul");
    }
?>
<script type="text/javascript" src="pages/CargaAcademica/Edificios/Script.js"></script>