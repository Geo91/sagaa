<?php
    $maindir = "../../../";
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir . "Datos/funciones.php");
?>

<div id="divRespuesta"></div>

<div class="row">
    <div class="col-lg-2">
        <h2>Edificios</h2>
    </div>
    <br>
    <div id="PaginaEdificios">
        <?php  require_once("DatosEdificios.php"); ?>
    </div>
</div>

<div id="PaginaEdificios">
    <?php  require_once("DatosEdificios.php"); ?>
</div>

<div id="page-wrapper"> 
    <div id="divContenido">
        <?php  require_once("FormNuevoEdificio.php"); ?>
    </div>
</div>

<!--*********************modal*********************************-->
<div class="modal fade" id="modalProyecto" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div id="subContenidoProyecto"></div>
        </div>
    </div>
</div>

<script type="text/javascript" src="pages/CargaAcademica/Edificios/Script.js"></script>