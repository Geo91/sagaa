
<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo edificio
    </div>
    <div class=" panel-body" >
        <div class="row">
            <div id="subContenidoFormulario" class="col-lg-12">
                <form class="form-horizontal" role="form" id="formNuevoEdificio" name="formNuevoEdificio">

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Nombre</label>
                        <div class="col-sm-8">               
                            <input type="text" class="form-control" id="nombre" name="nombre" maxlength="50" 
                            required autocomplete="off">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class=" col-sm-1 " ></div>
                        <div class="col-sm-9 input-group">
                            <span class="input-group-addon"><strong>Descripción</strong></span>
                            <textarea style="resize: vertical" id="Descripcion" class="form-control" name="Descripcion" required="required" rows="3" maxlength="70" placeholder='Ingrese una descripcion...'></textarea>
                        </div>
                    </div>

                    <div class="row form-group" >        
                        <div class="col-sm-8">  
                            <div class="col-sm-8" >       
                                <label >
                                    <input id="chkPrincipal" type="checkbox" /> Edificio principal
                                </label>   
                            </div>   
                        </div>              
                    </div>

                    <div class="modal-footer">
                        <button  id="guardarCambios" class="btn btn-primary" ><span class=" glyphicon glyphicon-floppy-disk"></span>Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>                                    
</div>
<script type="text/javascript" src="pages/CargaAcademica/Edificios/Script.js"></script>