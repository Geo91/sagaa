<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query = "SELECT * FROM `ca_edificios` ORDER BY ca_edificios.prioridad,ca_edificios.nombre";
?>
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li >
                <a id="lnkNuevoEdificio" href="#"><i class="glyphicon glyphicon-plus"></i> Agregar nuevo edificio</a>
            </li>
            <li data-popover="true" rel="popover" data-placement="right">

                <a href="#" data-target=".premium-menu" class="nav-header collapsed" data-toggle="collapse">
                    <i class="glyphicon glyphicon-list">
                    </i>
                    Todos los edificios
                    <i class="fa fa-collapse"></i>
                </a>
            </li>
            <li>
                <ul class="premium-menu nav nav-list collapse">
            <?php
                try{
                    $result =$db->prepare($query);
                    $result->execute();
                    while ($fila = $result->fetch()) {
                    ?>
                        <li ><a data-id="<?php echo $fila['id_edificio']; ?>"
                        data-nombre="<?php echo $fila['nombre']; ?>"
                        data-descripcion="<?php echo $fila['descripcion']; ?>" 
                        data-prioridad="<?php echo $fila['prioridad']; ?>" class="btnEdificio<?php echo $auto_increment; ?>" href="#"><span class="glyphicon glyphicon-chevron-right"></span> <?php echo $fila['nombre']; 
                                if ($fila['prioridad']==1){
                                    echo " (principal)";
                                }?></a>
                        </li> 
                    <?php
                    }

                } catch (Exception $e) {
                    echo mensajes($e->getMessage(),"rojo");
                }
            ?>
                </ul>
            </li>
                             
            
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<script type="text/javascript"> 

    $(document).ready(function(){   
        $("#lnkNuevoEdificio").click(function(event) {
            event.preventDefault();
            $("#divContenido").load('pages/CargaAcademica/Edificios/FormNuevoEdificio.php');
        });
    });

    $(document).on("click", ".btnEdificio<?php echo $auto_increment; ?>", function () {
        data={
                edificioId:$(this).data("id"),
                nombre:$(this).data("nombre"),
                descripcion:$(this).data("descripcion"),
                prioridad:$(this).data("prioridad")
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: abrirEdificio,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });

</script>
