<?php
    $maindir = "../../../";
    require_once($maindir . "Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query1="SELECT * FROM `ca_tipos_discapacidades`";
    $query2="SELECT * FROM `ca_tipos_acondicionamientos`";
    if (isset($_POST['EdificioID']) &&
        isset($_POST['NombreEdificio']) &&
        isset($_POST['Descripcion']) &&
        isset($_POST['prioridad'])) {
        $EdificioID = $_POST['EdificioID'];
        $NombreEdificio = $_POST['NombreEdificio'];
        $Descripcion = $_POST['Descripcion'];
        $prioridad = $_POST['prioridad'];

        $Edicion=false;
        if (isset($_POST['aulaID']) &&
            isset($_POST['nombreAula']) &&
            isset($_POST['capacidad']) ) {
            $aulaID = $_POST['aulaID'];
            $nombreAula = $_POST['nombreAula'];
            $capacidad = $_POST['capacidad'];
            $Edicion=true;
            $query1="SELECT ca_tipos_discapacidades.id_discapacidad,
                        ca_tipos_discapacidades.nombre,
                        COUNT(aulasD.id_aula) as acceso
                    FROM ca_tipos_discapacidades
                        LEFT JOIN (SELECT * FROM ca_aulas_discapacidades WHERE ca_aulas_discapacidades.id_aula=$aulaID) as aulasD
                            on ca_tipos_discapacidades.id_discapacidad=aulasD.id_discapacidad
                    GROUP by ca_tipos_discapacidades.id_discapacidad";
            $query2="SELECT ca_tipos_acondicionamientos.id_acondicionamiento,
                        ca_tipos_acondicionamientos.nombre,
                        COUNT(aulasA.id_aula) as acceso
                    FROM ca_tipos_acondicionamientos
                        LEFT JOIN (SELECT * FROM ca_aula_acondicionamientos WHERE ca_aula_acondicionamientos.id_aula=$aulaID) as aulasA
                            on ca_tipos_acondicionamientos.id_acondicionamiento=aulasA.id_acondicionamiento
                    GROUP by  ca_tipos_acondicionamientos.id_acondicionamiento";
        }
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3 class="modal-title">Nueva aula</h3>
</div>
<div class="modal-body">
    <form class="form-horizontal" role="form" id="formNuevaAula" name="formNuevaAula">

        <input type="hidden" class="form-control" id="EdificioIDAula" name="EdificioIDAula" value="<?php echo $EdificioID; ?>">

        <input type="hidden" class="form-control" id="NombreEdificioAula" name="NombreEdificioAula" value="<?php echo $NombreEdificio; ?>">

        <input type="hidden" class="form-control" id="DescripcionEdificioAula" name="DescripcionEdificioAula" value="<?php echo $Descripcion; ?>">

        <input type="hidden" class="form-control" id="prioridad" name="prioridad" value="<?php echo $prioridad; ?>">
        
        <input type="hidden" class="form-control" id="aulaID" name="aulaID" 
        <?php if ($Edicion) {
            echo " value='$aulaID' ";
        }else{
            echo " value='-1' ";
            } ?>>

        <div class="row form-group" >                    
            <label class=" col-sm-2 control-label" >Nombre</label>
            <div class="col-sm-8">               
                <input type="text" class="form-control" id="NombreAula" name="NombreAula" maxlength="50" 
                <?php if ($Edicion) {
                    echo " value='$nombreAula'";
                } ?>
                required>
            </div>
        </div>


        <div class="row form-group" >                    
            <label class=" col-sm-4 control-label" >Capacidad de alumnos</label>
            <div class="col-sm-2">     
                <input type="number" class="form-control" id="CuposAula" name="CuposAula" min="1" max="300" required
                <?php if ($Edicion) {
                    echo " value='$capacidad'";
                }else{
                    echo "value='1'";
                    } ?>>      
            </div>                  
        </div>

        <hr>


        <div class="row form-group">
            <label class=" col-sm-2 control-label" >Accesibilidad</label>
            <div class="col-sm-6">                            
                <select class="form-control" id="selectAccesible" name="selectAccesible" size="3" multiple title="seleccione una o más opciones">
                    <?php
                         $result =$db->prepare($query1);
                         $result->execute();
                        while ($fila = $result->fetch()) {
                            ?>
                            <option value="<?php echo $fila["id_discapacidad"]; ?>"
                            <?php if ($Edicion){
                                if ($fila["acceso"]>0) {
                                    echo " selected ";
                                }
                            } ?>><?php echo $fila["nombre"]; ?></option>
                            <?php
                        }
                    ?>
                </select>
            </div>
            <br>
            <span class="glyphicon glyphicon-info-sign"></span> Seleccione varias o ninguna
        </div>

        <div class="row form-group">
            <label class=" col-sm-2 control-label" >Recursos</label>
            <div class="col-sm-6">                            
                <select class="form-control" id="selectRecursos" name="selectRecursos" size="3" multiple title="seleccione una o más opciones">
                    <?php
                         $result =$db->prepare($query2);
                         $result->execute();
                        while ($fila = $result->fetch()) {
                            ?>
                            <option value="<?php echo $fila["id_acondicionamiento"]; ?>"
                            <?php if ($Edicion){
                                if ($fila["acceso"]>0) {
                                    echo " selected ";
                                }
                            } ?>><?php echo $fila["nombre"]; ?></option>
                            <?php
                        }
                    ?>
                </select>
            </div>
            <br>
            <span class="glyphicon glyphicon-info-sign"></span> Seleccione varias o ninguna
        </div>

        <div class="modal-footer">
            <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button  id="guardarCambios" class="btn btn-primary" ><span class=" glyphicon glyphicon-floppy-disk"></span>Guardar</button>
        </div>
    </form>
</div>
<?php  
    }else{
        echo mensajes("Problemas en el servidor","azul");
    }
?>
<script type="text/javascript" src="pages/CargaAcademica/Edificios/Script.js"></script>