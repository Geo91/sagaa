<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query1 = "SELECT ca_aulas.id_aula,
    ca_aulas.nombre,
    ca_aulas.capacidad_max,
    COUNT(ca_aulas_discapacidades.id_aula) as discapacidad,
    COUNT(ca_aula_acondicionamientos.id_aula) as acondicionamiento
FROM ca_aulas
    left JOIN ca_aulas_discapacidades
        on ca_aulas.id_aula=ca_aulas_discapacidades.id_aula
    LEFT JOIN ca_aula_acondicionamientos
        on ca_aulas.id_aula=ca_aula_acondicionamientos.id_aula
where ca_aulas.id_edificio=$edificioId
GROUP by ca_aulas.id_aula";
?>
<div class="box box-success">      
    <div class="box">
        <div class="box-body table-responsive">
            <table id="tablaAulasEdificios" class="table table-bordered table-striped">
                <thead>
                    <tr>               
                        <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Capacidad</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Accesibilidad</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Recursos</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Editar/Remover</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $result =$db->prepare($query1);
                    $result->execute();
                    while ($fila = $result->fetch()) {
                        $Aulas=true;
                        ?>
                        <tr data-id='<?php echo $fila["id_aula"]; ?>'
                            data-nombre='<?php echo $fila["nombre"]; ?>'
                            data-capacidad='<?php echo $fila["capacidad_max"]; ?>'>
                            <td><?php echo $fila["nombre"]; ?></td>
                            <td><?php echo $fila["capacidad_max"]; ?></td>
                            <td>
                                <center>
                                    <button type="button" class="verAccesibilidad<?php echo $auto_increment; ?> btn btn-primary glyphicon glyphicon glyphicon-list-alt"  title="Ver información" 
                                    <?php if ($fila["discapacidad"]==0){ ?>
                                        disabled="true";
                                    <?php
                                    } ?>></button>
                                </center>
                            </td>       
                            <td>
                                <center>
                                    <button type="button"  class="verRecursos<?php echo $auto_increment; ?> btn btn-primary glyphicon glyphicon-film"  title="Editar informacion" 
                                    <?php if ($fila["acondicionamiento"]==0){ ?>
                                        disabled="true";
                                    <?php
                                    }?>></button>
                                </center>
                            </td>       
                            <td>
                                <center>
                                    <div class="btn-group-horizontal">
                                        <button type="button" class="EditarAula<?php echo $auto_increment; ?> btn btn-info glyphicon glyphicon-pencil"  title="Editar aula">
                                        <button type="button" class="RemoverAula<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-remove"  title="Eliminar aula">
                                    </div>
                                </center>
                            </td>     
                        </tr>
                <?php } ?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div>
</div>
<input type="hidden" class="form-control" id="AulasEdificios" name="AulasEdificios" 
<?php if($Aulas){
    echo " value='1' ";
}else{
    echo " value='0' ";
    } ?>
>
<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#tablaAulasEdificios')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaAulasEdificios').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });

$(document).on("click", ".EditarAula<?php echo $auto_increment; ?>", function () {
    data={
        aulaID:$(this).parents("tr").data("id"),
        nombreAula:$(this).parents("tr").data("nombre"),
        capacidad:$(this).parents("tr").data("capacidad"),
        EdificioID:$('#EdificioID').val(),
        NombreEdificio:$('#NombreEdificio').val(),
        Descripcion:$('#DescripcionEdificio').val(),
        prioridad:$('#prioridad').val()
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: editarAula,
        timeout: 4000,
        error: problemas
    }); 
    return false;
});

$(document).on("click", ".verRecursos<?php echo $auto_increment; ?>", function () {
    data={
            id:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: abrirAcondicionamiento,
        timeout: 4000,
        error: problemas
    }); 
    return false;
});

$(document).on("click", ".verAccesibilidad<?php echo $auto_increment; ?>", function () {
    data={
            id:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: abrirAccesibilidad,
        timeout: 4000,
        error: problemas
    }); 
    return false;
});

$(document).on("click", ".RemoverAula<?php echo $auto_increment; ?>", function () {
    if (confirm("¿Está seguro de que desea eliminar el aula?")){
        data={
                    aulaID:$(this).parents("tr").data("id"),
                    nombreAula:$(this).parents("tr").data("nombre"),
                    edificioId:$('#EdificioID').val(),
                    nombreEdificio:$('#NombreEdificio').val(),
                    descripcion:$('#DescripcionEdificio').val(),
                    prioridad:$('#prioridad').val()
            }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: eliminarAula,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    }
});
</script>
<script type="text/javascript" src="pages/CargaAcademica/Edificios/Script.js"></script>