
$(document).ready(function(){   

    $("#formNuevaAula").submit(function(e) {
        e.preventDefault();
        data={
                    EdificioID:$('#EdificioIDAula').val(),
                    NombreAula:$('#NombreAula').val(),
                    selectAccesible:$('#selectAccesible').val(),
                    selectRecursos:$('#selectRecursos').val(),
                    NombreEdificio:$('#NombreEdificioAula').val(),
                    Descripcion:$('#DescripcionEdificioAula').val(),
                    CuposAula:$('#CuposAula').val(),
                    aulaID:$('#aulaID').val(),
                    prioridad:$('#prioridad').val()
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: guardarAula,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });

    $("#formNuevoEdificio").submit(function(e) {
        e.preventDefault();
        data={
                    nombre:$('#nombre').val(),
                    Descripcion:$('#Descripcion').val(),
                    Principal:$('#chkPrincipal').prop('checked')
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: guardarEdificio,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });
    
    $("#btnEditarEdificio").click(function(event) {
        event.preventDefault();
        data={
                    EdificioID:$('#EdificioID').val(),
                    NombreEdificio:$('#NombreEdificio').val(),
                    Descripcion:$('#DescripcionEdificio').val(),
                    prioridad:$('#prioridad').val()
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: editarEdificio,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });

    $("#agregarNuevaAula").click(function(event) {
        event.preventDefault();
        data={
                    EdificioID:$('#EdificioID').val(),
                    NombreEdificio:$('#NombreEdificio').val(),
                    Descripcion:$('#DescripcionEdificio').val(),
                    prioridad:$('#prioridad').val()
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: abrirNuevaAula,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });

    $("#formEditarEdificio").submit(function(e) {
        e.preventDefault();
        data={
                    EdificioID:$('#EdificioID').val(),
                    nombre:$('#nombre').val(),
                    Descripcion:$('#Descripcion').val(),
                    Principal:$("#chkPrincipal").prop('checked')
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: actualizarEdificio,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });
});

function abrirAccesibilidad(){
    $("#subContenidoProyecto").load('pages/CargaAcademica/Edificios/DatosAccesibilidad.php',data);
    $("#modalProyecto").modal('show');
}

function abrirAcondicionamiento(){
    $("#subContenidoProyecto").load('pages/CargaAcademica/Edificios/DatosAcondicionamiento.php',data);
    $("#modalProyecto").modal('show');
}

function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function abrirEdificio(){
    $("#divContenido").empty();
    $("#divContenido").load('pages/CargaAcademica/Edificios/Aulas.php',data);
}

function guardarEdificio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/CargaAcademica/Edificios/GuardarEdificio.php',data);
    $('#formNuevoEdificio').trigger("reset");
}

function editarEdificio(){
    $("#subContenidoProyecto").load('pages/CargaAcademica/Edificios/FormEditarEdificio.php',data);
    $("#modalProyecto").modal('show');
}

function actualizarEdificio(){
    $("#divRespuesta").load('pages/CargaAcademica/Edificios/ActualizarEdificio.php',data);
    $("#modalProyecto").modal('hide');
}

function eliminarEdificio(){
    $("#divRespuesta").load('pages/CargaAcademica/Edificios/EliminarEdificio.php',data);
}

function eliminarAula(){
    $("#divRespuesta").load('pages/CargaAcademica/Edificios/EliminarAula.php',data);
}

function abrirNuevaAula(){
    $("#subContenidoProyecto").load('pages/CargaAcademica/Edificios/FormNuevaAula.php',data);
    $("#modalProyecto").modal('show');
}

function guardarAula(){
    $("#divRespuesta").load('pages/CargaAcademica/Edificios/GuardarAula.php',data);
    $("#modalProyecto").modal('hide');
}

function editarAula(){
    $("#subContenidoProyecto").load('pages/CargaAcademica/Edificios/FormNuevaAula.php',data);
    $("#modalProyecto").modal('show');
}