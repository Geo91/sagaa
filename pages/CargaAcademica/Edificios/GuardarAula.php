<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['EdificioID']) && 
        isset($_POST['NombreAula'])&& 
        isset($_POST['selectAccesible']) && 
        isset($_POST['selectRecursos']) && 
        isset($_POST['NombreEdificio']) && 
        isset($_POST['Descripcion'])  && 
        isset($_POST['CuposAula']) && 
        isset($_POST['aulaID']) && 
        isset($_POST['prioridad']) ){
        try{
            $EdificioID = $_POST['EdificioID'];
            $NombreAula = $_POST['NombreAula'];
            $selectAccesible = $_POST['selectAccesible'];
            $selectRecursos = $_POST['selectRecursos'];
            $NombreEdificio = $_POST['NombreEdificio'];
            $Descripcion = $_POST['Descripcion'];
            $CuposAula = $_POST['CuposAula'];
            $aulaID = $_POST['aulaID'];
            $prioridad = $_POST['prioridad'];



            if($aulaID ==-1){
                $query = "SELECT * FROM ca_aulas WHERE ca_aulas.nombre='$NombreAula' AND ca_aulas.id_edificio=$EdificioID";
            }else{
                $query = "SELECT * FROM (SELECT * FROM ca_aulas WHERE ca_aulas.id_aula<>'$aulaID' and ca_aulas.id_edificio=$EdificioID) as aulaX WHERE aulaX.nombre='$NombreAula' AND aulaX.id_edificio=$EdificioID";
            }
            $result =$db->prepare($query);
            $result->execute();

            $ExisteAula=false;
            while ($fila = $result->fetch()) {
                $ExisteAula=true;
                break;
            }
            if ($ExisteAula) {
                $mensaje = "El aula `$NombreAula` ya existe.";
                $color = "azul";
            }
            else{

                if($aulaID ==-1){
                    $query = "INSERT INTO `ca_aulas` (`id_aula`, `nombre`, `capacidad_max`, `id_edificio`) VALUES (NULL, '$NombreAula', '$CuposAula', '$EdificioID');";
                }else{
                    $query = "UPDATE ca_aulas 
                            set ca_aulas.nombre='$NombreAula',
                                ca_aulas.capacidad_max=$CuposAula
                            WHERE ca_aulas.id_aula=$aulaID";
                }
                $result =$db->prepare($query);
                $result->execute();


                if($aulaID >-1){
                    $query = "DELETE FROM ca_aulas_discapacidades WHERE ca_aulas_discapacidades.id_aula=$aulaID;
                            DELETE FROM ca_aula_acondicionamientos WHERE ca_aula_acondicionamientos.id_aula=$aulaID";
                    $result =$db->prepare($query);
                    $result->execute();
                }

                if ($selectAccesible!=NULL){
                    foreach ($selectAccesible as $IdDiscapacidad) {
                        if($aulaID >-1){
                            $query = "INSERT INTO `ca_aulas_discapacidades` (`id_aula`, `id_discapacidad`) VALUES ('$aulaID', '$IdDiscapacidad');";
                        }else{
                            $query = "INSERT INTO `ca_aulas_discapacidades` (`id_aula`, `id_discapacidad`) VALUES ((SELECT Max(ca_aulas.id_aula) FROM ca_aulas order by ca_aulas.id_aula desc LIMIT 1), '$IdDiscapacidad');";
                        }
                        $result =$db->prepare($query);
                        $result->execute();
                    }
                }


                if ($selectRecursos!=NULL){
                    foreach ($selectRecursos as $IdAcondicionamiento) {
                        if($aulaID >-1){
                            $query = "INSERT INTO `ca_aula_acondicionamientos` (`id_aula`, `id_acondicionamiento`) VALUES ('$aulaID', '$IdAcondicionamiento');";
                        }else{
                            $query = "INSERT INTO `ca_aula_acondicionamientos` (`id_aula`, `id_acondicionamiento`) VALUES ((SELECT Max(ca_aulas.id_aula) FROM ca_aulas order by ca_aulas.id_aula desc LIMIT 1), '$IdAcondicionamiento');";
                        }
                        $result =$db->prepare($query);
                        $result->execute();
                    }
                }
                if($aulaID >-1){
                    $mensaje = "El aula `$NombreAula` ha sido editada.";
                }else{
                    $mensaje = "El aula `$NombreAula` ha sido agregada.";
                }
                $color = "verde";
                ?>

                    <script type="text/javascript">
                        DatosRecargarAulas();
                        function DatosRecargarAulas(){
                            data={
                                edificioId:<?php echo "'$EdificioID'"; ?>,
                                nombre:<?php echo "'$NombreEdificio'"; ?>,
                                descripcion:<?php echo "'$Descripcion'"; ?>,
                                prioridad:<?php echo "'$prioridad'"; ?>
                            }
                            $.ajax({
                                async: true,
                                type: "POST",
                                dataType: "html",
                                contentType: "application/x-www-form-urlencoded",
                                success: RecargarAulas,
                                timeout: 4000,
                                error: problemas
                            }); 
                            return false;
                        }
                        function RecargarAulas(){
                            $("#divContenido").empty();
                            $("#divContenido").load('pages/CargaAcademica/Edificios/Aulas.php',data);
                        }
                    </script>
                <?php
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }else{
        echo mensajes("Problemas con el servidor.","azul");
    }
?>