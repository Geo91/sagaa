<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    $bandera = false;
    if (isset($_POST['EdificioID']) && 
        isset($_POST['nombre']) && 
        isset($_POST['Descripcion']) && 
        isset($_POST['Principal']) ){
        try{
            $EdificioID = $_POST['EdificioID'];
            $nombre = $_POST['nombre'];
            $Descripcion = $_POST['Descripcion'];
            $Principal = $_POST['Principal'];

            if ($Principal=="true") {
                $queryE = "UPDATE `ca_edificios` SET `prioridad` = '2' WHERE `ca_edificios`.`prioridad` = '1';";
                $result =$db->prepare($queryE);
                $result->execute();
                $prioridadE=1;
            }else{
                $prioridadE=2;
            }

            $query = "UPDATE ca_edificios SET ca_edificios.nombre='$nombre', 
                                    ca_edificios.descripcion='$Descripcion',
                                    ca_edificios.prioridad='$prioridadE'
                WHERE ca_edificios.id_edificio=$EdificioID";

            $result =$db->prepare($query);
            $result->execute();
            $mensaje = "El edificio `$nombre` ha sido actualizado";
            $color = "verde";
            echo mensajes($mensaje,$color);
            $bandera = true;
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
    if ($bandera) {
?>
<script type="text/javascript">

    enviarDatosEdificio();

    function enviarDatosEdificio(){
        data={
                edificioId:<?php echo "'$EdificioID'"; ?>,
                nombre:<?php echo "'$nombre'"; ?>,
                descripcion:<?php echo "'$Descripcion'"; ?>
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: recargarEdificio,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    }

    function recargarEdificio(){
        $("#divContenido").empty();
        $("#divContenido").load('pages/CargaAcademica/Edificios/Aulas.php',data);
        $("#PaginaEdificios").empty();
        $("#PaginaEdificios").load('pages/CargaAcademica/Edificios/DatosEdificios.php');
    }
</script>
<?php
    }
?>