<?php
    $maindir = "../../";

    require_once ("clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (!isset($_SESSION['ultimoPeriodo'])) {
        $query = "SELECT ca_periodos.id_periodo,
                    ca_periodos.periodo,
                    ca_periodos.anio,
                    ca_periodos.fecha_ingreso_i,
                    ca_periodos.fecha_revision,
                    ca_periodos.fecha_modificacion,
                    ca_periodos.fecha_ingreso_f,
                    (CASE WHEN ca_periodos.fecha_ingreso_f<CURDATE() THEN
                        4
                    WHEN ca_periodos.fecha_modificacion<=CURDATE() THEN
                        3
                    WHEN ca_periodos.fecha_revision<=CURDATE() THEN
                        2
                    WHEN ca_periodos.fecha_ingreso_i<=CURDATE() THEN
                        1
                    ELSE 0 END) as proceso
                FROM ca_periodos
                where ca_periodos.fecha_ingreso_i<= curdate() ORDER by ca_periodos.fecha_ingreso_i DESC LIMIT 1";
        $result =$db->prepare($query);
        $result->execute();
        $banderaUltimoP=false;
        while ($fila = $result->fetch()) {
            $banderaUltimoP=true;
            $id_periodo = $fila['id_periodo'];
            $periodo = $fila['periodo'];
            $anio = $fila['anio'];
            $fechaI = $fila['fecha_ingreso_i'];
            $fechaR = $fila['fecha_revision'];
            $fechaM = $fila['fecha_modificacion'];
            $fechaF = $fila['fecha_ingreso_f'];
            $proceso = $fila['proceso'];
            break;
        }
        if ($banderaUltimoP) {

            $_SESSION['ultimoPeriodo'] = new Periodo($id_periodo,$periodo,$anio,$fechaI,$fechaR,$fechaM,$fechaF,$proceso);
        }else{
            $_SESSION['ultimoPeriodo'] = new Periodo(-1,1,1,'0001/01/01','0001/01/01','0001/01/01','0001/01/01',0);
        }
    }

    if (!isset($_SESSION['auto_increment'])){
        $_SESSION['auto_increment'] = 0;
    }
    try{
        $bandera = false;
        if (!isset($_SESSION['infoPeriodo'])){
            $query = "SELECT ca_periodos.id_periodo,
                        ca_periodos.periodo,
                        ca_periodos.anio,
                        ca_periodos.fecha_ingreso_i,
                        ca_periodos.fecha_revision,
                        ca_periodos.fecha_modificacion,
                        ca_periodos.fecha_ingreso_f,
                        (CASE WHEN ca_periodos.fecha_ingreso_f<CURDATE() THEN
                            4
                        WHEN ca_periodos.fecha_modificacion<=CURDATE() THEN
                            3
                        WHEN ca_periodos.fecha_revision<=CURDATE() THEN
                            2
                        WHEN ca_periodos.fecha_ingreso_i<=CURDATE() THEN
                            1
                        ELSE 0 END) as proceso
                    FROM ca_periodos
                    WHERE curdate() BETWEEN ca_periodos.fecha_ingreso_i AND ca_periodos.fecha_ingreso_f";
            $result =$db->prepare($query);
            $result->execute();
            while ($fila = $result->fetch()) {
                $bandera = true;
                $id_periodo = $fila['id_periodo'];
                $periodo = $fila['periodo'];
                $anio = $fila['anio'];
                $fechaI = $fila['fecha_ingreso_i'];
                $fechaR = $fila['fecha_revision'];
                $fechaM = $fila['fecha_modificacion'];
                $fechaF = $fila['fecha_ingreso_f'];
                $proceso = $fila['proceso'];
                $_SESSION['infoPeriodo'] = new Periodo($id_periodo,$periodo,$anio,$fechaI,$fechaR,$fechaM,$fechaF,$proceso);
                break;
            }
        }
        if (!isset($_SESSION['Id_depto_user'])){
            if($_SESSION['user_rol']==30){
            // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
                $id=$_SESSION['user_id'];
                $query = "SELECT * FROM ca_jefes_departamentos,usuario WHERE ca_jefes_departamentos.id_Usuario=usuario.id_Usuario and usuario.id_Usuario=$id";
                $result =$db->prepare($query);
                $result->execute();
                while ($fila = $result->fetch()) {
                    $_SESSION['Id_depto_user'] = $fila['id_departamento'];
                    break;
                }
            }
        }
        if (!isset($_SESSION['infoCarga']) && $bandera){
            if($_SESSION['user_rol']==30){
            // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
                $id=$_SESSION['user_id'];
                $PeriodoTemp = $_SESSION['infoPeriodo'];
                $query = "SELECT * FROM ca_jefes_departamentos,usuario WHERE ca_jefes_departamentos.id_Usuario=usuario.id_Usuario and usuario.id_Usuario=$id";
                $result =$db->prepare($query);
                $result->execute();
                $banderaJefe = false;
                while ($fila = $result->fetch()) {
                    $departamento = $fila['id_departamento'];
                    $banderaJefe = true;
                    break;
                }
                if ($banderaJefe) {
                    $tempId = $PeriodoTemp->getId();
                    $query = "SELECT * 
                            FROM ca_cargas,ca_departamentos_carreras 
                            WHERE ca_cargas.id_departamento=ca_departamentos_carreras.id_departamento
                                and ca_cargas.id_periodo = $tempId
                                and ca_cargas.id_departamento = $departamento";
                    $result =$db->prepare($query);
                    $result->execute();
                    while ($fila = $result->fetch()) {
                        $id_carga = $fila['id_carga'];
                        $nombre = $fila['nombre'];
                        $aprobado = $fila['aprobado'];
                        $_SESSION['infoCarga'] = new Carga($id_carga,$aprobado,$departamento,$nombre);
                        break;
                    }
                }
            }
        }
    }catch (Exception $e) {
          echo mensajes("<br><br><br>".$e->getMessage(),"rojo");
    }
?>