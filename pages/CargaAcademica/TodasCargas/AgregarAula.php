<?php
    $maindir = "../../../";

    require_once("../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Codigo']) &&
    isset($_POST['Asignatura']) &&
    isset($_POST['Cupos']) &&
    isset($_POST['Profesor']) &&
    isset($_POST['HoraI']) &&
    isset($_POST['HoraF']) &&
    isset($_POST['Lunes']) &&
    isset($_POST['Martes']) &&
    isset($_POST['Miercoles']) &&
    isset($_POST['Jueves']) &&
    isset($_POST['Viernes']) &&
    isset($_POST['Sabado']) &&
    isset($_POST['Domingo']) ){

        $Codigo = $_POST['Codigo'];
        $Asignatura = explode("/", $_POST['Asignatura']);
        $Cupos = $_POST['Cupos'];
        $Profesor = explode("/", $_POST['Profesor']);
        $HoraI = $_POST['HoraI'];
        $HoraF = $_POST['HoraF'];
        $Lunes = $_POST['Lunes'];
        $Martes = $_POST['Martes'];
        $Miercoles = $_POST['Miercoles'];
        $Jueves = $_POST['Jueves'];
        $Viernes = $_POST['Viernes'];
        $Sabado = $_POST['Sabado'];
        $Domingo = $_POST['Domingo'];
    }
        
?>

<div class="col-md-9 col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading"><button id="RegresarFormularioSeccion" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span> Regresar</button> Informacion de sección</div>

            <div class="panel-body" >
                <div class="row">
                    <div class="col-lg-12">

                    <!--************************Datos*********************-->
                            <div class="row" >                    
                                <label class=" col-sm-2 control-label" >Código</label>                           
                                <div class=" col-sm-8" >
                                    <input id="SeccionCodigo" name="SeccionCodigo" type="hidden" value="<?php echo $Codigo; ?>" ></input>
                                    <?php echo $Codigo; ?>
                                </div> 
                            </div>  

                            <div class="row" >                    
                                <label class=" col-sm-2 control-label" >Asignatura</label>                           
                                <div class=" col-sm-8" >
                                    <input id="SeccionAsignatura" name="SeccionAsignatura" type="hidden" value="<?php echo $Asignatura[0]; ?>" ></input>
                                    <?php echo $Asignatura[1]; ?>
                                </div> 
                            </div>  

                            <div class="row" >                    
                                <label class=" col-sm-2 control-label" >Cupos</label>                           
                                <div class=" col-sm-8" >
                                    <input id="SeccionCupos" name="SeccionCupos" type="hidden" value="<?php echo $Cupos; ?>" ></input>
                                    <?php echo $Cupos; ?>
                                </div> 
                            </div>  

                            <div class="row" >                    
                                <label class=" col-sm-2 control-label" >Profesor</label>                           
                                <div class=" col-sm-8" >
                                    <input id="SeccionProfesor" name="SeccionProfesor" type="hidden" value="<?php echo $Profesor[0]; ?>" ></input>
                                    <?php echo $Profesor[1]; ?>
                                </div> 
                            </div>  

                            <div class="row" >                    
                                <label class=" col-sm-2 control-label" >Hora inicio</label>      
                                <div class=" col-sm-8" >
                                    <input id="SeccionHoraI" name="SeccionHoraI" type="hidden" value="<?php echo $HoraI; ?>" ></input>
                                    <?php echo $HoraI; ?>
                                </div> 
                            </div> 

                            <div class="row" >                    
                                <label class=" col-sm-2 control-label" >Hora fin</label>      
                                <div class=" col-sm-8" >
                                    <input id="SeccionHoraF" name="SeccionHoraF" type="hidden" value="<?php echo $HoraF; ?>" ></input>
                                    <?php echo $HoraF; ?>
                                </div> 
                            </div> 
    <!--**************************dias**********************-->
                            <div class="row">                    
                                <label class=" col-sm-2 control-label" >Dias</label>      
                                <div class=" col-sm-8" >
                                    <input id="Lunes" name="Lunes" type="hidden" value="<?php echo $Lunes; ?>" ></input>
                                    <input id="Martes" name="Martes" type="hidden" value="<?php echo $Martes; ?>" ></input>
                                    <input id="Miercoles" name="Miercoles" type="hidden" value="<?php echo $Miercoles; ?>" ></input>
                                    <input id="Jueves" name="Jueves" type="hidden" value="<?php echo $Jueves; ?>" ></input>
                                    <input id="Viernes" name="Viernes" type="hidden" value="<?php echo $Viernes; ?>" ></input>
                                    <input id="Sabado" name="Sabado" type="hidden" value="<?php echo $Sabado; ?>" ></input>
                                    <input id="Domingo" name="Domingo" type="hidden" value="<?php echo $Domingo; ?>" ></input>
                                    <?php 
                                    if($Lunes=="true"){ 
                                    ?>
                                        <div class="row" >
                                            Lunes
                                        </div>
                                    <?php } 
                                    if($Martes=="true"){ ?>
                                        <div class="row" >
                                            Martes
                                        </div>
                                    <?php } 
                                    if($Miercoles=="true"){ ?>
                                        <div class="row" >
                                            Miércoles
                                        </div>
                                    <?php } 
                                    if($Jueves=="true"){ ?>
                                        <div class="row" >
                                            Jueves
                                        </div>
                                    <?php } 
                                    if($Viernes=="true"){ ?>
                                        <div class="row" >
                                            Viernes
                                        </div>
                                    <?php } 
                                    if($Sabado=="true"){ ?>
                                        <div class="row" >
                                            Sábado
                                        </div>
                                    <?php } 
                                    if($Domingo=="true"){ ?>
                                        <div class="row" >
                                            Domingo
                                        </div>
                                    <?php } ?>
                                </div> 
                            </div> 
    <!--**************************dias**********************-->
                    <!--************************Datos*********************-->

                    </div>
                </div>
            </div>                                    
        </div> 
    <div id="tablaAulasDisponibles">
        <div class="panel panel-default">
            <div class="panel-heading"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Aulas disponibles en el horario y cupos seleccionados</div>
                <?php include("DatosAulas.php"); ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    
$(document).ready(function(){   
    $("#RegresarFormularioSeccion").click(function(event) {
        event.preventDefault();
        data={
                Codigo:$('#SeccionCodigo').val(),
                Asignatura:$('#SeccionAsignatura').val(),
                Cupos:$('#SeccionCupos').val(),
                Profesor:$('#SeccionProfesor').val(),
                HoraI:$('#SeccionHoraI').val(),
                HoraF:$('#SeccionHoraF').val(),
                Lunes:$('#Lunes').val(),
                Martes:$('#Martes').val(),
                Miercoles:$('#Miercoles').val(),
                Jueves:$('#Jueves').val(),
                Viernes:$('#Viernes').val(),
                Sabado:$('#Sabado').val(),
                Domingo:$('#Domingo').val()
            }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: regresarInfoSeccion,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });
    function regresarInfoSeccion(){
        $("#contenedorCarga").load('pages/CargaAcademica/TodasCargas/AgregarSeccion.php',data);
    }
});
</script>
