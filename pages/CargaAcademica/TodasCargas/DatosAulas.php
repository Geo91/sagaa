<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    if(isset($_SESSION['Id_depto_user']) ){
        $_SESSION['auto_increment'] = $_SESSION['auto_increment']+1;
        $auto_increment =$_SESSION['auto_increment'];
        $departamento = $_SESSION['Id_depto_user'];
        $id_carga = $_SESSION['id_carga'];

        $query1 = "SELECT * FROM ca_profesores_discapacidades
                    WHERE ca_profesores_discapacidades.No_Empleado='$Profesor[0]'";
        $result =$db->prepare($query1);
        $result->execute();
        $Discapacitado = false;
        while ($fila = $result->fetch()) {
            $Discapacitado = true;
            break;
        }
        $query1 = "SELECT ca_aulas.id_aula,ca_aulas.nombre as aulaNom, ca_aulas.id_edificio as edificioId, ca_aulas.capacidad_max as capacidad, ca_edificios.nombre as edificioNom,  COUNT(ca_aulas_discapacidades.id_discapacidad) as accesibilidad,COUNT(ca_aula_acondicionamientos.id_acondicionamiento) as acondicionamiento
FROM ca_aulas left JOIN ca_edificios 
        on ca_aulas.id_edificio = ca_edificios.id_edificio 
    LEFT JOIN ca_aulas_discapacidades 
        on ca_aulas.id_aula=ca_aulas_discapacidades.id_aula 
    LEFT JOIN ca_aula_acondicionamientos
        on ca_aulas.id_aula=ca_aula_acondicionamientos.id_aula
    where ca_aulas.id_aula not in (select id_aula 
from ca_secciones,ca_cargas,ca_periodos,ca_secciones_dias,ca_dias
where ca_secciones.id_seccion=ca_secciones_dias.id_seccion
    and ca_secciones_dias.id_dia=ca_dias.id_dia
    and ca_secciones.id_carga=ca_cargas.id_carga 
    and ca_cargas.id_periodo = ca_periodos.id_periodo
    and ca_periodos.id_periodo=(select ca_cargas.id_periodo from ca_cargas where ca_cargas.id_carga=$id_carga)
    and (('$HoraI:00' >= ca_secciones.Hora_inicio 
                and '$HoraI:00' < ca_secciones.Hora_fin)
            or ('$HoraF:00' > ca_secciones.Hora_inicio 
                and '$HoraF:00' <= ca_secciones.Hora_fin)
            or (ca_secciones.Hora_inicio > '$HoraI:00'
                and ca_secciones.Hora_inicio < '$HoraF:00'))
    and (";
                $logico="";
                if($Lunes=="true"){
                    $query1=$query1.$logico." ca_dias.nombre='Lunes' ";
                    $logico=" or ";
                }
                if($Martes=="true"){
                    $query1=$query1.$logico." ca_dias.nombre='Martes' ";
                    $logico=" or ";
                }
                if($Miercoles=="true"){
                    $query1=$query1.$logico." ca_dias.nombre='Miércoles' ";
                    $logico=" or ";
                }
                if($Jueves=="true"){
                    $query1=$query1.$logico." ca_dias.nombre='Jueves' ";
                    $logico=" or ";
                }
                if($Viernes=="true"){
                    $query1=$query1.$logico." ca_dias.nombre='Viernes' ";
                    $logico=" or ";
                }
                if($Sabado=="true"){
                    $query1=$query1.$logico." ca_dias.nombre='Sábado' ";
                    $logico=" or ";
                }
                if($Domingo=="true"){
                    $query1=$query1.$logico." ca_dias.nombre='Domingo' ";
                    $logico=" or ";
                }
                $query1 = $query1."))
        and ca_aulas.capacidad_max>=($Cupos*0.9) ";
        if ($Discapacitado){
            $query1 = $query1." and ca_aulas.id_aula in (SELECT ca_aulas_discapacidades.id_aula FROM ca_profesores_discapacidades,ca_aulas_discapacidades
                WHERE ca_profesores_discapacidades.id_discapacidad=ca_aulas_discapacidades.id_discapacidad
                and ca_profesores_discapacidades.No_Empleado='$Profesor[0]') ";
        }
    $query1 = $query1." GROUP by ca_aulas.id_aula 
                        order by ca_edificios.prioridad,ca_edificios.nombre, ca_aulas.capacidad_max";
?>
<?php if ($Discapacitado){
    echo "<center><h4>Profesor con necesidades especiales</h4></center>";
} ?>
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaAulas" class="table table-bordered table-striped">
            <thead>
                <tr>                                            
                    <th>Seleccionar</th>
                    <th>Aula</th>
                    <th>Edificio</th>
                    <th>Capacidad</th>
                    <th>Accesible a:</th>
                    <th>Recursos</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $result =$db->prepare($query1);
                $result->execute();
                while ($fila = $result->fetch()) {
                    ?>
                    <tr data-id='<?php echo $fila["id_aula"]; ?>'>
                        <td><a class="lnkSeleccionar<?php echo $auto_increment; ?>" href="#"><i class="glyphicon glyphicon-ok"></i>Seleccionar</a></li></td>
                        <td><?php echo $fila["aulaNom"]; ?></td>
                        <td id='<?php echo $fila["edificioId"]; ?>'><?php echo $fila["edificioNom"]; ?></td>
                        <td><?php echo $fila["capacidad"]; ?></td>
                        <td>
                            <center>
                                <button type="button" class="accesibilidad btn btn-primary   glyphicon glyphicon glyphicon-list-alt"  title="Ver accesibilidad" 
                                <?php if ($fila["accesibilidad"]==0){ ?>
                                        disabled="true";
                                    <?php
                                    } ?>
                                    >
                            </center>
                        </td>       
                        <td>
                            <center>
                                <button type="button" class="Acondicionamiento btn btn-primary glyphicon glyphicon-film"  title="Ver acondicionamiento"
                                <?php if ($fila["acondicionamiento"]==0){ ?>
                                        disabled="true";
                                    <?php
                                    } ?>
                                    >
                            </center>
                        </td>   
                    </tr>
            <?php } ?>
            </tbody>
            <tfoot>
                <tr>                                  
                    <th>Seleccionar</th>    
                    <th>Aula</th>
                    <th>Edificio</th>
                    <th>Capacidad</th>
                    <th>Accesible a:</th>
                    <th>recursos</th>
                </tr>
            </tfoot>
        </table>
    </div><!-- /.box-body -->
</div>

<!--******************modal de edicion*****************-->
<div class="modal fade" id="modalSubTabla" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Informacion adicional</h4>
        </div>
        <div class="modal-body">
            <div id="subTabla"></div>
        </div>
      </div>
    </div>
</div>
<!--******************modal de edicion*****************-->

<?php
    }else{
        echo mensajes("Usted no tiene acceso a este formulario","rojo");
    }
?>
<script type="text/javascript">   
//opciones para buscador en la tabla tablaAulas
    $('#tablaAulas')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaAulas').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No hay aulas disponibles",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
$(document).ready(function(){   

    $(document).on("click", ".lnkSeleccionar<?php echo $auto_increment; ?>", function (e) {
        idAula =$(this).parents("tr").data("id");
        data4={
                id_carga:$('#id_carga').val(),
                Codigo:$('#SeccionCodigo').val(),
                Asignatura:$('#SeccionAsignatura').val(),
                Cupos:$('#SeccionCupos').val(),
                Profesor:$('#SeccionProfesor').val(),
                id_aula:idAula,
                HoraI:$('#SeccionHoraI').val(),
                HoraF:$('#SeccionHoraF').val(),
                Lunes:$('#Lunes').val(),
                Martes:$('#Martes').val(),
                Miercoles:$('#Miercoles').val(),
                Jueves:$('#Jueves').val(),
                Viernes:$('#Viernes').val(),
                Sabado:$('#Sabado').val(),
                Domingo:$('#Domingo').val()

        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: guardarSeccion,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });

    function guardarSeccion(){
        $("#divRespuesta").empty();
        $("#divRespuesta").load('pages/CargaAcademica/TodasCargas/GuardarSeccion.php',data4);
        $("#contenedorCarga").html('');
        $("#contenedorCarga").load('pages/CargaAcademica/TodasCargas/AgregarSeccion.php');
    }
});
</script>
