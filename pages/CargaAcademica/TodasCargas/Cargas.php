<?php
    $maindir = "../../../";
    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."conexion/config.inc.php"); 
    require_once($maindir."Datos/funciones.php");

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    if(isset($_SESSION['ultimoPeriodo']) && isset($_POST['selectDeptoID'])){
        $tempPeriodo = $_SESSION['ultimoPeriodo'];
        $IDdepto = $_POST['selectDeptoID'];
        $_SESSION['Id_depto_user'] = $IDdepto;

        $procesoActual = $tempPeriodo->getProceso();
        $periodoID = $tempPeriodo->getId();
        $periodo = $tempPeriodo->getPeriodo();
        $anio = $tempPeriodo->getAnio();
        $fechaI = $tempPeriodo->getFechaI();
        $fechaR = $tempPeriodo->getFechaR();
        $fechaM = $tempPeriodo->getFechaM();
        $fechaF = $tempPeriodo->getFechaF();

        $id_carga ="";
        $deptoNombre="";
        $_SESSION['id_carga']="";
        $queryNDepto = "SELECT ca_cargas.id_carga,
                        ca_departamentos_carreras.nombre
                    FROM ca_cargas,ca_departamentos_carreras
                    WHERE ca_cargas.id_departamento=ca_departamentos_carreras.id_departamento
                        and ca_cargas.id_periodo=$periodoID
                        and ca_departamentos_carreras.id_departamento=$IDdepto";

        $result =$db->prepare($queryNDepto);
        $result->execute();
        
        while ($fila = $result->fetch()) {
            $id_carga=$fila['id_carga'];
            $_SESSION['id_carga'] = $id_carga;
            $deptoNombre=$fila['nombre'];
            $_SESSION['deptoNombre']=$deptoNombre;
            break;
        }
?>
<input type="hidden" class="form-control" id="IDdeptoP" name="IDdeptoP" value="<?php echo $IDdepto; ?>">

<div class="row">
    <div class="col-lg-12">
            <h3 class="page-header panel-primary"><button onclick="regresarPrincipal()" id="RegresarFormulario" class="btn btn-warning"><span class="glyphicon glyphicon-arrow-left"></span> Volver </button><center><strong>Carga académica <?php echo $deptoNombre; ?> </strong></center></h3>
            
    </div> 
</div>
<div class="row" id="divRespuesta"></div> 
<?php if (!isset($_SESSION['infoPeriodo'])) { 
    $fechaFinal = DateTime::createFromFormat('Y-m-d',$fechaF);
    $today = date("Y-m-d");
    $hoy = DateTime::createFromFormat('Y-m-d', $today);
    $intervalo = date_diff($fechaFinal, $hoy);
    $inter = $intervalo -> format("%d");
    $limite = 3;
?>
<div class="row">
    <div class="panel panel-primary" <?php 
    if($inter < $limite){
        echo "";
    }else{
        echo "hidden";
    } ?> >
        <div class="panel-heading" role="tab" id="headingTwo">
          <h4 class="panel-title">
            <a id="panelRegistro" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            <span id="flechaRegistro" class="glyphicon glyphicon-chevron-down"></span> Registro de secciones
            </a>
          </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">




    <div id="contenedorCarga">
        <?php require_once("AgregarSeccion.php"); ?>
    </div> 
    <div id="infoPanel1" class="col-sm-3">
        <div class="row">
                <div class="col-md-offset-8">
                </div>
        </div>
        <div class="row">
            <div class="panel panel-info">

                <div class="panel-heading"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Información</div>

                <div class="panel-body" >
                    <div class="row">
                        <div class="col-lg-12">
                            <input id="id_carga" name="id_carga" type="hidden" value="<?php echo $id_carga; ?>" ></input>
                        <!--************************Datos*********************-->
                                <div class="row" >                    
                                    <label class=" col-sm-2 control-label" >Período</label>                  
                                </div>                     
                                <div class="row">                          
                                    <div class=" col-sm-2" >
                                        <?php echo $periodo; ?>
                                    </div> 
                                </div>  

                                <div class="row" >                    
                                    <label class=" col-sm-2 control-label" >Año</label>                  
                                </div>                     
                                <div class="row">                          
                                    <div class=" col-sm-2" >                      
                                        <?php echo $anio; ?>  
                                    </div>                  
                                </div>

                                <div class="row" >                    
                                    <label class=" col-sm-2 control-label" >Departamento</label>                  
                                </div>                     
                                <div class="row">                          
                                    <div class=" col-sm-12" >                   
                                        <?php echo $deptoNombre; ?>  
                                    </div>                  
                                </div>

                                <hr>

                                <div class="row" >                    
                                    <label class="col-sm-12 control-label" >Ingreso</label> 
                                </div>
                                <div class="row" >                    
                                    <div class="col-sm-8 ">    
                                        <?php echo fecha($fechaI); ?>  
                                    </div>                 
                                </div>
                                <br>
                                <div class="row" >                    
                                    <label class="col-sm-12 control-label" >Revisión</label> 
                                </div>
                                <div class="row" >                    
                                    <div class="col-sm-8 ">    
                                        <?php echo fecha($fechaR); ?>  
                                    </div>                 
                                </div>
                                <br>
                                <div class="row" >                    
                                    <label class="col-sm-12 control-label" >Modificación</label> 
                                </div>
                                <div class="row" >                    
                                    <div class="col-sm-8 ">    
                                        <?php echo fecha($fechaM); ?>  
                                    </div>                 
                                </div>
                                <br>
                                <div class="row" >                    
                                    <label class="col-sm-12 control-label" >Finalización</label> 
                                </div>
                                <div class="row" >                    
                                    <div class="col-sm-8 ">    
                                        <?php echo fecha($fechaF); ?>  
                                    </div>                 
                                </div>

                        </div>   
                        <!--************************Datos*********************-->

                    </div>
                </div>
            </div>                                    
        </div>
    </div> 


          </div>
        </div>
    </div>
</div>
<?php }else{
    echo mensajes("La modificación se habilitará hasta que el periodo de registro de la cargas haya terminado. <br>(Después del ".fecha($fechaF).")","azul");
} ?>
<div class="row" id="divRespuestaEditar"></div> 
<div class="row">



    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingTwo">
          <h4 class="panel-title">
            <a id="panelCarga" data-toggle="collapse" data-parent="#accordion" href="#collapseThreeCarga" aria-expanded="true" aria-controls="collapseThree">
              <span id="flechaCarga" class="glyphicon glyphicon-chevron-up"></span> Todas las secciones
            </a>
          </h4>
        </div>
        <div id="collapseThreeCarga" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="true">
          <div class="panel-body">
<?php if($procesoActual>=2){ ?>
            <div id="Observaciones" class="row">
                <?php require_once("Observaciones.php"); ?>                
            </div>

            <div id="TablaCarga" class="row">
                <?php include("DatosCarga.php"); ?>
            </div>
<?php }else{
    echo mensajes("La revisión se habilitará a partir del ".fecha($fechaR),"azul");
} ?>
          </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalTemp" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        <div class="modal-header" style = "background-color:#0FA6C3">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel" style = "color:white">Editar Sección</h4>
        </div>
        <div class="modal-body">
            <div id="subModalTemp">
            </div>
        </div>
      </div>
    </div>
</div>


<div class="modal fade" id="modalEditMatricula" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header" style = "background-color:#0FA6C3">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style = "color:white">Cantidad de matriculados</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" role="form" id="formEditMatricula" name="formEditMatricula">

                <input type="hidden" class="form-control" id="idSeccionMatr" name="idSeccionMatr" value="0">

                <div class="row form-group" >                    
                    <label class=" col-sm-5 control-label" >Nueva cantidad</label>
                    <div class="col-sm-5">     
                        <input type="number" class="form-control" id="nMatricula" name="nMatricula" min="0" max="150" required value="0">      
                    </div>                  
                </div>
                <div class="modal-footer">
                    <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button  id="guardarCambios" class="btn btn-primary" ><span class=" glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                </div>

            </form>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript" src="pages/CargaAcademica/TodasCargas/Script.js"></script>

<?php
    }else{
        echo mensajes("No hay periodos registrados.","azul");
    }
?>