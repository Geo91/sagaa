<?php
    $maindir = "../../../";
    require_once ("clases.php");
    require_once ("../clases.php");
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $deptoNombre="";
    if (isset($_SESSION['deptoNombre'])) {
        $deptoNombre=$_SESSION['deptoNombre'];
    }
    $modificaciones = true;
    if (isset($_SESSION['infoPeriodo'])) {
        $modificaciones = false;
    }
    $tempUltP = $_SESSION['ultimoPeriodo'];
    $tempUltPID = $tempUltP->getId();
    $procesoActual = $tempUltP->getProceso();
    $fechaI = $tempUltP->getFechaI();
    $fechaR = $tempUltP->getFechaR();
    $fechaM = $tempUltP->getFechaM();
    $fechaF = $tempUltP->getFechaF();
    $idDepto = $_SESSION['Id_depto_user'];
    $query1 = "SELECT ca_secciones.id_seccion,
                    ca_secciones.codigo_seccion,
                    ca_secciones.id_asignatura,
                    ca_asignaturas.nombre as nAsignatura,
                    ca_secciones.cupos_max,
                    ca_secciones.matriculados,
                    ca_secciones.Hora_inicio,
                    ca_secciones.Hora_fin,
                    ca_dias.id_dia,
                    ca_dias.nombre as Dia,
                    ca_secciones.id_aula,
                    ca_aulas.nombre as nAula,
                    ca_edificios.nombre as nEdificio,
                    ca_secciones.No_Empleado,
                    CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'nombreE'
                FROM ca_secciones,ca_asignaturas,ca_aulas,ca_edificios,empleado,persona,ca_secciones_dias,ca_dias
                WHERE ca_secciones.id_asignatura=ca_asignaturas.id_asignatura
                    and ca_secciones.id_seccion=ca_secciones_dias.id_seccion
                    and ca_secciones_dias.id_dia = ca_dias.id_dia
                    and ca_secciones.id_aula=ca_aulas.id_aula
                    and ca_aulas.id_edificio=ca_edificios.id_edificio
                    and ca_secciones.No_Empleado=empleado.No_Empleado
                    and empleado.N_identidad=persona.N_identidad
                    and ca_secciones.id_carga=(SELECT ca_cargas.id_carga
                                FROM ca_cargas,ca_periodos
                                WHERE ca_cargas.id_periodo=ca_periodos.id_periodo
                                    and ca_periodos.id_periodo=$tempUltPID
                                    and ca_cargas.id_departamento=$idDepto)";
    $result =$db->prepare($query1);
    $result->execute();
    $tempID=-1;
    $seccionesTabla = array();
    $Registros = false;
    while ($fila = $result->fetch()) {
        if ($tempID!=$fila["id_seccion"]) {
            $tempID = $fila["id_seccion"];
            $id_seccion = $fila["id_seccion"];
            $codigo_seccion = $fila["codigo_seccion"];
            $id_asignatura = $fila["id_asignatura"];
            $nAsignatura = $fila["nAsignatura"];
            $cupos_max = $fila["cupos_max"];
            $matriculados = $fila["matriculados"];
            $Hora_inicio = $fila["Hora_inicio"];
            $Hora_fin = $fila["Hora_fin"];
            $id_aula = $fila["id_aula"];
            $nAula = $fila["nAula"];
            $nEdificio = $fila["nEdificio"];
            $No_Empleado = $fila["No_Empleado"];
            $nombreE = $fila["nombreE"];
            $seccionesTabla[$fila["id_seccion"]] = new SeccionCarga($id_seccion,
                                     $codigo_seccion,
                                     $id_asignatura,
                                     $nAsignatura,
                                     $cupos_max,
                                     $matriculados, 
                                     $Hora_inicio,
                                     $Hora_fin,
                                     $id_aula,
                                     $nAula,
                                     $nEdificio,
                                     $No_Empleado,
                                     $nombreE);
            $seccionesTabla[$fila["id_seccion"]]->Dias[$fila["id_aula"]]=$fila["Dia"];
        }else{
            $seccionesTabla[$fila["id_seccion"]]->Dias[$fila["id_dia"]]=$fila["Dia"];
        }
        $Registros = true;
    }
?>
<script type="text/javascript">var ArregloDias = [];</script>
<?php
if ($Registros) {
    foreach ($seccionesTabla as $key => $subSeccion) { 
        $TempIDseccion = $subSeccion->getid_seccion();
        $jScript = "<script type='text/javascript'> 
                    ArregloDias[$TempIDseccion]=[";
        $largo = count($subSeccion->Dias);
        $indice = 0;
        foreach ($subSeccion->Dias as $value) {
            $indice = $indice+1;
            if ($indice==$largo) {
                $jScript=$jScript."'$value'];</script>";
            }else{
                $jScript=$jScript."'$value',";
            }
        }
        echo $jScript;
    }
}

$queryPermiso = "SELECT * FROM ca_modificaciones_cargas,ca_cargas
WHERE ca_modificaciones_cargas.id_carga=ca_cargas.id_carga
    and ca_modificaciones_cargas.fechaMod>=curdate()
    and ca_cargas.id_periodo=$tempUltPID
    and ca_cargas.id_departamento=$idDepto";

$result =$db->prepare($queryPermiso);
$result->execute();
$existePermiso=false;
while ($fila = $result->fetch()) {
    $existePermiso=true;
    $fechaMod=$fila['fechaMod'];
    break;
}
if ($modificaciones) {
?>
<div align="right">
    <button id="HabilitarModJefe<?php echo $auto_increment; ?>" class="btn btn-warning" 
    <?php if($existePermiso){
        echo " disabled='true' ";
    } ?>>Permitir modificación al jefe de departamento</button> (Solo el 
    <?php if($existePermiso){
        echo fecha($fechaMod);
    }else{
        echo " día siguiente de hoy";
        } ?>) &nbsp&nbsp&nbsp&nbsp
</div><br>
<?php 
} ?>

<input type="hidden" class="form-control" id="tempUltPID" name="tempUltPID" value="<?php echo $tempUltPID; ?>">
<input type="hidden" class="form-control" id="idDepto" name="idDepto" value="<?php echo $idDepto; ?>">
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaCargasR" class="table table-bordered table-striped">
            <thead>
                <tr>                                            
                    <th>Código</th>
                    <th>Asignatura</th>
                    <th>Cupos</th>
                    <th>Matriculados</th>
                    <th>Hora: inicio/ fin</th>
                    <th>Dias</th>
                    <th>Aula</th>
                    <th>Edificio</th>
                    <th>Profesor</th>
                    <?php if ($modificaciones) {
                        echo "<th>Editar/ Eliminar</th>";
                    } ?>
                </tr>
            </thead>
            <tbody>
            <?php
            if ($Registros) {
                foreach ($seccionesTabla as $key => $subSeccion) { 
                    $TempIDseccion = $subSeccion->getid_seccion();
                    ?>
                    <tr data-id='<?php echo $TempIDseccion; ?>'  
                    data-asignatura='<?php echo $subSeccion->getid_asignatura(); ?>'
                    data-aula='<?php echo $subSeccion->getid_aula(); ?>'
                    data-empleado='<?php echo $subSeccion->getNo_Empleado(); ?>'
                    data-hori='<?php echo $subSeccion->getHora_inicio(); ?>'
                    data-horf='<?php echo $subSeccion->getHora_fin(); ?>'
                    data-matr='<?php echo $subSeccion->getmatriculados(); ?>'>
                        <td><?php echo $subSeccion->getcodigo_seccion(); ?></td>
                        <td><?php echo $subSeccion->getnAsignatura(); ?></td>
                        <td><?php echo $subSeccion->getcupos_max(); ?></td>
                        <td>
                            <?php if ($modificaciones) {
                                echo "<a class='cambiarMatricula$auto_increment' href='#'>".$subSeccion->getmatriculados()."</a>";
                            }else{
                                echo $subSeccion->getmatriculados();
                            }?>
                        </td>
                        <td><?php echo substr($subSeccion->getHora_inicio(),0,-3)."\n <br/>". substr($subSeccion->getHora_fin(),0,-3); ?></td>
                        </td>
                        <td>
                        <?php foreach ($subSeccion->Dias as $value) {
                                if ($value!='Sábado'){
                                    echo substr($value, 0,2)." \n <br>";
                                }else{
                                    echo "Sa \n <br>";
                                }
                            }
                        ?></td>
                        <td><?php echo $subSeccion->getnAula(); ?></td>
                        <td><?php echo $subSeccion->getnEdificio(); ?></td>
                        <td><?php echo $subSeccion->getnombreE(); ?></td>
                        <?php if ($modificaciones) { ?>
                        <td>
                            <div class="btn-group-vertical">
                                <button type="button" class="editarSeccion<?php echo $auto_increment; ?> btn btn-primary glyphicon glyphicon-edit"  title="Editar contrato">
                                <button type="button" class="eliminarSeccion<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-remove"  title="Editar contrato">
                            </div>
                        </td>
                        <?php } ?>
                    </tr>
            <?php } 
            }?>
            </tbody>
            <tfoot>
                <tr>                                           
                    <th>Código</th>
                    <th>Asignatura</th>
                    <th>Cupos</th>
                    <th>Matriculados</th>
                    <th>Hora: inicio/ fin</th>
                    <th>Dias</th>
                    <th>Aula</th>
                    <th>Edificio</th>
                    <th>Profesor</th>
                    <?php if ($modificaciones) {
                        echo "<th>Editar/ Eliminar</th>";
                    } ?>
                </tr>
            </tfoot>
        </table>
    </div><!-- /.box-body -->
</div>
<script type="text/javascript">   

$(document).ready(function(){   
    $("#HabilitarModJefe<?php echo $auto_increment; ?>").click(function(event) {
        event.preventDefault();
        if (confirm("¿Está seguro que desea habilitar las modificaciones de esta carga durante el día de mañana?")) {
            data={
                tempUltPID: $('#tempUltPID').val(),
                idDepto:$('#idDepto').val()
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                success: permitirModJefe,
                timeout: 4000,
                error: problemas
            }); 
            return false;
        }
    });
});
function permitirModJefe(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/TodasCargas/PermitirModJefe.php',data);
}
$(document).on("click", ".editarSeccion<?php echo $auto_increment; ?>", function () {
        Lunes = false;
        Martes = false;
        Miercoles = false;
        Jueves = false;
        Viernes = false;
        Sabado = false;
        Domingo = false;
        if (ArregloDias[$(this).parents("tr").data("id")].indexOf("Lunes")>-1){
            Lunes = true;
        }
        if (ArregloDias[$(this).parents("tr").data("id")].indexOf("Martes")>-1){
            Martes = true;
        }
        if (ArregloDias[$(this).parents("tr").data("id")].indexOf("Miércoles")>-1){
            Miercoles = true;
        }
        if (ArregloDias[$(this).parents("tr").data("id")].indexOf("Jueves")>-1){
            Jueves = true;
        }
        if (ArregloDias[$(this).parents("tr").data("id")].indexOf("Viernes")>-1){
            Viernes = true;
        }
        if (ArregloDias[$(this).parents("tr").data("id")].indexOf("Sábado")>-1){
            Sabado = true;
        }
        if (ArregloDias[$(this).parents("tr").data("id")].indexOf("Domingo")>-1){
            Domingo = true;
        }
        data={
            Codigo: $(this).parents("tr").find("td").eq(0).html(),
            Asignatura: $(this).parents("tr").data("asignatura"),
            Cupos:$(this).parents("tr").find("td").eq(2).html(),
            Profesor:$(this).parents("tr").data("empleado"),
            HoraI:$(this).parents("tr").data("hori"),
            HoraF:$(this).parents("tr").data("horf"),
            Aula:$(this).parents("tr").data("aula"),
            IDseccion:$(this).parents("tr").data("id"),
            Lunes:Lunes,
            Martes:Martes,
            Miercoles:Miercoles,
            Jueves:Jueves,
            Viernes:Viernes,
            Sabado:Sabado,
            Domingo:Domingo
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: editarSeccion,
            timeout: 4000,
            error: problemas
        });
        return false;
});

$(document).on("click", ".eliminarSeccion<?php echo $auto_increment; ?>", function () {
    if (confirm("¿Está seguro que desea eliminar esta sección?")){
        data={
            seccionID:$(this).parents("tr").data("id"),
            Codigo:$(this).parents("tr").find("td").eq(0).html()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: eliminarSeccion,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    }
});

$(document).on("click", ".cambiarMatricula<?php echo $auto_increment; ?>", function () {
    $('#nMatricula').val($(this).parents("tr").data("matr"));
    $('#idSeccionMatr').val($(this).parents("tr").data("id"));
    $("#modalEditMatricula").modal('show');
});
</script>
<script type="text/javascript">
//opciones para buscador en la tabla tablaAsignaturas
//-----------------Exportar Tabla-------------------
    $('#tablaCargasR')
        .removeClass('display')
        .addClass('table table-striped table-bordered');
    $('#tablaCargasR').DataTable({
                dom: 'Blfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                download: 'open',
                title:'<?php echo "Carga académica del departamento de ".$deptoNombre; ?>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                download: 'open',
                orientation: 'landscape',
                pageSize: 'LETTER',
                title:'<?php echo "Carga académica del departamento de ".$deptoNombre; ?>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'print',
                title:'<?php echo "Carga académica del departamento de ".$deptoNombre; ?>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'
        ]
    });

    $('.toggle-vis1').on( 'click', function (e) {
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
 
        // Toggle the visibility
        column.visible( ! column.visible() );
        $(this).attr('color', ' blue');
    });
    $(".toggle-vis1").prop("disabled", "");
</script>