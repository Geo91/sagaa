<?php
    $maindir = "../../../";

    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    $query = "SELECT * FROM `ca_departamentos_carreras`";

    if (isset($_SESSION['ultimoPeriodo'])){
        if ($_SESSION['ultimoPeriodo']->getId()!=-1) {
            $tempPeriodo = $_SESSION['ultimoPeriodo'];
            $periodo = $tempPeriodo->getPeriodo();
            $anio = $tempPeriodo->getAnio();
            $fechaI = $tempPeriodo->getFechaI();
            $fechaR = $tempPeriodo->getFechaR();
            $fechaM = $tempPeriodo->getFechaM();
            $fechaF = $tempPeriodo->getFechaF();
?>

<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Cargas por departamentos</div>
    <div class="panel-body" >
        <div class="row">
                <form class="form-horizontal col-sm-7" role="form" id="formSelectDepto" name="formSelectDepto">


                    <!--**************select*****************-->
                    <div class="row">
                        <label class=" control-label" > &nbsp&nbsp Seleccione un departamento</label>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-8">                            
                            <select class="form-control" id="selectDeptoID" name="selectDeptoID" required>
                                <option value="">Seleccione una opcion</option>
                                <?php
                                    $result =$db->prepare($query);
                                    $result->execute();
                                    while ($fila = $result->fetch()) {
                                ?> 
                                        <option value="<?php echo $fila['id_departamento']; ?>"
                                        <?php if (isset($_POST['IDdeptoP'])) {
                                            if ($fila['id_departamento']==$_POST['IDdeptoP']) {
                                                echo " selected ";
                                            }
                                        } ?>>
                                            <?php echo $fila["nombre"]; ?>
                                        </option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button  id="guardarCambios" class="btn btn-primary" <?php //if($fechaF > date('d-m-y')){ echo "disabled"; } ?> ><span class="glyphicon glyphicon-share-alt"></span> Cargar</button>
                    </div>

                </form>


                <div class="panel panel-primary col-sm-5">
                        <div class=" col-sm-9">
                                <div class="row" >                    
                                    <label class="col-sm-5 control-label" >Ingreso</label>                 
                                    <div class="col-sm-6">    
                                        <?php echo fecha($fechaI); ?>  
                                    </div>          
                                </div>
                                <br>
                                <div class="row" >                    
                                    <label class="col-sm-5 control-label" >Revisión</label>             
                                    <div class="col-sm-6 ">    
                                        <?php echo fecha($fechaR); ?>  
                                    </div>                 
                                </div>
                                <br>
                                <div class="row" >                    
                                    <label class="col-sm-5 control-label" >Modificación</label>           
                                    <div class="col-sm-6 ">    
                                        <?php echo fecha($fechaM); ?>  
                                    </div>                 
                                </div>
                                <br>
                                <div class="row" >                    
                                    <label class="col-sm-5 control-label" >Finalización</label>                  
                                    <div class="col-sm-6">    
                                        <?php echo fecha($fechaF); ?>  
                                    </div>                 
                                </div>
                        </div>
                        <div class="panel panel-default col-sm-3">
                                <div class="row" >                    
                                    <label class=" col-sm-3 control-label" >Período</label>                  
                                </div>                     
                                <div class="row">                          
                                    <div class=" col-sm-2" >
                                        <?php echo $periodo; ?>
                                    </div> 
                                </div>  

                                <div class="row" >                    
                                    <label class=" col-sm-3 control-label" >Año</label>             
                                </div>                     
                                <div class="row">                                
                                    <div class=" col-sm-2" >                      
                                        <?php echo $anio; ?>  
                                    </div>                  
                                </div>
                                <br><br><br>
                        </div>
                </div> 
            </div>
        </div>
    </div>                                    
</div>
<?php 
    }else{
    echo mensajes("Aún no ha comenzado el registro de cargas de ningun periodo","azul");
    }
}else{
    echo mensajes("Problemas en la página, informe al administrador","rojo");
    } 
?>
<script type="text/javascript">

    $(document).ready(function(){   
        //---------------formulario N-----------------
            $("#formSelectDepto").submit(function(e) {
                e.preventDefault();
                data={
                            selectDeptoID:$('#selectDeptoID').val()
                        }
                $.ajax({
                    async: true,
                    type: "POST",
                    dataType: "html",
                    contentType: "application/x-www-form-urlencoded",
                    beforeSend: inicioEnvio,
                    success: cargarDepto,
                    timeout: 4000,
                    error: problemas
                }); 
                return false;
            });

        //---------------formulario N+1-----------------
    });
    function inicioEnvio(){
        $("#contenedor").text('Cargando...');
    }

    function problemas(){
        $("#contenedor").text('Problemas en el servidor.');
    }
    function cargarDepto(){
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/TodasCargas/Cargas.php",data);
    }
</script>