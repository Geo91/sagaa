<?php
    $maindir = "../../../";
    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."conexion/config.inc.php");

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    if(isset($_POST['IDseccion'])){
        $IDseccion = $_POST['IDseccion'];
        $query = "SELECT * FROM `ca_asignaturas` 
                    WHERE `id_departamento`=(SELECT ca_cargas.id_departamento
                                    FROM ca_secciones,ca_cargas
                                    WHERE ca_secciones.id_carga=ca_cargas.id_carga
                                        and ca_secciones.id_seccion=$IDseccion)
                    and  ca_asignaturas.habilitada = 1 
                    order by nombre";
        $query1 = "SELECT empleado.No_Empleado, 
                    CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombre
                FROM empleado,persona,cargo,empleado_has_cargo
                WHERE empleado.N_identidad=persona.N_identidad
                    and empleado_has_cargo.No_Empleado=empleado.No_Empleado
                    and empleado_has_cargo.ID_cargo=cargo.ID_cargo
                    and empleado.estado_empleado=1
                    and (cargo.Cargo='Docente' 
                         or cargo.Cargo='Decana'
                         or cargo.Cargo='Jefe de Departamento'
                         or cargo.Cargo='Coordinador de Maestría')
                    and empleado_has_cargo.Fecha_salida_cargo is null
                    and empleado.No_Empleado in (SELECT ca_profesores.No_Empleado FROM ca_profesores) 
                ORDER BY persona.Primer_nombre";
?>
<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span><strong> Información Actual de la Sección </strong></div>

    <div class="panel-body" >
        <div class="row">
            <div class="col-lg-12">
            <!--************************formulario*********************-->
                <form class="form-horizontal" role="form" id="formAsignaturaEditar" name="formAsignaturaEditar">

                    <input type="hidden" class="form-control" id="SeccionIdEditar" name="SeccionIdEditar"
                    <?php if (isset($_POST['IDseccion'])) {
                            $tempVal = $_POST['IDseccion'];
                            echo " value='$tempVal' ";
                        }else{
                            echo " value='-1' ";
                    }?>>

                    <input type="hidden" class="form-control" id="AulaEditar" name="AulaEditar"
                    <?php if (isset($_POST['Aula'])) {
                            $tempVal = $_POST['Aula'];
                            echo " value='$tempVal' ";
                        }else{
                            echo " value='-1' ";
                    }?>>

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Código</label>
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control" id="CodigoEditar" name="CodigoEditar" placeholder="Ejmpl: 1601"
                            <?php if (isset($_POST['Codigo'])) {
                                $tempVal = $_POST['Codigo'];
                                echo " value='$tempVal' ";
                            }?>
                             required>              
                        </div>                  
                    </div>
                    
                    <div class="row form-group">
                        <label class=" col-sm-2 control-label" >Asignatura</label>
                        <div class="col-sm-8">                            
                            <select class="form-control" id="AsignaturaEditar" name="AsignaturaEditar" required>
                                <option value="">Seleccione una opción</option>
                                <?php
                                     $result =$db->prepare($query);
                                     $result->execute();
                                    while ($fila = $result->fetch()) {
                                        ?>
                                        <option value="<?php echo $fila["id_asignatura"].'/'.$fila["nombre"]; ?>"
                                    <?php if (isset($_POST['Asignatura'])) {
                                            $tempVal = $_POST['Asignatura'];
                                            if ($tempVal==$fila["id_asignatura"]){
                                                echo " selected ";
                                            }
                                        }
                                    ?>
                             ><?php echo $fila["nombre"]; ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Cupos máximos</label>                       
                        <div class="col-sm-8">                   
                            <input type="number" id="CuposEditar" name="CuposEditar" min="1" max="150" 
                            <?php if (isset($_POST['Cupos'])) {
                                $tempVal = $_POST['Cupos'];
                                echo " value='$tempVal' ";
                            }else{
                                echo " value='1' ";
                                }?> required>
                        </div>                  
                    </div>

                    <div class="row form-group">
                        <label class=" col-sm-2 control-label" >Profesor</label>
                        <div class="col-sm-8">                            
                            <select class="form-control" id="ProfesorEditar" name="ProfesorEditar" required>
                                <option value="">Seleccione una opción</option>
                                <?php
                                     $result =$db->prepare($query1);
                                     $result->execute();
                                    while ($fila = $result->fetch()) {
                                        ?>
                                        <option value="<?php echo $fila["No_Empleado"].'/'.$fila["nombre"]; ?>"
                                    <?php if (isset($_POST['Profesor'])) {
                                            $tempVal = $_POST['Profesor'];
                                            if ($tempVal==$fila["No_Empleado"]){
                                                echo " selected ";
                                            }
                                        }
                                    ?> >
                        <?php 
                            $largo = strlen($fila["No_Empleado"]);
                            if ($largo<=7) {
                                $espacio = str_repeat ( "&nbsp", 7-$largo);
                            }else{
                                $espacio=" ";
                            }
                            echo $fila["nombre"]; ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Hora inicio</label>                       
                        <div class="col-sm-8">                   
                            <input type="time" id="HoraIEditar" name="HoraIEditar"  placeholder="Ejem: 16:00"
                            <?php if (isset($_POST['HoraI'])) {
                                $tempVal = $_POST['HoraI'];
                                echo " value='$tempVal' ";
                            }?> required>
                        </div>                  
                    </div>

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Hora fin</label>                       
                        <div class="col-sm-8">                   
                            <input type="time" id="HoraFEditar" name="HoraFEditar" placeholder="Ejem: 17:00"
                            <?php if (isset($_POST['HoraF'])) {
                                $tempVal = $_POST['HoraF'];
                                echo " value='$tempVal' ";
                            }?> required>
                        </div>                  
                    </div>
<!--*****************dias********************* -->
                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Dias </label>                       
                        <div class="col-sm-8">  
                            <div class="col-sm-4" >       
                                <label >
                                    <input id="EditarchkLunes" type="checkbox" 
                                    <?php if (isset($_POST['Lunes'])) {
                                        if ($_POST['Lunes']=="true"){
                                            echo " checked ";
                                        }
                                    }?> /> Lunes
                                </label>   
                            </div>      
                            <div class="col-sm-4" > 
                                <label>
                                    <input id="EditarchkMartes" type="checkbox"
                                    <?php if (isset($_POST['Martes'])) {
                                        if ($_POST['Martes']=="true"){
                                            echo " checked ";
                                        }
                                    }?> /> Martes
                                </label>  
                            </div>   
                            <div class="col-sm-4" > 
                                <label>
                                    <input id="EditarchkMiercoles" type="checkbox"
                                    <?php if (isset($_POST['Miercoles'])) {
                                        if ($_POST['Miercoles']=="true"){
                                            echo " checked ";
                                        }
                                    }?>/> Miércoles
                                </label>  
                            </div>   
                            <div class="col-sm-4" > 
                                <label>
                                    <input id="EditarchkJueves" type="checkbox"
                                    <?php if (isset($_POST['Jueves'])) {
                                        if ($_POST['Jueves']=="true"){
                                            echo " checked ";
                                        }
                                    }?>/> Jueves
                                </label>  
                            </div>   
                            <div class="col-sm-4" > 
                                <label>
                                    <input id="EditarchkViernes" type="checkbox"
                                    <?php if (isset($_POST['Viernes'])) {
                                        if ($_POST['Viernes']=="true"){
                                            echo " checked ";
                                        }
                                    }?>/> Viernes
                                </label>  
                            </div>   
                            <div class="col-sm-4" > 
                                <label>
                                    <input id="EditarchkSabado" type="checkbox"
                                    <?php if (isset($_POST['Sabado'])) {
                                        if ($_POST['Sabado']=="true"){
                                            echo " checked ";
                                        }
                                    }?>/> Sábado
                                </label>  
                            </div>   
                            <div class="col-sm-4" > 
                                <label>
                                    <input id="EditarchkDomingo" type="checkbox"
                                    <?php if (isset($_POST['Domingo'])) {
                                        if ($_POST['Domingo']=="true"){
                                            echo " checked ";
                                        }
                                    }?>/> Domingo
                                </label>  
                            </div>
                        </div>                  
                    </div>
<!--*****************dias********************* -->

                <div class="modal-footer">
                    <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button  id="guardarCambios" class="btn btn-primary">Siguiente <span class=" glyphicon glyphicon-arrow-right"></span></button>
                </div>  

                </form>
            <!--************************formulario*********************-->
            </div>
        </div>
    </div>                                    
</div> 
<?php
    }else{
        echo mensajes("Usted no tiene acceso a este formulario","rojo");
    }
?>
<script type="text/javascript">
$(document).ready(function(){   

//para cuando se da submit al formulario formAsignatura
    $("#formAsignaturaEditar").submit(function(e) {
        e.preventDefault();
        HoraI=$('#HoraIEditar').val();
        HoraF=$('#HoraFEditar').val();
        Lunes=$("#EditarchkLunes").prop('checked');
        Martes=$("#EditarchkMartes").prop('checked');
        Miercoles=$("#EditarchkMiercoles").prop('checked');
        Jueves=$("#EditarchkJueves").prop('checked');
        Viernes=$("#EditarchkViernes").prop('checked');
        Sabado=$("#EditarchkSabado").prop('checked');
        Domingo=$("#EditarchkDomingo").prop('checked');
        if (HoraI>=HoraF){
            alert("La hora de inicio debe de ser menor a la hora final.");
        }else if(!Lunes && !Martes && !Miercoles && !Jueves && !Viernes && !Sabado && !Domingo){
            alert("Debe seleccionar al menos un día de la semana.");
        }else{
            data={
                SeccionIdEditar:$('#SeccionIdEditar').val(),
                AulaEditar:$('#AulaEditar').val(),
                Codigo:$('#CodigoEditar').val(),
                Asignatura:$('#AsignaturaEditar').val(),
                Cupos:$('#CuposEditar').val(),
                Profesor:$('#ProfesorEditar').val(),
                HoraI:$('#HoraIEditar').val(),
                HoraF:$('#HoraFEditar').val(),
                Lunes:Lunes,
                Martes:Martes,
                Miercoles:Miercoles,
                Jueves:Jueves,
                Viernes:Viernes,
                Sabado:Sabado,
                Domingo:Domingo
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                success: agregarAsignaturaEditar,
                timeout: 100000,
                error: problemas
            }); 
            return false;
        }
    });
    function agregarAsignaturaEditar(){
        $("#subModalTemp").html('');
        $("#subModalTemp").load('pages/CargaAcademica/TodasCargas/AgregarAulaEditar.php',data);
    }
});
</script>