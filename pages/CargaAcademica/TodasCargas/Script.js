
$(document).ready(function(){   
    $("#formEditMatricula").submit(function(e) {
        e.preventDefault();
        data={
            idSeccionMatr:$('#idSeccionMatr').val(),
            nMatricula:$('#nMatricula').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: actualizarMatricula,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });
});

$(document).ready(function(){   
    $("#panelRegistro").click(function(event) {
        event.preventDefault();
        $('#flechaRegistro').toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
    });
    $("#panelCarga").click(function(event) {
        event.preventDefault();
        $('#flechaCarga').toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
    });
    $("#panelMensaje").click(function(event) {
        event.preventDefault();
        $('#flechaMensaje').toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
    });
});

$(document).on("click", ".accesibilidad", function () {
    data1={
            id:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: abrirAccesibilidad,
        timeout: 4000,
        error: problemas
    }); 

    $("#modalSubTabla").modal('show');
    return false;
});

$(document).on("click", ".Acondicionamiento", function () {
    data3={
            id:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: abrirAcondicionamiento,
        timeout: 4000,
        error: problemas
    }); 

    $("#modalSubTabla").modal('show');
    return false;
});


function inicioEnvio(){
    $("#divRespuesta").html('Cargando...');
}

function problemas(){
    $("#divRespuesta").html('Problemas en el servidor.');
}

function abrirAccesibilidad(){
    $("#subTabla").load('pages/CargaAcademica/TodasCargas/DatosAccesibilidad.php',data1);
}

function abrirAcondicionamiento(){
    $("#subTabla").load('pages/CargaAcademica/TodasCargas/DatosAcondicionamiento.php',data3);
}

function eliminarSeccion(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/TodasCargas/EliminarSeccion.php',data);
}

function editarSeccion(){
    $("#subModalTemp").load('pages/CargaAcademica/TodasCargas/AgregarSeccionEditar.php',data);
    $("#modalTemp").modal('show');
}

function enviarMensaje(){
    data={
            idDepto:$('#idDepto').val(),
            obserbacion:$('#obserbacion').val()
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: guardarObservacion,
        timeout: 4000,
        error: problemas
    }); 

    /*$("#modalSubTabla").modal('show');
    return false;*/
}

function guardarObservacion(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/TodasCargas/GuardarObservacion.php',data);
}

function regresarPrincipal(){
     data={
            IDdeptoP:$('#IDdeptoP').val()
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: recargarPrincipal,
        timeout: 4000,
        error: problemas
    }); 
    return false;
}

function recargarPrincipal(){
    $("#contenedor").load("pages/CargaAcademica/TodasCargas/principal.php",data);
}

function actualizarMatricula(){
    $("#divRespuestaEditar").load("pages/CargaAcademica/TodasCargas/ActualizarMatricula.php",data);
}