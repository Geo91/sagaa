<?php
    $maindir = "../../../";

    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['id_carga']) && 
    isset($_POST['seccionIDEditar']) &&
    isset($_POST['SeccionAulaEditar']) &&
    isset($_POST['Codigo']) && 
    isset($_POST['Asignatura']) && 
    isset($_POST['Cupos']) && 
    isset($_POST['Profesor']) && 
    isset($_POST['id_aula']) &&
    isset($_POST['HoraI']) && 
    isset($_POST['HoraF']) &&
    isset($_POST['Lunes']) &&
    isset($_POST['Martes']) &&
    isset($_POST['Miercoles']) &&
    isset($_POST['Jueves']) &&
    isset($_POST['Viernes']) &&
    isset($_POST['Sabado']) &&
    isset($_POST['Domingo']) ){
        
        $Disponible=true;
        $id_carga = $_POST['id_carga'];
        $seccionIDEditar = $_POST['seccionIDEditar'];
        $SeccionAulaEditar = $_POST['SeccionAulaEditar'];
        $Codigo = $_POST['Codigo'];
        $Asignatura = $_POST['Asignatura'];
        $Cupos = $_POST['Cupos'];
        $Profesor = $_POST['Profesor'];
        $id_aula = $_POST['id_aula'];
        $HoraI = $_POST['HoraI'];
        $HoraF = $_POST['HoraF'];
        $Lunes = $_POST['Lunes'];
        $Martes = $_POST['Martes'];
        $Miercoles = $_POST['Miercoles'];
        $Jueves = $_POST['Jueves'];
        $Viernes = $_POST['Viernes'];
        $Sabado = $_POST['Sabado'];
        $Domingo = $_POST['Domingo'];
            
        try{

            $query = "SELECT ca_secciones.No_Empleado, ca_secciones.Hora_inicio,ca_secciones.Hora_fin,ca_dias.nombre
                FROM ca_secciones,ca_cargas,ca_secciones_dias,ca_dias
                WHERE ca_secciones.id_seccion=ca_secciones_dias.id_seccion
                    and ca_secciones_dias.id_dia=ca_dias.id_dia
                    and ca_secciones.id_carga=ca_cargas.id_carga
                    and ca_cargas.id_periodo=(SELECT ca_periodos.id_periodo
                                   from ca_secciones,ca_cargas,ca_periodos
                                   WHERE ca_secciones.id_carga=ca_cargas.id_carga
                                    and ca_cargas.id_periodo=ca_periodos.id_periodo
                                   and ca_secciones.id_seccion=$seccionIDEditar)
                    and ca_secciones.No_Empleado='$Profesor'
                    and ca_secciones.id_seccion<>$seccionIDEditar
                    and ('$HoraI' BETWEEN ca_secciones.Hora_inicio 
                                        and ca_secciones.Hora_fin
                        or '$HoraF' BETWEEN ca_secciones.Hora_inicio 
                                        and ca_secciones.Hora_fin
                        or ca_secciones.Hora_inicio BETWEEN '$HoraI'
                                        and '$HoraF')
                    and (";
            $logico="";
            if($Lunes=="true"){
                $query=$query.$logico." ca_dias.nombre='Lunes' ";
                $logico=" or ";
            }
            if($Martes=="true"){
                $query=$query.$logico." ca_dias.nombre='Martes' ";
                $logico=" or ";
            }
            if($Miercoles=="true"){
                $query=$query.$logico." ca_dias.nombre='Miércoles' ";
                $logico=" or ";
            }
            if($Jueves=="true"){
                $query=$query.$logico." ca_dias.nombre='Jueves' ";
                $logico=" or ";
            }
            if($Viernes=="true"){
                $query=$query.$logico." ca_dias.nombre='Viernes' ";
                $logico=" or ";
            }
            if($Sabado=="true"){
                $query=$query.$logico." ca_dias.nombre='Sábado' ";
                $logico=" or ";
            }
            if($Domingo=="true"){
                $query=$query.$logico." ca_dias.nombre='Domingo' ";
                $logico=" or ";
            }
            $query = $query." )";

            $result =$db->prepare($query);
            $result->execute();
            while ($fila = $result->fetch()) {
                $Disponible=false;
                break;
            }
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
        if ($Disponible) {
            $AulaTomada = false;
            try{
                $queryAula = "SELECT ca_aulas.id_aula
                    from ca_secciones,ca_cargas,ca_periodos,ca_secciones_dias,ca_dias,ca_aulas
                    where ca_secciones.id_seccion=ca_secciones_dias.id_seccion
                        and ca_secciones_dias.id_dia=ca_dias.id_dia
                        and ca_secciones.id_aula=ca_aulas.id_aula
                        and ca_secciones.id_carga=ca_cargas.id_carga 
                        and ca_cargas.id_periodo = ca_periodos.id_periodo
                        and ca_aulas.id_aula=$id_aula
                        and ca_secciones.id_seccion<>$seccionIDEditar
                        and ca_periodos.id_periodo=(select ca_cargas.id_periodo from ca_cargas where ca_cargas.id_carga=$id_carga)
                        and (('$HoraI' >= ca_secciones.Hora_inicio 
                                    and '$HoraI' < ca_secciones.Hora_fin)
                                or ('$HoraF' > ca_secciones.Hora_inicio 
                                    and '$HoraF' <= ca_secciones.Hora_fin)
                                or (ca_secciones.Hora_inicio > '$HoraI'
                                    and ca_secciones.Hora_inicio < '$HoraF'))
                        and (";
                                    $logico="";
                                    if($Lunes=="true"){
                                        $queryAula=$queryAula.$logico." ca_dias.nombre='Lunes' ";
                                        $logico=" or ";
                                    }
                                    if($Martes=="true"){
                                        $queryAula=$queryAula.$logico." ca_dias.nombre='Martes' ";
                                        $logico=" or ";
                                    }
                                    if($Miercoles=="true"){
                                        $queryAula=$queryAula.$logico." ca_dias.nombre='Miércoles' ";
                                        $logico=" or ";
                                    }
                                    if($Jueves=="true"){
                                        $queryAula=$queryAula.$logico." ca_dias.nombre='Jueves' ";
                                        $logico=" or ";
                                    }
                                    if($Viernes=="true"){
                                        $queryAula=$queryAula.$logico." ca_dias.nombre='Viernes' ";
                                        $logico=" or ";
                                    }
                                    if($Sabado=="true"){
                                        $queryAula=$queryAula.$logico." ca_dias.nombre='Sábado' ";
                                        $logico=" or ";
                                    }
                                    if($Domingo=="true"){
                                        $queryAula=$queryAula.$logico." ca_dias.nombre='Domingo' ";
                                        $logico=" or ";
                                    }
                                    $queryAula = $queryAula.")";

                $result =$db->prepare($queryAula);
                $result->execute();
                while ($fila = $result->fetch()) {
                    $AulaTomada=true;
                    break;
                }
            } catch (Exception $e) {
                echo mensajes($e->getMessage(),"rojo");
            }

            if ($AulaTomada){
                echo mensajes("El aula seleccionada ha sido tomada por alguien más","azul");
            }else{
            // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
                $query0 = "SELECT * FROM `ca_secciones` 
                        WHERE `codigo_seccion`='$Codigo' 
                        and `id_carga`=$id_carga 
                        and `id_asignatura`='$Asignatura'
                        and `id_seccion`<>$seccionIDEditar";
                $query = "INSERT INTO `ca_secciones` (`id_seccion`, `codigo_seccion`, `id_carga`, `id_asignatura`, `cupos_max`, `matriculados`, `No_Empleado`, `id_aula`, `Hora_inicio`, `Hora_fin`) VALUES (NULL, '$Codigo', '$id_carga', '$Asignatura', '$Cupos','0', '$Profesor', '$id_aula', '$HoraI', '$HoraF');";
                $query1 = "SELECT * FROM ca_carga_profesores
                            WHERE ca_carga_profesores.No_Empleado=$Profesor
                                and ca_carga_profesores.id_periodo=(SELECT id_periodo FROM ca_cargas WHERE ca_cargas.id_carga=$id_carga)";

                try{
                    $result =$db->prepare($query0);
                    $result->execute();
                    
                    $bandera = false;
                    while ($fila = $result->fetch()) {
                        $bandera = true;
                        break;
                    }
                    if($bandera){
                        $mensaje = "La sección con codigo `$Codigo` ya existe";
                        $color = "azul";
                    }
                    else{

                        $query3="SELECT (case when (ca_carga_profesores.cantidad_uv-ca_asignaturas.uv)<0 then 0 else (ca_carga_profesores.cantidad_uv-ca_asignaturas.uv)  end )as NuevaUv
                                                   FROM ca_secciones,ca_asignaturas,ca_carga_profesores,ca_cargas
                                                   WHERE ca_secciones.id_asignatura=ca_asignaturas.id_asignatura
                                                       and ca_secciones.id_carga=ca_cargas.id_carga
                                                       and ca_secciones.No_Empleado=ca_carga_profesores.No_Empleado
                                                       and ca_cargas.id_periodo=ca_carga_profesores.id_periodo
                                                       and ca_secciones.id_seccion=$seccionIDEditar";

                        $result =$db->prepare($query3);
                        $result->execute();
                        $CantUv=-1;
                        while ($fila = $result->fetch()) {
                            $CantUv=$fila['NuevaUv'];
                            break;
                        }
                        if ($CantUv>-1) {
                            $query3="UPDATE ca_carga_profesores
                                    set ca_carga_profesores.cantidad_uv = $CantUv,
                                        ca_carga_profesores.completa=0
                                    WHERE ca_carga_profesores.No_Empleado=(SELECT 
                                                            ca_secciones.No_Empleado
                                                      FROM ca_secciones
                                                      WHERE ca_secciones.id_seccion=$seccionIDEditar)
                                        and ca_carga_profesores.id_periodo=(SELECT 
                                                            ca_periodos.id_periodo
                                                       from ca_secciones,ca_cargas,ca_periodos
                                                       WHERE ca_secciones.id_carga=ca_cargas.id_carga
                                                        and ca_cargas.id_periodo=ca_periodos.id_periodo
                                                        and ca_secciones.id_seccion=$seccionIDEditar)";
                            $result =$db->prepare($query3);
                            $result->execute();
                        }
                        $query3 = "DELETE FROM ca_secciones WHERE ca_secciones.id_seccion=$seccionIDEditar;
                                DELETE FROM ca_secciones_dias WHERE ca_secciones_dias.id_seccion=$seccionIDEditar;";

                        $result =$db->prepare($query3);
                        $result->execute();

                        $result =$db->prepare($query);
                        $result->execute();

                        $result =$db->prepare($query1);
                        $result->execute();
                        $cantidad_uv = -1;
                        while ($fila = $result->fetch()) {
                            $cantidad_uv = $fila['cantidad_uv'];
                            break;
                        }
            // -----------------------dias--------------------------
                        if($Lunes=="true"){
                            $queryDia = "INSERT INTO `ca_secciones_dias` (`id_seccion`, `id_dia`) 
                                VALUES ((SELECT id_seccion FROM `ca_secciones` WHERE `id_asignatura`='$Asignatura' and `id_carga`=$id_carga and `codigo_seccion`='$Codigo'), 
                                        (SELECT id_dia FROM ca_dias  WHERE nombre='Lunes'));";
                            $result =$db->prepare($queryDia);
                            $result->execute();
                        }
                        if($Martes=="true"){
                            $queryDia = "INSERT INTO `ca_secciones_dias` (`id_seccion`, `id_dia`) 
                                VALUES ((SELECT id_seccion FROM `ca_secciones` WHERE `id_asignatura`='$Asignatura' and `id_carga`=$id_carga and `codigo_seccion`='$Codigo'), 
                                        (SELECT id_dia FROM ca_dias  WHERE nombre='Martes'));";
                            $result =$db->prepare($queryDia);
                            $result->execute();
                        }
                        if($Miercoles=="true"){
                            $queryDia = "INSERT INTO `ca_secciones_dias` (`id_seccion`, `id_dia`) 
                                VALUES ((SELECT id_seccion FROM `ca_secciones` WHERE `id_asignatura`='$Asignatura' and `id_carga`=$id_carga and `codigo_seccion`='$Codigo'), 
                                        (SELECT id_dia FROM ca_dias  WHERE nombre='Miércoles'));";
                            $result =$db->prepare($queryDia);
                            $result->execute();
                        }
                        if($Jueves=="true"){
                            $queryDia = "INSERT INTO `ca_secciones_dias` (`id_seccion`, `id_dia`) 
                                VALUES ((SELECT id_seccion FROM `ca_secciones` WHERE `id_asignatura`='$Asignatura' and `id_carga`=$id_carga and `codigo_seccion`='$Codigo'), 
                                        (SELECT id_dia FROM ca_dias  WHERE nombre='Jueves'));";
                            $result =$db->prepare($queryDia);
                            $result->execute();
                        }
                        if($Viernes=="true"){
                            $queryDia = "INSERT INTO `ca_secciones_dias` (`id_seccion`, `id_dia`) 
                                VALUES ((SELECT id_seccion FROM `ca_secciones` WHERE `id_asignatura`='$Asignatura' and `id_carga`=$id_carga and `codigo_seccion`='$Codigo'), 
                                        (SELECT id_dia FROM ca_dias  WHERE nombre='Viernes'));";
                            $result =$db->prepare($queryDia);
                            $result->execute();
                        }
                        if($Sabado=="true"){
                            $queryDia = "INSERT INTO `ca_secciones_dias` (`id_seccion`, `id_dia`) 
                                VALUES ((SELECT id_seccion FROM `ca_secciones` WHERE `id_asignatura`='$Asignatura' and `id_carga`=$id_carga and `codigo_seccion`='$Codigo'), 
                                        (SELECT id_dia FROM ca_dias  WHERE nombre='Sábado'));";
                            $result =$db->prepare($queryDia);
                            $result->execute();
                        }
                        if($Domingo=="true"){
                            $queryDia = "INSERT INTO `ca_secciones_dias` (`id_seccion`, `id_dia`) 
                                VALUES ((SELECT id_seccion FROM `ca_secciones` WHERE `id_asignatura`='$Asignatura' and `id_carga`=$id_carga and `codigo_seccion`='$Codigo'), 
                                        (SELECT id_dia FROM ca_dias  WHERE nombre='Domingo'));";
                            $result =$db->prepare($queryDia);
                            $result->execute();
                        }
            // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
                        if($cantidad_uv==-1){
                            $query2 = "INSERT INTO `ca_carga_profesores` (`No_Empleado`, `cantidad_uv`, `completa`, `id_periodo`) VALUES ('$Profesor', (SELECT ca_asignaturas.uv FROM ca_asignaturas WHERE ca_asignaturas.id_asignatura='$Asignatura'), '0', (SELECT id_periodo FROM ca_cargas WHERE ca_cargas.id_carga=$id_carga) );";
                        }else{
                            $query2 = "UPDATE ca_carga_profesores set ca_carga_profesores.cantidad_uv = (SELECT ca_asignaturas.uv+$cantidad_uv FROM ca_asignaturas WHERE ca_asignaturas.id_asignatura='$Asignatura') where ca_carga_profesores.No_Empleado=$Profesor                            and ca_carga_profesores.id_periodo in (SELECT id_periodo FROM ca_cargas WHERE ca_cargas.id_carga=$id_carga)";
                        }
                        $result =$db->prepare($query2);
                        $result->execute();

                        $mensaje = "Sección `$Codigo` editada";
                        $color = "verde";
                        ?>
                        <script type="text/javascript">
                            $("#TablaCarga").load('pages/CargaAcademica/TodasCargas/DatosCarga.php');
                        </script>
                        <?php
                    }
                    echo mensajes($mensaje,$color);
                } catch (Exception $e) {
                    echo mensajes($e->getMessage(),"rojo");
                }
            }
        }else{
            echo mensajes("El empleado No.$Profesor tiene una o más secciones que interfieren con el horario y días seleccionados ","azul");
        }
    }else{
        echo mensajes("Error de conexión intentelo de nuevo:","rojo");
    }
?>
<script type="text/javascript">
    $("#modalTemp").modal('hide');
</script>