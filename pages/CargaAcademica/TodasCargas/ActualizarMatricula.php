<?php
    $maindir = "../../../";

    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['idSeccionMatr']) && 
    isset($_POST['nMatricula']) ){
        
        $idSeccionMatr = $_POST['idSeccionMatr'];
        $nMatricula = $_POST['nMatricula'];
        
        try{
            $query="UPDATE ca_secciones
                    SET ca_secciones.matriculados=$nMatricula
                    WHERE ca_secciones.id_seccion=$idSeccionMatr";

            $result =$db->prepare($query);
            $result->execute();

            echo mensajes("se ha actualizado la matricula de la sección","verde");
            ?>
            <script type="text/javascript">
                $("#modalEditMatricula").modal('hide');
                $("#TablaCarga").load('pages/CargaAcademica/TodasCargas/DatosCarga.php');
            </script>
            <?php

        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
          
    }else{
        echo mensajes("Error de conexión intentelo de nuevo:","rojo");
    }
?>