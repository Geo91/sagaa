<?php
    $maindir = "../../../";

    require_once("../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Codigo']) &&
    isset($_POST['SeccionIdEditar']) &&
    isset($_POST['AulaEditar']) &&
    isset($_POST['Asignatura']) &&
    isset($_POST['Cupos']) &&
    isset($_POST['Profesor']) &&
    isset($_POST['HoraI']) &&
    isset($_POST['HoraF']) &&
    isset($_POST['Lunes']) &&
    isset($_POST['Martes']) &&
    isset($_POST['Miercoles']) &&
    isset($_POST['Jueves']) &&
    isset($_POST['Viernes']) &&
    isset($_POST['Sabado']) &&
    isset($_POST['Domingo']) ){

        $Codigo = $_POST['Codigo'];
        $SeccionIdEditar = $_POST['SeccionIdEditar'];
        $AulaEditar = $_POST['AulaEditar'];
        $Asignatura = explode("/", $_POST['Asignatura']);
        $Cupos = $_POST['Cupos'];
        $Profesor = explode("/", $_POST['Profesor']);
        $HoraI = $_POST['HoraI'];
        $HoraF = $_POST['HoraF'];
        $Lunes = $_POST['Lunes'];
        $Martes = $_POST['Martes'];
        $Miercoles = $_POST['Miercoles'];
        $Jueves = $_POST['Jueves'];
        $Viernes = $_POST['Viernes'];
        $Sabado = $_POST['Sabado'];
        $Domingo = $_POST['Domingo'];
    }
        
?>

<div class="panel panel-info">
    <div class="panel-heading"><button id="RegresarFormularioEditar" class="btn btn-primary"><span class="glyphicon glyphicon-arrow-left"></span> Regresar</button><strong> Aulas disponibles para el horario y cupos seleccionados </strong></div>

    <div class="panel-body" >
        <div class="row">
            <div class="col-lg-12">

<!--************************Datos*********************-->
                <input id="SeccionCodigoEditar" name="SeccionCodigoEditar" type="hidden" value="<?php echo $Codigo; ?>" ></input>

                <input id="seccionIDEditar" name="seccionIDEditar" type="hidden" value="<?php echo $SeccionIdEditar; ?>" ></input>

                <input id="SeccionAulaEditar" name="SeccionAulaEditar" type="hidden" value="<?php echo $AulaEditar; ?>" ></input>
        
                <input id="SeccionAsignaturaEditar" name="SeccionAsignaturaEditar" type="hidden" value="<?php echo $Asignatura[0]; ?>" ></input>
                        
                <input id="SeccionCuposEditar" name="SeccionCuposEditar" type="hidden" value="<?php echo $Cupos; ?>" ></input>

                <input id="SeccionProfesorEditar" name="SeccionProfesorEditar" type="hidden" value="<?php echo $Profesor[0]; ?>" ></input>

                <input id="SeccionHoraIEditar" name="SeccionHoraIEditar" type="hidden" value="<?php echo $HoraI; ?>" ></input>

                <input id="SeccionHoraFEditar" name="SeccionHoraFEditar" type="hidden" value="<?php echo $HoraF; ?>" ></input>

<!--**************************dias**********************-->
                <input id="LunesEditar" name="Lunes" type="hidden" value="<?php echo $Lunes; ?>" ></input>
                <input id="MartesEditar" name="Martes" type="hidden" value="<?php echo $Martes; ?>" ></input>
                <input id="MiercolesEditar" name="Miercoles" type="hidden" value="<?php echo $Miercoles; ?>" ></input>
                <input id="JuevesEditar" name="Jueves" type="hidden" value="<?php echo $Jueves; ?>" ></input>
                <input id="ViernesEditar" name="Viernes" type="hidden" value="<?php echo $Viernes; ?>" ></input>
                <input id="SabadoEditar" name="Sabado" type="hidden" value="<?php echo $Sabado; ?>" ></input>
                <input id="DomingoEditar" name="Domingo" type="hidden" value="<?php echo $Domingo; ?>" ></input>
<!--**************************dias**********************-->
<!--************************Datos*********************--> 
                <div id="tablaAulasDisponibles">
                    <?php include("DatosAulasEditar.php"); ?>
                </div>

                <div class="modal-footer">
                    <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>  
            </div>
        </div>
    </div>                                    
</div>
<script type="text/javascript">
    
$(document).ready(function(){   
    $("#RegresarFormularioEditar").click(function(event) {
        event.preventDefault();
        data={
                IDseccion:$('#seccionIDEditar').val(),
                Aula:$('#SeccionAulaEditar').val(),
                Codigo:$('#SeccionCodigoEditar').val(),
                SeccionIdEditar:$('#seccionIDEditar').val(),
                AulaEditar:$('#SeccionAulaEditar').val(),
                Asignatura:$('#SeccionAsignaturaEditar').val(),
                Cupos:$('#SeccionCuposEditar').val(),
                Profesor:$('#SeccionProfesorEditar').val(),
                HoraI:$('#SeccionHoraIEditar').val(),
                HoraF:$('#SeccionHoraFEditar').val(),
                Lunes:$('#LunesEditar').val(),
                Martes:$('#MartesEditar').val(),
                Miercoles:$('#MiercolesEditar').val(),
                Jueves:$('#JuevesEditar').val(),
                Viernes:$('#ViernesEditar').val(),
                Sabado:$('#SabadoEditar').val(),
                Domingo:$('#DomingoEditar').val()
            }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: regresarInfoSeccionEditar,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });
    function regresarInfoSeccionEditar(){
        $("#subModalTemp").load('pages/CargaAcademica/TodasCargas/AgregarSeccionEditar.php',data);
    }
});
</script>
