<?php
    $maindir = "../../../";

    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['tempUltPID']) && 
    isset($_POST['idDepto']) ){
        
        $tempUltPID = $_POST['tempUltPID'];
        $idDepto = $_POST['idDepto'];
        
        try{
            $query = "INSERT INTO `ca_modificaciones_cargas` (`id_carga`, `fechaMod`) VALUES ((SELECT ca_cargas.id_carga FROM ca_cargas
WHERE ca_cargas.id_periodo=$tempUltPID 
    and ca_cargas.id_departamento=$idDepto), DATE_ADD(CURDATE(), INTERVAL 1 DAY));";
            $result =$db->prepare($query);
            $result->execute();
            ?>
            <script type="text/javascript">
                $("#TablaCarga").load('pages/CargaAcademica/TodasCargas/DatosCarga.php');
            </script>
            <?php

        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
          
    }else{
        echo mensajes("Problemas de conexión intentelo de nuevo:","azul");
    }
?>