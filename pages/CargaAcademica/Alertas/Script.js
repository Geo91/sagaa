$(document).on("click", ".btnSecciones", function () {
    data={
            No_Empleado:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: verSecciones,
        timeout: 8000,
        error: problemas
    }); 

    $("#InfoAdicional").modal('show');
    return false;
});

$(document).on("click", ".btnProyectos", function () {
    data={
            No_Empleado:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: verProyectos,
        timeout: 8000,
        error: problemas
    }); 

    $("#InfoAdicional").modal('show');
    return false;
});

$(document).on("click", ".btnActividades", function () {
    data={
            No_Empleado:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: verActividades,
        timeout: 8000,
        error: problemas
    }); 

    $("#InfoAdicional").modal('show');
    return false;
});

$(document).ready(function(){   
    $("#btnActualizarAlerta").click(function(event) {
        event.preventDefault();
        $("#tablaProfesores").load('pages/CargaAcademica/Alertas/RecargarTabla.php');
    });
});

function problemas(){
    $("#divRespuesta").html('Problemas en el servidor.');
}

function verSecciones(){
    $("#subTabla").load('pages/CargaAcademica/Alertas/DatosSecciones.php',data);
}

function verProyectos(){
    $("#subTabla").load('pages/CargaAcademica/Alertas/DatosProyectos.php',data);
}

function verActividades(){
    $("#subTabla").load('pages/CargaAcademica/Alertas/DatosActividades.php',data);
}

function actulizarCargaProfesor(){
    $("#divRespuesta").load('pages/CargaAcademica/Alertas/ActualizarCargaProfesor.php',data);
}
