<?php
  $maindir = "../../../";

  require_once("../clases.php");
  require_once($maindir."funciones/check_session.php");
  require_once($maindir."conexion/config.inc.php");
  require_once($maindir."Datos/funciones.php");

  // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
  if(isset($_POST['No_Empleado']) && isset($_POST['periodo'])){
    try{
      $nEmpleado = $_POST['No_Empleado'];
      $periodo = $_POST['periodo'];
      $query = "UPDATE ca_carga_profesores SET ca_carga_profesores.completa=1 WHERE ca_carga_profesores.No_Empleado = '$nEmpleado' and ca_carga_profesores.id_periodo = $periodo";
      $result =$db->prepare($query);
      $result->execute();
      echo mensajes("Empleado No.$nEmpleado actualizado correctamente","verde");
    }catch (Exception $e) {
        echo mensajes($e->getMessage(),"rojo");
    }
  }else{
    echo mensajes("Error desconocido","rojo");
  }
?>
<script type="text/javascript">
   $("#tablaProfesores").load('pages/CargaAcademica/Alertas/RecargarTabla.php');
</script>