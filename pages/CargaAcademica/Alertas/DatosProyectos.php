<?php
    $maindir = "../../../";
    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."Datos/funciones.php");

    if (isset($_SESSION['ProfesoresAlertas'])) {
        if(isset($_POST['No_Empleado'])){
?>
<div class="box-body table-responsive no-padding">
    <table id = "tablaProyectosProfesores" class="table table-hover">
        <thead>
              <tr>
                <th>Nombre del proyecto</th>
                <th>Area de proyecto</th>
                <th>Descripción</th>
                <th>Fecha de inicio</th>
                <th>Fecha de finalización</th>
              </tr>
        </thead>
        <tbody>
          <?php
            $proyectosProfesor=$_SESSION['ProfesoresAlertas']['profesores'][$_POST['No_Empleado']]->proyectos;
            foreach ($proyectosProfesor as $idProyecto => $proyectoTemp) {
          ?>
              <tr data-id='<?php echo $idProyecto; ?>'>
                <td><?php echo $proyectoTemp->getNombreProyecto(); ?></td>
                <td><?php echo $proyectoTemp->getTipoProyecto(); ?></td>
                <td><?php echo $proyectoTemp->getDescripcion(); ?></td>
                <td><?php echo fecha($proyectoTemp->getFecha_inicio()); ?></td>
                <td><?php echo fecha($proyectoTemp->getFecha_fin()); ?></td>
              </tr>
          <?php } ?>
        </tbody>
    </table>
</div><!-- /.box-body -->

<?php
        }
    }
?>