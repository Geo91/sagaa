<?php
    $maindir = "../../../";

    require_once("../clases.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    if(isset($_POST['No_Empleado']) && isset($_POST['periodo']) && isset($_POST['uvs']) && isset($_POST['tckt'])){
        $emp = $_POST['No_Empleado'];
        $cuvs = $_POST['uvs'];
        $ticket = $_POST['tckt'];

        $query = "SELECT ca_jefes_departamentos.id_Usuario FROM ca_jefes_departamentos WHERE ca_jefes_departamentos.id_Usuario IN (SELECT usuario.id_Usuario FROM usuario WHERE usuario.No_Empleado = '$emp');";
        $resultado = $db->prepare($query);
        $resultado -> execute();
        $conteo = $resultado->rowCount();
        if($conteo > 0){
            $ticket = true;
        }

        if($cuvs > 0){
            if($ticket){
                try{
                    $nEmpleado = $_POST['No_Empleado'];
                    $periodo = $_POST['periodo'];
                    $query = "UPDATE ca_carga_profesores SET ca_carga_profesores.completa=1 WHERE ca_carga_profesores.No_Empleado = '$nEmpleado' and ca_carga_profesores.id_periodo = $periodo";
                    $result =$db->prepare($query);
                    $result->execute();
                    echo mensajes("La carga del empleado No.$nEmpleado ha sido completada","verde");
                }catch (Exception $e) {
                    echo mensajes($e->getMessage(),"rojo");
                }
            }else{
                echo mensajes("El empleado no tiene actividades asignadas, su carga no puede completarse.","rojo");
            }
        }else{
            echo mensajes("El empleado no tiene secciones asignadas, su carga no puede completarse.","rojo");
        }
        
    }else{
        echo mensajes("Hubo un problema al tratar de completar la carga del empleado seleccionado, por favor intentelo de nuevo o contacte al administrador del sistema.","rojo");
    }
?>

<script type="text/javascript">
    $("#tablaProfesores").load('pages/CargaAcademica/Alertas/RecargarTabla.php');
</script>