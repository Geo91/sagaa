<?php
    $maindir = "../../../";

    require_once("../clases.php");
      require_once($maindir."funciones/check_session.php");
      include($maindir."funciones/timeout.php");
      require_once($maindir."conexion/config.inc.php");

    if (isset($_SESSION['auto_increment'])){
        $_SESSION['auto_increment'] = $_SESSION['auto_increment']+1;
        $auto_increment =$_SESSION['auto_increment'];
    }else{
        $_SESSION['auto_increment'] = 0;
        $auto_increment =0;
    }

    $idU = $_SESSION['user_id'];
    $rol = $_SESSION['user_rol'];
    $ultimoP = $_SESSION['ultimoPeriodo'];
    $ultimoPid = $ultimoP->getId();
  
  // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    if($rol == 30){
        $query = "SELECT ca_profesores.No_Empleado, persona.N_identidad, CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'nombre', departamento_laboral.nombre_departamento, (CASE WHEN historia_uv.uv >0 THEN CONCAT(historia_uv.nombre,'/',historia_uv.uv,'uv') WHEN historia_uv.uv = 0 THEN CONCAT(historia_uv.nombre) WHEN ca_tipos_contratos.uv > 0 THEN CONCAT(ca_tipos_contratos.nombre,'/',ca_tipos_contratos.uv,'uv') WHEN ca_tipos_contratos.uv = 0 THEN CONCAT(ca_tipos_contratos.nombre) ELSE CONCAT(ca_tipos_contratos.nombre,'/',ca_profesores_uv_variable.uv,'uv') END) as 'contrato', ca_periodos.id_periodo, ca_carga_profesores.cantidad_uv, (CASE ca_carga_profesores.completa WHEN 0 THEN 'INCOMPLETA' ELSE 'COMPLETA' END) as 'completa', IFNULL(secciones.id_seccion,'vacio') AS 'id_seccion', secciones.codigo_seccion, secciones.id_asignatura, ca_asignaturas.nombre as 'asignatura', ca_asignaturas.uv, IFNULL(proyectoP.id_proyecto,'vacio') AS 'id_proyecto', proyectoP.nombre as 'proyecto', ca_tipos_proyectos.nombre as 'tipoProyecto', proyectoP.descripcion, proyectoP.fecha_inicio, proyectoP.fecha_fin, IFNULL(actividades.id_actividad,'vacio') as 'id_actividad', actividades.descripcion AS 'Actividad', actividades.tipo  from ca_profesores LEFT JOIN ca_tipos_contratos on ca_profesores.id_contrato=ca_tipos_contratos.id_contrato  left join ca_carga_profesores on ca_profesores.No_Empleado=ca_carga_profesores.No_Empleado LEFT JOIN ca_profesores_uv_variable on ca_profesores.No_Empleado=ca_profesores_uv_variable.No_Empleado
        LEFT JOIN (SELECT * FROM ca_actividad INNER JOIN ca_actividad_empleado on ca_actividad.id_actividad=ca_actividad_empleado.actividad_id WHERE (ca_actividad.periodo_id=$ultimoPid and ca_actividad.tipo = 'Trimestral') OR (ca_actividad.tipo='Permanente')) as actividades on ca_profesores.No_Empleado=actividades.empleado_no
        LEFT JOIN 
        (SELECT ca_profesores_contratos_historico.No_Empleado, ca_profesores_contratos_historico.uv, ca_profesores_contratos_historico.nombre FROM ca_profesores_contratos_historico WHERE (SELECT ca_periodos.fecha_ingreso_f from ca_periodos  where ca_periodos.id_periodo=$ultimoPid) BETWEEN ca_profesores_contratos_historico.fecha_inicio and ca_profesores_contratos_historico.fecha_fin ORDER by ca_profesores_contratos_historico.fecha_inicio DESC) as historia_uv on ca_profesores.No_Empleado = historia_uv.No_Empleado LEFT JOIN ca_periodos on ca_carga_profesores.id_periodo=ca_periodos.id_periodo  left JOIN (SELECT ca_secciones.*,ca_cargas.id_periodo as 'periodoId' FROM ca_secciones,ca_cargas
        WHERE ca_secciones.id_carga = ca_cargas.id_carga) as secciones on ca_profesores.No_Empleado=secciones.No_Empleado and ca_periodos.id_periodo = secciones.periodoId
        LEFT join ca_profesores_proyectos on ca_profesores.No_Empleado=ca_profesores_proyectos.No_Empleado
        LEFT join (SELECT * FROM ca_proyectos where curdate() BETWEEN ca_proyectos.fecha_inicio and ca_proyectos.fecha_fin) as proyectoP on ca_profesores_proyectos.id_proyecto=proyectoP.id_proyecto
        LEFT join ca_tipos_proyectos on proyectoP.id_tipo_proyecto=ca_tipos_proyectos.id_tipo_proyecto
        LEFT JOIN (empleado INNER JOIN departamento_laboral on empleado.Id_departamento = departamento_laboral.Id_departamento_laboral) on ca_profesores.No_Empleado = empleado.No_Empleado
        LEFT JOIN persona on empleado.N_identidad = persona.N_identidad
        LEFT JOIN ca_asignaturas on secciones.id_asignatura=ca_asignaturas.id_asignatura
        WHERE ca_periodos.id_periodo = ( SELECT ca_periodos.id_periodo from ca_periodos where ca_periodos.fecha_ingreso_i<= curdate() ORDER by ca_periodos.fecha_ingreso_i DESC LIMIT 1)
        and (ca_carga_profesores.completa = 0 or ca_carga_profesores.completa = 1) and empleado.Id_departamento = (SELECT empleado.Id_departamento from empleado where empleado.No_Empleado = (SELECT usuario.No_Empleado from usuario where usuario.id_Usuario = $idU))
        ORDER BY ca_carga_profesores.cantidad_uv DESC";
    }else{
        $query = "SELECT ca_profesores.No_Empleado, persona.N_identidad, CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'nombre', departamento_laboral.nombre_departamento,  (CASE WHEN historia_uv.uv >0 THEN CONCAT(historia_uv.nombre,'/',historia_uv.uv,'uv') WHEN historia_uv.uv = 0 THEN CONCAT(historia_uv.nombre) WHEN ca_tipos_contratos.uv > 0 THEN CONCAT(ca_tipos_contratos.nombre,'/',ca_tipos_contratos.uv,'uv') WHEN ca_tipos_contratos.uv = 0 THEN CONCAT(ca_tipos_contratos.nombre) ELSE CONCAT(ca_tipos_contratos.nombre,'/',ca_profesores_uv_variable.uv,'uv') END) as 'contrato', ca_periodos.id_periodo, ca_carga_profesores.cantidad_uv, (CASE ca_carga_profesores.completa WHEN 0 THEN 'INCOMPLETA' ELSE 'COMPLETA' END) as 'completa', IFNULL(secciones.id_seccion,'vacio') AS 'id_seccion', secciones.codigo_seccion, secciones.id_asignatura, ca_asignaturas.nombre as 'asignatura', ca_asignaturas.uv, IFNULL(proyectoP.id_proyecto,'vacio') AS 'id_proyecto', proyectoP.nombre as 'proyecto', ca_tipos_proyectos.nombre as 'tipoProyecto', proyectoP.descripcion, proyectoP.fecha_inicio, proyectoP.fecha_fin, IFNULL(actividades.id_actividad,'vacio') as 'id_actividad', actividades.descripcion AS 'Actividad', actividades.tipo from ca_profesores LEFT JOIN ca_tipos_contratos on ca_profesores.id_contrato=ca_tipos_contratos.id_contrato  left join ca_carga_profesores on ca_profesores.No_Empleado=ca_carga_profesores.No_Empleado LEFT JOIN ca_profesores_uv_variable on ca_profesores.No_Empleado=ca_profesores_uv_variable.No_Empleado
        LEFT JOIN (SELECT * FROM ca_actividad INNER JOIN ca_actividad_empleado on ca_actividad.id_actividad=ca_actividad_empleado.actividad_id WHERE (ca_actividad.periodo_id=$ultimoPid and ca_actividad.tipo = 'Trimestral') OR (ca_actividad.tipo='Permanente')) as actividades on ca_profesores.No_Empleado=actividades.empleado_no
        LEFT JOIN 
        (SELECT ca_profesores_contratos_historico.No_Empleado, ca_profesores_contratos_historico.uv, ca_profesores_contratos_historico.nombre FROM ca_profesores_contratos_historico WHERE (SELECT ca_periodos.fecha_ingreso_f from ca_periodos  where ca_periodos.id_periodo=$ultimoPid) BETWEEN ca_profesores_contratos_historico.fecha_inicio and ca_profesores_contratos_historico.fecha_fin ORDER by ca_profesores_contratos_historico.fecha_inicio DESC) as historia_uv on ca_profesores.No_Empleado = historia_uv.No_Empleado LEFT JOIN ca_periodos on ca_carga_profesores.id_periodo=ca_periodos.id_periodo  left JOIN (SELECT ca_secciones.*,ca_cargas.id_periodo as 'periodoId' FROM ca_secciones,ca_cargas
        WHERE ca_secciones.id_carga = ca_cargas.id_carga) as secciones on ca_profesores.No_Empleado=secciones.No_Empleado and ca_periodos.id_periodo = secciones.periodoId
        LEFT join ca_profesores_proyectos on ca_profesores.No_Empleado=ca_profesores_proyectos.No_Empleado
        LEFT join (SELECT * FROM ca_proyectos where curdate() BETWEEN ca_proyectos.fecha_inicio and ca_proyectos.fecha_fin) as proyectoP on ca_profesores_proyectos.id_proyecto=proyectoP.id_proyecto
        LEFT join ca_tipos_proyectos on proyectoP.id_tipo_proyecto=ca_tipos_proyectos.id_tipo_proyecto
        LEFT JOIN (empleado INNER JOIN departamento_laboral on empleado.Id_departamento = departamento_laboral.Id_departamento_laboral) on ca_profesores.No_Empleado = empleado.No_Empleado
        LEFT JOIN persona on empleado.N_identidad = persona.N_identidad
        LEFT JOIN ca_asignaturas on secciones.id_asignatura=ca_asignaturas.id_asignatura
        WHERE ca_periodos.id_periodo = ( SELECT ca_periodos.id_periodo from ca_periodos where ca_periodos.fecha_ingreso_i<= curdate() ORDER by ca_periodos.fecha_ingreso_i DESC LIMIT 1) and (ca_carga_profesores.completa = 0 or ca_carga_profesores.completa = 1) ORDER BY ca_carga_profesores.cantidad_uv DESC,ca_profesores.No_Empleado";
    }
    
?>

<div class="box-body table-responsive no-padding">
    <table id = "tablaAlertasProfesores" class="table table-hover">
        <thead>
            <tr>
                <th style="text-align:center;background-color:#386D95;color:white;">Empleado</th>
                <th style="text-align:center;background-color:#386D95;color:white;display:none;">Identidad</th>
                <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th>
                <th style="text-align:center;background-color:#386D95;color:white;<?php if($rol==30){ echo 'display:none;';} ?>">Departamento</th>
                <th style="text-align:center;background-color:#386D95;color:white;">Contrato</th>
                <th style="text-align:center;background-color:#386D95;color:white;">UV's</th>
                <th style="text-align:center;background-color:#386D95;color:white;">Carga</th>
                <th style="text-align:center;background-color:#386D95;color:white;">Secciones</th>
                <th style="text-align:center;background-color:#386D95;color:white;">Proyectos</th>
                <th style="text-align:center;background-color:#386D95;color:white;">Actividades</th>
            </tr>
        </thead>
        <?php
            $result =$db->prepare($query);
            $result->execute();
            $tempEmpleato="";
            $bandera = true;
            $ticket = false;
            $ProfesoresAlerta=array();
            while ($fila = $result->fetch()) {
                if ($bandera){
                    $periodoCarga = $fila["id_periodo"];
                    $bandera = false;
                }

                if ($tempEmpleato!=$fila["No_Empleado"]){
                    $tempEmpleato = $fila["No_Empleado"];
                    $ProfesoresAlerta[$fila["No_Empleado"]]= new Profesor(
                                                $fila["No_Empleado"],
                                                $fila["N_identidad"],
                                                $fila["nombre"],
                                                $fila["nombre_departamento"],
                                                $fila["contrato"],
                                                $fila["cantidad_uv"],
                                                $fila["completa"]);
                }

                if($fila["id_seccion"]!="vacio"){
                    $ProfesoresAlerta[$fila["No_Empleado"]]->secciones[$fila["id_seccion"]]= new Seccion(
                                                $fila["codigo_seccion"],
                                                $fila["id_asignatura"],
                                                $fila["asignatura"],
                                                $fila["uv"]);
                }

                if($fila["id_proyecto"]!="vacio"){
                    $ProfesoresAlerta[$fila["No_Empleado"]]->proyectos[$fila["id_proyecto"]]= new Proyecto(
                                                  $fila["proyecto"],
                                                  $fila["tipoProyecto"],
                                                  $fila["descripcion"],
                                                  $fila["fecha_inicio"],
                                                  $fila["fecha_fin"]);
                }
                if($fila["id_actividad"]!="vacio"){
                    $ticket = true;
                    $ProfesoresAlerta[$fila["No_Empleado"]]->actividades[$fila["id_actividad"]]= new Actividad(
                                                  $fila["Actividad"],
                                                  $fila["tipo"]);
                }
            }
            if (!$bandera){
                $_SESSION['ProfesoresAlertas'] = array('periodo' => $periodoCarga,'profesores' => $ProfesoresAlerta );
            }
        ?>
        <tbody data-id='<?php echo $periodoCarga; ?>'>
            <?php
                foreach ($ProfesoresAlerta as $Nempleado => $profesorTemp) {
            ?>
                <tr data-id='<?php echo $Nempleado; ?>' data-nemp='<?php echo $profesorTemp->getNombre(); ?>' data-uvs='<?php echo $profesorTemp->getCantidad_uv(); ?>' data-ticket='<?php echo $ticket; ?>'>
                    <td><?php echo $profesorTemp->getNo_Empleado(); ?></td>
                    <td style = "display:none;"><?php echo $profesorTemp->getN_identidad(); ?></td>
                    <td><?php echo $profesorTemp->getNombre(); ?></td>
                    <td style = "<?php if($rol==30){ echo 'display:none;';} ?>"> <?php echo $profesorTemp->getNombre_depto(); ?> </td>
                    <td><?php echo $profesorTemp->getContrato(); ?></td>
                    <td><?php echo $profesorTemp->getCantidad_uv(); ?></td>
                    <td>
                        <?php if($_SESSION['user_rol']==100 || $_SESSION['user_rol']==50 || $_SESSION['user_rol']==30){ ?> 
                            <span class='label'>
                                <center><button class="btnCarga<?php echo $auto_increment; ?> <?php if (($profesorTemp ->getCompleta()) == 'COMPLETA') { echo 'label-success';}else{ echo 'label-danger'; }?>" <?php if(($profesorTemp->getCompleta())=='COMPLETA'){ echo 'disabled';} ?>> <?php echo $profesorTemp->getCompleta(); ?></button></center>
                            </span>
                        <?php }else{ ?>
                            <span class="label <?php if (($profesorTemp ->getCompleta()) == 'COMPLETA') { echo 'label-success'; }else{ echo 'label-danger'; }?>"><?php echo $profesorTemp->getCompleta(); ?></span>
                        <?php } ?>
                    </td>

                    <td>
                        <center>
                            <button type="button" class="btnSecciones btn btn-success   glyphicon glyphicon glyphicon-list-alt"  title="Ver secciones" 
                                <?php if (count($profesorTemp->secciones)<1){ ?>
                                    disabled="true";
                                <?php } ?>
                            </button>
                        </center>
                    </td>  
                    <td>
                        <center>
                            <button type="button" class="btnProyectos btn btn-warning   glyphicon glyphicon glyphicon-list-alt"  title="Ver proyectos" 
                                <?php if (count($profesorTemp->proyectos)<1){ ?>
                                    disabled="true";
                                <?php } ?>
                            </button>
                        </center>
                    </td>
                    <td>
                        <center>
                            <button type="button" style ="background-color:#04B486;border-color:#04B486" class="btnActividades btn btn-warning   glyphicon glyphicon glyphicon-list-alt"  title="Ver Actividaes" 
                                <?php if (count($profesorTemp->actividades)<1){ ?>
                                    disabled="true";
                                <?php } ?>
                            </button>
                        </center>
                    </td>  
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div><!-- /.box-body -->
<!--*****se coloca este script aquí y no en Script.js**********
******para utilizar la variable $_SESSION['auto_increment']****
*******************para eliminar duplicados*****************-->
<script type="text/javascript">
$('#tablaAlertasProfesores')
    .removeClass('display')
    .addClass('table table-striped table-bordered');
$('#tablaAlertasProfesores').dataTable({
    "order": [[0, "asc"]],
    "fnDrawCallback": function (oSettings) {


    }
    ,
    "language":
    {
        "lengthMenu": "Mostrar _MENU_ registros por página",
        "zeroRecords": "No hay datos disponibles",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
        "search": "Buscar",
        "paginate":
                {
                    "previous": "Anterior",
                    "next" : "Siguiente"
                }
    }
});


$(document).on("click", ".btnCarga<?php echo $auto_increment; ?>", function () {
    nEmpleado = $(this).parents("tr").data("nemp");
    if(confirm("¿Desea completar la carga académica de "+nEmpleado+"?")){
        data={
            No_Empleado:$(this).parents("tr").data("id"),
            periodo:$(this).parents("tbody").data("id"),
            uvs:$(this).parents("tr").data("uvs"),
            tckt:$(this).parents("tr").data("ticket")
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: actulizarCargaProfesor,
            timeout: 8000,
            error: problemas
        }); 
        return false;
    }
});

</script>