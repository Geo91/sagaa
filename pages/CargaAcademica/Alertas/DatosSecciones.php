<?php
  $maindir = "../../../";
  require_once ("../clases.php");
  require_once($maindir."funciones/check_session.php");

  if (isset($_SESSION['ProfesoresAlertas'])) {
    if(isset($_POST['No_Empleado'])){
?>
<div class="box-body table-responsive no-padding">
  <table id = "tablaSeccionesProfesores" class="table table-hover">
    <thead>
      <tr>
        <th>Código de sección</th>
        <th>Código de asignatura</th>
        <th>Nombre de asignatura</th>
        <th>UV's</th>
      </tr>
    </thead>
    <tbody>
      <?php
        $seccionesProfesor=$_SESSION['ProfesoresAlertas']['profesores'][$_POST['No_Empleado']]->secciones;
        foreach ($seccionesProfesor as $idSeccion => $seccionTemp) {
      ?>
          <tr data-id='<?php echo $idSeccion; ?>'>
            <td><?php echo $seccionTemp->getCodigo_seccion(); ?></td>
            <td><?php echo $seccionTemp->getId_asignatura(); ?></td>
            <td><?php echo $seccionTemp->getAsignatura(); ?></td>
            <td><?php echo $seccionTemp->getsUv(); ?></td>
          </tr>
      <?php } ?>
    </tbody>
  </table>
</div><!-- /.box-body -->
<?php
    }
  }
?>