<?php
    $maindir = "../../../";
    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."Datos/funciones.php");

    if (isset($_SESSION['ProfesoresAlertas'])) {
        if(isset($_POST['No_Empleado'])){
?>
<div class="box-body table-responsive no-padding">
    <table id = "tablaActividadesProfesores" class="table table-hover">
        <thead>
            <tr>
                <th>Descripción de la actividad</th>
                <th>Tipo</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $actividadesProfesor=$_SESSION['ProfesoresAlertas']['profesores'][$_POST['No_Empleado']]->actividades;
            foreach ($actividadesProfesor as $idActividad => $actividadTemp) {
        ?>
            <tr data-id='<?php echo $idActividad; ?>'>
                <td><?php echo $actividadTemp->getDescActividad(); ?></td>
                <td><?php echo $actividadTemp->getTipo(); ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div><!-- /.box-body -->

<?php
        }
    }
?>