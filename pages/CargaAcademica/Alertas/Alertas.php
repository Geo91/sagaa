<html lang="es">

<?php

    $maindir = "../../../";

  if(isset($_GET['contenido']))
    {
      $contenido = $_GET['contenido'];
    }
  else
    {
      $contenido = 'carga_academica';
    }

  require_once("../clases.php");

  require_once($maindir."funciones/check_session.php");

  include($maindir."funciones/timeout.php");

  $ultimoP =$_SESSION['ultimoPeriodo'];
  
?>
<div id="divRespuesta">
</div>
<div class="col-xs-12">
  <div class="box box-danger">      
      <div class="box-header">
        <h3 class="box-title">Resumen Carga Académica de Docentes</h3>
        <div align="right"><br><button type="button" id="btnActualizarAlerta" class="btn btn-info"  onmouseover=""><span class="glyphicon glyphicon-repeat"></span> Actualizar tabla</button>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</div>
      </div>
      <?php echo "&nbsp&nbsp&nbspPeriodo ".$ultimoP->getPeriodo()." año ".$ultimoP->getAnio(); ?>
      <div id="tablaProfesores">
        <?php require_once("RecargarTabla.php"); ?>
      </div>
  </div>
</div>

<!--************modal de informacion adicional*****************-->
<div class="modal fade" id="InfoAdicional" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header" style = "background-color:#0FA6C3">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="color:white"><strong>Información Adicional</strong></h4>
        </div>
        <div class="modal-body">
            <div id="subTabla"></div>
        </div>
      </div>
    </div>
</div>
<!--******************modal de edicion*****************-->
<script type="text/javascript" src="pages/CargaAcademica/Alertas/Script.js"/>