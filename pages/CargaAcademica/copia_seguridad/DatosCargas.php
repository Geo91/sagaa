<?php
    $maindir = "../../";
    require_once ("Reportes/Cargas/clases.php");
    require_once ("clases.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO

if (isset($_SESSION['ultimoPeriodo'])) {

    $ultimoPID = $_SESSION['ultimoPeriodo']->getId();
    $IDusuario = $_SESSION['user_id'];

    $query1 = "SELECT CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'nombreE'
            FROM usuario,empleado,persona 
            WHERE usuario.No_Empleado=empleado.No_Empleado
                and empleado.N_identidad=persona.N_identidad
                and usuario.id_Usuario=$IDusuario ";

    $nombreProfesor="";
    $result =$db->prepare($query1);
    $result->execute();
    while ($fila = $result->fetch()) {
        $nombreProfesor = $fila["nombreE"];
        break;
    }

    $query1 = "SELECT ca_secciones.id_seccion,
                    ca_secciones.codigo_seccion,
                    ca_secciones.id_asignatura,
                    ca_asignaturas.nombre as nAsignatura,
                    ca_secciones.cupos_max,
                    ca_secciones.matriculados,
                    ca_secciones.Hora_inicio,
                    ca_secciones.Hora_fin,
                    ca_dias.id_dia,
                    ca_dias.nombre as Dia,
                    ca_secciones.id_aula,
                    ca_aulas.nombre as nAula,
                    ca_edificios.nombre as nEdificio,
                    ca_secciones.No_Empleado,
                    CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'nombreE',
                    ca_departamentos_carreras.nombre as nombreDepto
                FROM ca_secciones,ca_asignaturas,ca_aulas,ca_edificios,empleado,persona,ca_secciones_dias,ca_dias,ca_cargas,ca_periodos,ca_departamentos_carreras
                WHERE ca_secciones.id_asignatura=ca_asignaturas.id_asignatura
                    and ca_secciones.id_seccion=ca_secciones_dias.id_seccion
                    and ca_secciones_dias.id_dia = ca_dias.id_dia
                    and ca_secciones.id_aula=ca_aulas.id_aula
                    and ca_aulas.id_edificio=ca_edificios.id_edificio
                    and ca_secciones.No_Empleado=empleado.No_Empleado
                    and empleado.N_identidad=persona.N_identidad
                    AND ca_secciones.id_carga=ca_cargas.id_carga
                    and ca_cargas.id_periodo=ca_periodos.id_periodo
                    and ca_cargas.id_departamento=ca_departamentos_carreras.id_departamento
                    and ca_periodos.id_periodo= $ultimoPID
                    AND ca_secciones.No_Empleado=(SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario=$IDusuario  )";

    $result =$db->prepare($query1);
    $result->execute();
    $tempID=-1;
    $seccionesTabla = array();
    $Registros = false;
    while ($fila = $result->fetch()) {
        if ($tempID!=$fila["id_seccion"]) {
            $tempID = $fila["id_seccion"];
            $id_seccion = $fila["id_seccion"];
            $codigo_seccion = $fila["codigo_seccion"];
            $id_asignatura = $fila["id_asignatura"];
            $nAsignatura = $fila["nAsignatura"];
            $cupos_max = $fila["cupos_max"];
            $matriculados = $fila["matriculados"];
            $Hora_inicio = $fila["Hora_inicio"];
            $Hora_fin = $fila["Hora_fin"];
            $id_aula = $fila["id_aula"];
            $nAula = $fila["nAula"];
            $nEdificio = $fila["nEdificio"];
            $No_Empleado = $fila["No_Empleado"];
            $nombreE = $fila["nombreE"];
            $Depto = $fila["nombreDepto"];
            $seccionesTabla[$fila["id_seccion"]] = new SeccionCarga($id_seccion,
                                     $codigo_seccion,
                                     $id_asignatura,
                                     $nAsignatura,
                                     $cupos_max,
                                     $matriculados, 
                                     $Hora_inicio,
                                     $Hora_fin,
                                     $id_aula,
                                     $nAula,
                                     $nEdificio,
                                     $No_Empleado,
                                     $nombreE,
                                     $Depto);
            $seccionesTabla[$fila["id_seccion"]]->Dias[$fila["id_aula"]]=$fila["Dia"];
        }else{
            $seccionesTabla[$fila["id_seccion"]]->Dias[$fila["id_dia"]]=$fila["Dia"];
        }
        $Registros = true;
    }
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Carga académica: <?php echo $nombreProfesor; ?></h1>
    </div>
</div> 
<div class="row">
<div class="box box-warning">
    <div class="box-body table-responsive">
        <table id="tablaRepoCargas" class="table table-bordered table-striped">
            <thead>
                <tr>                                            
                    <th>Código</th>
                    <th>Asignatura</th>
                    <th>Matriculados/ Cupos</th>
                    <th>Hora: inicio/ fin</th>
                    <th>Dias</th>
                    <th>Aula</th>
                    <th>Edificio</th>
                    <th>Profesor</th>
                    <th>Departamento</th>
                </tr>
            </thead>
            <tbody>
            <?php
            if ($Registros) {
                foreach ($seccionesTabla as $key => $subSeccion) { 
                    $TempIDseccion = $subSeccion->getid_seccion();
                    ?>
                    <tr>
                        <td><?php echo $subSeccion->getcodigo_seccion(); ?></td>
                        <td><?php echo $subSeccion->getnAsignatura(); ?></td>
                        <td><?php echo $subSeccion->getmatriculados()."/".$subSeccion->getcupos_max(); ?></td>
                        <td><?php echo substr($subSeccion->getHora_inicio(),0,-3)."\n <br/>". substr($subSeccion->getHora_fin(),0,-3); ?></td>
                        </td>
                        <td>
                        <?php foreach ($subSeccion->Dias as $value) {
                                if ($value!='Sábado'){
                                    echo substr($value, 0,2)." \n <br>";
                                }else{
                                    echo "Sa \n <br>";
                                }
                            }
                        ?></td>
                        <td><?php echo $subSeccion->getnAula(); ?></td>
                        <td><?php echo $subSeccion->getnEdificio(); ?></td>
                        <td><?php echo $subSeccion->getnombreE(); ?></td>
                        <td><?php echo $subSeccion->getDepto(); ?></td>
                    </tr>
            <?php } 
            }?>
            </tbody>
            <tfoot>
                <tr>                                           
                    <th>Código</th>
                    <th>Asignatura</th>
                    <th>Matriculados/ Cupos</th>
                    <th>Hora: inicio/ fin</th>
                    <th>Dias</th>
                    <th>Aula</th>
                    <th>Edificio</th>
                    <th>Profesor</th>
                    <th>Departamento</th>
                </tr>
            </tfoot>
        </table>
    </div><!-- /.box-body -->
</div>
</div>
<script type="text/javascript">
     $('#tablaRepoCargas')
        .removeClass('display')
        .addClass('table table-striped table-bordered');
    $('#tablaRepoCargas').DataTable({
                dom: 'Blfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                download: 'open',
                title:'Carga académica de: <?php echo $nombreProfesor; ?>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                download: 'open',
                orientation: 'landscape',
                pageSize: 'LETTER',
                title:'Carga académica de: <?php echo $nombreProfesor; ?>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'print',
                title:'Carga académica de: <?php echo $nombreProfesor; ?>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'
        ]
    });

    $('.toggle-vis1').on( 'click', function (e) {
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
 
        // Toggle the visibility
        column.visible( ! column.visible() );
        $(this).attr('color', ' blue');
    });
    $(".toggle-vis1").prop("disabled", "");
</script>
<?php }else{
    echo mensajes("Problemas en la página, intentelo de nuevo","azul");
} ?>