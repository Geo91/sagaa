<?php
    $maindir = "../../";
    require_once("ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $IDusuario = $_SESSION['user_id'];
    $query1 = "SELECT ca_proyectos.id_proyecto,
                    ca_proyectos.nombre,
                    ca_tipos_proyectos.nombre as area,
                    ca_proyectos.fecha_inicio,
                    ca_proyectos.fecha_fin
                FROM ca_proyectos,ca_tipos_proyectos
                WHERE ca_proyectos.id_tipo_proyecto=ca_tipos_proyectos.id_tipo_proyecto
                    and ca_proyectos.id_proyecto in (SELECT ca_profesores_proyectos.id_proyecto
            from ca_profesores_proyectos,empleado,usuario
            WHERE ca_profesores_proyectos.No_Empleado=empleado.No_Empleado
                and empleado.No_Empleado=usuario.No_Empleado
                and usuario.id_Usuario=$IDusuario)";

    $queryNom = "SELECT CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'nombreE'
            FROM usuario,empleado,persona 
            WHERE usuario.No_Empleado=empleado.No_Empleado
                and empleado.N_identidad=persona.N_identidad
                and usuario.id_Usuario=$IDusuario ";

    $nombreProfesor="";
    $result =$db->prepare($queryNom);
    $result->execute();
    while ($fila = $result->fetch()) {
        $nombreProfesor = $fila["nombreE"];
        break;
    }
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Todos los proyectos de: <?php echo $nombreProfesor; ?></h1>
    </div>
</div> 
<div class="box box-primary">      
    <div class="box">
        <div class="box-body table-responsive">
            <table id="tablaProyectosProf" class="table table-bordered table-striped">
                <thead>
                    <tr>                                            
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Area</th>
                        <th>Fecha inicio</th>
                        <th>Fecha finalización</th>
                        <th>Ver</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $result =$db->prepare($query1);
                    $result->execute();
                    while ($fila = $result->fetch()) {
                        ?>
                        <tr data-id='<?php echo $fila["id_proyecto"]; ?>'>
                            <td><?php echo $fila["id_proyecto"]; ?></td>
                            <td><?php echo $fila["nombre"]; ?></td>
                            <td><?php echo $fila["area"]; ?></td>
                            <td><?php echo $fila["fecha_inicio"]; ?></td>
                            <td><?php echo $fila["fecha_fin"]; ?></td>
                            <td>
                                <center>
                                    <button type="button" class="VerInvolucrados<?php echo $auto_increment; ?> btn btn-primary glyphicon glyphicon-info-sign"  title="Ver información"></button>
                                </center>
                            </td>         
                        </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                    <tr>                                         
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Area</th>
                        <th>Fecha inicio</th>
                        <th>Fecha finalización</th>
                        <th>Ver</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
    </div>
</div>
<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#tablaProyectosProf')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaProyectosProf').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
    
$(document).on("click", ".VerInvolucrados<?php echo $auto_increment; ?>", function () {
    data={
            proyectoId:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: verInvolucrados,
        timeout: 4000,
        error: problemas
    }); 
    return false;
});

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function verInvolucrados(){
    $("#subContenidoProyecto").empty();
    $("#subContenidoProyecto").load("pages/CargaAcademica/GestionProyectos/DatosInvolucrados.php",data);
    $("#modalProyecto").modal('show');
}


</script>