<?php

class Periodo{
	private $id;
	private $periodo;
	private $anio;
	private $fechaI;
	private $fechaR;
	private $fechaM;
	private $fechaF;
	private $proceso;

	public function __construct ($id,$periodo,$anio,$fechaI,$fechaR,$fechaM,$fechaF,$proceso){
		$this->id=$id;
		$this->id=$id;
		$this->periodo=$periodo;
		$this->anio=$anio;
		$this->fechaI=$fechaI;
		$this->fechaF=$fechaF;
		$this->fechaR=$fechaR;
		$this->fechaM=$fechaM;
		$this->proceso=$proceso;
	}

	public function getId(){
		return $this->id;
	}
	public function getPeriodo(){
		return $this->periodo;
	}
	public function getAnio(){
		return $this->anio;
	}
	public function getFechaI(){
		return $this->fechaI;
	}
	public function getFechaR(){
		return $this->fechaR;
	}
	public function getFechaM(){
		return $this->fechaM;
	}
	public function getFechaF(){
		return $this->fechaF;
	}
	public function getProceso(){
		return $this->proceso;
	}
	public function setProceso($proceso){
		return $this->proceso=$proceso;
	}
}
class Carga{
	private $id;
	private $aprobado;
	private $id_departamento;
	private $nombre_departamento;



	public function __construct ($id,$aprobado,$id_departamento,$nombre_departamento){
		$this->id=$id;
		$this->aprobado=$aprobado;
		$this->id_departamento= $id_departamento;
		$this->nombre_departamento= $nombre_departamento;
	}

	public function getId(){
		return $this->id;
	}
	
	public function getAprobado(){
		return $this->aprobado;
	}
	
	public function getId_departamento(){
		return $this->id_departamento;
	}
	public function getNombre_departamento(){
		return $this->nombre_departamento;
	}
}

//--------------------clases para las alertas-----------------
class Profesor{
	private $No_Empleado;
	private $N_identidad;
	private $nombre;
	private $contrato;
	private $cantidad_uv;
	private $completa;
	public $secciones;
	public $proyectos;



	public function __construct ($No_Empleado,$N_identidad,$nombre,$contrato,$cantidad_uv,$completa){
		$this->No_Empleado=$No_Empleado;
		$this->N_identidad=$N_identidad;
		$this->nombre= $nombre;
		$this->contrato= $contrato;
		$this->cantidad_uv= $cantidad_uv;
		$this->completa= $completa;
		$this->secciones= array();
		$this->proyectos= array();
	}

	public function getNo_Empleado(){
		return $this->No_Empleado;
	}
	
	public function getN_identidad(){
		return $this->N_identidad;
	}
	public function getNombre(){
		return $this->nombre;
	}
	public function getContrato(){
		return $this->contrato;
	}
	public function getCantidad_uv(){
		return $this->cantidad_uv;
	}
	public function getCompleta(){
		return $this->completa;
	}
	public function getSecciones(){
		return $this->secciones;
	}
	public function getProyectos(){
		return $this->proyectos;
	}
}
class Seccion{
	private $codigo_seccion;
	private $id_asignatura;
	private $asignatura;
	private $uv;



	public function __construct ($codigo_seccion,$id_asignatura,$asignatura,$uv){
		$this->codigo_seccion=$codigo_seccion;
		$this->id_asignatura=$id_asignatura;
		$this->asignatura= $asignatura;
		$this->uv= $uv;
	}

	public function getCodigo_seccion(){
		return $this->codigo_seccion;
	}
	
	public function getId_asignatura(){
		return $this->id_asignatura;
	}
	public function getAsignatura(){
		return $this->asignatura;
	}
	public function getsUv(){
		return $this->uv;
	}
}
class Proyecto{
	private $nombreProyecto;
	private $tipoProyecto;
	private $descripcion;
	private $fecha_inicio;
	private $fecha_fin;



	public function __construct ($nombreProyecto,$tipoProyecto,$descripcion,$fecha_inicio,$fecha_fin){
		$this->nombreProyecto=$nombreProyecto;
		$this->tipoProyecto=$tipoProyecto;
		$this->descripcion=$descripcion;
		$this->fecha_inicio= $fecha_inicio;
		$this->fecha_fin= $fecha_fin;
	}

	public function getNombreProyecto(){
		return $this->nombreProyecto;
	}
	
	public function getTipoProyecto(){
		return $this->tipoProyecto;
	}
	
	public function getDescripcion(){
		return $this->descripcion;
	}
	public function getFecha_inicio(){
		return $this->fecha_inicio;
	}
	public function getFecha_fin(){
		return $this->fecha_fin;
	}
}
?>