  /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){	

    $("#Flecha1").click(function(event) {
        event.preventDefault();
        $('#SubFlecha1').toggleClass('glyphicon-chevron-down glyphicon-chevron-right');
    });
    
    $("#Flecha2").click(function(event) {
        event.preventDefault();
        $('#SubFlecha2').toggleClass('glyphicon-chevron-down glyphicon-chevron-right');
    });

    $("#Flecha3").click(function(event) {
        event.preventDefault();
        $('#SubFlecha3').toggleClass('glyphicon-chevron-down glyphicon-chevron-right');
    });

    $("#Flecha4").click(function(event) {
        event.preventDefault();
        $('#SubFlecha4').toggleClass('glyphicon-chevron-down glyphicon-chevron-right');
    });

    $("#Flecha5").click(function(event) {
        event.preventDefault();
        $('#SubFlecha5').toggleClass('glyphicon-chevron-down glyphicon-chevron-right');
    });

    $("#Flecha6").click(function(event) {
        event.preventDefault();
        $('#SubFlecha6').toggleClass('glyphicon-chevron-down glyphicon-chevron-right');
    });


    $("#inicioCargaAcademica").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/Alertas/Alertas.php");
    });

    $("#lnkTiposProyectos").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/TiposProyectos/TiposProyectos.php");
    });

    $("#lnkProyProf").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/DatosProyectos.php");
    });

    $("#lnkInicioProfesor").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/CargaDocente.php");
    });

    $("#lnkInicioDocente").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/CargaDocente.php");
    });

    $("#lnkRepoContrato").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/Reportes/Contratos/Contratos.php");
    });

    $("#lnkRepoProfProy").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/Reportes/Proyectos/ProfesoresProyectos.php");
    });

    $("#lnkReporteCargas").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/Reportes/Cargas/CargsaAcademicas.php");
    });

    $("#lnkTodosPeriodos").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/TodosPeriodos/TodosPeriodos.php");
    });

    $("#lnkCargasAcademicas").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/TodasCargas/principal.php");
    });

    $("#lnkJefesDepartamento").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/JefesDepartamento/JefesDepartamento.php");
    });

    $("#lnkDepartamentos").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/Departamentos/Departamentos.php");
    });

    $("#lnkEdificios").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/Edificios/Edificios.php");
    });

    $("#lnkTodosProyectos").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/GestionProyectos/DatosProyectos.php");
    });

    $("#lnkNuevoProyecto").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/GestionProyectos/GestionProyectos.php");
    });

    $("#lnkProfesoresDiscapacidades").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/ProfesoresDiscapacidades/Discapacidades.php");
    });

    $("#lnkTiposDiscapacidades").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/TiposDiscapacidades/TiposDiscapacidades.php");
    });

    $("#lnkTiposContrato").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/GestionContratos/TiposContrato.php");
    });

    $("#lnkAgregarAsignaturas").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/GestionAsignaturas/Asignaturas.php");
    });

    $("#lnkProgramacion").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/ProgramacionCarga/ProgramacionCarga.php");
    });

    $("#lnkAgregarCarga").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/GestionCargas/Cargas.php");
    });

    $("#lnkProfesores").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/GestionProfesores/GestionProfesores.php");
    });

    $("#lnkTiposAcond").click(function(event) {
        event.preventDefault();
        $("#contenedor").empty();
        $("#contenedor").load("pages/CargaAcademica/TiposAcondicionamiento/TiposAcondicionamiento.php");
    });
});
