<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query1 = "SELECT id_asignatura,ca_asignaturas.nombre as nombre, uv, ca_departamentos_carreras.nombre as depto,
        ca_departamentos_carreras.id_departamento as id_depto
                FROM ca_asignaturas,ca_departamentos_carreras
                where ca_asignaturas.id_departamento = ca_departamentos_carreras.id_departamento
                    and  ca_asignaturas.habilitada = 1";
?>
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaAsignaturas" class="table table-bordered table-striped">
            <thead>
                <tr>                                            
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Departamento</th>
                    <th>UV's</th>
                    <th>Editar</th>
                    <th>Deshabilitar</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $result =$db->prepare($query1);
                $result->execute();
                while ($fila = $result->fetch()) {
                    ?>
                    <tr data-id='<?php echo $fila["id_asignatura"]; ?>'>
                        <td><?php echo $fila["id_asignatura"]; ?></td>
                        <td><?php echo $fila["nombre"]; ?></td>
                        <td id='<?php echo $fila["id_depto"]; ?>'><?php echo $fila["depto"]; ?></td>
                        <td><?php echo $fila["uv"]; ?></td>
                        <td>
                            <center>
                                <button type="button" class="editarAsignatura btn btn-primary glyphicon glyphicon-edit"  title="Editar asignatura">
                            </center>
                        </td>       
                        <td>
                            <center>
                                <button type="button" class="deshabilitarAsignatura<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-ban-circle"  title="Deshabilitar Asignatura">
                            </center>
                        </td>     
                    </tr>
            <?php } ?>
            </tbody>
            <tfoot>
                <tr>                                            
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Departamento</th>
                    <th>UV's</th>
                    <th>Editar</th>
                    <th>Deshabilitar</th>
                </tr>
            </tfoot>
        </table>
    </div><!-- /.box-body -->
</div>
<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#tablaAsignaturas')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaAsignaturas').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
$(document).on("click", ".deshabilitarAsignatura<?php echo $auto_increment; ?>", function () {
    if (confirm("ATENCIÓN!: LA ASIGNATURA NO SERÁ ELIMINADA SOLO SERÁ DESHABILITADA PARA NO VOLVERLA A USAR. ¿Está seguro de que desea deshabilitar esta asignatura?")){
        codigo = $(this).parents("tr").data("id");
        data={
                    Codigo:codigo
            }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: deshabilitarAsignaturas,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    }
});
</script>