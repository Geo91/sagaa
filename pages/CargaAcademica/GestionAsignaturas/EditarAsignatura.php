<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Codigo']) && 
        isset($_POST['Nombre']) && 
        isset($_POST['Departamento']) && 
        isset($_POST['uv']) ){
        
        $Codigo = $_POST['Codigo'];
        $Nombre = $_POST['Nombre'];
        $Departamento = $_POST['Departamento'];
        $uv = $_POST['uv'];

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query0 = "SELECT * FROM `ca_asignaturas` WHERE `id_asignatura`='$Codigo'";
        $query = "UPDATE `ca_asignaturas` set `nombre`='$Nombre', `uv`=$uv,`id_departamento`=$Departamento where `id_asignatura`='$Codigo'";
        try {
            $result =$db->prepare($query0);
            $result->execute();

            $bandera = true;
            while ($fila = $result->fetch()) {
                $bandera = false;
            }
            if($bandera){
                $mensaje = "La asignatura con codigo `$Codigo` no existe";
                $color = "azul";
            }
            else{
                $result =$db->prepare($query);
                $result->execute();
                $mensaje = "La asignatura con codigo `$Codigo` ha sido editada";
                $color = "verde";
                echo mensajes($mensaje,$color);
            }
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>
<script type="text/javascript">
    $("#datosTabla").load('pages/CargaAcademica/GestionAsignaturas/DatosAsignaturas.php');
</script>