<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Codigo'])){
        
        $Codigo = $_POST['Codigo'];

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query = "UPDATE ca_asignaturas set ca_asignaturas.habilitada=0 WHERE ca_asignaturas.id_asignatura='$Codigo'";

        try{
            $result =$db->prepare($query);
            $result->execute();

            echo mensajes("La asignatura con código `$Codigo` se ha deshabilitado","verde");
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }else{
        echo mensajes("Problemas con la conexión","azul");
    }
?>
<script type="text/javascript">
    $("#datosTabla").load('pages/CargaAcademica/GestionAsignaturas/DatosAsignaturas.php');
</script>