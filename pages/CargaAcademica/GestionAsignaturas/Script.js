//cargar la tabla desde un principio
    $("#datosTabla").load('pages/CargaAcademica/GestionAsignaturas/DatosAsignaturas.php');

$(document).ready(function(){   

//para cuando se da submit al formulario formAsignatura
    $("#formAsignatura").submit(function(e) {
        e.preventDefault();
        data={
                    Codigo:$('#Codigo').val(),
                    Nombre:$('#Nombre').val(),
                    Departamento:$('#selectDepartamento').val(),
                    uv:$('#uv').val()
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: agregarAsignaturas,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });


//para cuando se da submit al formulario formAsignatura
    $("#formEditar").submit(function(e) {
        e.preventDefault();
        data={
                    Codigo:$('#modalCodigo').val(),
                    Nombre:$('#modalNombre').val(),
                    Departamento:$('#modalselectDepartamento').val(),
                    uv:$('#modaluv').val()
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: editarAsignaturas,
            timeout: 4000,
            error: problemas
        }); 
        $("#modalEditar").modal('hide');
        return false;
    });

});

//para editar una asignatura
$(document).on("click", ".editarAsignatura", function () {
    codigo = $(this).parents("tr").find("td").eq(0).html();
    Nombre = $(this).parents("tr").find("td").eq(1).html();
    depto = $(this).parents("tr").find("td").eq(2).attr('id').valueOf();
    uv = $(this).parents("tr").find("td").eq(3).html();

    $("#modalCodigo").val(codigo);
    $("#modalNombre").val(Nombre);
    $("#modalselectDepartamento").val(depto);
    $("#modaluv").val(uv);

    $("#modalEditar").modal('show');
});


function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function agregarAsignaturas(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/CargaAcademica/GestionAsignaturas/AgregarAsignatura.php',data);
    $('#formAsignatura').trigger("reset");
}

function editarAsignaturas(){
    $("#divRespuestaEditar").empty();
    $("#divRespuestaEditar").load('pages/CargaAcademica/GestionAsignaturas/EditarAsignatura.php',data);
}

function deshabilitarAsignaturas(){
    $("#divRespuestaEditar").empty();
    $("#divRespuestaEditar").load('pages/CargaAcademica/GestionAsignaturas/DeshabilitarAsignatura.php',data);
}