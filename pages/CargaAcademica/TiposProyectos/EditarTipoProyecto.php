<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Codigo']) && 
        isset($_POST['Nombre']) ){
        
        $Codigo = $_POST['Codigo'];
        $Nombre = $_POST['Nombre'];

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query = "UPDATE ca_tipos_proyectos
                SET ca_tipos_proyectos.nombre='$Nombre'
                WHERE ca_tipos_proyectos.id_tipo_proyecto=$Codigo";
        try {
            $result =$db->prepare($query);
            $result->execute();
            echo mensajes("Se ha actualizado el tipo de proyecto ID.$Codigo","verde");
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }else{
        echo mensajes("Ha ocurrido un error del servidor...","verde");
    }
?>
<script type="text/javascript">
    $("#datosTabla").load('pages/CargaAcademica/TiposProyectos/DatosTipoProyecto.php');
</script>