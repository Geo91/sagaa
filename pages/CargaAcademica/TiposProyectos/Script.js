//cargar la tabla desde un principio
    $("#datosTabla").load('pages/CargaAcademica/TiposProyectos/DatosTipoProyecto.php');

$(document).ready(function(){   

//para cuando se da submit al formulario formAsignatura
    $("#formTipoProyecto").submit(function(e) {
        e.preventDefault();
        data={
                    Nombre:$('#NombreTipoProyecto').val()                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: agregarTipoProyecto,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });


//para cuando se da submit al formulario formAsignatura
    $("#formEditar").submit(function(e) {
        e.preventDefault();
        data={
                    Codigo:$('#modalCodigo').val(),
                    Nombre:$('#modalNombre').val()
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: editarTipoProyecto,
            timeout: 4000,
            error: problemas
        }); 
        $("#modalEditar").modal('hide');
        return false;
    });

});

//para editar una asignatura
$(document).on("click", ".editarTipoProyecto", function () {
    codigo = $(this).parents("tr").find("td").eq(0).html();
    Nombre = $(this).parents("tr").find("td").eq(1).html();

    $("#modalCodigo").val(codigo);
    $("#modalNombre").val(Nombre);

    $("#modalEditar").modal('show');
});


function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function agregarTipoProyecto(){
    $("#divRespuesta").load('pages/CargaAcademica/TiposProyectos/GuardarTipoProyecto.php',data);
    $('#formTipoProyecto').trigger("reset");
}

function editarTipoProyecto(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/TiposProyectos/EditarTipoProyecto.php',data);
}

function eliminarTiposProyecto(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/TiposProyectos/EliminarTipoProyecto.php',data);
}