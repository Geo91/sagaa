<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
?>
<div  id="divRespuesta"></div>         
<div class="panel panel-default">
    <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Agregar nuevo tipo de proyecto</div>

    <div class="panel-body" >
        <div class="row">
            <div class="col-lg-12">
            <!--************************formulario*********************-->
                <form class="form-horizontal" role="form" id="formTipoProyecto" name="formTipoProyecto">

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Nombre</label>                       
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control" id="NombreTipoProyecto" name="NombreTipoProyecto" required>      
                        </div>                  
                    </div>

                    <div class="row">
                        <label class="control-label col-sm-2"></label>
                        <div class="col-sm-8">
                            <p aling ="right">
                                <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" ><span class=" glyphicon glyphicon-floppy-disk"></span> Agregar</button> 
                            </p>          
                        </div>                            
                    </div>    

                </form>
            <!--************************formulario*********************-->
            </div>
        </div>
    </div>                                    
</div> 

<!--tabla de asignaturas-->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Tipos de Proyectos:</h1>
    </div>
</div> 
<div id="divRespuestaEditar">
</div>
<div id="datosTabla">
</div>
<!--******************modal de edicion*****************-->
<div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar tipo de Proyecto</h4>
        </div>
        <div class="modal-body">
            <!--************************formulario*********************-->
                <form class="form-horizontal" role="form" id="formEditar" name="formEditar">

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Código</label>                       
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control" id="modalCodigo" name="modalCodigo" required disabled="true">              
                        </div>                  
                    </div>
                    
                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Nombre</label>                       
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control" id="modalNombre" name="modalNombre" required autocomplete="off">      
                        </div>                  
                    </div>

                    <div class="modal-footer">
                        <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button  id="guardarCambios" class="btn btn-primary" >Guardar</button>
                    </div>
                </form>
            <!--************************formulario*********************-->
        </div>
      </div>
    </div>
</div>
<!--******************modal de edicion*****************-->
<script type="text/javascript" src="pages/CargaAcademica/TiposProyectos/Script.js"></script>