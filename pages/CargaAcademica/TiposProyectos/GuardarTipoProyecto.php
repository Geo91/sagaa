<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Nombre']) ){
        
        $Nombre = $_POST['Nombre'];

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query0 = "SELECT * FROM `ca_tipos_proyectos`
                WHERE `nombre`='$Nombre'";
        $query = "INSERT INTO `ca_tipos_proyectos` (`id_tipo_proyecto`, `nombre`) VALUES (NULL, '$Nombre');";

        try{
            $result =$db->prepare($query0);
            $result->execute();
            
            $bandera = false;
            while ($fila = $result->fetch()) {
                $bandera = true;
                break;
            }
            if($bandera){
                $mensaje = "El tipo de proyecto `$Nombre` ya existe.";
                $color = "azul";
            }
            else{
                $result =$db->prepare($query);
                $result->execute();
                $mensaje = "Tipo de proyecto agregado";
                $color = "verde";
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>
<script type="text/javascript">
    $("#datosTabla").load('pages/CargaAcademica/TiposProyectos/DatosTipoProyecto.php');
</script>