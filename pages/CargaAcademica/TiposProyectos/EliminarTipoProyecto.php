<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Codigo'])){
        
        $Codigo = $_POST['Codigo'];

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query0 = "SELECT * FROM ca_proyectos
                WHERE ca_proyectos.id_tipo_proyecto=$Codigo";
        $query = "DELETE FROM ca_tipos_proyectos
                WHERE ca_tipos_proyectos.id_tipo_proyecto=$Codigo";
        try {
            $result =$db->prepare($query0);
            $result->execute();
            
            $bandera = false;
            while ($fila = $result->fetch()) {
                $bandera = true;
                break;
            }
            if($bandera){
                $mensaje = "El tipo de proyecto ID.$Codigo no puede ser eliminado ya que hay proyectos vinculados a este.";
                $color = "azul";
            }
            else{
                $result =$db->prepare($query);
                $result->execute();
                $mensaje = "Se ha eliminado el tipo de proyecto ID.$Codigo";
                $color = "verde";
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }else{
        echo mensajes("Ha ocurrido un error del servidor...","verde");
    }
?>
<script type="text/javascript">
    $("#datosTabla").load('pages/CargaAcademica/TiposProyectos/DatosTipoProyecto.php');
</script>