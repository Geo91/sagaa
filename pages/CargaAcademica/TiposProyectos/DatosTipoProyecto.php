<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query1 = "SELECT * FROM `ca_tipos_proyectos`";
?>
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaTiposDiscapacidades" class="table table-bordered table-striped">
            <thead>
                <tr>                                            
                    <th style="text-align:center;background-color:#386D95;color:white;display:none">Código</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Editar</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Eliminar</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $result =$db->prepare($query1);
                $result->execute();
                while ($fila = $result->fetch()) {
                    ?>
                    <tr data-id='<?php echo $fila["id_tipo_proyecto"]; ?>'>
                        <td style = "display:none"><?php echo $fila["id_tipo_proyecto"]; ?></td>
                        <td><?php echo $fila["nombre"]; ?></td>
                        <td>
                            <center>
                                <button type="button" class="editarTipoProyecto btn btn-info glyphicon glyphicon-edit"  title="Editar Tipo de Proyecto">
                            </center>
                        </td>       
                        <td>
                            <center>
                                <button type="button" class="eliminarTipoProyecto<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-remove"  title="Eliminar Tipo de Proyecto">
                            </center>
                        </td>     
                    </tr>
            <?php } ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div>
<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#tablaTiposDiscapacidades')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaTiposDiscapacidades').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
$(document).on("click", ".eliminarTipoProyecto<?php echo $auto_increment; ?>", function () {
    if (confirm("¿Está seguro de que desea eliminar este tipo de proyecto?")){
        codigo = $(this).parents("tr").data("id");
        data={
                    Codigo:codigo
            }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: eliminarTiposProyecto,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    }
});

</script>