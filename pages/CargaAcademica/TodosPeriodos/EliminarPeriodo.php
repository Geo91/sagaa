<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");
    if (isset($_POST['Codigo']) ){
        
        $Codigo = $_POST['Codigo'];
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query = "DELETE FROM ca_periodos WHERE ca_periodos.id_periodo=$Codigo;
                DELETE FROM ca_cargas WHERE ca_cargas.id_periodo=$Codigo;
                DELETE FROM ca_carga_profesores WHERE ca_carga_profesores.id_periodo=$Codigo;";

        try{

            $result =$db->prepare($query);
            $result->execute();
            ?>
            <script type="text/javascript">
                $("#datosTabla").load('pages/CargaAcademica/TodosPeriodos/DatosPeriodos.php');
            </script>
            <?php

            echo mensajes("El la programación para el periodo ha sido eliminada.","verde");
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>
<script type="text/javascript">
    $("#modalTempPeriodo").modal('hide');
</script>