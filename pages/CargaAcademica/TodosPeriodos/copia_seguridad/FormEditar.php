<?php  

	$maindir = "../../../";
	require_once($maindir."funciones/check_session.php");
	include($maindir."funciones/timeout.php");

    if (isset($_POST['idPeriodoEdit']) &&
    isset($_POST['proceso']) &&
    isset($_POST['anio']) &&
    isset($_POST['periodo']) &&
    isset($_POST['fechaI']) &&
    isset($_POST['fechaR']) &&
    isset($_POST['fechaM']) &&
    isset($_POST['fechaF']) ) {
        $idPeriodoEdit = $_POST['idPeriodoEdit'];
        $proceso = $_POST['proceso'];
        $anio = $_POST['anio'];
        $periodo = $_POST['periodo'];
        $fechaI = $_POST['fechaI'];
        $fechaR = $_POST['fechaR'];
        $fechaM = $_POST['fechaM'];
        $fechaF = $_POST['fechaF'];
    }

?>
<form class="form-horizontal" role="form" id="formProgramacionEditar" name="formProgramacionEditar"> 
 	<br>
    <input type="hidden" class="form-control" id="idPeriodoEdit" name="idPeriodoEdit" value="<?php echo $idPeriodoEdit; ?>"> 
    <div class="row form-group" >                    
        <label class=" col-sm-2 control-label" >Período </label>                       
        <div class="col-sm-8">                   
            <input type="number" id="periodoEditar" name="periodoEditar" min="1" max="3" value="<?php echo $periodo; ?>" required
            <?php if ($proceso>0) {
                echo " disabled='true' ";
            } ?>>
        </div>
    </div>

    <div class="row form-group" >                    
        <label class=" col-sm-2 control-label" >Año </label>
        <div class="col-sm-8">                   
            <input type="number" id="anioEditar" name="anioEditar" min="<?php echo date("Y"); ?>" max="2100" value="<?php echo $anio; ?>" required
            <?php if ($proceso>0) {
                echo " disabled='true' ";
            } ?>>
        </div>                  
    </div>
	<hr>
	<div class="row form-group" > 
    	<label class="col-sm-8 control-label" >Fechas de inicio de cada proceso:</label>
    </div>
    <hr>
		<div class="row form-group" >                    
        <label class=" col-sm-4 control-label" >Ingreso de cargas</label>
        <div class="col-sm-8">  
			<p> <input type="date" id="fechaInicioEditar" placeholder="aaaa-mm-dd" required value="<?php echo $fechaI; ?>" 
            <?php if ($proceso>0) {
                echo " disabled='true' ";
            } ?>></p>
        </div>                  
    </div>
    <div class="row form-group" >                    
        <label class=" col-sm-4 control-label" >Revisión</label>
        <div class="col-sm-8">  
            <p> <input type="date" id="fechaRevisionEditar" placeholder="aaaa-mm-dd" required value="<?php echo $fechaR; ?>"
            <?php if ($proceso>1) {
                echo " disabled='true' ";
            } ?>></p>
        </div>  
    </div>               
    <div class="row form-group" >                    
        <label class=" col-sm-4 control-label" >Modificación</label>
        <div class="col-sm-8">  
            <p> <input type="date" id="fechaModificaciónEditar" placeholder="aaaa-mm-dd" required value="<?php echo $fechaM; ?>"
            <?php if ($proceso>2) {
                echo " disabled='true' ";
            } ?>></p>
        </div>   
    </div>                           

		<div class="row form-group" >                    
        <label class=" col-sm-4 control-label" >Finalización</label>
        <div class="col-sm-8">  
			<p> <input type="date" id="fechaFinEditar" placeholder="aaaa-mm-dd" required value="<?php echo $fechaF; ?>"
            <?php if ($proceso>3) {
                echo " disabled='true' ";
            } ?>></p>
        </div>                  
    </div>

    <div class="modal-footer">
        <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button  id="guardarCambios" class="btn btn-primary" ><span class=" glyphicon glyphicon-floppy-disk"></span>Guardar</button>
    </div> 
</form>
<script type="text/javascript">
    $(document).ready(function(){   

//para cuando se da submit al formulario formProgramacion
    $("#formProgramacionEditar").submit(function(e) {
        e.preventDefault();
        var fechaInicio=$('#fechaInicioEditar').val();
        var fechaRevision=$('#fechaRevisionEditar').val();
        var fechaModificación=$('#fechaModificaciónEditar').val();
        var fechaFin=$('#fechaFinEditar').val();
        var fehcaR=false;
        var fehcaM=false;
        var fehcaF=false;
        if (fechaInicio>=fechaRevision){
            fehcaR=true;
        }
        if (fechaRevision>=fechaModificación){
            fehcaM=true;
        }
        if (fechaModificación>=fechaFin){
            fehcaF=true;
        }
        if (fehcaR || fehcaM || fehcaF){
            alert("Cada fecha debe ser mayor a la fecha del proceso anterior.");
        }else{
            data={
                        idPeriodoEdit:$('#idPeriodoEdit').val(),
                        periodo:$('#periodoEditar').val(),
                        anio:$('#anioEditar').val(),
                        fechaInicio:$('#fechaInicioEditar').val(),
                        fechaRevision:$('#fechaRevisionEditar').val(),
                        fechaModificación:$('#fechaModificaciónEditar').val(),
                        fechaFin:$('#fechaFinEditar').val()
                    }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                beforeSend: inicioEnvio,
                success: actualizarPeriodo,
                timeout: 4000,
                error: problemas
            }); 
            return false;
        }
    });
});

</script>