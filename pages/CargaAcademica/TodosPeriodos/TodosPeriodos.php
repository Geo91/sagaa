<?php  

	$maindir = "../../../";
	require_once($maindir."funciones/check_session.php");
	include($maindir."funciones/timeout.php");
?>
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong>  Historial de Programación de Registro Cargas </strong></center></h3>
    </div>
</div> 
<div id="divRespuesta">
</div>
<div id="datosTabla">
    <?php require_once("DatosPeriodos.php") ?>
</div>

<div class="modal fade" id="modalTempPeriodo" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">

        <div class="modal-header" style = "background-color:#0FA6C3">
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
		  <h4 class="modal-title" style = "color:white">Editar programación</h4>
		</div>
		<div class="modal-body">
			 <div id="subModalPeriodo">         
             </div>
		</div>
      </div>
    </div>
</div>

<script type="text/javascript" src="pages/CargaAcademica/TodosPeriodos/Script.js"></script>