<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");
    if (isset($_POST['idPeriodoEdit']) && 
        isset($_POST['periodo']) && 
        isset($_POST['anio']) && 
        isset($_POST['fechaInicio']) && 
        isset($_POST['fechaRevision']) && 
        isset($_POST['fechaModificación']) && 
        isset($_POST['fechaFin']) ){
        
        $idPeriodoEdit = $_POST['idPeriodoEdit'];
        $periodo = $_POST['periodo'];
        $anio = $_POST['anio'];
        $fechaInicio = $_POST['fechaInicio'];
        $fechaRevision = $_POST['fechaRevision'];
        $fechaModificación = $_POST['fechaModificación'];
        $fechaFin = $_POST['fechaFin'];
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query0 = "SELECT * FROM `ca_periodos` WHERE `periodo`=$periodo and `anio`=$anio and `id_periodo`<>$idPeriodoEdit";
        $query1 = "SELECT * FROM `ca_periodos` 
                WHERE ((ca_periodos.fecha_ingreso_i BETWEEN '$fechaInicio' and '$fechaFin') 
                    or (ca_periodos.fecha_ingreso_f BETWEEN '$fechaInicio' and '$fechaFin') 
                    or ('$fechaInicio' BETWEEN ca_periodos.fecha_ingreso_i and ca_periodos.fecha_ingreso_f))
                    and ca_periodos.id_periodo<>$idPeriodoEdit";
        $query = "UPDATE ca_periodos
                set ca_periodos.periodo=$periodo,
                    ca_periodos.anio=$anio,
                    ca_periodos.fecha_ingreso_i='$fechaInicio',
                    ca_periodos.fecha_revision='$fechaRevision',
                    ca_periodos.fecha_modificacion='$fechaModificación',
                    ca_periodos.fecha_ingreso_f='$fechaFin'
                WHERE ca_periodos.id_periodo=$idPeriodoEdit";

        try{
            $bandera = 0;
            //validacion de periodos
            $result =$db->prepare($query0);
            $result->execute();
            while ($fila = $result->fetch()) {
                $bandera = 1;
                break;
            }
            //validacion de fechas
            $result =$db->prepare($query1);
            $result->execute();
            while ($fila = $result->fetch()) {
                $bandera = 2;
                break;
            }

            if($bandera==1){
                $mensaje = "Ya existe el periodo para la carga $periodo-$anio";
                $color = "azul";
            }
            elseif($bandera==2){
                $mensaje = "Las fechas de inicio y fin de periodo de ingreso interfieren con el período de otra carga académica";
                $color = "azul";
            
            }else{
                $result =$db->prepare($query);
                $result->execute();

                $mensaje = "Periodo editado";
                $color = "verde";
                ?>
                <script type="text/javascript">
                    $("#datosTabla").load('pages/CargaAcademica/TodosPeriodos/DatosPeriodos.php');
                </script>
                <?php
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>
<script type="text/javascript">
    $("#modalTempPeriodo").modal('hide');
</script>