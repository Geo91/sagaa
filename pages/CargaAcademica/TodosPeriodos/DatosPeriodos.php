<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query1 = "SELECT ca_periodos.id_periodo,
    ca_periodos.periodo,
    ca_periodos.anio,
    ca_periodos.fecha_ingreso_i,
    ca_periodos.fecha_revision,
    ca_periodos.fecha_modificacion,
    ca_periodos.fecha_ingreso_f,
    (CASE WHEN ca_periodos.fecha_ingreso_f<=CURDATE() THEN
        4
    WHEN ca_periodos.fecha_modificacion<=CURDATE() THEN
        3
    WHEN ca_periodos.fecha_revision<=CURDATE() THEN
        2
    WHEN ca_periodos.fecha_ingreso_i<=CURDATE() THEN
        1
    ELSE 0 END) as proceso
FROM ca_periodos
ORDER BY ca_periodos.anio DESC,ca_periodos.periodo ASC";
?>
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaPeriodos" class="table table-bordered table-striped">
            <thead>
                <tr>                                            
                    <th style="text-align:center;background-color:#386D95;color:white;">Año</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Periodo</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Ingreso</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Revisión</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Modificación</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Finalización</th>
                <?php if($_SESSION['user_rol']!=30){  ?>
                    <th style="text-align:center;background-color:#386D95;color:white;">Editar</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Eliminar</th>
                <?php } ?>
                </tr>
            </thead>
            <tbody>
            <?php
                $result =$db->prepare($query1);
                $result->execute();
                while ($fila = $result->fetch()) {
                    ?>
                    <tr data-id='<?php echo $fila["id_periodo"]; ?>'
                        data-proceso='<?php echo $fila["proceso"]; ?>'>
                        <td><?php echo $fila["anio"]; ?></td>
                        <td><?php echo $fila["periodo"]; ?></td>
                        <td><?php echo $fila["fecha_ingreso_i"]; ?></td>
                        <td><?php echo $fila["fecha_revision"]; ?></td>
                        <td><?php echo $fila["fecha_modificacion"]; ?></td>
                        <td><?php echo $fila["fecha_ingreso_f"]; ?></td>
                    <?php if($_SESSION['user_rol']!=30){  ?>
                        <td>
                            <center>
                                <button type="button" class="EditarPeriodo<?php echo $auto_increment; ?> btn btn-info glyphicon glyphicon-pencil"  title="Editar periodo"
                                <?php  if($fila["proceso"]>3){
                                    echo " disabled='true' ";
                                }?>>
                            </center>
                        </td>
                        <td>
                            <center>     
                                <button type="button" class="EliminarPeriodo<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-remove"  title="Eliminar periodo" 
                                <?php  if($fila["proceso"]>0){
                                    echo " disabled='true' ";
                                }?>>
                            </center>
                        </td> 
                    <?php } ?>    
                    </tr>
            <?php } ?>
            </tbody>

        </table>
    </div><!-- /.box-body -->
</div>
<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#tablaPeriodos')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaPeriodos').dataTable({
        "order": [[0, "desc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
$(document).on("click", ".EditarPeriodo<?php echo $auto_increment; ?>", function () {
    data={
                idPeriodoEdit:$(this).parents("tr").data("id"),
                proceso:$(this).parents("tr").data("proceso"),
                anio:$(this).parents("tr").find("td").eq(0).html(),
                periodo:$(this).parents("tr").find("td").eq(1).html(),
                fechaI:$(this).parents("tr").find("td").eq(2).html(),
                fechaR:$(this).parents("tr").find("td").eq(3).html(),
                fechaM:$(this).parents("tr").find("td").eq(4).html(),
                fechaF:$(this).parents("tr").find("td").eq(5).html()
        }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: abrirEditarPeriodo,
        timeout: 4000,
        error: problemas
    }); 
    return false;
});

$(document).on("click", ".EliminarPeriodo<?php echo $auto_increment; ?>", function () {
    if (confirm("¿Está seguro de que desea eliminar esta programación?")){
        codigo = $(this).parents("tr").data("id");
        data={
                    Codigo:codigo
            }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: eliminarPeriodo,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    }
});

</script>