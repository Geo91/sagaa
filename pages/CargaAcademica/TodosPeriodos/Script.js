
function inicioEnvio(){
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function abrirEditarPeriodo(){
    $("#subModalPeriodo").load('pages/CargaAcademica/TodosPeriodos/FormEditar.php',data);
    $("#modalTempPeriodo").modal('show');
}

function eliminarPeriodo(){
    $("#divRespuesta").load('pages/CargaAcademica/TodosPeriodos/EliminarPeriodo.php',data);
}

function actualizarPeriodo(){
    $("#divRespuesta").load('pages/CargaAcademica/TodosPeriodos/ActualizarPeriodo.php',data);
}
