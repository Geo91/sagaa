
$(document).ready(function(){   
    $("#panelRegistro").click(function(event) {
        event.preventDefault();
        $('#flechaRegistro').toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
    });
    $("#panelCarga").click(function(event) {
        event.preventDefault();
        $('#flechaCarga').toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
    });
    $("#panelMensaje").click(function(event) {
        event.preventDefault();
        $('#flechaMensaje').toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
    });
});

$(document).on("click", ".accesibilidad", function () {
    data1={
            id:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: abrirAccesibilidad,
        timeout: 4000,
        error: problemas
    }); 

    $("#modalSubTabla").modal('show');
    return false;
});

$(document).on("click", ".Acondicionamiento", function () {
    data3={
            id:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: abrirAcondicionamiento,
        timeout: 4000,
        error: problemas
    }); 

    $("#modalSubTabla").modal('show');
    return false;
});


function inicioEnvio(){
    $("#divRespuesta").html('Cargando...');
}

function problemas(){
    $("#divRespuesta").html('Problemas en el servidor.');
}

function abrirAccesibilidad(){
    $("#subTabla").load('pages/CargaAcademica/GestionCargas/DatosAccesibilidad.php',data1);
}

function abrirAcondicionamiento(){
    $("#subTabla").load('pages/CargaAcademica/GestionCargas/DatosAcondicionamiento.php',data3);
}

function eliminarSeccion(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/GestionCargas/EliminarSeccion.php',data);
}

function editarSeccion(){
    $("#subModalTemp").load('pages/CargaAcademica/GestionCargas/AgregarSeccionEditar.php',data);
    $("#modalTemp").modal('show');
}

function enviarMensaje(){
    data={
            idDepto:$('#idDepto').val(),
            obserbacion:$('#obserbacion').val()
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: guardarObservacion,
        timeout: 4000,
        error: problemas
    }); 

}

function guardarObservacion(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/GestionCargas/GuardarObservacion.php',data);
}