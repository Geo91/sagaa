<?php
    $maindir = "../../../";
    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    $tempUltP = $_SESSION['ultimoPeriodo'];
    $tempUltPID = $tempUltP->getId();
    $procesoActualOBS = $tempUltP->getProceso();
    $idDepto = $_SESSION['Id_depto_user'];

    $query = "SELECT usuario.id_Usuario,
                usuario.Id_Rol,
                usuario.nombre,
                ca_observaciones_cargas.Hora,
                ca_observaciones_cargas.Fecha,
                (CASE WHEN ca_observaciones_cargas.Fecha=curdate() THEN
                    1
                 ELSE 0 end) as UsarHora,
                 ca_observaciones_cargas.Observacion
            FROM ca_observaciones_cargas,usuario,ca_cargas
            WHERE ca_observaciones_cargas.id_Usuario=usuario.id_Usuario
                and ca_observaciones_cargas.id_carga=ca_cargas.id_carga
                and ca_cargas.id_periodo=$tempUltPID
                and ca_cargas.id_departamento=$idDepto
            ORDER BY ca_observaciones_cargas.Fecha desc,ca_observaciones_cargas.Hora desc";
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
?>
<input type="hidden" class="form-control" id="idDepto" name="idDepto" value="<?php echo $idDepto; ?>">
<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo1">
      <div class="panel-title">
        <a id="panelMensaje" data-toggle="collapse" data-parent="#accordion" href="#collapseThreeMensajes" aria-expanded="true" aria-controls="collapseThree">
          <span id="flechaMensaje" class="glyphicon glyphicon-chevron-<?php if ($procesoActualOBS<4) { echo " up "; }else{ echo "down"; }?>"></span> Observaciones
        </a>
      </div>
    </div>
    <div id="collapseThreeMensajes" 
    class="panel-collapse collapse <?php if ($procesoActualOBS<4) { echo " in "; }?>" role="tabpanel" aria-labelledby="headingTwo1" aria-expanded="true">
        <div class="panel-body">

            <div class="message-wrap ">


            <?php if ($procesoActualOBS<4) { 
                $tempPoss = ""; 
                ?>

                <div class="col-sm-4">
                    <label>Agregar observación</label>
                    <textarea id="obserbacion" class="form-control send-message" rows="3" placeholder="Escriba un mensaje..." ></textarea>
                    <div class="btn-panel">
                        <a href="javascript:enviarMensaje();" class=" text-right btn   send-message-btn pull-right" role="button"><i class="fa fa-plus"></i> Enviar</a>
                    </div>
                </div>

            <?php }else{
                $tempPoss = " col-sm-offset-2 ";
            } ?>


                <div class="msg-wrap <?php echo $tempPoss; ?> col-sm-8 well" id="mensajes" style="width: 500; height: 210px; overflow:auto;">
                    
                    <?php 
                    $result =$db->prepare($query);
                    $result->execute();
                    $ExistMensajes = false;
                    while ($fila = $result->fetch()) {
                        $ExistMensajes = true;
                        $fila['id_Usuario']
                    ?>
                         <div class='
                         <?php if($fila['Id_Rol']==50 ||
                                $fila['Id_Rol']==100 ){
                                echo " alert alert-info ";
                            } ?> media msg' align="center" >
                            <div class='media-body'>
                                <small class='time'>
                                    <i class='fa fa-clock-o'></i>
                                     <?php if($fila['UsarHora']==1){
                                            echo substr($fila['Hora'], 0,-3);
                                        }else{
                                            echo fecha($fila['Fecha']);
                                        } ?>
                                </small>
                                <h5 class='media-heading'>USUARIO: 
                                    <?php echo $fila['nombre']; ?>
                                </h5>
                                <small class='col-sm-12'>
                                    <?php echo $fila['Observacion']; ?>
                                </small>
                            </div>
                        </div>
                    <?php 
                    } 
                    if (!$ExistMensajes) {
                    ?>
                         <div class='alert alert-warning media msg' align="center" >
                            <div class='media-body'>
                                <small class='col-sm-12'>No hay observaciones.
                                </small>
                            </div>
                        </div>
                    <?php
                    }
                    ?>



                </div>

            </div>
        </div>
    </div>
</div>