<?php
    $maindir = "../../../";
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO

    if(isset($_POST['id'])){
        $id = $_POST['id'];
        $query1 = "SELECT ca_tipos_discapacidades.id_discapacidad as id,
    ca_tipos_discapacidades.nombre as nombre
FROM ca_aulas,ca_aulas_discapacidades,ca_tipos_discapacidades
WHERE ca_aulas.id_aula= ca_aulas_discapacidades.id_aula
    and ca_aulas_discapacidades.id_discapacidad=ca_tipos_discapacidades.id_discapacidad
    and ca_aulas.id_aula=$id";
?>
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaAulas" class="table table-bordered table-striped">
            <thead>
                <tr>                                            
                    <th style="text-align:center;background-color:#386D95;color:white;">Id</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Tipo accesibilidad</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $result =$db->prepare($query1);
                $result->execute();
                while ($fila = $result->fetch()) {
                    ?>
                    <tr data-id='<?php echo $fila["id"]; ?>'>
                        <td><?php echo $fila["id"]; ?></td>
                        <td><?php echo $fila["nombre"]; ?></td>
                    </tr>
            <?php } ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div>
<div class="modal-footer">
    <button type="button"  class="btn btn-default" data-dismiss="modal">Salir</button>
</div>
<?php
    }else{
        echo mensajes("Usted no tiene acceso a este formulario","rojo");
    }
?>