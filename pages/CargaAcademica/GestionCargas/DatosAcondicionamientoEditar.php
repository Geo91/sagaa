<?php
    $maindir = "../../../";
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO

    if(isset($_POST['id'])){
        $id = $_POST['id'];
        $query1 = "SELECT ca_tipos_acondicionamientos.id_acondicionamiento as id, ca_tipos_acondicionamientos.nombre as nombre
FROM ca_aulas,ca_aula_acondicionamientos,ca_tipos_acondicionamientos
where ca_aulas.id_aula=ca_aula_acondicionamientos.id_aula
    and ca_aula_acondicionamientos.id_acondicionamiento = ca_tipos_acondicionamientos.id_acondicionamiento
    and ca_aulas.id_aula = $id";
?>
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaAulas" class="table table-bordered table-striped">
            <thead>
                <tr>                                            
                    <th>Id</th>
                    <th>Tipo acondicionamiento</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $result =$db->prepare($query1);
                $result->execute();
                while ($fila = $result->fetch()) {
                    ?>
                    <tr data-id='<?php echo $fila["id"]; ?>'>
                        <td><?php echo $fila["id"]; ?></td>
                        <td><?php echo $fila["nombre"]; ?></td>
                    </tr>
            <?php } ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div>
<div class="modal-footer">
    <button id="SalirMod" type="button"  class="btn btn-default" >Salir</button>
</div>
<?php
    }else{
        echo mensajes("Usted no tiene acceso a este formulario","rojo");
    }
?>
<script type="text/javascript">
    $(document).ready(function(){   
        $("#SalirMod").click(function(event) {
            event.preventDefault();
            $("#modalSubTablaEditar").modal('hide');
        });
    });
</script>