<?php
    $maindir = "../../../";

    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['idDepto']) && 
    isset($_POST['obserbacion']) ){
        
        $tempUltP = $_SESSION['ultimoPeriodo'];
        $tempUltPID = $tempUltP->getId();
        $idDepto = $_POST['idDepto'];
        $observacion = nl2br($_POST['obserbacion']);
        $idUsuarioObs = $_SESSION['user_id'];
        
        try{
            $query = "INSERT INTO `ca_observaciones_cargas` (`id_Usuario`, `id_carga`, `Observacion`, `Fecha`, `Hora`) VALUES ('$idUsuarioObs', (SELECT ca_cargas.id_carga
                from ca_cargas,ca_periodos
                WHERE ca_cargas.id_periodo=ca_periodos.id_periodo
                    and ca_cargas.id_departamento=$idDepto
                    and ca_periodos.id_periodo=$tempUltPID), '$observacion', curdate(), curtime());";

            if ($observacion!=''){
                $result =$db->prepare($query);
                $result->execute();
                ?>
                <script type="text/javascript">
                    $("#Observaciones").load('pages/CargaAcademica/GestionCargas/Observaciones.php');
                </script>
                <?php
            }

        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
          
    }else{
        echo mensajes("Problemas de conexión intentelo de nuevo:","azul");
    }
?>