<?php
    $maindir = "../../../";

    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['id_carga']) && 
    isset($_POST['Codigo']) && 
    isset($_POST['Asignatura']) && 
    isset($_POST['Cupos']) && 
    isset($_POST['Profesor']) && 
    isset($_POST['id_aula']) &&
    isset($_POST['HoraI']) && 
    isset($_POST['HoraF']) &&
    isset($_POST['Lunes']) &&
    isset($_POST['Martes']) &&
    isset($_POST['Miercoles']) &&
    isset($_POST['Jueves']) &&
    isset($_POST['Viernes']) &&
    isset($_POST['Sabado']) &&
    isset($_POST['Domingo']) ){ //if de verificación de parámetros
        
        $Disponible=true;
        $id_carga = $_POST['id_carga'];
        $Codigo = $_POST['Codigo'];
        $Asignatura = $_POST['Asignatura'];
        $Cupos = $_POST['Cupos'];
        $Profesor = $_POST['Profesor'];
        $id_aula = $_POST['id_aula'];
        $HoraI = $_POST['HoraI'];
        $HoraF = $_POST['HoraF'];
        $Lunes = $_POST['Lunes'];
        $Martes = $_POST['Martes'];
        $Miercoles = $_POST['Miercoles'];
        $Jueves = $_POST['Jueves'];
        $Viernes = $_POST['Viernes'];
        $Sabado = $_POST['Sabado'];
        $Domingo = $_POST['Domingo'];

        if (isset($_SESSION['infoPeriodo'])){ //if de validación de datos de período de carga académica

            $consulta = "SELECT No_Empleado from ca_profesores where ca_profesores.No_Empleado = '$Profesor' and ca_profesores.id_contrato in (SELECT ca_tipos_contratos.id_contrato from ca_tipos_contratos WHERE ca_tipos_contratos.horaIntermedia = 1);";
            $resultado =$db->prepare($consulta);
            $resultado->execute();
            $horaIntermedia=false;
            while ($filas = $resultado->fetch()) {
                $horaIntermedia=true;
                break;
            }

            try{
                $tempPeriodo = $_SESSION['infoPeriodo'];
                $idPeriodo = $tempPeriodo->getId();

                $query = "SELECT ca_secciones.No_Empleado, ca_secciones.Hora_inicio,ca_secciones.Hora_fin,ca_dias.nombre
                    FROM ca_secciones,ca_cargas,ca_secciones_dias,ca_dias
                    WHERE ca_secciones.id_seccion=ca_secciones_dias.id_seccion
                        and ca_secciones_dias.id_dia=ca_dias.id_dia
                        and ca_secciones.id_carga=ca_cargas.id_carga
                        and ca_cargas.id_periodo=$idPeriodo
                        and ca_secciones.No_Empleado='$Profesor' ";

                //if($horaIntermedia == false){
                    $query = $query."and ('$HoraI' BETWEEN ADDTIME(ca_secciones.Hora_inicio, '00:01:00') 
                                            and SUBTIME(ca_secciones.Hora_fin, '00:01:00')
                            or '$HoraF' BETWEEN ADDTIME(ca_secciones.Hora_inicio, '00:01:00') 
                                            and SUBTIME(ca_secciones.Hora_fin, '00:01:00')
                            or ca_secciones.Hora_inicio BETWEEN ADDTIME('$HoraI', '00:01:00')
                                            and SUBTIME('$HoraF','00:01:00'))
                        and (";
                /*}else{
                    $query = $query."and ('$HoraI' BETWEEN ca_secciones.Hora_inicio 
                                            and ca_secciones.Hora_fin
                            or '$HoraF' BETWEEN ca_secciones.Hora_inicio 
                                            and ca_secciones.Hora_fin
                            or ca_secciones.Hora_inicio BETWEEN '$HoraI'
                                            and '$HoraF')
                        and (";
			$query = $query." and ("; 
                }*/
                        
                $logico="";
                if($Lunes=="true"){
                    $query=$query.$logico." ca_dias.nombre='Lunes' ";
                    $logico=" or ";
                }
                if($Martes=="true"){
                    $query=$query.$logico." ca_dias.nombre='Martes' ";
                    $logico=" or ";
                }
                if($Miercoles=="true"){
                    $query=$query.$logico." ca_dias.nombre='Miércoles' ";
                    $logico=" or ";
                }
                if($Jueves=="true"){
                    $query=$query.$logico." ca_dias.nombre='Jueves' ";
                    $logico=" or ";
                }
                if($Viernes=="true"){
                    $query=$query.$logico." ca_dias.nombre='Viernes' ";
                    $logico=" or ";
                }
                if($Sabado=="true"){
                    $query=$query.$logico." ca_dias.nombre='Sábado' ";
                    $logico=" or ";
                }
                if($Domingo=="true"){
                    $query=$query.$logico." ca_dias.nombre='Domingo' ";
                    //$logico=" or ";
                }
                $query = $query." )";

                $result =$db->prepare($query);
                $result->execute();
                while ($fila = $result->fetch()) {
                    $Disponible=false;
                    break;
                }
            } catch (Exception $e) {
                echo mensajes($e->getMessage(),"rojo");
            }
            
            if ($Disponible) {
                $AulaTomada = false;
                try{
                    $queryAula = "SELECT ca_aulas.id_aula
                        from ca_secciones,ca_cargas,ca_periodos,ca_secciones_dias,ca_dias,ca_aulas
                        where ca_secciones.id_seccion=ca_secciones_dias.id_seccion
                            and ca_secciones_dias.id_dia=ca_dias.id_dia
                            and ca_secciones.id_aula=ca_aulas.id_aula
                            and ca_secciones.id_carga=ca_cargas.id_carga 
                            and ca_cargas.id_periodo = ca_periodos.id_periodo
                            and ca_aulas.id_aula=$id_aula
                            and ca_periodos.id_periodo=(select ca_cargas.id_periodo from ca_cargas where ca_cargas.id_carga=$id_carga)
                            and (('$HoraI' >= ca_secciones.Hora_inicio 
                                        and '$HoraI' < ca_secciones.Hora_fin)
                                    or ('$HoraF' > ca_secciones.Hora_inicio 
                                        and '$HoraF' <= ca_secciones.Hora_fin)
                                    or (ca_secciones.Hora_inicio > '$HoraI'
                                        and ca_secciones.Hora_inicio < '$HoraF'))
                            and (";
                                        $logico="";
                                        if($Lunes=="true"){
                                            $queryAula=$queryAula.$logico." ca_dias.nombre='Lunes' ";
                                            $logico=" or ";
                                        }
                                        if($Martes=="true"){
                                            $queryAula=$queryAula.$logico." ca_dias.nombre='Martes' ";
                                            $logico=" or ";
                                        }
                                        if($Miercoles=="true"){
                                            $queryAula=$queryAula.$logico." ca_dias.nombre='Miércoles' ";
                                            $logico=" or ";
                                        }
                                        if($Jueves=="true"){
                                            $queryAula=$queryAula.$logico." ca_dias.nombre='Jueves' ";
                                            $logico=" or ";
                                        }
                                        if($Viernes=="true"){
                                            $queryAula=$queryAula.$logico." ca_dias.nombre='Viernes' ";
                                            $logico=" or ";
                                        }
                                        if($Sabado=="true"){
                                            $queryAula=$queryAula.$logico." ca_dias.nombre='Sábado' ";
                                            $logico=" or ";
                                        }
                                        if($Domingo=="true"){
                                            $queryAula=$queryAula.$logico." ca_dias.nombre='Domingo' ";
                                            $logico=" or ";
                                        }
                                        $queryAula = $queryAula.")";

                    $result =$db->prepare($queryAula);
                    $result->execute();
                    while ($fila = $result->fetch()) {
                        $AulaTomada=true;
                        break;
                    }
                } catch (Exception $e) {
                    echo mensajes($e->getMessage(),"rojo");
                }

                if ($AulaTomada){
                    echo mensajes("El aula seleccionada ha sido tomada por alguien más","azul");
                }else{
                // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
                    $query0 = "SELECT * FROM `ca_secciones` WHERE `codigo_seccion`='$Codigo' and `id_carga`=$id_carga and `id_asignatura`='$Asignatura'";
                    $query = "INSERT INTO `ca_secciones` (`id_seccion`, `codigo_seccion`, `id_carga`, `id_asignatura`, `cupos_max`, `matriculados`, `No_Empleado`, `id_aula`, `Hora_inicio`, `Hora_fin`) VALUES (NULL, '$Codigo', '$id_carga', '$Asignatura', '$Cupos','0', '$Profesor', '$id_aula', '$HoraI', '$HoraF');";
                    $query1 = "SELECT * FROM ca_carga_profesores
                                WHERE ca_carga_profesores.No_Empleado=$Profesor
                                    and ca_carga_profesores.id_periodo=(SELECT id_periodo FROM ca_cargas WHERE ca_cargas.id_carga=$id_carga)";

                    try{
                        $result =$db->prepare($query0);
                        $result->execute();
                        
                        $bandera = false;
                        while ($fila = $result->fetch()) {
                            $bandera = true;
                            break;
                        }
                        if($bandera){
                            $mensaje = "La sección con codigo `$Codigo` ya existe";
                            $color = "azul";
                        }
                        else{
                            $result =$db->prepare($query);
                            $result->execute();

                            $result =$db->prepare($query1);
                            $result->execute();
                            $cantidad_uv = -1;
                            while ($fila = $result->fetch()) {
                                $cantidad_uv = $fila['cantidad_uv'];
                                break;
                            }
                // -----------------------dias--------------------------
                            if($Lunes=="true"){
                                $queryDia = "INSERT INTO `ca_secciones_dias` (`id_seccion`, `id_dia`) 
                                    VALUES ((SELECT id_seccion FROM `ca_secciones` WHERE `id_asignatura`='$Asignatura' and `id_carga`=$id_carga and `codigo_seccion`='$Codigo'), 
                                            (SELECT id_dia FROM ca_dias  WHERE nombre='Lunes'));";
                                $result =$db->prepare($queryDia);
                                $result->execute();
                            }
                            if($Martes=="true"){
                                $queryDia = "INSERT INTO `ca_secciones_dias` (`id_seccion`, `id_dia`) 
                                    VALUES ((SELECT id_seccion FROM `ca_secciones` WHERE `id_asignatura`='$Asignatura' and `id_carga`=$id_carga and `codigo_seccion`='$Codigo'), 
                                            (SELECT id_dia FROM ca_dias  WHERE nombre='Martes'));";
                                $result =$db->prepare($queryDia);
                                $result->execute();
                            }
                            if($Miercoles=="true"){
                                $queryDia = "INSERT INTO `ca_secciones_dias` (`id_seccion`, `id_dia`) 
                                    VALUES ((SELECT id_seccion FROM `ca_secciones` WHERE `id_asignatura`='$Asignatura' and `id_carga`=$id_carga and `codigo_seccion`='$Codigo'), 
                                            (SELECT id_dia FROM ca_dias  WHERE nombre='Miércoles'));";
                                $result =$db->prepare($queryDia);
                                $result->execute();
                            }
                            if($Jueves=="true"){
                                $queryDia = "INSERT INTO `ca_secciones_dias` (`id_seccion`, `id_dia`) 
                                    VALUES ((SELECT id_seccion FROM `ca_secciones` WHERE `id_asignatura`='$Asignatura' and `id_carga`=$id_carga and `codigo_seccion`='$Codigo'), 
                                            (SELECT id_dia FROM ca_dias  WHERE nombre='Jueves'));";
                                $result =$db->prepare($queryDia);
                                $result->execute();
                            }
                            if($Viernes=="true"){
                                $queryDia = "INSERT INTO `ca_secciones_dias` (`id_seccion`, `id_dia`) 
                                    VALUES ((SELECT id_seccion FROM `ca_secciones` WHERE `id_asignatura`='$Asignatura' and `id_carga`=$id_carga and `codigo_seccion`='$Codigo'), 
                                            (SELECT id_dia FROM ca_dias  WHERE nombre='Viernes'));";
                                $result =$db->prepare($queryDia);
                                $result->execute();
                            }
                            if($Sabado=="true"){
                                $queryDia = "INSERT INTO `ca_secciones_dias` (`id_seccion`, `id_dia`) 
                                    VALUES ((SELECT id_seccion FROM `ca_secciones` WHERE `id_asignatura`='$Asignatura' and `id_carga`=$id_carga and `codigo_seccion`='$Codigo'), 
                                            (SELECT id_dia FROM ca_dias  WHERE nombre='Sábado'));";
                                $result =$db->prepare($queryDia);
                                $result->execute();
                            }
                            if($Domingo=="true"){
                                $queryDia = "INSERT INTO `ca_secciones_dias` (`id_seccion`, `id_dia`) 
                                    VALUES ((SELECT id_seccion FROM `ca_secciones` WHERE `id_asignatura`='$Asignatura' and `id_carga`=$id_carga and `codigo_seccion`='$Codigo'), 
                                            (SELECT id_dia FROM ca_dias  WHERE nombre='Domingo'));";
                                $result =$db->prepare($queryDia);
                                $result->execute();
                            }
                // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
                            if($cantidad_uv==-1){
                                $query2 = "INSERT INTO `ca_carga_profesores` (`No_Empleado`, `cantidad_uv`, `completa`, `id_periodo`) VALUES ('$Profesor', (SELECT ca_asignaturas.uv FROM ca_asignaturas WHERE ca_asignaturas.id_asignatura='$Asignatura'), '0', (SELECT id_periodo FROM ca_cargas WHERE ca_cargas.id_carga=$id_carga) );";
                            }else{
                                $query2 = "UPDATE ca_carga_profesores set ca_carga_profesores.cantidad_uv = (SELECT ca_asignaturas.uv+$cantidad_uv FROM ca_asignaturas WHERE ca_asignaturas.id_asignatura='$Asignatura') where ca_carga_profesores.No_Empleado=$Profesor                            and ca_carga_profesores.id_periodo in (SELECT id_periodo FROM ca_cargas WHERE ca_cargas.id_carga=$id_carga)";
                            }
                            $result =$db->prepare($query2);
                            $result->execute();

                            $mensaje = "Sección agregada";
                            $color = "verde";


			    $conconpro = "SELECT ca_tipos_contratos.uv FROM ca_tipos_contratos WHERE ca_tipos_contratos.id_contrato = (SELECT ca_profesores.id_contrato FROM ca_profesores WHERE ca_profesores.No_Empleado = '$Profesor')";
                            $Rconconpro = $db->prepare($conconpro);
                            $Rconconpro->execute();

                            while ($filaccp = $Rconconpro->fetch()) {
                                $contratouv = $filaccp['uv'];
                                break;
                            }

                            $conteouvs = "SELECT ca_carga_profesores.cantidad_uv FROM ca_carga_profesores WHERE ca_carga_profesores.No_Empleado = '$Profesor' and ca_carga_profesores.id_periodo in (SELECT id_periodo FROM ca_cargas WHERE ca_cargas.id_carga=$id_carga)";
                            $Rconteouvs = $db->prepare($conteouvs);
                            $Rconteouvs->execute();

                            while ($filacuvs = $Rconteouvs->fetch()) {
                                $uvscontadas = $filacuvs['cantidad_uv'];
                                break;
                            }

                            $verificado = $contratouv - $uvscontadas;

                            if($verificado < 2){
                                $concompletar = "UPDATE ca_carga_profesores SET ca_carga_profesores.completa = '1' WHERE ca_carga_profesores.No_Empleado = '$Profesor' and ca_carga_profesores.id_periodo in (SELECT id_periodo FROM ca_cargas WHERE ca_cargas.id_carga=$id_carga)";

                                $Rcompletar = $db->prepare($concompletar);
                                $Rcompletar -> execute();
                            }

                            ?>
                            <script type="text/javascript">
                                $("#TablaCarga").load('pages/CargaAcademica/GestionCargas/DatosCarga.php');
                            </script>
                            <?php
                        }
                        echo mensajes($mensaje,$color);
                    } catch (Exception $e) { //Fin del Try
                        echo mensajes($e->getMessage(),"rojo");
                    }
                }
            }else{
                //if($horaIntermedia == false){
                    echo mensajes("El empleado No.$Profesor tiene una o más secciones que interfieren con el horario y días seleccionados ","azul");
                /*}else{
                    echo mensajes("El empleado No.$Profesor tiene contrato con hora intermedia entre clases, la sección no puede ser creada en el horario seleccionado", "azul");
                }*/
            }
        }else{ //cierre del if de validación de datos de período de carga académica
            echo mensajes("Problemas con la conexión","azul");
        }
    }else{ //cierre del if de verificación de parámetros
        echo mensajes("Error de conexión intentelo de nuevo:","rojo");
    }
?>