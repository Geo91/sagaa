
$(document).ready(function(){   
    $("#panelRegistro").click(function(event) {
        event.preventDefault();
        $('#flechaRegistro').toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
    });
    $("#panelCarga").click(function(event) {
        event.preventDefault();
        $('#flechaCarga').toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
    });
    $("#panelMensaje").click(function(event) {
        event.preventDefault();
        $('#flechaMensaje').toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
    });
});

$(document).on("click", ".accesibilidad", function () {
    data1={
            id:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: abrirAccesibilidad,
        timeout: 4000,
        error: problemas
    }); 

    $("#modalSubTabla").modal('show');
    return false;
});

$(document).on("click", ".Acondicionamiento", function () {
    data3={
            id:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: abrirAcondicionamiento,
        timeout: 4000,
        error: problemas
    }); 

    $("#modalSubTabla").modal('show');
    return false;
});

$(document).on("change","#depto",function () {
    id = $("#depto").val();            
    obtenerProfesores(id);
    return false;
});

function obtenerProfesores(deptoID){                  
    var datos = 
        {
            id:deptoID
        }; //Array 
        
    $.ajax({
        async: true,
        type: "POST",
        data:datos,
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        url: "pages/CargaAcademica/GestionCargas/obtenerProfesores.php",
        success: function(response){
                                       
            var arr = JSON.parse(response);
            
            var options = '';
            var val='NULL';
            var def='Seleccione una opción';
            options += '<option value="' + val + '" >' +
                            def+ '</option>';
                    
            for (var index = 0; index < arr.length; index++) 
            {
                var No_Empleado = arr[index].No_Empleado;
                var nombre = arr[index].nombre;
                
                options += '<option value="' + No_Empleado + '/' + nombre +'">' +
                            nombre + '</option>';
            }
            
            $("#Profesor").html(options);                                                        
            
        },
        timeout: 6000,    
    });
}

function inicioEnvio(){
    $("#divRespuesta").html('Cargando...');
}

function problemas(){
    $("#divRespuesta").html('Problemas en el servidor.');
}

function abrirAccesibilidad(){
    $("#subTabla").load('pages/CargaAcademica/GestionCargas/DatosAccesibilidad.php',data1);
}

function abrirAcondicionamiento(){
    $("#subTabla").load('pages/CargaAcademica/GestionCargas/DatosAcondicionamiento.php',data3);
}

function eliminarSeccion(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/GestionCargas/EliminarSeccion.php',data);
}

function editarSeccion(){
    $("#subModalTemp").load('pages/CargaAcademica/GestionCargas/AgregarSeccionEditar.php',data);
    $("#modalTemp").modal('show');
}

function enviarMensaje(){
    data={
            idDepto:$('#idDepto').val(),
            obserbacion:$('#obserbacion').val()
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: guardarObservacion,
        timeout: 4000,
        error: problemas
    }); 

}

function guardarObservacion(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/GestionCargas/GuardarObservacion.php',data);
}