<?php
    $maindir = "../../../";

    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['seccionID']) && 
    isset($_POST['Codigo']) ){
        
        $seccionID = $_POST['seccionID'];
        $Codigo = $_POST['Codigo'];
        
        try{
            $query="SELECT (case when (ca_carga_profesores.cantidad_uv-ca_asignaturas.uv)<0 then 0 else (ca_carga_profesores.cantidad_uv-ca_asignaturas.uv)  end )as NuevaUv
                                       FROM ca_secciones,ca_asignaturas,ca_carga_profesores,ca_cargas
                                       WHERE ca_secciones.id_asignatura=ca_asignaturas.id_asignatura
                                           and ca_secciones.id_carga=ca_cargas.id_carga
                                           and ca_secciones.No_Empleado=ca_carga_profesores.No_Empleado
                                           and ca_cargas.id_periodo=ca_carga_profesores.id_periodo
                                           and ca_secciones.id_seccion=$seccionID";

            $result =$db->prepare($query);
            $result->execute();
            $CantUv=-1;
            while ($fila = $result->fetch()) {
                $CantUv=$fila['NuevaUv'];
                break;
            }
            if ($CantUv>-1) {
                $query="UPDATE ca_carga_profesores
                        set ca_carga_profesores.cantidad_uv = $CantUv,
                            ca_carga_profesores.completa=0
                        WHERE ca_carga_profesores.No_Empleado=(SELECT 
                                                ca_secciones.No_Empleado
                                          FROM ca_secciones
                                          WHERE ca_secciones.id_seccion=$seccionID)
                            and ca_carga_profesores.id_periodo=(SELECT 
                                                ca_periodos.id_periodo
                                           from ca_secciones,ca_cargas,ca_periodos
                                           WHERE ca_secciones.id_carga=ca_cargas.id_carga
                                            and ca_cargas.id_periodo=ca_periodos.id_periodo
                                            and ca_secciones.id_seccion=$seccionID)";
                $result =$db->prepare($query);
                $result->execute();
            }
            $query = "DELETE FROM ca_secciones WHERE ca_secciones.id_seccion=$seccionID;
                    DELETE FROM ca_secciones_dias WHERE ca_secciones_dias.id_seccion=$seccionID;";

            $result =$db->prepare($query);
            $result->execute();

            echo mensajes("La sección `$Codigo` ha sido eliminada","verde");
            ?>
            <script type="text/javascript">
                $("#TablaCarga").load('pages/CargaAcademica/GestionCargas/DatosCarga.php');
            </script>
            <?php

        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
          
    }else{
        echo mensajes("Error de conexión intentelo de nuevo:","rojo");
    }
?>