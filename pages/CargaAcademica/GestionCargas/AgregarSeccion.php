<?php
    $maindir = "../../../";
    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."conexion/config.inc.php");
    
    $idU = $_SESSION['user_id'];
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    if(isset($_SESSION['infoCarga'])){
        $tempCarga = $_SESSION['infoCarga'];
        $departamento = $tempCarga->getId_departamento();
        $query = "SELECT * FROM `ca_asignaturas` WHERE `id_departamento`=$departamento
                    and  ca_asignaturas.habilitada = 1 
                    order by nombre";
        $query1 = "SELECT empleado.No_Empleado, 
                    CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombre
                FROM empleado,persona,cargo,empleado_has_cargo
                WHERE empleado.N_identidad=persona.N_identidad
                    and empleado_has_cargo.No_Empleado=empleado.No_Empleado
                    and empleado_has_cargo.ID_cargo=cargo.ID_cargo
                    and empleado.estado_empleado=1
                    and (cargo.Cargo='Docente'
                         or cargo.Cargo='Decana'
                         or cargo.Cargo='Jefe de Departamento'
                         or cargo.Cargo='Coordinador de Maestría'
			or cargo.Cargo = 'Instructor A III')
                    and empleado_has_cargo.Fecha_salida_cargo is null
                    and empleado.No_Empleado in (SELECT ca_profesores.No_Empleado FROM ca_profesores) and empleado.Id_departamento = (SELECT empleado.Id_departamento from empleado where empleado.No_Empleado = (SELECT usuario.No_Empleado from usuario where usuario.id_Usuario = $idU))
                ORDER BY persona.Primer_nombre";
?>

<div class="col-md-9 col-sm-8">
<div class="panel panel-info">
    <div class="panel-heading"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar sección</div>

    <div class="panel-body" >
        <div class="row">
            <div class="col-lg-12">
            <!--************************formulario*********************-->
                <form class="form-horizontal" role="form" id="formAsignatura" name="formAsignatura">

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Sección</label>                       
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control" id="Codigo" name="Codigo" placeholder="Ejmpl: 1601"
                            <?php if (isset($_POST['Codigo'])) {
                                $tempVal = $_POST['Codigo'];
                                echo " value='$tempVal' ";
                            }?>
                             required>              
                        </div>                  
                    </div>
                    
                    <div class="row form-group">
                        <label class=" col-sm-2 control-label" >Asignatura</label>
                        <div class="col-sm-8">                            
                            <select class="form-control" id="Asignatura" name="Asignatura" required>
                                <option value="">Seleccione una opción</option>
                                <?php
                                     $result =$db->prepare($query);
                                     $result->execute();
                                    while ($fila = $result->fetch()) {
                                        ?>
                                        <option value="<?php echo $fila["id_asignatura"].'/'.$fila["nombre"]; ?>"
                                    <?php if (isset($_POST['Asignatura'])) {
                                            $tempVal = $_POST['Asignatura'];
                                            if ($tempVal==$fila["id_asignatura"]){
                                                echo " selected ";
                                            }
                                        }
                                    ?>
                             ><?php echo $fila["nombre"]; ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Cupos máximos</label>                       
                        <div class="col-sm-8">                   
                            <input type="number" id="Cupos" name="Cupos" min="1" max="150" 
                            <?php if (isset($_POST['Cupos'])) {
                                $tempVal = $_POST['Cupos'];
                                echo " value='$tempVal' ";
                            }else{
                                echo " value='1' ";
                                }?> required>
                        </div>                  
                    </div>

                    <div class="row form-group">
                        <label class=" col-sm-2 control-label" > Departamento</label>
                        <div class="col-sm-8">                            
                            <select class="form-control" id="depto" name="depto" required>
                                <option value="">Seleccione una opción</option>
                                <?php
                                    $consulta = "SELECT * from ca_departamentos_carreras;";
                                    $result =$db->prepare($consulta);
                                    $result->execute();
                                    while ($fila = $result->fetch()) {
                                        ?>
                                        <option value="<?php echo $fila["id_departamento"]?>"
                                            <?php 
                                                if ($departamento==$fila["id_departamento"]){
                                                    echo " selected ";
                                                }
                                            ?>
                                        >   <?php echo $fila["nombre"]; ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class=" col-sm-2 control-label" >Profesor</label>
                        <div class="col-sm-8">                            
                            <select class="form-control" id="Profesor" name="Profesor" required>
                                <option value="">Seleccione una opción</option>
                                <?php
                                     $result =$db->prepare($query1);
                                     $result->execute();
                                    while ($fila = $result->fetch()) {
                                        ?>
                                        <option value="<?php echo $fila["No_Empleado"].'/'.$fila["nombre"]; ?>"
                                    <?php if (isset($_POST['Profesor'])) {
                                            $tempVal = $_POST['Profesor'];
                                            if ($tempVal==$fila["No_Empleado"]){
                                                echo " selected ";
                                            }
                                        }
                                    ?> >
                        <?php 
                            $largo = strlen($fila["No_Empleado"]);
                            if ($largo<=7) {
                                $espacio = str_repeat ( "&nbsp", 7-$largo);
                            }else{
                                $espacio=" ";
                            }
                            echo $fila["nombre"]; ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Hora inicio</label>                       
                        <div class="col-sm-8">                   
                            <input type="time" id="HoraI" name="HoraI"  placeholder="Ejem: 16:00"
                            <?php if (isset($_POST['HoraI'])) {
                                $tempVal = $_POST['HoraI'];
                                echo " value='$tempVal' ";
                            }?> required>
                        </div>                  
                    </div>

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Hora fin</label>                       
                        <div class="col-sm-8">                   
                            <input type="time" id="HoraF" name="HoraF" placeholder="Ejem: 17:00"
                            <?php if (isset($_POST['HoraF'])) {
                                $tempVal = $_POST['HoraF'];
                                echo " value='$tempVal' ";
                            }?> required>
                        </div>                  
                    </div>
<!--*****************dias********************* -->
                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Dias </label>                       
                        <div class="col-sm-8">  
                            <div class="col-sm-4" >       
                                <label >
                                    <input id="chkLunes" type="checkbox" 
                                    <?php if (isset($_POST['Lunes'])) {
                                        if ($_POST['Lunes']=="true"){
                                            echo " checked ";
                                        }
                                    }?> /> Lunes
                                </label>   
                            </div>      
                            <div class="col-sm-4" > 
                                <label>
                                    <input id="chkMartes" type="checkbox"
                                    <?php if (isset($_POST['Martes'])) {
                                        if ($_POST['Martes']=="true"){
                                            echo " checked ";
                                        }
                                    }?> /> Martes
                                </label>  
                            </div>   
                            <div class="col-sm-4" > 
                                <label>
                                    <input id="chkMiercoles" type="checkbox"
                                    <?php if (isset($_POST['Miercoles'])) {
                                        if ($_POST['Miercoles']=="true"){
                                            echo " checked ";
                                        }
                                    }?>/> Miércoles
                                </label>  
                            </div>   
                            <div class="col-sm-4" > 
                                <label>
                                    <input id="chkJueves" type="checkbox"
                                    <?php if (isset($_POST['Jueves'])) {
                                        if ($_POST['Jueves']=="true"){
                                            echo " checked ";
                                        }
                                    }?>/> Jueves
                                </label>  
                            </div>   
                            <div class="col-sm-4" > 
                                <label>
                                    <input id="chkViernes" type="checkbox"
                                    <?php if (isset($_POST['Viernes'])) {
                                        if ($_POST['Viernes']=="true"){
                                            echo " checked ";
                                        }
                                    }?>/> Viernes
                                </label>  
                            </div>   
                            <div class="col-sm-4" > 
                                <label>
                                    <input id="chkSabado" type="checkbox"
                                    <?php if (isset($_POST['Sabado'])) {
                                        if ($_POST['Sabado']=="true"){
                                            echo " checked ";
                                        }
                                    }?>/> Sábado
                                </label>  
                            </div>   
                            <div class="col-sm-4" > 
                                <label>
                                    <input id="chkDomingo" type="checkbox"
                                    <?php if (isset($_POST['Domingo'])) {
                                        if ($_POST['Domingo']=="true"){
                                            echo " checked ";
                                        }
                                    }?>/> Domingo
                                </label>  
                            </div>
                        </div>                  
                    </div>
<!--*****************dias********************* -->

                    <div class="row">
                        <label class="control-label col-sm-2"></label>
                        <div class="col-sm-8">
                            <p aling ="right">
                                <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10">Siguiente <span class="glyphicon glyphicon-arrow-right"></span></button> 
                            </p>          
                        </div>                            
                    </div>    

                </form>
            <!--************************formulario*********************-->
            </div>
        </div>
    </div>                                    
</div> 
</div>
<?php
    }else{
        echo mensajes("Usted no tiene acceso a este formulario","rojo");
    }
?>
<script type="text/javascript">
$(document).ready(function(){   

//para cuando se da submit al formulario formAsignatura
    $("#formAsignatura").submit(function(e) {
        e.preventDefault();
        HoraI=$('#HoraI').val();
        HoraF=$('#HoraF').val();
        Lunes=$("#chkLunes").prop('checked');
        Martes=$("#chkMartes").prop('checked');
        Miercoles=$("#chkMiercoles").prop('checked');
        Jueves=$("#chkJueves").prop('checked');
        Viernes=$("#chkViernes").prop('checked');
        Sabado=$("#chkSabado").prop('checked');
        Domingo=$("#chkDomingo").prop('checked');
        if (HoraI>=HoraF){
            alert("La hora de inicio debe de ser menor a la hora final.");
        }else if(!Lunes && !Martes && !Miercoles && !Jueves && !Viernes && !Sabado && !Domingo){
            alert("Debe seleccionar al menos un día de la semana.");
        }else{
            data={
                Codigo:$('#Codigo').val(),
                Asignatura:$('#Asignatura').val(),
                Cupos:$('#Cupos').val(),
                Profesor:$('#Profesor').val(),
                HoraI:$('#HoraI').val(),
                HoraF:$('#HoraF').val(),
                Lunes:Lunes,
                Martes:Martes,
                Miercoles:Miercoles,
                Jueves:Jueves,
                Viernes:Viernes,
                Sabado:Sabado,
                Domingo:Domingo
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                success: agregarAsignatura,
                timeout: 100000,
                error: problemas
            }); 
            return false;
        }
    });
    function agregarAsignatura(){
        $("#divRespuesta").html('');
        $("#contenedorCarga").html('');
        $("#contenedorCarga").load('pages/CargaAcademica/GestionCargas/AgregarAula.php',data);
    }
});
</script>