<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong> Carga Académica </strong></center></h3>
    </div> 
</div>
<div class="row" id="divRespuesta"></div> 
<?php
    $maindir = "../../../";
    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");
    $deptoNombre="";
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
if (isset($_SESSION['Id_depto_user'])) {
    if (isset($_SESSION['ultimoPeriodo'])) {
        $tempultPiD=$_SESSION['ultimoPeriodo']->getId();
        if ($tempultPiD!=-1) {
            $tempPeriodoP = $_SESSION['ultimoPeriodo'];
            $fechaRP = $tempPeriodoP->getFechaR();
            $procesoActualP = $tempPeriodoP->getProceso();
            if(!(isset($_SESSION['infoPeriodo']) && isset($_SESSION['infoCarga'])) ){
                try{
                    $tempIdDepto = $_SESSION['Id_depto_user'];
                    $queryNdepto = "SELECT * FROM `ca_departamentos_carreras`
                                    WHERE `id_departamento`=$tempIdDepto";

                    $result =$db->prepare($queryNdepto);
                    $result->execute();
                    
                    while ($fila = $result->fetch()) {
                        $deptoNombre=$fila['nombre'];
                        $_SESSION['deptoNombre']=$deptoNombre;
                        break;
                    }

                } catch (Exception $e) {
                    echo mensajes($e->getMessage(),"rojo");
                }

                $queryPermiso = "SELECT * FROM ca_modificaciones_cargas,ca_cargas
                WHERE ca_modificaciones_cargas.id_carga=ca_cargas.id_carga
                    and ca_modificaciones_cargas.fechaMod=curdate()
                    and ca_cargas.id_periodo=$tempultPiD
                    and ca_cargas.id_departamento=$tempIdDepto";

                $result =$db->prepare($queryPermiso);
                $result->execute();
                $existePermiso=false;
                while ($fila = $result->fetch()) {
                    $existePermiso=true;
                    $idCargaTemp = $fila['id_carga'];
                    break;
                }
                if($existePermiso){
                    $_SESSION['ultimoPeriodo']->setProceso(3);
                    $_SESSION['infoPeriodo']=$_SESSION['ultimoPeriodo'];
                    $_SESSION['infoCarga'] = new Carga($idCargaTemp,0,$tempIdDepto,$deptoNombre);
                }
            }

            if(isset($_SESSION['infoPeriodo']) && isset($_SESSION['infoCarga'])){
                $tempCarga = $_SESSION['infoCarga'];
                $tempPeriodo = $_SESSION['infoPeriodo'];
                $departamento = $tempCarga->getId_departamento();
                $id_carga = $tempCarga->getId();
                $_SESSION['id_carga'] = $id_carga;
                $periodo = $tempPeriodo->getPeriodo();
                $procesoActual = $tempPeriodo->getProceso();
                $anio = $tempPeriodo->getAnio();
                $deptoNombre = $tempCarga->getNombre_departamento();
                $_SESSION['deptoNombre']=$deptoNombre;
                $fechaI = $tempPeriodo->getFechaI();
                $fechaR = $tempPeriodo->getFechaR();
                $fechaM = $tempPeriodo->getFechaM();
                $fechaF = $tempPeriodo->getFechaF();
                $aprobado = $tempCarga->getAprobado();
?>

<?php if ($procesoActual==1 || $procesoActual==3) { ?>
<div class="row">
    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingTwo">
          <h4 class="panel-title">
            <a id="panelRegistro" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            <span id="flechaRegistro" class="glyphicon glyphicon-chevron-down"></span> Registro de secciones
            </a>
          </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">



            <div id="contenedorCarga">
                <?php require_once("AgregarSeccion.php"); ?>
            </div> 
            <div id="infoPanel1" class="col-sm-3">
                <div class="row">
                        <div class="col-md-offset-8">
                        </div>
                </div>
                <div class="row">
                    <div class="panel panel-info">

                        <div class="panel-heading"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Información de la carga</div>

                        <div class="panel-body" >
                            <div class="row">
                                <div class="col-lg-12">
                                    <input id="id_carga" name="id_carga" type="hidden" value="<?php echo $id_carga; ?>" ></input>
                                <!--************************Datos*********************-->
                                        <div class="row" >                    
                                            <label class=" col-sm-2 control-label" >Período</label>                  
                                        </div>                     
                                        <div class="row">                          
                                            <div class=" col-sm-2" >
                                                <?php echo $periodo; ?>
                                            </div> 
                                        </div>  

                                        <div class="row" >                    
                                            <label class=" col-sm-2 control-label" >Año</label>                  
                                        </div>                     
                                        <div class="row">                          
                                            <div class=" col-sm-2" >                      
                                                <?php echo $anio; ?>  
                                            </div>                  
                                        </div>

                                        <div class="row" >                    
                                            <label class=" col-sm-2 control-label" >Departamento</label>                  
                                        </div>                     
                                        <div class="row">                          
                                            <div class=" col-sm-12" >                   
                                                <?php echo $deptoNombre; ?>  
                                            </div>                  
                                        </div>

                                        <hr>

                                        <div class="row" >                    
                                            <label class="col-sm-12 control-label" >Ingreso</label> 
                                        </div>
                                        <div class="row" >                    
                                            <div class="col-sm-8 ">    
                                                <?php echo fecha($fechaI); ?>  
                                            </div>                 
                                        </div>
                                        <br>
                                        <div class="row" >                    
                                            <label class="col-sm-12 control-label" >Revisión</label> 
                                        </div>
                                        <div class="row" >                    
                                            <div class="col-sm-8 ">    
                                                <?php echo fecha($fechaR); ?>  
                                            </div>                 
                                        </div>
                                        <br>
                                        <div class="row" >                    
                                            <label class="col-sm-12 control-label" >Modificación</label> 
                                        </div>
                                        <div class="row" >                    
                                            <div class="col-sm-8 ">    
                                                <?php echo fecha($fechaM); ?>  
                                            </div>                 
                                        </div>
                                        <br>
                                        <div class="row" >                    
                                            <label class="col-sm-12 control-label" >Finalización</label> 
                                        </div>
                                        <div class="row" >                    
                                            <div class="col-sm-8 ">    
                                                <?php echo fecha($fechaF); ?>  
                                            </div>                 
                                        </div>

                                </div>   
                                <!--************************Datos*********************-->

                            </div>
                        </div>
                    </div>                                    
                </div>
            </div> 


          </div>
        </div>
    </div>
</div>

<?php
        }else{
            echo mensajes("El periodo de ingreso de carga ya finalizó, la modificación se habilitará hasta que finalice la revisión.<br>(Hasta el ".fecha($fechaM).")","azul");            
        }
    }else{
        echo mensajes("El periodo de registro de cargas ya finalizó.","azul");
    }
?>
<div class="row" id="divRespuestaEditar"></div> 
<div class="row">



    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingTwo">
          <h4 class="panel-title">
            <a id="panelCarga" data-toggle="collapse" data-parent="#accordion" href="#collapseThreeCarga" aria-expanded="true" aria-controls="collapseThree">
              <span id="flechaCarga" class="glyphicon glyphicon-chevron-up"></span> Todas las secciones
            </a>
          </h4>
        </div>
        <div id="collapseThreeCarga" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="true">
          <div class="panel-body">
<?php if($procesoActualP>=2){ ?>
            <div id="Observaciones" class="row">
              <?php require_once("Observaciones.php"); ?>                
            </div>
<?php }else{
    echo mensajes("La revisión se habilitará a partir del ".fecha($fechaRP),"azul");
} ?>

            <div id="TablaCarga" class="row">
              <?php require_once("DatosCarga.php"); ?>
            </div>

          </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalTemp" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-body">
            <div id="subModalTemp">
            </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript" src="pages/CargaAcademica/GestionCargas/Script.js"></script>

<?php 
    }else{
    echo mensajes("Aún no ha comenzado el registro de cargas de ningun periodo","azul");
    }
}else{
    echo mensajes("Problemas en la página, informe al administrador","rojo");
    } 
}else{
    echo mensajes("Usted no tiene permisos para ver esta página","rojo");
    } 
?>