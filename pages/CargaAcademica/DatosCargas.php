<?php
    $maindir = "../../";
    require_once ("Reportes/Cargas/clases.php");
    require_once ("clases.php");
    require_once("ChekAutoIncrement.php");
    //require_once($maindir."funciones/check_session.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
//$modificaciones = true;
    $fechaM = $_SESSION['ultimoPeriodo']->getFechaM();
    $fechaF = $_SESSION['ultimoPeriodo']->getFechaF();
    $hoy = date("Y-m-d");
    if($hoy > $fechaM and $hoy<$fechaF){
        $modificaciones = true;
    }else{
        $modificaciones = false;
    }
if (isset($_SESSION['ultimoPeriodo'])) {

    $ultimoPID = $_SESSION['ultimoPeriodo']->getId();
    $IDusuario = $_SESSION['user_id'];

    $query1 = "SELECT CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'nombreE'
            FROM usuario,empleado,persona 
            WHERE usuario.No_Empleado=empleado.No_Empleado
                and empleado.N_identidad=persona.N_identidad
                and usuario.id_Usuario=$IDusuario ";

    $nombreProfesor="";
    $result =$db->prepare($query1);
    $result->execute();
    while ($fila = $result->fetch()) {
        $nombreProfesor = $fila["nombreE"];
        break;
    }

    $query1 = "SELECT ca_secciones.id_seccion,
                    ca_secciones.codigo_seccion,
                    ca_secciones.id_asignatura,
                    ca_asignaturas.nombre as nAsignatura,
                    ca_secciones.cupos_max,
                    ca_secciones.matriculados,
                    ca_secciones.Hora_inicio,
                    ca_secciones.Hora_fin,
                    ca_dias.id_dia,
                    ca_dias.nombre as Dia,
                    ca_secciones.id_aula,
                    ca_aulas.nombre as nAula,
                    ca_edificios.nombre as nEdificio,
                    ca_secciones.No_Empleado,
                    CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'nombreE',
                    ca_departamentos_carreras.nombre as nombreDepto
                FROM ca_secciones,ca_asignaturas,ca_aulas,ca_edificios,empleado,persona,ca_secciones_dias,ca_dias,ca_cargas,ca_periodos,ca_departamentos_carreras
                WHERE ca_secciones.id_asignatura=ca_asignaturas.id_asignatura
                    and ca_secciones.id_seccion=ca_secciones_dias.id_seccion
                    and ca_secciones_dias.id_dia = ca_dias.id_dia
                    and ca_secciones.id_aula=ca_aulas.id_aula
                    and ca_aulas.id_edificio=ca_edificios.id_edificio
                    and ca_secciones.No_Empleado=empleado.No_Empleado
                    and empleado.N_identidad=persona.N_identidad
                    AND ca_secciones.id_carga=ca_cargas.id_carga
                    and ca_cargas.id_periodo=ca_periodos.id_periodo
                    and ca_cargas.id_departamento=ca_departamentos_carreras.id_departamento
                    and ca_periodos.id_periodo= $ultimoPID
                    AND ca_secciones.No_Empleado=(SELECT usuario.No_Empleado FROM usuario WHERE usuario.id_Usuario=$IDusuario  )";

    $result =$db->prepare($query1);
    $result->execute();
    $tempID=-1;
    $seccionesTabla = array();
    $Registros = false;
    while ($fila = $result->fetch()) {
        if ($tempID!=$fila["id_seccion"]) {
            $tempID = $fila["id_seccion"];
            $id_seccion = $fila["id_seccion"];
            $codigo_seccion = $fila["codigo_seccion"];
            $id_asignatura = $fila["id_asignatura"];
            $nAsignatura = $fila["nAsignatura"];
            $cupos_max = $fila["cupos_max"];
            $matriculados = $fila["matriculados"];
            $Hora_inicio = $fila["Hora_inicio"];
            $Hora_fin = $fila["Hora_fin"];
            $id_aula = $fila["id_aula"];
            $nAula = $fila["nAula"];
            $nEdificio = $fila["nEdificio"];
            $No_Empleado = $fila["No_Empleado"];
            $nombreE = $fila["nombreE"];
            $Depto = $fila["nombreDepto"];
            $seccionesTabla[$fila["id_seccion"]] = new SeccionCarga($id_seccion,
                                     $codigo_seccion,
                                     $id_asignatura,
                                     $nAsignatura,
                                     $cupos_max,
                                     $matriculados, 
                                     $Hora_inicio,
                                     $Hora_fin,
                                     $id_aula,
                                     $nAula,
                                     $nEdificio,
                                     $No_Empleado,
                                     $nombreE,
                                     $Depto);
            $seccionesTabla[$fila["id_seccion"]]->Dias[$fila["id_aula"]]=$fila["Dia"];
        }else{
            $seccionesTabla[$fila["id_seccion"]]->Dias[$fila["id_dia"]]=$fila["Dia"];
        }
        $Registros = true;
    }
?>


    <div class="box box-warning">
        <div class="box-body table-responsive">
            <table id="tablaRepoCargas" class="table table-bordered table-striped">
                <thead>
                    <tr>                                            
                        <th style="text-align:center;background-color:#386D95;color:white;">Código</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Asignatura</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Matricula</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Horario</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Dias</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Aula</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Edificio</th>
                        <th style="text-align:center;background-color:#386D95;color:white;display:none;">Profesor</th>
                        <th style="text-align:center;background-color:#386D95;color:white;display:none;">Departamento</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if ($Registros) {
                    foreach ($seccionesTabla as $key => $subSeccion) { 
                        $TempIDseccion = $subSeccion->getid_seccion();
                        ?>
                        <tr data-id='<?php echo $TempIDseccion; ?>'  
                        data-asignatura='<?php echo $subSeccion->getid_asignatura(); ?>'
                        data-aula='<?php echo $subSeccion->getid_aula(); ?>'
                        data-empleado='<?php echo $subSeccion->getNo_Empleado(); ?>'
                        data-hori='<?php echo $subSeccion->getHora_inicio(); ?>'
                        data-horf='<?php echo $subSeccion->getHora_fin(); ?>'
                        data-matr='<?php echo $subSeccion->getmatriculados(); ?>'>
                            <td><?php echo $subSeccion->getcodigo_seccion(); ?></td>
                            <td><?php echo $subSeccion->getnAsignatura(); ?></td>
                            <td>
                                <?php if ($modificaciones) {
                                    echo "<a class='cambiarMatricula$auto_increment' href='#'>".$subSeccion->getmatriculados()."/".$subSeccion->getcupos_max()."</a>";
                                }else{
                                    echo $subSeccion->getmatriculados()."/".$subSeccion->getcupos_max();
                                }?>
                            </td>
                            <td><?php echo substr($subSeccion->getHora_inicio(),0,-3)." - ". substr($subSeccion->getHora_fin(),0,-3); ?></td>
                            </td>
                            <td>
                            <?php foreach ($subSeccion->Dias as $value) {
                                    if ($value!='Sábado'){
                                        echo substr($value, 0,2)." ";
                                    }else{
                                        echo "Sa \n <br>";
                                    }
                                }
                            ?></td>
                            <td><?php echo $subSeccion->getnAula(); ?></td>
                            <td><?php echo $subSeccion->getnEdificio(); ?></td>
                            <td style="display:none;"><?php echo $subSeccion->getnombreE(); ?></td>
                            <td style="display:none;"><?php echo $subSeccion->getDepto(); ?></td>
                        </tr>
                <?php } 
                }?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div>


<script type="text/javascript">
     $('#tablaRepoCargas')
        .removeClass('display')
        .addClass('table table-striped table-bordered');
    $('#tablaRepoCargas').DataTable({
                dom: 'Blfrtip',
        buttons: [
            /*{
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                download: 'open',
                title:'Carga académica de: <?php echo $nombreProfesor; ?>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                download: 'open',
                orientation: 'landscape',
                pageSize: 'LETTER',
                title:'Carga académica de: <?php echo $nombreProfesor; ?>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'print',
                title:'Carga académica de: <?php echo $nombreProfesor; ?>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'*/
        ]
    });

    $(document).on("click", ".cambiarMatricula<?php echo $auto_increment; ?>", function () {
        $('#nMatricula').val($(this).parents("tr").data("matr"));
        $('#idSeccionMatr').val($(this).parents("tr").data("id"));
        $("#modalMatricula").modal('show');
    });

    $('.toggle-vis1').on( 'click', function (e) {
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
 
        // Toggle the visibility
        column.visible( ! column.visible() );
        $(this).attr('color', ' blue');
    });
    $(".toggle-vis1").prop("disabled", "");
</script>
<?php }else{
    echo mensajes("Problemas en la página, intentelo de nuevo","azul");
} ?>