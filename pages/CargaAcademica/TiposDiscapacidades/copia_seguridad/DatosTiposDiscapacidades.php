<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query1 = "SELECT * FROM `ca_tipos_discapacidades`";
?>
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaTiposDiscapacidades" class="table table-bordered table-striped">
            <thead>
                <tr>                                            
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $result =$db->prepare($query1);
                $result->execute();
                while ($fila = $result->fetch()) {
                    ?>
                    <tr data-id='<?php echo $fila["id_discapacidad"]; ?>'>
                        <td><?php echo $fila["id_discapacidad"]; ?></td>
                        <td><?php echo $fila["nombre"]; ?></td>
                        <td>
                            <center>
                                <button type="button" class="editarTipoDiscapacidad btn btn-primary glyphicon glyphicon-edit"  title="Editar Tipo discapacidad">
                            </center>
                        </td>       
                        <td>
                            <center>
                                <button type="button" class="eliminarTipoDiscapacidad<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-remove"  title="Deshabilitar Asignatura">
                            </center>
                        </td>     
                    </tr>
            <?php } ?>
            </tbody>
            <tfoot>
                <tr>                                            
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                </tr>
            </tfoot>
        </table>
    </div><!-- /.box-body -->
</div>
<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#tablaTiposDiscapacidades')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaTiposDiscapacidades').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
$(document).on("click", ".eliminarTipoDiscapacidad<?php echo $auto_increment; ?>", function () {
    if (confirm("¿Está seguro de que desea eliminar este tipo de discapacidad?")){
        codigo = $(this).parents("tr").data("id");
        data={
                    Codigo:codigo
            }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: eliminarTiposDiscapacidades,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    }
});

</script>