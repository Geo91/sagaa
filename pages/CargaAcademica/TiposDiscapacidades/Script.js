//cargar la tabla desde un principio
    $("#datosTabla").load('pages/CargaAcademica/TiposDiscapacidades/DatosTiposDiscapacidades.php');

$(document).ready(function(){   

//para cuando se da submit al formulario formAsignatura
    $("#formTipoDiscapacidad").submit(function(e) {
        e.preventDefault();
        data={
                    Nombre:$('#NombreTipoDiscapacidad').val()                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: agregarTipoDiscapacidad,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });


//para cuando se da submit al formulario formAsignatura
    $("#formEditar").submit(function(e) {
        e.preventDefault();
        data={
                    Codigo:$('#modalCodigo').val(),
                    Nombre:$('#modalNombre').val()
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: editarTipoDiscapacidad,
            timeout: 4000,
            error: problemas
        }); 
        $("#modalEditar").modal('hide');
        return false;
    });

});

//para editar una asignatura
$(document).on("click", ".editarTipoDiscapacidad", function () {
    codigo = $(this).parents("tr").find("td").eq(0).html();
    Nombre = $(this).parents("tr").find("td").eq(1).html();

    $("#modalCodigo").val(codigo);
    $("#modalNombre").val(Nombre);

    $("#modalEditar").modal('show');
});


function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function agregarTipoDiscapacidad(){
    $("#divRespuesta").load('pages/CargaAcademica/TiposDiscapacidades/GuardarTipoDiscapacidad.php',data);
    $('#formTipoDiscapacidad').trigger("reset");
}

function editarTipoDiscapacidad(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/TiposDiscapacidades/EditarTiposDiscapacidades.php',data);
}

function eliminarTiposDiscapacidades(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/TiposDiscapacidades/EliminarTiposDiscapacidades.php',data);
}