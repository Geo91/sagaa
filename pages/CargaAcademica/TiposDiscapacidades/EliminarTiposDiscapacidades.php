<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Codigo'])){
        
        $Codigo = $_POST['Codigo'];

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query = "DELETE FROM ca_profesores_discapacidades WHERE ca_profesores_discapacidades.id_discapacidad=$Codigo;
        DELETE FROM ca_tipos_discapacidades where ca_tipos_discapacidades.id_discapacidad=$Codigo;";
        try {
            $result =$db->prepare($query);
            $result->execute();
            echo mensajes("Se ha eliminado el tipo de discapacidad ID.$Codigo","verde");
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }else{
        echo mensajes("Ha ocurrido un error del servidor...","verde");
    }
?>
<script type="text/javascript">
    $("#datosTabla").load('pages/CargaAcademica/TiposDiscapacidades/DatosTiposDiscapacidades.php');
</script>