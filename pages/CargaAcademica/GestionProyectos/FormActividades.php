<?php
    $maindir = "../../../";
    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."conexion/config.inc.php");
    
    $idU = $_SESSION['user_id'];
    $rol = $_SESSION['user_rol'];
    $ultPID = $_SESSION['ultimoPeriodo']->getId();
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    if($rol == 30 || $rol == 50 || $rol == 100){
        if($rol == 30){
        	$query1 = "SELECT empleado.No_Empleado, 
                    CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombre
                FROM empleado,persona,cargo,empleado_has_cargo
                WHERE empleado.N_identidad=persona.N_identidad
                    and empleado_has_cargo.No_Empleado=empleado.No_Empleado
                    and empleado_has_cargo.ID_cargo=cargo.ID_cargo
                    and empleado.estado_empleado=1
                    and (cargo.Cargo='Docente' 
                         or cargo.Cargo='Decana'
                         or cargo.Cargo='Jefe de Departamento'
                         or cargo.Cargo='Coordinador de Maestría'
			or cargo.Cargo = 'Instructor A III')
                    and empleado_has_cargo.Fecha_salida_cargo is null
                    and empleado.No_Empleado in (SELECT ca_profesores.No_Empleado FROM ca_profesores) and empleado.Id_departamento = (SELECT empleado.Id_departamento from empleado where empleado.No_Empleado = (SELECT usuario.No_Empleado from usuario where usuario.id_Usuario = $idU))
                ORDER BY persona.Primer_nombre";
            }elseif($rol == 50 || $rol == 100){
            	$query1 = "SELECT empleado.No_Empleado, 
                    CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombre
                FROM empleado,persona,cargo,empleado_has_cargo
                WHERE empleado.N_identidad=persona.N_identidad
                    and empleado_has_cargo.No_Empleado=empleado.No_Empleado
                    and empleado_has_cargo.ID_cargo=cargo.ID_cargo
                    and empleado.estado_empleado=1
                    and (cargo.Cargo='Docente' 
                         or cargo.Cargo='Decana'
                         or cargo.Cargo='Jefe de Departamento'
                         or cargo.Cargo='Coordinador de Maestría')
                    and empleado_has_cargo.Fecha_salida_cargo is null
                    and empleado.No_Empleado in (SELECT ca_profesores.No_Empleado FROM ca_profesores)
                ORDER BY persona.Primer_nombre";
            }
        
?>

<div class="col-md-9 col-sm-8">
	<div class="panel panel-info">
	    <div class="panel-heading"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar actividades en las que participan los docentes</div>

	    <div class="panel-body" >
	        <div class="row">
	            <div class="col-lg-12">
	            <!--************************formulario*********************-->
	                <form class="form-horizontal" role="form" id="formActividades" name="formActividades">

                        <div class="row form-group" style="display:none">
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="IDP" value = "<?php echo $ultPID; ?>" name="periodo" required>
                            </div>
                        </div>

	                    <div class="row form-group" >                    
	                        <label class=" col-sm-2 control-label" >Actividad</label>                       
	                        <div class="col-sm-8">                            
	                            <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Ejmpl: Comité de Desarrollo Curricular" required>              
	                        </div>                  
	                    </div>
	                    
	                    <div class="row form-group">                    
                            <label class=" col-sm-2 control-label"> Tipo </label>
                            <div class="col-sm-2">
                                <select id="tipoAct" name="tipoActividad" size="2" required>
                                    <option value="Permanente"> Permanente </option>
                                    <option value="Trimestral"> Trimestral </option>
                                </select>            
                            </div>                             
                        </div>

                        <div class="row form-group">
                            <label class=" col-sm-2 control-label" >Participantes <br> <br> <span class="glyphicon glyphicon-info-sign"></span> Seleccione una o más opciones </label>

                            <div class="col-sm-8">                            
                                
                                    <?php
                                        $result =$db->prepare($query1);
                                        $result->execute();
                                        $count = $result->rowCount();
                                        if ($count < 20){
                                        	echo '<select class="form-control" id="participantesAct" name="selectParticipantes" size="'.$count.'" multiple title="seleccione una o más opciones" required>';
                                    	}else{
                                    		echo '<select class="form-control" id="participantesAct" name="selectParticipantes" size="10" multiple title="seleccione una o más opciones" required>';
                                    	}
                                        while ($fila = $result->fetch()) {
                                            ?>
                                            <option value="<?php echo $fila["No_Empleado"] ?>">
                                            <?php 
                                                echo $fila["nombre"]; ?></option>
                                                <?php
                                            }
                                        ?>
                                </select>
                            </div>
                            
                        </div>

	                    <div class="row">
	                        <label class="control-label col-sm-2"></label>
	                        <div class="col-sm-8">
	                            <p aling ="right">
	                                <button id ="guardarCambios" type="submit" class="btn btn-primary col-sm-offset-10"> Guardar <span class="glyphicon glyphicon-floppy-disk"></span></button>
	                            </p>          
	                        </div>                            
	                    </div>    

	                </form>
	            <!--************************formulario*********************-->
	            </div>
	        </div>
	    </div>                                    
	</div> 
</div>

<?php
    }else{
        echo mensajes("Usted no tiene acceso a este formulario","rojo");
    }
?>

<script type="text/javascript" src="pages/CargaAcademica/GestionProyectos/Script.js"></script>