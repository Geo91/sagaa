<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
?>

<div id="divRespuesta"></div>

<div id="divContenido">
    <?php  require_once("tablaInfoActividades.php"); ?>
</div>

<div class="modal fade" id="modalActividades2" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div id="subContenidoAct2">
            </div>
            <div class="modal-footer">
                <button type="button"  class="btn btn-default" data-dismiss="modal">Salir</button>
            </div>
        </div>
    </div>
</div>