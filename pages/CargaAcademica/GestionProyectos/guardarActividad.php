<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['pID']) && 
        isset($_POST['desc']) && 
        isset($_POST['tipo']) && 
        isset($_POST['participantes']) ){
        
        $p_ID = $_POST['pID'];
        $desc_act = $_POST['desc'];
        $tipo_act = $_POST['tipo'];
        $part = $_POST['participantes'];

        try{
        // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
            $query0 = "SELECT * FROM ca_actividad WHERE ca_actividad.descripcion='$desc_act' AND ((ca_actividad.periodo_id='$p_ID' AND ca_actividad.tipo='TRIMESTRAL') OR ca_actividad.tipo='PERMANENTE')";
            $query = "INSERT INTO ca_actividad (`id_actividad`,`descripcion`,`periodo_id`,`tipo`,`estado`) VALUES (NULL, '$desc_act', '$p_ID', '$tipo_act', '1');";
            $result = $db->prepare($query0);
            $result->execute();
            
            $bandera = true;
            while ($fila = $result->fetch()) {
                $bandera = false;
                break;
            }
            if($bandera){
                $result = $db->prepare($query);
                $result->execute();

                foreach ($part as $participante) {
                    $query2 = "INSERT INTO ca_actividad_empleado (`actividad_id`, `empleado_no`) VALUES ((SELECT ca_actividad.id_actividad FROM ca_actividad  WHERE ca_actividad.descripcion = '$desc_act' LIMIT 1),'$participante');";
                    $result = $db->prepare($query2);
                    $result->execute();
                }

                $mensaje = "La actividad `$desc_act` se ha guardado.";
                $color = "verde";
            }
            else{
                $mensaje ="Ya existe una actividad descrita como `$desc_act`";
                $color = "rojo";
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>