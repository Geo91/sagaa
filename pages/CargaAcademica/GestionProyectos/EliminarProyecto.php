<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['proyectoId'])  ){
        
        $proyectoId = $_POST['proyectoId'];

        try{
        // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
            $query0 = "DELETE FROM ca_proyectos WHERE ca_proyectos.id_proyecto=$proyectoId;
                DELETE FROM ca_profesores_proyectos WHERE ca_profesores_proyectos.id_proyecto=$proyectoId;";
            $result =$db->prepare($query0);
            $result->execute();
            echo mensajes("Se ha eliminado el proyecto ID.$proyectoId","verde");
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>
<script type='text/javascript'>
    $('#divContenido').empty();
    $('#divContenido').load('pages/CargaAcademica/GestionProyectos/TablaDatos.php',data);
</script>