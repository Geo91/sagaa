<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query1 = "SELECT ca_proyectos.id_proyecto,
                    ca_proyectos.nombre,
                    ca_tipos_proyectos.nombre as area,
                    ca_proyectos.fecha_inicio,
                    ca_proyectos.fecha_fin
                FROM ca_proyectos,ca_tipos_proyectos
                WHERE ca_proyectos.id_tipo_proyecto=ca_tipos_proyectos.id_tipo_proyecto";
?>

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong>Historial de Proyectos</strong></center></h3>
    </div>
</div> 
    
    <div class="box">
        <div class="box-body table-responsive">
            <table id="tablaAsignaturas" class="table table-bordered table-striped">
                <thead>
                    <tr>                                            
                        <th style="text-align:center;background-color:#386D95;color:white;display:none">ID</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Area</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Fecha inicio</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Fecha finalización</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Ver</th>
                    <?php if($_SESSION['user_rol']!=30){  ?>
                        <th style="text-align:center;background-color:#386D95;color:white;">Editar</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Eliminar</th>
                    <?php } ?>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $result =$db->prepare($query1);
                    $result->execute();
                    while ($fila = $result->fetch()) {
                        ?>
                        <tr data-id='<?php echo $fila["id_proyecto"]; ?>'>
                            <td style = "display:none"><?php echo $fila["id_proyecto"]; ?></td>
                            <td><?php echo $fila["nombre"]; ?></td>
                            <td><?php echo $fila["area"]; ?></td>
                            <td><?php echo $fila["fecha_inicio"]; ?></td>
                            <td><?php echo $fila["fecha_fin"]; ?></td>
                            <td>
                                <center>
                                    <button type="button" class="VerInvolucrados<?php echo $auto_increment; ?> btn btn-success glyphicon glyphicon-info-sign"  title="Ver información"></button>
                                </center>
                            </td>       
                        <?php if($_SESSION['user_rol']!=30){  ?>
                            <td>
                                <center>
                                    <button type="button" class="VerProyecto<?php echo $auto_increment; ?> btn btn-info glyphicon glyphicon-edit"  title="Editar informacion"></button>
                                </center>
                            </td>       
                            <td>
                                <center>
                                    <button type="button" class="RemoverProyecto<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-remove"  title="Eliminar proyecto">
                                </center>
                            </td> 
                        <?php } ?>    
                        </tr>
                <?php } ?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div>

<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#tablaAsignaturas')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaAsignaturas').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
    
$(document).on("click", ".VerInvolucrados<?php echo $auto_increment; ?>", function () {
    data={
            proyectoId:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: verInvolucrados,
        timeout: 4000,
        error: problemas
    }); 
    return false;
});

$(document).on("click", ".VerProyecto<?php echo $auto_increment; ?>", function () {
    data={
            proyectoId:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: editarProyecto,
        timeout: 4000,
        error: problemas
    }); 
    return false;
});

$(document).on("click", ".RemoverProyecto<?php echo $auto_increment; ?>", function () {
    if (confirm("¿Está seguro de que desea eliminar este proyecto?")){
        codigo = $(this).parents("tr").data("id");
        data={
                    proyectoId:codigo
            }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: eliminarProyecto,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    }
});
</script>
<script type="text/javascript" src="pages/CargaAcademica/GestionProyectos/Script.js"></script>