<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir . "conexion/config.inc.php");

    $bandera= false;
    $proyectoId = -1;
    if (isset($_POST['proyectoId'])) {
        $proyectoId = $_POST['proyectoId'];
    }

    $query = "SELECT * FROM `ca_tipos_proyectos`";
    $query1 = "SELECT empleado.No_Empleado, 
                CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombre,
            (CASE when proyectoProfesor.id_proyecto is null THEN
                    0
                ELSE 1 END) as proyectoP
            FROM empleado 
                LEFT JOIN persona
                    on empleado.N_identidad=persona.N_identidad
                LEFT JOIN empleado_has_cargo
                    on empleado_has_cargo.No_Empleado=empleado.No_Empleado
                LEFT JOIN cargo
                    on empleado_has_cargo.ID_cargo=cargo.ID_cargo
                LEFT JOIN (SELECT * FROM ca_profesores_proyectos WHERE ca_profesores_proyectos.id_proyecto=$proyectoId) as proyectoProfesor
                    on empleado.No_Empleado=proyectoProfesor.No_Empleado
            WHERE empleado.estado_empleado=1
                and (cargo.Cargo='Docente' 
                     or cargo.Cargo='Decana'
                     or cargo.Cargo='Jefe de Departamento'
                     or cargo.Cargo='Coordinador de Maestría')
                and empleado_has_cargo.Fecha_salida_cargo is null
                and empleado.No_Empleado in (SELECT ca_profesores.No_Empleado FROM ca_profesores) 
            ORDER BY persona.Primer_nombre";
    $query3 = "SELECT * FROM ca_proyectos WHERE ca_proyectos.id_proyecto=$proyectoId";
    if($proyectoId>-1){
        $result =$db->prepare($query3);
        $result->execute();
        while ($fila = $result->fetch()) { 
            $bandera= true;
            $id_proyecto = $fila["id_proyecto"];
            $nombre = $fila["nombre"];
            $descripcion = $fila["descripcion"];
            $fecha_inicio = $fila["fecha_inicio"];
            $fecha_fin = $fila["fecha_fin"];
            $id_tipo_proyecto = $fila["id_tipo_proyecto"];
        }
    }

?>
<div class="col-sm-10">
    <div class="panel panel-primary">
        <div class="panel-heading"><h4><span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span><?php if($bandera){
                                        echo " Editar ";
                                    }else{
                                        echo " Agregar ";
                                        } ?>proyecto</h4> </div>
        <div class=" panel-body" >
            <div class="row">
                <div id="subContenidoFormulario" class="col-lg-12">
                    <form class="form-horizontal" role="form" id="formNuevoProyecto" name="formNuevoProyecto">

                        <input type="hidden" class="form-control" id="id_proyecto" name="id_proyecto" required value="<?php echo $proyectoId; ?>">

                        <div class="row form-group" >                    
                            <label class=" col-sm-2 control-label" >Nombre</label>
                            <div class="col-sm-8">               
                                <input type="text" class="form-control" id="nombre" name="nombre" maxlength="50" 
                                <?php if($bandera){
                                        echo " value='$nombre' ";
                                    } ?>
                                required autocomplete="off">
                            </div>                  
                        </div>

                        <div class="row form-group">
                            <label class=" col-sm-2 control-label" >Area</label>
                            <div class="col-sm-8">                            
                                <select class="form-control" id="selectTipoArea" name="selectTipoArea" required>
                                    <option value="">Seleccione una opcion</option>
                                    <?php
                                        $result =$db->prepare($query);
                                        $result->execute();
                                        while ($fila = $result->fetch()) {
                                    ?> 
                                            <option value="<?php echo $fila['id_tipo_proyecto']; ?>"
                                        <?php if($bandera){
                                            if($fila['id_tipo_proyecto']==$id_tipo_proyecto){
                                                    echo " selected ";
                                                } 
                                        } ?>
                                    ><?php echo $fila["nombre"]; ?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class=" col-sm-2 control-label" >Involucrados</label>
                            <div class="col-sm-8">                            
                                <select class="form-control" id="selectInvolucrados" name="selectInvolucrados" size="10" multiple title="seleccione una o más opciones" required>
                                    <?php
                                         $result =$db->prepare($query1);
                                         $result->execute();
                                        while ($fila = $result->fetch()) {
                                            ?>
                                            <option value="<?php echo $fila["No_Empleado"] ?>"
                                        <?php if($fila['proyectoP']==1){
                                                echo " selected ";
                                            } ?>>
                            <?php 
                                $largo = strlen($fila["No_Empleado"]);
                                if ($largo<=7) {
                                    $espacio = str_repeat ( "&nbsp", 7-$largo);
                                }else{
                                    $espacio=" ";
                                }
                                echo "█ No.".$fila["No_Empleado"].$espacio."█ &nbsp&nbsp".$fila["nombre"]; ?></option>
                                            <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <br><br><br>
                            <span class="glyphicon glyphicon-info-sign"></span> Seleccione una o más opciones
                        </div>

                        <div class="row form-group">
                            <div class=" col-sm-1 " ></div>
                            <div class="col-sm-9 input-group">
                                <span class="input-group-addon"><strong>Descripción</strong></span>
                                <textarea style="resize: vertical" id="Descripcion" class="form-control" name="Descripcion" required="required" rows="3" maxlength="250" placeholder='Ingrese una descripcion...'><?php if($bandera){
                                        echo "$descripcion";
                                    }?></textarea>
                            </div>
                        </div>

                        <div class="row form-group" >                    
                            <label class=" col-sm-2 control-label" >Fecha de inicio </label>
                            <div class="col-sm-8">  
                                <p> <input type="date" id="fechaInicio"placeholder="aaaa-mm-dd" 
                                <?php if($bandera){
                                        echo " value='$fecha_inicio' ";
                                    } ?> 
                                required></p>
                            </div>                  
                        </div>

                        <div class="row form-group" >                    
                            <label class=" col-sm-2 control-label" >Fecha de finalización </label>
                            <div class="col-sm-8">  
                                <p> <input type="date" id="fechaFin" placeholder="aaaa-mm-dd"
                                <?php if($bandera){
                                        echo " value='$fecha_fin' ";
                                    } ?> 
                                required></p>
                            </div>                  
                        </div>

                        <div class="modal-footer">
                            <?php if($bandera){ ?>
                                <button type="button" id="CancelarProyecto" class="btn btn-default" >Cancelar</button>
                            <?php } ?> 
                            <button  id="guardarCambios" class="btn btn-primary" ><span class=" glyphicon glyphicon-floppy-disk"></span>Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>                                    
    </div>
</div>
<script type="text/javascript" src="pages/CargaAcademica/GestionProyectos/Script.js"></script>