<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    if (isset($_POST['proyectoId'])) {
        $proyectoId = $_POST['proyectoId'];
        $query = "SELECT ca_proyectos.nombre,
                    ca_proyectos.descripcion
                FROM ca_proyectos
                WHERE ca_proyectos.id_proyecto=$proyectoId";
        $query1 = "SELECT empleado.No_Empleado, 
                        persona.N_identidad,
                        CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombre
                FROM ca_profesores_proyectos,empleado,persona
                WHERE ca_profesores_proyectos.No_Empleado=empleado.No_Empleado
                    and empleado.N_identidad = persona.N_identidad
                    and ca_profesores_proyectos.id_proyecto=$proyectoId";

        $result =$db->prepare($query);
        $result->execute();
        while ($fila = $result->fetch()) {
        ?>
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3 class="modal-title"><?php echo $fila["nombre"]; ?></h3>
            </div>
            <div class="row">
                <div class="col-sm-offset-1 col-sm-10">
                    <center><?php echo $fila["descripcion"]; ?></center>
                </div>
            </div>
        <?php
            break;
        }
?>
<div class="modal-body">
<h4 >Involucrados:</h4>
<table id="tablaAsignaturas" class="table table-bordered table-striped">
    <thead>
        <tr>                                            
            <th>No Empleado</th>
            <th>Identidad</th>
            <th>nombre</th>
        </tr>
    </thead>
    <tbody>
    <?php
        $result =$db->prepare($query1);
        $result->execute();
        while ($fila = $result->fetch()) {
            ?>
            <tr data-id='<?php echo $fila["No_Empleado"]; ?>'>
                <td><?php echo $fila["No_Empleado"]; ?></td>
                <td><?php echo $fila["N_identidad"]; ?></td>
                <td><?php echo $fila["nombre"]; ?></td>
            </tr>
    <?php } ?>
    </tbody>
</table>
<?php  
    }else{
        echo mensajes("Problemas en el servidor","azul");
    }
?>
</div>