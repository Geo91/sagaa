<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query1 = "SELECT ca_proyectos.id_proyecto,
                    ca_proyectos.nombre,
                    ca_tipos_proyectos.nombre as area,
                    ca_proyectos.fecha_inicio,
                    ca_proyectos.fecha_fin
                FROM ca_proyectos,ca_tipos_proyectos
                WHERE ca_proyectos.id_tipo_proyecto=ca_tipos_proyectos.id_tipo_proyecto";
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Todos los proyectos</h1>
    </div>
</div> 
<div class="box box-primary">      
    <div class="box">
        <div class="box-body table-responsive">
            <table id="tablaAsignaturas" class="table table-bordered table-striped">
                <thead>
                    <tr>                                            
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Area</th>
                        <th>Fecha inicio</th>
                        <th>Fecha finalización</th>
                        <th>Ver</th>
                    <?php if($_SESSION['user_rol']!=30){  ?>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    <?php } ?>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $result =$db->prepare($query1);
                    $result->execute();
                    while ($fila = $result->fetch()) {
                        ?>
                        <tr data-id='<?php echo $fila["id_proyecto"]; ?>'>
                            <td><?php echo $fila["id_proyecto"]; ?></td>
                            <td><?php echo $fila["nombre"]; ?></td>
                            <td><?php echo $fila["area"]; ?></td>
                            <td><?php echo $fila["fecha_inicio"]; ?></td>
                            <td><?php echo $fila["fecha_fin"]; ?></td>
                            <td>
                                <center>
                                    <button type="button" class="VerInvolucrados<?php echo $auto_increment; ?> btn btn-primary glyphicon glyphicon-info-sign"  title="Ver información"></button>
                                </center>
                            </td>       
                        <?php if($_SESSION['user_rol']!=30){  ?>
                            <td>
                                <center>
                                    <button type="button" class="VerProyecto<?php echo $auto_increment; ?> btn btn-primary glyphicon glyphicon-edit"  title="Editar informacion"></button>
                                </center>
                            </td>       
                            <td>
                                <center>
                                    <button type="button" class="RemoverProyecto<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-remove"  title="Eliminar proyecto">
                                </center>
                            </td> 
                        <?php } ?>    
                        </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                    <tr>                                         
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Area</th>
                        <th>Fecha inicio</th>
                        <th>Fecha finalización</th>
                        <th>Ver</th>
                    <?php if($_SESSION['user_rol']!=30){  ?>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    <?php } ?>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
    </div>
</div>
<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#tablaAsignaturas')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaAsignaturas').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
    
$(document).on("click", ".VerInvolucrados<?php echo $auto_increment; ?>", function () {
    data={
            proyectoId:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: verInvolucrados,
        timeout: 4000,
        error: problemas
    }); 
    return false;
});

$(document).on("click", ".VerProyecto<?php echo $auto_increment; ?>", function () {
    data={
            proyectoId:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: editarProyecto,
        timeout: 4000,
        error: problemas
    }); 
    return false;
});

$(document).on("click", ".RemoverProyecto<?php echo $auto_increment; ?>", function () {
    if (confirm("¿Está seguro de que desea eliminar este proyecto?")){
        codigo = $(this).parents("tr").data("id");
        data={
                    proyectoId:codigo
            }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: eliminarProyecto,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    }
});
</script>
<script type="text/javascript" src="pages/CargaAcademica/GestionProyectos/Script.js"></script>