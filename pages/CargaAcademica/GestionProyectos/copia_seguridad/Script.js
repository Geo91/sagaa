$(document).ready(function(){   

//para cuando se da submit al formulario formAsignatura
    $("#formNuevoProyecto").submit(function(e) {
        e.preventDefault();
        fechaInicio=$('#fechaInicio').val();
        fechaFin=$('#fechaFin').val();
        if (fechaInicio>=fechaFin){
            alert("La fecha de inicio debe de ser menor a la fecha final.");
        }else{
            data={
                        nombre:$('#nombre').val(),
                        selectTipoArea:$('#selectTipoArea').val(),
                        selectInvolucrados:$('#selectInvolucrados').val(),
                        Descripcion:$('#Descripcion').val(),
                        FechaI:$('#fechaInicio').val(),
                        FechaF:$('#fechaFin').val(),
                        accion:$('#id_proyecto').val()
                    }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                beforeSend: inicioEnvio,
                success: agregarProyecto,
                timeout: 4000,
                error: problemas
            }); 
            return false;
        }
    });

    $("#CancelarProyecto").click(function(event) {
        event.preventDefault();
        $('#divContenido').empty();
        $('#divContenido').load('pages/CargaAcademica/GestionProyectos/TablaDatos.php',data);
    });
});


function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function agregarProyecto(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/CargaAcademica/GestionProyectos/GuardarProyecto.php',data);
    $('#formNuevoProyecto').trigger("reset");
}

function eliminarProyecto(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/CargaAcademica/GestionProyectos/EliminarProyecto.php',data);
}

function editarProyecto(){
    $("#divContenido").empty();
    $("#divContenido").load("pages/CargaAcademica/GestionProyectos/FormProyecto.php",data);
}

function verInvolucrados(){
    $("#subContenidoProyecto").empty();
    $("#subContenidoProyecto").load("pages/CargaAcademica/GestionProyectos/DatosInvolucrados.php",data);
    $("#modalProyecto").modal('show');
}
