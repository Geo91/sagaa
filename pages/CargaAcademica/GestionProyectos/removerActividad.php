<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['emp']) && 
        isset($_POST['act']) ){
        
        $a_ID = $_POST['act'];
        $e_NO = $_POST['emp'];

        try{
        // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO

            $query2 = "DELETE FROM ca_actividad_empleado WHERE actividad_id='$a_ID' AND empleado_no='$e_NO'";
            $result = $db->prepare($query2);
            if($result->execute()){                

                $mensaje = "El empleado fue dado de baja en la actividad seleccionada";
                $color = "verde";
            }
            else{
                $mensaje ="El empleado no pudo ser dado de baja en la actividad seleccionada";
                $color = "rojo";
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>