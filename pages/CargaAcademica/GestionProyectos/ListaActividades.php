<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    if (isset($_POST['emp']) && isset($_POST['idP'])) {
        $empid = $_POST['emp'];
        $perid = $_POST['idP'];
                
        $query1 = "SELECT DISTINCT ca_actividad.id_actividad, ca_actividad.descripcion, ca_actividad.tipo FROM ca_actividad WHERE ca_actividad.id_actividad NOT IN (SELECT ca_actividad_empleado.actividad_id FROM ca_actividad_empleado WHERE ca_actividad_empleado.empleado_no = $empid) AND ca_actividad.estado = '1' AND ((ca_actividad.tipo = 'TRIMESTRAL' AND ca_actividad.periodo_id=$perid) OR ca_actividad.tipo='PERMANENTE')";
        
?>

<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title" style = "color:white">Listado de Actividades Activas</h4>
</div>

<div class="modal-body">
    <table id="tablaListaActividades" class="table table-bordered table-striped">
        <thead>
            <tr>                                            
                <th style="text-align:center;background-color:#386D95;color:white;display:none">ID</th>
                <th style="text-align:center;background-color:#386D95;color:white;">Descripción</th>
                <th style="text-align:center;background-color:#386D95;color:white;">Tipo</th>
                <th style="text-align:center;background-color:#386D95;color:white;width:100px">Seleccionar</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $result =$db->prepare($query1);
            $result->execute();
            while ($fila = $result->fetch()) {
                ?>
                <tr data-id='<?php echo $fila["id_actividad"]; ?>' data-emp='<?php echo $empid;  ?>'>
                    <td style = "display:none"><?php echo $fila["id_actividad"]; ?></td>
                    <td><?php echo $fila["descripcion"]; ?></td>
                    <td><?php echo $fila["tipo"]; ?></td>
                    <td>
                        <center>
                            <button type="button" class="seleccionar<?php echo $auto_increment; ?> btn btn-info" title="Ver información"><span class ="glyphicon glyphicon-ok"></span></button>
                        </center>
                    </td>
                </tr>
        <?php } ?>
        </tbody>
    </table>

</div>

<script type="text/javascript">   
//opciones para buscador en la tabla tablaListaActividades
    $('#tablaListaActividades')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaListaActividades').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        },
        "bAutoWidth" : false, 
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
    
$(document).on("click", ".seleccionar<?php echo $auto_increment; ?>", function () {
    data={
        act:$(this).parents("tr").data("id"),
        emp:$(this).parents("tr").data("emp")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: actividadAdd,
        timeout: 8000,
        error: problemas
    }); 
    return false;
});

</script>

<script type="text/javascript" src="pages/CargaAcademica/GestionProyectos/Script.js"></script>

<?php  
    }else{
        echo mensajes("Problemas en el servidor","azul");
    }
?>