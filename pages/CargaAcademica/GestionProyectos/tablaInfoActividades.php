<?php
    $maindir = "../../../";
    require_once ("../clases.php");
    require_once("../ChekAutoIncrement.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query = "SELECT ca_periodos.id_periodo FROM ca_periodos ORDER BY ca_periodos.id_periodo DESC LIMIT 1;";

    $result =$db->prepare($query);
    $result->execute();
    while ($fila = $result->fetch()) {
        $periodo = $fila['id_periodo'];
        break;
    }
        $query1 = "SELECT DISTINCT ca_actividad.id_actividad, ca_actividad.descripcion, ca_actividad.tipo FROM ca_actividad WHERE ca_actividad.estado = '1' AND ((ca_actividad.tipo = 'TRIMESTRAL' AND ca_actividad.periodo_id=$periodo) OR ca_actividad.tipo='PERMANENTE');";
?>

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong> Registro de Actividades </strong></center></h3>
    </div>
</div> 

<div id="divRespuesta2"></div>
    
    <div class="box">
        <div class="box-body table-responsive">
            <table id="tablaActividades" class="table table-bordered table-striped">
                <thead>
                    <tr>                                            
                        <th style="text-align:center;background-color:#386D95;color:white;">ID</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Descripcion</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Tipo</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Participantes</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Agregar</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $result =$db->prepare($query1);
                    $result->execute();
                    while ($fila = $result->fetch()) {
                        ?>
                        <tr data-id='<?php echo $fila["id_actividad"]; ?>'>
                            <td><?php echo $fila["id_actividad"]; ?></td>
                            <td><?php echo $fila["descripcion"]; ?></td>
                            <td><?php echo $fila["tipo"]; ?></td>
                            <td>
                                <center>
                                    <button type="button" class="VerParticipantes<?php echo $auto_increment; ?> btn btn-warning glyphicon glyphicon-eye-open"  title="Ver información de los Participantes"></button>
                                </center>
                            </td>
                            <td>
                                <center>
                                    <button type="button" class="AgregarP<?php echo $auto_increment; ?> btn btn-info glyphicon glyphicon-plus-sign"  title="Agregar Nuevos Participantes"></button>
                                </center>
                            </td>
                            <td>
                                <center>
                                    <button type="button" class="Eliminar<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-remove"  title="Eliminar registro" ></button>
                                </center>
                            </td>
                           
                        </tr>
                <?php } ?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div>

<script type="text/javascript">   
//opciones para buscador en la tabla tablaEmpActividades
    $('#tablaActividades')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaActividades').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
    
$(document).on("click", ".VerParticipantes<?php echo $auto_increment; ?>", function () {
    data={
        actid:$(this).parents("tr").data("id"),
        idP:$(this).parents("tr").data("perid")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: verParticipantes,
        timeout: 8000,
        error: problemas
    }); 
    return false;
});

$(document).on("click", ".AgregarP<?php echo $auto_increment; ?>", function () {
    data={
        actid:$(this).parents("tr").data("id"),
        idP:$(this).parents("tr").data("perid")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: agregarParticipantes,
        timeout: 8000,
        error: problemas
    }); 
    return false;
});

$(document).on("click", ".Eliminar<?php echo $auto_increment; ?>", function () {
    if (confirm("¿Está seguro de que desea eliminar este registro?")){
        data={
            actid:$(this).parents("tr").data("id")
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: borrarAct,
            timeout: 8000,
            error: problemas
        }); 
        return false;
    }
});

</script>
<script type="text/javascript" src="pages/CargaAcademica/GestionProyectos/Script.js"></script>