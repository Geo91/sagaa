<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['actid']) ){
        
        $a_ID = $_POST['actid'];

        try{
        // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO

            $query = "DELETE FROM ca_actividad_empleado WHERE actividad_id='$a_ID'";
            $query2 = "UPDATE ca_actividad SET ca_actividad.estado = '0' WHERE ca_actividad.id_actividad = '$a_ID'";
            $result = $db->prepare($query);
            $result2 = $db->prepare($query2);
            if($result->execute() && $result2->execute()){                

                $mensaje = "La actividad seleccionada fue eliminada exitosamente";
                $color = "verde";
            }
            else{
                $mensaje ="La actividad seleccionada no pudo eliminarse";
                $color = "rojo";
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }else{
        echo mensajes("Hubo un error al tratar de eliminar este registro, favor intente de nuevo o contacte al administrador del sistema.","rojo");
    }
?>