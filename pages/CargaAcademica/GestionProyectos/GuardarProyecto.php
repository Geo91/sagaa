<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['nombre']) && 
        isset($_POST['selectTipoArea']) && 
        isset($_POST['selectInvolucrados'])  && 
        isset($_POST['Descripcion'])  && 
        isset($_POST['FechaI'])  && 
        isset($_POST['FechaF']) && 
        isset($_POST['accion'])   ){
        
        $nombre = $_POST['nombre'];
        $selectTipoArea = $_POST['selectTipoArea'];
        $selectInvolucrados = $_POST['selectInvolucrados'];
        $Descripcion = $_POST['Descripcion'];
        $FechaI = $_POST['FechaI'];
        $FechaF = $_POST['FechaF'];
        $proyectoID = $_POST['accion'];

        try{
            if ($proyectoID<0) {
            // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
                $query0 = "SELECT * FROM `ca_proyectos` WHERE `nombre`='$nombre'";
                $query = "INSERT INTO `ca_proyectos` (`id_proyecto`, `nombre`, `descripcion`, `fecha_inicio`, `fecha_fin`, `id_tipo_proyecto`) VALUES (NULL, '$nombre', '$Descripcion', '$FechaI', '$FechaF', '$selectTipoArea');";
                $result =$db->prepare($query0);
                $result->execute();
                
                $bandera = true;
                while ($fila = $result->fetch()) {
                    $bandera = false;
                    break;
                }
                if($bandera){
                    $result =$db->prepare($query);
                    $result->execute();

                    foreach ($selectInvolucrados as $involucrado) {
                        $query = "INSERT INTO `ca_profesores_proyectos` (`No_Empleado`, `id_proyecto`) VALUES ('$involucrado',(SELECT ca_proyectos.id_proyecto FROM ca_proyectos  WHERE ca_proyectos.nombre = '$nombre' LIMIT 1));";
                        $result =$db->prepare($query);
                        $result->execute();
                    }

                    $mensaje = "El proyecto `$nombre` se ha guardado.";
                    $color = "verde";
                }
                else{
                    $mensaje="Ya existe un proyecto con el nombre `$nombre`";
                    $color = "azul";
                }
                echo mensajes($mensaje,$color);
            }else{
            // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
                $query0 = "UPDATE ca_proyectos 
                            SET ca_proyectos.nombre='$nombre',
                                ca_proyectos.descripcion='$Descripcion',
                                ca_proyectos.fecha_inicio='$FechaI',
                                ca_proyectos.fecha_fin='$FechaF',
                                ca_proyectos.id_tipo_proyecto=$selectTipoArea
                            WHERE ca_proyectos.id_proyecto=$proyectoID;"."DELETE FROM ca_profesores_proyectos WHERE ca_profesores_proyectos.id_proyecto=$proyectoID;";
                $result =$db->prepare($query0);
                $result->execute();

                foreach ($selectInvolucrados as $involucrado) {
                    $query = "INSERT INTO `ca_profesores_proyectos` (`No_Empleado`, `id_proyecto`) VALUES ('$involucrado',$proyectoID);";
                    $result =$db->prepare($query);
                    $result->execute();
                }

                $mensaje = "El proyecto `$nombre` ha sido editado.";
                $color = "verde";
                echo mensajes($mensaje,$color);
                echo "<script type='text/javascript'>
                        $('#divContenido').empty();
                        $('#divContenido').load('pages/CargaAcademica/GestionProyectos/TablaDatos.php',data);
                    </script>";
            }
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>