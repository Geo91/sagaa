$(document).ready(function(){   

//para cuando se da submit al formulario formAsignatura
    $("#formNuevoProyecto").submit(function(e) {
        e.preventDefault();
        fechaInicio=$('#fechaInicio').val();
        fechaFin=$('#fechaFin').val();
        if (fechaInicio>=fechaFin){
            alert("La fecha de inicio debe de ser menor a la fecha final.");
        }else{
            data={
                nombre:$('#nombre').val(),
                selectTipoArea:$('#selectTipoArea').val(),
                selectInvolucrados:$('#selectInvolucrados').val(),
                Descripcion:$('#Descripcion').val(),
                FechaI:$('#fechaInicio').val(),
                FechaF:$('#fechaFin').val(),
                accion:$('#id_proyecto').val()
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                beforeSend: inicioEnvio,
                success: agregarProyecto,
                timeout: 8000,
                error: problemas
            }); 
            return false;
        }
    });

    $("#CancelarProyecto").click(function(event) {
        event.preventDefault();
        $('#divContenido').empty();
        $('#divContenido').load('pages/CargaAcademica/GestionProyectos/TablaDatos.php',data);
    });

    $("#formActividades").submit(function(e) {
        e.preventDefault();
        data={
            pID:$('#IDP').val(),
            desc:$('#descripcion').val(),
            tipo:$('#tipoAct').val(),
            participantes:$('#participantesAct').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: nuevaActividad,
            timeout: 8000,
            error: problemas
        }); 
        return false;
    });

});


function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function agregarProyecto(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/CargaAcademica/GestionProyectos/GuardarProyecto.php',data);
    $('#formNuevoProyecto').trigger("reset");
}

function eliminarProyecto(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/CargaAcademica/GestionProyectos/EliminarProyecto.php',data);
}

function editarProyecto(){
    $("#divContenido").empty();
    $("#divContenido").load("pages/CargaAcademica/GestionProyectos/FormProyecto.php",data);
}

function verInvolucrados(){
    $("#subContenidoProyecto").empty();
    $("#subContenidoProyecto").load("pages/CargaAcademica/GestionProyectos/DatosInvolucrados.php",data);
    $("#modalProyecto").modal('show');
}

function nuevaActividad(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/CargaAcademica/GestionProyectos/guardarActividad.php',data);
    $('#formActividades').trigger("reset");
}

function actividadAdd(){
    $("#divRespuesta2").empty();
    $("#divRespuesta2").load('pages/CargaAcademica/GestionProyectos/agregarActividad.php',data);
    $("#modalActividades").modal('hide');
    $("#modalActividades2").modal('hide');
}

function verActividades(){
    $("#subContenidoAct").empty();
    $("#subContenidoAct").load("pages/CargaAcademica/GestionProyectos/DatosActEmpleado.php",data);
    $("#modalActividades").modal('show');
}

function agregarActividad(){
    $("#subContenidoAct").empty();
    $("#subContenidoAct").load("pages/CargaAcademica/GestionProyectos/ListaActividades.php",data);
    $("#modalActividades").modal('show');
}

function actividadRemove(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/CargaAcademica/GestionProyectos/removerActividad.php',data);
    $("#modalActividades").modal('hide');
    $("#divContenido").load('pages/CargaAcademica/GestionProyectos/tablaEmpleados.php');
}

function agregarParticipantes(){
    $("#subContenidoAct2").empty();
    $("#subContenidoAct2").load("pages/CargaAcademica/GestionProyectos/ListadoEmpleados.php",data);
    $("#modalActividades2").modal('show');
}

function verParticipantes(){
    $("#subContenidoAct2").empty();
    $("#subContenidoAct2").load("pages/CargaAcademica/GestionProyectos/ListadoParticipantes.php",data);
    $("#modalActividades2").modal('show');
}

function quitarParticipante(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/CargaAcademica/GestionProyectos/removerActividad.php',data);
    $("#modalActividades2").modal('hide');
}

function borrarAct(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/CargaAcademica/GestionProyectos/borrarActividad.php',data);
    $("#divContenido").load('pages/CargaAcademica/GestionProyectos/tablaInfoActividades.php');
}