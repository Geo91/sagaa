<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong> Actividades a Empleados </strong></center></h3>
    </div> 
</div>

<?php
    $maindir = "../../../";
    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");
    $deptoNombre="";
    $rol = $_SESSION['user_rol'];
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
if (($rol == 30 || $rol == 50 || $rol == 100)) {
    if (isset($_SESSION['ultimoPeriodo'])) {
        $tempultPiD=$_SESSION['ultimoPeriodo']->getId();
        if ($tempultPiD!=-1) {
            $tempPeriodoP = $_SESSION['ultimoPeriodo'];
            $fechaRP = $tempPeriodoP->getFechaR();
            $procesoActualP = $tempPeriodoP->getProceso();
            if(!(isset($_SESSION['infoPeriodo']) && isset($_SESSION['infoCarga'])) ){

                $queryPermiso = "SELECT * FROM ca_modificaciones_cargas,ca_cargas
                WHERE ca_modificaciones_cargas.id_carga=ca_cargas.id_carga
                    and ca_modificaciones_cargas.fechaMod=curdate()
                    and ca_cargas.id_periodo=$tempultPiD";

                $result =$db->prepare($queryPermiso);
                $result->execute();
                $existePermiso=false;
                while ($fila = $result->fetch()) {
                    $existePermiso=true;
                    $idCargaTemp = $fila['id_carga'];
                    break;
                }
                if($existePermiso){
                    $_SESSION['ultimoPeriodo']->setProceso(3);
                    $_SESSION['infoPeriodo']=$_SESSION['ultimoPeriodo'];
                }
            }

            if(isset($_SESSION['infoPeriodo'])){
                $tempPeriodo = $_SESSION['infoPeriodo'];
                $periodo = $tempPeriodo->getPeriodo();
                $procesoActual = $tempPeriodo->getProceso();
                $anio = $tempPeriodo->getAnio();
?>

<?php if ($procesoActual==1 || $procesoActual==3 || $procesoActaul==4) { ?>

<div  id="divRespuesta"></div>

<div class="row">
    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingTwo">
			<h4 class="panel-title">
				<span id="flechaRegistro" class="glyphicon glyphicon-chevron-down"></span> Registro de Actividades
			</h4>
        </div>
        <div class="panel" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">

	            <div id="contenedorCarga">
	                <?php require_once("FormActividades.php"); ?>
	            </div> 

          	</div>
        </div>
    </div>
</div>

<?php
		        }else{
		            echo mensajes("El periodo de ingreso de carga ya finalizó, la modificación se habilitará hasta que finalice la revisión.","azul");            
		        }
		    }else{
		        echo mensajes("El periodo de registro de cargas ya finalizó.","azul");
		    }

	    }else{
	    	echo mensajes("Aún no ha comenzado el registro de cargas de ningun periodo","azul");
	    }
	}else{
    	echo mensajes("Problemas en la página, informe al administrador","rojo");
    } 
}else{
    echo mensajes("Usted no tiene permisos para ver esta página","rojo");
} 
?>