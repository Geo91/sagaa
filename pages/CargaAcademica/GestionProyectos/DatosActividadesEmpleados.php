<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
?>

<div id="divRespuesta"></div>

<div id="divContenido">
    <?php  require_once("tablaEmpleados.php"); ?>
</div>

<div class="modal fade" id="modalActividades" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div id="subContenidoAct">
            </div>
            <div class="modal-footer">
                <button type="button"  class="btn btn-default" data-dismiss="modal">Salir</button>
            </div>
        </div>
    </div>
</div>