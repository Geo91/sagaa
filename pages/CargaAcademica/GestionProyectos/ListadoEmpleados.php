<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    if (isset($_POST['actid'])) {
        $idact = $_POST['actid'];
                
        $query1 = "SELECT empleado.No_Empleado, 
                    CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'nombre', departamento_laboral.nombre_departamento
                FROM empleado,persona,cargo,empleado_has_cargo,departamento_laboral
                WHERE empleado.N_identidad=persona.N_identidad
                    and empleado.Id_departamento=departamento_laboral.Id_departamento_laboral
                    and empleado_has_cargo.No_Empleado=empleado.No_Empleado
                    and empleado_has_cargo.ID_cargo=cargo.ID_cargo
                    and empleado.estado_empleado=1
                    and (cargo.Cargo='Docente' 
                         or cargo.Cargo='Decana'
                         or cargo.Cargo='Jefe de Departamento'
                         or cargo.Cargo='Coordinador de Maestría'
			or cargo.Cargo='Instructor A III')
                    and empleado_has_cargo.Fecha_salida_cargo is null
                    and empleado.No_Empleado in (SELECT ca_profesores.No_Empleado FROM ca_profesores) and empleado.No_Empleado NOT IN (SELECT ca_actividad_empleado.empleado_no FROM ca_actividad_empleado WHERE ca_actividad_empleado.actividad_id = $idact)
                ORDER BY persona.Primer_nombre";

?>

<div class="modal-header" style = "background-color:#0FA6C3">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title" style = "color:white">Agregar Participante</h4>
</div>

<div class="modal-body">
    <table id="tablaEmpleados" class="table table-bordered table-striped">
        <thead>
            <tr>                                            
                <th style="text-align:center;background-color:#386D95;color:white;display:none">Empleado</th>
                <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th>
                <th style="text-align:center;background-color:#386D95;color:white;">Departamento</th>
                <th style="text-align:center;background-color:#386D95;color:white;width:100px">Seleccionar</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $result =$db->prepare($query1);
            $result->execute();
            while ($fila = $result->fetch()) {
                ?>
                <tr data-id='<?php echo $fila["No_Empleado"]; ?>' data-act='<?php echo $idact;  ?>'>
                    <td style = "display:none"><?php echo $fila["No_Empleado"]; ?></td>
                    <td><?php echo $fila["nombre"]; ?></td>
                    <td><?php echo $fila["nombre_departamento"]; ?></td>
                    <td>
                        <center>
                            <button type="button" class="selecccion<?php echo $auto_increment; ?> btn btn-success" title="Seleccionar"><span class ="glyphicon glyphicon-ok"></span></button>
                        </center>
                    </td>
                </tr>
        <?php } ?>
        </tbody>
    </table>

</div>

<script type="text/javascript">

//opciones para buscador en la tabla tablaEmpActividades
    $('#tablaEmpleados')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaEmpleados').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {

        }
        ,
        "bAutoWidth" : false, 
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });    
    
$(document).on("click", ".selecccion<?php echo $auto_increment; ?>", function () {
    data={
        act:$(this).parents("tr").data("act"),
        emp:$(this).parents("tr").data("id")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: actividadAdd,
        timeout: 8000,
        error: problemas
    }); 
    return false;
});

</script>

<script type="text/javascript" src="pages/CargaAcademica/GestionProyectos/Script.js"></script>

<?php  
    }else{
        echo mensajes("Problemas en el servidor","azul");
    }
?>