<?php
    $maindir = "../../../";
    require_once ("../clases.php");
    require_once("../ChekAutoIncrement.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query = "SELECT ca_periodos.id_periodo FROM ca_periodos ORDER BY ca_periodos.id_periodo DESC LIMIT 1;";

    $result =$db->prepare($query);
    $result->execute();
    while ($fila = $result->fetch()) {
        $periodo = $fila['id_periodo'];
        break;
    }
        $query1 = "SELECT empleado.No_Empleado, CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'nombre', departamento_laboral.nombre_departamento  FROM empleado INNER JOIN persona ON empleado.N_identidad=persona.N_identidad INNER JOIN departamento_laboral ON empleado.Id_departamento=departamento_laboral.Id_departamento_laboral  WHERE empleado.No_Empleado IN (SELECT ca_actividad_empleado.empleado_no FROM ca_actividad_empleado WHERE ca_actividad_empleado.actividad_id IN (SELECT ca_actividad.id_actividad FROM ca_actividad WHERE ca_actividad.estado = '1' AND ((ca_actividad.tipo = 'TRIMESTRAL' AND ca_actividad.periodo_id=$periodo) OR ca_actividad.tipo='PERMANENTE')));";
?>

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong> Registro de Empleados con Actividades </strong></center></h3>
    </div>
</div> 

<div id="divRespuesta2"></div>
    
    <div class="box">
        <div class="box-body table-responsive">
            <table id="tablaEmpActividades" class="table table-bordered table-striped">
                <thead>
                    <tr>                                            
                        <th style="text-align:center;background-color:#386D95;color:white;"">No. Emp</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Departamento</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Ver</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Agregar</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $result =$db->prepare($query1);
                    $result->execute();
                    while ($fila = $result->fetch()) {
                        ?>
                        <tr data-id='<?php echo $fila["No_Empleado"]; ?>' data-perid='<?php echo $periodo; ?>'>
                            <td><?php echo $fila["No_Empleado"]; ?></td>
                            <td><?php echo $fila["nombre"]; ?></td>
                            <td><?php echo $fila["nombre_departamento"]; ?></td>
                            <td>
                                <center>
                                    <button type="button" class="VerActividades<?php echo $auto_increment; ?> btn btn-warning glyphicon glyphicon-eye-open"  title="Ver información"></button>
                                </center>
                            </td>       
                        <?php //if($_SESSION['user_rol']!=30){  ?>
                            <td>
                                <center>
                                    <button type="button" class="Agregar<?php echo $auto_increment; ?> btn btn-info glyphicon glyphicon-plus-sign"  title="Editar informacion"></button>
                                </center>
                            </td>
                        <?php// } ?>    
                        </tr>
                <?php } ?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div>

<script type="text/javascript">   
//opciones para buscador en la tabla tablaEmpActividades
    $('#tablaEmpActividades')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaEmpActividades').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
    
$(document).on("click", ".VerActividades<?php echo $auto_increment; ?>", function () {
    data={
        emp:$(this).parents("tr").data("id"),
        idP:$(this).parents("tr").data("perid")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: verActividades,
        timeout: 8000,
        error: problemas
    }); 
    return false;
});

$(document).on("click", ".Agregar<?php echo $auto_increment; ?>", function () {
    data={
        emp:$(this).parents("tr").data("id"),
        idP:$(this).parents("tr").data("perid")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: agregarActividad,
        timeout: 8000,
        error: problemas
    }); 
    return false;
});

</script>
<script type="text/javascript" src="pages/CargaAcademica/GestionProyectos/Script.js"></script>