<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    if (isset($_POST['emp']) && isset($_POST['idP'])) {
        $empid = $_POST['emp'];
        $perid = $_POST['idP'];
        $query = "SELECT empleado.No_Empleado, CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombre FROM empleado,persona
                WHERE empleado.N_identidad = persona.N_identidad AND empleado.No_Empleado = $empid";
                
        $query1 = "SELECT ca_actividad.id_actividad, ca_actividad.descripcion, ca_actividad.tipo FROM ca_actividad INNER JOIN (ca_actividad_empleado INNER JOIN empleado ON empleado.No_Empleado=ca_actividad_empleado.empleado_no) ON ca_actividad.id_actividad=ca_actividad_empleado.actividad_id  WHERE empleado.No_Empleado = $empid AND ca_actividad.estado = '1' AND ((ca_actividad.tipo = 'TRIMESTRAL' AND ca_actividad.periodo_id=$perid) OR ca_actividad.tipo='PERMANENTE')";

        $result =$db->prepare($query);
        $result->execute();
        while ($fila = $result->fetch()) {
        ?>
            <div class="modal-header" style = "background-color:#0FA6C3">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style = "color:white"><?php echo $fila["nombre"]; ?></h4>
            </div>
        <?php
            break;
        }
?>
<div class="modal-body">
    <table id="tablaActividadesEmp" class="table table-bordered table-striped">
        <thead>
            <tr>                                            
                <th style="text-align:center;background-color:#386D95;color:white;display:none">ID</th>
                <th style="text-align:center;background-color:#386D95;color:white;">Descripción</th>
                <th style="text-align:center;background-color:#386D95;color:white;">Tipo</th>
                <th style="text-align:center;background-color:#386D95;color:white;">Remover</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $result =$db->prepare($query1);
            $result->execute();
            while ($fila = $result->fetch()) {
                ?>
                <tr data-id='<?php echo $fila["id_actividad"]; ?>' data-emp='<?php echo $empid;  ?>'>
                    <td style = "display:none"><?php echo $fila["id_actividad"]; ?></td>
                    <td><?php echo $fila["descripcion"]; ?></td>
                    <td><?php echo $fila["tipo"]; ?></td>
                    <td>
                        <center>
                            <button type="button" class="remover<?php echo $auto_increment; ?> btn btn-danger" title="Ver información"><span class ="glyphicon glyphicon-remove"></span></button>
                        </center>
                    </td>
                </tr>
        <?php } ?>
        </tbody>
    </table>

</div>

<script type="text/javascript">       
    
$(document).on("click", ".remover<?php echo $auto_increment; ?>", function () {
    data={
        act:$(this).parents("tr").data("id"),
        emp:$(this).parents("tr").data("emp")
    }
    $.ajax({
        async: true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        success: actividadRemove,
        timeout: 8000,
        error: problemas
    }); 
    return false;
});

</script>

<script type="text/javascript" src="pages/CargaAcademica/GestionProyectos/Script.js"></script>

<?php  
    }else{
        echo mensajes("Problemas en el servidor","azul");
    }
?>