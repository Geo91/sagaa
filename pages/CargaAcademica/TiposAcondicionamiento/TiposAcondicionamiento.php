<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
?>

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong>Gestión de Tipos de Acondicionamientos</strong></center></h3>
    </div>
</div>

<div  id="divRespuesta"></div>     

<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Agregar nuevo tipo de acondicionamiento</div>

    <div class="panel-body" >
        <div class="row">
            <div class="col-lg-12">
            <!--************************formulario*********************-->
                <form class="form-horizontal" role="form" id="formTipoAcond" name="formTipoAcond">

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Nombre</label>                       
                        <div class="col-sm-6">                            
                            <input type="text" class="form-control" id="NombreTipoAcond" name="NombreTipoAcond" required>      
                        </div>                  
                    </div>

                    <div class="row">
                        <label class="control-label col-sm-2"></label>
                        <div class="col-sm-6">
                            <p aling ="right">
                                <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" ><span class=" glyphicon glyphicon-floppy-disk"></span> Agregar</button> 
                            </p>          
                        </div>                            
                    </div>    

                </form>
            <!--************************formulario*********************-->
            </div>
        </div>
    </div>                                    
</div> 

<!--tabla de asignaturas-->
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center> Historial de Tipos de acondicionamiento </center></h3>
    </div>
</div> 
<div id="divRespuestaEditar">
</div>
<div id="datosTabla">
<?php require_once("DatosTiposAcondicionamiento.php"); ?>
</div>
<!--******************modal de edicion*****************-->
<div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header" style = "background-color:#0FA6C3">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style = "color:white">Editar tipo de discapacidad</h4>
        </div>
        <div class="modal-body">
            <!--************************formulario*********************-->
                <form class="form-horizontal" role="form" id="formEditar" name="formEditar">

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Código</label>                       
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control" id="modalCodigo" name="modalCodigo" required disabled="true">              
                        </div>                  
                    </div>
                    
                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Nombre</label>                       
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control" id="modalNombre" name="modalNombre" required>      
                        </div>                  
                    </div>

                    <div class="modal-footer">
                        <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button  id="guardarCambios" class="btn btn-primary" > Guardar</button>
                    </div>
                </form>
            <!--************************formulario*********************-->
        </div>
      </div>
    </div>
</div>
<!--******************modal de edicion*****************-->
<script type="text/javascript" src="pages/CargaAcademica/TiposAcondicionamiento/Script.js"></script>