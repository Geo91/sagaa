$(document).ready(function(){   

//para cuando se da submit al formulario formAsignatura
    $("#formTipoAcond").submit(function(e) {
        e.preventDefault();
        data={
                    Nombre:$('#NombreTipoAcond').val()                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: agregarTipoAcond,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });


//para cuando se da submit al formulario formAsignatura
    $("#formEditar").submit(function(e) {
        e.preventDefault();
        data={
                    Codigo:$('#modalCodigo').val(),
                    Nombre:$('#modalNombre').val()
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: editarTipoAcond,
            timeout: 4000,
            error: problemas
        }); 
        $("#modalEditar").modal('hide');
        return false;
    });

});

//para editar una asignatura
$(document).on("click", ".editarTipoAcond", function () {
    codigo = $(this).parents("tr").find("td").eq(0).html();
    Nombre = $(this).parents("tr").find("td").eq(1).html();

    $("#modalCodigo").val(codigo);
    $("#modalNombre").val(Nombre);

    $("#modalEditar").modal('show');
});


function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function agregarTipoAcond(){
    $("#divRespuesta").load('pages/CargaAcademica/TiposAcondicionamiento/GuardarTipoAcondicionamiento.php',data);
    $('#formTipoAcond').trigger("reset");
}

function editarTipoAcond(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/TiposAcondicionamiento/EditarTiposAcondicionamiento.php',data);
}

function eliminarTipoAcond(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/TiposAcondicionamiento/EliminarTiposAcondicionamiento.php',data);
}