<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Nombre']) ){
        
        $Nombre = $_POST['Nombre'];

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query0 = "SELECT *
                FROM ca_tipos_acondicionamientos
                WHERE ca_tipos_acondicionamientos.nombre='$Nombre'";
        $query = "INSERT INTO `ca_tipos_acondicionamientos` (`id_acondicionamiento`, `nombre`) VALUES (NULL, '$Nombre');";

        try{
            $result =$db->prepare($query0);
            $result->execute();
            
            $bandera = false;
            while ($fila = $result->fetch()) {
                $bandera = true;
            }
            if($bandera){
                $mensaje = "El tipo de acondicionamiento `$Nombre` ya existe.";
                $color = "azul";
            }
            else{
                $result =$db->prepare($query);
                $result->execute();
                $mensaje = "Tipo de acondicionamiento `$Nombre` agregado";
                $color = "verde";
                ?>
                    <script type="text/javascript">
                        $("#datosTabla").load('pages/CargaAcademica/TiposAcondicionamiento/DatosTiposAcondicionamiento.php');
                    </script>
                <?php 
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>