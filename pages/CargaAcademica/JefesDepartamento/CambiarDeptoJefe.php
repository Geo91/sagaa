<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['IdUsuarioJefe']) &&
        isset($_POST['selectDeptosJefe']) ){
        
        $IdUsuarioJefe = $_POST['IdUsuarioJefe'];
        $selectDeptosJefe = $_POST['selectDeptosJefe'];

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO

        $query = "SELECT * FROM ca_jefes_departamentos WHERE ca_jefes_departamentos.id_departamento=$selectDeptosJefe";

        try{
            $result =$db->prepare($query);
            $result->execute();
            $existeDepto = false;
            while ($fila = $result->fetch()) {
                $existeDepto = true;
                break;
            }
            if ($existeDepto) {
                $mensaje = "Ya hay un usuario como jefe de este departamento.";
                $color = "azul";
            }else{
                $query = "UPDATE ca_jefes_departamentos
                        SET ca_jefes_departamentos.id_departamento=$selectDeptosJefe
                        WHERE ca_jefes_departamentos.id_Usuario=$IdUsuarioJefe";

                $result =$db->prepare($query);
                $result->execute();
                $mensaje = "Se cambiado el departamento al usuario";
                $color = "verde";
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }else{
        echo mensajes("Problemas con la conexión","azul");
    }
?>
<script type="text/javascript">
    $("#datosTabla").load('pages/CargaAcademica/JefesDepartamento/DatosJefeDepartamento.php');
</script>