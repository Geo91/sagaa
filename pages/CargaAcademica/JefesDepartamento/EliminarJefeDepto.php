<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['IdUsuarioJefe']) ){
        try{
            $IdUsuarioJefe = $_POST['IdUsuarioJefe'];

            $query = "DELETE FROM `ca_jefes_departamentos` WHERE `ca_jefes_departamentos`.`id_Usuario` = $IdUsuarioJefe";

            $result =$db->prepare($query);
            $result->execute();

            echo mensajes("Se ha eliminado el departamento del empleado No.$IdUsuarioJefe","verde");
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>
<script type="text/javascript">
    $("#divFormulario").load('pages/CargaAcademica/JefesDepartamento/FormJefeDepartamento.php');
    $("#datosTabla").load('pages/CargaAcademica/JefesDepartamento/DatosJefeDepartamento.php');
</script>