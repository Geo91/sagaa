
$(document).ready(function(){   
    $("#formEditarJefeDepartamento").submit(function(e) {
        e.preventDefault();
        data={
                    IdUsuarioJefe:$('#IdUsuarioJefe').val(),
                    selectDeptosJefe:$('#selectDeptosJefe').val()
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: cambiarDepto,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });
});

function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function guardarJefeDepto(){
    $("#divRespuesta").empty();
    $("#divRespuesta").load('pages/CargaAcademica/JefesDepartamento/GuardarJefeDepartamento.php',data);
}

function cambiarDepto(){
    $("#divRespuestaEditar").empty();
    $("#divRespuestaEditar").load('pages/CargaAcademica/JefesDepartamento/CambiarDeptoJefe.php',data);
    $("#modalEditar").modal('hide');
}

function removerJefe(){
    $("#divRespuestaEditar").empty();
    $("#divRespuestaEditar").load('pages/CargaAcademica/JefesDepartamento/EliminarJefeDepto.php',data);
}