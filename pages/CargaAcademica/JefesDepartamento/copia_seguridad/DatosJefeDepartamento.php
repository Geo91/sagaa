<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query1 = "SELECT usuario.id_Usuario,
                    usuario.nombre as nombreU,
                    usuario.No_Empleado,
                    empleado.N_identidad,
                    CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombre,
                    ca_departamentos_carreras.id_departamento,
                    ca_departamentos_carreras.nombre as nombreD
                FROM ca_jefes_departamentos,ca_departamentos_carreras,usuario,empleado,persona
                WHERE ca_jefes_departamentos.id_departamento=ca_departamentos_carreras.id_departamento
                    and ca_jefes_departamentos.id_Usuario=usuario.id_Usuario
                    and usuario.No_Empleado=empleado.No_Empleado
                    and empleado.N_identidad=persona.N_identidad
                    and usuario.Estado=1";
?>
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaJefesDepartamento" class="table table-bordered table-striped">
            <thead>
                <tr>                                             
                    <th>Usuario</th>                       
                    <th>No Empleado</th>
                    <th>Identidad</th>
                    <th>Nombre</th>
                    <th>Departamento</th>
                    <th>Cambiar departamento</th>
                    <th>Remover</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $result =$db->prepare($query1);
                $result->execute();
                while ($fila = $result->fetch()) {
                    ?>
                    <tr data-id='<?php echo $fila["id_Usuario"]; ?>'
                        data-departamento='<?php echo $fila["id_departamento"]; ?>'>
                        <td><?php echo $fila["nombreU"]; ?></td>
                        <td><?php echo $fila["No_Empleado"]; ?></td>
                        <td><?php echo $fila["N_identidad"]; ?></td>
                        <td><?php echo $fila["nombre"]; ?></td>
                        <td><?php echo $fila["nombreD"]; ?></td>
                        <td>
                            <center>
                                <button type="button" class="CambiarDepartamento<?php echo $auto_increment; ?> btn btn-primary glyphicon glyphicon-edit"  title="Cambiar departamento"></button>
                            </center>
                        </td>        
                        <td>
                            <center>
                                <button type="button" class="RemoverJefe<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-remove"  title="Cambiar departamento"></button>
                            </center>
                        </td>        
                    </tr>
            <?php } ?>
            </tbody>
            <tfoot>
                <tr>                                      
                    <th>Usuario</th>            
                    <th>No Empleado</th>
                    <th>Identidad</th>
                    <th>Nombre</th>
                    <th>Departamento</th>
                    <th>Cambiar departamento</th>
                    <th>Remover</th>
                </tr>
            </tfoot>
        </table>
    </div><!-- /.box-body -->
</div>
<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#tablaJefesDepartamento')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaJefesDepartamento').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
$(document).on("click",".CambiarDepartamento<?php echo $auto_increment; ?>", function () {

    $('#IdUsuarioJefe').val($(this).parents("tr").data("id"));
    $('#NombreUJefe').val($(this).parents("tr").find("td").eq(0).html());
    $('#NoEmpleadoJefe').val($(this).parents("tr").find("td").eq(1).html());
    $('#IdentidadJefe').val($(this).parents("tr").find("td").eq(2).html());
    $('#NombreJefe').val($(this).parents("tr").find("td").eq(3).html());
    $('#selectDeptosJefe').val($(this).parents("tr").data("departamento"));

    $("#modalEditar").modal('show');
});
$(document).on("click",".RemoverJefe<?php echo $auto_increment; ?>", function () {
    if (confirm("¿Esta seguro que desea remover el departamento de este usuario?")) {
        data={
                    IdUsuarioJefe:$(this).parents("tr").data("id")
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: removerJefe,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    }
});
</script>