<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir . "conexion/config.inc.php");

    $query1 = "SELECT * FROM `ca_departamentos_carreras`";

    $query = "SELECT usuario.id_Usuario,
                usuario.No_Empleado,
                usuario.nombre as nombreUsuario,
                CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombrePersona
            FROM usuario,empleado,persona
            WHERE usuario.No_Empleado=empleado.No_Empleado
                and empleado.N_identidad=persona.N_identidad
                and usuario.Id_Rol=30
                and usuario.Estado=1
                and usuario.id_Usuario not in 
                    (SELECT ca_jefes_departamentos.id_Usuario
                        FROM ca_jefes_departamentos)
            ORDER BY persona.Primer_nombre";
?>

<!--************************formulario*********************-->
    <form class="form-horizontal" role="form" id="formNuevoJefeDepto" name="formNuevoJefeDepto">

        <div class="row form-group">
            <label class=" col-sm-2 control-label" >Usuario</label>
            <div class="col-sm-8">                            
                <select class="form-control" id="IDNuevoJefeDepto" name="IDNuevoJefeDepto" required>
                    <option value="">Seleccione una opción</option>
                    <?php
                         $result =$db->prepare($query);
                         $result->execute();
                        while ($fila = $result->fetch()) {
                            ?>
                            <option value="<?php echo $fila["id_Usuario"] ?>">
                            <?php 
                                $largo = strlen($fila["No_Empleado"]);
                                if ($largo<=7) {
                                    $espacio = str_repeat ( "&nbsp", 7-$largo);
                                }else{
                                    $espacio=" ";
                                }
                                echo $fila["nombreUsuario"]." / ".$fila["nombrePersona"]; ?></option>
                                <?php
                            }
                        ?>
                </select>
            </div>
        </div>

        <div class="row form-group">
            <label class=" col-sm-2 control-label" >Departamento</label>
            <div class="col-sm-6">                            
                <select class="form-control" id="selectDepartamento" name="selectDepartamento"  title="seleccione una o más opciones" required>
                    <option value="">Seleccione una opcion</option>
                    <?php
                         $result =$db->prepare($query1);
                         $result->execute();
                        while ($fila = $result->fetch()) {
                            ?>
                            <option value="<?php echo $fila["id_departamento"]; ?>"><?php echo $fila["nombre"]; ?></option>
                            <?php
                        }
                    ?>
                </select>
            </div>
        </div>

        <div class="row">
            <label class="control-label col-sm-2"></label>
            <div class="col-sm-8">
                <p aling ="right">
                    <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" ><span class=" glyphicon glyphicon-floppy-disk"></span> Agregar</button> 
                </p>          
            </div>                            
        </div>    

    </form>
<!--************************formulario*********************-->
<script type="text/javascript">
$(document).ready(function(){   

//para cuando se da submit al formulario formAsignatura
    $("#formNuevoJefeDepto").submit(function(e) {
        e.preventDefault();
        data={
                    IdUsuarioJefe:$('#IDNuevoJefeDepto').val(),
                    IdDepartamentoJefe:$('#selectDepartamento').val()
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: guardarJefeDepto,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });

});
</script>