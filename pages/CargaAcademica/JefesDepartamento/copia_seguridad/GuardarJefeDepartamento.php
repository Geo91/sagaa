<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['IdUsuarioJefe']) && 
        isset($_POST['IdDepartamentoJefe']) ){
        try{
            $IdUsuarioJefe = $_POST['IdUsuarioJefe'];
            $IdDepartamentoJefe = $_POST['IdDepartamentoJefe'];

            $query = "SELECT * FROM ca_jefes_departamentos WHERE ca_jefes_departamentos.id_departamento=$IdDepartamentoJefe";

            $result =$db->prepare($query);
            $result->execute();
            $existeDepto = false;
            while ($fila = $result->fetch()) {
                $existeDepto = true;
                break;
            }
            if ($existeDepto) {
                $mensaje = "Ya hay un usuario como jefe de este departamento.";
                $color = "azul";
            }else{
                $query = "INSERT INTO `ca_jefes_departamentos` (`id_Usuario`, `id_departamento`) VALUES ('$IdUsuarioJefe', '$IdDepartamentoJefe');";
                $result =$db->prepare($query);
                $result->execute();
                $mensaje = "Datos agregados correctamente.";
                $color = "verde";
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>
<script type="text/javascript">
    $("#divFormulario").load('pages/CargaAcademica/JefesDepartamento/FormJefeDepartamento.php');
    $("#datosTabla").load('pages/CargaAcademica/JefesDepartamento/DatosJefeDepartamento.php');
</script>