<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
        require_once($maindir."conexion/config.inc.php");
    
    $queryDeptos = "SELECT * FROM `ca_departamentos_carreras`";
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Jefes de departamento:</h1>
    </div>
</div>         
<div  id="divRespuesta"></div> 
<div class="panel panel-default">
    <div class="panel-heading"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Agregar departamento a usuario</div>

    <div class="panel-body" >
        <div class="row">
            <div class="col-lg-12">
                <div id="divFormulario">
                    <?php require_once("FormJefeDepartamento.php"); ?>
                </div>
            </div>
        </div>
    </div>                                    
</div> 

<!--tabla de jefes por departameno-->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Usuarios jefes de departametno:</h1>
    </div>
</div> 
<div id="divRespuestaEditar">
</div>
<div id="datosTabla">
    <?php require_once("DatosJefeDepartamento.php"); ?>
</div>
<!--******************modal de edicion*****************-->
<div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Cambiar departamento</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" role="form" id="formEditarJefeDepartamento" name="formEditarJefeDepartamento">

                <input type="hidden" class="form-control" id="IdUsuarioJefe" name="IdUsuarioJefe" required>
                
                <div class="row form-group" >                    
                    <label class=" col-sm-2 control-label" >Usuario</label>
                    <div class="col-sm-8">                            
                        <input type="text" class="form-control" id="NombreUJefe" name="NombreUJefe" disabled="true" required>      
                    </div>                  
                </div>

                <div class="row form-group" >                    
                    <label class=" col-sm-2 control-label" >No Empleado</label>
                    <div class="col-sm-8">                            
                        <input type="text" class="form-control" id="NoEmpleadoJefe" name="NoEmpleadoJefe" disabled="true" required>      
                    </div>                  
                </div>

                <div class="row form-group" >                    
                    <label class=" col-sm-2 control-label" >Identidad</label>
                    <div class="col-sm-8">           
                        <input type="text" class="form-control" id="IdentidadJefe" name="IdentidadJefe"  disabled="true" required>           
                    </div>                  
                </div>

                <div class="row form-group" >                    
                    <label class=" col-sm-2 control-label" >Nombre</label>
                    <div class="col-sm-8">           
                        <input type="text" class="form-control" id="NombreJefe" name="NombreJefe"  disabled="true" required>           
                    </div>                  
                </div>

                <div class="row form-group">
                    <label class=" col-sm-2 control-label" >Departamento</label>
                    <div class="col-sm-8">                            
                        <select class="form-control" id="selectDeptosJefe" name="selectDeptosJefe" required>
                            <option value="">Seleccione una opcion</option>
                            <?php
                                $result =$db->prepare($queryDeptos);
                                $result->execute();
                                while ($fila = $result->fetch()) {
                            ?> 
                                    <option value="<?php echo $fila['id_departamento']; ?>"><?php echo $fila["nombre"]; ?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button  id="guardarCambios" class="btn btn-primary" ><span class=" glyphicon glyphicon-floppy-disk"></span>Guardar</button>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>
<!--******************modal de edicion*****************-->
<script type="text/javascript" src="pages/CargaAcademica/JefesDepartamento/Script.js"></script>