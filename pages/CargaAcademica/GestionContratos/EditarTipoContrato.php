<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Codigo']) && 
        isset($_POST['Nombre'])&& 
        isset($_POST['uv']) && isset($_POST['hIntermedia'])){
        
        $Codigo = $_POST['Codigo'];
        $Nombre = $_POST['Nombre'];
        $uv = $_POST['uv'];
        $hIntermedia = $_POST['hIntermedia'];

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query = "UPDATE ca_tipos_contratos set ca_tipos_contratos.nombre='$Nombre', ca_tipos_contratos.uv=$uv, ca_tipos_contratos.horaIntermedia=$hIntermedia WHERE ca_tipos_contratos.id_contrato=$Codigo";

        try{
            $result =$db->prepare($query);
            $result->execute();
            echo mensajes("El tipo de contrato ID.$Codigo ha sido actualizado.","verde");
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>
<script type="text/javascript">
    $("#datosTabla").load('pages/CargaAcademica/GestionContratos/DatosTiposContratos.php');
</script>