<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
?>

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong>Gestión de Tipos de Contrato</strong></center></h3>
    </div>
</div> 

<div  id="divRespuesta"></div>         
<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Agregar nuevo tipo de contrato</div>

    <div class="panel-body" >
        <div class="row">
            <div class="col-lg-12">
            <!--************************formulario*********************-->
                <form class="form-horizontal" role="form" id="formNuevoTipoContrato" name="formNuevoTipoContrato">

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Nombre</label>                       
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="NombreContrato" name="NombreContrato"  required>              
                        </div>                  
                    </div>

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Tipo de carga </label>                       
                        <div class="col-sm-8">
                            <select id="TipoCargaUv" name="TipoCargaUv" size="3" required>
                                <option value="1" selected>Uv's Fijas</option>
                                <option value="0">Uv's No requeridas</option>
                                <option value="-1">Uv's Variables</option>
                            </select>            
                        </div>                  
                    </div>


                    <div class="row form-group" > 
                        <label class=" col-sm-2 control-label"> Hora Intermedia </label>  
                        <div class="col-sm-8">
                            <select id="horaIntermedia" name="horaIntermedia" size="2" required>
                                <option value="1" selected>Si</option>
                                <option value="0">No</option>
                            </select>
                        </div>               
                    </div>

                    <div id="tempUvContrato" class="row form-group" > 
                        <label class=" col-sm-2 control-label" >Unidades valorativas</label>                       
                        <div class="col-sm-8">                   
                            <input type="number" id="CantidadUvContrato" name="uv" min="1" max="30" value="1" required>
                        </div>                  
                    </div>

                    <div class="row">
                        <label class="control-label col-sm-2"></label>
                        <div class="col-sm-8">
                            <p aling ="right">
                                <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" ><span class=" glyphicon glyphicon-floppy-disk"></span> Agregar</button> 
                            </p>          
                        </div>                            
                    </div>    

                </form>
            <!--************************formulario*********************-->
            </div>
        </div>
    </div>                                    
</div> 

<!--tabla de asignaturas-->
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center> Historial de Tipos de Contrato </center></h3>
    </div>
</div> 
<div id="divRespuestaEditar">
</div>
<div id="datosTabla">
</div>
<!--******************modal de edicion*****************-->
<div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header" style = "background-color:#0FA6C3">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style = "color:white">Editar tipo de contrato</h4>
        </div>
        <div class="modal-body">
            <!--************************formulario*********************-->
                <form class="form-horizontal" role="form" id="formEditar" name="formEditar">

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >ID</label>                       
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control" id="idTipoContrato" name="idTipoContrato" required disabled="true">              
                        </div>                  
                    </div>
                    
                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Nombre</label>                       
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control" id="ModalNombreTipoContrato" name="ModalNombreTipoContrato" required>      
                        </div>                  
                    </div>

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Tipo de carga </label>                       
                        <div class="col-sm-8">
                            <select id="ModalTipoCargaUv" name="ModalTipoCargaUv" size="3" required>
                                <option value="1" selected>Uv's Fijas</option>
                                <option value="0">Uv's No requeridas</option>
                                <option value="-1">Uv's Variables</option>
                            </select>            
                        </div>                  
                    </div>

                    <div id="tempUvContrato" class="row form-group" > 
                        <label class=" col-sm-2 control-label"> Hora Intermedia </label>  
                        <div class="col-sm-8">
                            <select id="modalHoraIntermedia" name="modalHoraIntermedia" size="2" required>
                                <option value="1" selected>Si</option>
                                <option value="0">No</option>
                            </select>
                        </div>               
                    </div>

                    <div id="tempUvContratoEditar" class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Unidades valorativas</label>                       
                        <div class="col-sm-8">                   
                            <input type="number" id="ModalTipoContratoUV" name="ModalTipoContratoUV" min="1" max="30" value="1" required>
                        </div>                  
                    </div>

                    <div class="modal-footer">
                        <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button  id="guardarCambios" class="btn btn-primary" >Guardar</button>
                    </div>
                </form>
            <!--************************formulario*********************-->
        </div>
      </div>
    </div>
</div>
<!--******************modal de edicion*****************-->
<script type="text/javascript" src="pages/CargaAcademica/GestionContratos/Script.js"></script>