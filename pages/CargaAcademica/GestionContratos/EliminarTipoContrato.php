<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Codigo']) ){
        
        $Codigo = $_POST['Codigo'];

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query0 = "SELECT * FROM `ca_profesores` WHERE `id_contrato` = $Codigo";
        $query = "DELETE FROM `ca_tipos_contratos` WHERE `id_contrato` = $Codigo";

        try{
            $result =$db->prepare($query0);
            $result->execute();
            
            $bandera = true;
            while ($fila = $result->fetch()) {
                $bandera = false;
                break;
            }
            if($bandera){
                $result =$db->prepare($query);
                $result->execute();
                $mensaje = "Tipo de contrato ID.$Codigo eliminado.";
                $color = "verde";
            }
            else{
                $mensaje="No se puede eliminar el tipo de cotrato ID.$Codigo ya que hay empleados que actualmente poseen este tipo de contrato.";
                $color = "azul";
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>
<script type="text/javascript">
    $("#datosTabla").load('pages/CargaAcademica/GestionContratos/DatosTiposContratos.php');
</script>