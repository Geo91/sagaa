<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Nombre']) && 
        isset($_POST['uv']) ){
        
        $Nombre = $_POST['Nombre'];
        $uv = $_POST['uv'];

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query0 = "SELECT * FROM `ca_tipos_contratos` WHERE `nombre`='$Nombre'";
        $query = "INSERT INTO `ca_tipos_contratos` (`id_contrato`, `nombre`, `uv`) VALUES (NULL, '$Nombre', '$uv');";

        try{
            $result =$db->prepare($query0);
            $result->execute();
            
            $bandera = true;
            while ($fila = $result->fetch()) {
                $bandera = false;
                break;
            }
            if($bandera){
                $result =$db->prepare($query);
                $result->execute();
                $mensaje = "Tipo de contrato agregado";
                $color = "verde";
            }
            else{
                $mensaje="Ya existe un contrato con el nombre `$Nombre`";
                $color = "azul";
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>
<script type="text/javascript">
    $("#datosTabla").load('pages/CargaAcademica/GestionContratos/DatosTiposContratos.php');
</script>