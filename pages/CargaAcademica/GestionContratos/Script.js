$("#datosTabla").load('pages/CargaAcademica/GestionContratos/DatosTiposContratos.php');

$(document).ready(function(){   

    $("#formNuevoTipoContrato").submit(function(e) {
        e.preventDefault();
        data={
            Nombre:$('#NombreContrato').val(),
            uv:$('#CantidadUvContrato').val(),
            hIntermedia:$('#horaIntermedia').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: guardarNuevoTipoContrato,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });

    $("#formEditar").submit(function(e) {
        e.preventDefault();
        data={
            Codigo:$('#idTipoContrato').val(),
            Nombre:$('#ModalNombreTipoContrato').val(),
            uv:$('#ModalTipoContratoUV').val(),
            hIntermedia:$('#modalHoraIntermedia').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: editarTipoContrato,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });

    $("#TipoCargaUv").click(function(event) {
        event.preventDefault();
        var valor = $('#TipoCargaUv').val();
        if (valor==1) {
            $('#tempUvContrato').show('fast');
            document.getElementById("CantidadUvContrato").min = 1;
            $('#CantidadUvContrato').val(1);
            document.getElementById("CantidadUvContrato").disabled = false;
        }else if (valor==0) {
            $('#tempUvContrato').hide('fast');
            document.getElementById("CantidadUvContrato").min = 0;
            $('#CantidadUvContrato').val(0);
            document.getElementById("CantidadUvContrato").disabled = true;
        }else{
            $('#tempUvContrato').hide('fast');
            document.getElementById("CantidadUvContrato").min = -1;
            $('#CantidadUvContrato').val(-1);
            document.getElementById("CantidadUvContrato").disabled = true;
        }
    });

    $("#ModalTipoCargaUv").click(function(event) {
        event.preventDefault();
        var valor = $('#ModalTipoCargaUv').val();
        if (valor==1) {
            $('#tempUvContratoEditar').show('fast');
            document.getElementById("ModalTipoContratoUV").min = 1;
            $('#ModalTipoContratoUV').val(1);
            document.getElementById("ModalTipoContratoUV").disabled = false;
        }else if (valor==0) {
            $('#tempUvContratoEditar').hide('fast');
            document.getElementById("ModalTipoContratoUV").min = 0;
            $('#ModalTipoContratoUV').val(0);
            document.getElementById("ModalTipoContratoUV").value = 0;
            document.getElementById("ModalTipoContratoUV").disabled = true;
        }else{
            $('#tempUvContratoEditar').hide('fast');
            document.getElementById("ModalTipoContratoUV").min = -1;
            $('#ModalTipoContratoUV').val(-1);
            document.getElementById("ModalTipoContratoUV").disabled = true;
        }
    });

});

$(document).on("click", ".editarTipoContrato", function () {
    document.getElementById("CantidadUvContrato").disabled = false;
    $('#idTipoContrato').val($(this).parents("tr").data("id"));
    $('#ModalNombreTipoContrato').val($(this).parents("tr").find("td").eq(1).html());

    var valor = $(this).data("id");
    if (valor>0) {
        $('#tempUvContratoEditar').show();
        $('#ModalTipoCargaUv').val(1);
        document.getElementById("ModalTipoContratoUV").min = 1;
        document.getElementById("ModalTipoContratoUV").disabled = false;
    }else if (valor==0) {
        $('#tempUvContratoEditar').hide();
        $('#ModalTipoCargaUv').val(0);
        document.getElementById("ModalTipoContratoUV").min = 0;
        document.getElementById("ModalTipoContratoUV").disabled = true;
    }else{
        $('#tempUvContratoEditar').hide();
        $('#ModalTipoCargaUv').val(-1);
        document.getElementById("ModalTipoContratoUV").min = -1;
        document.getElementById("ModalTipoContratoUV").disabled = true;
    }
    $('#ModalTipoContratoUV').val(valor);
    $("#modalEditar").modal('show');
});

function inicioEnvio(){
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function guardarNuevoTipoContrato(){
    $("#divRespuesta").load('pages/CargaAcademica/GestionContratos/GuardarTipoContrato.php',data);
    document.getElementById("CantidadUvContrato").disabled = false;
    $('#formNuevoTipoContrato').trigger("reset");
}

function eliminarTipoContrato(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/GestionContratos/EliminarTipoContrato.php',data);
}

function editarTipoContrato(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/GestionContratos/EditarTipoContrato.php',data);
    $("#modalEditar").modal('hide');
}