<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query1 = "SELECT * FROM `ca_tipos_contratos`";
?>
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaTiposContrato" class="table table-bordered table-striped">
            <thead>
                <tr>                                            
                    <th style="text-align:center;background-color:#386D95;color:white;display:none">ID</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">UV's</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Hora Intermedia</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Editar</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Eliminar</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $result =$db->prepare($query1);
                $result->execute();
                while ($fila = $result->fetch()) {
                    ?>
                    <tr data-id='<?php echo $fila["id_contrato"]; ?>'>
                        <td style = "display:none"><?php echo $fila["id_contrato"]; ?></td>
                        <td><?php echo $fila["nombre"]; ?></td>
                        <td>
                            <?php if ($fila["uv"]>1){
                                echo $fila["uv"];
                            }elseif ($fila["uv"]==0) {
                                echo "N/A";
                            }else{
                                echo "Varía";
                            }
                            ?>
                        </td>
                        <td>
                            <?php if ($fila["horaIntermedia"]==1){
                                echo "Si aplica";
                            }else{
                                echo "No aplica";
                            }
                            ?>
                        </td>
                        <td>
                            <center>
                                <button data-id='<?php echo $fila["uv"]; ?>' type="button" class="editarTipoContrato btn btn-info glyphicon glyphicon-edit"  title="Editar tipo de contrato">
                            </center>
                        </td>       
                        <td>
                            <center>
                                <button type="button" class="EliminarTipoContrato<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-remove"  title="Eliminar tipo de contrato">
                            </center>
                        </td>     
                    </tr>
            <?php } ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div>
<script type="text/javascript">   
//opciones para buscador en la tabla tablaTiposContrato
    $('#tablaTiposContrato')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaTiposContrato').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
$(document).on("click", ".EliminarTipoContrato<?php echo $auto_increment; ?>", function () {
    if (confirm("¿Está seguro de que desea eliminar este tipo de contrato?")){
        codigo = $(this).parents("tr").data("id");
        data={
                    Codigo:codigo
            }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: eliminarTipoContrato,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    }
});

</script>