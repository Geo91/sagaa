<?php
    $maindir = "../../../../";
    require_once ("../../clases.php");
    require_once("../../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO

if (isset($_POST['RepoProy']) &&
    isset($_POST['RepoArea']) &&
    isset($_POST['RepoProf']) &&
    isset($_POST['RepoTipoFecha']) &&
    isset($_POST['fechaInicio']) &&
    isset($_POST['fechaFin']) ) {

    $DatosProy= split("/", $_POST['RepoProy']);
    $RepoProy = $DatosProy[0];
    $RepoProyN = $DatosProy[1];

    $DatosArea= split("/", $_POST['RepoArea']);
    $RepoArea = $DatosArea[0];
    $RepoAreaN = $DatosArea[1];
    
    $DatosProf= split("/", $_POST['RepoProf']);
    $RepoProf = $DatosProf[0];
    $RepoProfN = $DatosProf[1];
    
    $RepoTipoFecha = $_POST['RepoTipoFecha'];
    $fechaInicio = $_POST['fechaInicio'];
    $fechaFin = $_POST['fechaFin'];

    $query1 = "SELECT empleado.No_Empleado, 
    persona.N_identidad,
    CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombreE,
    ca_proyectos.nombre as nombreP,
    ca_tipos_proyectos.nombre as Area,
    ca_proyectos.fecha_inicio,
    ca_proyectos.fecha_fin
FROM ca_proyectos,ca_profesores_proyectos,empleado,persona,ca_tipos_proyectos
WHERE ca_proyectos.id_proyecto=ca_profesores_proyectos.id_proyecto
    and ca_proyectos.id_tipo_proyecto=ca_tipos_proyectos.id_tipo_proyecto
    and ca_profesores_proyectos.No_Empleado=empleado.No_Empleado
    and persona.N_identidad=empleado.N_identidad";

    if ($RepoProf!=-1) {
        $query1 = $query1." and ca_profesores_proyectos.No_Empleado='$RepoProf' ";
    }
     
    if ($RepoProy!=-1) {
        $query1 = $query1." and ca_proyectos.id_proyecto=$RepoProy ";
    }  

    if ($RepoArea!=-1) {
        $query1 = $query1." and ca_tipos_proyectos.id_tipo_proyecto=$RepoArea ";
    }

    if ($RepoTipoFecha!=-1) {
        $query1 = $query1." and (('$fechaInicio' BETWEEN ca_proyectos.fecha_inicio
                                            and ca_proyectos.fecha_fin)
                                 or ('$fechaFin' BETWEEN ca_proyectos.fecha_inicio
                                            and ca_proyectos.fecha_fin)
                                 or (ca_proyectos.fecha_inicio BETWEEN '$fechaInicio' and '$fechaFin'  )) ";
    }
?>
<div class="row">
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaRepoProy" class="table table-bordered table-striped">
            <thead>
                <tr>                                            
                    <th>No Empleado</th>
                    <th>Identidad</th>
                    <th>Nombre</th>
                    <th>Proyecto</th>
                    <th>Área</th>
                    <th>Fecha inicio</th>
                    <th>Fecha finalización</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $result =$db->prepare($query1);
                $result->execute();
                while ($fila = $result->fetch()) {
                    ?>
                    <tr>
                        <td><?php echo $fila['No_Empleado']; ?></td>
                        <td><?php echo $fila['N_identidad']; ?></td>
                        <td><?php echo $fila['nombreE']; ?></td>
                        <td><?php echo $fila['nombreP']; ?></td>
                        <td><?php echo $fila['Area']; ?></td>
                        <td><?php echo $fila['fecha_inicio']; ?></td>
                        <td><?php echo $fila['fecha_fin']; ?></td>
                    </tr>
            <?php } ?>
            </tbody>
            <tfoot>
                <tr>                                    
                    <th>No Empleado</th>
                    <th>Identidad</th>
                    <th>Nombre</th>
                    <th>Proyecto</th>
                    <th>Área</th>
                    <th>Fecha inicio</th>
                    <th>Fecha finalización</th>
                </tr>
            </tfoot>
        </table>
    </div><!-- /.box-body -->
</div>
</div>
<script type="text/javascript">
     $('#tablaRepoProy')
        .removeClass('display')
        .addClass('table table-striped table-bordered');
    $('#tablaRepoProy').DataTable({
                dom: 'Blfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                download: 'open',
                title:'Proyectos: <?php 
                    if ($RepoProy!=-1) {
                        echo " $RepoProyN ";
                    }  

                    if ($RepoProf!=-1) {
                        echo "de $RepoProfN";
                    } 

                    if ($RepoArea!=-1) {
                        echo " del área de $RepoAreaN";
                    }

                    if ($RepoTipoFecha!=-1) {
                        echo " en proceso entre ".fecha($fechaInicio)." y ".fecha($fechaFin);
                    } ?>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                download: 'open',
                orientation: 'landscape',
                pageSize: 'LETTER',
                title:'Proyectos: <?php 
                    if ($RepoProy!=-1) {
                        echo " $RepoProyN ";
                    }  

                    if ($RepoProf!=-1) {
                        echo "de $RepoProfN";
                    } 

                    if ($RepoArea!=-1) {
                        echo " del área de $RepoAreaN";
                    }

                    if ($RepoTipoFecha!=-1) {
                        echo " en proceso entre ".fecha($fechaInicio)." y ".fecha($fechaFin);
                    } ?>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'print',
                title:'Proyectos: <?php 
                    if ($RepoProy!=-1) {
                        echo " $RepoProyN ";
                    }  

                    if ($RepoProf!=-1) {
                        echo "de $RepoProfN";
                    } 

                    if ($RepoArea!=-1) {
                        echo " del área de $RepoAreaN";
                    }

                    if ($RepoTipoFecha!=-1) {
                        echo " en proceso entre ".fecha($fechaInicio)." y ".fecha($fechaFin);
                    } ?>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'
        ]
    });

    $('.toggle-vis1').on( 'click', function (e) {
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
 
        // Toggle the visibility
        column.visible( ! column.visible() );
        $(this).attr('color', ' blue');
    });
    $(".toggle-vis1").prop("disabled", "");
</script>
<?php }else{
    echo mensajes("Problemas en la página, intentelo de nuevo","azul");
} ?>