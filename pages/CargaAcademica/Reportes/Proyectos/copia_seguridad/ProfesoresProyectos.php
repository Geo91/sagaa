<?php
    $maindir = "../../../../";

    require_once ("../../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    $query = "SELECT empleado.No_Empleado, 
                    CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombre
                FROM empleado,persona,cargo,empleado_has_cargo
                WHERE empleado.N_identidad=persona.N_identidad
                    and empleado_has_cargo.No_Empleado=empleado.No_Empleado
                    and empleado_has_cargo.ID_cargo=cargo.ID_cargo
                    and (cargo.Cargo='Docente' 
                         or cargo.Cargo='Decana'
                         or cargo.Cargo='Jefe de Departamento'
                         or cargo.Cargo='Coordinador de Maestría')
                    and empleado_has_cargo.Fecha_salida_cargo is null
                    and empleado.No_Empleado in (SELECT ca_profesores.No_Empleado FROM ca_profesores) 
                ORDER BY persona.Primer_nombre";

        $query2="SELECT * FROM ca_proyectos";
        $query3="SELECT * FROM ca_tipos_proyectos";

?>

<div id="divRespuesta"></div>
<div class="panel panel-primary">
    <div class="panel-heading"><h4><span class="glyphicon glyphicon-stats" aria-hidden="true"></span> Reporte de profesores con proyectos</h4></div>
    <div class="panel-body" >
        <div class="row">
                <form class="form-horizontal col-sm-12" role="form" id="formRepoCargas" name="formRepoCargas">


            <hr>

                    <div class="row form-group">
                        <label class=" col-sm-2 control-label" >Proyecto: </label>
                        <div class="col-sm-8">                            
                            <select class="form-control" id="RepoProy" name="RepoProy" required>
                                <option value="-1/*">---------------Todos---------------</option>
                                <?php
                                    $result =$db->prepare($query2);
                                    $result->execute();
                                    while ($fila = $result->fetch()) {
                                ?> 
                                        <option value="<?php echo $fila['id_proyecto']."/".$fila["nombre"]; ?>">
                                        <?php echo $fila["nombre"]; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="row form-group">
                        <label class=" col-sm-2 control-label" >Área: </label>
                        <div class="col-sm-8">                            
                            <select class="form-control" id="RepoArea" name="RepoArea" required>
                                <option value="-1/*">---------------Todas---------------</option>
                                <?php
                                    $result =$db->prepare($query3);
                                    $result->execute();
                                    while ($fila = $result->fetch()) {
                                ?> 
                                        <option value="<?php echo $fila['id_tipo_proyecto']."/".$fila["nombre"]; ?>">
                                        <?php echo $fila["nombre"]; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class=" col-sm-2 control-label" >Profesor</label>
                        <div class="col-sm-8">                            
                            <select class="form-control" id="RepoProf" name="RepoProf" required>
                                <option value="-1/*">---------------Todos---------------</option>
                                <?php
                                     $result =$db->prepare($query);
                                     $result->execute();
                                    while ($fila = $result->fetch()) {
                                        ?>
                                        <option value='<?php echo $fila["No_Empleado"]."/".$fila["nombre"]; ?>'>
                        <?php 
                            $largo = strlen($fila["No_Empleado"]);
                            if ($largo<=7) {
                                $espacio = str_repeat ( "&nbsp", 7-$largo);
                            }else{
                                $espacio=" ";
                            }
                            echo "█ No.".$fila["No_Empleado"].$espacio."█ &nbsp&nbsp".$fila["nombre"]; ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="row form-group">
                        <label class=" col-sm-2 control-label" >Fecha:</label>
                        <div class="col-sm-4">                            
                            <select class="form-control" id="RepoTipoFecha" name="RepoTipoFecha" required>
                                <option value="-1">---------------Mostrar todos---------------</option>
                                <option value='1'>Entre fechas específicas</option>
                            </select>
                        </div>
                    </div>


            <div id="RepoFechas" hidden > 
                <div class="row form-group">
                    <div class="col-sm-offset-2">(Se mostrarán proyectos en proceso durante estas fechas)</div>
                </div>
                <div class="row">
                    <label class=" col-sm-3 control-label" >Entre: </label>
                    <div class="col-sm-2">     
                        <p> <input type="date" id="fechaInicio" placeholder="aaaa-mm-dd"></p>      
                    </div>
                </div>
                <div class="row">
                    <label class=" col-sm-3 control-label" >Y:</label>
                    <div class="col-sm-2">     
                        <p> <input type="date" id="fechaFin" placeholder="aaaa-mm-dd"></p>     
                    </div>  
                </div>
            </div>

                    <div class="modal-footer">
                        <button  id="guardarCambios" class="btn btn-primary" ><span class="glyphicon glyphicon-share-alt"></span> Generar</button>
                    </div>

                </form>
            </div>
        </div>
    </div>                                    
</div>
<div id="divReporte"></div>
<script type="text/javascript" src="pages/CargaAcademica/Reportes/Proyectos/Script.js"></script>