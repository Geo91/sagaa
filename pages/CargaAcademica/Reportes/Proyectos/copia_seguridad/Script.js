

$(document).ready(function(){   

//para cuando se da submit al formulario formProgramacion
    $("#formRepoCargas").submit(function(e) {
        e.preventDefault();
        if (($('#fechaInicio').val()=="" || $('#fechaFin').val()=="") && $('#RepoTipoFecha').val()==1){
            alert("Complete las fechas.");
        }else{
            data={
                        RepoProy:$('#RepoProy').val(),
                        RepoArea:$('#RepoArea').val(),
                        RepoProf:$('#RepoProf').val(),
                        RepoTipoFecha:$('#RepoTipoFecha').val(),
                        fechaInicio:$('#fechaInicio').val(),
                        fechaFin:$('#fechaFin').val()
                    }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                beforeSend: inicioEnvio,
                success: cargarRepoProy,
                timeout: 4000,
                error: problemas
            }); 
            return false;
        }
    });

    $("#RepoTipoFecha").click(function(event) {
        event.preventDefault();
        var valor = $('#RepoTipoFecha').val();
        if (valor==-1) {
            $('#RepoFechas').hide('fast');
        }else{
            $('#RepoFechas').show('fast');
        }
    });
});


function inicioEnvio(){
    $("#divReporte").text('Cargando...');
}

function problemas(){
    $("#divReporte").text('Problemas en el servidor.');
}

function cargarRepoProy(){
    $("#divReporte").load('pages/CargaAcademica/Reportes/Proyectos/DatosProfProyectos.php',data);
}
