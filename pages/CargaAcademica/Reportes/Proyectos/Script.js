

$(document).ready(function(){   

//para cuando se da submit al formulario formProgramacion
    $("#formRepoCargas").submit(function(e) {
        e.preventDefault();
        if (($('#fechaInicio').val()=="" || $('#fechaFin').val()=="") && $('#RepoTipoFecha').val()==1){
            alert("Complete las fechas.");
        }else{
            data={
                        RepoProy:$('#RepoProy').val(),
                        RepoArea:$('#RepoArea').val(),
                        RepoProf:$('#RepoProf').val(),
                        RepoTipoFecha:$('#RepoTipoFecha').val(),
                        fechaInicio:$('#fechaInicio').val(),
                        fechaFin:$('#fechaFin').val()
                    }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                beforeSend: inicioEnvio,
                success: cargarRepoProy,
                timeout: 4000,
                error: problemas
            }); 
            return false;
        }
    });

    $("#RepoTipoFecha").click(function(event) {
        event.preventDefault();
        var valor = $('#RepoTipoFecha').val();
        if (valor==-1) {
            $('#RepoFechas').hide('fast');
        }else{
            $('#RepoFechas').show('fast');
        }
    });
});

    $(document).on("change","#RepoArea",function () {
        id = $("#RepoArea").val();            
        cargarProyectos(id);
        return false;
    });

    function cargarProyectos(id){
        var datos = {
            idArea:id
        };

        $.ajax({
            async: true,
            type: "POST",
            data:datos,
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            url: "pages/CargaAcademica/Reportes/Proyectos/cargarProyectos.php",
            //beforeSend: inicioVer,
            success: function(response){
                                           
                var arr = JSON.parse(response);
                
                var options = '';
                var val='-1/*';
                var def='------------------Todos------------------';
                options += '<option value="' + val + '">' +
                                def+ '</option>';
                        
                for (var index = 0; index < arr.length; index++) 
                {
                    var Nemple = arr[index].idProyecto;
                    var nombre = arr[index].nombreProyecto;
                    var caracter = "/";
                    var Proyeecto = Nemple.concat(caracter, nombre);
                    
                    options += '<option value="' + Proyeecto + '">' +
                                nombre + '</option>';
                }
                
                $("#RepoProy").html(options);
            },
            timeout: 6000,
        });
    }


function inicioEnvio(){
    $("#divReporte").text('Cargando...');
}

function problemas(){
    $("#divReporte").text('Problemas en el servidor.');
}

function cargarRepoProy(){
    $("#divReporte").load('pages/CargaAcademica/Reportes/Proyectos/DatosProfProyectos.php',data);
}
