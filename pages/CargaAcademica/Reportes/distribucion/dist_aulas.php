<?php

	$maindir = "../../../../";

	require_once($maindir."fpdf/fpdf.php");
	require($maindir."conexion/config.inc.php");

	$varPeriodo = $_GET['vP'];
    $varAnio = $_GET['vA'];

	function hora($hora){
        switch ($hora) {
            case '7':
                $hour = '7am';
                break;
            case '8':
                $hour = '8 am';
                break;
            case '9':
                $hour = '9 am';
                break;
            case '10':
                $hour = '10am';
                break;
            case '11':
                $hour = '11 am';
                break;
            case '12':
                $hour = '12 m';
                break;
            case '13':
                $hour = '1pm';
                break;
            case '14':
                $hour = '2pm';
                break;
            case '15':
                $hour = '3pm';
                break;
            case '16':
                $hour = '4pm';
                break;
            case '17':
                $hour = '5pm';
                break;
            case '18':
                $hour = '6pm';
                break;
            case '19':
                $hour = '7pm';
                break;
            case '20':
                $hour = '8pm';
                break;
            case '21':
                $hour = '9pm';
                break;
        }
        return $hour;
    }

    function array_ref(&$arreglo,$cadena,$limite){
	    $contador = 0;
		$arreglo[$contador] = $cadena;
	}

	function Mayus($texto) {
		$resp = strtr(strtoupper($texto),"àèìòùáéíóúçñäëïöü","ÀÈÌÒÙÁÉÍÓÚÇÑÄËÏÖÜ");
		return $resp;
	}

    class PDF extends FPDF{
	     // Cabecera de página
	         
	    function Header(){
    		$varPeriodo = $_GET['vP'];
    		$varAnio = $_GET['vA'];

	        $this->SetY(20);
	        //$this->SetFont('Arial','B',12);
	        $this->SetFont('Arial', 'B', 18);
		$this->Cell(0, 10, utf8_decode('DISTRIBUCIÓN DE AULAS CARRERA DE DERECHO'), 0,0,"C");
		$this->Ln(6);
		$this->Cell(0, 10, utf8_decode('CARGA ACADÉMICA '.$varPeriodo. ' - '.$varAnio), 0,0,"C");
	    }

	    function Footer(){
    		$varPeriodo = $_GET['vP'];
    		$varAnio = $_GET['vA'];

	        $this->SetY(-20);
	        $this->SetFont('Arial','I',8);
	        $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().' de {nb}   - - - -   Impreso desde SAGAA el ' . date('d-m-y') . ' fecha del sistema   - - - -',0,0,'C');
	    }
	}

	$pdf = new PDF();
	$pdf->AliasNbPages();
	//$pdf->AddPage('L','Folio',0);
	/*$pdf->SetFont('Arial', 'B', 18);
	$pdf->Cell(0, 10, utf8_decode('DISTRIBUCIÓN DE AULAS DEL EDIFICIO - A2'), 0,0,"C");
	
	$pdf->Cell(0, 10, utf8_decode('CARGA ACADÉMICA '.$varPeriodo. ' - '.$varAnio), 0,0,"C");*/
	$pdf->Ln(15);
	

	$consulta = "SELECT DISTINCT ca_edificios.nombre AS 'nEdificio' FROM ca_secciones,ca_asignaturas,ca_aulas,ca_edificios,ca_cargas,ca_periodos WHERE ca_secciones.id_asignatura=ca_asignaturas.id_asignatura AND ca_secciones.id_aula=ca_aulas.id_aula AND ca_aulas.id_edificio=ca_edificios.id_edificio AND ca_secciones.id_carga=ca_cargas.id_carga AND ca_cargas.id_periodo=ca_periodos.id_periodo AND ca_periodos.id_periodo AND ca_periodos.periodo = :cP AND ca_periodos.anio = :cA";

	$query = $db->prepare($consulta);
    $query->bindParam(":cA",$varAnio);
    $query->bindParam(":cP",$varPeriodo);
    $query->execute();

    while ($fila = $query->fetch()){
    	$nEdif = $fila["nEdificio"];
    	$consulta2 = "SELECT DISTINCT ca_aulas.nombre AS 'nAula' FROM ca_secciones,ca_asignaturas,ca_aulas,ca_edificios,ca_cargas,ca_periodos WHERE ca_secciones.id_asignatura=ca_asignaturas.id_asignatura AND ca_secciones.id_aula=ca_aulas.id_aula AND ca_aulas.id_edificio=ca_edificios.id_edificio AND ca_secciones.id_carga=ca_cargas.id_carga AND ca_cargas.id_periodo=ca_periodos.id_periodo AND ca_periodos.periodo = :cP AND ca_periodos.anio = :cA AND ca_edificios.nombre = :cE ORDER BY ca_aulas.nombre";

		$query2 = $db->prepare($consulta2);
	    $query2->bindParam(":cA",$varAnio);
	    $query2->bindParam(":cP",$varPeriodo);
	    $query2->bindParam(":cE",$nEdif);
	    $query2->execute();

	    while ($fila2 = $query2->fetch()){
		$pdf->AddPage('L','Folio',0);
		$pdf->SetAutoPageBreak(1,'5');
		$pdf->Ln(10);
	    	$nAula = $fila2['nAula'];

			$pdf->SetFont('Arial', 'B', 18);
			$pdf->Cell(0, 10, utf8_decode('AULA -  '.$nAula.'  EDIFICIO - '.$nEdif), 0,0,"C");
	    	$pdf->SetTextColor(255,255,255);  // Establece el color del texto (en este caso es blanco)
			$pdf->SetFillColor(0, 134, 179); // establece el color del fondo de la celda (en este caso es AZUL
			$pdf->SetFont('Arial', 'B', 16);
			//Encabezados de la tabla 
			$pdf->Ln(10);
	    	$pdf->Cell(2);
			$pdf->Cell(50, 10, utf8_decode("Horario"), "T,B",0,"C", True);
			$pdf->Cell(110, 10, strtoupper(utf8_decode(Mayus("Profesor"))), "T,B",0,"C", True);
			$pdf->Cell(25, 10, utf8_decode("Código"), "T,B",0,"C", True);
			$pdf->Cell(125, 10, utf8_decode("Asignatura"), "T,B",0,"C", True);
			$pdf->Ln(8);
			$pdf->SetFont('Arial', 'B', 14);
			$pdf->SetTextColor(0,0,0);
			$pdf->Ln(1);

			$sql = "CALL SP_CA_DISTRIBUCION_AULAS_EDIFICIO(?,?,?,?)";
		    $query3 = $db->prepare($sql);
		    $query3->bindParam(1,$varPeriodo,PDO::PARAM_INT);
		    $query3->bindParam(2,$varAnio,PDO::PARAM_INT);
		    $query3->bindParam(3,$nEdif,PDO::PARAM_STR);
		    $query3->bindParam(4,$nAula,PDO::PARAM_STR);
		    $query3->execute();

			while ($ordenados = $query3->fetch()){
				$altura = 10;
				$pdf->Cell(2);
				$pdf->CellFitSpace(50, $altura, "De ".hora(date("H",strtotime($ordenados["Hora_inicio"])))." a ".hora(date("H",strtotime($ordenados["Hora_fin"]))), "B",0,"C");
				$pdf->CellFitScale(110, $altura, utf8_decode($ordenados["nombreE"]), "B",0,"L");
				$pdf->CellFitSpace(25, $altura, utf8_decode($ordenados["id_asignatura"]), "B",0,"C");
				$pdf->CellFitScale(125, $altura, utf8_decode($ordenados["nAsignatura"]), "B",0,"C");
				$pdf->Ln();
				
			}

			$pdf->Ln(15);

	    }
    }

	$pdf->SetFont('Arial', '', 10);
	$pdf->Output('Distribucion de Aulas - '.$varPeriodo.' PAC - '.$varAnio.'.pdf','I');

?>