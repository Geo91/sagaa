<?php
    $maindir = "../../../../";

    //require_once ("../../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

?>

<div id="divRespuesta"></div>

<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span><strong> Reporte de Distribución de Aulas</strong></div>
    <div class="panel-body" >
        <div class="row">
            <form class="form-horizontal col-sm-12" role="form" id="formDistribucionAulas" name="formDistribucionAulas">

                <div class="row form-group" >                    
                    <label class=" col-sm-2 control-label" >Período</label>
                    <div class="col-sm-2">     
                        <input type="number" class="form-control" id="vPeriodo" name="Periodo" min="1" max="3" required
                        value="1">      
                    </div>                                 
                    <label class=" col-sm-2 control-label" >Año</label>
                    <div class="col-sm-2">     
                        <input type="number" class="form-control" id="vAnio" name="Año" min="2016" max="2100" required
                        value="2016">      
                    </div>                  
                </div>
                <br>

                <div class="modal-footer">
                    <button  id="generarTabla" class="btn btn-primary" ><span class="glyphicon glyphicon-log-out"></span> Generar </button>
                    <button  type = "button" id="epdf" class="btn btn-danger" ><i class="fa fa-file-pdf-o"></i> Exportar PDF</button>
                </div>

            </form>
        </div>
    </div>
</div>                              

<div id="graficoDistribucion" hidden></div>

<div id="tablaDatos"></div>

<script type="text/javascript">    
    $(document).ready(function(){

        $("#epdf").click(function(e){
            e.preventDefault();
            data={
                vPeriodo:$('#vPeriodo').val(),
                vAnio:$('#vAnio').val()
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                beforeSend: inicioEnvio,
                success: exportarPDF,
                timeout: 10000,
                error: problemas
            }); 
            return false;
        })
    });
</script>

<script type="text/javascript" src="pages/CargaAcademica/Reportes/distribucion/ScriptDistribucion.js"></script>