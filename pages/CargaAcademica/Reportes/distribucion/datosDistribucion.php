<?php
    $maindir = "../../../../";
    //require_once ("clases.php");
    require_once ("../../clases.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO

    if (isset($_POST['vPeriodo']) && isset($_POST['vAnio']) ) {

        $varPeriodo = $_POST['vPeriodo'];
        $varAnio = $_POST['vAnio'];

        $sql = "CALL SP_CA_DISTRIBUCION_T(?,?)";
        $query = $db->prepare($sql);
        $query->bindParam(1,$varPeriodo,PDO::PARAM_INT);
        $query->bindParam(2,$varAnio,PDO::PARAM_INT);
    echo $varPeriodo.$varAnio;
?>

<div class="row">
    <div class="box">
        <div class="box-body table-responsive">
            <table id="tablaDistribucion" class="table table-bordered table-striped">
                <thead>
                    <tr>                                            
                        <th style="text-align:center;background-color:#386D95;color:white;">Edificio</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Aula</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Horario</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Profesor</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Código</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Asignatura</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    try{
                        $query->execute();
                        while ($fila = $query->fetch()) {
                ?>
                            <tr>
                                <td><?php echo $fila["nEdificio"]; ?></td>
                                <td><?php echo $fila["nAula"]; ?></td>
                                <td><?php echo $fila["Hora_inicio"]." - ".$fila["Hora_fin"]; ?></td>
                                <td><?php echo $fila["nombreE"]; ?></td>
                                <td><?php echo $fila["id_asignatura"] ?></td>
                                <td><?php echo $fila["nAsignatura"]; ?></td>
                            </tr>
                <?php 
                        } //cierre del ciclo while para llenar la tabla de datos
                    }catch(PDOException $e){
                        echo "Error: ".$e;
                    }
                ?>
 
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div>
</div>

<script type="text/javascript">
//opciones para buscador en la tabla 
    $('#tablaDistribucion')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaDistribucion').dataTable({
        "order": [],
        "fnDrawCallback": function (oSettings) {

        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
</script>

<?php }else{
    echo mensajes("Problemas en la página, intentelo de nuevo","azul");
} ?>