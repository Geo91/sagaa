

$(document).ready(function(){   

//para cuando se da submit al formulario formProgramacion
    $("#formRepoContrato").submit(function(e) {
        e.preventDefault();
        if (($('#fechaInicio').val()=="" || $('#fechaFin').val()=="") && $('#RepoTipoFecha').val()==1){
            alert("Complete las fechas.");
        }else{
            data={
                RepoPeriodo:$('#RepoPeriodo').val(),
                RepoAnio:$('#RepoAnio').val(),
                RepoCargaT:$('#RepoCargaT').val(),
                RepoContratoT:$('#RepoContratoT').val(),
                RepoDeptoC:$("#RepoDeptoC").val()
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                beforeSend: inicioEnvio,
                success: cargarRepoContrato,
                timeout: 4000,
                error: problemas
            }); 
            return false;
        }
    });

    $("#RepoTipoFecha").click(function(event) {
        event.preventDefault();
        var valor = $('#RepoTipoFecha').val();
        if (valor==-1) {
            $('#RepoFechas').hide('fast');
        }else{
            $('#RepoFechas').show('fast');
        }
    });
});


function inicioEnvio(){
    $("#divReporte").text('Cargando...');
}

function problemas(){
    $("#divReporte").text('Problemas en el servidor.');
}

function cargarRepoContrato(){
    $("#divReporte").load('pages/CargaAcademica/Reportes/Contratos/DatosContratos.php',data);
}
