<?php
    $maindir = "../../../../";

    require_once ("../../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    $query = "SELECT DISTINCT(SubTabla.nombre) as nombre FROM
            ((SELECT DISTINCT (ca_profesores_contratos_historico.nombre)
            from ca_profesores_contratos_historico)
            UNION
            (SELECT DISTINCT (ca_tipos_contratos.nombre)
            FROM ca_tipos_contratos)) as SubTabla";

?>

<div id="divRespuesta"></div>
<div class="panel panel-primary">
    <div class="panel-heading"><h4><span class="glyphicon glyphicon-stats" aria-hidden="true"></span> Reporte profesores por contrato</h4></div>
    <div class="panel-body" >
        <div class="row">
                <form class="form-horizontal col-sm-offset-2" role="form" id="formRepoContrato" name="formRepoContrato">


            <div class="row form-group" >                    
                <label class=" col-sm-2 control-label" >Periodo</label>
                <div class="col-sm-2">     
                    <input type="number" class="form-control" id="RepoPeriodo" name="RepoPeriodo" min="1" max="3" required
                    value="1">      
                </div>                                 
                <label class=" col-sm-2 control-label" >Año</label>
                <div class="col-sm-2">     
                    <input type="number" class="form-control" id="RepoAnio" name="RepoAnio" min="2016" max="2100" required
                    value="2016">      
                </div>                  
            </div>
            <hr>

                    <div class="row form-group">
                        <label class=" col-sm-2 control-label" >Carga: </label>
                        <div class="col-sm-6">                            
                            <select class="form-control" id="RepoCargaT" name="RepoCargaT" required>
                                <option value="-1">---------------Todas---------------</option>
                                <option value="0">Incompleta</option>
                                <option value="1">Completa</option>
                            </select>
                        </div>
                    </div>


                    <div class="row form-group">
                        <label class=" col-sm-2 control-label" >Contrato: </label>
                        <div class="col-sm-6">                            
                            <select class="form-control" id="RepoContratoT" name="RepoContratoT" required>
                                <option value="-1">---------------Todos---------------</option>
                                <?php
                                    $result =$db->prepare($query);
                                    $result->execute();
                                    while ($fila = $result->fetch()) {
                                ?> 
                                        <option value="<?php echo $fila['nombre']; ?>">
                                        <?php echo $fila["nombre"]; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="modal-footer">
                        <button  id="guardarCambios" class="btn btn-primary" ><span class="glyphicon glyphicon-share-alt"></span> Generar</button>
                    </div>

                </form>
            </div>
        </div>
    </div>                                    
</div>
<div id="divReporte"></div>
<script src="js/plugins/Highcharts-4/js/highcharts.js" type="text/javascript"></script>
<script type="text/javascript" src="pages/CargaAcademica/Reportes/Contratos/Script.js"></script>