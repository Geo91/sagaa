<?php
    $maindir = "../../../../";
    require_once ("../../clases.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO

if (isset($_POST['RepoPeriodo']) &&
    isset($_POST['RepoAnio']) &&
    isset($_POST['RepoCargaT']) &&
    isset($_POST['RepoContratoT']) ) {
    
    $PeriodoEnProceso = -1;
    if (isset($_SESSION['infoPeriodo'])) {
        $PeriodoEnProceso = $_SESSION['infoPeriodo']->getId();
    }

    $subTotal = 0;
    $RepoPeriodo = $_POST['RepoPeriodo'];
    $RepoAnio = $_POST['RepoAnio'];
    $RepoCargaT = $_POST['RepoCargaT'];
    $RepoContratoT = $_POST['RepoContratoT'];

    $queryTotal = "SELECT count(*) as Total
from ca_profesores LEFT JOIN ca_tipos_contratos on ca_profesores.id_contrato=ca_tipos_contratos.id_contrato
  left join ca_carga_profesores on ca_profesores.No_Empleado=ca_carga_profesores.No_Empleado
  LEFT JOIN ca_profesores_uv_variable on ca_profesores.No_Empleado=ca_profesores_uv_variable.No_Empleado
  LEFT JOIN (SELECT ca_profesores_contratos_historico.No_Empleado,
                  ca_profesores_contratos_historico.uv,
                  ca_profesores_contratos_historico.nombre
               FROM ca_profesores_contratos_historico
               WHERE (SELECT ca_periodos.fecha_ingreso_f
                      from ca_periodos 
                      where ca_periodos.periodo=$RepoPeriodo
                        and ca_periodos.anio=$RepoAnio )
               BETWEEN ca_profesores_contratos_historico.fecha_inicio
               and ca_profesores_contratos_historico.fecha_fin
               ORDER by ca_profesores_contratos_historico.fecha_inicio DESC) as historia_uv
    on ca_profesores.No_Empleado = historia_uv.No_Empleado
  LEFT JOIN ca_periodos on ca_carga_profesores.id_periodo=ca_periodos.id_periodo
    LEFT JOIN empleado on ca_profesores.No_Empleado = empleado.No_Empleado
    LEFT JOIN persona on empleado.N_identidad = persona.N_identidad
WHERE ca_periodos.periodo=$RepoPeriodo
    and ca_periodos.anio=$RepoAnio 
    and ca_periodos.id_periodo<>$PeriodoEnProceso ";

    $Total=1;
    $result =$db->prepare($queryTotal);
    $result->execute();
    while ($fila = $result->fetch()) {
        if($fila['Total']==0){
            $Total = 1;   
        }else{
            $Total = $fila['Total'];
        }
        break;
    }
    

    $query1 = "SELECT ca_profesores.No_Empleado,
  persona.N_identidad,
    CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'nombre',
  (CASE 
     WHEN historia_uv.uv >0 THEN
        CONCAT(historia_uv.nombre,'/',historia_uv.uv,'uv')
     WHEN historia_uv.uv = 0 THEN
        CONCAT(historia_uv.nombre)
     WHEN ca_tipos_contratos.uv > 0 THEN
        CONCAT(ca_tipos_contratos.nombre,'/',ca_tipos_contratos.uv,'uv')
     WHEN ca_tipos_contratos.uv = 0 THEN
        CONCAT(ca_tipos_contratos.nombre)
      ELSE 
        CONCAT(ca_tipos_contratos.nombre,'/',ca_profesores_uv_variable.uv,'uv')
    END) as 'contrato',
    ca_carga_profesores.cantidad_uv,
    (CASE ca_carga_profesores.completa WHEN 0 THEN
      'INCOMPLETA'
     ELSE 'COMPLETA' END) as 'Carga'
from ca_profesores LEFT JOIN ca_tipos_contratos on ca_profesores.id_contrato=ca_tipos_contratos.id_contrato
  left join ca_carga_profesores on ca_profesores.No_Empleado=ca_carga_profesores.No_Empleado
  LEFT JOIN ca_profesores_uv_variable on ca_profesores.No_Empleado=ca_profesores_uv_variable.No_Empleado
  LEFT JOIN (SELECT ca_profesores_contratos_historico.No_Empleado,
                  ca_profesores_contratos_historico.uv,
                  ca_profesores_contratos_historico.nombre
               FROM ca_profesores_contratos_historico
               WHERE (SELECT ca_periodos.fecha_ingreso_f
                      from ca_periodos 
                      where ca_periodos.periodo=$RepoPeriodo
                        and ca_periodos.anio=$RepoAnio )
               BETWEEN ca_profesores_contratos_historico.fecha_inicio
               and ca_profesores_contratos_historico.fecha_fin
               ORDER by ca_profesores_contratos_historico.fecha_inicio DESC) as historia_uv
    on ca_profesores.No_Empleado = historia_uv.No_Empleado
  LEFT JOIN ca_periodos on ca_carga_profesores.id_periodo=ca_periodos.id_periodo
    LEFT JOIN empleado on ca_profesores.No_Empleado = empleado.No_Empleado
    LEFT JOIN persona on empleado.N_identidad = persona.N_identidad
WHERE ca_periodos.periodo=$RepoPeriodo
    and ca_periodos.anio=$RepoAnio 
    and ca_periodos.id_periodo<>$PeriodoEnProceso ";

    if ($RepoCargaT!=-1) {
        $query1 = $query1." and ca_carga_profesores.completa=$RepoCargaT ";
    }
     
    if ($RepoContratoT!=-1) {
        $query1 = $query1." and (historia_uv.nombre = '$RepoContratoT'
       or ca_tipos_contratos.nombre = '$RepoContratoT') ";
    }  
    if ($RepoCargaT!=-1 || $RepoContratoT!=-1) {
?>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading"><span class="glyphicon glyphicon-indent-right" aria-hidden="true"></span> Gráfico</div>
        <div class="panel-body" >
            <div class="row">
                <div class="col-lg-12">
      <div id="divRepoGraph">
      </div>
                </div>
            </div>
        </div>                                    
    </div> 
</div>
<?php } ?>
<div class="row">
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaRepoContrato" class="table table-bordered table-striped">
            <thead>
                <tr>                                            
                    <th>No Empleado</th>
                    <th>Identidad</th>
                    <th>Nombre</th>
                    <th>Contrato</th>
                    <th>Total de UV's</th>
                    <th>Carga</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $result =$db->prepare($query1);
                $result->execute();
                while ($fila = $result->fetch()) {
                    ?>
                    <tr>
                        <td><?php echo $fila['No_Empleado']; ?></td>
                        <td><?php echo $fila['N_identidad']; ?></td>
                        <td><?php echo $fila['nombre']; ?></td>
                        <td><?php echo $fila['contrato']; ?></td>
                        <td><?php echo $fila['cantidad_uv']; ?></td>
                        <td><?php echo $fila['Carga']; ?></td>
                    </tr>
            <?php $subTotal = $subTotal+1;
            } ?>
            </tbody>
            <tfoot>
                <tr>                                                    
                    <th>No Empleado</th>
                    <th>Identidad</th>
                    <th>Nombre</th>
                    <th>Contrato</th>
                    <th>Total de UV's</th>
                    <th>Carga</th>
                </tr>
            </tfoot>
        </table>
    </div><!-- /.box-body -->
</div>
</div>
<script type="text/javascript">
    $(function () {
        $(document).ready(function () {

            // Build the chart
            $('#divRepoGraph').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Profesores: <?php 
                    if ($RepoCargaT!=-1) {
                        echo " con carga ";
                        if ($RepoCargaT==0) {
                            echo "incompleta ";
                        }else{
                            echo "completa ";
                        }
                    }
                    if ($RepoContratoT!=-1) {
                        echo " de contrato `$RepoContratoT`";
                    }  ?>'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: "Secciones",
                    colorByPoint: true,
                    data: [{name: 'Mostrando el <?php echo round(($subTotal/$Total)*100,2); ?>%', y: <?php echo $subTotal; ?>,
                                   sliced: true,
                                   selected: true },
                           {name: 'Resto <?php echo 100-round(($subTotal/$Total)*100,2); ?>%', y: <?php echo $Total-$subTotal; ?>
                            }]
                }]
            });
        });
    });
     $('#tablaRepoContrato')
        .removeClass('display')
        .addClass('table table-striped table-bordered');
    $('#tablaRepoContrato').DataTable({
                dom: 'Blfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                download: 'open',
                title:'Distribución de profesores: <?php 
                    if ($RepoCargaT!=-1) {
                        echo " con carga ";
                        if ($RepoCargaT==0) {
                            echo "incompleta ";
                        }else{
                            echo "completa ";
                        }
                    }
                    if ($RepoContratoT!=-1) {
                        echo " de contrato `$RepoContratoT`";
                    }  ?>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                download: 'open',
                orientation: 'landscape',
                pageSize: 'LETTER',
                title:'Distribución de profesores: <?php 
                    if ($RepoCargaT!=-1) {
                        echo " con carga ";
                        if ($RepoCargaT==0) {
                            echo "incompleta ";
                        }else{
                            echo "completa ";
                        }
                    }
                    if ($RepoContratoT!=-1) {
                        echo " de contrato `$RepoContratoT`";
                    }  ?>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'print',
                title:'Distribución de profesores: <?php 
                    if ($RepoCargaT!=-1) {
                        echo " con carga ";
                        if ($RepoCargaT==0) {
                            echo "incompleta ";
                        }else{
                            echo "completa ";
                        }
                    }
                    if ($RepoContratoT!=-1) {
                        echo " de contrato `$RepoContratoT`";
                    }  ?>',
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'
        ]
    });

    $('.toggle-vis1').on( 'click', function (e) {
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
 
        // Toggle the visibility
        column.visible( ! column.visible() );
        $(this).attr('color', ' blue');
    });
    $(".toggle-vis1").prop("disabled", "");
</script>
<?php }else{
    echo mensajes("Problemas en la página, intentelo de nuevo","azul");
} ?>