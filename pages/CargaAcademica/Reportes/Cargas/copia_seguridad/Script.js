$(document).ready(function(){   

//para cuando se da submit al formulario formProgramacion
    $("#formRepoCargas").submit(function(e) {
        e.preventDefault();
        data={
                    RepoPeriodo:$('#RepoPeriodo').val(),
                    RepoAnio:$('#RepoAnio').val(),
                    RepoProf:$('#RepoProf').val(),
                    RepoDepto:$('#RepoDepto').val(),
                    RepoMatricula:$('#RepoMatricula').val()
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: cargarRepoCarga,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });
});


function inicioEnvio(){
    $("#divReporte").text('Cargando...');
}

function problemas(){
    $("#divReporte").text('Problemas en el servidor.');
}

function cargarRepoCarga(){
    $("#divReporte").load('pages/CargaAcademica/Reportes/Cargas/DatosCargas.php',data);
}
