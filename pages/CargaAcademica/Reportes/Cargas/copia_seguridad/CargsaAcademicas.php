<?php
    $maindir = "../../../../";

    require_once ("../../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    $query = "SELECT empleado.No_Empleado, 
                    CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombre
                FROM empleado,persona,cargo,empleado_has_cargo
                WHERE empleado.N_identidad=persona.N_identidad
                    and empleado_has_cargo.No_Empleado=empleado.No_Empleado
                    and empleado_has_cargo.ID_cargo=cargo.ID_cargo
                    and (cargo.Cargo='Docente' 
                         or cargo.Cargo='Decana'
                         or cargo.Cargo='Jefe de Departamento'
                         or cargo.Cargo='Coordinador de Maestría')
                    and empleado_has_cargo.Fecha_salida_cargo is null
                    and empleado.No_Empleado in (SELECT ca_profesores.No_Empleado FROM ca_profesores) 
                ORDER BY persona.Primer_nombre";

        $query2="SELECT * FROM `ca_departamentos_carreras`";

?>

<div id="divRespuesta"></div>
<div class="panel panel-primary">
    <div class="panel-heading"><h4><span class="glyphicon glyphicon-stats" aria-hidden="true"></span> Reporte de Cargas académicas</h4></div>
    <div class="panel-body" >
        <div class="row">
                <form class="form-horizontal col-sm-12" role="form" id="formRepoCargas" name="formRepoCargas">



            <div class="row form-group" >                    
                <label class=" col-sm-2 control-label" >Periodo</label>
                <div class="col-sm-2">     
                    <input type="number" class="form-control" id="RepoPeriodo" name="RepoPeriodo" min="1" max="3" required
                    value="1">      
                </div>                                 
                <label class=" col-sm-2 control-label" >Año</label>
                <div class="col-sm-2">     
                    <input type="number" class="form-control" id="RepoAnio" name="RepoAnio" min="2016" max="2100" required
                    value="2016">      
                </div>                  
            </div>

            <hr>

                    <div class="row form-group">
                        <label class=" col-sm-2 control-label" >Departamento: </label>
                        <div class="col-sm-8">                            
                            <select class="form-control" id="RepoDepto" name="RepoDepto" required>
                                <option value="-1">---------------Todos---------------</option>
                                <?php
                                    $result =$db->prepare($query2);
                                    $result->execute();
                                    while ($fila = $result->fetch()) {
                                ?> 
                                        <option value="<?php echo $fila['id_departamento']; ?>">
                                        <?php echo $fila["nombre"]; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class=" col-sm-2 control-label" >Profesor</label>
                        <div class="col-sm-8">                            
                            <select class="form-control" id="RepoProf" name="RepoProf" required>
                                <option value="-1/*">---------------Todos---------------</option>
                                <?php
                                     $result =$db->prepare($query);
                                     $result->execute();
                                    while ($fila = $result->fetch()) {
                                        ?>
                                        <option value='<?php echo $fila["No_Empleado"]."/".$fila["nombre"]; ?>'>
                        <?php 
                            $largo = strlen($fila["No_Empleado"]);
                            if ($largo<=7) {
                                $espacio = str_repeat ( "&nbsp", 7-$largo);
                            }else{
                                $espacio=" ";
                            }
                            echo "█ No.".$fila["No_Empleado"].$espacio."█ &nbsp&nbsp".$fila["nombre"]; ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="row form-group">
                        <label class=" col-sm-2 control-label" >Matriculados: </label>
                        <div class="col-sm-8">                            
                            <select class="form-control" id="RepoMatricula" name="RepoMatricula" required>
                                <option value="-1">---------------Todos---------------</option>
                                <option value="1">Que sobrepasan los cupos</option>
                                <option value="2">Menores o iguales a los cupos</option>
                            </select>
                        </div>
                    </div>


                    <div class="modal-footer">
                        <button  id="guardarCambios" class="btn btn-primary" ><span class="glyphicon glyphicon-share-alt"></span> Generar</button>
                    </div>

                </form>
            </div>
        </div>
    </div>                                    
</div>
<div id="divReporte"></div>
<script src="js/plugins/Highcharts-4/js/highcharts.js" type="text/javascript"></script>
<script type="text/javascript" src="pages/CargaAcademica/Reportes/Cargas/Script.js"></script>