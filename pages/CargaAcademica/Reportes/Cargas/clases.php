<?php

class SeccionCarga{
	private $id_seccion;
	private $codigo_seccion;
	private $id_asignatura;
	private $nAsignatura;
	private $cupos_max;
	private $matriculados;
	private $Hora_inicio;
	private $Hora_fin;
	public $Dias;
	private $id_aula;
	private $nAula;
	private $nEdificio;
	private $No_Empleado;
	private $nombreE;
	private $Depto;

	public function __construct ($id_seccion,
								 $codigo_seccion,
								 $id_asignatura,
								 $nAsignatura,
								 $cupos_max,
								 $matriculados,
								 $Hora_inicio,
								 $Hora_fin,
								 $id_aula,
								 $nAula,
								 $nEdificio,
								 $No_Empleado,
								 $nombreE,
								 $Depto){
		$this->id_seccion=$id_seccion;
		$this->codigo_seccion=$codigo_seccion;
		$this->id_asignatura=$id_asignatura;
		$this->nAsignatura=$nAsignatura;
		$this->cupos_max=$cupos_max;
		$this->matriculados=$matriculados;
		$this->Hora_inicio=$Hora_inicio;
		$this->Hora_fin=$Hora_fin;
		$this->id_aula=$id_aula;
		$this->nAula=$nAula;
		$this->nEdificio=$nEdificio;
		$this->No_Empleado=$No_Empleado;
		$this->nombreE=$nombreE;
		$this->Depto=$Depto;
		$this->Dias=array();
	}


	public function getid_seccion(){
		return $this->id_seccion;
	}
	public function getcodigo_seccion(){
		return $this->codigo_seccion;
	}
	public function getid_asignatura(){
		return $this->id_asignatura;
	}
	public function getnAsignatura(){
		return $this->nAsignatura;
	}
	public function getcupos_max(){
		return $this->cupos_max;
	}
	public function getmatriculados(){
		return $this->matriculados;
	}
	public function getHora_inicio(){
		return $this->Hora_inicio;
	}
	public function getHora_fin(){
		return $this->Hora_fin;
	}
	public function getid_aula(){
		return $this->id_aula;
	}
	public function getnAula(){
		return $this->nAula;
	}
	public function getnEdificio(){
		return $this->nEdificio;
	}
	public function getNo_Empleado(){
		return $this->No_Empleado;
	}
	public function getnombreE(){
		return $this->nombreE;
	}
	public function getDepto(){
		return $this->Depto;
	}

}