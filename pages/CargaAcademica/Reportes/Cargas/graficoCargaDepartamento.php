<?php
    $maindir = "../../../../";
    require_once ("../../clases.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO

if (isset($_POST['RepoPeriodo']) && isset($_POST['RepoAnio']) && isset($_POST['RepoDepto']) && isset($_POST['RepoProf']) && isset($_POST['RepoMatricula'])) {
    
    $PeriodoEnProceso = -1;
    if (isset($_SESSION['infoPeriodo'])) {
        $PeriodoEnProceso = $_SESSION['infoPeriodo']->getId();
    }

    $subTotal = 0;
    $RepoPeriodo = $_POST['RepoPeriodo'];
    $RepoAnio = $_POST['RepoAnio'];
    $RepoDeptoC = $_POST['RepoDepto'];
    $DatosProf= split("/", $_POST['RepoProf']);
    $RepoProf = $DatosProf[0];
    $RepoProfN = $DatosProf[1];
    $RepoM = $_POST['RepoMatricula'];

    $queryGraph = "SELECT COUNT(*) as Total, ca_asignaturas.nombre FROM ca_secciones,ca_cargas,ca_periodos, ca_asignaturas, empleado WHERE ca_secciones.id_carga=ca_cargas.id_carga and ca_cargas.id_periodo=ca_periodos.id_periodo and ca_asignaturas.id_asignatura=ca_secciones.id_asignatura and ca_secciones.No_Empleado=empleado.No_Empleado and empleado.Id_departamento in (SELECT DISTINCT empleado.Id_departamento from empleado where empleado.Id_departamento in (SELECT departamento_laboral.Id_departamento_laboral from departamento_laboral, ca_departamentos_carreras where departamento_laboral.nombre_departamento=ca_departamentos_carreras.nombre and ca_departamentos_carreras.id_departamento = '$RepoDeptoC')) and ca_periodos.periodo='$RepoPeriodo' and ca_periodos.anio='$RepoAnio' group by ca_asignaturas.nombre";

//and ca_periodos.id_periodo<>'$PeriodoEnProceso'

    $queryDepto = "SELECT ca_departamentos_carreras.nombre from ca_departamentos_carreras where ca_departamentos_carreras.id_departamento = '$RepoDeptoC'";
    $resultD =$db->prepare($queryDepto);
    $resultD->execute();
    while($fila = $resultD->fetch()){
        $departamento = $fila['nombre'];
    };
    
    if($RepoDeptoC != -1 && $RepoProf == -1 && $RepoM == -1){
                                
?>
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span><strong> Análisis de Carga Académica</strong></div>
            <div class="panel-body" >
                <div class="row">
                    <div class="col-lg-12">
                        <div id="divRepoGraph"> 
                        </div>
                    </div>
                </div>
            </div>                                    
        </div> 
    </div>
<?php
    }
?>

<script type="text/javascript">
    $(function () {
        $(document).ready(function () {

            // Build the chart
            $('#divRepoGraph').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'column'
                },

                title: {
                    text: 'Secciones Departamento: <?php 
                        if ($RepoDeptoC==-1) {
                            echo "Todos los departamentos";
                        }else{
                            echo "$departamento";
                        }?>'
                },

                tooltip: {
                    pointFormat: 'No. de Secciones: <b>{point.y}</b>'
                },

                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },

                yAxis:{
                    allowDecimals: false
                },

                xAxis: {
                    categories: ['Secciones por Asignatura']
                },

                series: [
                <?php 
                    $result =$db->prepare($queryGraph);
                    $result->execute();
                    while ($fila = $result->fetch() ){ ?>
                        { //indicar la información que conformará las series

                        name: "<?php echo $fila['nombre'] ?>",
                        data: [{name: "<?php echo $fila['nombre'] ?>", y: <?php echo $fila['Total']; ?>}]
                        }, //cierre de llave que almacena los atributos de las series 
                    <?php }; ?> //cierre del ciclo while que llena las series del gráfico
                ] //cierre de las series 
            });
        });
    });

</script>
<?php
    }else{
        echo "Problemas en el servidor";
    }
?>