<?php
    $maindir = "../../../../";
include($maindir."conexion/config.inc.php");

/* Hacemos una consulta a la base de datos para obtener los datos de la tabla */
$idDepto = $_POST['idDepto'];
$query = "SELECT empleado.No_Empleado, 
                    CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombre
                FROM empleado,persona,cargo,empleado_has_cargo
                WHERE empleado.N_identidad=persona.N_identidad
                    and empleado_has_cargo.No_Empleado=empleado.No_Empleado
                    and empleado_has_cargo.ID_cargo=cargo.ID_cargo
                    and (cargo.Cargo='Docente' 
                         or cargo.Cargo='Decana'
                         or cargo.Cargo='Jefe de Departamento'
                         or cargo.Cargo='Coordinador de Maestría')
                    and empleado_has_cargo.Fecha_salida_cargo is null
                    and empleado.No_Empleado in (SELECT ca_profesores.No_Empleado FROM ca_profesores) and empleado.Id_departamento in (SELECT DISTINCT empleado.Id_departamento from empleado where empleado.Id_departamento in (SELECT departamento_laboral.Id_departamento_laboral from departamento_laboral, ca_departamentos_carreras where departamento_laboral.nombre_departamento=ca_departamentos_carreras.nombre and ca_departamentos_carreras.id_departamento = '".$idDepto."')) ORDER BY persona.Primer_nombre";
$result = mysql_query($query);
$json = array();
$contadorIteracion = 0;

    while ($fila = mysql_fetch_array($result)) { 

        $json[$contadorIteracion] = array
            (
                "N_empleado" => utf8_encode($fila["No_Empleado"]),
                "nombreEmpleado" => utf8_encode($fila["nombre"])
            );

        $contadorIteracion++;
    }

    //Retornamos el jason con todos los elmentos tomados de la base de datos.
    echo json_encode($json);
?>