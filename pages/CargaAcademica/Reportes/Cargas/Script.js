$(document).ready(function(){   

//para cuando se da submit al formulario formProgramacion
    $("#formRepoCargas").submit(function(e) {
        e.preventDefault();
        data={
                    RepoPeriodo:$('#RepoPeriodo').val(),
                    RepoAnio:$('#RepoAnio').val(),
                    RepoProf:$('#RepoProf').val(),
                    RepoDepto:$('#RepoDepto').val(),
                    RepoMatricula:$('#RepoMatricula').val()
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: cargarRepoCarga,
            timeout: 6000,
            error: problemas
        }); 
        return false;
    });
});

    $(document).on("change","#RepoDepto",function () {
        id = $("#RepoDepto").val();            
        cargarDocentes(id);
        return false;
    });

    function cargarDocentes(id){
        var datos = {
            idDepto:id
        };

        $.ajax({
            async: true,
            type: "POST",
            data:datos,
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            url: "pages/CargaAcademica/Reportes/Cargas/cargarDocentes.php",
            //beforeSend: inicioVer,
            success: function(response){
                                           
                var arr = JSON.parse(response);
                
                var options = '';
                var val='-1/*';
                var def='------------------Todos------------------';
                options += '<option value="' + val + '">' +
                                def+ '</option>';
                        
                for (var index = 0; index < arr.length; index++) 
                {
                    var Nemple = arr[index].N_empleado;
                    var nombre = arr[index].nombreEmpleado;
                    var caracter = "/";
                    var Profe = Nemple.concat(caracter, nombre);
                    
                    options += '<option value="' + Profe + '">' +
                                nombre + '</option>';
                }
                
                $("#RepoProf").html(options);
            },
            timeout: 6000,
        });
    }


function inicioEnvio(){
    $("#divReporte").text('Cargando...');
}

function problemas(){
    $("#divReporte").text('Problemas en el servidor.');
}

function cargarRepoCarga(){
    $("#divReporte").load('pages/CargaAcademica/Reportes/Cargas/DatosCargas.php',data);
    $("#divGraficoReporte").show();
    $("#divGraficoReporte").load('pages/CargaAcademica/Reportes/Cargas/graficoCargaDepartamento.php', data);

}
