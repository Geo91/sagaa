<?php  
    $maindir = "../../../../";
    require_once ("../../clases.php");
    require_once($maindir."conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    if (isset($_POST['vPeriodo']) && isset($_POST['vAnio']) ) {


        $varPeriodo = $_POST['vPeriodo'];
        $varAnio = $_POST['vAnio'];

        try{
            $sql = "CALL SP_CA_DOCENTES_HORA_T(?,?)";
            $query = $db->prepare($sql);
            $query->bindParam(1,$varPeriodo,PDO::PARAM_INT);
            $query->bindParam(2,$varAnio,PDO::PARAM_INT);

            $query->execute();
            $count = $query->rowCount();

            if ($count > 0){
                echo mensajes('El reporte de docentes por hora ha sido exportado éxitosamente', 'verde');
                echo "<script type='text/javascript'>
                        var tP = '$varPeriodo';
                        var tA = '$varAnio';
                        window.open('pages/CargaAcademica/Reportes/porHora/reporteDocentesHora.php?vP='+tP+'&vA='+tA);
                    </script>";
            }else{
                echo mensajes('Error! No hay registros disponibles', 'rojo');
            }
        }catch(PDOexception $e){
            echo mensajes('Error!'. $e, 'rojo');
        }
    }else{
        echo mensajes("Hubo un error al intentar exportar el documento. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
    }

?>

<script type="text/javascript">
    //$("#tablaDatos").load('pages/CargaAcademica/Reportes/distribucion/datosDistribucion.php');
</script>