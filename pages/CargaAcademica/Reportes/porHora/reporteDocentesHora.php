<?php

	$maindir = "../../../../";

	require_once($maindir."fpdf/fpdf.php");
	require($maindir."conexion/config.inc.php");

	$varPeriodo = $_GET['vP'];
    $varAnio = $_GET['vA'];

    function Mayus($texto) {
		$resp = strtr(strtoupper($texto),"àèìòùáéíóúçñäëïöü","ÀÈÌÒÙÁÉÍÓÚÇÑÄËÏÖÜ");
		return $resp;
	}

	class AlphaPDF extends FPDF {
	var $extgstates = array();

	// alpha: real value from 0 (transparent) to 1 (opaque)
	// bm:    blend mode, one of the following:
	//          Normal, Multiply, Screen, Overlay, Darken, Lighten, ColorDodge, ColorBurn,
	//          HardLight, SoftLight, Difference, Exclusion, Hue, Saturation, Color, Luminosity
	function SetAlpha($alpha, $bm='Normal'){
		// set alpha for stroking (CA) and non-stroking (ca) operations
		$gs = $this->AddExtGState(array('ca'=>$alpha, 'CA'=>$alpha, 'BM'=>'/'.$bm));
		$this->SetExtGState($gs);
	}

	function AddExtGState($parms){
		$n = count($this->extgstates)+1;
		$this->extgstates[$n]['parms'] = $parms;
		return $n;
	}

	function SetExtGState($gs){
		$this->_out(sprintf('/GS%d gs', $gs));
	}

	function _enddoc(){
		if(!empty($this->extgstates) && $this->PDFVersion<'1.4')
			$this->PDFVersion='1.4';
		parent::_enddoc();
	}

	function _putextgstates(){
		for ($i = 1; $i <= count($this->extgstates); $i++)
		{
			$this->_newobj();
			$this->extgstates[$i]['n'] = $this->n;
			$this->_out('<</Type /ExtGState');
			$parms = $this->extgstates[$i]['parms'];
			$this->_out(sprintf('/ca %.3F', $parms['ca']));
			$this->_out(sprintf('/CA %.3F', $parms['CA']));
			$this->_out('/BM '.$parms['BM']);
			$this->_out('>>');
			$this->_out('endobj');
		}
	}

	function _putresourcedict(){
		parent::_putresourcedict();
		$this->_out('/ExtGState <<');
		foreach($this->extgstates as $k=>$extgstate)
			$this->_out('/GS'.$k.' '.$extgstate['n'].' 0 R');
		$this->_out('>>');
	}

	function _putresources(){
		$this->_putextgstates();
		parent::_putresources();
	}
}

   class PDF extends AlphaPDF{
	     // Cabecera de página

   		function header(){
   			$maindir = "../../../../";
	        $this->Image($maindir.'assets/img/encabezado.png', 8,1,207,32, 'PNG'); //Encabezado
	        $this->SetFont('Arial','',9);
	        $this->SetTextColor(0,0,0);
	        $this->SetY(8);
	        $this->Cell(163);
	        $this->Cell(0, 13, 'Tel: 2216-5100', 0,0,"R");
	        $this->Ln(4);
	        $this->Cell(0, 13, 'Edificio A-2', 0,0,"R");
	        $this->SetY(12);
	        $this->Ln(25);
   		}

	   	function Footer(){
	        $maindir = "../../../../";

	        $this->SetY(-20);
	        $this->SetAlpha(0.3);
	        $this->Image($maindir.'assets/img/pieDocT.png', 5,148,208,130, 'PNG'); //Pie de página
	        $this->SetAlpha(1);
	    }
	}

	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage('P','Letter',0);
    $pdf->SetFont('Arial', '', 9);
	$pdf->Cell(0, 8, utf8_decode('PROFESORES POR HORA '.$varPeriodo.' PERIODO ACADÉMICO '.$varAnio), 0,0,"C");

	$pdf->SetFont('Arial', 'B', 10);
	//Encabezados de la tabla 
	$pdf->Ln(10);
	$pdf->Cell(2);
	$pdf->Cell(100, 8, utf8_decode("NOMBRE DEL DOCENTE"), "L,R,T,B",0,"L", False);
	$pdf->Cell(22, 8, utf8_decode("SECCIÓN"), "L,R,T,B",0,"C", False);
	$pdf->Cell(12, 8, utf8_decode("U.V"), "L,R,T,B",0,"C", False);
	$pdf->Cell(60, 8, utf8_decode("U.V EXTRA PROF HORARIO"), "L,R,T,B",0,"C", False);
	$pdf->Ln(8);
	$pdf->SetFont('Arial', '', 10);

	$consulta = "SELECT ca_departamentos_carreras.id_departamento, ca_departamentos_carreras.nombre FROM ca_departamentos_carreras";

	$query = $db->prepare($consulta);
    $query->execute();

    while ($fila = $query->fetch()){
    	$idDepto = $fila["id_departamento"];
    	$nDepto = $fila["nombre"];

    	$pdf->Cell(2);
    	$pdf->Cell(100, 7, "DEPARTAMENTO ".utf8_decode($nDepto), "L,R,T,B",0,"L", False);
		$pdf->Cell(22, 7, "", "L,R,T,B",0,"C", False);
		$pdf->Cell(12, 7, "", "L,R,T,B",0,"C", False);
		$pdf->Cell(60, 7, "", "L,R,T,B",0,"C", False);
		$pdf->Ln();

    	$consulta2 = "SELECT ca_profesores.No_Empleado, CONCAT('No. Emp. ',empleado.No_Empleado,' - ', persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'Empleado' FROM ca_profesores INNER JOIN (empleado INNER JOIN persona ON empleado.N_identidad=persona.N_identidad) ON ca_profesores.No_Empleado=empleado.No_Empleado INNER JOIN ca_tipos_contratos ON ca_profesores.id_contrato=ca_tipos_contratos.id_contrato WHERE ca_tipos_contratos.id_contrato='3' AND ca_profesores.No_Empleado IN (SELECT empleado.No_Empleado FROM empleado INNER JOIN departamento_laboral ON empleado.Id_departamento=departamento_laboral.Id_departamento_laboral WHERE departamento_laboral.nombre_departamento = :cND)";

		$query2 = $db->prepare($consulta2);
	    $query2->bindParam(":cND",$nDepto);
	    $query2->execute();

	    while ($fila2 = $query2->fetch()) {
	    	$nEmp = $fila2['No_Empleado'];

	    	$pdf->SetFont('Arial', 'IB', 10);
	    	$pdf->SetTextColor(0, 162, 255);  // Establece el color del texto (en este caso es blanco)
	    	$pdf->Cell(2);
			$pdf->Cell(100, 7, utf8_decode($fila2['Empleado']), "L,R,T,B",0,"L", False);
			$pdf->Cell(22, 7, "", "L,R,T,B",0,"C", False);
			$pdf->Cell(12, 7, "", "L,R,T,B",0,"C", False);
			$pdf->Cell(60, 7, "", "L,R,T,B",0,"C", False);
			$pdf->Ln();
			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont('Arial', '', 10);

			$sql = "CALL SP_CA_REPORTE_DOCENTES_HORA(?,?,?)";
		    $query3 = $db->prepare($sql);
		    $query3->bindParam(1,$varPeriodo,PDO::PARAM_INT);
		    $query3->bindParam(2,$varAnio,PDO::PARAM_INT);
		    $query3->bindParam(3,$nEmp,PDO::PARAM_INT);
		    $query3->execute();

			while ($row = $query3->fetch()){
				$altura = 7;
				$pdf->Cell(2);
				$pdf->CellFitScale(100, $altura, strtoupper(utf8_decode(Mayus($row["asignatura"]))), "L,R,T,B",0,"L");
				$pdf->CellFitSpace(22, $altura, date("H",strtotime($row["Hora_inicio"]))." - ".date("H",strtotime($row["Hora_fin"])), "L,R,T,B",0,"C");
				$pdf->CellFitSpace(12, $altura, utf8_decode($row["uv"]), "L,R,T,B",0,"C");
				$pdf->CellFitScale(60, $altura, " ", "L,R,T,B",0,"C");
				$pdf->Ln();
				
			}

			$pdf->Ln(5);
			$query3->closeCursor();

	    }
	    $query2->closeCursor();
    }
    $query->closeCursor();

    $pdf->AddPage('P','Letter',0);
    $pdf->Ln(40);
	$pdf->SetFont('Arial', 'I', 12); 

	$con = "SELECT CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'Jefe', ca_departamentos_carreras.nombre as 'Depto' FROM persona INNER JOIN (empleado INNER JOIN (usuario INNER JOIN (ca_jefes_departamentos INNER JOIN ca_departamentos_carreras ON ca_jefes_departamentos.id_departamento=ca_departamentos_carreras.id_departamento) ON usuario.id_Usuario=ca_jefes_departamentos.id_Usuario) ON empleado.No_Empleado=usuario.No_Empleado) ON persona.N_identidad=empleado.N_identidad";

	$mquery = $db->prepare($con);
    $mquery->execute();

    $n = 1;
    while ($row2 = $mquery->fetch()){
    	if($n == 2){
			$pdf->Cell(98);
		}
    	$pdf->Cell(97, 5, utf8_decode($row2['Jefe']), "",0,"C", False);
    	$pdf->Ln();
    	if($n == 2){
			$pdf->Cell(98);
		}
		$pdf->Cell(97, 5, "Jefe de Depto. ".utf8_decode($row2['Depto']), "",0,"C", False);
		$pdf->Ln();
		$n = $n + 1;
		if($n > 2){
			$pdf->Ln(20);
			$n = 1;
		}
    }


	$pdf->SetFont('Arial', '', 10);
	$pdf->Output('Docentes por Hora - '.$varPeriodo.' PAC - '.$varAnio.'.pdf','I');

?>