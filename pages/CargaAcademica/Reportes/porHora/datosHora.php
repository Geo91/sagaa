<?php
    $maindir = "../../../../";
    //require_once ("clases.php");
    require_once ("../../clases.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO

    if (isset($_POST['vPeriodo']) && isset($_POST['vAnio']) ) {

        $varPeriodo = $_POST['vPeriodo'];
        $varAnio = $_POST['vAnio'];

        $sql = "CALL SP_CA_DOCENTES_HORA_T(?,?)";
        $query = $db->prepare($sql);
        $query->bindParam(1,$varPeriodo,PDO::PARAM_INT);
        $query->bindParam(2,$varAnio,PDO::PARAM_INT);
?>

<div class="row">
    <div class="box">
        <div class="box-body table-responsive">
            <table id="tablaDocentes" class="table table-bordered table-striped">
                <thead>
                    <tr>                                            
                        <th style="display:none;text-align:center;background-color:#386D95;color:white;">Codigo</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">No. Empleado</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Profesor</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Asignatura</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">UV</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Horario</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Departamento</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    try{
                        $query->execute();
                        while ($fila = $query->fetch()) {
                ?>
                            <tr>
                                <td style="display:none;"><?php echo $fila["id_seccion"]; ?></td>
                                <td><?php echo $fila["No_Empleado"]; ?></td>
                                <td><?php echo $fila["nombreE"]; ?></td>
                                <td><?php echo $fila["asignatura"] ?></td>
                                <td><?php echo $fila["uv"]; ?></td>
                                <td><?php echo $fila["Hora_inicio"]." - ".$fila["Hora_fin"]; ?></td>
                                <td><?php echo $fila["nombreDepto"]; ?></td>
                            </tr>
                <?php 
                        } //cierre del ciclo while para llenar la tabla de datos
                    }catch(PDOException $e){
                        echo "Error: ".$e;
                    }
                ?>
 
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div>
</div>

<script type="text/javascript">
//opciones para buscador en la tabla 
    $('#tablaDocentes')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaDocentes').dataTable({
        "order": [],
        "fnDrawCallback": function (oSettings) {

        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
</script>

<?php }else{
    echo mensajes("Problemas en la página, intentelo de nuevo o contacte al administrador del sistema","azul");
} ?>