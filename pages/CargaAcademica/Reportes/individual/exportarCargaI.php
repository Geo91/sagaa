<?php  
    $maindir = "../../../../";
    require_once ("../../clases.php");
    require_once($maindir."conexion/config.inc.php");
    require_once($maindir."Datos/funciones.php");

    if (isset($_POST['vPeriodo']) && isset($_POST['vAnio']) && isset($_POST['vDepto'])) {


        $varPeriodo = $_POST['vPeriodo'];
        $varAnio = $_POST['vAnio'];
        $varDepto = $_POST['vDepto'];

        echo mensajes('La distribución de aulas ha sido exportado éxitosamente', 'verde');
        echo "<script type='text/javascript'>
                var tP = '$varPeriodo';
                var tA = '$varAnio';
                var tD = '$varDepto';
                window.open('pages/CargaAcademica/Reportes/individual/reporteCargaI.php?vP='+tP+'&vA='+tA+'&vD='+tD);
            </script>";
            
    }else{
        echo mensajes("Hubo un error al intentar exportar el documento. Inténtelo de nuevo o contacte al administrador del sistema", "rojo");
    }

?>

<script type="text/javascript">
    //$("#tablaDatos").load('pages/CargaAcademica/Reportes/distribucion/datosDistribucion.php');
</script>