<?php

	$maindir = "../../../../";

	require_once($maindir."fpdf/fpdf.php");
	require_once($maindir."conexion/config.inc.php");

	$varPeriodo = $_GET['vP'];
    $varAnio = $_GET['vA'];
    $varDepto = $_GET['vD'];

    function Mayus($texto) {
		$resp = strtr(strtoupper($texto),"àèìòùáéíóúçñäëïöü","ÀÈÌÒÙÁÉÍÓÚÇÑÄËÏÖÜ");
		return $resp;
	}

    class PDF extends FPDF{
	     // Cabecera de página

    	function Header(){
    		$maindir = "../../../../";
	        $this->Image($maindir.'assets/img/logo_unah_docs.png', 10,3,35,20, 'PNG'); //Encabezado
    		$this->SetFont('Arial','B',12);
	        $this->SetTextColor(0,0,0);
	        $this->SetY(5);
	        $this->Cell(0, 5, utf8_decode('UNIVERSIDAD NACIONAL AUTÓNOMA DE HONDURAS'), 0,0,"C");
	        $this->Ln();
	        $this->Cell(0, 5, utf8_decode('SECRETARÍA EJECUTIVA DE DESARROLLO DE PERSONAL'), 0,0,"C");
	        $this->Ln();
	        $this->Cell(0, 5, utf8_decode('DEPARTAMENTO DE EFECTIVIDAD DE RECURSO HUMANO'), 0,0,"C");
	        $this->Ln();
	        $this->Cell(0, 5, utf8_decode('REGISTRO DE ACTIVIDADES DEL DOCENTE'), 0,0,"C");
	        $this->Ln(2);
    	}
	    
	}

	$pdf = new PDF();
	
	//Aqui debe ir el primer ciclo while

	$con = "SELECT persona.N_identidad, CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'Nombre', DATE_FORMAT(persona.Fecha_nacimiento,'%d/%m/%Y') as 'FechaNac', persona.Nacionalidad, empleado.No_Empleado, DATE_FORMAT(empleado.Fecha_ingreso,'%d/%m/%Y') as 'FechaIng', persona.Sexo, departamento_laboral.nombre_departamento FROM persona INNER JOIN (empleado INNER JOIN departamento_laboral ON empleado.Id_departamento=departamento_laboral.Id_departamento_laboral)ON persona.N_identidad=empleado.N_identidad WHERE departamento_laboral.nombre_departamento IN (SELECT ca_departamentos_carreras.nombre FROM ca_departamentos_carreras WHERE ca_departamentos_carreras.id_departamento = :cID)";

	$mquery = $db->prepare($con);
	$mquery->bindParam(":cID",$varDepto);
	$mquery->execute();

	while($r = $mquery->fetch()){
		$I = $r['N_identidad'];
		$NE = $r['No_Empleado'];
		$pdf->AddPage('L','folio',0);
		$pdf->SetAutoPageBreak(1,'5');
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(260);
		$pdf->Cell(18,5,utf8_decode('PERÍODO: '), 0,0,"L");
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(10,5,utf8_decode($varPeriodo), 0,0,"C");
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(10,5,utf8_decode('AÑO: '), 0,0,"L");
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(12,5,utf8_decode($varAnio), 0,0,"C");
		$pdf->Ln(6);
		$pdf->Rect(10,27,312,32);

		//cuadro de información general
		$pdf->Cell(1);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(18,5,utf8_decode('CENTRO:'), 0,0,"L");
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(50,5,utf8_decode(' CIUDAD UNIVERSITARIA '), 0,0,"L");
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(21,5,utf8_decode('FACULTAD:'), 0,0,"L");
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(50,5,utf8_decode(' CIENCIAS JURÍDICAS'),0,0,'L');
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(65);
		$pdf->Cell(35,5,utf8_decode('DEPARTAMENTO: '),0,0,'L');
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(35,5,strtoupper(utf8_decode($r['nombre_departamento'])),0,0,'L');
		$pdf->Ln(5);
		$pdf->Cell(1);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(45,5,utf8_decode('NOMBRE DEL DOCENTE:  '), 0,0,"L");
		$pdf->SetFont('Arial', '', 10);
		$pdf->CellFitSpace(80,5,strtoupper(utf8_decode(Mayus($r['Nombre']))), 0,0,"L");
		$pdf->Cell(15);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(30,5,utf8_decode('No. EMPLEADO: '),0,0,'L');
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(15,5,utf8_decode($NE),0,0,'L');
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(25,5,utf8_decode('CATEGORÍA: '),0,0,'L');
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(60,5,utf8_decode(''),0,0,'L');
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(12,5,utf8_decode('SEXO: '),0,0,"L");
		$pdf->SetFont('Arial', '', 10);

		if($r['Sexo'] == 'F'){
			$pdf->Cell(12,5,utf8_decode(' M(   ) '),0,0,"L");
			$pdf->Cell(10,5,utf8_decode(' F( X ) '),0,0,"L");
		}else{
			$pdf->Cell(12,5,utf8_decode(' M( X ) '),0,0,"L");
			$pdf->Cell(10,5,utf8_decode(' F(   ) '),0,0,"L");
		}
		$pdf->Ln(5);

		$pdf->Cell(1);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(35,5,utf8_decode('FORMACIÓN PROFESIONAL:'),0,0,"L");
		$pdf->Ln(5);
		

		$conl = "SELECT titulo.titulo FROM titulo INNER JOIN estudios_academico ON titulo.id_titulo=estudios_academico.titulo_id WHERE estudios_academico.ID_Tipo_estudio = '4' AND estudios_academico.N_identidad = :cNI LIMIT 1";
		$queryl = $db->prepare($conl);
		$queryl->bindParam(":cNI",$I);
		$queryl->execute();
		$countl = $queryl->rowCount();
		if($countl > 0){
			while($rl = $queryl->fetch()){
				$lic = $rl['titulo'];
				$pdf->Cell(1);
				$pdf->SetFont('Arial', 'B', 10);
				$pdf->Cell(35,5,utf8_decode('LICENCIATURA EN: '),0,0,"L");
				$pdf->SetFont('Arial', '', 10);
				$pdf->Cell(150,5,strtoupper(utf8_decode(Mayus($lic))),0,0,"L");
			}
		}else{
			$pdf->Cell(1);
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(35,5,utf8_decode('LICENCIATURA EN: '),0,0,"L");
			$pdf->SetFont('Arial', '', 10);
			$pdf->Cell(45,5,utf8_decode('_________________________'),0,0,"L");
		}

		$pdf->Ln(5);

		$conm = "SELECT titulo.titulo FROM titulo INNER JOIN estudios_academico ON titulo.id_titulo=estudios_academico.titulo_id WHERE estudios_academico.ID_Tipo_estudio = '2' AND estudios_academico.N_identidad = :cNI LIMIT 1";
		$querym = $db->prepare($conm);
		$querym->bindParam(":cNI",$I);
		$querym->execute();
		$countm = $querym->rowCount();
		$countm;
		if($countm > 0){
			while($rm = $querym->fetch()){
				$master = $rm['titulo'];
				$pdf->Cell(1);
				$pdf->SetFont('Arial', 'B', 10);
				$pdf->Cell(28,5,utf8_decode('MAESTRÍA EN: '),0,0,"L");
				$pdf->SetFont('Arial', '', 10);
				$pdf->CellFitSpace(100,5,strtoupper(utf8_decode(Mayus($master))),0,0,"L");
			}
		}else{
			$pdf->Cell(1);
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(28,5,utf8_decode('MAESTRÍA EN: '),0,0,"L");
			$pdf->SetFont('Arial', '', 10);
			$pdf->Cell(120,5,utf8_decode('_________________________'),0,0,"L");
		}

		$cond = "SELECT titulo.titulo FROM titulo INNER JOIN estudios_academico ON titulo.id_titulo=estudios_academico.titulo_id WHERE estudios_academico.ID_Tipo_estudio = '1' AND estudios_academico.N_identidad = :cNI LIMIT 1";
		$queryd = $db->prepare($cond);
		$queryd->bindParam(":cNI",$I);
		$queryd->execute();
		$countd = $queryd->rowCount();
		if($countd > 0){
			while($rd = $queryd->fetch()){
				$doc = $rd['titulo'];
				$pdf->Cell(1);
				$pdf->SetFont('Arial', 'B', 10);
				$pdf->Cell(32,5,utf8_decode('DOCTORADO EN: '),0,0,"L");
				$pdf->SetFont('Arial', '', 10);
				$pdf->CellFitSpace(120,5,strtoupper(utf8_decode(Mayus($doc))),0,0,"L");
			}
		}else{
			$pdf->Cell(1);
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(32,5,utf8_decode('DOCTORADO EN: '),0,0,"L");
			$pdf->SetFont('Arial', '', 10);
			$pdf->Cell(45,5,utf8_decode('_________________________'),0,0,"L");
		}

		$pdf->Ln(6);
		/*$pdf->Cell(6);
		$pdf->Cell(25,5,utf8_decode('Nacionalidad: '),0,0,"L");
		$pdf->Cell(25,5,utf8_decode($r['Nacionalidad']),0,0,"L");
		$pdf->Cell(10,5,utf8_decode('Sexo: '),0,0,"L");

		if($r['Sexo'] == 'F'){
			$pdf->Cell(25,5,utf8_decode(' M(  ) F( X )'),0,0,"L");
		}else{
			$pdf->Cell(25,5,utf8_decode(' M( X ) F(  )'),0,0,"L");
		}
		
		$pdf->Cell(32,5,utf8_decode('Fecha de Ingreso: '),0,0,"L");
		$pdf->Cell(35,5,utf8_decode($r['FechaIng']),0,0,"L");
		$pdf->Cell(38,5,utf8_decode('Fecha de Nacimiento: '),0,0,"L");
		$pdf->Cell(35,5,utf8_decode($r['FechaNac']),0,0,"L");
		$pdf->Cell(16,5,utf8_decode('Sueldo: '),0,0,"L");
		$pdf->Cell(25,5,utf8_decode('__________________________'),0,0,"L");
		$pdf->Ln(8);*/

		$conj = "SELECT ca_tipos_contratos.nombre, DATE_FORMAT(ca_profesores.hora_ent,'%h:%i') as 'horaE', DATE_FORMAT(ca_profesores.hora_sal,'%h:%i') as 'horaS' FROM ca_profesores INNER JOIN empleado ON ca_profesores.No_Empleado=empleado.No_Empleado INNER JOIN ca_tipos_contratos ON ca_profesores.id_contrato=ca_tipos_contratos.id_contrato WHERE ca_profesores.No_Empleado = :cNE";
		$queryj = $db->prepare($conj);
		$queryj->bindParam(":cNE",$NE);
		$queryj->execute();
		while($rj = $queryj->fetch()){ // Ciclo while para obtener e imprimir el tipo de contrato del docente
			$jorn = $rj['nombre'];
			if($jorn == 'Tiempo Completo' or $jorn == ('Jefatura - Coordinación')){
				$pdf->Cell(1);
				$pdf->SetFont('Arial', 'B', 10);
				$pdf->Cell(20,5,utf8_decode('JORNADA: '),0,0,"L");
				$pdf->SetFont('Arial', '', 10);
				$pdf->Cell(48,5,utf8_decode('TIEMPO COMPLETO: ( X )'),0,0,"L");
				$pdf->Cell(35,5,utf8_decode('MEDIO TIEMPO: (  )'),0,0,"L");
				$pdf->Cell(53,5,utf8_decode('DEDICACIÓN EXCLUSIVA: (  ) '),0,0,"L");
				$pdf->Cell(53,5,utf8_decode('PROFESOR POR HORA: (  ) '),0,0,"L");
				$pdf->Cell(33,5,(utf8_decode('HORARIO: ').$rj['horaE'].' - '.$rj['horaS']),0,0,"L");
			}elseif($jorn == 'Medio Tiempo'){
				$pdf->Cell(1);
				$pdf->SetFont('Arial', 'B', 10);
				$pdf->Cell(20,5,utf8_decode('JORNADA: '),0,0,"L");
				$pdf->SetFont('Arial', '', 10);
				$pdf->Cell(45,5,utf8_decode('TIEMPO COMPLETO: (  )'),0,0,"L");
				$pdf->Cell(37,5,utf8_decode('MEDIO TIEMPO: ( X )'),0,0,"L");
				$pdf->Cell(53,5,utf8_decode('DEDICACIÓN EXCLUSIVA: (  ) '),0,0,"L");
				$pdf->Cell(53,5,utf8_decode('PROFESOR POR HORA: (  ) '),0,0,"L");
				$pdf->Cell(33,5,utf8_decode('HORARIO: ').$rj['horaE'].' - '.$rj['horaS'],0,0,"L");
			}elseif($jorn == 'Por hora'){
				$pdf->Cell(1);
				$pdf->SetFont('Arial', 'B', 10);
				$pdf->Cell(20,5,utf8_decode('JORNADA: '),0,0,"L");
				$pdf->SetFont('Arial', '', 10);
				$pdf->Cell(45,5,utf8_decode('TIEMPO COMPLETO: (  )'),0,0,"L");
				$pdf->Cell(35,5,utf8_decode('MEDIO TIEMPO: (  )'),0,0,"L");
				$pdf->Cell(53,5,utf8_decode('DEDICACIÓN EXCLUSIVA: (  ) '),0,0,"L");
				$pdf->Cell(53,5,utf8_decode('PROFESOR POR HORA: ( X ) '),0,0,"L");
				$pdf->Cell(33,5,utf8_decode('HORARIO: ').$rj['horaE'].' - '.$rj['horaS'],0,0,"L");
			}
		} // Fin del Ciclo while para obtener e imprimir el tipo de contrato del docente
		
		//cuadro de asignaturas
		$pdf->Ln(5);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(0,5,utf8_decode('REGISTRO DE ACTIVIDADES DEL DOCENTE'),0,0,"C");
		$pdf->Ln(6);
		
		//Encabezados de la tabla
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(30, 7, utf8_decode("CODIGO ASIGNATURA"), 1,0,"C", False);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(113, 7, utf8_decode("NOMBRE ASIGNATURA"), 1,0,"C", False);
		$pdf->Cell(20, 7, utf8_decode("SECCIÓN"), 1,0,"C", False);
		$pdf->Cell(12, 7, utf8_decode("U.V"), 1,0,"C", False);
		$pdf->Cell(7, 7, utf8_decode("L"), 1,0,"C", False);
		$pdf->Cell(7, 7, utf8_decode("M"), 1,0,"C", False);
		$pdf->Cell(7, 7, utf8_decode("Mi"), 1,0,"C", False);
		$pdf->Cell(7, 7, utf8_decode("J"), 1,0,"C", False);
		$pdf->Cell(7, 7, utf8_decode("V"), 1,0,"C", False);
		$pdf->Cell(7, 7, utf8_decode("S"), 1,0,"C", False);
		$pdf->Cell(35, 7, utf8_decode("EDIFICIO"), 1,0,"C", False);
		$pdf->Cell(38, 7, utf8_decode("AULA"), 1,0,"C", False);
		$pdf->Cell(22, 7, utf8_decode("No. ALUM"), 1,0,"C", False);

		$pdf->Ln();
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(30, 6, utf8_decode("hora de consulta/"), 1,0,"C", False);
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->Cell(145, 6, utf8_decode("Defina Ubicación/.."), 1,0,"L", False);
		$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
		$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
		$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
		$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
		$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
		$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
		$pdf->Cell(35, 6, utf8_decode(""), 1,0,"C", False);
		$pdf->Cell(38, 6, utf8_decode(""), 1,0,"C", False);
		$pdf->Cell(22, 6, utf8_decode(""), 1,0,"C", False);
		$pdf->Ln();

		//Contenido de la tabla 
		$conca = "SELECT ca_secciones.id_seccion, ca_secciones.codigo_seccion, ca_secciones.id_asignatura, ca_asignaturas.nombre as 'nAsignatura', ca_asignaturas.uv, ca_secciones.Hora_inicio, ca_secciones.Hora_fin, ca_aulas.nombre as 'nAula', ca_edificios.nombre as 'nEdificio', ca_secciones.No_Empleado FROM ca_secciones,ca_asignaturas,ca_aulas,ca_edificios,empleado,ca_cargas,ca_periodos,ca_departamentos_carreras WHERE ca_secciones.id_asignatura=ca_asignaturas.id_asignatura AND ca_secciones.id_aula=ca_aulas.id_aula AND ca_aulas.id_edificio=ca_edificios.id_edificio AND ca_secciones.No_Empleado=empleado.No_Empleado AND ca_secciones.id_carga=ca_cargas.id_carga AND ca_cargas.id_periodo=ca_periodos.id_periodo AND ca_cargas.id_departamento=ca_departamentos_carreras.id_departamento AND ca_periodos.periodo=:cP AND ca_periodos.anio=:cA AND ca_secciones.No_Empleado = :cNE";
		$queryca = $db->prepare($conca);
		$queryca->bindParam(":cP",$varPeriodo);
		$queryca->bindParam(":cA",$varAnio);
		$queryca->bindParam(":cNE",$NE);
		$queryca->execute();

		while($rca = $queryca->fetch()){ // Ciclo while para obtener las secciones que impartirá el docente
			$pdf->SetFont('Arial', '', 10);
			
			$pdf->Cell(30, 6, utf8_decode($rca['id_asignatura']), 1,0,"C", False); //codigo asignatura
			$pdf->Cell(113, 6, strtoupper(utf8_decode(Mayus($rca['nAsignatura']))), 1,0,"L", False); //nombre asignatura
			$pdf->Cell(20, 6, date("H",strtotime($rca['Hora_inicio'])).' - '.date("H",strtotime($rca['Hora_fin'])), 1,0,"C", False);
			$pdf->Cell(12, 6, $rca['uv'], 1,0,"C", False); // horario de la seccion

			// Adición de Días de la sección
			$condia = "SELECT ca_dias.id_dia,ca_dias.nombre FROM ca_dias INNER JOIN ca_secciones_dias ON ca_dias.id_dia=ca_secciones_dias.id_dia INNER JOIN ca_secciones ON ca_secciones.id_seccion=ca_secciones_dias.id_seccion WHERE ca_secciones.id_seccion = :cD";
			$querydia = $db->prepare($condia);
			$querydia->bindParam(":cD",$rca['id_seccion']);
			$querydia->execute();
			$countdias = $querydia->rowCount();
			$n=1;
			
			if($countdias == 5){ // Inicio condiciones IF
				while($rdia = $querydia->fetch()){
					$dia = $rdia['nombre'];
					$ndia = $rdia['id_dia'];
					
					
					while($n <= 6){
						if($n == $ndia){
							$pdf->Cell(7, 6, utf8_decode("X"), 1,0,"C", False);
							break;
						}else{
							$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
							$n++;
						}
					}
					$n++;
				}
				$pdf->Cell(7, 7, utf8_decode(""), 1,0,"C", False);
			}elseif($countdias == 4){ // Inicio condiciones IF
				while($rdia = $querydia->fetch()){
					$dia = $rdia['nombre'];
					$ndia = $rdia['id_dia'];
					
					
					while($n <= 6){
						if($n == $ndia){
							$pdf->Cell(7, 6, utf8_decode("X"), 1,0,"C", False);
							break;
						}else{
							$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
							$n++;
						}
					}
					$n++;
				}
				$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
			}elseif($countdias == 3){
				while($rdia = $querydia->fetch()){
					$dia = $rdia['nombre'];
					$ndia = $rdia['id_dia'];
					
					
					while($n <= 6){
						if($n == $ndia){
							$pdf->Cell(7, 6, utf8_decode("X"), 1,0,"C", False);
							break;
						}else{
							$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
							$n++;
						}
					}
					$n++;
				}
				$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
			}elseif($countdias == 2){
				$m = 1;
				while($rdia = $querydia->fetch()){ // Ciclo while para recorrer los días de la sección
					$dia = $rdia['nombre'];
					$ndia = $rdia['id_dia'];
					
					while($n <= 6){ // Ciclo while para marcar las casillas de los días de la sección y las casillas en blanco de los días que no es impartida la sección.
						if($n == $ndia){
							$pdf->Cell(7, 6, utf8_decode("X"), 1,0,"C", False);
							break;
						}else{
							$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
							$n++;
						}
					} // Fin Ciclo while para marcar las casillas de los días de la sección y las casillas en blanco de los días que no es impartida la sección.
					$n++;
					if($ndia == 5 && $m == 2){
						$pdf->Cell(7, 7, utf8_decode(""), 1,0,"C", False);
					}elseif($ndia == 4 && $m == 2){
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
					}elseif($ndia == 3){
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
					}
					$m++;
				} // Fin de Ciclo while para recorrer los días de la sección
			}elseif($countdias == 1){
				while($rdia = $querydia->fetch()){ // Ciclo while para recorrer los días de la sección
					$dia = $rdia['nombre'];
					$ndia = $rdia['id_dia'];
					
					while($n <= 6){ // Ciclo while para marcar las casillas de los días de la sección y las casillas en blanco de los días que no es impartida la sección.
						if($n == $ndia){
							$pdf->Cell(7, 6, utf8_decode("X"), 1,0,"C", False);
							break;
						}else{
							$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
							$n++;
						}
					} // Fin Ciclo while para marcar las casillas de los días de la sección y las casillas en blanco de los días que no es impartida la sección.
					$n++;
					if($ndia == 5){
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
					}elseif($ndia == 4){
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
					}elseif($ndia == 3){
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
					}elseif($ndia == 2){
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
					}elseif($ndia == 1){
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
						$pdf->Cell(7, 6, utf8_decode(""), 1,0,"C", False);
					}
				} // Fin de Ciclo while para recorrer los días de la sección
			} // fin de las condiciones IF
				
			$pdf->CellFitSpace(35, 6, utf8_decode($rca['nEdificio']), 1,0,"C", False); // Edificio
			$pdf->CellFitSpace(38, 6, utf8_decode($rca['nAula']), 1,0,"C", False); // Aula
			$pdf->Cell(22, 6, utf8_decode(""), 1,0,"C", False); // No. A
			$pdf->Ln();
		} // Ciclo while para obtener las secciones que impartirá el docente
	
		$pdf->Ln(2);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(0,5,utf8_decode('ACTIVIDAD DE INVESTIGACIÓN, EXTENSIÓN U OTRA'),0,0,"C");
		$pdf->Ln();
		
		$pdf->Cell(105, 6, utf8_decode("ACTIVIDAD"), 1,0,"C", False);
		$pdf->Cell(152, 6, utf8_decode("NOMBRE DEL PROYECTO"), 1,0,"C", False);
		$pdf->Cell(55, 6, utf8_decode("HORAS SEMANALES"), 1,0,"C", False);
		$pdf->Ln();
		$pdf->SetFont('Arial', '', 10);

		$help = "SELECT ca_periodos.id_periodo FROM ca_periodos WHERE ca_periodos.periodo = :cP AND ca_periodos.anio = :cA";
		$queryhelp = $db->prepare($help);
		$queryhelp->bindParam(":cP",$varPeriodo);
		$queryhelp->bindParam(":cA",$varAnio);
		$queryhelp->execute();
		while($rhelp = $queryhelp->fetch()){
			$varPID = $rhelp['id_periodo'];
			break;
		}

		$conact = "SELECT ca_actividad.descripcion FROM ca_actividad INNER JOIN ca_actividad_empleado ON ca_actividad.id_actividad = ca_actividad_empleado.actividad_id WHERE ca_actividad.estado = '1' AND ((ca_actividad.periodo_id = :cPiD AND ca_actividad.tipo = 'TRIMESTRAL') OR ca_actividad.tipo = 'PERMANENTE') AND ca_actividad_empleado.empleado_no = :cNE LIMIT 4";
		$queryact = $db->prepare($conact);
		$queryact->bindParam(":cPiD",$varPID);
		$queryact->bindParam(":cNE",$NE);
		$queryact->execute();
		$conteo2 = $queryact->rowCount();

		//GENERANDO LA TABLA DE ACTIVIDADES DE INVESTIGACIÓN, EXTENSIONES U OTRAS
		if ($conteo2 == 0){
			
			$pdf->Cell(105, 6, utf8_decode(""), 1,0,"C", False);
			$pdf->Cell(152, 6, utf8_decode(""), 1,0,"C", False);
			$pdf->Cell(55, 6, utf8_decode(""), 1,0,"C", False);
			$pdf->Ln();
			
			$pdf->Cell(105, 6, utf8_decode(""), 1,0,"C", False);
			$pdf->Cell(152, 6, utf8_decode(""), 1,0,"C", False);
			$pdf->Cell(55, 6, utf8_decode(""), 1,0,"C", False);
			$pdf->Ln();
			
			$pdf->Cell(105, 6, utf8_decode(""), 1,0,"C", False);
			$pdf->Cell(152, 6, utf8_decode(""), 1,0,"C", False);
			$pdf->Cell(55, 6, utf8_decode(""), 1,0,"C", False);
			$pdf->Ln();

		}elseif($conteo2 == 1){
			while($ract = $queryact->fetch()){
				
				$pdf->CellFitSpace(105, 6, strtoupper(utf8_decode($ract['descripcion'])), 1,0,"C", False);
				$pdf->Cell(152, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(55, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Ln();
			}				
			
			$pdf->Cell(105, 6, utf8_decode(""), 1,0,"C", False);
			$pdf->Cell(152, 6, utf8_decode(""), 1,0,"C", False);
			$pdf->Cell(55, 6, utf8_decode(""), 1,0,"C", False);
			$pdf->Ln();
			
			$pdf->Cell(105, 6, utf8_decode(""), 1,0,"C", False);
			$pdf->Cell(152, 6, utf8_decode(""), 1,0,"C", False);
			$pdf->Cell(55, 6, utf8_decode(""), 1,0,"C", False);
			$pdf->Ln();
		}elseif($conteo2 >= 2){
			while($ract = $queryact->fetch()){
				
				$pdf->CellFitSpace(105, 6, strtoupper(utf8_decode($ract['descripcion'])), 1,0,"C", False);
				$pdf->Cell(152, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(55, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Ln();
			}
			if($conteo2 = 2){
				
				$pdf->Cell(105, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(152, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(55, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Ln();
			}
		}



		$conjefe = "SELECT ca_departamentos_carreras.nombre AS 'depto' FROM ca_departamentos_carreras INNER JOIN ca_jefes_departamentos ON ca_departamentos_carreras.id_departamento=ca_jefes_departamentos.id_departamento WHERE ca_jefes_departamentos.id_Usuario IN (SELECT usuario.id_Usuario FROM usuario WHERE usuario.No_Empleado = :cNE)";
		$querycjd = $db->prepare($conjefe);
		$querycjd->bindParam(":cNE",$NE);
		$querycjd->execute();
		$conteo = $querycjd->rowCount();

		//GENERANDO LA TABLA DE COMISIÓN O COORDINACIÓN
		$pdf->Ln(2);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(105, 6, utf8_decode("COMISIÓN O COORDINACIÓN"), 1,0,"C", False);
		$pdf->Cell(152, 6, utf8_decode("CARRERA"), 1,0,"C", False);
		$pdf->Cell(55, 6, utf8_decode("HORAS SEMANALES"), 1,0,"C", False);
		$pdf->Ln();
		$pdf->SetFont('Arial', '', 10);

		if ($conteo == 1){
			while($rjd = $querycjd->fetch()){
				
				$pdf->CellFitSpace(105, 6, 'JEFE DEPARTAMENTO '.strtoupper(utf8_decode($rjd['depto'])), 1,0,"C", False);
				$pdf->Cell(152, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(55, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Ln();
				
				$pdf->Cell(105, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(152, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(55, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Ln();
				
				$pdf->Cell(105, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(152, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(55, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Ln();
			}
		}else{
			if ($conteo == 0){
				
				$pdf->Cell(105, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(152, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(55, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Ln();
				
				$pdf->Cell(105, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(152, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(55, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Ln();
				
				$pdf->Cell(105, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(152, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Cell(55, 6, utf8_decode(""), 1,0,"C", False);
				$pdf->Ln();
			}
		}
		
		$pdf->Ln();
		$pdf->Cell(0,4,'OBSERVACIONES: ______________________________________________________________________________________________________________ ',0,0,'L');
		$pdf->Ln(6);
		$pdf->Cell(0,4,'_______________________________________________________________________________________________________________________________ ',0,0,'L');
		$pdf->Ln();

		$conjd = "SELECT CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as 'Jefe', ca_departamentos_carreras.nombre as 'Depto' FROM persona INNER JOIN (empleado INNER JOIN (usuario INNER JOIN (ca_jefes_departamentos INNER JOIN ca_departamentos_carreras ON ca_jefes_departamentos.id_departamento=ca_departamentos_carreras.id_departamento) ON usuario.id_Usuario=ca_jefes_departamentos.id_Usuario) ON empleado.No_Empleado=usuario.No_Empleado) ON persona.N_identidad=empleado.N_identidad WHERE ca_departamentos_carreras.id_departamento=:cID";

		$queryjd = $db->prepare($conjd);
		$queryjd->bindParam(":cID",$varDepto);
	    $queryjd->execute();
	    while ($rjd = $queryjd->fetch()){
	    	$jefe = Mayus($rjd['Jefe']);
	    	$depto = Mayus($rjd['Depto']);
	    }

		$pdf->SetY(198);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(258,4,'FIRMA DEL DOCENTE: _________________________________',0,0,'L');
		$pdf->Cell(58,4,'VO.BO. _____________________________________________ ',0,0,'R');
		$pdf->Ln();
		$pdf->Cell(0,4,'FIRMA Y SELLO DEL JEFE DEPARTAMENTO ',0,0,'R');
		$pdf->Ln();
		$pdf->Cell(245);
		$pdf->Cell(50,4,'(obligatorio)',0,0,'C');
	}


	$pdf->SetFont('Arial', '', 10);
	$pdf->Output('Carga Departamento - '.$depto.' - '.$varPeriodo.' PAC - '.$varAnio.'.pdf','I');

?>