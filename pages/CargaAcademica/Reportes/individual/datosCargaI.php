<?php
    $maindir = "../../../../";
    require_once ("clases.php");
    require_once ("../../clases.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO

    if (isset($_POST['vPeriodo']) && isset($_POST['vAnio']) && isset($_POST['vDepto']) ) {

        $varPeriodo = $_POST['vPeriodo'];
        $varAnio = $_POST['vAnio'];
        $varDepto = $_POST['vDepto'];

        $sql = "CALL SP_CA_CARGA_INDIVIDUALIZADA(?,?,?)";
        $query = $db->prepare($sql);
        $query->bindParam(1,$varPeriodo,PDO::PARAM_INT);
        $query->bindParam(2,$varAnio,PDO::PARAM_INT);
        $query->bindParam(3,$varDepto,PDO::PARAM_INT);

        $subTotal=0;

        $query->execute();
        $tempID=-1;
        $seccionesTabla = array();
        $Registros = false;
        while ($fila = $query->fetch()) {
            if ($tempID!=$fila["id_seccion"]) {
                $tempID = $fila["id_seccion"];
                $id_seccion = $fila["id_seccion"];
                $codigo_seccion = $fila["codigo_seccion"];
                $id_asignatura = $fila["id_asignatura"];
                $nAsignatura = $fila["nAsignatura"];
                $Hora_inicio = $fila["Hora_inicio"];
                $Hora_fin = $fila["Hora_fin"];
                $id_aula = $fila["id_aula"];
                $nAula = $fila["nAula"];
                $nEdificio = $fila["nEdificio"];
                $No_Empleado = $fila["No_Empleado"];
                $nombreE = $fila["nombreE"];
                $Depto = $fila["nombreDepto"];
                $seccionesTabla[$fila["id_seccion"]] = new SeccionCarga($id_seccion,
                                         $codigo_seccion,
                                         $id_asignatura,
                                         $nAsignatura, 
                                         $Hora_inicio,
                                         $Hora_fin,
                                         $id_aula,
                                         $nAula,
                                         $nEdificio,
                                         $No_Empleado,
                                         $nombreE,
                                         $Depto);
                $seccionesTabla[$fila["id_seccion"]]->Dias[$fila["id_aula"]]=$fila["Dia"];
                $subTotal=$subTotal+1;
            }else{
                $seccionesTabla[$fila["id_seccion"]]->Dias[$fila["id_dia"]]=$fila["Dia"];
            }
            $Registros = true;
        }

?>

<div class="row">
    <div class="box">
        <div class="box-body table-responsive">
            <table id="tablaCarga" class="table table-bordered table-striped">
                <thead>
                    <tr>                                            
                        <th style="text-align:center;background-color:#386D95;color:white;">Código</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Asignatura</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Horario</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Dias</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Aula</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Edificio</th>
                        <th style="text-align:center;background-color:#386D95;color:white;">Profesor</th>
                        <th style="text-align:center;background-color:#386D95;color:white;<?php if ($varDepto != -1){ echo "display:none;"; }  ?>">Departamento</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if ($Registros) {
                    foreach ($seccionesTabla as $key => $subSeccion) { 
                        $TempIDseccion = $subSeccion->getid_seccion();
                        ?>
                        <tr data-id='<?php echo $TempIDseccion; ?>'  
                        data-asignatura='<?php echo $subSeccion->getid_asignatura(); ?>'
                        data-aula='<?php echo $subSeccion->getid_aula(); ?>'
                        data-empleado='<?php echo $subSeccion->getNo_Empleado(); ?>'
                        data-hori='<?php echo $subSeccion->getHora_inicio(); ?>'
                        data-horf='<?php echo $subSeccion->getHora_fin(); ?>'>
                            <td><?php echo $subSeccion->getcodigo_seccion(); ?></td>
                            <td><?php echo $subSeccion->getnAsignatura(); ?></td>
                            <td><?php echo substr($subSeccion->getHora_inicio(),0,-3)."\n <br/>". substr($subSeccion->getHora_fin(),0,-3); ?></td>
                            </td>
                            <td>
                            <?php foreach ($subSeccion->Dias as $value) {
                                    if ($value!='Sábado'){
                                        echo substr($value, 0,2);
                                    }else{
                                        echo "Sa \n <br>";
                                    }
                                }
                            ?></td>
                            <td><?php echo $subSeccion->getnAula(); ?></td>
                            <td><?php echo $subSeccion->getnEdificio(); ?></td>
                            <td><?php echo $subSeccion->getnombreE(); ?></td>
                            <td <?php if ($varDepto != -1){ echo "style ='display:none';"; }  ?> ><?php echo $subSeccion->getDepto(); ?></td>
                        </tr>
                <?php } 
                }?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div>
</div>

<script type="text/javascript">
//opciones para buscador en la tabla 
    $('#tablaCarga')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaCarga').dataTable({
        "order": [],
        "fnDrawCallback": function (oSettings) {

        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
</script>

<?php } else{
    echo mensajes("Problemas en la página, intentelo de nuevo","azul");
} ?>