$(document).ready(function(){   

//para cuando se da submit al formulario formProgramacion
    $("#formDistribucionAulas").submit(function(e) {
        e.preventDefault();
        data={
            vPeriodo:$('#vPeriodo').val(),
            vAnio:$('#vAnio').val(),
            vDepto:$('#vDepto').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: cargarDatos,
            timeout: 10000,
            error: problemas
        }); 
        return false;
    });
});  

function inicioEnvio(){
    $("#tablaDatos").text('Cargando...');
}

function problemas(){
    $("#tablaDatos").text('Problemas en el servidor.');
}

function cargarDatos(){
    $("#tablaDatos").load('pages/CargaAcademica/Reportes/individual/datosCargaI.php',data);
}

function exportarPDF(){
    $("#divRespuesta").load('pages/CargaAcademica/Reportes/individual/exportarCargaI.php',data);
    $("#tablaDatos").empty();
}