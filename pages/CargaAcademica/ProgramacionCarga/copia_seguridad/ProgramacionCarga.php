<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
?>
<div id="divRespuesta">
</div>
<div class="row">
    <div class="col-lg-10">
        <h1 class="page-header">Programación de registro de carga académica</h1>
			<div class="panel panel-default">
			    <div class="panel-heading"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo periodo</div>
<!--**********************form*******************-->
				 <form class="form-horizontal" role="form" id="formProgramacion" name="formProgramacion"> 
				 	<br> 
                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Período </label>                       
                        <div class="col-sm-8">                   
                            <input type="number" id="periodo" name="periodo" min="1" max="3" value="1" required>
                        </div>                  
                    </div>

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Año </label>
                        <div class="col-sm-8">                   
                            <input type="number" id="anio" name="anio" min="<?php echo date("Y"); ?>" max="2100" value="<?php echo date("Y"); ?>" required>
                        </div>                  
                    </div>
					<hr>
 					<div class="row form-group" > 
                    	<label class="col-sm-6 control-label" >Fechas de inicio de cada proceso:</label>
                    </div>
                    <hr>
 					<div class="row form-group" >                    
                        <label class=" col-sm-4 control-label" >Ingreso de cargas</label>
                        <div class="col-sm-8">  
			    			<p> <input type="date" id="fechaInicio" placeholder="aaaa-mm-dd" required></p>
                        </div>                  
                    </div>
                    <div class="row form-group" >                    
                        <label class=" col-sm-4 control-label" >Revisión</label>
                        <div class="col-sm-8">  
                            <p> <input type="date" id="fechaRevision" placeholder="aaaa-mm-dd" required></p>
                        </div>  
                    </div>               
                    <div class="row form-group" >                    
                        <label class=" col-sm-4 control-label" >Modificación</label>
                        <div class="col-sm-8">  
                            <p> <input type="date" id="fechaModificación" placeholder="aaaa-mm-dd" required></p>
                        </div>   
                    </div>                           

 					<div class="row form-group" >                    
                        <label class=" col-sm-4 control-label" >Finalización</label>
                        <div class="col-sm-8">  
			    			<p> <input type="date" id="fechaFin" placeholder="aaaa-mm-dd" required></p>
                        </div>                  
                    </div>

				    <div class="row">
				        <label class="control-label col-sm-2"></label>
				        <div class="col-sm-8">
				            <p aling ="right">
				                <button type="submit" class="btn btn-primary btn-primary col-sm-offset-8" ><span class=" glyphicon glyphicon-floppy-disk"></span> Guardar</button> 
				            </p>          
				        </div>                            
				    </div> 
				</form>
<!--********************************from*******************-->
			</div>
    </div>
</div> 

<script type="text/javascript" src="pages/CargaAcademica/ProgramacionCarga/Script.js"></script>