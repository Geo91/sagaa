$(document).ready(function(){   

//para cuando se da submit al formulario formProgramacion
    $("#formProgramacion").submit(function(e) {
        e.preventDefault();
        var fechaInicio=$('#fechaInicio').val();
        var fechaRevision=$('#fechaRevision').val();
        var fechaModificación=$('#fechaModificación').val();
        var fechaFin=$('#fechaFin').val();
        var fehcaR=false;
        var fehcaM=false;
        var fehcaF=false;
        if (fechaInicio>=fechaRevision){
            fehcaR=true;
        }
        if (fechaRevision>=fechaModificación){
            fehcaM=true;
        }
        if (fechaModificación>=fechaFin){
            fehcaF=true;
        }
        if (fehcaR || fehcaM || fehcaF){
            alert("Cada fecha debe ser mayor a la fecha del proceso anterior.");
        }else{
            data={
                        periodo:$('#periodo').val(),
                        anio:$('#anio').val(),
                        fechaInicio:$('#fechaInicio').val(),
                        fechaRevision:$('#fechaRevision').val(),
                        fechaModificación:$('#fechaModificación').val(),
                        fechaFin:$('#fechaFin').val()
                    }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                beforeSend: inicioEnvio,
                success: agregarPeriodo,
                timeout: 4000,
                error: problemas
            }); 
            return false;
        }
    });
});


function inicioEnvio(){
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function agregarPeriodo(){
    $("#divRespuesta").load('pages/CargaAcademica/ProgramacionCarga/AgregarPeriodo.php',data);
    $('#formProgramacion').trigger("reset");
}
