<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");
    if (isset($_POST['periodo']) && 
        isset($_POST['anio']) && 
        isset($_POST['fechaInicio']) && 
        isset($_POST['fechaRevision']) && 
        isset($_POST['fechaModificación']) && 
        isset($_POST['fechaFin']) ){
        
        $periodo = $_POST['periodo'];
        $anio = $_POST['anio'];
        $fechaInicio = $_POST['fechaInicio'];
        $fechaRevision = $_POST['fechaRevision'];
        $fechaModificación = $_POST['fechaModificación'];
        $fechaFin = $_POST['fechaFin'];
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query0 = "SELECT * FROM `ca_periodos` WHERE `periodo`=$periodo and `anio`=$anio";
        $query1 = "SELECT * FROM `ca_periodos` 
                WHERE (ca_periodos.fecha_ingreso_i BETWEEN '$fechaInicio' and '$fechaFin') 
                    or (ca_periodos.fecha_ingreso_f BETWEEN '$fechaInicio' and '$fechaFin') 
                    or ('$fechaInicio' BETWEEN ca_periodos.fecha_ingreso_i and ca_periodos.fecha_ingreso_f)";
        $query = "INSERT INTO `ca_periodos` (`id_periodo`, `periodo`, `anio`, `fecha_ingreso_i`,`fecha_revision`,`fecha_modificacion`, `fecha_ingreso_f`) VALUES (NULL, '$periodo', '$anio', '$fechaInicio', '$fechaRevision', '$fechaModificación', '$fechaFin');";

        try{
            $bandera = 0;
            //validacion de periodos
            $result =$db->prepare($query0);
            $result->execute();
            while ($fila = $result->fetch()) {
                $bandera = 1;
                break;
            }
            //validacion de fechas
            $result =$db->prepare($query1);
            $result->execute();
            while ($fila = $result->fetch()) {
                $bandera = 2;
                break;
            }

            if($bandera==1){
                $mensaje = "Ya existe el periodo para la carga $periodo-$anio";
                $color = "azul";
            }
            elseif($bandera==2){
                $mensaje = "Las fechas de inicio y fin de periodo de ingreso interfieren con el período de otra carga académica";
                $color = "azul";
            
            }else{
                $result =$db->prepare($query);
                $result->execute();

        // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
                $deptos = array();
                $query1 = "SELECT * FROM `ca_departamentos_carreras`";
                $result =$db->prepare($query1);
                $result->execute();
                $index = 0 ;
                while ($fila = $result->fetch()) {
                    $deptos[$index] = $fila['id_departamento'];
                    $index = $index+1;
                }
                foreach ($deptos as $llave => $val) {
        // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
                    $query3 = "INSERT INTO `ca_cargas` (`id_carga`, `id_periodo`, `aprobado`, `id_departamento`) VALUES (NULL, (SELECT `id_periodo` FROM `ca_periodos` WHERE `periodo`=$periodo and `anio`=$anio), '0', '$val');";
                    $result =$db->prepare($query3);
                    $result->execute();
                }
                $mensaje = "Periodo establecido";
                $color = "verde";
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>