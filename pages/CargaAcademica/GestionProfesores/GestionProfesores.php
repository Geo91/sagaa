<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
if($_SESSION['user_rol']!=30){  
?>
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header panel-primary"><center><strong>  Gestión de Docentes </strong></center></h3>
    </div>
</div> 
<div id="divRespuesta"></div>     
<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Docentes sin contrato</div>

    <div class="panel-body" >
        <div class="row">
            <div class="col-lg-12">
            <!--************************formulario*********************-->
                <div id="formularioAgregar">
                    <?php require_once("AgregarContrato.php"); ?>
                </div>
            <!--************************formulario*********************-->
            </div>
        </div>
    </div>                                    
</div> 
<?php } ?>
<!--tabla de contratos-->
<div class="panel panel-primary">
    <div class="panel-heading"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Registro de Docentes </div>

    <div class="panel-body" >
    <div class="row">
        <div class="col-lg-12">

            <div id="divRespuestaEditar">
            </div>
            <div id="datosTabla">
                <?php require_once("DatosProfesores.php"); ?>
            </div>
        </div>
    </div>
</div>
<!--******************modal de edicion*****************-->
<div class="modal fade" id="modalTemp" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div id="subContenidoTemp">
        </div>
      </div>
    </div>
</div>
<!--******************modal de edicion*****************-->
<script type="text/javascript" src="pages/CargaAcademica/GestionProfesores/Script.js"></script>