<?php
    $maindir = "../../../";
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO

    if(isset($_POST['No_empleado']) &&
    isset($_POST['id_contrato']) &&
    isset($_POST['uv']) &&
    isset($_POST['accion']) &&
    isset($_POST['horaEnt']) &&
    isset($_POST['horaSal'])) {
            $No_empleado = $_POST['No_empleado'];
            $id_contrato = $_POST['id_contrato'];
            $horaEntrada = $_POST['horaEnt'];
            $horaSalida = $_POST['horaSal'];
            $uv = $_POST['uv'];
            $accion = $_POST['accion'];
            
            $query = "UPDATE ca_profesores SET ca_profesores.id_contrato=$id_contrato, ca_profesores.fecha_contrato=curdate(), ca_profesores.hora_ent='$horaEntrada', ca_profesores.hora_sal='$horaSalida' WHERE ca_profesores.No_Empleado='$No_empleado';";
            if ($accion=="editarUv"){
                $query = $query." INSERT INTO `ca_profesores_uv_variable` (`No_Empleado`, `id_contrato`, `uv`) VALUES ('$No_empleado', '$id_contrato', '$uv');";
            }

        try{
            $result =$db->prepare($query);
            $result->execute();
            echo mensajes("Empleado No.$No_empleado actualizado","verde");

        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }else{
        echo mensajes("Problemas con la conexión","azul");
    }
?>
<script type="text/javascript">
    $("#datosTabla").load('pages/CargaAcademica/GestionProfesores/DatosProfesores.php');
</script>