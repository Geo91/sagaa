<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO

    $idUser = $_SESSION['user_id'];
    $rol = $_SESSION['user_rol'];
    $query1 = "SELECT empleado.No_Empleado, 
                persona.N_identidad,
                CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombre,
                cargo.Cargo,
                ca_tipos_contratos.id_contrato,
                (case WHEN ca_tipos_contratos.uv>0 THEN
                        CONCAT(ca_tipos_contratos.nombre, '/' ,ca_tipos_contratos.uv, 'uv')
                     WHEN ca_profesores_uv_variable.No_Empleado IS NULL THEN
                        ca_tipos_contratos.nombre
                     WHEN ca_tipos_contratos.uv<0 THEN
                        CONCAT(ca_tipos_contratos.nombre, '/' ,ca_profesores_uv_variable.uv, 'uv')
                     WHEN ca_tipos_contratos.uv=0 THEN
                        ca_tipos_contratos.nombre
                END) AS contrato, hora_ent, hora_sal
            FROM ca_profesores LEFT JOIN empleado
                    on empleado.No_Empleado = ca_profesores.No_Empleado
                LEFT join persona 
                    on empleado.N_identidad=persona.N_identidad
                LEFT JOIN empleado_has_cargo 
                    on empleado_has_cargo.No_Empleado=empleado.No_Empleado
                LEFT JOIN cargo
                    on empleado_has_cargo.ID_cargo=cargo.ID_cargo
                LEFT JOIN ca_tipos_contratos
                    on ca_profesores.id_contrato = ca_tipos_contratos.id_contrato
                LEFT JOIN ca_profesores_uv_variable
                    on ca_profesores.No_Empleado=ca_profesores_uv_variable.No_Empleado
            WHERE (cargo.Cargo='Docente' 
                     or cargo.Cargo='Decana'
                     or cargo.Cargo='Jefe de Departamento'
                     or cargo.Cargo='Coordinador de Maestría')
                and empleado_has_cargo.Fecha_salida_cargo is null
                and empleado.estado_empleado=1";

    if($rol == 30){
        $query1 = $query1. " and empleado.Id_departamento in (SELECT empleado.Id_departamento from empleado where empleado.No_Empleado in (SELECT usuario.No_Empleado from usuario where usuario.id_Usuario = $idUser))";
    }

?>
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaAsignaturas" class="table table-bordered table-striped">
            <thead>
                <tr>                                            
                    <th style="text-align:center;background-color:#386D95;color:white;">No Empleado</th>
                    <th style="text-align:center;background-color:#386D95;color:white;display:none">Identidad</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Cargo</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Contrato</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Hora Entrada</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Hora Salida</th>
                <?php if($_SESSION['user_rol']!=30){  ?>
                    <th style="text-align:center;background-color:#386D95;color:white;">Editar</th>
                <?php } ?>
                </tr>
            </thead>
            <tbody>
            <?php
                $result =$db->prepare($query1);
                $result->execute();
                while ($fila = $result->fetch()) {
                    ?>
                    <tr data-id='<?php echo $fila["No_Empleado"]; ?>'  data-contrato='<?php echo $fila["id_contrato"]; ?>'>
                        <td><?php echo $fila["No_Empleado"]; ?></td>
                        <td style = "display:none"><?php echo $fila["N_identidad"]; ?></td>
                        <td><?php echo $fila["nombre"]; ?></td>
                        <td><?php echo $fila["Cargo"]; ?></td>
                        <td><?php echo $fila["contrato"]; ?></td>
                        <td><?php echo $fila["hora_ent"]; ?></td>
                        <td><?php echo $fila["hora_sal"]; ?></td>
                    <?php if($_SESSION['user_rol']!=30){  ?>
                        <td>
                            <center>
                                <button type="button" class="editarContrato<?php echo $auto_increment; ?> btn btn-info glyphicon glyphicon-edit"  title="Editar contrato">
                            </center>
                        </td> 
                    <?php } ?>      
                    </tr>
            <?php } ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div>
<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#tablaAsignaturas')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaAsignaturas').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });

//para editar una asignatura
$(document).on("click", ".editarContrato<?php echo $auto_increment; ?>", function () {
        data={
            No_empleado:$(this).parents("tr").data("id"),
            id_contrato:$(this).parents("tr").data("contrato"),
            Identidad:$(this).parents("tr").find("td").eq(1).html(),
            nombre:$(this).parents("tr").find("td").eq(2).html(),
            Cargo:$(this).parents("tr").find("td").eq(3).html(),
            hEnt:$(this).parents("tr").find("td").eq(5).html(),
            hSal:$(this).parents("tr").find("td").eq(6).html()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: editarContrato,
            timeout: 4000,
            error: problemas
        }); 
        return false;
});
</script>