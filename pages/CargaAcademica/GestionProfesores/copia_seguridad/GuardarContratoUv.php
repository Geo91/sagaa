<?php
    $maindir = "../../../";
    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    if(isset($_POST['No_empleado'])&&
    isset($_POST['id_contrato'])&&
    isset($_POST['uv']) ){
        try{
            $No_empleado = $_POST['No_empleado'];
            $id_contrato = $_POST['id_contrato'];
            $uv = $_POST['uv'];

            if(isset($_SESSION['infoPeriodo']) ){
                $infoPeriodo = $_SESSION['infoPeriodo'];
                $periodoId =$infoPeriodo->getId(); 
                $query = "INSERT INTO `ca_carga_profesores` (`No_Empleado`, `id_periodo`, `cantidad_uv`, `completa`) VALUES ('$No_empleado', '$periodoId', '0', '0');";
                $result =$db->prepare($query);
                $result->execute();
            }
            $query = "INSERT INTO `ca_profesores` (`No_Empleado`, `id_contrato`, `fecha_contrato`) VALUES ('$No_empleado', '$id_contrato', curdate());
                    INSERT INTO `ca_profesores_uv_variable` (`No_Empleado`, `id_contrato`, `uv`) VALUES ('$No_empleado', '$id_contrato', '$uv');";

            $result =$db->prepare($query);
            $result->execute();
            echo mensajes("Contrato agregado al empleado No.$No_empleado correctamente","verde");

        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }else{
        echo mensajes("Problemas con la conexión","azul");
    }
?>
<script type="text/javascript">
    $("#formularioAgregar").load('pages/CargaAcademica/GestionProfesores/AgregarContrato.php');
    $("#datosTabla").load('pages/CargaAcademica/GestionProfesores/DatosProfesores.php');
</script>