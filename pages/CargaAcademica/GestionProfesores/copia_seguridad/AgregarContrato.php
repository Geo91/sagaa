<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir . "conexion/config.inc.php");

    $query = "SELECT empleado.No_Empleado, 
                CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombre
            FROM empleado,persona,cargo,empleado_has_cargo
            WHERE empleado.N_identidad=persona.N_identidad
                and empleado_has_cargo.No_Empleado=empleado.No_Empleado
                and empleado_has_cargo.ID_cargo=cargo.ID_cargo
                and empleado.estado_empleado=1
                and (cargo.Cargo='Docente' 
                     or cargo.Cargo='Decana'
                     or cargo.Cargo='Jefe de Departamento'
		     or cargo.Cargo='Secretario Académico'
                     or cargo.Cargo='Coordinador de Maestría')
                and empleado_has_cargo.Fecha_salida_cargo is null
                and empleado.No_Empleado not in (SELECT ca_profesores.No_Empleado FROM ca_profesores) 
            ORDER BY persona.Primer_nombre";
    $query2 = "SELECT * FROM ca_tipos_contratos";
?>     
<!--************************formulario*********************-->
<form class="form-horizontal" role="form" id="formSeleccionProfesor" name="formSeleccionProfesor">

    <div class="row form-group">
        <label class=" col-sm-2 control-label" >Empleados</label>
        <div class="col-sm-8">                            
            <select class="form-control" id="selectEmpleado" name="selectEmpleado" required>
                <option value="">Seleccione una opción</option>
                <?php
                     $result =$db->prepare($query);
                     $result->execute();
                    while ($fila = $result->fetch()) {
                        ?>
                        <option value='<?php echo $fila["No_Empleado"]; ?>'>
                        <?php 
                            $largo = strlen($fila["No_Empleado"]);
                            $espacio = str_repeat (" ", 10-$largo);
                            echo $fila["nombre"]; ?></option>
                        <?php
                    }
                ?>
            </select>
        </div>
    </div>

    <div class="row form-group">
        <label class=" col-sm-2 control-label" >Contratos</label>
        <div class="col-sm-8">                            
            <select class="form-control" id="selectContratos" name="selectContratos" required>
                <option value="">Seleccione una opcion</option>
                <?php
                    $result =$db->prepare($query2);
                    $result->execute();
                    while ($fila = $result->fetch()) {
                ?> 
                        <option value='<?php echo $fila['id_contrato']."|*".$fila['uv']; ?>'><?php echo $fila["nombre"]; ?></option>
                <?php
                    }
                ?>
            </select>
        </div>
    </div>

    <!--<div class="row form-group">                            
        <label class="control-label col-sm-2">Jornada</label>
        <div class="col-sm-8">
            <select id="horario" name="horario" size="2" required>
                <option value="1">Con Horario</option>
                <option value="0">Sin Horario</option>
            </select>            
        </div> 
    </div>-->

    <div id = "hEntrada" class="row form-group">                            
        <label class="control-label col-sm-2">Hora Entrada</label>
        <div class="col-sm-8">
            <input type="time" id="horaE" name="horaE" required><span class="">
        </div>
    </div>

    <div id = "hSalida" class="row form-group">                            
        <label class="control-label col-sm-2">Hora Salida</label>
        <div class="col-sm-8">
            <input type="time" id="horaS" name="horaS" required><span class="">
        </div>
    </div>

    <div class="row">
        <label class="control-label col-sm-2"></label>
        <div class="col-sm-8">
            <p aling ="right">
                <button type="submit" class="btn btn-primary btn-primary col-sm-offset-10" ><span class=" glyphicon glyphicon-floppy-disk"></span> Agregar contrato</button> 
            </p>          
        </div>                            
    </div>    

</form>
<!--************************formulario*********************-->
<script type="text/javascript">
    
    /*$("#horario").click(function(event) {
        event.preventDefault();
        var valor = $('#horario').val();
        if (valor==1) {
            $('#hEntrada').show('fast');
            $('#hSalida').show('fast');
        }else{
            $('#hEntrada').hide('fast');
            $('#hEntrada').val('08:00:00');
            $('#hSalida').hide('fast');
            $('#hSalida').val('20:00:00');
        }
    });*/

//para cuando se da submit al formulario formAsignatura
    $("#formSeleccionProfesor").submit(function(e) {
        e.preventDefault();
        Contrato = $('#selectContratos').val().split("|*");
        id_contrato = Contrato[0];
        uv = Contrato[1];
        if(uv>-1){
            data={
                No_empleado:$('#selectEmpleado').val(),
                horaEntrada: $("#horaE").val(),
                horaSalida:$("#horaS").val(),
                id_contrato:id_contrato 
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                success: guardarContrato,
                timeout: 4000,
                error: problemas
            }); 
            return false;
        }else{
            data={
                No_empleado:$('#selectEmpleado').val(),
                horaEntrada: $("#horaE").val(),
                horaSalida:$("#horaS").val(),
                id_contrato:id_contrato 
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                success: agregarContrato,
                timeout: 4000,
                error: problemas
            }); 
            return false;
        }
    });
</script>