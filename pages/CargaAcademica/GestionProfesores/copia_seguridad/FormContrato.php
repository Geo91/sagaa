<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir . "conexion/config.inc.php");

    if (isset($_POST['No_empleado'])&&isset($_POST['id_contrato'])) {
        $No_Empleado = $_POST['No_empleado'];
        $id_contrato = $_POST['id_contrato'];

        $query = "SELECT empleado.No_Empleado,
                      CONCAT(persona.Primer_nombre,' ',persona.Segundo_nombre,' ',persona.Primer_apellido,' ',persona.Segundo_apellido) as nombre,
                      cargo.Cargo
                FROM empleado,empleado_has_cargo,cargo,persona
                WHERE empleado.N_identidad=persona.N_identidad
                    and empleado.No_Empleado=empleado_has_cargo.No_Empleado
                    and empleado_has_cargo.ID_cargo=cargo.ID_cargo
                    and empleado_has_cargo.Fecha_salida_cargo is null
                    and empleado.No_Empleado='$No_Empleado'
                    limit 1";
        $query2 = "SELECT * FROM ca_tipos_contratos WHERE ca_tipos_contratos.id_contrato=$id_contrato";

        $result =$db->prepare($query);
        $result->execute();
        while ($fila = $result->fetch()) {
        ?> 

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Especificar la cantidad de UV's para: </h4>
</div>
<div class="modal-body">
    <!--************************formulario*********************-->
    <form class="form-horizontal" role="form" id="formAgregarContrato" name="formAgregarContrato">

        <div class="row form-group" >                    
            <label class=" col-sm-2 control-label" >No Empleado</label>
            <div class="col-sm-8">                            
                <input type="text" class="form-control" id="No_empleadoUV" name="No_empleadoUV" required disabled="true" value="<?php echo $No_Empleado; ?>">      
            </div>                  
        </div>

        <div class="row form-group" >                    
            <label class=" col-sm-2 control-label" >Nombre</label>
            <div class="col-sm-8">                            
                <input type="text" class="form-control" id="nombre" name="nombre" required disabled="true" value="<?php echo $fila['nombre']; ?>">      
            </div>                  
        </div>

        <div class="row form-group" >                    
            <label class=" col-sm-2 control-label" >Cargo</label>
            <div class="col-sm-8">                            
                <input type="text" class="form-control" id="nombre" name="nombre" required disabled="true" value="<?php echo $fila['Cargo']; ?>">      
            </div>                  
        </div>

        <?php 
            }
        ?>

        <div class="row form-group" >                    
            <label class=" col-sm-2 control-label" >Contrato</label>
            <div class="col-sm-8">   
                <?php
                    $result =$db->prepare($query2);
                    $result->execute();
                    while ($fila = $result->fetch()) {
                ?>                          
                    <input type="hidden" class="form-control" id="Contrato" name="Contrato" required value="<?php echo $fila['id_contrato']; ?>">

                    <input type="text" class="form-control" id="ContratoNombre" name="ContratoNombre" required disabled="true" value="<?php echo $fila['nombre']; ?>">   
                <?php
                    }
                ?>  
            </div>                  
        </div>
        
        <div class="row form-group" >                    
            <label class=" col-sm-2 control-label" >Carga de UV's</label>
            <div class="col-sm-2">     
                <input type="number" class="form-control" id="uv" name="uv" min="1" max="30" required value="1">      
            </div>                  
        </div>

        <div class="modal-footer">
            <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button  id="guardarCambios" class="btn btn-primary" ><span class=" glyphicon glyphicon-floppy-disk"></span>Guardar</button>
        </div>   

    </form>
    <!--************************formulario*********************-->
</div>
<?php
    }
?>
<script type="text/javascript" src="pages/CargaAcademica/GestionProfesores/Script.js"></script>