<?php
    $maindir = "../../../";

    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
if($_SESSION['user_rol']!=30){  
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gestión de profesores:</h1>
    </div>
</div> 
<div id="divRespuesta"></div>     
<div class="panel panel-default">
    <div class="panel-heading"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Profesores sin contrato</div>

    <div class="panel-body" >
        <div class="row">
            <div class="col-lg-12">
            <!--************************formulario*********************-->
                <div id="formularioAgregar">
                    <?php require_once("AgregarContrato.php"); ?>
                </div>
            <!--************************formulario*********************-->
            </div>
        </div>
    </div>                                    
</div> 
<?php } ?>
<!--tabla de contratos-->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Profesores con contrato:</h1>
    </div>
</div> 
<div id="divRespuestaEditar">
</div>
<div id="datosTabla">
    <?php require_once("DatosProfesores.php"); ?>
</div>

<!--******************modal de edicion*****************-->
<div class="modal fade" id="modalTemp" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div id="subContenidoTemp">
        </div>
      </div>
    </div>
</div>
<!--******************modal de edicion*****************-->
<script type="text/javascript" src="pages/CargaAcademica/GestionProfesores/Script.js"></script>