<div class="modal-header" style = "background-color:#0FA6C3">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title" style = "color:white">Editar tipo de contrato</h4>
</div>
<?php
    $maindir = "../../../";

    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    include($maindir."funciones/timeout.php");
    require_once($maindir . "conexion/config.inc.php");
    require_once($maindir . "Datos/funciones.php");
    //-------------verificacion si tiene secciones-------------
    $bandera = true;
    if (isset($_POST['No_empleado']) && 
    isset($_POST['id_contrato']) && 
    isset($_POST['nombre']) && 
    isset($_POST['Cargo']) && 
    isset($_POST['Identidad']) && 
    isset($_POST['hEnt']) && 
    isset($_POST['hSal'])) {
        $No_empleado=$_POST['No_empleado'];
        $id_contrato=$_POST['id_contrato'];
        $nombre = $_POST['nombre'];
        $Cargo = $_POST['Cargo'];
        $Identidad = $_POST['Identidad'];
        $entrada = $_POST['hEnt'];
        $salida = $_POST['hSal'];

        if(isset($_SESSION['infoPeriodo']) ){
            try{
                $infoPeriodo = $_SESSION['infoPeriodo'];
                $periodoId =$infoPeriodo->getId();  
                $query = "SELECT * FROM ca_secciones,ca_periodos,ca_cargas
                    where ca_secciones.id_carga=ca_cargas.id_carga
                        and ca_cargas.id_periodo=ca_periodos.id_periodo
                        and ca_periodos.id_periodo=$periodoId
                        and ca_secciones.No_Empleado='$No_empleado'";
                $result =$db->prepare($query);
                $result->execute();
                while ($fila = $result->fetch()) {
                    $bandera = false;
                    break;
                }

            } catch (Exception $e) {
                echo mensajes($e->getMessage(),"rojo");
            }
        }
        if ($bandera){
            $query = "SELECT * FROM ca_tipos_contratos";
            ?>
            <div class="modal-body">
                <form class="form-horizontal" role="form" id="formEditarContrato" name="formEditarContrato">

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >No Empleado</label>
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control" id="No_empleadoEditar" name="No_empleadoEditar" required disabled="true" value="<?php echo $No_empleado; ?>">      
                        </div>                  
                    </div>

                    <div class="row form-group" style ="display:none">
                        <label class=" col-sm-2 control-label" >No Identidad</label>
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control" id="noIdentidad" name="noIdentidad" required disabled="true" value="<?php echo $entrada; ?>">      
                        </div>                  
                    </div>

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Nombre</label>
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control" id="nombreN" name="nombre" required disabled = "true" value="<?php echo $nombre; ?>">      
                        </div>                  
                    </div>

                    <div class="row form-group" >                    
                        <label class=" col-sm-2 control-label" >Cargo</label>
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control" id="nombre" name="nombre" required disabled="true" value="<?php echo $Cargo; ?>">      
                        </div>                  
                    </div>

                    <div class="row form-group">
                        <label class=" col-sm-2 control-label" >Contrato</label>
                        <div class="col-sm-8">                            
                            <select class="form-control" id="selectContratosEditar" name="selectContratosEditar" required>
                                <option value="">Seleccione una opcion</option>
                                <?php
                                    $result =$db->prepare($query);
                                    $result->execute();
                                    while ($fila = $result->fetch()) {
                                ?> 
                                        <option value='<?php echo $fila['id_contrato']."|*".$fila['uv']; ?>'  
                                        <?php if ($fila['id_contrato']==$id_contrato) {
                                            echo " selected";
                                        }?>
                                        ><?php echo $fila["nombre"]; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div> 

                    <div class = "row form-group">
                        <label class="col-sm-2 control-label">Hora Entrada</label>
                        <div class="col-sm-4">
                            <input type = "time" class="form-control" id="hora_Entrada" name="hEntrada" required value="<?php echo $entrada; ?>">
                        </div>
                    </div>

                    <div class = "row form-group">
                        <label class="col-sm-2 control-label">Hora Salida</label>
                        <div class="col-sm-4">
                            <input type="time" class="form-control" id="hora_Salida" name="hSalida" required value="<?php echo $salida; ?>">
                        </div>
                    </div>                   

                    <div class="modal-footer">
                        <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button  id="guardarCambios" class="btn btn-primary" ><span class=" glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                    </div>
                </form>
            </div>

            <div class="modal fade" id="modalTempEdicion" role="dialog">
                <div class="modal-dialog modal-md">
                  <div class="modal-content">
                    <div id="subModalTempEdicion">
                        <div class="modal-header" style = "background-color:#0FA6C3">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title" style = "color:white">Carga de Unidades Valorativas</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" role="form" id="formEditarContratoUv" name="formEditarContratoUv">

                                <div class="row form-group" >                    
                                    <label class=" col-sm-2 control-label" >UV's</label>
                                    <div class="col-sm-2">     
                                        <input type="number" class="form-control" id="uvEditar" name="uvEditar" min="1" max="30" required
                                        value="1">      
                                    </div>                  
                                </div>

                                <div class="modal-footer">
                                    <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button  id="guardarCambios" class="btn btn-primary" ><span class=" glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
            <script type="text/javascript" src="pages/CargaAcademica/GestionProfesores/Script.js"></script>
<?php
        }else{
            echo mensajes("Se encuentra dentro del periodo de registro de cargas.<br>Durante este periodo de tiempo mientras el docente tenga secciones asignadas no está permitido modificar el contrato de este.","azul");
        }
    }
?>