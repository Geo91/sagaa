$(document).ready(function(){   

//para cuando se da submit al formulario de editar
    $("#formEditarContrato").submit(function(e) {
        e.preventDefault();
        Contrato = $('#selectContratosEditar').val().split("|*");
        var salida = $('#hora_Salida').val();
        var entrada = $('#hora_Entrada').val();
        id_contrato = Contrato[0];
        uv = Contrato[1];
        if(uv>-1){
            data={
                No_empleado: $('#No_empleadoEditar').val(),
                horaEnt: entrada,
                horaSal: salida,
                id_contrato: id_contrato,
                uv: uv,
                accion: "editar"
            }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                success: actualizarContrato,
                timeout: 4000,
                error: problemas
            }); 
            return false;
        }else{
            $("#modalTempEdicion").modal('show');
        }
    });

//para cuando se da submit al formulario de agregar contrato
    $("#formAgregarContrato").submit(function(e) {
        e.preventDefault();
        data={
            No_empleado:$('#No_empleadoUV').val(),
            id_contrato:$('#Contrato').val(),
            horaEntrada:$('#horaE').val(),
            horaSalida:$('#horaS').val(),
            uv:$('#uv').val()
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: guardarContratoUv,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });

    $("#formEditarContratoUv").submit(function(e) {
        e.preventDefault();
        Contrato = $('#selectContratosEditar').val().split("|*");
        var salida = $('#hora_Salida').val();
        var entrada = $('#hora_Entrada').val();
        id_contrato = Contrato[0];
        data={
            No_empleado:$('#No_empleadoEditar').val(),
            id_contrato:id_contrato,
            horaEnt:entrada,
            horaSal:salida,
            uv:$('#uvEditar').val(),
            accion:"editarUv"
        }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: actualizarContratoUv,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });
});


function inicioEnvio(){
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function agregarContrato(){
    $("#subContenidoTemp").load('pages/CargaAcademica/GestionProfesores/FormContrato.php',data);
    $("#modalTemp").modal('show');
}

function editarContrato(){
    $("#subContenidoTemp").load('pages/CargaAcademica/GestionProfesores/FormEditar.php',data);
    $("#modalTemp").modal('show');
}

function actualizarContrato(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/GestionProfesores/ActualizarContrato.php',data);
    $("#modalTemp").modal('hide');
}

function actualizarContratoUv(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/GestionProfesores/ActualizarContrato.php',data);
    $("#modalTempEdicion").modal('hide');
    $("#modalTemp").modal('hide');
}

function guardarContrato(){
    $("#divRespuesta").load('pages/CargaAcademica/GestionProfesores/GuardarContrato.php',data);
}

function guardarContratoUv(){
    $("#divRespuesta").load('pages/CargaAcademica/GestionProfesores/GuardarContratoUv.php',data);
    $("#modalTemp").modal('hide');
}