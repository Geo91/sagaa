<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query1 = "SELECT * FROM `ca_departamentos_carreras`";
?>
<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaTiposDiscapacidades" class="table table-bordered table-striped">
            <thead>
                <tr>                                            
                    <th style="text-align:center;background-color:#386D95;color:white;">ID</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Nombre</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Editar</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Deshabilitar</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $result =$db->prepare($query1);
                $result->execute();
                while ($fila = $result->fetch()) {
                    ?>
                    <tr data-id='<?php echo $fila["id_departamento"]; ?>'>
                        <td><?php echo $fila["id_departamento"]; ?></td>
                        <td><?php echo $fila["nombre"]; ?></td>
                        <td>
                            <center>
                                <button type="button" class="editarDepartamento<?php echo $auto_increment; ?> btn btn-primary glyphicon glyphicon-edit"  title="Editar Tipo discapacidad">
                            </center>
                        </td>       
                        <td>
                            <center>
                                <button type="button" class="eliminarDepartamento<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-remove"  title="Deshabilitar Asignatura">
                            </center>
                        </td>     
                    </tr>
            <?php } ?>
            </tbody>

        </table>
    </div><!-- /.box-body -->
</div>
<script type="text/javascript">   
//opciones para buscador en la tabla tablaAsignaturas
    $('#tablaTiposDiscapacidades')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaTiposDiscapacidades').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });
    
$(document).on("click", ".editarDepartamento<?php echo $auto_increment; ?>", function () {
    codigo = $(this).parents("tr").data("id");
    Nombre = $(this).parents("tr").find("td").eq(1).html();

    $("#modalCodigo").val(codigo);
    $("#modalNombre").val(Nombre);

    $("#modalEditar").modal('show');
});

$(document).on("click", ".eliminarDepartamento<?php echo $auto_increment; ?>", function () {
    if (confirm("¿Está seguro de que desea eliminar este departamento?")){
        codigo = $(this).parents("tr").data("id");
        data={
                    Codigo:codigo
            }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: eliminarDepartamento,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    }
});

</script>