<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Codigo'])){
        
        $Codigo = $_POST['Codigo'];

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query0 = "SELECT ca_departamentos_carreras.id_departamento,
                    COUNT(ca_asignaturas.id_asignatura) as asignaturas,
                    COUNT(ca_cargas.id_carga) as cargas,
                    COUNT(ca_jefes_departamentos.id_Usuario) as jefes
                FROM ca_departamentos_carreras
                    LEFT JOIN ca_asignaturas
                        on ca_departamentos_carreras.id_departamento=ca_asignaturas.id_departamento
                    LEFT JOIN ca_cargas
                        on ca_departamentos_carreras.id_departamento=ca_cargas.id_departamento
                    LEFT JOIN ca_jefes_departamentos
                        on ca_departamentos_carreras.id_departamento=ca_jefes_departamentos.id_departamento
                WHERE ca_departamentos_carreras.id_departamento=$Codigo
                GROUP BY ca_departamentos_carreras.id_departamento";
        $query = "DELETE FROM ca_departamentos_carreras WHERE ca_departamentos_carreras.id_departamento=$Codigo";

        try{
            $result =$db->prepare($query0);
            $result->execute();
            $Asignaturas=false;
            $Cargas=false;
            $Jefes=false;
            while ($fila = $result->fetch()) {
                if($fila['asignaturas']>0){
                    $Asignaturas=true;
                }
                if($fila['cargas']>0){
                    $Cargas=true;
                }
                if($fila['jefes']>0){
                    $Jefes=true;
                }
                break;
            }

            if($Asignaturas || $Cargas || $Jefes){
                $mensaje = "El departamento no se puede borrar ya que este se encuentra vinculado a otros registros:<br>";
                if($Asignaturas){
                    $mensaje=$mensaje."Hay asignaturas vinculadas a este departamento.<br>";
                }
                if($Cargas){
                    $mensaje=$mensaje."Hay cargas académicas vinculadas a este departamento.<br>";
                }
                if($Jefes){
                    $mensaje=$mensaje."Hay usuarios Jefes de departamento vinculadas a este departamento.<br>";
                }
                $color = "azul";
            }
            else{
                $result =$db->prepare($query);
                $result->execute();
                $mensaje = "Departamento ID.$Codigo eliminado";
                $color = "verde";
                ?>
                    <script type="text/javascript">
                        $("#datosTabla").load('pages/CargaAcademica/Departamentos/DatosDepartamento.php');
                    </script>
                <?php
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }else{
        echo mensajes("Ha ocurrido un error del servidor...","verde");
    }
?>