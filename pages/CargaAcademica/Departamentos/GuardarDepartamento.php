<?php
    $maindir = "../../../";

    require_once ("../clases.php");
    require_once($maindir."funciones/check_session.php");
    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Nombre']) ){
        
        $Nombre = $_POST['Nombre'];

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query0 = "SELECT * FROM `ca_departamentos_carreras` WHERE `nombre`='$Nombre'";
        $query = "INSERT INTO `ca_departamentos_carreras` (`id_departamento`, `nombre`) VALUES (NULL, '$Nombre');";

        try{
            $result =$db->prepare($query0);
            $result->execute();
            
            $bandera = false;
            while ($fila = $result->fetch()) {
                $bandera = true;
            }
            if($bandera){
                $mensaje = "El departamento `$Nombre` ya existe.";
                $color = "azul";
            }
            else{
                $result =$db->prepare($query);
                $result->execute();

                if(isset($_SESSION['infoPeriodo']) ){
                    $infoPeriodo = $_SESSION['infoPeriodo'];
                    $periodoId =$infoPeriodo->getId(); 
                    $query = "INSERT INTO `ca_cargas` (`id_carga`, `id_periodo`, `aprobado`, `id_departamento`) VALUES (NULL, $periodoId, '0', ( SELECT MAX(ca_departamentos_carreras.id_departamento) FROM `ca_departamentos_carreras` ));";
                    $result =$db->prepare($query);
                    $result->execute();
                }
                $mensaje = "departamento `$Nombre` agregado";
                $color = "verde";
                ?>
                    <script type="text/javascript">
                        $("#datosTabla").load('pages/CargaAcademica/Departamentos/DatosDepartamento.php');
                    </script>
                <?php
            }
            echo mensajes($mensaje,$color);
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }
?>