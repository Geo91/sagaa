$(document).ready(function(){   

//para cuando se da submit al formulario formAsignatura
    $("#formNuevoDepartamento").submit(function(e) {
        e.preventDefault();
        data={
                    Nombre:$('#nombreDepartamento').val()
            }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            beforeSend: inicioEnvio,
            success: agregarDepartamento,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });


//para cuando se da submit al formulario formAsignatura
    $("#formEditar").submit(function(e) {
        e.preventDefault();
        data={
                    Codigo:$('#modalCodigo').val(),
                    Nombre:$('#modalNombre').val()
                }
        $.ajax({
            async: true,
            type: "POST",
            dataType: "html",
            contentType: "application/x-www-form-urlencoded",
            success: editarDepartamento,
            timeout: 4000,
            error: problemas
        }); 
        return false;
    });

});



function inicioEnvio(){
    $("#divRespuesta").empty();
    $("#divRespuesta").text('Cargando...');
}

function problemas(){
    $("#divRespuesta").text('Problemas en el servidor.');
}

function agregarDepartamento(){
    $("#divRespuesta").load('pages/CargaAcademica/Departamentos/GuardarDepartamento.php',data);
    $('#formNuevoDepartamento').trigger("reset");
}

function editarDepartamento(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/Departamentos/EditarDepartamento.php',data);
    $("#modalEditar").modal('hide');
}

function eliminarDepartamento(){
    $("#divRespuestaEditar").load('pages/CargaAcademica/Departamentos/EliminarDepartamento.php',data);
}
