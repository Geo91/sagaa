<?php
    $maindir = "../../../";

    require_once($maindir."Datos/funciones.php");
    require_once($maindir."conexion/config.inc.php");

    if (isset($_POST['Codigo']) && 
        isset($_POST['Nombre']) ){
        
        $Codigo = $_POST['Codigo'];
        $Nombre = $_POST['Nombre'];

    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
        $query = "UPDATE ca_departamentos_carreras 
                set ca_departamentos_carreras.nombre='$Nombre' 
                WHERE ca_departamentos_carreras.id_departamento=$Codigo";
        try {
            $result =$db->prepare($query);
            $result->execute();
            echo mensajes("Se ha actualizado el departamento `$Nombre`","verde");
        } catch (Exception $e) {
            echo mensajes($e->getMessage(),"rojo");
        }
    }else{
        echo mensajes("Ha ocurrido un error del servidor...","verde");
    }
?>
<script type="text/javascript">
    $("#datosTabla").load('pages/CargaAcademica/Departamentos/DatosDepartamento.php');
</script>