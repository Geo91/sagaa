<html lang="es">

<?php

    $maindir = "../../";

  if(isset($_GET['contenido']))
    {
      $contenido = $_GET['contenido'];
    }
  else
    {
      $contenido = 'carga_academica';
    }
  require_once("principal.php"); 
  require_once($maindir."funciones/check_session.php");

  include($maindir."funciones/timeout.php");

  require_once($maindir."pages/navbar.php");
  
?>
 
    <head>    
    <meta charset="utf-8">
    <script type="text/javascript" src = "pages/CargaAcademica/menu.js" />
    </head>
    <body>

      <div class="container-fluid">
        <div class = "row">
          <div class = "col-sm-2" >
            <ul class="list-unstyled">

              <!--***********************INICIO del admon**********************-->
              <!--*******************inicoi del profesor********************-->
              <?php if($_SESSION['user_rol']!=20){ ?>

                <li class="nav-header active"> <a id="inicioCargaAcademica" href="#"><i class="glyphicon glyphicon-home"></i> Inicio Carga Académica</a></li>
              
              <?php }else{ ?>

                <li class="nav-header active"><a id="lnkInicioDocente" href="#"><i class="glyphicon glyphicon-home"></i> Inicio de Carga Académica</a></li>

              <?php } ?>
              
              <hr>



              <?php if($_SESSION['user_rol']==50 ||
                      $_SESSION['user_rol']==100){ ?>
              <!--*******************inicoi del ADMON********************-->
                <li class="nav-header active"><a id="lnkCargasAcademicas" href="#"><i class="glyphicon glyphicon-list-alt"></i> Registro de la Carga Académica</a></li>
              <?php } ?>


              <?php if($_SESSION['user_rol']==30){ ?>
                <!--*******************inicoi del Jefe********************-->
                <li class="nav-header active"><a id="lnkAgregarCarga" href="#"><i class="glyphicon glyphicon-list-alt"></i> Registro de la Carga Académica</a></li>
              <?php } ?>

              <br>

              <!--****************************PROGRAMACION**********************-->
          <?php if($_SESSION['user_rol']!=20){ ?>
              <li class="nav-header">
                  <a id="Flecha1" href="#" data-toggle="collapse" data-target="#desplegableProgram"><h5><i class="glyphicon glyphicon-calendar"></i> Programación Académica<i id="SubFlecha1"  class="glyphicon glyphicon-chevron-right"></i></h5></a>
                    <ul class="list-unstyled collapse" id="desplegableProgram">
                      <!--      opciones       -->
                   

                <?php if($_SESSION['user_rol']==50 ||
                    $_SESSION['user_rol']==100){ ?>
                      <li class="nav-header active"><a id="lnkProgramacion" href="#"><i class="glyphicon glyphicon-plus"></i> Programar Registro de Cargas</a></li>
                <?php } ?>

                      <li class="nav-header active"><a id="lnkTodosPeriodos" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Historial de Programaciones</a></li>


                    </ul>
              </li>
            <?php } ?>


              <!--****************************PROFESORES**********************-->
          <?php if($_SESSION['user_rol']!=20){ ?>
              <li class="nav-header">
                  <a id="Flecha2" href="#" data-toggle="collapse" data-target="#desplegableProfe"><h5><i class="glyphicon glyphicon-user"></i> Gestión de Docentes<i id="SubFlecha2" class="glyphicon glyphicon-chevron-right"></i></h5></a>
                    <ul class="list-unstyled collapse" id="desplegableProfe">
                      <!--      opciones       -->
                   
                      <li class="nav-header active"><a id="lnkProfesores" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Contratos de Docentes</a></li>

                  <?php if($_SESSION['user_rol']==100){ ?>
                      <li class="nav-header active"><a id="lnkJefesDepartamento" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Jefes de Departamento</a></li>
                  <?php } ?>

                      <li class="nav-header active"><a id="lnkProfesoresDiscapacidades" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Docenctes discapacitados</a></li>


                    </ul>
              </li>
            <?php } ?>


              <!--****************************PROYECTOS**********************-->
              <li class="nav-header">
                  <a id="Flecha3" href="#" data-toggle="collapse" data-target="#desplegableProyecto"><h5><i class="glyphicon glyphicon-pushpin"></i> Proyectos y Actividades<i id="SubFlecha3" class="glyphicon glyphicon-chevron-right"></i></h5></a>
                    <ul class="list-unstyled collapse" id="desplegableProyecto">
                      <!--      opciones       -->

                  <?php if($_SESSION['user_rol']==50 ||
                      $_SESSION['user_rol']==100){ ?>
                      <li class="nav-header active"><a id="lnkNuevoProyecto" href="#"><i class="glyphicon glyphicon-plus"></i> Nuevo proyecto</a></li>
                  <?php } ?>

                  <?php if($_SESSION['user_rol']!=20){ ?>
                      <li class="nav-header active"><a id="lnkTodosProyectos" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Todos los proyectos</a></li>
                      <li class="nav-header active"><a id="lnkActividades" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Nueva Actividad</a></li>
                      <li class="nav-header active"><a id="lnkEmpActividades" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Actividades por Empleado</a></li>
                      <li class="nav-header active"><a id="lnkTActividades" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Todas los actividades</a></li>
                  <?php }else{ ?>
                      <li class="nav-header active"><a id="lnkProyProf" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Todos los proyectos</a></li>
                      <li class="nav-header active"><a id="lnkTodosProyectos" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Todas los actividades</a></li>
                  <?php } ?>

                    </ul>
              </li>


              <!--****************************REPORTES**********************-->
            <?php if($_SESSION['user_rol']==50 ||
              $_SESSION['user_rol']==100 || $_SESSION['user_rol']==45){ ?>
              <li class="nav-header">
                  <a id="Flecha5" href="#" data-toggle="collapse" data-target="#desplegableReport"><h5><i class="glyphicon glyphicon-stats"></i> Reportes<i id="SubFlecha5" class="glyphicon glyphicon-chevron-right"></i></h5></a>
                    <ul class="list-unstyled collapse" id="desplegableReport">
                      <!--      opciones       -->

                      <li class="nav-header active"><a id="lnkReporteCargas" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Cargas Académicas</a></li>

                      <li class="nav-header active"><a id="lnkRepoContrato" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Docentes por Contratos</a></li>

                      <li class="nav-header active"><a id="lnkRepoProfProy" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Docentes con Proyectos</a></li>

                      <li class="nav-header active"><a id="lnkReporteAulas" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Distribución de Aulas</a></li>

                      <li class="nav-header active"><a id="lnkReportePorHora" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Profesores por Hora</a></li>

                      <li class="nav-header active"><a id="lnkReporteIndiv" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Carga Individualizada</a></li>

                    </ul>
              </li>
            <?php } ?>


              <!--****************************CLASES**********************-->
              <!--****************************MANTENIMIENTO**********************-->
          <?php if($_SESSION['user_rol']==100){ ?>
              <li class="nav-header">
                  <a id="Flecha4" href="#" data-toggle="collapse" data-target="#desplegableClases"><h5><i class="glyphicon glyphicon-list"></i> Gestión de Asignaturas<i id="SubFlecha4" class="glyphicon glyphicon-chevron-right"></i></h5></a>
                    <ul class="list-unstyled collapse" id="desplegableClases">
                      <!--      opciones       -->
                      <li class="nav-header active"><a id="lnkAgregarAsignaturas" href="#"><i class="glyphicon glyphicon-book"></i> Asignaturas</a></li>

                      <li class="nav-header active"><a id="lnkDepartamentos" href="#"><i class="glyphicon glyphicon-arrow-right"></i> Departamentos</a></li>

                    </ul>
              </li>

              <li class="nav-header">
                  <a id="Flecha6" href="#" data-toggle="collapse" data-target="#desplegableMant"><h5><i class="glyphicon glyphicon-cog"></i> Mantenimiento<i id="SubFlecha6" class="glyphicon glyphicon-chevron-right"></i></h5></a>
                    <ul class="list-unstyled collapse" id="desplegableMant">
                      <!--      opciones       -->

						<li class="nav-header active"><a id="lnkEdificios" href="#"><i class="glyphicon glyphicon-tower"></i> Gestión de Edificios</a></li>

						<li class="nav-header active"><a id="lnkTiposContrato" href="#"><i class="glyphicon glyphicon-list"></i> Tipos de Contrato</a></li>

						<li class="nav-header active"><a id="lnkTiposProyectos" href="#"><i class="glyphicon glyphicon-pushpin"></i> Tipos de Proyecto</a></li>

						<li class="nav-header active"><a id="lnkTiposDiscapacidades" href="#"><i class="glyphicon glyphicon-ok-circle"></i> Discapacidades</a></li>

						<li class="nav-header active"><a id="lnkTiposAcond" href="#"><i class="glyphicon glyphicon-film"></i> Tipos de acondicionamiento</a></li>


                    </ul>
              </li>
          <?php } ?>


            </ul>
          </div>
            <div class="col-sm-9">
                <div id="contenedor" class="content-panel">
                </div>
            </div>
      </div> 
    </div>
    </body>
</html>
<script type="text/javascript">
<?php if($_SESSION['user_rol']==20){ 
    echo "$('#contenedor').load('pages/CargaAcademica/CargaDocente.php');";
 }else{
    echo "$('#contenedor').load('pages/CargaAcademica/Alertas/Alertas.php');";
  } ?>
</script>