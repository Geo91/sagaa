
//INSERTAR NUEVO PLAN ESTRATEGICO


$(document).ready(function () {

    $("#formNuevoPlan").submit(function (e) {
        e.preventDefault();
              inicio = document.getElementById('inicio').value;
              fin = document.getElementById('fin').value;

              if(inicio >= fin ){
                  alert("El año de inicio debe ser menor que el año de fin!");

              }else
              {
                ingreso = document.getElementById('fecha_ingreso').value;
                modificacion = document.getElementById('fecha_modificacion').value;
                terminacion = document.getElementById('fecha_terminacion').value;

                if(ingreso >= modificacion || ingreso >= terminacion || modificacion >= terminacion){
                    alert("Fechas Erroneas");
                }else{

                data = { inicio: $("#inicio").val(),
                         fin: $("#fin").val(),
                         fecha_ingreso : $('#fecha_ingreso').val(),
                         fecha_modificacion : $('#fecha_modificacion').val(),
                         fecha_terminacion : $('#fecha_terminacion').val()
                  };

              $.ajax({
                  async: true,
                  type: "POST",
                  dataType: "html",
                  contentType: "application/x-www-form-urlencoded",
                  //url: "Datos/pe_insertar_PlanEstrategico.php",
                  //beforeSend: inicioEnvio,
                  success: llegadaGuardar,
                  timeout: 10000,
                  error: problemas
              });

              limpiarCampos();

              return false;
            }
          }
    });
    //$("#myModal").modal('hide');
});
//fin agregar plan Estrategico


//fin editar plan Estrategico

function inicioEnvio()
{

    var x = $("#contenedor");

    //x.html('Cargando...');
}


function problemas()
{
    $("#contenedor").text('Problemas en el servidor.');
}

function llegadaGuardar()
{
    $("#respuesta").load('Datos/pe_insertar_PlanEstrategico.php', data);
    $("#myModal").modal("hide");
    $("#tabla_planes").load('pages/cargarTablaPlanesEstrategicos.php');
  /*$("#contenedor").empty();
  $("#contenedor").load("pages/planEstrategico.php");*/
}

function limpiarCampos() {
    $("#inicio").val('');
    $("#fin").val('');
    $("#fecha_ingreso").val('');
    $("#fecha_modificacion").val('');
    $("#fecha_terminacion").val('');
}
