
  //Editar plan Estrategico
  var id;
  $(document).on("click",".Editar",function () {

         id = $(this).parents("tr").data("id");
         ini = $(this).parents("tr").data("ini");
         fin = $(this).parents("tr").data("fin");
         ingr = $(this).parents("tr").data("ingr");
         mod = $(this).parents("tr").data("mod");
         ter = $(this).parents("tr").data("ter");

         $("#editarInicio").val(ini);
         $("#editarFin").val(fin);
         $("#dpIngreso").val(ingr);
         $("#dpModificacion").val(mod);
         $("#dpTerminacion").val(ter);
         //alert(id);

  });

  $("#formEditarPlan").submit(function(e) {
      e.preventDefault();
    //  $("#modalEditar").modal('hide');
      data4 = {
          ide: id,
          inicio: $("#editarInicio").val(),
          fin: $("#editarFin").val(),
          dpIngreso: $("#dpIngreso").val(),
          dpModificacion: $("#dpModificacion").val(),
          dpTerminacion: $("#dpTerminacion").val()
      };

      $.ajax({
          async: true,
          type: "POST",
          dataType: "html",
          //contentType: "application/x-www-form-urlencoded",
          //url: "Datos/insertarArea.php",
          //beforeSend: inicioEnvio,
          success: llegadaEditarPlan,
          timeout: 10000,
          error: problemas
      });

      //limpiarCamposArea();
      return false;
  });

//fin editar plan Estrategico


  function llegadaEditarPlan()
  {
      $("#respuesta").load('Datos/editarPlanEstrategico.php', data4);
      $("#modalEditar").modal("hide");
      $("#tabla_planes").load('pages/cargarTablaPlanesEstrategicos.php');
  }

  function problemas()
  {
      $("#contenedor").text('Problemas en el servidor.');
  }
