       /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

	var y;
	y=$(document);
	y.ready(inicio);

	function inicio()
	{
		var x;
		x=$("#solicitud");
		x.click(Solicitud);
		
		var x1;
		x1=$("#motivos");
		x1.click(Motivos);
	  
	  
		var x2;
		x2=$("#edificios");
		x2.click(Edificios);
		
		var x4;
		x4=$("#revision");
		x4.click(Revision);
		
		var x5;
		x5=$("#reportetotal");
		x5.click(ReporteDepto);
		
		var x6;
		x6=$("#reportetrimestral");
		x6.click(ReporteTr);
		
		var x7;
		x7=$("#solicitude");
		x7.click(Solicitud_empleado);
		
		var x8;
		x8=$("#estadistica");
		x8.click(Esta);
                
        var x9;
		x9=$("#nE");
		x9.click(nE);

		var x10;
		x10=$("#TipoDePermisos");
		x10.click(TipoPermisos);

		var x11;
		x11=$("#reportesgraficos");
		x11.click(reporteGraficos);

		var x12;
		x12=$("#reportedepto");
		x12.click(reporteDepto);

		var x13;
		x13=$('#reportemotivos');
		x13.click(reporteMotivos);

		var x14;
		x14=$('#reportetipos');
		x14.click(reporteTipos);

		var x15;
		x15=$('#reportepersonal');
		x15.click(reportePersonal);

		var x16;
		x16=$('#asistencia');
		x16.click(llegadaAsistencia);

		var x17;
		x17=$('#vacaciones');
		x17.click(llegadaVacaciones);

		var x18;
		x18=$('#asignar');
		x18.click(llegadaAsignar);

		//Justificaciones del informe de inasistencias
		var x19;
		x19=$('#informe');
		x19.click(llegadaInforme);
		var x20;
		x20=$('#justificar');
		x20.click(llegadaJustificar);
		var x21;
		x21=$('#revisarjust');
		x21.click(llegadaRevisionJ);

	}

	function reportePersonal(){
			$.ajax({
			async:true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url:"pages/permisos/reporteria/reportePersonal/reportePersonal.php",      
			beforeSend:inicioEnvio,
			success:llegadaReportePersonal,
			timeout:4000,
			error:function(result){  
            alert('ERROR ' + result.status + ' ' + result.statusText);  
          }
		}); 
		return false;
	}

		function reporteTipos(){
			$.ajax({
			async:true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url:"pages/permisos/reporteria/reporteTipos/reporteTipo.php",      
			beforeSend:inicioEnvio,
			success:llegadaReporteTipo,
			timeout:4000,
			error:function(result){  
            alert('ERROR ' + result.status + ' ' + result.statusText);  
          }
		}); 
		return false;
	}

	function reporteMotivos(){
			$.ajax({
			async:true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url:"pages/permisos/reporteria/reporteMotivos/reporteMotivos.php",      
			beforeSend:inicioEnvio,
			success:llegadaReporteMotivo,
			timeout:4000,
			error:function(result){  
            alert('ERROR ' + result.status + ' ' + result.statusText);  
          }
		}); 
		return false;
	}

	function reporteDepto()
	{
		$.ajax({
			async:true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url:"pages/permisos/reporteria/reporteDepartamento/reporteDepartamento.php",      
			beforeSend:inicioEnvio,
			success:llegadaReporteDepto,
			timeout:4000,
			error:function(result){  
            alert('ERROR ' + result.status + ' ' + result.statusText);  
          }
		}); 
		return false;
	}

	function reporteGraficos()
	{
		$.ajax({
			async:true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url:"pages/permisos/reporteria/graficos/reporteGraficos.php",      
			beforeSend:inicioEnvio,
			success:llegadaReportesGraficos,
			timeout:4000,
			error:function(result){  
            alert('ERROR ' + result.status + ' ' + result.statusText);  
          }
		}); 
		return false;
	}


	function Esta()
	{
		$.ajax({
			async:true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url:"pages/permisos/estadistica/estadistica.php",    
			// url:"../estadistica.php",  
			beforeSend:inicioEnvio,
			success:llegadaEsta,
			timeout:4000,
			error:function(result){  
            alert('ERROR ' + result.status + ' ' + result.statusText);  
          }
		}); 
		return false;
	}
	
	function llegadaEsta()
	{
		$("#contenedor").load('pages/permisos/estadistica/estadistica.php');
		 //$("#contenedor").load('../permisos/estadistica.php');
	}
	function Solicitud()
	{
		$.ajax({
			async:true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url:"pages/permisos/solicitudpersonal/SolicitudPermiso.php",    
			// url:"../solicitudes.php",  
			beforeSend:inicioEnvio,
			success:llegadaSolic,
			timeout:4000,
			error:function(result){  
            alert('ERROR ' + result.status + ' ' + result.statusText);  
          }  
		}); 
		return false;
	}
	
	function Solicitud_empleado()
	{
		$.ajax({
			async:true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url:"pages/permisos/solicitudempleado/SolicitudPermisoEmpleado.php",      
			beforeSend:inicioEnvio,
			success:llegadaCreacion,
			timeout:4000,
			error:function(result){  
            alert('ERROR ' + result.status + ' ' + result.statusText);  
          }
		}); 
		return false;
	}

	function Motivos()
	{
		$.ajax({
			async:true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url:"pages/permisos/motivo/motivo.php",    
			// url:"../Motivos.php",  
			beforeSend:inicioEnvio,
			success:llegadaMotivos,
			timeout:4000,
			error:function(result){  
            alert('ERROR ' + result.status + ' ' + result.statusText);  
          }
		}); 
		return false;
	}

	function Edificios()
	{
		$.ajax({
			async:true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url:"pages/permisos/edificio/Edificios.php",    
			// url:"../Edificios.php",  
			beforeSend:inicioEnvio,
			success:llegadaEdificios,
			timeout:4000,
			error:function(result){  
            alert('ERROR ' + result.status + ' ' + result.statusText);  
          }
		}); 
		return false;
	}
	
	function Revision()
	{
		$.ajax({
			async:true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url:"pages/permisos/revision/RevisarSolicitud.php",    
			// url:"../Revision.php",  
			beforeSend:inicioEnvio,
			success:llegadaRevision,
			timeout:4000,
			error:function(result){  
            alert('ERROR ' + result.status + ' ' + result.statusText);  
          }
		}); 
		return true;
	}
        
    function nE()
	{
		$.ajax({
			async:true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url:"pages/permisos/revision/RevisionEmpleado/revisionEmpleado.php",    
			// url:"../Revision.php",  
			beforeSend:inicioEnvio,
			success:llegadanE,
			timeout:4000,
			error:function(result){  
            alert('ERROR ' + result.status + ' ' + result.statusText);  
          }
		}); 
		return true;
	}
	
	function ReporteDepto()
	{
		$.ajax({
			async:true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url:"pages/permisos/reporteria/reporteMensual/reporteMensual.php",    
			// url:"../ReporteTotal.php",  
			beforeSend:inicioEnvio,
			success:llegadaReporteTotal,
			timeout:4000,
			error:function(result){  
            alert('ERROR ' + result.status + ' ' + result.statusText);  
          }
		}); 
		return false;
	}
	function ReporteTr()
	{
		$.ajax({
			async:true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url:"pages/permisos/consultaempleado/reporteEmpleado.php",    
			// url:"../reporteTrimestral.php",  
			beforeSend:inicioEnvio,
			success:llegadaReporteTrimestral,
			timeout:4000,
			error:function(result){  
            alert('ERROR ' + result.status + ' ' + result.statusText);  
          }
		}); 
		return false;
	}
	function CrearUsuario()
	{
		$.ajax({
			async:true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url:"pages/permisos/solicitudempleado/Nuevo_usuario.php",      
			beforeSend:inicioEnvio,
			success:llegadaCreacion,
			timeout:4000,
			error:function(result){  
            alert('ERROR ' + result.status + ' ' + result.statusText);  
          }
		}); 
		return false;
	}
	function TipoPermisos()
	{
		$.ajax({
			async:true,
			type: "POST",
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			url:"pages/permisos/TipoDePermisos/TiposDePermisos.php",    
			// url:"../Motivos.php",  
			beforeSend:inicioEnvio,
			success:llegadaTipoPermiso,
			timeout:4000,
			error:function(result){  
            alert('ERROR ' + result.status + ' ' + result.statusText);  
          }
		}); 
		return false;
	}

	function inicioEnvio()
	{
		var x=$("#contenedor");
		x.html('Cargando...');
	}
	
	function llegadaSolic()
	{
		$("#contenedor").load('pages/permisos/solicitudpersonal/SolicitudPermiso.php');
		 //$("#contenedor").load('../permisos/solicitudes.php');
	}

	function llegadaMotivos()
	{
		$("#contenedor").load('pages/permisos/motivo/motivo.php');
		 //$("#contenedor").load('../permisos/motivo.php');
	}

	function llegadaEdificios()
	{
		$("#contenedor").load('pages/permisos/edificio/Edificios.php');
		 //$("#contenedor").load('../permisos/Edificios.php');
	}

	function llegadaUnidad()
	{
		$("#contenedor").load('pages/permisos/Unidad_Academica.php');
		 //$("#contenedor").load('../permisos/Unidad_Academica.php');
	}

	function llegadaRevision()
	{
		$("#contenedor").load('pages/permisos/revision/RevisarSolicitud.php');
		 //$("#contenedor").load('../permisos/Revision.php');
	}

    function llegadanE()
	{
		$("#contenedor").load('pages/permisos/revision/RevisionEmpleado/revisionEmpleado.php');
		 //$("#contenedor").load('../permisos/Revision.php');
	}

	function llegadaReporteTotal()
	{
		$("#contenedor").load('pages/permisos/reporteria/reporteMensual/reporteMensual.php');
		 //$("#contenedor").load('../permisos/ReporteTotal.php');
	}

	function llegadaReporteTrimestral()
	{
		$("#contenedor").load('pages/permisos/consultaempleado/reporteEmpleado.php');
		 //$("#contenedor").load('../permisos/reporteTrimestral.php');
	}

	function llegadaCreacion(){
		$("#contenedor").load('pages/permisos/solicitudempleado/SolicitudPermisoEmpleado.php');
	}

	function problemas()
	{
		$("#contenedor").text();
	}

	function llegadaTipoPermiso(){
		$("#contenedor").load('pages/permisos/TipoDePermisos/TiposDePermisos.php');
	}

	function llegadaReportesGraficos(){
		$("#contenedor").load('pages/permisos/reporteria/graficos/reporteGraficos.php');
	}

	function llegadaReporteDepto(){
		$("#contenedor").load('pages/permisos/reporteria/reporteDepartamento/reporteDepartamento.php');
	}

	function llegadaReporteMotivo(){
		$("#contenedor").load('pages/permisos/reporteria/reporteMotivos/reporteMotivos.php');
	}

	function llegadaReporteTipo(){
		$("#contenedor").load('pages/permisos/reporteria/reporteTipos/reporteTipo.php');
	}

	function llegadaReportePersonal(){
		$("#contenedor").load('pages/permisos/reporteria/reportePersonal/reportePersonal.php');
	}

	function llegadaAsistencia(){
		$("#contenedor").load('pages/permisos/revision/asistenciaUsuarios.php');
	}

	function llegadaVacaciones(){
		$("#contenedor").load('pages/permisos/vacaciones/vacaciones.php');
	}

	function llegadaAsignar(){
		$("#contenedor").load('pages/permisos/vacaciones/asignarVacaciones.php');
	}

	function llegadaInforme(){
		$("#contenedor").load('pages/permisos/justificaciones/cargarDoc.php');
	}

	function llegadaJustificar(){
		$("#contenedor").load('pages/permisos/justificaciones/justificaciones.php');
	}

	function llegadaRevisionJ(){
		$("#contenedor").load('pages/permisos/justificaciones/revisionJustificaciones.php');
	}

