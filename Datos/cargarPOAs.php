<?php
$maindir = "../";
include '../Datos/conexion.php';
require_once($maindir."conexion/config.inc.php");

$query = $db->prepare("SELECT * FROM poa  ORDER BY fecha_Fin");
$query->execute();
?>
<!DOCTYPE html>
<html lang="en">


<body>

    <div class="box-body table-responsive">
        <table id="tabla_prioridad" class='table table-bordered table-striped'>
            <thead>
                <tr>   
                    <th style="text-align:center;background-color:#386D95;color:white;">Id</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">T&iacute;tulo</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Del</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Al</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Ver Objetivos</th>
		    <th style="text-align:center;background-color:#386D95;color:white;">Editar POA</th>
		    <th style="text-align:center;background-color:#386D95;color:white;">Eliminar POA</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php
                while ($row = $query->fetch()) {
                    $id = $row['id_Poa'];
                    ?>
                    <tr>
                        <td><?php echo $row['id_Poa'] ?></td>
                        <td><div class="text" id="titulo-<?php echo $id ?>"><?php echo utf8_encode($row['nombre']); ?></div></td>
                        <td><div class="text" id="del-<?php echo $id ?>"><?php echo utf8_encode($row['fecha_de_Inicio']); ?></div></td>
                        <td><div class="text" id="al-<?php echo $id ?>"><?php echo $row['fecha_Fin'] ?></div></td>
                        <td style="text-align:center;"><a class="ver btn btn-success  fa fa-arrow-right "></a></td>
                        <td style="text-align:center;"><a class="editar btn btn-info fa fa-pencil "></a></td>
                        <td style="text-align:center;"><a class="elimina btn btn-danger fa fa-trash-o"></a></td>
                        
                        
                    </tr>
                    <?php
                }
                ?>

            </tbody>
        </table>
    </div>


    <script type="text/javascript">

        $(document).ready(function() {
            $('#tabla_prioridad').dataTable({
                
                "order": [[ 0, "asc" ]],
                "fnDrawCallback": function( oSettings ) {
                    
                }
    }); // example es el id de la tabla
        });
        
    </script>

    <!-- Script necesario para que la tabla se ajuste a el tamanio de la pag-->
    <script type="text/javascript">
  // For demo to fit into DataTables site builder...
  $('#tabla_prioridad')
  .removeClass( 'display' )
  .addClass('table table-striped table-bordered');
</script>

</body>


</html>