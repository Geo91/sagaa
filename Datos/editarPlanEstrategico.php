<?php

include '../Datos/conexion.php';
$idPlan = $_POST['ide'];
$inicio = $_POST['inicio'];
$fin = $_POST['fin'];
$ingreso = $_POST['dpIngreso'];
$modificacion = $_POST['dpModificacion'];
$terminacion = $_POST['dpTerminacion'];


$consulta=$conectar->prepare("CALL pa_editar_plan_estrategico(?,?,?,?,?,?)");
$consulta->bind_param('ssssss',$inicio,$fin,$ingreso,$modificacion,$terminacion,$idPlan);
$resultado=$consulta->execute();

if($resultado==1)
    {
    echo '<div id="resultado" class="alert alert-success">';
    echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo '<strong> El Registro ha sido modificado! </strong>';
    echo ' </div>';
  
    }else{
         echo '<div id="resultado" class="alert alert-danger">';
         echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
         echo '<strong> Error! Revise los datos en intentelo nuevamente. </strong>';
         echo ' </div>';
    }

//include '../pages/PlanEstrategico.php';


?>
<script type="text/javascript">
$(document).ready(function() {
   setTimeout(function() {
       $("#resultado").fadeOut(1500);
   },3000);

});
</script>
