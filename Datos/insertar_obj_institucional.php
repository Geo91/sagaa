<?php

include '../Datos/conexion.php';
//require_once("../conexion/config.inc.php");


$nombre_obj_est_inst = $_POST['nombre_objetivo'];
$descripcion_obj_inst = $_POST['descripcion_objetivo'];
$dimension_id = $_POST['idd'];


  $consulta=$conectar->prepare("CALL pa_insertar_objetivo_institucional(?,?,?)");
  $consulta->bind_param('sss',$nombre_obj_est_inst,$descripcion_obj_inst,$dimension_id);
  $resultado=$consulta->execute();

  if($resultado==1)
      {
      echo '<div id="resultado" class="alert alert-success">';
      echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
      echo '<strong> Se ha creado un nuevo Objetivo Estratégico Institucional! </strong>';
      echo ' </div>';

      }else{
           echo '<div id="resultado" class="alert alert-danger">';
           echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
           echo '<strong> Error! No se puede insertar, revise los datos e intente nuevamente. </strong>';
           echo ' </div>';
      }

//include '../pages/crearPlanEstrategico.php';

 ?>
 <script type="text/javascript">
 $(document).ready(function() {
     setTimeout(function() {
         $("#resultado").fadeOut(1500);

     },3000);

 });
 </script>
