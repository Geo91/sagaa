<?php

include '../Datos/conexion.php';

$inicio = $_POST['inicio'];
$fin = $_POST['fin'];
$fecha_ingreso = $_POST['fecha_ingreso'];
$fecha_modificacion = $_POST['fecha_modificacion'];
$fecha_terminacion = $_POST['fecha_terminacion'];

  $consulta=$conectar->prepare("CALL pa_insertar_plan_estrategico(?,?,?,?,?)");
  $consulta->bind_param('sssss',$inicio,$fin,$fecha_ingreso,$fecha_modificacion,$fecha_terminacion);
  $resultado=$consulta->execute();

  if($resultado==1)
      {
      echo '<div id="resultado" class="alert alert-success">';
      echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
      echo '<strong> Se ha Creado un nuevo Plan Estrategico! </strong>';
      echo ' </div>';

      }else{
           echo '<div id="resultado" class="alert alert-danger">';
           echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
           echo '<strong> Error! No se pueden ingresar, revise las fechas e intente nuevamente. </strong>';
           echo ' </div>';
      }

//include '../pages/planEstrategico.php';

 ?>
 <script type="text/javascript">
 $(document).ready(function() {
     setTimeout(function() {
         $("#resultado").fadeOut(1500);
     },3000);

 });
 </script>
