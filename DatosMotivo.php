<?php
    $maindir = "../../../";
    require_once("../ChekAutoIncrement.php");
    require_once($maindir . "conexion/config.inc.php");
    // AGREGAR ESTA CONSULTA EN UN PROCEDIMIENTO ALMACENADO
    $query1 = "SELECT * FROM `motivos`";
?>

<div class="box">
    <div class="box-body table-responsive">
        <table id="tablaMotivos" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style ="display:none">Codigo</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">DescripciÃ³n</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Editar</th>
                    <th style="text-align:center;background-color:#386D95;color:white;">Eliminar</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $result =$db->prepare($query1);
                    $result->execute();
                    while ($fila = $result->fetch()) {
                        ?>
                        <tr data-id='<?php echo $fila["Motivo_ID"]; ?>'>
                            <td style = "display:none"><?php echo $fila["Motivo_ID"]; ?></td>
                            <td><?php echo $fila["descripcion_motivo"]; ?></td>
                            <td>
                                <center>
                                    <button type="button" class="editarMotivo btn btn-info glyphicon glyphicon-edit"  title="Editar Motivo">
                                </center>
                            </td>
                            <td>
                                <center>
                                    <button type="button" class="eliminarMotivo<?php echo $auto_increment; ?> btn btn-danger glyphicon glyphicon-remove"  title="Eliminar Motivo">
                                </center>
                            </td>
                        </tr>
                <?php } ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div>

<script type="text/javascript">
//opciones para buscador en la tabla tablaAsignaturas
    $('#tablaMotivos')
        .removeClass('display')
        .addClass('table table-striped table-bordered');

    $('#tablaMotivos').dataTable({
        "order": [[0, "asc"]],
        "fnDrawCallback": function (oSettings) {


        }
        ,
        "language":
        {
            "lengthMenu": "Mostrar _MENU_ registros por pÃ¡gina",
            "zeroRecords": "No se han encontrado registros",
            "info": "Mostrando pÃ¡gina _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros)"   ,
            "search": "Buscar",
            "paginate":
                    {
                        "previous": "Anterior",
                        "next" : "Siguiente"
                    }
        }
    });

    $(document).on("click", ".eliminarMotivo<?php echo $auto_increment; ?>", function () {
        if (confirm("Â¿EstÃ¡ seguro de que desea eliminar este registro?")){
            codigo = $(this).parents("tr").data("id");
            data={
                        Codigo:codigo
                }
            $.ajax({
                async: true,
                type: "POST",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded",
                success: eliminarMotivo,
                timeout: 6000,
                error: problemas
            });
            return false;
        }
    });

</script>
