<?php

  if(isset($_GET['contenido']))
    {
      $contenido = $_GET['contenido'];
    }
  else
    {
      $contenido = 'home';
    }
    
  require_once("funciones/check_session.php");

  require_once("funciones/timeout.php");

  require_once("pages/navbar.php");

?>

<!-- Main -->
<div class="container-fluid">
<div class="row">
    <div class="col-sm-12" style = "align:center">
        
    	<div class="row">
<br>
	        <div class="col-sm-4">
				<div class="featurette">
						
						<br>
						<br>
							<img class="featurette-image img-square pull-left" style = "width:100%" src="assets/img/logo_sagaa.png">
						         
				</div>
	        </div>

	        <div class ="col-sm-6">
	        <br>
	        <br>
	        		<h4 class="featurette-heading">Facultad de Ciencias Jurídicas. <span class="text-muted">  </span></h4>  
	            	<p class="lead" style = "text-align:justify">El Sistema de Apoyo para la Gestión Académica y Administrativa (SAGAA) es una plataforma tecnológica propia de la Facultad de Ciencias Jurídicas, diseñada para facilitar a los colaboradores la realización de los procesos académicos y administrativos internos a través de una interfaz sencilla y amigable para los usuarios.</p>
		<br>
		<br>
		<br>
	        </div>
		<br>
		</div>
	<br>
    </div><!--/col-span-9-->  
<br>   
</div>
<br>
</div>
<!-- /Main -->

<script>
  window.onbeforeunload = function(event) {
    // do something
    alert('alert()');
    $.ajax("funciones/eventoCerrarPestaña.php");
};

$( window ).unload(function() {
  return "Bye now!";
});
</script>